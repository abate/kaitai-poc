const KaitaiStream = require('kaitai-struct/KaitaiStream');
const BlockHeaderShell = require("./kaitai/BlockHeaderShell");
const axios = require('axios')
const base58 = require('bs58check')

const block_hash = "BLYDg1hJPTMBNzcp72WTUVKZLLPnB88WLzKsyUdLAWM2Xn1qxbD"
const BASE_URL = "https://rpc.ghostnet.teztnets.com"
const path = `${BASE_URL}/chains/main/blocks/${block_hash}/header/shell`;

// Helper function used to prepen the block indentifier to
// the raw bytes from the RPC
function prependBytes(existingArray) {
  const block_bytes = new Uint8Array([0x01, 0x34]);
  // Create a new Uint8Array with the concatenated result
  const newArray = new Uint8Array(block_bytes.length + existingArray.length);
  newArray.set(block_bytes, 0);
  newArray.set(existingArray, block_bytes.length);
  return newArray
}

// Query the node asking for the binary RPC and decode using kaitai
axios.get(path, { headers: {"Accept": "application/octet-stream"}, responseType: "arraybuffer" })
  .then((res) => {
    const a = new Uint8Array(res.data).slice(4)
    //console.log(a)
    //console.log(a.length)
    const stream = new KaitaiStream(a)
    const c = new BlockHeaderShell(stream)
    console.log(JSON.stringify({
      level: c.level,
      proto: c.proto,
      predecessor: base58.encode(prependBytes(c.predecessor))
    }))
  })
  .catch((error) => {
    console.error(error);
  });

// Query the node asking for the js representation and dump its content
axios.get(path, { headers: {"Accept": "application/json"} })
  .then((res) => {
    console.log(JSON.stringify({
      level: res.data.level,
      proto: res.data.proto,
      predecessor: res.data.predecessor
    }))
  })
  .catch((error) => {
    console.error(error);
  });
