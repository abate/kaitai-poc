// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id011Pthangz2ReceiptBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 011-PtHangz2.receipt.balance_updates
 */

var Id011Pthangz2ReceiptBalanceUpdates = (function() {
  Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2ContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceTag = Object.freeze({
    CONTRACT: 0,
    REWARDS: 1,
    FEES: 2,
    DEPOSITS: 3,

    0: "CONTRACT",
    1: "REWARDS",
    2: "FEES",
    3: "DEPOSITS",
  });

  Id011Pthangz2ReceiptBalanceUpdates.OriginTag = Object.freeze({
    BLOCK_APPLICATION: 0,
    PROTOCOL_MIGRATION: 1,
    SUBSIDY: 2,

    0: "BLOCK_APPLICATION",
    1: "PROTOCOL_MIGRATION",
    2: "SUBSIDY",
  });

  Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id011Pthangz2ReceiptBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2ReceiptBalanceUpdates.prototype._read = function() {
    this.id011Pthangz2OperationMetadataAlphaBalanceUpdates = new Id011Pthangz2OperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Originated = Id011Pthangz2ReceiptBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Id011Pthangz2ContractId = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2ContractId = (function() {
    function Id011Pthangz2ContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2ContractId.prototype._read = function() {
      this.id011Pthangz2ContractIdTag = this._io.readU1();
      if (this.id011Pthangz2ContractIdTag == Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2ContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id011Pthangz2ContractIdTag == Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2ContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id011Pthangz2ContractId;
  })();

  var Rewards = Id011Pthangz2ReceiptBalanceUpdates.Rewards = (function() {
    function Rewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Rewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Rewards;
  })();

  var Id011Pthangz2OperationMetadataAlphaBalanceUpdates0 = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id011Pthangz2OperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2OperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId011Pthangz2OperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId011Pthangz2OperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId011Pthangz2OperationMetadataAlphaBalanceUpdates, this._io, "/types/id_011__pthangz2__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id011Pthangz2OperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId011Pthangz2OperationMetadataAlphaBalanceUpdates);
      var _io__raw_id011Pthangz2OperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id011Pthangz2OperationMetadataAlphaBalanceUpdates);
      this.id011Pthangz2OperationMetadataAlphaBalanceUpdates = new Id011Pthangz2OperationMetadataAlphaBalanceUpdates(_io__raw_id011Pthangz2OperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id011Pthangz2OperationMetadataAlphaBalanceUpdates0;
  })();

  var Deposits = Id011Pthangz2ReceiptBalanceUpdates.Deposits = (function() {
    function Deposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposits.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Deposits;
  })();

  var Id011Pthangz2OperationMetadataAlphaBalance = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalance = (function() {
    function Id011Pthangz2OperationMetadataAlphaBalance(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2OperationMetadataAlphaBalance.prototype._read = function() {
      this.id011Pthangz2OperationMetadataAlphaBalanceTag = this._io.readU1();
      if (this.id011Pthangz2OperationMetadataAlphaBalanceTag == Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceTag.CONTRACT) {
        this.contract = new Id011Pthangz2ContractId(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationMetadataAlphaBalanceTag == Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceTag.REWARDS) {
        this.rewards = new Rewards(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationMetadataAlphaBalanceTag == Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceTag.FEES) {
        this.fees = new Fees(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationMetadataAlphaBalanceTag == Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceTag.DEPOSITS) {
        this.deposits = new Deposits(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Id011Pthangz2OperationMetadataAlphaBalance;
  })();

  var Fees = Id011Pthangz2ReceiptBalanceUpdates.Fees = (function() {
    function Fees(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Fees.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Fees;
  })();

  var Id011Pthangz2OperationMetadataAlphaBalanceUpdatesEntries = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id011Pthangz2OperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2OperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id011Pthangz2OperationMetadataAlphaBalance = new Id011Pthangz2OperationMetadataAlphaBalance(this._io, this, this._root);
      this.id011Pthangz2OperationMetadataAlphaBalanceUpdate = new Id011Pthangz2OperationMetadataAlphaBalanceUpdate(this._io, this, this._root);
      this.id011Pthangz2OperationMetadataAlphaUpdateOrigin = new Id011Pthangz2OperationMetadataAlphaUpdateOrigin(this._io, this, this._root);
    }

    return Id011Pthangz2OperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var Id011Pthangz2OperationMetadataAlphaBalanceUpdates = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceUpdates = (function() {
    function Id011Pthangz2OperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2OperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id011Pthangz2OperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id011Pthangz2OperationMetadataAlphaBalanceUpdatesEntries.push(new Id011Pthangz2OperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id011Pthangz2OperationMetadataAlphaBalanceUpdates;
  })();

  var Id011Pthangz2OperationMetadataAlphaBalanceUpdate = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceUpdate = (function() {
    function Id011Pthangz2OperationMetadataAlphaBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2OperationMetadataAlphaBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id011Pthangz2OperationMetadataAlphaBalanceUpdate;
  })();

  var Id011Pthangz2OperationMetadataAlphaUpdateOrigin = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaUpdateOrigin = (function() {
    function Id011Pthangz2OperationMetadataAlphaUpdateOrigin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2OperationMetadataAlphaUpdateOrigin.prototype._read = function() {
      this.origin = this._io.readU1();
    }

    return Id011Pthangz2OperationMetadataAlphaUpdateOrigin;
  })();

  var PublicKeyHash = Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id011Pthangz2ReceiptBalanceUpdates;
})();
return Id011Pthangz2ReceiptBalanceUpdates;
}));
