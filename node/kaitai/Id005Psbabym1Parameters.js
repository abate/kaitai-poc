// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id005Psbabym1Parameters = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 005-PsBabyM1.parameters
 */

var Id005Psbabym1Parameters = (function() {
  Id005Psbabym1Parameters.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id005Psbabym1Parameters.BootstrapAccountsEltTag = Object.freeze({
    PUBLIC_KEY_KNOWN: 0,
    PUBLIC_KEY_UNKNOWN: 1,

    0: "PUBLIC_KEY_KNOWN",
    1: "PUBLIC_KEY_UNKNOWN",
  });

  Id005Psbabym1Parameters.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id005Psbabym1Parameters.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id005Psbabym1Parameters(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1Parameters.prototype._read = function() {
    this.bootstrapAccounts = new BootstrapAccounts0(this._io, this, this._root);
    this.bootstrapContracts = new BootstrapContracts0(this._io, this, this._root);
    this.commitments = new Commitments0(this._io, this, this._root);
    this.securityDepositRampUpCyclesTag = this._io.readU1();
    if (this.securityDepositRampUpCyclesTag == Id005Psbabym1Parameters.Bool.TRUE) {
      this.securityDepositRampUpCycles = new Int31(this._io, this, this._root);
    }
    this.noRewardCyclesTag = this._io.readU1();
    if (this.noRewardCyclesTag == Id005Psbabym1Parameters.Bool.TRUE) {
      this.noRewardCycles = new Int31(this._io, this, this._root);
    }
    this.preservedCycles = this._io.readU1();
    this.blocksPerCycle = this._io.readS4be();
    this.blocksPerCommitment = this._io.readS4be();
    this.blocksPerRollSnapshot = this._io.readS4be();
    this.blocksPerVotingPeriod = this._io.readS4be();
    this.timeBetweenBlocks = new TimeBetweenBlocks0(this._io, this, this._root);
    this.endorsersPerBlock = this._io.readU2be();
    this.hardGasLimitPerOperation = new Z(this._io, this, this._root);
    this.hardGasLimitPerBlock = new Z(this._io, this, this._root);
    this.proofOfWorkThreshold = this._io.readS8be();
    this.tokensPerRoll = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.michelsonMaximumTypeSize = this._io.readU2be();
    this.seedNonceRevelationTip = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.originationSize = new Int31(this._io, this, this._root);
    this.blockSecurityDeposit = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.endorsementSecurityDeposit = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.blockReward = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.endorsementReward = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.costPerByte = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.hardStorageLimitPerOperation = new Z(this._io, this, this._root);
    this.testChainDuration = this._io.readS8be();
    this.quorumMin = this._io.readS4be();
    this.quorumMax = this._io.readS4be();
    this.minProposalQuorum = this._io.readS4be();
    this.initialEndorsers = this._io.readU2be();
    this.delayPerMissingEndorsement = this._io.readS8be();
  }

  var TimeBetweenBlocksEntries = Id005Psbabym1Parameters.TimeBetweenBlocksEntries = (function() {
    function TimeBetweenBlocksEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TimeBetweenBlocksEntries.prototype._read = function() {
      this.timeBetweenBlocksElt = this._io.readS8be();
    }

    return TimeBetweenBlocksEntries;
  })();

  var PublicKeyKnown = Id005Psbabym1Parameters.PublicKeyKnown = (function() {
    function PublicKeyKnown(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyKnown.prototype._read = function() {
      this.publicKeyKnownField0 = new PublicKey(this._io, this, this._root);
      this.publicKeyKnownField1 = new Id005Psbabym1Mutez(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key
     * 
     * signature__v0__public_key
     */

    /**
     * id_005__psbabym1__mutez
     */

    return PublicKeyKnown;
  })();

  var Commitments = Id005Psbabym1Parameters.Commitments = (function() {
    function Commitments(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments.prototype._read = function() {
      this.commitmentsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.commitmentsEntries.push(new CommitmentsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Commitments;
  })();

  var N = Id005Psbabym1Parameters.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var TimeBetweenBlocks = Id005Psbabym1Parameters.TimeBetweenBlocks = (function() {
    function TimeBetweenBlocks(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TimeBetweenBlocks.prototype._read = function() {
      this.timeBetweenBlocksEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.timeBetweenBlocksEntries.push(new TimeBetweenBlocksEntries(this._io, this, this._root));
        i++;
      }
    }

    return TimeBetweenBlocks;
  })();

  var Id005Psbabym1Mutez = Id005Psbabym1Parameters.Id005Psbabym1Mutez = (function() {
    function Id005Psbabym1Mutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1Mutez.prototype._read = function() {
      this.id005Psbabym1Mutez = new N(this._io, this, this._root);
    }

    return Id005Psbabym1Mutez;
  })();

  var CommitmentsEntries = Id005Psbabym1Parameters.CommitmentsEntries = (function() {
    function CommitmentsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CommitmentsEntries.prototype._read = function() {
      this.commitmentsEltField0 = this._io.readBytes(20);
      this.commitmentsEltField1 = new Id005Psbabym1Mutez(this._io, this, this._root);
    }

    /**
     * blinded__public__key__hash
     */

    /**
     * id_005__psbabym1__mutez
     */

    return CommitmentsEntries;
  })();

  var Commitments0 = Id005Psbabym1Parameters.Commitments0 = (function() {
    function Commitments0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments0.prototype._read = function() {
      this.lenCommitments = this._io.readU4be();
      if (!(this.lenCommitments <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCommitments, this._io, "/types/commitments_0/seq/0");
      }
      this._raw_commitments = this._io.readBytes(this.lenCommitments);
      var _io__raw_commitments = new KaitaiStream(this._raw_commitments);
      this.commitments = new Commitments(_io__raw_commitments, this, this._root);
    }

    return Commitments0;
  })();

  var BootstrapAccounts0 = Id005Psbabym1Parameters.BootstrapAccounts0 = (function() {
    function BootstrapAccounts0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccounts0.prototype._read = function() {
      this.lenBootstrapAccounts = this._io.readU4be();
      if (!(this.lenBootstrapAccounts <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBootstrapAccounts, this._io, "/types/bootstrap_accounts_0/seq/0");
      }
      this._raw_bootstrapAccounts = this._io.readBytes(this.lenBootstrapAccounts);
      var _io__raw_bootstrapAccounts = new KaitaiStream(this._raw_bootstrapAccounts);
      this.bootstrapAccounts = new BootstrapAccounts(_io__raw_bootstrapAccounts, this, this._root);
    }

    return BootstrapAccounts0;
  })();

  var PublicKey = Id005Psbabym1Parameters.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id005Psbabym1Parameters.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id005Psbabym1Parameters.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id005Psbabym1Parameters.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var PublicKeyUnknown = Id005Psbabym1Parameters.PublicKeyUnknown = (function() {
    function PublicKeyUnknown(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyUnknown.prototype._read = function() {
      this.publicKeyUnknownField0 = new PublicKeyHash(this._io, this, this._root);
      this.publicKeyUnknownField1 = new Id005Psbabym1Mutez(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     * 
     * signature__v0__public_key_hash
     */

    /**
     * id_005__psbabym1__mutez
     */

    return PublicKeyUnknown;
  })();

  var BootstrapAccountsEntries = Id005Psbabym1Parameters.BootstrapAccountsEntries = (function() {
    function BootstrapAccountsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccountsEntries.prototype._read = function() {
      this.bootstrapAccountsEltTag = this._io.readU1();
      if (this.bootstrapAccountsEltTag == Id005Psbabym1Parameters.BootstrapAccountsEltTag.PUBLIC_KEY_KNOWN) {
        this.publicKeyKnown = new PublicKeyKnown(this._io, this, this._root);
      }
      if (this.bootstrapAccountsEltTag == Id005Psbabym1Parameters.BootstrapAccountsEltTag.PUBLIC_KEY_UNKNOWN) {
        this.publicKeyUnknown = new PublicKeyUnknown(this._io, this, this._root);
      }
    }

    return BootstrapAccountsEntries;
  })();

  var BootstrapContracts0 = Id005Psbabym1Parameters.BootstrapContracts0 = (function() {
    function BootstrapContracts0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContracts0.prototype._read = function() {
      this.lenBootstrapContracts = this._io.readU4be();
      if (!(this.lenBootstrapContracts <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBootstrapContracts, this._io, "/types/bootstrap_contracts_0/seq/0");
      }
      this._raw_bootstrapContracts = this._io.readBytes(this.lenBootstrapContracts);
      var _io__raw_bootstrapContracts = new KaitaiStream(this._raw_bootstrapContracts);
      this.bootstrapContracts = new BootstrapContracts(_io__raw_bootstrapContracts, this, this._root);
    }

    return BootstrapContracts0;
  })();

  var BytesDynUint30 = Id005Psbabym1Parameters.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var TimeBetweenBlocks0 = Id005Psbabym1Parameters.TimeBetweenBlocks0 = (function() {
    function TimeBetweenBlocks0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TimeBetweenBlocks0.prototype._read = function() {
      this.lenTimeBetweenBlocks = this._io.readU4be();
      if (!(this.lenTimeBetweenBlocks <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenTimeBetweenBlocks, this._io, "/types/time_between_blocks_0/seq/0");
      }
      this._raw_timeBetweenBlocks = this._io.readBytes(this.lenTimeBetweenBlocks);
      var _io__raw_timeBetweenBlocks = new KaitaiStream(this._raw_timeBetweenBlocks);
      this.timeBetweenBlocks = new TimeBetweenBlocks(_io__raw_timeBetweenBlocks, this, this._root);
    }

    return TimeBetweenBlocks0;
  })();

  var BootstrapContractsEntries = Id005Psbabym1Parameters.BootstrapContractsEntries = (function() {
    function BootstrapContractsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContractsEntries.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.amount = new Id005Psbabym1Mutez(this._io, this, this._root);
      this.script = new Id005Psbabym1ScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return BootstrapContractsEntries;
  })();

  var Id005Psbabym1ScriptedContracts = Id005Psbabym1Parameters.Id005Psbabym1ScriptedContracts = (function() {
    function Id005Psbabym1ScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1ScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id005Psbabym1ScriptedContracts;
  })();

  var BootstrapContracts = Id005Psbabym1Parameters.BootstrapContracts = (function() {
    function BootstrapContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContracts.prototype._read = function() {
      this.bootstrapContractsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bootstrapContractsEntries.push(new BootstrapContractsEntries(this._io, this, this._root));
        i++;
      }
    }

    return BootstrapContracts;
  })();

  var BootstrapAccounts = Id005Psbabym1Parameters.BootstrapAccounts = (function() {
    function BootstrapAccounts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccounts.prototype._read = function() {
      this.bootstrapAccountsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bootstrapAccountsEntries.push(new BootstrapAccountsEntries(this._io, this, this._root));
        i++;
      }
    }

    return BootstrapAccounts;
  })();

  var Int31 = Id005Psbabym1Parameters.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id005Psbabym1Parameters.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var PublicKeyHash = Id005Psbabym1Parameters.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id005Psbabym1Parameters.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id005Psbabym1Parameters.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id005Psbabym1Parameters.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Z = Id005Psbabym1Parameters.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  return Id005Psbabym1Parameters;
})();
return Id005Psbabym1Parameters;
}));
