// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id010PtgranadRoll = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 010-PtGRANAD.roll
 */

var Id010PtgranadRoll = (function() {
  function Id010PtgranadRoll(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id010PtgranadRoll.prototype._read = function() {
    this.id010PtgranadRoll = this._io.readS4be();
  }

  return Id010PtgranadRoll;
})();
return Id010PtgranadRoll;
}));
