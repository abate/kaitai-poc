// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id009PsflorenOperationInternal = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 009-PsFLoren.operation.internal
 */

var Id009PsflorenOperationInternal = (function() {
  Id009PsflorenOperationInternal.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id009PsflorenOperationInternal.Id009PsflorenContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id009PsflorenOperationInternal.Id009PsflorenEntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    255: "NAMED",
  });

  Id009PsflorenOperationInternal.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id009PsflorenOperationInternal.Id009PsflorenOperationAlphaInternalOperationTag = Object.freeze({
    REVEAL: 0,
    TRANSACTION: 1,
    ORIGINATION: 2,
    DELEGATION: 3,

    0: "REVEAL",
    1: "TRANSACTION",
    2: "ORIGINATION",
    3: "DELEGATION",
  });

  Id009PsflorenOperationInternal.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id009PsflorenOperationInternal(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id009PsflorenOperationInternal.prototype._read = function() {
    this.id009PsflorenOperationAlphaInternalOperation = new Id009PsflorenOperationAlphaInternalOperation(this._io, this, this._root);
  }

  var Id009PsflorenContractId = Id009PsflorenOperationInternal.Id009PsflorenContractId = (function() {
    function Id009PsflorenContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenContractId.prototype._read = function() {
      this.id009PsflorenContractIdTag = this._io.readU1();
      if (this.id009PsflorenContractIdTag == Id009PsflorenOperationInternal.Id009PsflorenContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id009PsflorenContractIdTag == Id009PsflorenOperationInternal.Id009PsflorenContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id009PsflorenContractId;
  })();

  var Id009PsflorenScriptedContracts = Id009PsflorenOperationInternal.Id009PsflorenScriptedContracts = (function() {
    function Id009PsflorenScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id009PsflorenScriptedContracts;
  })();

  var Originated = Id009PsflorenOperationInternal.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var N = Id009PsflorenOperationInternal.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id009PsflorenOperationAlphaInternalOperation = Id009PsflorenOperationInternal.Id009PsflorenOperationAlphaInternalOperation = (function() {
    function Id009PsflorenOperationAlphaInternalOperation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenOperationAlphaInternalOperation.prototype._read = function() {
      this.source = new Id009PsflorenContractId(this._io, this, this._root);
      this.nonce = this._io.readU2be();
      this.id009PsflorenOperationAlphaInternalOperationTag = this._io.readU1();
      if (this.id009PsflorenOperationAlphaInternalOperationTag == Id009PsflorenOperationInternal.Id009PsflorenOperationAlphaInternalOperationTag.REVEAL) {
        this.reveal = new PublicKey(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaInternalOperationTag == Id009PsflorenOperationInternal.Id009PsflorenOperationAlphaInternalOperationTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaInternalOperationTag == Id009PsflorenOperationInternal.Id009PsflorenOperationAlphaInternalOperationTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaInternalOperationTag == Id009PsflorenOperationInternal.Id009PsflorenOperationAlphaInternalOperationTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Id009PsflorenOperationAlphaInternalOperation;
  })();

  var Id009PsflorenMutez = Id009PsflorenOperationInternal.Id009PsflorenMutez = (function() {
    function Id009PsflorenMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenMutez.prototype._read = function() {
      this.id009PsflorenMutez = new N(this._io, this, this._root);
    }

    return Id009PsflorenMutez;
  })();

  var PublicKey = Id009PsflorenOperationInternal.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id009PsflorenOperationInternal.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id009PsflorenOperationInternal.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id009PsflorenOperationInternal.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var Named = Id009PsflorenOperationInternal.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Delegation = Id009PsflorenOperationInternal.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id009PsflorenOperationInternal.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var BytesDynUint30 = Id009PsflorenOperationInternal.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Id009PsflorenEntrypoint = Id009PsflorenOperationInternal.Id009PsflorenEntrypoint = (function() {
    function Id009PsflorenEntrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenEntrypoint.prototype._read = function() {
      this.id009PsflorenEntrypointTag = this._io.readU1();
      if (this.id009PsflorenEntrypointTag == Id009PsflorenOperationInternal.Id009PsflorenEntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id009PsflorenEntrypoint;
  })();

  var Origination = Id009PsflorenOperationInternal.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.balance = new Id009PsflorenMutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id009PsflorenOperationInternal.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id009PsflorenScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var NChunk = Id009PsflorenOperationInternal.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Named0 = Id009PsflorenOperationInternal.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id009PsflorenOperationInternal.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.amount = new Id009PsflorenMutez(this._io, this, this._root);
      this.destination = new Id009PsflorenContractId(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id009PsflorenOperationInternal.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Transaction;
  })();

  var Parameters = Id009PsflorenOperationInternal.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id009PsflorenEntrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var PublicKeyHash = Id009PsflorenOperationInternal.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id009PsflorenOperationInternal.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id009PsflorenOperationInternal.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id009PsflorenOperationInternal.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id009PsflorenOperationInternal;
})();
return Id009PsflorenOperationInternal;
}));
