// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaReceiptBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.receipt.balance_updates
 */

var AlphaReceiptBalanceUpdates = (function() {
  AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaUpdateOriginTag = Object.freeze({
    BLOCK_APPLICATION: 0,
    PROTOCOL_MIGRATION: 1,
    SUBSIDY: 2,
    SIMULATION: 3,
    DELAYED_OPERATION: 4,

    0: "BLOCK_APPLICATION",
    1: "PROTOCOL_MIGRATION",
    2: "SUBSIDY",
    3: "SIMULATION",
    4: "DELAYED_OPERATION",
  });

  AlphaReceiptBalanceUpdates.AlphaFrozenStakerTag = Object.freeze({
    SINGLE: 0,
    SHARED: 1,
    BAKER: 2,
    BAKER_EDGE: 3,

    0: "SINGLE",
    1: "SHARED",
    2: "BAKER",
    3: "BAKER_EDGE",
  });

  AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag = Object.freeze({
    CONTRACT: 0,
    BLOCK_FEES: 2,
    DEPOSITS: 4,
    NONCE_REVELATION_REWARDS: 5,
    ATTESTING_REWARDS: 7,
    BAKING_REWARDS: 8,
    BAKING_BONUSES: 9,
    STORAGE_FEES: 11,
    DOUBLE_SIGNING_PUNISHMENTS: 12,
    LOST_ATTESTING_REWARDS: 13,
    LIQUIDITY_BAKING_SUBSIDIES: 14,
    BURNED: 15,
    COMMITMENTS: 16,
    BOOTSTRAP: 17,
    INVOICE: 18,
    INITIAL_COMMITMENTS: 19,
    MINTED: 20,
    FROZEN_BONDS: 21,
    SMART_ROLLUP_REFUTATION_PUNISHMENTS: 24,
    SMART_ROLLUP_REFUTATION_REWARDS: 25,
    UNSTAKED_DEPOSITS: 26,
    STAKING_DELEGATOR_NUMERATOR: 27,
    STAKING_DELEGATE_DENOMINATOR: 28,

    0: "CONTRACT",
    2: "BLOCK_FEES",
    4: "DEPOSITS",
    5: "NONCE_REVELATION_REWARDS",
    7: "ATTESTING_REWARDS",
    8: "BAKING_REWARDS",
    9: "BAKING_BONUSES",
    11: "STORAGE_FEES",
    12: "DOUBLE_SIGNING_PUNISHMENTS",
    13: "LOST_ATTESTING_REWARDS",
    14: "LIQUIDITY_BAKING_SUBSIDIES",
    15: "BURNED",
    16: "COMMITMENTS",
    17: "BOOTSTRAP",
    18: "INVOICE",
    19: "INITIAL_COMMITMENTS",
    20: "MINTED",
    21: "FROZEN_BONDS",
    24: "SMART_ROLLUP_REFUTATION_PUNISHMENTS",
    25: "SMART_ROLLUP_REFUTATION_REWARDS",
    26: "UNSTAKED_DEPOSITS",
    27: "STAKING_DELEGATOR_NUMERATOR",
    28: "STAKING_DELEGATE_DENOMINATOR",
  });

  AlphaReceiptBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  AlphaReceiptBalanceUpdates.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  AlphaReceiptBalanceUpdates.AlphaBondIdTag = Object.freeze({
    SMART_ROLLUP_BOND_ID: 1,

    1: "SMART_ROLLUP_BOND_ID",
  });

  AlphaReceiptBalanceUpdates.AlphaContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  AlphaReceiptBalanceUpdates.AlphaStakerTag = Object.freeze({
    SINGLE: 0,
    SHARED: 1,

    0: "SINGLE",
    1: "SHARED",
  });

  function AlphaReceiptBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaReceiptBalanceUpdates.prototype._read = function() {
    this.alphaOperationMetadataAlphaBalanceUpdates = new AlphaOperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var AlphaContractId = AlphaReceiptBalanceUpdates.AlphaContractId = (function() {
    function AlphaContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaContractId.prototype._read = function() {
      this.alphaContractIdTag = this._io.readU1();
      if (this.alphaContractIdTag == AlphaReceiptBalanceUpdates.AlphaContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.alphaContractIdTag == AlphaReceiptBalanceUpdates.AlphaContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return AlphaContractId;
  })();

  var Commitments = AlphaReceiptBalanceUpdates.Commitments = (function() {
    function Commitments(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments.prototype._read = function() {
      this.committer = this._io.readBytes(20);
      this.alphaOperationMetadataAlphaTezBalanceUpdate = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    return Commitments;
  })();

  var AlphaBondId = AlphaReceiptBalanceUpdates.AlphaBondId = (function() {
    function AlphaBondId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaBondId.prototype._read = function() {
      this.alphaBondIdTag = this._io.readU1();
      if (this.alphaBondIdTag == AlphaReceiptBalanceUpdates.AlphaBondIdTag.SMART_ROLLUP_BOND_ID) {
        this.smartRollupBondId = this._io.readBytes(20);
      }
    }

    return AlphaBondId;
  })();

  var FrozenBonds = AlphaReceiptBalanceUpdates.FrozenBonds = (function() {
    function FrozenBonds(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    FrozenBonds.prototype._read = function() {
      this.contract = new AlphaContractId(this._io, this, this._root);
      this.bondId = new AlphaBondId(this._io, this, this._root);
      this.alphaOperationMetadataAlphaTezBalanceUpdate = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return FrozenBonds;
  })();

  var Originated = AlphaReceiptBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var AlphaFrozenStaker = AlphaReceiptBalanceUpdates.AlphaFrozenStaker = (function() {
    function AlphaFrozenStaker(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaFrozenStaker.prototype._read = function() {
      this.alphaFrozenStakerTag = this._io.readU1();
      if (this.alphaFrozenStakerTag == AlphaReceiptBalanceUpdates.AlphaFrozenStakerTag.SINGLE) {
        this.single = new Single(this._io, this, this._root);
      }
      if (this.alphaFrozenStakerTag == AlphaReceiptBalanceUpdates.AlphaFrozenStakerTag.SHARED) {
        this.shared = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.alphaFrozenStakerTag == AlphaReceiptBalanceUpdates.AlphaFrozenStakerTag.BAKER) {
        this.baker = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.alphaFrozenStakerTag == AlphaReceiptBalanceUpdates.AlphaFrozenStakerTag.BAKER_EDGE) {
        this.bakerEdge = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return AlphaFrozenStaker;
  })();

  var AlphaOperationMetadataAlphaBalanceUpdates0 = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceUpdates0 = (function() {
    function AlphaOperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaOperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenAlphaOperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenAlphaOperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenAlphaOperationMetadataAlphaBalanceUpdates, this._io, "/types/alpha__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_alphaOperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenAlphaOperationMetadataAlphaBalanceUpdates);
      var _io__raw_alphaOperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_alphaOperationMetadataAlphaBalanceUpdates);
      this.alphaOperationMetadataAlphaBalanceUpdates = new AlphaOperationMetadataAlphaBalanceUpdates(_io__raw_alphaOperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return AlphaOperationMetadataAlphaBalanceUpdates0;
  })();

  var AlphaOperationMetadataAlphaBalanceUpdatesEntries = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function AlphaOperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaOperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.alphaOperationMetadataAlphaBalanceAndUpdate = new AlphaOperationMetadataAlphaBalanceAndUpdate(this._io, this, this._root);
      this.alphaOperationMetadataAlphaUpdateOrigin = new AlphaOperationMetadataAlphaUpdateOrigin(this._io, this, this._root);
    }

    return AlphaOperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var AlphaOperationMetadataAlphaTezBalanceUpdate = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate = (function() {
    function AlphaOperationMetadataAlphaTezBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaOperationMetadataAlphaTezBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return AlphaOperationMetadataAlphaTezBalanceUpdate;
  })();

  var StakingDelegateDenominator = AlphaReceiptBalanceUpdates.StakingDelegateDenominator = (function() {
    function StakingDelegateDenominator(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    StakingDelegateDenominator.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.alphaOperationMetadataAlphaStakingAbstractQuantity = new AlphaOperationMetadataAlphaStakingAbstractQuantity(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return StakingDelegateDenominator;
  })();

  var AlphaOperationMetadataAlphaUpdateOrigin = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaUpdateOrigin = (function() {
    function AlphaOperationMetadataAlphaUpdateOrigin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaOperationMetadataAlphaUpdateOrigin.prototype._read = function() {
      this.alphaOperationMetadataAlphaUpdateOriginTag = this._io.readU1();
      if (this.alphaOperationMetadataAlphaUpdateOriginTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaUpdateOriginTag.DELAYED_OPERATION) {
        this.delayedOperation = this._io.readBytes(32);
      }
    }

    return AlphaOperationMetadataAlphaUpdateOrigin;
  })();

  var Single = AlphaReceiptBalanceUpdates.Single = (function() {
    function Single(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Single.prototype._read = function() {
      this.contract = new AlphaContractId(this._io, this, this._root);
      this.delegate = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Single;
  })();

  var Deposits = AlphaReceiptBalanceUpdates.Deposits = (function() {
    function Deposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposits.prototype._read = function() {
      this.staker = new AlphaFrozenStaker(this._io, this, this._root);
      this.alphaOperationMetadataAlphaTezBalanceUpdate = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * frozen_staker: Abstract notion of staker used in operation receipts for frozen deposits, either a single staker or all the stakers delegating to some delegate.
     */

    return Deposits;
  })();

  var AlphaOperationMetadataAlphaBalanceUpdates = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceUpdates = (function() {
    function AlphaOperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaOperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.alphaOperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.alphaOperationMetadataAlphaBalanceUpdatesEntries.push(new AlphaOperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return AlphaOperationMetadataAlphaBalanceUpdates;
  })();

  var AlphaOperationMetadataAlphaStakingAbstractQuantity = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaStakingAbstractQuantity = (function() {
    function AlphaOperationMetadataAlphaStakingAbstractQuantity(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaOperationMetadataAlphaStakingAbstractQuantity.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return AlphaOperationMetadataAlphaStakingAbstractQuantity;
  })();

  var LostAttestingRewards = AlphaReceiptBalanceUpdates.LostAttestingRewards = (function() {
    function LostAttestingRewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LostAttestingRewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.participation = this._io.readU1();
      this.revelation = this._io.readU1();
      this.alphaOperationMetadataAlphaTezBalanceUpdate = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return LostAttestingRewards;
  })();

  var AlphaStaker = AlphaReceiptBalanceUpdates.AlphaStaker = (function() {
    function AlphaStaker(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaStaker.prototype._read = function() {
      this.alphaStakerTag = this._io.readU1();
      if (this.alphaStakerTag == AlphaReceiptBalanceUpdates.AlphaStakerTag.SINGLE) {
        this.single = new Single(this._io, this, this._root);
      }
      if (this.alphaStakerTag == AlphaReceiptBalanceUpdates.AlphaStakerTag.SHARED) {
        this.shared = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return AlphaStaker;
  })();

  var UnstakedDeposits = AlphaReceiptBalanceUpdates.UnstakedDeposits = (function() {
    function UnstakedDeposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    UnstakedDeposits.prototype._read = function() {
      this.staker = new AlphaStaker(this._io, this, this._root);
      this.cycle = this._io.readS4be();
      this.alphaOperationMetadataAlphaTezBalanceUpdate = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * unstaked_frozen_staker: Abstract notion of staker used in operation receipts for unstaked frozen deposits, either a single staker or all the stakers delegating to some delegate.
     */

    return UnstakedDeposits;
  })();

  var AlphaOperationMetadataAlphaBalanceAndUpdate = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdate = (function() {
    function AlphaOperationMetadataAlphaBalanceAndUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaOperationMetadataAlphaBalanceAndUpdate.prototype._read = function() {
      this.alphaOperationMetadataAlphaBalanceAndUpdateTag = this._io.readU1();
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.CONTRACT) {
        this.contract = new Contract(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.BLOCK_FEES) {
        this.blockFees = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.DEPOSITS) {
        this.deposits = new Deposits(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.NONCE_REVELATION_REWARDS) {
        this.nonceRevelationRewards = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.ATTESTING_REWARDS) {
        this.attestingRewards = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.BAKING_REWARDS) {
        this.bakingRewards = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.BAKING_BONUSES) {
        this.bakingBonuses = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.STORAGE_FEES) {
        this.storageFees = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.DOUBLE_SIGNING_PUNISHMENTS) {
        this.doubleSigningPunishments = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.LOST_ATTESTING_REWARDS) {
        this.lostAttestingRewards = new LostAttestingRewards(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.LIQUIDITY_BAKING_SUBSIDIES) {
        this.liquidityBakingSubsidies = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.BURNED) {
        this.burned = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.COMMITMENTS) {
        this.commitments = new Commitments(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.BOOTSTRAP) {
        this.bootstrap = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.INVOICE) {
        this.invoice = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.INITIAL_COMMITMENTS) {
        this.initialCommitments = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.MINTED) {
        this.minted = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.FROZEN_BONDS) {
        this.frozenBonds = new FrozenBonds(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.SMART_ROLLUP_REFUTATION_PUNISHMENTS) {
        this.smartRollupRefutationPunishments = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.SMART_ROLLUP_REFUTATION_REWARDS) {
        this.smartRollupRefutationRewards = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.UNSTAKED_DEPOSITS) {
        this.unstakedDeposits = new UnstakedDeposits(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.STAKING_DELEGATOR_NUMERATOR) {
        this.stakingDelegatorNumerator = new StakingDelegatorNumerator(this._io, this, this._root);
      }
      if (this.alphaOperationMetadataAlphaBalanceAndUpdateTag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.STAKING_DELEGATE_DENOMINATOR) {
        this.stakingDelegateDenominator = new StakingDelegateDenominator(this._io, this, this._root);
      }
    }

    return AlphaOperationMetadataAlphaBalanceAndUpdate;
  })();

  var PublicKeyHash = AlphaReceiptBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == AlphaReceiptBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaReceiptBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaReceiptBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaReceiptBalanceUpdates.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Contract = AlphaReceiptBalanceUpdates.Contract = (function() {
    function Contract(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Contract.prototype._read = function() {
      this.contract = new AlphaContractId(this._io, this, this._root);
      this.alphaOperationMetadataAlphaTezBalanceUpdate = new AlphaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Contract;
  })();

  var StakingDelegatorNumerator = AlphaReceiptBalanceUpdates.StakingDelegatorNumerator = (function() {
    function StakingDelegatorNumerator(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    StakingDelegatorNumerator.prototype._read = function() {
      this.delegator = new AlphaContractId(this._io, this, this._root);
      this.alphaOperationMetadataAlphaStakingAbstractQuantity = new AlphaOperationMetadataAlphaStakingAbstractQuantity(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return StakingDelegatorNumerator;
  })();

  return AlphaReceiptBalanceUpdates;
})();
return AlphaReceiptBalanceUpdates;
}));
