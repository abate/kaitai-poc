// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './P2pPointId', './P2pConnectionId'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./P2pPointId'), require('./P2pConnectionId'));
  } else {
    root.P2pConnectionPoolEvent = factory(root.KaitaiStream, root.P2pPointId, root.P2pConnectionId);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, P2pPointId, P2pConnectionId) {
/**
 * Encoding id: p2p_connection.pool_event
 * Description: An event that may happen during maintenance of and other operations on the p2p connection pool. Typically, it includes connection errors, peer swaps, etc.
 */

var P2pConnectionPoolEvent = (function() {
  P2pConnectionPoolEvent.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  P2pConnectionPoolEvent.P2pConnectionPoolEventTag = Object.freeze({
    TOO_FEW_CONNECTIONS: 0,
    TOO_MANY_CONNECTIONS: 1,
    NEW_POINT: 2,
    NEW_PEER: 3,
    INCOMING_CONNECTION: 4,
    OUTGOING_CONNECTION: 5,
    AUTHENTICATION_FAILED: 6,
    ACCEPTING_REQUEST: 7,
    REJECTING_REQUEST: 8,
    REQUEST_REJECTED: 9,
    CONNECTION_ESTABLISHED: 10,
    DISCONNECTION: 11,
    EXTERNAL_DISCONNECTION: 12,
    GC_POINTS: 13,
    GC_PEER_IDS: 14,
    SWAP_REQUEST_RECEIVED: 15,
    SWAP_ACK_RECEIVED: 16,
    SWAP_REQUEST_SENT: 17,
    SWAP_ACK_SENT: 18,
    SWAP_REQUEST_IGNORED: 19,
    SWAP_SUCCESS: 20,
    SWAP_FAILURE: 21,
    BOOTSTRAP_SENT: 22,
    BOOTSTRAP_RECEIVED: 23,
    ADVERTISE_SENT: 24,
    ADVERTISE_RECEIVED: 25,

    0: "TOO_FEW_CONNECTIONS",
    1: "TOO_MANY_CONNECTIONS",
    2: "NEW_POINT",
    3: "NEW_PEER",
    4: "INCOMING_CONNECTION",
    5: "OUTGOING_CONNECTION",
    6: "AUTHENTICATION_FAILED",
    7: "ACCEPTING_REQUEST",
    8: "REJECTING_REQUEST",
    9: "REQUEST_REJECTED",
    10: "CONNECTION_ESTABLISHED",
    11: "DISCONNECTION",
    12: "EXTERNAL_DISCONNECTION",
    13: "GC_POINTS",
    14: "GC_PEER_IDS",
    15: "SWAP_REQUEST_RECEIVED",
    16: "SWAP_ACK_RECEIVED",
    17: "SWAP_REQUEST_SENT",
    18: "SWAP_ACK_SENT",
    19: "SWAP_REQUEST_IGNORED",
    20: "SWAP_SUCCESS",
    21: "SWAP_FAILURE",
    22: "BOOTSTRAP_SENT",
    23: "BOOTSTRAP_RECEIVED",
    24: "ADVERTISE_SENT",
    25: "ADVERTISE_RECEIVED",
  });

  function P2pConnectionPoolEvent(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  P2pConnectionPoolEvent.prototype._read = function() {
    this.p2pConnectionPoolEventTag = this._io.readU1();
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.NEW_POINT) {
      this.newPoint = new P2pPointId(this._io, this, null);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.NEW_PEER) {
      this.newPeer = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.INCOMING_CONNECTION) {
      this.incomingConnection = new P2pPointId(this._io, this, null);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.OUTGOING_CONNECTION) {
      this.outgoingConnection = new P2pPointId(this._io, this, null);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.AUTHENTICATION_FAILED) {
      this.authenticationFailed = new P2pPointId(this._io, this, null);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.ACCEPTING_REQUEST) {
      this.acceptingRequest = new AcceptingRequest(this._io, this, this._root);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.REJECTING_REQUEST) {
      this.rejectingRequest = new RejectingRequest(this._io, this, this._root);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.REQUEST_REJECTED) {
      this.requestRejected = new RequestRejected(this._io, this, this._root);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.CONNECTION_ESTABLISHED) {
      this.connectionEstablished = new ConnectionEstablished(this._io, this, this._root);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.DISCONNECTION) {
      this.disconnection = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.EXTERNAL_DISCONNECTION) {
      this.externalDisconnection = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.SWAP_REQUEST_RECEIVED) {
      this.swapRequestReceived = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.SWAP_ACK_RECEIVED) {
      this.swapAckReceived = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.SWAP_REQUEST_SENT) {
      this.swapRequestSent = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.SWAP_ACK_SENT) {
      this.swapAckSent = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.SWAP_REQUEST_IGNORED) {
      this.swapRequestIgnored = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.SWAP_SUCCESS) {
      this.swapSuccess = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.SWAP_FAILURE) {
      this.swapFailure = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.BOOTSTRAP_SENT) {
      this.bootstrapSent = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.BOOTSTRAP_RECEIVED) {
      this.bootstrapReceived = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.ADVERTISE_SENT) {
      this.advertiseSent = this._io.readBytes(16);
    }
    if (this.p2pConnectionPoolEventTag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.ADVERTISE_RECEIVED) {
      this.advertiseReceived = this._io.readBytes(16);
    }
  }

  var RequestRejected = P2pConnectionPoolEvent.RequestRejected = (function() {
    function RequestRejected(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RequestRejected.prototype._read = function() {
      this.point = new P2pPointId(this._io, this, null);
      this.identityTag = this._io.readU1();
      if (this.identityTag == P2pConnectionPoolEvent.Bool.TRUE) {
        this.identity = new Identity(this._io, this, this._root);
      }
    }

    return RequestRejected;
  })();

  var RejectingRequest = P2pConnectionPoolEvent.RejectingRequest = (function() {
    function RejectingRequest(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RejectingRequest.prototype._read = function() {
      this.point = new P2pPointId(this._io, this, null);
      this.idPoint = new P2pConnectionId(this._io, this, null);
      this.peerId = this._io.readBytes(16);
    }

    return RejectingRequest;
  })();

  var ConnectionEstablished = P2pConnectionPoolEvent.ConnectionEstablished = (function() {
    function ConnectionEstablished(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ConnectionEstablished.prototype._read = function() {
      this.idPoint = new P2pConnectionId(this._io, this, null);
      this.peerId = this._io.readBytes(16);
    }

    return ConnectionEstablished;
  })();

  var AcceptingRequest = P2pConnectionPoolEvent.AcceptingRequest = (function() {
    function AcceptingRequest(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AcceptingRequest.prototype._read = function() {
      this.point = new P2pPointId(this._io, this, null);
      this.idPoint = new P2pConnectionId(this._io, this, null);
      this.peerId = this._io.readBytes(16);
    }

    return AcceptingRequest;
  })();

  var Identity = P2pConnectionPoolEvent.Identity = (function() {
    function Identity(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Identity.prototype._read = function() {
      this.identityField0 = new P2pConnectionId(this._io, this, null);
      this.identityField1 = this._io.readBytes(16);
    }

    /**
     * crypto_box__public_key_hash
     */

    return Identity;
  })();

  return P2pConnectionPoolEvent;
})();
return P2pConnectionPoolEvent;
}));
