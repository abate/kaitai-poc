// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './OperationShellHeader'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./OperationShellHeader'));
  } else {
    root.Operation = factory(root.KaitaiStream, root.OperationShellHeader);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, OperationShellHeader) {
/**
 * Encoding id: operation
 * Description: An operation. The shell_header part indicates a block an operation is meant to apply on top of. The proto part is protocol-specific and appears as a binary blob.
 */

var Operation = (function() {
  function Operation(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Operation.prototype._read = function() {
    this.operation = new OperationShellHeader(this._io, this, null);
    this.data = this._io.readBytesFull();
  }

  return Operation;
})();
return Operation;
}));
