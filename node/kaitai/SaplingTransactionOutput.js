// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './SaplingTransactionCommitment', './SaplingTransactionCiphertext'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./SaplingTransactionCommitment'), require('./SaplingTransactionCiphertext'));
  } else {
    root.SaplingTransactionOutput = factory(root.KaitaiStream, root.SaplingTransactionCommitment, root.SaplingTransactionCiphertext);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, SaplingTransactionCommitment, SaplingTransactionCiphertext) {
/**
 * Encoding id: sapling.transaction.output
 * Description: Output of a transaction
 */

var SaplingTransactionOutput = (function() {
  function SaplingTransactionOutput(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  SaplingTransactionOutput.prototype._read = function() {
    this.cm = new SaplingTransactionCommitment(this._io, this, null);
    this.proofO = this._io.readBytes(192);
    this.ciphertext = new SaplingTransactionCiphertext(this._io, this, null);
  }

  return SaplingTransactionOutput;
})();
return SaplingTransactionOutput;
}));
