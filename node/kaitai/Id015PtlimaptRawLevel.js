// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id015PtlimaptRawLevel = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 015-PtLimaPt.raw_level
 */

var Id015PtlimaptRawLevel = (function() {
  function Id015PtlimaptRawLevel(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id015PtlimaptRawLevel.prototype._read = function() {
    this.id015PtlimaptRawLevel = this._io.readS4be();
  }

  return Id015PtlimaptRawLevel;
})();
return Id015PtlimaptRawLevel;
}));
