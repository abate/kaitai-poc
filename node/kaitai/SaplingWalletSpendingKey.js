// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.SaplingWalletSpendingKey = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: sapling.wallet.spending_key
 */

var SaplingWalletSpendingKey = (function() {
  function SaplingWalletSpendingKey(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  SaplingWalletSpendingKey.prototype._read = function() {
    this.depth = this._io.readBytes(1);
    this.parentFvkTag = this._io.readBytes(4);
    this.childIndex = this._io.readBytes(4);
    this.chainCode = this._io.readBytes(32);
    this.expsk = new SaplingWalletExpandedSpendingKey(this._io, this, this._root);
    this.dk = this._io.readBytes(32);
  }

  var SaplingWalletExpandedSpendingKey = SaplingWalletSpendingKey.SaplingWalletExpandedSpendingKey = (function() {
    function SaplingWalletExpandedSpendingKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SaplingWalletExpandedSpendingKey.prototype._read = function() {
      this.ask = this._io.readBytes(32);
      this.nsk = this._io.readBytes(32);
      this.ovk = this._io.readBytes(32);
    }

    return SaplingWalletExpandedSpendingKey;
  })();

  return SaplingWalletSpendingKey;
})();
return SaplingWalletSpendingKey;
}));
