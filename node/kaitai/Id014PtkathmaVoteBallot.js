// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id014PtkathmaVoteBallot = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 014-PtKathma.vote.ballot
 */

var Id014PtkathmaVoteBallot = (function() {
  function Id014PtkathmaVoteBallot(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id014PtkathmaVoteBallot.prototype._read = function() {
    this.id014PtkathmaVoteBallot = this._io.readS1();
  }

  return Id014PtkathmaVoteBallot;
})();
return Id014PtkathmaVoteBallot;
}));
