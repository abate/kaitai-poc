// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.OperationShellHeader = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: operation.shell_header
 * Description: An operation's shell header.
 */

var OperationShellHeader = (function() {
  function OperationShellHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  OperationShellHeader.prototype._read = function() {
    this.branch = this._io.readBytes(32);
  }

  return OperationShellHeader;
})();
return OperationShellHeader;
}));
