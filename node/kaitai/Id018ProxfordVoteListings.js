// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id018ProxfordVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 018-Proxford.vote.listings
 */

var Id018ProxfordVoteListings = (function() {
  Id018ProxfordVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function Id018ProxfordVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id018ProxfordVoteListings.prototype._read = function() {
    this.lenId018ProxfordVoteListings = this._io.readU4be();
    if (!(this.lenId018ProxfordVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId018ProxfordVoteListings, this._io, "/seq/0");
    }
    this._raw_id018ProxfordVoteListings = this._io.readBytes(this.lenId018ProxfordVoteListings);
    var _io__raw_id018ProxfordVoteListings = new KaitaiStream(this._raw_id018ProxfordVoteListings);
    this.id018ProxfordVoteListings = new Id018ProxfordVoteListings(_io__raw_id018ProxfordVoteListings, this, this._root);
  }

  var Id018ProxfordVoteListings = Id018ProxfordVoteListings.Id018ProxfordVoteListings = (function() {
    function Id018ProxfordVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordVoteListings.prototype._read = function() {
      this.id018ProxfordVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id018ProxfordVoteListingsEntries.push(new Id018ProxfordVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id018ProxfordVoteListings;
  })();

  var Id018ProxfordVoteListingsEntries = Id018ProxfordVoteListings.Id018ProxfordVoteListingsEntries = (function() {
    function Id018ProxfordVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.votingPower = this._io.readS8be();
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id018ProxfordVoteListingsEntries;
  })();

  var PublicKeyHash = Id018ProxfordVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id018ProxfordVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id018ProxfordVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id018ProxfordVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id018ProxfordVoteListings.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id018ProxfordVoteListings;
})();
return Id018ProxfordVoteListings;
}));
