// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id005Psbabym1VotingPeriodKind = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 005-PsBabyM1.voting_period.kind
 */

var Id005Psbabym1VotingPeriodKind = (function() {
  Id005Psbabym1VotingPeriodKind.Id005Psbabym1VotingPeriodKindTag = Object.freeze({
    PROPOSAL: 0,
    TESTING_VOTE: 1,
    TESTING: 2,
    PROMOTION_VOTE: 3,

    0: "PROPOSAL",
    1: "TESTING_VOTE",
    2: "TESTING",
    3: "PROMOTION_VOTE",
  });

  function Id005Psbabym1VotingPeriodKind(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1VotingPeriodKind.prototype._read = function() {
    this.id005Psbabym1VotingPeriodKindTag = this._io.readU1();
  }

  return Id005Psbabym1VotingPeriodKind;
})();
return Id005Psbabym1VotingPeriodKind;
}));
