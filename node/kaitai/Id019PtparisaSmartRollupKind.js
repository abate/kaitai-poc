// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaSmartRollupKind = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.smart_rollup.kind
 */

var Id019PtparisaSmartRollupKind = (function() {
  Id019PtparisaSmartRollupKind.Id019PtparisaSmartRollupKind = Object.freeze({
    ARITH: 0,
    WASM_2_0_0: 1,
    RISCV: 2,

    0: "ARITH",
    1: "WASM_2_0_0",
    2: "RISCV",
  });

  function Id019PtparisaSmartRollupKind(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaSmartRollupKind.prototype._read = function() {
    this.id019PtparisaSmartRollupKind = this._io.readU1();
  }

  return Id019PtparisaSmartRollupKind;
})();
return Id019PtparisaSmartRollupKind;
}));
