// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id015PtlimaptBlockHeaderProtocolData = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 015-PtLimaPt.block_header.protocol_data
 */

var Id015PtlimaptBlockHeaderProtocolData = (function() {
  Id015PtlimaptBlockHeaderProtocolData.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id015PtlimaptBlockHeaderProtocolData(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id015PtlimaptBlockHeaderProtocolData.prototype._read = function() {
    this.id015PtlimaptBlockHeaderAlphaSignedContents = new Id015PtlimaptBlockHeaderAlphaSignedContents(this._io, this, this._root);
  }

  var Id015PtlimaptBlockHeaderAlphaSignedContents = Id015PtlimaptBlockHeaderProtocolData.Id015PtlimaptBlockHeaderAlphaSignedContents = (function() {
    function Id015PtlimaptBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id015PtlimaptBlockHeaderAlphaUnsignedContents = new Id015PtlimaptBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id015PtlimaptBlockHeaderAlphaSignedContents;
  })();

  var Id015PtlimaptBlockHeaderAlphaUnsignedContents = Id015PtlimaptBlockHeaderProtocolData.Id015PtlimaptBlockHeaderAlphaUnsignedContents = (function() {
    function Id015PtlimaptBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id015PtlimaptBlockHeaderProtocolData.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id015PtlimaptLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id015PtlimaptBlockHeaderAlphaUnsignedContents;
  })();

  var Id015PtlimaptLiquidityBakingToggleVote = Id015PtlimaptBlockHeaderProtocolData.Id015PtlimaptLiquidityBakingToggleVote = (function() {
    function Id015PtlimaptLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptLiquidityBakingToggleVote.prototype._read = function() {
      this.id015PtlimaptLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id015PtlimaptLiquidityBakingToggleVote;
  })();

  return Id015PtlimaptBlockHeaderProtocolData;
})();
return Id015PtlimaptBlockHeaderProtocolData;
}));
