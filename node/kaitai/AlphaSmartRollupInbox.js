// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaSmartRollupInbox = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.smart_rollup.inbox
 */

var AlphaSmartRollupInbox = (function() {
  function AlphaSmartRollupInbox(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaSmartRollupInbox.prototype._read = function() {
    this.level = this._io.readS4be();
    this.oldLevelsMessages = new OldLevelsMessages(this._io, this, this._root);
  }

  var BackPointers0 = AlphaSmartRollupInbox.BackPointers0 = (function() {
    function BackPointers0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BackPointers0.prototype._read = function() {
      this.lenBackPointers = this._io.readU4be();
      if (!(this.lenBackPointers <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBackPointers, this._io, "/types/back_pointers_0/seq/0");
      }
      this._raw_backPointers = this._io.readBytes(this.lenBackPointers);
      var _io__raw_backPointers = new KaitaiStream(this._raw_backPointers);
      this.backPointers = new BackPointers(_io__raw_backPointers, this, this._root);
    }

    return BackPointers0;
  })();

  var N = AlphaSmartRollupInbox.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var OldLevelsMessages = AlphaSmartRollupInbox.OldLevelsMessages = (function() {
    function OldLevelsMessages(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    OldLevelsMessages.prototype._read = function() {
      this.index = new N(this._io, this, this._root);
      this.content = new Content(this._io, this, this._root);
      this.backPointers = new BackPointers0(this._io, this, this._root);
    }

    return OldLevelsMessages;
  })();

  var Content = AlphaSmartRollupInbox.Content = (function() {
    function Content(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Content.prototype._read = function() {
      this.hash = this._io.readBytes(32);
      this.level = this._io.readS4be();
    }

    return Content;
  })();

  var BackPointers = AlphaSmartRollupInbox.BackPointers = (function() {
    function BackPointers(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BackPointers.prototype._read = function() {
      this.backPointersEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.backPointersEntries.push(new BackPointersEntries(this._io, this, this._root));
        i++;
      }
    }

    return BackPointers;
  })();

  var NChunk = AlphaSmartRollupInbox.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var BackPointersEntries = AlphaSmartRollupInbox.BackPointersEntries = (function() {
    function BackPointersEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BackPointersEntries.prototype._read = function() {
      this.smartRollupInboxHash = this._io.readBytes(32);
    }

    return BackPointersEntries;
  })();

  return AlphaSmartRollupInbox;
})();
return AlphaSmartRollupInbox;
}));
