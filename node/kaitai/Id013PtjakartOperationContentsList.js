// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './OperationShellHeader', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./OperationShellHeader'), require('./BlockHeaderShell'));
  } else {
    root.Id013PtjakartOperationContentsList = factory(root.KaitaiStream, root.OperationShellHeader, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, OperationShellHeader, BlockHeaderShell) {
/**
 * Encoding id: 013-PtJakart.operation.contents_list
 */

var Id013PtjakartOperationContentsList = (function() {
  Id013PtjakartOperationContentsList.Id013PtjakartInlinedEndorsementMempoolContentsTag = Object.freeze({
    ENDORSEMENT: 21,

    21: "ENDORSEMENT",
  });

  Id013PtjakartOperationContentsList.Id013PtjakartInlinedPreendorsementContentsTag = Object.freeze({
    PREENDORSEMENT: 20,

    20: "PREENDORSEMENT",
  });

  Id013PtjakartOperationContentsList.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id013PtjakartOperationContentsList.Case131EltField1Tag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,

    0: "CASE__0",
    1: "CASE__1",
  });

  Id013PtjakartOperationContentsList.ProofTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
  });

  Id013PtjakartOperationContentsList.Case129EltField1Tag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,

    0: "CASE__0",
    1: "CASE__1",
  });

  Id013PtjakartOperationContentsList.KindTag = Object.freeze({
    EXAMPLE_ARITH__SMART__CONTRACT__ROLLUP__KIND: 0,

    0: "EXAMPLE_ARITH__SMART__CONTRACT__ROLLUP__KIND",
  });

  Id013PtjakartOperationContentsList.Case3Field3EltTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__7: 7,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,
    CASE__11: 11,
    CASE__12: 12,
    CASE__13: 13,
    CASE__14: 14,
    CASE__15: 15,
    CASE__128: 128,
    CASE__129: 129,
    CASE__130: 130,
    CASE__131: 131,
    CASE__192: 192,
    CASE__193: 193,
    CASE__195: 195,
    CASE__224: 224,
    CASE__225: 225,
    CASE__226: 226,
    CASE__227: 227,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    7: "CASE__7",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
    11: "CASE__11",
    12: "CASE__12",
    13: "CASE__13",
    14: "CASE__14",
    15: "CASE__15",
    128: "CASE__128",
    129: "CASE__129",
    130: "CASE__130",
    131: "CASE__131",
    192: "CASE__192",
    193: "CASE__193",
    195: "CASE__195",
    224: "CASE__224",
    225: "CASE__225",
    226: "CASE__226",
    227: "CASE__227",
  });

  Id013PtjakartOperationContentsList.Case1Field3EltTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__7: 7,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,
    CASE__11: 11,
    CASE__12: 12,
    CASE__13: 13,
    CASE__14: 14,
    CASE__15: 15,
    CASE__128: 128,
    CASE__129: 129,
    CASE__130: 130,
    CASE__131: 131,
    CASE__192: 192,
    CASE__193: 193,
    CASE__195: 195,
    CASE__224: 224,
    CASE__225: 225,
    CASE__226: 226,
    CASE__227: 227,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    7: "CASE__7",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
    11: "CASE__11",
    12: "CASE__12",
    13: "CASE__13",
    14: "CASE__14",
    15: "CASE__15",
    128: "CASE__128",
    129: "CASE__129",
    130: "CASE__130",
    131: "CASE__131",
    192: "CASE__192",
    193: "CASE__193",
    195: "CASE__195",
    224: "CASE__224",
    225: "CASE__225",
    226: "CASE__226",
    227: "CASE__227",
  });

  Id013PtjakartOperationContentsList.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id013PtjakartOperationContentsList.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id013PtjakartOperationContentsList.Id013PtjakartContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id013PtjakartOperationContentsList.Case130EltField1Tag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,

    0: "CASE__0",
    1: "CASE__1",
  });

  Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag = Object.freeze({
    SEED_NONCE_REVELATION: 1,
    DOUBLE_ENDORSEMENT_EVIDENCE: 2,
    DOUBLE_BAKING_EVIDENCE: 3,
    ACTIVATE_ACCOUNT: 4,
    PROPOSALS: 5,
    BALLOT: 6,
    DOUBLE_PREENDORSEMENT_EVIDENCE: 7,
    FAILING_NOOP: 17,
    PREENDORSEMENT: 20,
    ENDORSEMENT: 21,
    REVEAL: 107,
    TRANSACTION: 108,
    ORIGINATION: 109,
    DELEGATION: 110,
    REGISTER_GLOBAL_CONSTANT: 111,
    SET_DEPOSITS_LIMIT: 112,
    TX_ROLLUP_ORIGINATION: 150,
    TX_ROLLUP_SUBMIT_BATCH: 151,
    TX_ROLLUP_COMMIT: 152,
    TX_ROLLUP_RETURN_BOND: 153,
    TX_ROLLUP_FINALIZE_COMMITMENT: 154,
    TX_ROLLUP_REMOVE_COMMITMENT: 155,
    TX_ROLLUP_REJECTION: 156,
    TX_ROLLUP_DISPATCH_TICKETS: 157,
    TRANSFER_TICKET: 158,
    SC_ROLLUP_ORIGINATE: 200,
    SC_ROLLUP_ADD_MESSAGES: 201,
    SC_ROLLUP_CEMENT: 202,
    SC_ROLLUP_PUBLISH: 203,

    1: "SEED_NONCE_REVELATION",
    2: "DOUBLE_ENDORSEMENT_EVIDENCE",
    3: "DOUBLE_BAKING_EVIDENCE",
    4: "ACTIVATE_ACCOUNT",
    5: "PROPOSALS",
    6: "BALLOT",
    7: "DOUBLE_PREENDORSEMENT_EVIDENCE",
    17: "FAILING_NOOP",
    20: "PREENDORSEMENT",
    21: "ENDORSEMENT",
    107: "REVEAL",
    108: "TRANSACTION",
    109: "ORIGINATION",
    110: "DELEGATION",
    111: "REGISTER_GLOBAL_CONSTANT",
    112: "SET_DEPOSITS_LIMIT",
    150: "TX_ROLLUP_ORIGINATION",
    151: "TX_ROLLUP_SUBMIT_BATCH",
    152: "TX_ROLLUP_COMMIT",
    153: "TX_ROLLUP_RETURN_BOND",
    154: "TX_ROLLUP_FINALIZE_COMMITMENT",
    155: "TX_ROLLUP_REMOVE_COMMITMENT",
    156: "TX_ROLLUP_REJECTION",
    157: "TX_ROLLUP_DISPATCH_TICKETS",
    158: "TRANSFER_TICKET",
    200: "SC_ROLLUP_ORIGINATE",
    201: "SC_ROLLUP_ADD_MESSAGES",
    202: "SC_ROLLUP_CEMENT",
    203: "SC_ROLLUP_PUBLISH",
  });

  Id013PtjakartOperationContentsList.Id013PtjakartEntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    255: "NAMED",
  });

  Id013PtjakartOperationContentsList.Id013PtjakartTransactionDestinationTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,
    TX_ROLLUP: 2,

    0: "IMPLICIT",
    1: "ORIGINATED",
    2: "TX_ROLLUP",
  });

  Id013PtjakartOperationContentsList.PredecessorTag = Object.freeze({
    NONE: 0,
    SOME: 1,

    0: "NONE",
    1: "SOME",
  });

  Id013PtjakartOperationContentsList.AmountTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
  });

  Id013PtjakartOperationContentsList.Case2Field3EltTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__7: 7,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,
    CASE__11: 11,
    CASE__12: 12,
    CASE__13: 13,
    CASE__14: 14,
    CASE__15: 15,
    CASE__128: 128,
    CASE__129: 129,
    CASE__130: 130,
    CASE__131: 131,
    CASE__192: 192,
    CASE__193: 193,
    CASE__195: 195,
    CASE__224: 224,
    CASE__225: 225,
    CASE__226: 226,
    CASE__227: 227,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    7: "CASE__7",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
    11: "CASE__11",
    12: "CASE__12",
    13: "CASE__13",
    14: "CASE__14",
    15: "CASE__15",
    128: "CASE__128",
    129: "CASE__129",
    130: "CASE__130",
    131: "CASE__131",
    192: "CASE__192",
    193: "CASE__193",
    195: "CASE__195",
    224: "CASE__224",
    225: "CASE__225",
    226: "CASE__226",
    227: "CASE__227",
  });

  Id013PtjakartOperationContentsList.MessageTag = Object.freeze({
    BATCH: 0,
    DEPOSIT: 1,

    0: "BATCH",
    1: "DEPOSIT",
  });

  Id013PtjakartOperationContentsList.Case0Field3EltTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__7: 7,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,
    CASE__11: 11,
    CASE__12: 12,
    CASE__13: 13,
    CASE__14: 14,
    CASE__15: 15,
    CASE__128: 128,
    CASE__129: 129,
    CASE__130: 130,
    CASE__131: 131,
    CASE__192: 192,
    CASE__193: 193,
    CASE__195: 195,
    CASE__224: 224,
    CASE__225: 225,
    CASE__226: 226,
    CASE__227: 227,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    7: "CASE__7",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
    11: "CASE__11",
    12: "CASE__12",
    13: "CASE__13",
    14: "CASE__14",
    15: "CASE__15",
    128: "CASE__128",
    129: "CASE__129",
    130: "CASE__130",
    131: "CASE__131",
    192: "CASE__192",
    193: "CASE__193",
    195: "CASE__195",
    224: "CASE__224",
    225: "CASE__225",
    226: "CASE__226",
    227: "CASE__227",
  });

  function Id013PtjakartOperationContentsList(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id013PtjakartOperationContentsList.prototype._read = function() {
    this.id013PtjakartOperationContentsListEntries = [];
    var i = 0;
    while (!this._io.isEof()) {
      this.id013PtjakartOperationContentsListEntries.push(new Id013PtjakartOperationContentsListEntries(this._io, this, this._root));
      i++;
    }
  }

  var Case131EltField00 = Id013PtjakartOperationContentsList.Case131EltField00 = (function() {
    function Case131EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131EltField00.prototype._read = function() {
      this.lenCase131EltField0 = this._io.readU1();
      if (!(this.lenCase131EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase131EltField0, this._io, "/types/case__131_elt_field0_0/seq/0");
      }
      this._raw_case131EltField0 = this._io.readBytes(this.lenCase131EltField0);
      var _io__raw_case131EltField0 = new KaitaiStream(this._raw_case131EltField0);
      this.case131EltField0 = new Case131EltField0(_io__raw_case131EltField0, this, this._root);
    }

    return Case131EltField00;
  })();

  var Op20 = Id013PtjakartOperationContentsList.Op20 = (function() {
    function Op20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op20.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_0/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op2(_io__raw_op2, this, this._root);
    }

    return Op20;
  })();

  var Case1 = Id013PtjakartOperationContentsList.Case1 = (function() {
    function Case1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1.prototype._read = function() {
      this.case1Field0 = this._io.readS2be();
      this.case1Field1 = this._io.readBytes(32);
      this.case1Field2 = this._io.readBytes(32);
      this.case1Field3 = new Case1Field30(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case1;
  })();

  var Case192 = Id013PtjakartOperationContentsList.Case192 = (function() {
    function Case192(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case192.prototype._read = function() {
      this.case192 = this._io.readBytesFull();
    }

    return Case192;
  })();

  var Messages = Id013PtjakartOperationContentsList.Messages = (function() {
    function Messages(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Messages.prototype._read = function() {
      this.messagesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messagesEntries.push(new MessagesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Messages;
  })();

  var MessageResultPathEntries = Id013PtjakartOperationContentsList.MessageResultPathEntries = (function() {
    function MessageResultPathEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageResultPathEntries.prototype._read = function() {
      this.messageResultListHash = this._io.readBytes(32);
    }

    return MessageResultPathEntries;
  })();

  var ScRollupPublish = Id013PtjakartOperationContentsList.ScRollupPublish = (function() {
    function ScRollupPublish(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupPublish.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id013PtjakartRollupAddress(this._io, this, this._root);
      this.commitment = new Commitment0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupPublish;
  })();

  var Case1933 = Id013PtjakartOperationContentsList.Case1933 = (function() {
    function Case1933(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1933.prototype._read = function() {
      this.lenCase193 = this._io.readU2be();
      if (!(this.lenCase193 <= 65535)) {
        throw new KaitaiStream.ValidationGreaterThanError(65535, this.lenCase193, this._io, "/types/case__193_3/seq/0");
      }
      this._raw_case193 = this._io.readBytes(this.lenCase193);
      var _io__raw_case193 = new KaitaiStream(this._raw_case193);
      this.case193 = new Case193(_io__raw_case193, this, this._root);
    }

    return Case1933;
  })();

  var Case8 = Id013PtjakartOperationContentsList.Case8 = (function() {
    function Case8(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case8.prototype._read = function() {
      this.case8Field0 = this._io.readU1();
      this.case8Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__8_field1_field1
     */

    return Case8;
  })();

  var Case10 = Id013PtjakartOperationContentsList.Case10 = (function() {
    function Case10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case10.prototype._read = function() {
      this.case10Field0 = this._io.readS4be();
      this.case10Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__10_field1_field1
     */

    return Case10;
  })();

  var Case2Field3Entries = Id013PtjakartOperationContentsList.Case2Field3Entries = (function() {
    function Case2Field3Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case2Field3Entries.prototype._read = function() {
      this.case2Field3EltTag = this._io.readU1();
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__8) {
        this.case8 = new Case8(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__4) {
        this.case4 = new Case4(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__12) {
        this.case12 = new Case12(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__9) {
        this.case9 = new Case9(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__5) {
        this.case5 = new Case5(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__13) {
        this.case13 = new Case13(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__10) {
        this.case10 = new Case10(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__6) {
        this.case6 = new Case6(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__14) {
        this.case14 = new Case14(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__11) {
        this.case11 = new Case11(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__7) {
        this.case7 = new Case7(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__15) {
        this.case15 = new Case15(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__129) {
        this.case129 = new Case129Entries(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__130) {
        this.case130 = new Case130Entries(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__131) {
        this.case131 = new Case1311(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__192) {
        this.case192 = new Case1921(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__193) {
        this.case193 = new Case1931(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__195) {
        this.case195 = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__224) {
        this.case224 = new Case224(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__225) {
        this.case225 = new Case225(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__226) {
        this.case226 = new Case226(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id013PtjakartOperationContentsList.Case2Field3EltTag.CASE__227) {
        this.case227 = new Case227(this._io, this, this._root);
      }
    }

    return Case2Field3Entries;
  })();

  var Case3Field30 = Id013PtjakartOperationContentsList.Case3Field30 = (function() {
    function Case3Field30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case3Field30.prototype._read = function() {
      this.lenCase3Field3 = this._io.readU4be();
      if (!(this.lenCase3Field3 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase3Field3, this._io, "/types/case__3_field3_0/seq/0");
      }
      this._raw_case3Field3 = this._io.readBytes(this.lenCase3Field3);
      var _io__raw_case3Field3 = new KaitaiStream(this._raw_case3Field3);
      this.case3Field3 = new Case3Field3(_io__raw_case3Field3, this, this._root);
    }

    return Case3Field30;
  })();

  var ActivateAccount = Id013PtjakartOperationContentsList.ActivateAccount = (function() {
    function ActivateAccount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ActivateAccount.prototype._read = function() {
      this.pkh = this._io.readBytes(20);
      this.secret = this._io.readBytes(20);
    }

    return ActivateAccount;
  })();

  var Case12Field1 = Id013PtjakartOperationContentsList.Case12Field1 = (function() {
    function Case12Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case12Field1.prototype._read = function() {
      this.case12Field1Field0 = this._io.readBytes(32);
      this.case12Field1Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case12Field1;
  })();

  var DoubleEndorsementEvidence = Id013PtjakartOperationContentsList.DoubleEndorsementEvidence = (function() {
    function DoubleEndorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleEndorsementEvidence.prototype._read = function() {
      this.op1 = new Op10(this._io, this, this._root);
      this.op2 = new Op20(this._io, this, this._root);
    }

    return DoubleEndorsementEvidence;
  })();

  var Id013PtjakartBlockHeaderAlphaUnsignedContents = Id013PtjakartOperationContentsList.Id013PtjakartBlockHeaderAlphaUnsignedContents = (function() {
    function Id013PtjakartBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id013PtjakartOperationContentsList.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id013PtjakartLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id013PtjakartBlockHeaderAlphaUnsignedContents;
  })();

  var Case226 = Id013PtjakartOperationContentsList.Case226 = (function() {
    function Case226(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case226.prototype._read = function() {
      this.case226Field0 = this._io.readS4be();
      this.case226Field1 = new Case226Field10(this._io, this, this._root);
      this.case226Field2 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    return Case226;
  })();

  var Case9 = Id013PtjakartOperationContentsList.Case9 = (function() {
    function Case9(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case9.prototype._read = function() {
      this.case9Field0 = this._io.readU2be();
      this.case9Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__9_field1_field1
     */

    return Case9;
  })();

  var Case13 = Id013PtjakartOperationContentsList.Case13 = (function() {
    function Case13(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case13.prototype._read = function() {
      this.case13Field0 = this._io.readU2be();
      this.case13Field1 = new Case13Field1(this._io, this, this._root);
    }

    return Case13;
  })();

  var Originated = Id013PtjakartOperationContentsList.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Case226Field1 = Id013PtjakartOperationContentsList.Case226Field1 = (function() {
    function Case226Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case226Field1.prototype._read = function() {
      this.case226Field1 = this._io.readBytesFull();
    }

    return Case226Field1;
  })();

  var N = Id013PtjakartOperationContentsList.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Case131EltField1 = Id013PtjakartOperationContentsList.Case131EltField1 = (function() {
    function Case131EltField1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131EltField1.prototype._read = function() {
      this.case131EltField1Tag = this._io.readU1();
      if (this.case131EltField1Tag == Id013PtjakartOperationContentsList.Case131EltField1Tag.CASE__0) {
        this.case0 = this._io.readBytes(32);
      }
      if (this.case131EltField1Tag == Id013PtjakartOperationContentsList.Case131EltField1Tag.CASE__1) {
        this.case1 = this._io.readBytes(32);
      }
    }

    return Case131EltField1;
  })();

  var Id013PtjakartEntrypoint = Id013PtjakartOperationContentsList.Id013PtjakartEntrypoint = (function() {
    function Id013PtjakartEntrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartEntrypoint.prototype._read = function() {
      this.id013PtjakartEntrypointTag = this._io.readU1();
      if (this.id013PtjakartEntrypointTag == Id013PtjakartOperationContentsList.Id013PtjakartEntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id013PtjakartEntrypoint;
  })();

  var Case227 = Id013PtjakartOperationContentsList.Case227 = (function() {
    function Case227(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case227.prototype._read = function() {
      this.case227Field0 = this._io.readS8be();
      this.case227Field1 = new Case227Field10(this._io, this, this._root);
      this.case227Field2 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    return Case227;
  })();

  var Case4 = Id013PtjakartOperationContentsList.Case4 = (function() {
    function Case4(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case4.prototype._read = function() {
      this.case4Field0 = this._io.readU1();
      this.case4Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__4_field1_field0
     */

    return Case4;
  })();

  var Endorsement = Id013PtjakartOperationContentsList.Endorsement = (function() {
    function Endorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Endorsement.prototype._read = function() {
      this.slot = this._io.readU2be();
      this.level = this._io.readS4be();
      this.round = this._io.readS4be();
      this.blockPayloadHash = this._io.readBytes(32);
    }

    return Endorsement;
  })();

  var Case130Entries = Id013PtjakartOperationContentsList.Case130Entries = (function() {
    function Case130Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130Entries.prototype._read = function() {
      this.case130EltField0 = new Case130EltField00(this._io, this, this._root);
      this.case130EltField1 = new Case130EltField1(this._io, this, this._root);
    }

    return Case130Entries;
  })();

  var Id013PtjakartTxRollupId = Id013PtjakartOperationContentsList.Id013PtjakartTxRollupId = (function() {
    function Id013PtjakartTxRollupId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartTxRollupId.prototype._read = function() {
      this.rollupHash = this._io.readBytes(20);
    }

    return Id013PtjakartTxRollupId;
  })();

  var Proposals0 = Id013PtjakartOperationContentsList.Proposals0 = (function() {
    function Proposals0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals0.prototype._read = function() {
      this.lenProposals = this._io.readU4be();
      if (!(this.lenProposals <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenProposals, this._io, "/types/proposals_0/seq/0");
      }
      this._raw_proposals = this._io.readBytes(this.lenProposals);
      var _io__raw_proposals = new KaitaiStream(this._raw_proposals);
      this.proposals = new Proposals(_io__raw_proposals, this, this._root);
    }

    return Proposals0;
  })();

  var MessagesEntries = Id013PtjakartOperationContentsList.MessagesEntries = (function() {
    function MessagesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagesEntries.prototype._read = function() {
      this.messageResultHash = this._io.readBytes(32);
    }

    return MessagesEntries;
  })();

  var Case13Field1 = Id013PtjakartOperationContentsList.Case13Field1 = (function() {
    function Case13Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case13Field1.prototype._read = function() {
      this.case13Field1Field0 = this._io.readBytes(32);
      this.case13Field1Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case13Field1;
  })();

  var TransferTicket = Id013PtjakartOperationContentsList.TransferTicket = (function() {
    function TransferTicket(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TransferTicket.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.ticketContents = new BytesDynUint30(this._io, this, this._root);
      this.ticketTy = new BytesDynUint30(this._io, this, this._root);
      this.ticketTicketer = new Id013PtjakartContractId(this._io, this, this._root);
      this.ticketAmount = new N(this._io, this, this._root);
      this.destination = new Id013PtjakartContractId(this._io, this, this._root);
      this.entrypoint = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return TransferTicket;
  })();

  var Case129EltField0 = Id013PtjakartOperationContentsList.Case129EltField0 = (function() {
    function Case129EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129EltField0.prototype._read = function() {
      this.case129EltField0 = this._io.readBytesFull();
    }

    return Case129EltField0;
  })();

  var Case1310 = Id013PtjakartOperationContentsList.Case1310 = (function() {
    function Case1310(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1310.prototype._read = function() {
      this.lenCase131 = this._io.readU4be();
      if (!(this.lenCase131 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase131, this._io, "/types/case__131_0/seq/0");
      }
      this._raw_case131 = this._io.readBytes(this.lenCase131);
      var _io__raw_case131 = new KaitaiStream(this._raw_case131);
      this.case131 = new Case131(_io__raw_case131, this, this._root);
    }

    return Case1310;
  })();

  var Case15Field1 = Id013PtjakartOperationContentsList.Case15Field1 = (function() {
    function Case15Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case15Field1.prototype._read = function() {
      this.case15Field1Field0 = this._io.readBytes(32);
      this.case15Field1Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case15Field1;
  })();

  var Op12 = Id013PtjakartOperationContentsList.Op12 = (function() {
    function Op12(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op12.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_2/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op11(_io__raw_op1, this, this._root);
    }

    return Op12;
  })();

  var TxRollupRemoveCommitment = Id013PtjakartOperationContentsList.TxRollupRemoveCommitment = (function() {
    function TxRollupRemoveCommitment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupRemoveCommitment.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id013PtjakartTxRollupId(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupRemoveCommitment;
  })();

  var Case0 = Id013PtjakartOperationContentsList.Case0 = (function() {
    function Case0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case0.prototype._read = function() {
      this.case0Field0 = this._io.readS2be();
      this.case0Field1 = this._io.readBytes(32);
      this.case0Field2 = this._io.readBytes(32);
      this.case0Field3 = new Case0Field30(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case0;
  })();

  var Case1921 = Id013PtjakartOperationContentsList.Case1921 = (function() {
    function Case1921(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1921.prototype._read = function() {
      this.lenCase192 = this._io.readU1();
      if (!(this.lenCase192 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase192, this._io, "/types/case__192_1/seq/0");
      }
      this._raw_case192 = this._io.readBytes(this.lenCase192);
      var _io__raw_case192 = new KaitaiStream(this._raw_case192);
      this.case192 = new Case192(_io__raw_case192, this, this._root);
    }

    return Case1921;
  })();

  var Id013PtjakartBlockHeaderAlphaSignedContents = Id013PtjakartOperationContentsList.Id013PtjakartBlockHeaderAlphaSignedContents = (function() {
    function Id013PtjakartBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id013PtjakartBlockHeaderAlphaUnsignedContents = new Id013PtjakartBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id013PtjakartBlockHeaderAlphaSignedContents;
  })();

  var Reveal = Id013PtjakartOperationContentsList.Reveal = (function() {
    function Reveal(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Reveal.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.publicKey = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Reveal;
  })();

  var Id013PtjakartInlinedEndorsement = Id013PtjakartOperationContentsList.Id013PtjakartInlinedEndorsement = (function() {
    function Id013PtjakartInlinedEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartInlinedEndorsement.prototype._read = function() {
      this.id013PtjakartInlinedEndorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id013PtjakartInlinedEndorsementMempoolContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id013PtjakartOperationContentsList.Bool.TRUE) {
        this.signature = this._io.readBytes(64);
      }
    }

    return Id013PtjakartInlinedEndorsement;
  })();

  var Case1922 = Id013PtjakartOperationContentsList.Case1922 = (function() {
    function Case1922(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1922.prototype._read = function() {
      this.lenCase192 = this._io.readU1();
      if (!(this.lenCase192 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase192, this._io, "/types/case__192_2/seq/0");
      }
      this._raw_case192 = this._io.readBytes(this.lenCase192);
      var _io__raw_case192 = new KaitaiStream(this._raw_case192);
      this.case192 = new Case192(_io__raw_case192, this, this._root);
    }

    return Case1922;
  })();

  var MessageEntries = Id013PtjakartOperationContentsList.MessageEntries = (function() {
    function MessageEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageEntries.prototype._read = function() {
      this.messageElt = new BytesDynUint30(this._io, this, this._root);
    }

    return MessageEntries;
  })();

  var Id013PtjakartContractId = Id013PtjakartOperationContentsList.Id013PtjakartContractId = (function() {
    function Id013PtjakartContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartContractId.prototype._read = function() {
      this.id013PtjakartContractIdTag = this._io.readU1();
      if (this.id013PtjakartContractIdTag == Id013PtjakartOperationContentsList.Id013PtjakartContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id013PtjakartContractIdTag == Id013PtjakartOperationContentsList.Id013PtjakartContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id013PtjakartContractId;
  })();

  var Case5 = Id013PtjakartOperationContentsList.Case5 = (function() {
    function Case5(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case5.prototype._read = function() {
      this.case5Field0 = this._io.readU2be();
      this.case5Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__5_field1_field0
     */

    return Case5;
  })();

  var Case193 = Id013PtjakartOperationContentsList.Case193 = (function() {
    function Case193(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case193.prototype._read = function() {
      this.case193 = this._io.readBytesFull();
    }

    return Case193;
  })();

  var ScRollupOriginate = Id013PtjakartOperationContentsList.ScRollupOriginate = (function() {
    function ScRollupOriginate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupOriginate.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.kind = this._io.readU2be();
      this.bootSector = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return ScRollupOriginate;
  })();

  var SeedNonceRevelation = Id013PtjakartOperationContentsList.SeedNonceRevelation = (function() {
    function SeedNonceRevelation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SeedNonceRevelation.prototype._read = function() {
      this.level = this._io.readS4be();
      this.nonce = this._io.readBytes(32);
    }

    return SeedNonceRevelation;
  })();

  var Case3Field3 = Id013PtjakartOperationContentsList.Case3Field3 = (function() {
    function Case3Field3(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case3Field3.prototype._read = function() {
      this.case3Field3Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case3Field3Entries.push(new Case3Field3Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case3Field3;
  })();

  var Case225Field10 = Id013PtjakartOperationContentsList.Case225Field10 = (function() {
    function Case225Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case225Field10.prototype._read = function() {
      this.lenCase225Field1 = this._io.readU1();
      if (!(this.lenCase225Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase225Field1, this._io, "/types/case__225_field1_0/seq/0");
      }
      this._raw_case225Field1 = this._io.readBytes(this.lenCase225Field1);
      var _io__raw_case225Field1 = new KaitaiStream(this._raw_case225Field1);
      this.case225Field1 = new Case225Field1(_io__raw_case225Field1, this, this._root);
    }

    return Case225Field10;
  })();

  var Id013PtjakartTransactionDestination = Id013PtjakartOperationContentsList.Id013PtjakartTransactionDestination = (function() {
    function Id013PtjakartTransactionDestination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartTransactionDestination.prototype._read = function() {
      this.id013PtjakartTransactionDestinationTag = this._io.readU1();
      if (this.id013PtjakartTransactionDestinationTag == Id013PtjakartOperationContentsList.Id013PtjakartTransactionDestinationTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id013PtjakartTransactionDestinationTag == Id013PtjakartOperationContentsList.Id013PtjakartTransactionDestinationTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
      if (this.id013PtjakartTransactionDestinationTag == Id013PtjakartOperationContentsList.Id013PtjakartTransactionDestinationTag.TX_ROLLUP) {
        this.txRollup = new TxRollup(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id013PtjakartTransactionDestination;
  })();

  var Preendorsement = Id013PtjakartOperationContentsList.Preendorsement = (function() {
    function Preendorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Preendorsement.prototype._read = function() {
      this.slot = this._io.readU2be();
      this.level = this._io.readS4be();
      this.round = this._io.readS4be();
      this.blockPayloadHash = this._io.readBytes(32);
    }

    return Preendorsement;
  })();

  var Case11 = Id013PtjakartOperationContentsList.Case11 = (function() {
    function Case11(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case11.prototype._read = function() {
      this.case11Field0 = this._io.readS8be();
      this.case11Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__11_field1_field1
     */

    return Case11;
  })();

  var Id013PtjakartOperationAlphaContents = Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContents = (function() {
    function Id013PtjakartOperationAlphaContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartOperationAlphaContents.prototype._read = function() {
      this.id013PtjakartOperationAlphaContentsTag = this._io.readU1();
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.ENDORSEMENT) {
        this.endorsement = new Endorsement(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.PREENDORSEMENT) {
        this.preendorsement = new Preendorsement(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.SEED_NONCE_REVELATION) {
        this.seedNonceRevelation = new SeedNonceRevelation(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.DOUBLE_ENDORSEMENT_EVIDENCE) {
        this.doubleEndorsementEvidence = new DoubleEndorsementEvidence(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.DOUBLE_PREENDORSEMENT_EVIDENCE) {
        this.doublePreendorsementEvidence = new DoublePreendorsementEvidence(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.DOUBLE_BAKING_EVIDENCE) {
        this.doubleBakingEvidence = new DoubleBakingEvidence(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.ACTIVATE_ACCOUNT) {
        this.activateAccount = new ActivateAccount(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.PROPOSALS) {
        this.proposals = new Proposals1(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.BALLOT) {
        this.ballot = new Ballot(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.REVEAL) {
        this.reveal = new Reveal(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.SET_DEPOSITS_LIMIT) {
        this.setDepositsLimit = new SetDepositsLimit(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.FAILING_NOOP) {
        this.failingNoop = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.REGISTER_GLOBAL_CONSTANT) {
        this.registerGlobalConstant = new RegisterGlobalConstant(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.TX_ROLLUP_ORIGINATION) {
        this.txRollupOrigination = new TxRollupOrigination(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.TX_ROLLUP_SUBMIT_BATCH) {
        this.txRollupSubmitBatch = new TxRollupSubmitBatch(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.TX_ROLLUP_COMMIT) {
        this.txRollupCommit = new TxRollupCommit(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.TX_ROLLUP_RETURN_BOND) {
        this.txRollupReturnBond = new TxRollupReturnBond(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.TX_ROLLUP_FINALIZE_COMMITMENT) {
        this.txRollupFinalizeCommitment = new TxRollupFinalizeCommitment(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.TX_ROLLUP_REMOVE_COMMITMENT) {
        this.txRollupRemoveCommitment = new TxRollupRemoveCommitment(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.TX_ROLLUP_REJECTION) {
        this.txRollupRejection = new TxRollupRejection(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.TX_ROLLUP_DISPATCH_TICKETS) {
        this.txRollupDispatchTickets = new TxRollupDispatchTickets(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.TRANSFER_TICKET) {
        this.transferTicket = new TransferTicket(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.SC_ROLLUP_ORIGINATE) {
        this.scRollupOriginate = new ScRollupOriginate(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.SC_ROLLUP_ADD_MESSAGES) {
        this.scRollupAddMessages = new ScRollupAddMessages(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.SC_ROLLUP_CEMENT) {
        this.scRollupCement = new ScRollupCement(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationAlphaContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartOperationAlphaContentsTag.SC_ROLLUP_PUBLISH) {
        this.scRollupPublish = new ScRollupPublish(this._io, this, this._root);
      }
    }

    return Id013PtjakartOperationAlphaContents;
  })();

  var TxRollupCommit = Id013PtjakartOperationContentsList.TxRollupCommit = (function() {
    function TxRollupCommit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupCommit.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id013PtjakartTxRollupId(this._io, this, this._root);
      this.commitment = new Commitment(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupCommit;
  })();

  var DoubleBakingEvidence = Id013PtjakartOperationContentsList.DoubleBakingEvidence = (function() {
    function DoubleBakingEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleBakingEvidence.prototype._read = function() {
      this.bh1 = new Bh10(this._io, this, this._root);
      this.bh2 = new Bh20(this._io, this, this._root);
    }

    return DoubleBakingEvidence;
  })();

  var Id013PtjakartScriptedContracts = Id013PtjakartOperationContentsList.Id013PtjakartScriptedContracts = (function() {
    function Id013PtjakartScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id013PtjakartScriptedContracts;
  })();

  var Case225Field1 = Id013PtjakartOperationContentsList.Case225Field1 = (function() {
    function Case225Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case225Field1.prototype._read = function() {
      this.case225Field1 = this._io.readBytesFull();
    }

    return Case225Field1;
  })();

  var Case129EltField00 = Id013PtjakartOperationContentsList.Case129EltField00 = (function() {
    function Case129EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129EltField00.prototype._read = function() {
      this.lenCase129EltField0 = this._io.readU1();
      if (!(this.lenCase129EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase129EltField0, this._io, "/types/case__129_elt_field0_0/seq/0");
      }
      this._raw_case129EltField0 = this._io.readBytes(this.lenCase129EltField0);
      var _io__raw_case129EltField0 = new KaitaiStream(this._raw_case129EltField0);
      this.case129EltField0 = new Case129EltField0(_io__raw_case129EltField0, this, this._root);
    }

    return Case129EltField00;
  })();

  var Case3Field3Entries = Id013PtjakartOperationContentsList.Case3Field3Entries = (function() {
    function Case3Field3Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case3Field3Entries.prototype._read = function() {
      this.case3Field3EltTag = this._io.readU1();
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__8) {
        this.case8 = new Case8(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__4) {
        this.case4 = new Case4(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__12) {
        this.case12 = new Case12(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__9) {
        this.case9 = new Case9(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__5) {
        this.case5 = new Case5(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__13) {
        this.case13 = new Case13(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__10) {
        this.case10 = new Case10(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__6) {
        this.case6 = new Case6(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__14) {
        this.case14 = new Case14(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__11) {
        this.case11 = new Case11(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__7) {
        this.case7 = new Case7(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__15) {
        this.case15 = new Case15(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__129) {
        this.case129 = new Case129Entries(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__130) {
        this.case130 = new Case130Entries(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__131) {
        this.case131 = new Case1313(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__192) {
        this.case192 = new Case1923(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__193) {
        this.case193 = new Case1933(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__195) {
        this.case195 = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__224) {
        this.case224 = new Case224(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__225) {
        this.case225 = new Case225(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__226) {
        this.case226 = new Case226(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id013PtjakartOperationContentsList.Case3Field3EltTag.CASE__227) {
        this.case227 = new Case227(this._io, this, this._root);
      }
    }

    return Case3Field3Entries;
  })();

  var PublicKey = Id013PtjakartOperationContentsList.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id013PtjakartOperationContentsList.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id013PtjakartOperationContentsList.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id013PtjakartOperationContentsList.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var Case12 = Id013PtjakartOperationContentsList.Case12 = (function() {
    function Case12(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case12.prototype._read = function() {
      this.case12Field0 = this._io.readU1();
      this.case12Field1 = new Case12Field1(this._io, this, this._root);
    }

    return Case12;
  })();

  var TxRollupDispatchTickets = Id013PtjakartOperationContentsList.TxRollupDispatchTickets = (function() {
    function TxRollupDispatchTickets(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupDispatchTickets.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.txRollup = new Id013PtjakartTxRollupId(this._io, this, this._root);
      this.level = this._io.readS4be();
      this.contextHash = this._io.readBytes(32);
      this.messageIndex = new Int31(this._io, this, this._root);
      this.messageResultPath = new MessageResultPath0(this._io, this, this._root);
      this.ticketsInfo = new TicketsInfo0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupDispatchTickets;
  })();

  var Bh2 = Id013PtjakartOperationContentsList.Bh2 = (function() {
    function Bh2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh2.prototype._read = function() {
      this.id013PtjakartBlockHeaderAlphaFullHeader = new Id013PtjakartBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh2;
  })();

  var PreviousMessageResultPathEntries = Id013PtjakartOperationContentsList.PreviousMessageResultPathEntries = (function() {
    function PreviousMessageResultPathEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResultPathEntries.prototype._read = function() {
      this.messageResultListHash = this._io.readBytes(32);
    }

    return PreviousMessageResultPathEntries;
  })();

  var Case2Field30 = Id013PtjakartOperationContentsList.Case2Field30 = (function() {
    function Case2Field30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case2Field30.prototype._read = function() {
      this.lenCase2Field3 = this._io.readU4be();
      if (!(this.lenCase2Field3 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase2Field3, this._io, "/types/case__2_field3_0/seq/0");
      }
      this._raw_case2Field3 = this._io.readBytes(this.lenCase2Field3);
      var _io__raw_case2Field3 = new KaitaiStream(this._raw_case2Field3);
      this.case2Field3 = new Case2Field3(_io__raw_case2Field3, this, this._root);
    }

    return Case2Field30;
  })();

  var ScRollupCement = Id013PtjakartOperationContentsList.ScRollupCement = (function() {
    function ScRollupCement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupCement.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id013PtjakartRollupAddress(this._io, this, this._root);
      this.commitment = this._io.readBytes(32);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupCement;
  })();

  var Named = Id013PtjakartOperationContentsList.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Amount = Id013PtjakartOperationContentsList.Amount = (function() {
    function Amount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Amount.prototype._read = function() {
      this.amountTag = this._io.readU1();
      if (this.amountTag == Id013PtjakartOperationContentsList.AmountTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.amountTag == Id013PtjakartOperationContentsList.AmountTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.amountTag == Id013PtjakartOperationContentsList.AmountTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.amountTag == Id013PtjakartOperationContentsList.AmountTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
    }

    return Amount;
  })();

  var Case1311 = Id013PtjakartOperationContentsList.Case1311 = (function() {
    function Case1311(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1311.prototype._read = function() {
      this.lenCase131 = this._io.readU4be();
      if (!(this.lenCase131 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase131, this._io, "/types/case__131_1/seq/0");
      }
      this._raw_case131 = this._io.readBytes(this.lenCase131);
      var _io__raw_case131 = new KaitaiStream(this._raw_case131);
      this.case131 = new Case131(_io__raw_case131, this, this._root);
    }

    return Case1311;
  })();

  var Case129EltField1 = Id013PtjakartOperationContentsList.Case129EltField1 = (function() {
    function Case129EltField1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129EltField1.prototype._read = function() {
      this.case129EltField1Tag = this._io.readU1();
      if (this.case129EltField1Tag == Id013PtjakartOperationContentsList.Case129EltField1Tag.CASE__0) {
        this.case0 = this._io.readBytes(32);
      }
      if (this.case129EltField1Tag == Id013PtjakartOperationContentsList.Case129EltField1Tag.CASE__1) {
        this.case1 = this._io.readBytes(32);
      }
    }

    return Case129EltField1;
  })();

  var Case131 = Id013PtjakartOperationContentsList.Case131 = (function() {
    function Case131(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131.prototype._read = function() {
      this.case131Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case131Entries.push(new Case131Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case131;
  })();

  var Bh20 = Id013PtjakartOperationContentsList.Bh20 = (function() {
    function Bh20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh20.prototype._read = function() {
      this.lenBh2 = this._io.readU4be();
      if (!(this.lenBh2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh2, this._io, "/types/bh2_0/seq/0");
      }
      this._raw_bh2 = this._io.readBytes(this.lenBh2);
      var _io__raw_bh2 = new KaitaiStream(this._raw_bh2);
      this.bh2 = new Bh2(_io__raw_bh2, this, this._root);
    }

    return Bh20;
  })();

  var Commitment = Id013PtjakartOperationContentsList.Commitment = (function() {
    function Commitment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitment.prototype._read = function() {
      this.level = this._io.readS4be();
      this.messages = new Messages0(this._io, this, this._root);
      this.predecessor = new Predecessor(this._io, this, this._root);
      this.inboxMerkleRoot = this._io.readBytes(32);
    }

    return Commitment;
  })();

  var Case2 = Id013PtjakartOperationContentsList.Case2 = (function() {
    function Case2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case2.prototype._read = function() {
      this.case2Field0 = this._io.readS2be();
      this.case2Field1 = this._io.readBytes(32);
      this.case2Field2 = this._io.readBytes(32);
      this.case2Field3 = new Case2Field30(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case2;
  })();

  var TicketsInfoEntries = Id013PtjakartOperationContentsList.TicketsInfoEntries = (function() {
    function TicketsInfoEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TicketsInfoEntries.prototype._read = function() {
      this.contents = new BytesDynUint30(this._io, this, this._root);
      this.ty = new BytesDynUint30(this._io, this, this._root);
      this.ticketer = new Id013PtjakartContractId(this._io, this, this._root);
      this.amount = new Amount(this._io, this, this._root);
      this.claimer = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return TicketsInfoEntries;
  })();

  var Id013PtjakartInlinedPreendorsement = Id013PtjakartOperationContentsList.Id013PtjakartInlinedPreendorsement = (function() {
    function Id013PtjakartInlinedPreendorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartInlinedPreendorsement.prototype._read = function() {
      this.id013PtjakartInlinedPreendorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id013PtjakartInlinedPreendorsementContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id013PtjakartOperationContentsList.Bool.TRUE) {
        this.signature = this._io.readBytes(64);
      }
    }

    return Id013PtjakartInlinedPreendorsement;
  })();

  var Delegation = Id013PtjakartOperationContentsList.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id013PtjakartOperationContentsList.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var Case1923 = Id013PtjakartOperationContentsList.Case1923 = (function() {
    function Case1923(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1923.prototype._read = function() {
      this.lenCase192 = this._io.readU1();
      if (!(this.lenCase192 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase192, this._io, "/types/case__192_3/seq/0");
      }
      this._raw_case192 = this._io.readBytes(this.lenCase192);
      var _io__raw_case192 = new KaitaiStream(this._raw_case192);
      this.case192 = new Case192(_io__raw_case192, this, this._root);
    }

    return Case1923;
  })();

  var RegisterGlobalConstant = Id013PtjakartOperationContentsList.RegisterGlobalConstant = (function() {
    function RegisterGlobalConstant(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RegisterGlobalConstant.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return RegisterGlobalConstant;
  })();

  var Case129Entries = Id013PtjakartOperationContentsList.Case129Entries = (function() {
    function Case129Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129Entries.prototype._read = function() {
      this.case129EltField0 = new Case129EltField00(this._io, this, this._root);
      this.case129EltField1 = new Case129EltField1(this._io, this, this._root);
    }

    return Case129Entries;
  })();

  var Case2Field3 = Id013PtjakartOperationContentsList.Case2Field3 = (function() {
    function Case2Field3(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case2Field3.prototype._read = function() {
      this.case2Field3Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case2Field3Entries.push(new Case2Field3Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case2Field3;
  })();

  var PreviousMessageResult = Id013PtjakartOperationContentsList.PreviousMessageResult = (function() {
    function PreviousMessageResult(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResult.prototype._read = function() {
      this.contextHash = this._io.readBytes(32);
      this.withdrawListHash = this._io.readBytes(32);
    }

    return PreviousMessageResult;
  })();

  var Case1Field30 = Id013PtjakartOperationContentsList.Case1Field30 = (function() {
    function Case1Field30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1Field30.prototype._read = function() {
      this.lenCase1Field3 = this._io.readU4be();
      if (!(this.lenCase1Field3 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase1Field3, this._io, "/types/case__1_field3_0/seq/0");
      }
      this._raw_case1Field3 = this._io.readBytes(this.lenCase1Field3);
      var _io__raw_case1Field3 = new KaitaiStream(this._raw_case1Field3);
      this.case1Field3 = new Case1Field3(_io__raw_case1Field3, this, this._root);
    }

    return Case1Field30;
  })();

  var Case130EltField0 = Id013PtjakartOperationContentsList.Case130EltField0 = (function() {
    function Case130EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130EltField0.prototype._read = function() {
      this.case130EltField0 = this._io.readBytesFull();
    }

    return Case130EltField0;
  })();

  var Case6 = Id013PtjakartOperationContentsList.Case6 = (function() {
    function Case6(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case6.prototype._read = function() {
      this.case6Field0 = this._io.readS4be();
      this.case6Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__6_field1_field0
     */

    return Case6;
  })();

  var PreviousMessageResultPath = Id013PtjakartOperationContentsList.PreviousMessageResultPath = (function() {
    function PreviousMessageResultPath(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResultPath.prototype._read = function() {
      this.previousMessageResultPathEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.previousMessageResultPathEntries.push(new PreviousMessageResultPathEntries(this._io, this, this._root));
        i++;
      }
    }

    return PreviousMessageResultPath;
  })();

  var ScRollupAddMessages = Id013PtjakartOperationContentsList.ScRollupAddMessages = (function() {
    function ScRollupAddMessages(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupAddMessages.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id013PtjakartRollupAddress(this._io, this, this._root);
      this.message = new Message1(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupAddMessages;
  })();

  var Id013PtjakartLiquidityBakingToggleVote = Id013PtjakartOperationContentsList.Id013PtjakartLiquidityBakingToggleVote = (function() {
    function Id013PtjakartLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartLiquidityBakingToggleVote.prototype._read = function() {
      this.id013PtjakartLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id013PtjakartLiquidityBakingToggleVote;
  })();

  var Case1Field3Entries = Id013PtjakartOperationContentsList.Case1Field3Entries = (function() {
    function Case1Field3Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1Field3Entries.prototype._read = function() {
      this.case1Field3EltTag = this._io.readU1();
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__8) {
        this.case8 = new Case8(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__4) {
        this.case4 = new Case4(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__12) {
        this.case12 = new Case12(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__9) {
        this.case9 = new Case9(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__5) {
        this.case5 = new Case5(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__13) {
        this.case13 = new Case13(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__10) {
        this.case10 = new Case10(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__6) {
        this.case6 = new Case6(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__14) {
        this.case14 = new Case14(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__11) {
        this.case11 = new Case11(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__7) {
        this.case7 = new Case7(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__15) {
        this.case15 = new Case15(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__129) {
        this.case129 = new Case129Entries(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__130) {
        this.case130 = new Case130Entries(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__131) {
        this.case131 = new Case1312(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__192) {
        this.case192 = new Case1922(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__193) {
        this.case193 = new Case1932(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__195) {
        this.case195 = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__224) {
        this.case224 = new Case224(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__225) {
        this.case225 = new Case225(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__226) {
        this.case226 = new Case226(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id013PtjakartOperationContentsList.Case1Field3EltTag.CASE__227) {
        this.case227 = new Case227(this._io, this, this._root);
      }
    }

    return Case1Field3Entries;
  })();

  var Id013PtjakartMutez = Id013PtjakartOperationContentsList.Id013PtjakartMutez = (function() {
    function Id013PtjakartMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartMutez.prototype._read = function() {
      this.id013PtjakartMutez = new N(this._io, this, this._root);
    }

    return Id013PtjakartMutez;
  })();

  var PreviousMessageResultPath0 = Id013PtjakartOperationContentsList.PreviousMessageResultPath0 = (function() {
    function PreviousMessageResultPath0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResultPath0.prototype._read = function() {
      this.lenPreviousMessageResultPath = this._io.readU4be();
      if (!(this.lenPreviousMessageResultPath <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenPreviousMessageResultPath, this._io, "/types/previous_message_result_path_0/seq/0");
      }
      this._raw_previousMessageResultPath = this._io.readBytes(this.lenPreviousMessageResultPath);
      var _io__raw_previousMessageResultPath = new KaitaiStream(this._raw_previousMessageResultPath);
      this.previousMessageResultPath = new PreviousMessageResultPath(_io__raw_previousMessageResultPath, this, this._root);
    }

    return PreviousMessageResultPath0;
  })();

  var Case0Field3Entries = Id013PtjakartOperationContentsList.Case0Field3Entries = (function() {
    function Case0Field3Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case0Field3Entries.prototype._read = function() {
      this.case0Field3EltTag = this._io.readU1();
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__8) {
        this.case8 = new Case8(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__4) {
        this.case4 = new Case4(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__12) {
        this.case12 = new Case12(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__9) {
        this.case9 = new Case9(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__5) {
        this.case5 = new Case5(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__13) {
        this.case13 = new Case13(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__10) {
        this.case10 = new Case10(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__6) {
        this.case6 = new Case6(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__14) {
        this.case14 = new Case14(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__11) {
        this.case11 = new Case11(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__7) {
        this.case7 = new Case7(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__15) {
        this.case15 = new Case15(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__129) {
        this.case129 = new Case129Entries(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__130) {
        this.case130 = new Case130Entries(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__131) {
        this.case131 = new Case1310(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__192) {
        this.case192 = new Case1920(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__193) {
        this.case193 = new Case1930(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__195) {
        this.case195 = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__224) {
        this.case224 = new Case224(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__225) {
        this.case225 = new Case225(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__226) {
        this.case226 = new Case226(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id013PtjakartOperationContentsList.Case0Field3EltTag.CASE__227) {
        this.case227 = new Case227(this._io, this, this._root);
      }
    }

    return Case0Field3Entries;
  })();

  var Bh10 = Id013PtjakartOperationContentsList.Bh10 = (function() {
    function Bh10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh10.prototype._read = function() {
      this.lenBh1 = this._io.readU4be();
      if (!(this.lenBh1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh1, this._io, "/types/bh1_0/seq/0");
      }
      this._raw_bh1 = this._io.readBytes(this.lenBh1);
      var _io__raw_bh1 = new KaitaiStream(this._raw_bh1);
      this.bh1 = new Bh1(_io__raw_bh1, this, this._root);
    }

    return Bh10;
  })();

  var Case131EltField0 = Id013PtjakartOperationContentsList.Case131EltField0 = (function() {
    function Case131EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131EltField0.prototype._read = function() {
      this.case131EltField0 = this._io.readBytesFull();
    }

    return Case131EltField0;
  })();

  var Case3 = Id013PtjakartOperationContentsList.Case3 = (function() {
    function Case3(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case3.prototype._read = function() {
      this.case3Field0 = this._io.readS2be();
      this.case3Field1 = this._io.readBytes(32);
      this.case3Field2 = this._io.readBytes(32);
      this.case3Field3 = new Case3Field30(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case3;
  })();

  var Case224 = Id013PtjakartOperationContentsList.Case224 = (function() {
    function Case224(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case224.prototype._read = function() {
      this.case224Field0 = this._io.readU1();
      this.case224Field1 = new Case224Field10(this._io, this, this._root);
      this.case224Field2 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    return Case224;
  })();

  var Op22 = Id013PtjakartOperationContentsList.Op22 = (function() {
    function Op22(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op22.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_2/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op21(_io__raw_op2, this, this._root);
    }

    return Op22;
  })();

  var Case14Field1 = Id013PtjakartOperationContentsList.Case14Field1 = (function() {
    function Case14Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case14Field1.prototype._read = function() {
      this.case14Field1Field0 = this._io.readBytes(32);
      this.case14Field1Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case14Field1;
  })();

  var Case227Field10 = Id013PtjakartOperationContentsList.Case227Field10 = (function() {
    function Case227Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case227Field10.prototype._read = function() {
      this.lenCase227Field1 = this._io.readU1();
      if (!(this.lenCase227Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase227Field1, this._io, "/types/case__227_field1_0/seq/0");
      }
      this._raw_case227Field1 = this._io.readBytes(this.lenCase227Field1);
      var _io__raw_case227Field1 = new KaitaiStream(this._raw_case227Field1);
      this.case227Field1 = new Case227Field1(_io__raw_case227Field1, this, this._root);
    }

    return Case227Field10;
  })();

  var Message0 = Id013PtjakartOperationContentsList.Message0 = (function() {
    function Message0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message0.prototype._read = function() {
      this.messageEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messageEntries.push(new MessageEntries(this._io, this, this._root));
        i++;
      }
    }

    return Message0;
  })();

  var Case1930 = Id013PtjakartOperationContentsList.Case1930 = (function() {
    function Case1930(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1930.prototype._read = function() {
      this.lenCase193 = this._io.readU2be();
      if (!(this.lenCase193 <= 65535)) {
        throw new KaitaiStream.ValidationGreaterThanError(65535, this.lenCase193, this._io, "/types/case__193_0/seq/0");
      }
      this._raw_case193 = this._io.readBytes(this.lenCase193);
      var _io__raw_case193 = new KaitaiStream(this._raw_case193);
      this.case193 = new Case193(_io__raw_case193, this, this._root);
    }

    return Case1930;
  })();

  var BytesDynUint30 = Id013PtjakartOperationContentsList.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Case131Entries = Id013PtjakartOperationContentsList.Case131Entries = (function() {
    function Case131Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131Entries.prototype._read = function() {
      this.case131EltField0 = new Case131EltField00(this._io, this, this._root);
      this.case131EltField1 = new Case131EltField1(this._io, this, this._root);
    }

    return Case131Entries;
  })();

  var Bh1 = Id013PtjakartOperationContentsList.Bh1 = (function() {
    function Bh1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh1.prototype._read = function() {
      this.id013PtjakartBlockHeaderAlphaFullHeader = new Id013PtjakartBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh1;
  })();

  var Case0Field3 = Id013PtjakartOperationContentsList.Case0Field3 = (function() {
    function Case0Field3(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case0Field3.prototype._read = function() {
      this.case0Field3Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case0Field3Entries.push(new Case0Field3Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case0Field3;
  })();

  var TxRollupRejection = Id013PtjakartOperationContentsList.TxRollupRejection = (function() {
    function TxRollupRejection(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupRejection.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id013PtjakartTxRollupId(this._io, this, this._root);
      this.level = this._io.readS4be();
      this.message = new Message(this._io, this, this._root);
      this.messagePosition = new N(this._io, this, this._root);
      this.messagePath = new MessagePath0(this._io, this, this._root);
      this.messageResultHash = this._io.readBytes(32);
      this.messageResultPath = new MessageResultPath0(this._io, this, this._root);
      this.previousMessageResult = new PreviousMessageResult(this._io, this, this._root);
      this.previousMessageResultPath = new PreviousMessageResultPath0(this._io, this, this._root);
      this.proof = new Proof(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupRejection;
  })();

  var SetDepositsLimit = Id013PtjakartOperationContentsList.SetDepositsLimit = (function() {
    function SetDepositsLimit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SetDepositsLimit.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.limitTag = this._io.readU1();
      if (this.limitTag == Id013PtjakartOperationContentsList.Bool.TRUE) {
        this.limit = new Id013PtjakartMutez(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return SetDepositsLimit;
  })();

  var Case15 = Id013PtjakartOperationContentsList.Case15 = (function() {
    function Case15(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case15.prototype._read = function() {
      this.case15Field0 = this._io.readS8be();
      this.case15Field1 = new Case15Field1(this._io, this, this._root);
    }

    return Case15;
  })();

  var Message1 = Id013PtjakartOperationContentsList.Message1 = (function() {
    function Message1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message1.prototype._read = function() {
      this.lenMessage = this._io.readU4be();
      if (!(this.lenMessage <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessage, this._io, "/types/message_1/seq/0");
      }
      this._raw_message = this._io.readBytes(this.lenMessage);
      var _io__raw_message = new KaitaiStream(this._raw_message);
      this.message = new Message0(_io__raw_message, this, this._root);
    }

    return Message1;
  })();

  var Op10 = Id013PtjakartOperationContentsList.Op10 = (function() {
    function Op10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op10.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_0/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op1(_io__raw_op1, this, this._root);
    }

    return Op10;
  })();

  var Op21 = Id013PtjakartOperationContentsList.Op21 = (function() {
    function Op21(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op21.prototype._read = function() {
      this.id013PtjakartInlinedPreendorsement = new Id013PtjakartInlinedPreendorsement(this._io, this, this._root);
    }

    return Op21;
  })();

  var Id013PtjakartBlockHeaderAlphaFullHeader = Id013PtjakartOperationContentsList.Id013PtjakartBlockHeaderAlphaFullHeader = (function() {
    function Id013PtjakartBlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartBlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id013PtjakartBlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id013PtjakartBlockHeaderAlphaSignedContents = new Id013PtjakartBlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id013PtjakartBlockHeaderAlphaFullHeader;
  })();

  var Proof = Id013PtjakartOperationContentsList.Proof = (function() {
    function Proof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proof.prototype._read = function() {
      this.proofTag = this._io.readU1();
      if (this.proofTag == Id013PtjakartOperationContentsList.ProofTag.CASE__0) {
        this.case0 = new Case0(this._io, this, this._root);
      }
      if (this.proofTag == Id013PtjakartOperationContentsList.ProofTag.CASE__2) {
        this.case2 = new Case2(this._io, this, this._root);
      }
      if (this.proofTag == Id013PtjakartOperationContentsList.ProofTag.CASE__1) {
        this.case1 = new Case1(this._io, this, this._root);
      }
      if (this.proofTag == Id013PtjakartOperationContentsList.ProofTag.CASE__3) {
        this.case3 = new Case3(this._io, this, this._root);
      }
    }

    return Proof;
  })();

  var Commitment0 = Id013PtjakartOperationContentsList.Commitment0 = (function() {
    function Commitment0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitment0.prototype._read = function() {
      this.compressedState = this._io.readBytes(32);
      this.inboxLevel = this._io.readS4be();
      this.predecessor = this._io.readBytes(32);
      this.numberOfMessages = this._io.readS4be();
      this.numberOfTicks = this._io.readS4be();
    }

    return Commitment0;
  })();

  var Origination = Id013PtjakartOperationContentsList.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.balance = new Id013PtjakartMutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id013PtjakartOperationContentsList.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id013PtjakartScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var Case226Field10 = Id013PtjakartOperationContentsList.Case226Field10 = (function() {
    function Case226Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case226Field10.prototype._read = function() {
      this.lenCase226Field1 = this._io.readU1();
      if (!(this.lenCase226Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase226Field1, this._io, "/types/case__226_field1_0/seq/0");
      }
      this._raw_case226Field1 = this._io.readBytes(this.lenCase226Field1);
      var _io__raw_case226Field1 = new KaitaiStream(this._raw_case226Field1);
      this.case226Field1 = new Case226Field1(_io__raw_case226Field1, this, this._root);
    }

    return Case226Field10;
  })();

  var Case1312 = Id013PtjakartOperationContentsList.Case1312 = (function() {
    function Case1312(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1312.prototype._read = function() {
      this.lenCase131 = this._io.readU4be();
      if (!(this.lenCase131 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase131, this._io, "/types/case__131_2/seq/0");
      }
      this._raw_case131 = this._io.readBytes(this.lenCase131);
      var _io__raw_case131 = new KaitaiStream(this._raw_case131);
      this.case131 = new Case131(_io__raw_case131, this, this._root);
    }

    return Case1312;
  })();

  var Case130EltField1 = Id013PtjakartOperationContentsList.Case130EltField1 = (function() {
    function Case130EltField1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130EltField1.prototype._read = function() {
      this.case130EltField1Tag = this._io.readU1();
      if (this.case130EltField1Tag == Id013PtjakartOperationContentsList.Case130EltField1Tag.CASE__0) {
        this.case0 = this._io.readBytes(32);
      }
      if (this.case130EltField1Tag == Id013PtjakartOperationContentsList.Case130EltField1Tag.CASE__1) {
        this.case1 = this._io.readBytes(32);
      }
    }

    return Case130EltField1;
  })();

  var Message = Id013PtjakartOperationContentsList.Message = (function() {
    function Message(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message.prototype._read = function() {
      this.messageTag = this._io.readU1();
      if (this.messageTag == Id013PtjakartOperationContentsList.MessageTag.BATCH) {
        this.batch = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.messageTag == Id013PtjakartOperationContentsList.MessageTag.DEPOSIT) {
        this.deposit = new Deposit(this._io, this, this._root);
      }
    }

    return Message;
  })();

  var Case1932 = Id013PtjakartOperationContentsList.Case1932 = (function() {
    function Case1932(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1932.prototype._read = function() {
      this.lenCase193 = this._io.readU2be();
      if (!(this.lenCase193 <= 65535)) {
        throw new KaitaiStream.ValidationGreaterThanError(65535, this.lenCase193, this._io, "/types/case__193_2/seq/0");
      }
      this._raw_case193 = this._io.readBytes(this.lenCase193);
      var _io__raw_case193 = new KaitaiStream(this._raw_case193);
      this.case193 = new Case193(_io__raw_case193, this, this._root);
    }

    return Case1932;
  })();

  var Op2 = Id013PtjakartOperationContentsList.Op2 = (function() {
    function Op2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op2.prototype._read = function() {
      this.id013PtjakartInlinedEndorsement = new Id013PtjakartInlinedEndorsement(this._io, this, this._root);
    }

    return Op2;
  })();

  var Case7 = Id013PtjakartOperationContentsList.Case7 = (function() {
    function Case7(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case7.prototype._read = function() {
      this.case7Field0 = this._io.readS8be();
      this.case7Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__7_field1_field0
     */

    return Case7;
  })();

  var TxRollupReturnBond = Id013PtjakartOperationContentsList.TxRollupReturnBond = (function() {
    function TxRollupReturnBond(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupReturnBond.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id013PtjakartTxRollupId(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupReturnBond;
  })();

  var Case224Field10 = Id013PtjakartOperationContentsList.Case224Field10 = (function() {
    function Case224Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case224Field10.prototype._read = function() {
      this.lenCase224Field1 = this._io.readU1();
      if (!(this.lenCase224Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase224Field1, this._io, "/types/case__224_field1_0/seq/0");
      }
      this._raw_case224Field1 = this._io.readBytes(this.lenCase224Field1);
      var _io__raw_case224Field1 = new KaitaiStream(this._raw_case224Field1);
      this.case224Field1 = new Case224Field1(_io__raw_case224Field1, this, this._root);
    }

    return Case224Field10;
  })();

  var TicketsInfo0 = Id013PtjakartOperationContentsList.TicketsInfo0 = (function() {
    function TicketsInfo0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TicketsInfo0.prototype._read = function() {
      this.lenTicketsInfo = this._io.readU4be();
      if (!(this.lenTicketsInfo <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenTicketsInfo, this._io, "/types/tickets_info_0/seq/0");
      }
      this._raw_ticketsInfo = this._io.readBytes(this.lenTicketsInfo);
      var _io__raw_ticketsInfo = new KaitaiStream(this._raw_ticketsInfo);
      this.ticketsInfo = new TicketsInfo(_io__raw_ticketsInfo, this, this._root);
    }

    return TicketsInfo0;
  })();

  var TxRollupFinalizeCommitment = Id013PtjakartOperationContentsList.TxRollupFinalizeCommitment = (function() {
    function TxRollupFinalizeCommitment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupFinalizeCommitment.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id013PtjakartTxRollupId(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupFinalizeCommitment;
  })();

  var TxRollupOrigination = Id013PtjakartOperationContentsList.TxRollupOrigination = (function() {
    function TxRollupOrigination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupOrigination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return TxRollupOrigination;
  })();

  var Case227Field1 = Id013PtjakartOperationContentsList.Case227Field1 = (function() {
    function Case227Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case227Field1.prototype._read = function() {
      this.case227Field1 = this._io.readBytesFull();
    }

    return Case227Field1;
  })();

  var Id013PtjakartRollupAddress = Id013PtjakartOperationContentsList.Id013PtjakartRollupAddress = (function() {
    function Id013PtjakartRollupAddress(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartRollupAddress.prototype._read = function() {
      this.id013PtjakartRollupAddress = new BytesDynUint30(this._io, this, this._root);
    }

    return Id013PtjakartRollupAddress;
  })();

  var MessageResultPath = Id013PtjakartOperationContentsList.MessageResultPath = (function() {
    function MessageResultPath(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageResultPath.prototype._read = function() {
      this.messageResultPathEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messageResultPathEntries.push(new MessageResultPathEntries(this._io, this, this._root));
        i++;
      }
    }

    return MessageResultPath;
  })();

  var Int31 = Id013PtjakartOperationContentsList.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id013PtjakartOperationContentsList.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var MessageResultPath0 = Id013PtjakartOperationContentsList.MessageResultPath0 = (function() {
    function MessageResultPath0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageResultPath0.prototype._read = function() {
      this.lenMessageResultPath = this._io.readU4be();
      if (!(this.lenMessageResultPath <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessageResultPath, this._io, "/types/message_result_path_0/seq/0");
      }
      this._raw_messageResultPath = this._io.readBytes(this.lenMessageResultPath);
      var _io__raw_messageResultPath = new KaitaiStream(this._raw_messageResultPath);
      this.messageResultPath = new MessageResultPath(_io__raw_messageResultPath, this, this._root);
    }

    return MessageResultPath0;
  })();

  var MessagePathEntries = Id013PtjakartOperationContentsList.MessagePathEntries = (function() {
    function MessagePathEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagePathEntries.prototype._read = function() {
      this.inboxListHash = this._io.readBytes(32);
    }

    return MessagePathEntries;
  })();

  var Id013PtjakartOperationContentsListEntries = Id013PtjakartOperationContentsList.Id013PtjakartOperationContentsListEntries = (function() {
    function Id013PtjakartOperationContentsListEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartOperationContentsListEntries.prototype._read = function() {
      this.id013PtjakartOperationAlphaContents = new Id013PtjakartOperationAlphaContents(this._io, this, this._root);
    }

    return Id013PtjakartOperationContentsListEntries;
  })();

  var Deposit = Id013PtjakartOperationContentsList.Deposit = (function() {
    function Deposit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposit.prototype._read = function() {
      this.sender = new PublicKeyHash(this._io, this, this._root);
      this.destination = this._io.readBytes(20);
      this.ticketHash = this._io.readBytes(32);
      this.amount = new Amount(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Deposit;
  })();

  var Id013PtjakartInlinedEndorsementMempoolContents = Id013PtjakartOperationContentsList.Id013PtjakartInlinedEndorsementMempoolContents = (function() {
    function Id013PtjakartInlinedEndorsementMempoolContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartInlinedEndorsementMempoolContents.prototype._read = function() {
      this.id013PtjakartInlinedEndorsementMempoolContentsTag = this._io.readU1();
      if (this.id013PtjakartInlinedEndorsementMempoolContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartInlinedEndorsementMempoolContentsTag.ENDORSEMENT) {
        this.endorsement = new Endorsement(this._io, this, this._root);
      }
    }

    return Id013PtjakartInlinedEndorsementMempoolContents;
  })();

  var MessagePath = Id013PtjakartOperationContentsList.MessagePath = (function() {
    function MessagePath(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagePath.prototype._read = function() {
      this.messagePathEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messagePathEntries.push(new MessagePathEntries(this._io, this, this._root));
        i++;
      }
    }

    return MessagePath;
  })();

  var Named0 = Id013PtjakartOperationContentsList.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id013PtjakartOperationContentsList.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.amount = new Id013PtjakartMutez(this._io, this, this._root);
      this.destination = new Id013PtjakartTransactionDestination(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id013PtjakartOperationContentsList.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A destination of a transaction: A destination notation compatible with the contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash, a base58 originated contract hash, or a base58 originated transaction rollup.
     */

    return Transaction;
  })();

  var Case14 = Id013PtjakartOperationContentsList.Case14 = (function() {
    function Case14(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case14.prototype._read = function() {
      this.case14Field0 = this._io.readS4be();
      this.case14Field1 = new Case14Field1(this._io, this, this._root);
    }

    return Case14;
  })();

  var Parameters = Id013PtjakartOperationContentsList.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id013PtjakartEntrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var Case225 = Id013PtjakartOperationContentsList.Case225 = (function() {
    function Case225(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case225.prototype._read = function() {
      this.case225Field0 = this._io.readU2be();
      this.case225Field1 = new Case225Field10(this._io, this, this._root);
      this.case225Field2 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    return Case225;
  })();

  var Case224Field1 = Id013PtjakartOperationContentsList.Case224Field1 = (function() {
    function Case224Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case224Field1.prototype._read = function() {
      this.case224Field1 = this._io.readBytesFull();
    }

    return Case224Field1;
  })();

  var Messages0 = Id013PtjakartOperationContentsList.Messages0 = (function() {
    function Messages0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Messages0.prototype._read = function() {
      this.lenMessages = this._io.readU4be();
      if (!(this.lenMessages <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessages, this._io, "/types/messages_0/seq/0");
      }
      this._raw_messages = this._io.readBytes(this.lenMessages);
      var _io__raw_messages = new KaitaiStream(this._raw_messages);
      this.messages = new Messages(_io__raw_messages, this, this._root);
    }

    return Messages0;
  })();

  var Case1920 = Id013PtjakartOperationContentsList.Case1920 = (function() {
    function Case1920(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1920.prototype._read = function() {
      this.lenCase192 = this._io.readU1();
      if (!(this.lenCase192 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase192, this._io, "/types/case__192_0/seq/0");
      }
      this._raw_case192 = this._io.readBytes(this.lenCase192);
      var _io__raw_case192 = new KaitaiStream(this._raw_case192);
      this.case192 = new Case192(_io__raw_case192, this, this._root);
    }

    return Case1920;
  })();

  var Case1313 = Id013PtjakartOperationContentsList.Case1313 = (function() {
    function Case1313(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1313.prototype._read = function() {
      this.lenCase131 = this._io.readU4be();
      if (!(this.lenCase131 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase131, this._io, "/types/case__131_3/seq/0");
      }
      this._raw_case131 = this._io.readBytes(this.lenCase131);
      var _io__raw_case131 = new KaitaiStream(this._raw_case131);
      this.case131 = new Case131(_io__raw_case131, this, this._root);
    }

    return Case1313;
  })();

  var Proposals1 = Id013PtjakartOperationContentsList.Proposals1 = (function() {
    function Proposals1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals1.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposals = new Proposals0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Proposals1;
  })();

  var TxRollupSubmitBatch = Id013PtjakartOperationContentsList.TxRollupSubmitBatch = (function() {
    function TxRollupSubmitBatch(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupSubmitBatch.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id013PtjakartMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id013PtjakartTxRollupId(this._io, this, this._root);
      this.content = new BytesDynUint30(this._io, this, this._root);
      this.burnLimitTag = this._io.readU1();
      if (this.burnLimitTag == Id013PtjakartOperationContentsList.Bool.TRUE) {
        this.burnLimit = new Id013PtjakartMutez(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupSubmitBatch;
  })();

  var Case130EltField00 = Id013PtjakartOperationContentsList.Case130EltField00 = (function() {
    function Case130EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130EltField00.prototype._read = function() {
      this.lenCase130EltField0 = this._io.readU1();
      if (!(this.lenCase130EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase130EltField0, this._io, "/types/case__130_elt_field0_0/seq/0");
      }
      this._raw_case130EltField0 = this._io.readBytes(this.lenCase130EltField0);
      var _io__raw_case130EltField0 = new KaitaiStream(this._raw_case130EltField0);
      this.case130EltField0 = new Case130EltField0(_io__raw_case130EltField0, this, this._root);
    }

    return Case130EltField00;
  })();

  var TicketsInfo = Id013PtjakartOperationContentsList.TicketsInfo = (function() {
    function TicketsInfo(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TicketsInfo.prototype._read = function() {
      this.ticketsInfoEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.ticketsInfoEntries.push(new TicketsInfoEntries(this._io, this, this._root));
        i++;
      }
    }

    return TicketsInfo;
  })();

  var Ballot = Id013PtjakartOperationContentsList.Ballot = (function() {
    function Ballot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Ballot.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposal = this._io.readBytes(32);
      this.ballot = this._io.readS1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Ballot;
  })();

  var Predecessor = Id013PtjakartOperationContentsList.Predecessor = (function() {
    function Predecessor(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Predecessor.prototype._read = function() {
      this.predecessorTag = this._io.readU1();
      if (this.predecessorTag == Id013PtjakartOperationContentsList.PredecessorTag.SOME) {
        this.some = this._io.readBytes(32);
      }
    }

    return Predecessor;
  })();

  var PublicKeyHash = Id013PtjakartOperationContentsList.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id013PtjakartOperationContentsList.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id013PtjakartOperationContentsList.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id013PtjakartOperationContentsList.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Case1Field3 = Id013PtjakartOperationContentsList.Case1Field3 = (function() {
    function Case1Field3(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1Field3.prototype._read = function() {
      this.case1Field3Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case1Field3Entries.push(new Case1Field3Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case1Field3;
  })();

  var MessagePath0 = Id013PtjakartOperationContentsList.MessagePath0 = (function() {
    function MessagePath0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagePath0.prototype._read = function() {
      this.lenMessagePath = this._io.readU4be();
      if (!(this.lenMessagePath <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessagePath, this._io, "/types/message_path_0/seq/0");
      }
      this._raw_messagePath = this._io.readBytes(this.lenMessagePath);
      var _io__raw_messagePath = new KaitaiStream(this._raw_messagePath);
      this.messagePath = new MessagePath(_io__raw_messagePath, this, this._root);
    }

    return MessagePath0;
  })();

  var TxRollup = Id013PtjakartOperationContentsList.TxRollup = (function() {
    function TxRollup(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollup.prototype._read = function() {
      this.id013PtjakartTxRollupId = new Id013PtjakartTxRollupId(this._io, this, this._root);
      this.txRollupPadding = this._io.readBytes(1);
    }

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    /**
     * This field is for padding, ignore
     */

    return TxRollup;
  })();

  var Op11 = Id013PtjakartOperationContentsList.Op11 = (function() {
    function Op11(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op11.prototype._read = function() {
      this.id013PtjakartInlinedPreendorsement = new Id013PtjakartInlinedPreendorsement(this._io, this, this._root);
    }

    return Op11;
  })();

  var Proposals = Id013PtjakartOperationContentsList.Proposals = (function() {
    function Proposals(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals.prototype._read = function() {
      this.proposalsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.proposalsEntries.push(new ProposalsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Proposals;
  })();

  var Case1931 = Id013PtjakartOperationContentsList.Case1931 = (function() {
    function Case1931(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1931.prototype._read = function() {
      this.lenCase193 = this._io.readU2be();
      if (!(this.lenCase193 <= 65535)) {
        throw new KaitaiStream.ValidationGreaterThanError(65535, this.lenCase193, this._io, "/types/case__193_1/seq/0");
      }
      this._raw_case193 = this._io.readBytes(this.lenCase193);
      var _io__raw_case193 = new KaitaiStream(this._raw_case193);
      this.case193 = new Case193(_io__raw_case193, this, this._root);
    }

    return Case1931;
  })();

  var ProposalsEntries = Id013PtjakartOperationContentsList.ProposalsEntries = (function() {
    function ProposalsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ProposalsEntries.prototype._read = function() {
      this.protocolHash = this._io.readBytes(32);
    }

    return ProposalsEntries;
  })();

  var Id013PtjakartInlinedPreendorsementContents = Id013PtjakartOperationContentsList.Id013PtjakartInlinedPreendorsementContents = (function() {
    function Id013PtjakartInlinedPreendorsementContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartInlinedPreendorsementContents.prototype._read = function() {
      this.id013PtjakartInlinedPreendorsementContentsTag = this._io.readU1();
      if (this.id013PtjakartInlinedPreendorsementContentsTag == Id013PtjakartOperationContentsList.Id013PtjakartInlinedPreendorsementContentsTag.PREENDORSEMENT) {
        this.preendorsement = new Preendorsement(this._io, this, this._root);
      }
    }

    return Id013PtjakartInlinedPreendorsementContents;
  })();

  var Op1 = Id013PtjakartOperationContentsList.Op1 = (function() {
    function Op1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op1.prototype._read = function() {
      this.id013PtjakartInlinedEndorsement = new Id013PtjakartInlinedEndorsement(this._io, this, this._root);
    }

    return Op1;
  })();

  var Case0Field30 = Id013PtjakartOperationContentsList.Case0Field30 = (function() {
    function Case0Field30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case0Field30.prototype._read = function() {
      this.lenCase0Field3 = this._io.readU4be();
      if (!(this.lenCase0Field3 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase0Field3, this._io, "/types/case__0_field3_0/seq/0");
      }
      this._raw_case0Field3 = this._io.readBytes(this.lenCase0Field3);
      var _io__raw_case0Field3 = new KaitaiStream(this._raw_case0Field3);
      this.case0Field3 = new Case0Field3(_io__raw_case0Field3, this, this._root);
    }

    return Case0Field30;
  })();

  var DoublePreendorsementEvidence = Id013PtjakartOperationContentsList.DoublePreendorsementEvidence = (function() {
    function DoublePreendorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoublePreendorsementEvidence.prototype._read = function() {
      this.op1 = new Op12(this._io, this, this._root);
      this.op2 = new Op22(this._io, this, this._root);
    }

    return DoublePreendorsementEvidence;
  })();

  return Id013PtjakartOperationContentsList;
})();
return Id013PtjakartOperationContentsList;
}));
