// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.SignerMessagesSupportsDeterministicNoncesResponse = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: signer_messages.supports_deterministic_nonces.response
 */

var SignerMessagesSupportsDeterministicNoncesResponse = (function() {
  SignerMessagesSupportsDeterministicNoncesResponse.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function SignerMessagesSupportsDeterministicNoncesResponse(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  SignerMessagesSupportsDeterministicNoncesResponse.prototype._read = function() {
    this.bool = this._io.readU1();
  }

  return SignerMessagesSupportsDeterministicNoncesResponse;
})();
return SignerMessagesSupportsDeterministicNoncesResponse;
}));
