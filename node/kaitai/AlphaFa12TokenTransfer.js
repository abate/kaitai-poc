// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaFa12TokenTransfer = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.fa1.2.token_transfer
 */

var AlphaFa12TokenTransfer = (function() {
  AlphaFa12TokenTransfer.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function AlphaFa12TokenTransfer(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaFa12TokenTransfer.prototype._read = function() {
    this.tokenContract = new BytesDynUint30(this._io, this, this._root);
    this.destination = new BytesDynUint30(this._io, this, this._root);
    this.amount = new Z(this._io, this, this._root);
    this.tezAmountTag = this._io.readU1();
    if (this.tezAmountTag == AlphaFa12TokenTransfer.Bool.TRUE) {
      this.tezAmount = new BytesDynUint30(this._io, this, this._root);
    }
    this.feeTag = this._io.readU1();
    if (this.feeTag == AlphaFa12TokenTransfer.Bool.TRUE) {
      this.fee = new BytesDynUint30(this._io, this, this._root);
    }
    this.gasLimitTag = this._io.readU1();
    if (this.gasLimitTag == AlphaFa12TokenTransfer.Bool.TRUE) {
      this.gasLimit = new N(this._io, this, this._root);
    }
    this.storageLimitTag = this._io.readU1();
    if (this.storageLimitTag == AlphaFa12TokenTransfer.Bool.TRUE) {
      this.storageLimit = new Z(this._io, this, this._root);
    }
  }

  var BytesDynUint30 = AlphaFa12TokenTransfer.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var N = AlphaFa12TokenTransfer.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var NChunk = AlphaFa12TokenTransfer.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Z = AlphaFa12TokenTransfer.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  return AlphaFa12TokenTransfer;
})();
return AlphaFa12TokenTransfer;
}));
