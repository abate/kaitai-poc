// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id011Pthangz2VotingPeriodKind = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 011-PtHangz2.voting_period.kind
 */

var Id011Pthangz2VotingPeriodKind = (function() {
  Id011Pthangz2VotingPeriodKind.Id011Pthangz2VotingPeriodKindTag = Object.freeze({
    PROPOSAL: 0,
    EXPLORATION: 1,
    COOLDOWN: 2,
    PROMOTION: 3,
    ADOPTION: 4,

    0: "PROPOSAL",
    1: "EXPLORATION",
    2: "COOLDOWN",
    3: "PROMOTION",
    4: "ADOPTION",
  });

  function Id011Pthangz2VotingPeriodKind(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2VotingPeriodKind.prototype._read = function() {
    this.id011Pthangz2VotingPeriodKindTag = this._io.readU1();
  }

  return Id011Pthangz2VotingPeriodKind;
})();
return Id011Pthangz2VotingPeriodKind;
}));
