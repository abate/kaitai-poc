// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.UserActivatedProtocolOverrides = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: user_activated.protocol_overrides
 * Description: User activated protocol overrides: activate a protocol instead of another.
 */

var UserActivatedProtocolOverrides = (function() {
  function UserActivatedProtocolOverrides(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  UserActivatedProtocolOverrides.prototype._read = function() {
    this.lenUserActivatedProtocolOverrides = this._io.readU4be();
    if (!(this.lenUserActivatedProtocolOverrides <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenUserActivatedProtocolOverrides, this._io, "/seq/0");
    }
    this._raw_userActivatedProtocolOverrides = this._io.readBytes(this.lenUserActivatedProtocolOverrides);
    var _io__raw_userActivatedProtocolOverrides = new KaitaiStream(this._raw_userActivatedProtocolOverrides);
    this.userActivatedProtocolOverrides = new UserActivatedProtocolOverrides(_io__raw_userActivatedProtocolOverrides, this, this._root);
  }

  var UserActivatedProtocolOverrides = UserActivatedProtocolOverrides.UserActivatedProtocolOverrides = (function() {
    function UserActivatedProtocolOverrides(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    UserActivatedProtocolOverrides.prototype._read = function() {
      this.userActivatedProtocolOverridesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.userActivatedProtocolOverridesEntries.push(new UserActivatedProtocolOverridesEntries(this._io, this, this._root));
        i++;
      }
    }

    return UserActivatedProtocolOverrides;
  })();

  var UserActivatedProtocolOverridesEntries = UserActivatedProtocolOverrides.UserActivatedProtocolOverridesEntries = (function() {
    function UserActivatedProtocolOverridesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    UserActivatedProtocolOverridesEntries.prototype._read = function() {
      this.replacedProtocol = this._io.readBytes(32);
      this.replacementProtocol = this._io.readBytes(32);
    }

    return UserActivatedProtocolOverridesEntries;
  })();

  return UserActivatedProtocolOverrides;
})();
return UserActivatedProtocolOverrides;
}));
