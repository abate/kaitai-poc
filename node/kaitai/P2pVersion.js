// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.P2pVersion = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: p2p_version
 * Description: A version number for the p2p layer.
 */

var P2pVersion = (function() {
  function P2pVersion(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  P2pVersion.prototype._read = function() {
    this.p2pVersion = this._io.readU2be();
  }

  return P2pVersion;
})();
return P2pVersion;
}));
