// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id018ProxfordSmartRollupMetadata = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 018-Proxford.smart_rollup.metadata
 */

var Id018ProxfordSmartRollupMetadata = (function() {
  function Id018ProxfordSmartRollupMetadata(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id018ProxfordSmartRollupMetadata.prototype._read = function() {
    this.address = this._io.readBytes(20);
    this.originationLevel = this._io.readS4be();
  }

  return Id018ProxfordSmartRollupMetadata;
})();
return Id018ProxfordSmartRollupMetadata;
}));
