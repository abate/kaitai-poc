// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaConstantsParametric = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.constants.parametric
 */

var Id019PtparisaConstantsParametric = (function() {
  Id019PtparisaConstantsParametric.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id019PtparisaConstantsParametric.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function Id019PtparisaConstantsParametric(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaConstantsParametric.prototype._read = function() {
    this.consensusRightsDelay = this._io.readU1();
    this.blocksPreservationCycles = this._io.readU1();
    this.delegateParametersActivationDelay = this._io.readU1();
    this.blocksPerCycle = this._io.readS4be();
    this.blocksPerCommitment = this._io.readS4be();
    this.nonceRevelationThreshold = this._io.readS4be();
    this.cyclesPerVotingPeriod = this._io.readS4be();
    this.hardGasLimitPerOperation = new Z(this._io, this, this._root);
    this.hardGasLimitPerBlock = new Z(this._io, this, this._root);
    this.proofOfWorkThreshold = this._io.readS8be();
    this.minimalStake = new Id019PtparisaMutez(this._io, this, this._root);
    this.minimalFrozenStake = new Id019PtparisaMutez(this._io, this, this._root);
    this.vdfDifficulty = this._io.readS8be();
    this.originationSize = new Int31(this._io, this, this._root);
    this.issuanceWeights = new IssuanceWeights(this._io, this, this._root);
    this.costPerByte = new Id019PtparisaMutez(this._io, this, this._root);
    this.hardStorageLimitPerOperation = new Z(this._io, this, this._root);
    this.quorumMin = this._io.readS4be();
    this.quorumMax = this._io.readS4be();
    this.minProposalQuorum = this._io.readS4be();
    this.liquidityBakingSubsidy = new Id019PtparisaMutez(this._io, this, this._root);
    this.liquidityBakingToggleEmaThreshold = this._io.readS4be();
    this.maxOperationsTimeToLive = this._io.readS2be();
    this.minimalBlockDelay = this._io.readS8be();
    this.delayIncrementPerRound = this._io.readS8be();
    this.consensusCommitteeSize = new Int31(this._io, this, this._root);
    this.consensusThreshold = new Int31(this._io, this, this._root);
    this.minimalParticipationRatio = new MinimalParticipationRatio(this._io, this, this._root);
    this.limitOfDelegationOverBaking = this._io.readU1();
    this.percentageOfFrozenDepositsSlashedPerDoubleBaking = this._io.readU2be();
    this.percentageOfFrozenDepositsSlashedPerDoubleAttestation = this._io.readU2be();
    this.maxSlashingPerBlock = this._io.readU2be();
    this.maxSlashingThreshold = new Int31(this._io, this, this._root);
    this.testnetDictatorTag = this._io.readU1();
    if (this.testnetDictatorTag == Id019PtparisaConstantsParametric.Bool.TRUE) {
      this.testnetDictator = new PublicKeyHash(this._io, this, this._root);
    }
    this.initialSeedTag = this._io.readU1();
    if (this.initialSeedTag == Id019PtparisaConstantsParametric.Bool.TRUE) {
      this.initialSeed = this._io.readBytes(32);
    }
    this.cacheScriptSize = new Int31(this._io, this, this._root);
    this.cacheStakeDistributionCycles = this._io.readS1();
    this.cacheSamplerStateCycles = this._io.readS1();
    this.dalParametric = new DalParametric(this._io, this, this._root);
    this.smartRollupArithPvmEnable = this._io.readU1();
    this.smartRollupOriginationSize = new Int31(this._io, this, this._root);
    this.smartRollupChallengeWindowInBlocks = new Int31(this._io, this, this._root);
    this.smartRollupStakeAmount = new Id019PtparisaMutez(this._io, this, this._root);
    this.smartRollupCommitmentPeriodInBlocks = new Int31(this._io, this, this._root);
    this.smartRollupMaxLookaheadInBlocks = this._io.readS4be();
    this.smartRollupMaxActiveOutboxLevels = this._io.readS4be();
    this.smartRollupMaxOutboxMessagesPerLevel = new Int31(this._io, this, this._root);
    this.smartRollupNumberOfSectionsInDissection = this._io.readU1();
    this.smartRollupTimeoutPeriodInBlocks = new Int31(this._io, this, this._root);
    this.smartRollupMaxNumberOfCementedCommitments = new Int31(this._io, this, this._root);
    this.smartRollupMaxNumberOfParallelGames = new Int31(this._io, this, this._root);
    this.smartRollupRevealActivationLevel = new SmartRollupRevealActivationLevel(this._io, this, this._root);
    this.smartRollupPrivateEnable = this._io.readU1();
    this.smartRollupRiscvPvmEnable = this._io.readU1();
    this.zkRollupEnable = this._io.readU1();
    this.zkRollupOriginationSize = new Int31(this._io, this, this._root);
    this.zkRollupMinPendingToProcess = new Int31(this._io, this, this._root);
    this.zkRollupMaxTicketPayloadSize = new Int31(this._io, this, this._root);
    this.globalLimitOfStakingOverBaking = this._io.readU1();
    this.edgeOfStakingOverDelegation = this._io.readU1();
    this.adaptiveIssuanceLaunchEmaThreshold = this._io.readS4be();
    this.adaptiveRewardsParams = new AdaptiveRewardsParams(this._io, this, this._root);
    this.adaptiveIssuanceActivationVoteEnable = this._io.readU1();
    this.autostakingEnable = this._io.readU1();
    this.adaptiveIssuanceForceActivation = this._io.readU1();
    this.nsEnable = this._io.readU1();
    this.directTicketSpendingEnable = this._io.readU1();
  }

  var AdaptiveRewardsParams = Id019PtparisaConstantsParametric.AdaptiveRewardsParams = (function() {
    function AdaptiveRewardsParams(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AdaptiveRewardsParams.prototype._read = function() {
      this.issuanceRatioFinalMin = new IssuanceRatioFinalMin(this._io, this, this._root);
      this.issuanceRatioFinalMax = new IssuanceRatioFinalMax(this._io, this, this._root);
      this.issuanceRatioInitialMin = new IssuanceRatioInitialMin(this._io, this, this._root);
      this.issuanceRatioInitialMax = new IssuanceRatioInitialMax(this._io, this, this._root);
      this.initialPeriod = this._io.readU1();
      this.transitionPeriod = this._io.readU1();
      this.maxBonus = this._io.readS8be();
      this.growthRate = new GrowthRate(this._io, this, this._root);
      this.centerDz = new CenterDz(this._io, this, this._root);
      this.radiusDz = new RadiusDz(this._io, this, this._root);
    }

    return AdaptiveRewardsParams;
  })();

  var CenterDz = Id019PtparisaConstantsParametric.CenterDz = (function() {
    function CenterDz(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CenterDz.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return CenterDz;
  })();

  var IssuanceWeights = Id019PtparisaConstantsParametric.IssuanceWeights = (function() {
    function IssuanceWeights(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IssuanceWeights.prototype._read = function() {
      this.baseTotalIssuedPerMinute = new Id019PtparisaMutez(this._io, this, this._root);
      this.bakingRewardFixedPortionWeight = new Int31(this._io, this, this._root);
      this.bakingRewardBonusWeight = new Int31(this._io, this, this._root);
      this.attestingRewardWeight = new Int31(this._io, this, this._root);
      this.seedNonceRevelationTipWeight = new Int31(this._io, this, this._root);
      this.vdfRevelationTipWeight = new Int31(this._io, this, this._root);
    }

    return IssuanceWeights;
  })();

  var DalParametric = Id019PtparisaConstantsParametric.DalParametric = (function() {
    function DalParametric(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalParametric.prototype._read = function() {
      this.featureEnable = this._io.readU1();
      this.incentivesEnable = this._io.readU1();
      this.numberOfSlots = this._io.readU2be();
      this.attestationLag = this._io.readU1();
      this.attestationThreshold = this._io.readU1();
      this.redundancyFactor = this._io.readU1();
      this.pageSize = this._io.readU2be();
      this.slotSize = new Int31(this._io, this, this._root);
      this.numberOfShards = this._io.readU2be();
    }

    return DalParametric;
  })();

  var N = Id019PtparisaConstantsParametric.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var MinimalParticipationRatio = Id019PtparisaConstantsParametric.MinimalParticipationRatio = (function() {
    function MinimalParticipationRatio(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MinimalParticipationRatio.prototype._read = function() {
      this.numerator = this._io.readU2be();
      this.denominator = this._io.readU2be();
    }

    return MinimalParticipationRatio;
  })();

  var GrowthRate = Id019PtparisaConstantsParametric.GrowthRate = (function() {
    function GrowthRate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    GrowthRate.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return GrowthRate;
  })();

  var SmartRollupRevealActivationLevel = Id019PtparisaConstantsParametric.SmartRollupRevealActivationLevel = (function() {
    function SmartRollupRevealActivationLevel(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SmartRollupRevealActivationLevel.prototype._read = function() {
      this.rawData = this._io.readS4be();
      this.metadata = this._io.readS4be();
      this.dalPage = this._io.readS4be();
      this.dalParameters = this._io.readS4be();
      this.dalAttestedSlotsValidityLag = new Int31(this._io, this, this._root);
    }

    return SmartRollupRevealActivationLevel;
  })();

  var Id019PtparisaMutez = Id019PtparisaConstantsParametric.Id019PtparisaMutez = (function() {
    function Id019PtparisaMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaMutez.prototype._read = function() {
      this.id019PtparisaMutez = new N(this._io, this, this._root);
    }

    return Id019PtparisaMutez;
  })();

  var IssuanceRatioFinalMin = Id019PtparisaConstantsParametric.IssuanceRatioFinalMin = (function() {
    function IssuanceRatioFinalMin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IssuanceRatioFinalMin.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return IssuanceRatioFinalMin;
  })();

  var IssuanceRatioInitialMax = Id019PtparisaConstantsParametric.IssuanceRatioInitialMax = (function() {
    function IssuanceRatioInitialMax(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IssuanceRatioInitialMax.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return IssuanceRatioInitialMax;
  })();

  var IssuanceRatioInitialMin = Id019PtparisaConstantsParametric.IssuanceRatioInitialMin = (function() {
    function IssuanceRatioInitialMin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IssuanceRatioInitialMin.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return IssuanceRatioInitialMin;
  })();

  var RadiusDz = Id019PtparisaConstantsParametric.RadiusDz = (function() {
    function RadiusDz(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RadiusDz.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return RadiusDz;
  })();

  var Int31 = Id019PtparisaConstantsParametric.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id019PtparisaConstantsParametric.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var PublicKeyHash = Id019PtparisaConstantsParametric.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id019PtparisaConstantsParametric.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaConstantsParametric.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaConstantsParametric.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaConstantsParametric.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var IssuanceRatioFinalMax = Id019PtparisaConstantsParametric.IssuanceRatioFinalMax = (function() {
    function IssuanceRatioFinalMax(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IssuanceRatioFinalMax.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return IssuanceRatioFinalMax;
  })();

  var Z = Id019PtparisaConstantsParametric.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  /**
   * A Ed25519, Secp256k1, P256, or BLS public key hash
   */

  return Id019PtparisaConstantsParametric;
})();
return Id019PtparisaConstantsParametric;
}));
