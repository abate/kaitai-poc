// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id018ProxfordBlockHeaderProtocolData = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 018-Proxford.block_header.protocol_data
 */

var Id018ProxfordBlockHeaderProtocolData = (function() {
  Id018ProxfordBlockHeaderProtocolData.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id018ProxfordBlockHeaderProtocolData.Id018ProxfordPerBlockVotesTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
  });

  function Id018ProxfordBlockHeaderProtocolData(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id018ProxfordBlockHeaderProtocolData.prototype._read = function() {
    this.id018ProxfordBlockHeaderAlphaSignedContents = new Id018ProxfordBlockHeaderAlphaSignedContents(this._io, this, this._root);
  }

  var Id018ProxfordBlockHeaderAlphaSignedContents = Id018ProxfordBlockHeaderProtocolData.Id018ProxfordBlockHeaderAlphaSignedContents = (function() {
    function Id018ProxfordBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id018ProxfordBlockHeaderAlphaUnsignedContents = new Id018ProxfordBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytesFull();
    }

    return Id018ProxfordBlockHeaderAlphaSignedContents;
  })();

  var Id018ProxfordBlockHeaderAlphaUnsignedContents = Id018ProxfordBlockHeaderProtocolData.Id018ProxfordBlockHeaderAlphaUnsignedContents = (function() {
    function Id018ProxfordBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id018ProxfordBlockHeaderProtocolData.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.perBlockVotes = new Id018ProxfordPerBlockVotes(this._io, this, this._root);
    }

    return Id018ProxfordBlockHeaderAlphaUnsignedContents;
  })();

  var Id018ProxfordPerBlockVotes = Id018ProxfordBlockHeaderProtocolData.Id018ProxfordPerBlockVotes = (function() {
    function Id018ProxfordPerBlockVotes(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordPerBlockVotes.prototype._read = function() {
      this.id018ProxfordPerBlockVotesTag = this._io.readU1();
    }

    return Id018ProxfordPerBlockVotes;
  })();

  return Id018ProxfordBlockHeaderProtocolData;
})();
return Id018ProxfordBlockHeaderProtocolData;
}));
