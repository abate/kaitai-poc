// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id008Ptedo2zkPeriod = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 008-PtEdo2Zk.period
 */

var Id008Ptedo2zkPeriod = (function() {
  function Id008Ptedo2zkPeriod(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkPeriod.prototype._read = function() {
    this.id008Ptedo2zkPeriod = this._io.readS8be();
  }

  return Id008Ptedo2zkPeriod;
})();
return Id008Ptedo2zkPeriod;
}));
