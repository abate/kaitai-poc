// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id007Psdelph1BlockHeaderShellHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 007-PsDELPH1.block_header.shell_header
 */

var Id007Psdelph1BlockHeaderShellHeader = (function() {
  function Id007Psdelph1BlockHeaderShellHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1BlockHeaderShellHeader.prototype._read = function() {
    this.id007Psdelph1BlockHeaderShellHeader = new BlockHeaderShell(this._io, this, null);
  }

  return Id007Psdelph1BlockHeaderShellHeader;
})();
return Id007Psdelph1BlockHeaderShellHeader;
}));
