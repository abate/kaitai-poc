// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id015PtlimaptFitness = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 015-PtLimaPt.fitness
 */

var Id015PtlimaptFitness = (function() {
  Id015PtlimaptFitness.LockedRoundTag = Object.freeze({
    NONE: 0,
    SOME: 1,

    0: "NONE",
    1: "SOME",
  });

  function Id015PtlimaptFitness(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id015PtlimaptFitness.prototype._read = function() {
    this.level = this._io.readS4be();
    this.lockedRound = new LockedRound(this._io, this, this._root);
    this.predecessorRound = this._io.readS4be();
    this.round = this._io.readS4be();
  }

  var LockedRound = Id015PtlimaptFitness.LockedRound = (function() {
    function LockedRound(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LockedRound.prototype._read = function() {
      this.lockedRoundTag = this._io.readU1();
      if (this.lockedRoundTag == Id015PtlimaptFitness.LockedRoundTag.SOME) {
        this.some = this._io.readS4be();
      }
    }

    return LockedRound;
  })();

  return Id015PtlimaptFitness;
})();
return Id015PtlimaptFitness;
}));
