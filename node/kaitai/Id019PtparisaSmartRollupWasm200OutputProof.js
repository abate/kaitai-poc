// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaSmartRollupWasm200OutputProof = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.smart_rollup.wasm_2_0_0.output.proof
 */

var Id019PtparisaSmartRollupWasm200OutputProof = (function() {
  Id019PtparisaSmartRollupWasm200OutputProof.Id019PtparisaContractIdOriginatedTag = Object.freeze({
    ORIGINATED: 1,

    1: "ORIGINATED",
  });

  Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,
    CASE__128: 128,
    CASE__129: 129,
    CASE__130: 130,
    CASE__131: 131,
    CASE__192: 192,
    CASE__193: 193,
    CASE__195: 195,
    CASE__200: 200,
    CASE__208: 208,
    CASE__216: 216,
    CASE__217: 217,
    CASE__218: 218,
    CASE__219: 219,
    CASE__224: 224,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
    128: "CASE__128",
    129: "CASE__129",
    130: "CASE__130",
    131: "CASE__131",
    192: "CASE__192",
    193: "CASE__193",
    195: "CASE__195",
    200: "CASE__200",
    208: "CASE__208",
    216: "CASE__216",
    217: "CASE__217",
    218: "CASE__218",
    219: "CASE__219",
    224: "CASE__224",
  });

  Id019PtparisaSmartRollupWasm200OutputProof.OutputProofTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
  });

  Id019PtparisaSmartRollupWasm200OutputProof.Id019PtparisaMichelsonV1Primitives = Object.freeze({
    PARAMETER: 0,
    STORAGE: 1,
    CODE: 2,
    FALSE: 3,
    ELT: 4,
    LEFT: 5,
    NONE: 6,
    PAIR_1: 7,
    RIGHT: 8,
    SOME_0: 9,
    TRUE: 10,
    UNIT_1: 11,
    PACK: 12,
    UNPACK: 13,
    BLAKE2B: 14,
    SHA256: 15,
    SHA512: 16,
    ABS: 17,
    ADD: 18,
    AMOUNT: 19,
    AND: 20,
    BALANCE: 21,
    CAR: 22,
    CDR: 23,
    CHECK_SIGNATURE: 24,
    COMPARE: 25,
    CONCAT: 26,
    CONS: 27,
    CREATE_ACCOUNT: 28,
    CREATE_CONTRACT: 29,
    IMPLICIT_ACCOUNT: 30,
    DIP: 31,
    DROP: 32,
    DUP: 33,
    EDIV: 34,
    EMPTY_MAP: 35,
    EMPTY_SET: 36,
    EQ: 37,
    EXEC: 38,
    FAILWITH: 39,
    GE: 40,
    GET: 41,
    GT: 42,
    HASH_KEY: 43,
    IF: 44,
    IF_CONS: 45,
    IF_LEFT: 46,
    IF_NONE: 47,
    INT_0: 48,
    LAMBDA_0: 49,
    LE: 50,
    LEFT_0: 51,
    LOOP: 52,
    LSL: 53,
    LSR: 54,
    LT: 55,
    MAP_0: 56,
    MEM: 57,
    MUL: 58,
    NEG: 59,
    NEQ: 60,
    NIL: 61,
    NONE_0: 62,
    NOT: 63,
    NOW: 64,
    OR_0: 65,
    PAIR_0: 66,
    PUSH: 67,
    RIGHT_0: 68,
    SIZE: 69,
    SOME: 70,
    SOURCE: 71,
    SENDER: 72,
    SELF: 73,
    STEPS_TO_QUOTA: 74,
    SUB: 75,
    SWAP: 76,
    TRANSFER_TOKENS: 77,
    SET_DELEGATE: 78,
    UNIT_0: 79,
    UPDATE: 80,
    XOR: 81,
    ITER: 82,
    LOOP_LEFT: 83,
    ADDRESS_0: 84,
    CONTRACT_0: 85,
    ISNAT: 86,
    CAST: 87,
    RENAME: 88,
    BOOL: 89,
    CONTRACT: 90,
    INT: 91,
    KEY: 92,
    KEY_HASH: 93,
    LAMBDA: 94,
    LIST: 95,
    MAP: 96,
    BIG_MAP: 97,
    NAT: 98,
    OPTION: 99,
    OR: 100,
    PAIR: 101,
    SET: 102,
    SIGNATURE: 103,
    STRING: 104,
    BYTES: 105,
    MUTEZ: 106,
    TIMESTAMP: 107,
    UNIT: 108,
    OPERATION: 109,
    ADDRESS: 110,
    SLICE: 111,
    DIG: 112,
    DUG: 113,
    EMPTY_BIG_MAP: 114,
    APPLY: 115,
    CHAIN_ID: 116,
    CHAIN_ID_0: 117,
    LEVEL: 118,
    SELF_ADDRESS: 119,
    NEVER: 120,
    NEVER_0: 121,
    UNPAIR: 122,
    VOTING_POWER: 123,
    TOTAL_VOTING_POWER: 124,
    KECCAK: 125,
    SHA3: 126,
    PAIRING_CHECK: 127,
    BLS12_381_G1: 128,
    BLS12_381_G2: 129,
    BLS12_381_FR: 130,
    SAPLING_STATE: 131,
    SAPLING_TRANSACTION_DEPRECATED: 132,
    SAPLING_EMPTY_STATE: 133,
    SAPLING_VERIFY_UPDATE: 134,
    TICKET: 135,
    TICKET_DEPRECATED: 136,
    READ_TICKET: 137,
    SPLIT_TICKET: 138,
    JOIN_TICKETS: 139,
    GET_AND_UPDATE: 140,
    CHEST: 141,
    CHEST_KEY: 142,
    OPEN_CHEST: 143,
    VIEW_0: 144,
    VIEW: 145,
    CONSTANT: 146,
    SUB_MUTEZ: 147,
    TX_ROLLUP_L2_ADDRESS: 148,
    MIN_BLOCK_TIME: 149,
    SAPLING_TRANSACTION: 150,
    EMIT: 151,
    LAMBDA_REC_0: 152,
    LAMBDA_REC: 153,
    TICKET_0: 154,
    BYTES_0: 155,
    NAT_0: 156,
    TICKET_1: 157,

    0: "PARAMETER",
    1: "STORAGE",
    2: "CODE",
    3: "FALSE",
    4: "ELT",
    5: "LEFT",
    6: "NONE",
    7: "PAIR_1",
    8: "RIGHT",
    9: "SOME_0",
    10: "TRUE",
    11: "UNIT_1",
    12: "PACK",
    13: "UNPACK",
    14: "BLAKE2B",
    15: "SHA256",
    16: "SHA512",
    17: "ABS",
    18: "ADD",
    19: "AMOUNT",
    20: "AND",
    21: "BALANCE",
    22: "CAR",
    23: "CDR",
    24: "CHECK_SIGNATURE",
    25: "COMPARE",
    26: "CONCAT",
    27: "CONS",
    28: "CREATE_ACCOUNT",
    29: "CREATE_CONTRACT",
    30: "IMPLICIT_ACCOUNT",
    31: "DIP",
    32: "DROP",
    33: "DUP",
    34: "EDIV",
    35: "EMPTY_MAP",
    36: "EMPTY_SET",
    37: "EQ",
    38: "EXEC",
    39: "FAILWITH",
    40: "GE",
    41: "GET",
    42: "GT",
    43: "HASH_KEY",
    44: "IF",
    45: "IF_CONS",
    46: "IF_LEFT",
    47: "IF_NONE",
    48: "INT_0",
    49: "LAMBDA_0",
    50: "LE",
    51: "LEFT_0",
    52: "LOOP",
    53: "LSL",
    54: "LSR",
    55: "LT",
    56: "MAP_0",
    57: "MEM",
    58: "MUL",
    59: "NEG",
    60: "NEQ",
    61: "NIL",
    62: "NONE_0",
    63: "NOT",
    64: "NOW",
    65: "OR_0",
    66: "PAIR_0",
    67: "PUSH",
    68: "RIGHT_0",
    69: "SIZE",
    70: "SOME",
    71: "SOURCE",
    72: "SENDER",
    73: "SELF",
    74: "STEPS_TO_QUOTA",
    75: "SUB",
    76: "SWAP",
    77: "TRANSFER_TOKENS",
    78: "SET_DELEGATE",
    79: "UNIT_0",
    80: "UPDATE",
    81: "XOR",
    82: "ITER",
    83: "LOOP_LEFT",
    84: "ADDRESS_0",
    85: "CONTRACT_0",
    86: "ISNAT",
    87: "CAST",
    88: "RENAME",
    89: "BOOL",
    90: "CONTRACT",
    91: "INT",
    92: "KEY",
    93: "KEY_HASH",
    94: "LAMBDA",
    95: "LIST",
    96: "MAP",
    97: "BIG_MAP",
    98: "NAT",
    99: "OPTION",
    100: "OR",
    101: "PAIR",
    102: "SET",
    103: "SIGNATURE",
    104: "STRING",
    105: "BYTES",
    106: "MUTEZ",
    107: "TIMESTAMP",
    108: "UNIT",
    109: "OPERATION",
    110: "ADDRESS",
    111: "SLICE",
    112: "DIG",
    113: "DUG",
    114: "EMPTY_BIG_MAP",
    115: "APPLY",
    116: "CHAIN_ID",
    117: "CHAIN_ID_0",
    118: "LEVEL",
    119: "SELF_ADDRESS",
    120: "NEVER",
    121: "NEVER_0",
    122: "UNPAIR",
    123: "VOTING_POWER",
    124: "TOTAL_VOTING_POWER",
    125: "KECCAK",
    126: "SHA3",
    127: "PAIRING_CHECK",
    128: "BLS12_381_G1",
    129: "BLS12_381_G2",
    130: "BLS12_381_FR",
    131: "SAPLING_STATE",
    132: "SAPLING_TRANSACTION_DEPRECATED",
    133: "SAPLING_EMPTY_STATE",
    134: "SAPLING_VERIFY_UPDATE",
    135: "TICKET",
    136: "TICKET_DEPRECATED",
    137: "READ_TICKET",
    138: "SPLIT_TICKET",
    139: "JOIN_TICKETS",
    140: "GET_AND_UPDATE",
    141: "CHEST",
    142: "CHEST_KEY",
    143: "OPEN_CHEST",
    144: "VIEW_0",
    145: "VIEW",
    146: "CONSTANT",
    147: "SUB_MUTEZ",
    148: "TX_ROLLUP_L2_ADDRESS",
    149: "MIN_BLOCK_TIME",
    150: "SAPLING_TRANSACTION",
    151: "EMIT",
    152: "LAMBDA_REC_0",
    153: "LAMBDA_REC",
    154: "TICKET_0",
    155: "BYTES_0",
    156: "NAT_0",
    157: "TICKET_1",
  });

  Id019PtparisaSmartRollupWasm200OutputProof.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  Id019PtparisaSmartRollupWasm200OutputProof.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1ExpressionTag = Object.freeze({
    INT: 0,
    STRING: 1,
    SEQUENCE: 2,
    PRIM__NO_ARGS__NO_ANNOTS: 3,
    PRIM__NO_ARGS__SOME_ANNOTS: 4,
    PRIM__1_ARG__NO_ANNOTS: 5,
    PRIM__1_ARG__SOME_ANNOTS: 6,
    PRIM__2_ARGS__NO_ANNOTS: 7,
    PRIM__2_ARGS__SOME_ANNOTS: 8,
    PRIM__GENERIC: 9,
    BYTES: 10,

    0: "INT",
    1: "STRING",
    2: "SEQUENCE",
    3: "PRIM__NO_ARGS__NO_ANNOTS",
    4: "PRIM__NO_ARGS__SOME_ANNOTS",
    5: "PRIM__1_ARG__NO_ANNOTS",
    6: "PRIM__1_ARG__SOME_ANNOTS",
    7: "PRIM__2_ARGS__NO_ANNOTS",
    8: "PRIM__2_ARGS__SOME_ANNOTS",
    9: "PRIM__GENERIC",
    10: "BYTES",
  });

  Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,
    CASE__128: 128,
    CASE__129: 129,
    CASE__130: 130,
    CASE__131: 131,
    CASE__192: 192,
    CASE__208: 208,
    CASE__209: 209,
    CASE__210: 210,
    CASE__211: 211,
    CASE__224: 224,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
    128: "CASE__128",
    129: "CASE__129",
    130: "CASE__130",
    131: "CASE__131",
    192: "CASE__192",
    208: "CASE__208",
    209: "CASE__209",
    210: "CASE__210",
    211: "CASE__211",
    224: "CASE__224",
  });

  Id019PtparisaSmartRollupWasm200OutputProof.MessageTag = Object.freeze({
    ATOMIC_TRANSACTION_BATCH: 0,
    ATOMIC_TRANSACTION_BATCH_TYPED: 1,
    WHITELIST_UPDATE: 2,

    0: "ATOMIC_TRANSACTION_BATCH",
    1: "ATOMIC_TRANSACTION_BATCH_TYPED",
    2: "WHITELIST_UPDATE",
  });

  function Id019PtparisaSmartRollupWasm200OutputProof(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaSmartRollupWasm200OutputProof.prototype._read = function() {
    this.outputProof = new OutputProof(this._io, this, this._root);
    this.outputProofOutput = new OutputProofOutput(this._io, this, this._root);
  }

  var Case131EltField00 = Id019PtparisaSmartRollupWasm200OutputProof.Case131EltField00 = (function() {
    function Case131EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131EltField00.prototype._read = function() {
      this.lenCase131EltField0 = this._io.readU1();
      if (!(this.lenCase131EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase131EltField0, this._io, "/types/case__131_elt_field0_0/seq/0");
      }
      this._raw_case131EltField0 = this._io.readBytes(this.lenCase131EltField0);
      var _io__raw_case131EltField0 = new KaitaiStream(this._raw_case131EltField0);
      this.case131EltField0 = new Case131EltField0(_io__raw_case131EltField0, this, this._root);
    }

    return Case131EltField00;
  })();

  var Case211Field1 = Id019PtparisaSmartRollupWasm200OutputProof.Case211Field1 = (function() {
    function Case211Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case211Field1.prototype._read = function() {
      this.case211Field1 = this._io.readBytesFull();
    }

    return Case211Field1;
  })();

  var Case1 = Id019PtparisaSmartRollupWasm200OutputProof.Case1 = (function() {
    function Case1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1.prototype._read = function() {
      this.case1Field0 = this._io.readU2be();
      this.case1Field1 = new Case1Field1(this._io, this, this._root);
    }

    return Case1;
  })();

  var Case2Field1 = Id019PtparisaSmartRollupWasm200OutputProof.Case2Field1 = (function() {
    function Case2Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case2Field1.prototype._read = function() {
      this.case2Field1Field0 = new InodeTree(this._io, this, this._root);
      this.case2Field1Field1 = new InodeTree(this._io, this, this._root);
    }

    return Case2Field1;
  })();

  var Case192 = Id019PtparisaSmartRollupWasm200OutputProof.Case192 = (function() {
    function Case192(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case192.prototype._read = function() {
      this.case192 = this._io.readBytesFull();
    }

    return Case192;
  })();

  var Case211 = Id019PtparisaSmartRollupWasm200OutputProof.Case211 = (function() {
    function Case211(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case211.prototype._read = function() {
      this.case211Field0 = this._io.readS8be();
      this.case211Field1 = new Case211Field10(this._io, this, this._root);
      this.case211Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case211;
  })();

  var Case10 = Id019PtparisaSmartRollupWasm200OutputProof.Case10 = (function() {
    function Case10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case10.prototype._read = function() {
      this.case1Field0 = this._io.readS2be();
      this.case1Field1 = this._io.readBytes(32);
      this.case1Field2 = this._io.readBytes(32);
      this.case1Field3 = new TreeEncoding(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case10;
  })();

  var Whitelist0 = Id019PtparisaSmartRollupWasm200OutputProof.Whitelist0 = (function() {
    function Whitelist0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Whitelist0.prototype._read = function() {
      this.lenWhitelist = this._io.readU4be();
      if (!(this.lenWhitelist <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenWhitelist, this._io, "/types/whitelist_0/seq/0");
      }
      this._raw_whitelist = this._io.readBytes(this.lenWhitelist);
      var _io__raw_whitelist = new KaitaiStream(this._raw_whitelist);
      this.whitelist = new Whitelist(_io__raw_whitelist, this, this._root);
    }

    return Whitelist0;
  })();

  var Transactions2 = Id019PtparisaSmartRollupWasm200OutputProof.Transactions2 = (function() {
    function Transactions2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transactions2.prototype._read = function() {
      this.lenTransactions = this._io.readU4be();
      if (!(this.lenTransactions <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenTransactions, this._io, "/types/transactions_2/seq/0");
      }
      this._raw_transactions = this._io.readBytes(this.lenTransactions);
      var _io__raw_transactions = new KaitaiStream(this._raw_transactions);
      this.transactions = new Transactions1(_io__raw_transactions, this, this._root);
    }

    return Transactions2;
  })();

  var InodeTree = Id019PtparisaSmartRollupWasm200OutputProof.InodeTree = (function() {
    function InodeTree(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InodeTree.prototype._read = function() {
      this.inodeTreeTag = this._io.readU1();
      if (this.inodeTreeTag == Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag.CASE__0) {
        this.case0 = new Case0(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag.CASE__1) {
        this.case1 = new Case1(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag.CASE__2) {
        this.case2 = new Case2(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag.CASE__3) {
        this.case3 = new Case3(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag.CASE__129) {
        this.case129 = new Case129Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag.CASE__130) {
        this.case130 = new Case130Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag.CASE__131) {
        this.case131 = new Case1310(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag.CASE__192) {
        this.case192 = this._io.readBytes(32);
      }
      if (this.inodeTreeTag == Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag.CASE__208) {
        this.case208 = new Case208(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag.CASE__209) {
        this.case209 = new Case209(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag.CASE__210) {
        this.case210 = new Case210(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id019PtparisaSmartRollupWasm200OutputProof.InodeTreeTag.CASE__211) {
        this.case211 = new Case211(this._io, this, this._root);
      }
    }

    return InodeTree;
  })();

  var Case20 = Id019PtparisaSmartRollupWasm200OutputProof.Case20 = (function() {
    function Case20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case20.prototype._read = function() {
      this.case2Field0 = this._io.readS2be();
      this.case2Field1 = this._io.readBytes(32);
      this.case2Field2 = this._io.readBytes(32);
      this.case2Field3 = new TreeEncoding(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case20;
  })();

  var Case0Field10 = Id019PtparisaSmartRollupWasm200OutputProof.Case0Field10 = (function() {
    function Case0Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case0Field10.prototype._read = function() {
      this.case0Field1Field0 = new InodeTree(this._io, this, this._root);
      this.case0Field1Field1 = new InodeTree(this._io, this, this._root);
    }

    /**
     * inode_tree
     */

    return Case0Field10;
  })();

  var Originated = Id019PtparisaSmartRollupWasm200OutputProof.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var N = Id019PtparisaSmartRollupWasm200OutputProof.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Case130Entries = Id019PtparisaSmartRollupWasm200OutputProof.Case130Entries = (function() {
    function Case130Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130Entries.prototype._read = function() {
      this.case130EltField0 = new Case130EltField00(this._io, this, this._root);
      this.case130EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case130Entries;
  })();

  var Case208Field1 = Id019PtparisaSmartRollupWasm200OutputProof.Case208Field1 = (function() {
    function Case208Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case208Field1.prototype._read = function() {
      this.case208Field1 = this._io.readBytesFull();
    }

    return Case208Field1;
  })();

  var Case129EltField0 = Id019PtparisaSmartRollupWasm200OutputProof.Case129EltField0 = (function() {
    function Case129EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129EltField0.prototype._read = function() {
      this.case129EltField0 = this._io.readBytesFull();
    }

    return Case129EltField0;
  })();

  var Case218 = Id019PtparisaSmartRollupWasm200OutputProof.Case218 = (function() {
    function Case218(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case218.prototype._read = function() {
      this.case218Field0 = this._io.readS4be();
      this.case218Field1 = new Case218Field10(this._io, this, this._root);
      this.case218Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case218;
  })();

  var Case1310 = Id019PtparisaSmartRollupWasm200OutputProof.Case1310 = (function() {
    function Case1310(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1310.prototype._read = function() {
      this.lenCase131 = this._io.readU4be();
      if (!(this.lenCase131 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase131, this._io, "/types/case__131_0/seq/0");
      }
      this._raw_case131 = this._io.readBytes(this.lenCase131);
      var _io__raw_case131 = new KaitaiStream(this._raw_case131);
      this.case131 = new Case131(_io__raw_case131, this, this._root);
    }

    return Case1310;
  })();

  var Case0Field1 = Id019PtparisaSmartRollupWasm200OutputProof.Case0Field1 = (function() {
    function Case0Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case0Field1.prototype._read = function() {
      this.case0Field1Field0 = new InodeTree(this._io, this, this._root);
      this.case0Field1Field1 = new InodeTree(this._io, this, this._root);
    }

    return Case0Field1;
  })();

  var OutputProof = Id019PtparisaSmartRollupWasm200OutputProof.OutputProof = (function() {
    function OutputProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    OutputProof.prototype._read = function() {
      this.outputProofTag = this._io.readU1();
      if (this.outputProofTag == Id019PtparisaSmartRollupWasm200OutputProof.OutputProofTag.CASE__0) {
        this.case0 = new Case01(this._io, this, this._root);
      }
      if (this.outputProofTag == Id019PtparisaSmartRollupWasm200OutputProof.OutputProofTag.CASE__2) {
        this.case2 = new Case20(this._io, this, this._root);
      }
      if (this.outputProofTag == Id019PtparisaSmartRollupWasm200OutputProof.OutputProofTag.CASE__1) {
        this.case1 = new Case10(this._io, this, this._root);
      }
      if (this.outputProofTag == Id019PtparisaSmartRollupWasm200OutputProof.OutputProofTag.CASE__3) {
        this.case3 = new Case30(this._io, this, this._root);
      }
    }

    return OutputProof;
  })();

  var Case0 = Id019PtparisaSmartRollupWasm200OutputProof.Case0 = (function() {
    function Case0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case0.prototype._read = function() {
      this.case0Field0 = this._io.readU1();
      this.case0Field1 = new Case0Field1(this._io, this, this._root);
    }

    return Case0;
  })();

  var Case210Field1 = Id019PtparisaSmartRollupWasm200OutputProof.Case210Field1 = (function() {
    function Case210Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case210Field1.prototype._read = function() {
      this.case210Field1 = this._io.readBytesFull();
    }

    return Case210Field1;
  })();

  var Prim1ArgSomeAnnots = Id019PtparisaSmartRollupWasm200OutputProof.Prim1ArgSomeAnnots = (function() {
    function Prim1ArgSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim1ArgSomeAnnots.prototype._read = function() {
      this.prim = new Id019PtparisaMichelsonV1Primitives(this._io, this, this._root);
      this.arg = new Micheline019PtparisaMichelsonV1Expression(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return Prim1ArgSomeAnnots;
  })();

  var Case210 = Id019PtparisaSmartRollupWasm200OutputProof.Case210 = (function() {
    function Case210(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case210.prototype._read = function() {
      this.case210Field0 = this._io.readS4be();
      this.case210Field1 = new Case210Field10(this._io, this, this._root);
      this.case210Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case210;
  })();

  var OutputProofOutput = Id019PtparisaSmartRollupWasm200OutputProof.OutputProofOutput = (function() {
    function OutputProofOutput(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    OutputProofOutput.prototype._read = function() {
      this.outboxLevel = this._io.readS4be();
      this.messageIndex = new N(this._io, this, this._root);
      this.message = new Message(this._io, this, this._root);
    }

    return OutputProofOutput;
  })();

  var Case216Field1 = Id019PtparisaSmartRollupWasm200OutputProof.Case216Field1 = (function() {
    function Case216Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case216Field1.prototype._read = function() {
      this.case216Field1 = this._io.readBytesFull();
    }

    return Case216Field1;
  })();

  var Case217Field1 = Id019PtparisaSmartRollupWasm200OutputProof.Case217Field1 = (function() {
    function Case217Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case217Field1.prototype._read = function() {
      this.case217Field1 = this._io.readBytesFull();
    }

    return Case217Field1;
  })();

  var Case193 = Id019PtparisaSmartRollupWasm200OutputProof.Case193 = (function() {
    function Case193(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case193.prototype._read = function() {
      this.case193 = this._io.readBytesFull();
    }

    return Case193;
  })();

  var Case30 = Id019PtparisaSmartRollupWasm200OutputProof.Case30 = (function() {
    function Case30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case30.prototype._read = function() {
      this.case3Field0 = this._io.readS2be();
      this.case3Field1 = this._io.readBytes(32);
      this.case3Field2 = this._io.readBytes(32);
      this.case3Field3 = new TreeEncoding(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case30;
  })();

  var Case208Field10 = Id019PtparisaSmartRollupWasm200OutputProof.Case208Field10 = (function() {
    function Case208Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case208Field10.prototype._read = function() {
      this.lenCase208Field1 = this._io.readU1();
      if (!(this.lenCase208Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase208Field1, this._io, "/types/case__208_field1_0/seq/0");
      }
      this._raw_case208Field1 = this._io.readBytes(this.lenCase208Field1);
      var _io__raw_case208Field1 = new KaitaiStream(this._raw_case208Field1);
      this.case208Field1 = new Case208Field1(_io__raw_case208Field1, this, this._root);
    }

    return Case208Field10;
  })();

  var PrimNoArgsSomeAnnots = Id019PtparisaSmartRollupWasm200OutputProof.PrimNoArgsSomeAnnots = (function() {
    function PrimNoArgsSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrimNoArgsSomeAnnots.prototype._read = function() {
      this.prim = new Id019PtparisaMichelsonV1Primitives(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return PrimNoArgsSomeAnnots;
  })();

  var Case1Field1 = Id019PtparisaSmartRollupWasm200OutputProof.Case1Field1 = (function() {
    function Case1Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1Field1.prototype._read = function() {
      this.case1Field1Field0 = new InodeTree(this._io, this, this._root);
      this.case1Field1Field1 = new InodeTree(this._io, this, this._root);
    }

    return Case1Field1;
  })();

  var WhitelistEntries = Id019PtparisaSmartRollupWasm200OutputProof.WhitelistEntries = (function() {
    function WhitelistEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    WhitelistEntries.prototype._read = function() {
      this.signaturePublicKeyHash = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return WhitelistEntries;
  })();

  var Case219 = Id019PtparisaSmartRollupWasm200OutputProof.Case219 = (function() {
    function Case219(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case219.prototype._read = function() {
      this.case219Field0 = this._io.readS8be();
      this.case219Field1 = new Case219Field10(this._io, this, this._root);
      this.case219Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case219;
  })();

  var Case219Field10 = Id019PtparisaSmartRollupWasm200OutputProof.Case219Field10 = (function() {
    function Case219Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case219Field10.prototype._read = function() {
      this.lenCase219Field1 = this._io.readU1();
      if (!(this.lenCase219Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase219Field1, this._io, "/types/case__219_field1_0/seq/0");
      }
      this._raw_case219Field1 = this._io.readBytes(this.lenCase219Field1);
      var _io__raw_case219Field1 = new KaitaiStream(this._raw_case219Field1);
      this.case219Field1 = new Case219Field1(_io__raw_case219Field1, this, this._root);
    }

    return Case219Field10;
  })();

  var TransactionsEntries = Id019PtparisaSmartRollupWasm200OutputProof.TransactionsEntries = (function() {
    function TransactionsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TransactionsEntries.prototype._read = function() {
      this.parameters = new Micheline019PtparisaMichelsonV1Expression(this._io, this, this._root);
      this.destination = new Id019PtparisaContractIdOriginated(this._io, this, this._root);
      this.entrypoint = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A contract handle -- originated account: A contract notation as given to an RPC or inside scripts. Can be a base58 originated contract hash.
     */

    return TransactionsEntries;
  })();

  var Case217Field10 = Id019PtparisaSmartRollupWasm200OutputProof.Case217Field10 = (function() {
    function Case217Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case217Field10.prototype._read = function() {
      this.lenCase217Field1 = this._io.readU1();
      if (!(this.lenCase217Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase217Field1, this._io, "/types/case__217_field1_0/seq/0");
      }
      this._raw_case217Field1 = this._io.readBytes(this.lenCase217Field1);
      var _io__raw_case217Field1 = new KaitaiStream(this._raw_case217Field1);
      this.case217Field1 = new Case217Field1(_io__raw_case217Field1, this, this._root);
    }

    return Case217Field10;
  })();

  var Case129EltField00 = Id019PtparisaSmartRollupWasm200OutputProof.Case129EltField00 = (function() {
    function Case129EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129EltField00.prototype._read = function() {
      this.lenCase129EltField0 = this._io.readU1();
      if (!(this.lenCase129EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase129EltField0, this._io, "/types/case__129_elt_field0_0/seq/0");
      }
      this._raw_case129EltField0 = this._io.readBytes(this.lenCase129EltField0);
      var _io__raw_case129EltField0 = new KaitaiStream(this._raw_case129EltField0);
      this.case129EltField0 = new Case129EltField0(_io__raw_case129EltField0, this, this._root);
    }

    return Case129EltField00;
  })();

  var Case210Field10 = Id019PtparisaSmartRollupWasm200OutputProof.Case210Field10 = (function() {
    function Case210Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case210Field10.prototype._read = function() {
      this.lenCase210Field1 = this._io.readU1();
      if (!(this.lenCase210Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase210Field1, this._io, "/types/case__210_field1_0/seq/0");
      }
      this._raw_case210Field1 = this._io.readBytes(this.lenCase210Field1);
      var _io__raw_case210Field1 = new KaitaiStream(this._raw_case210Field1);
      this.case210Field1 = new Case210Field1(_io__raw_case210Field1, this, this._root);
    }

    return Case210Field10;
  })();

  var TreeEncoding = Id019PtparisaSmartRollupWasm200OutputProof.TreeEncoding = (function() {
    function TreeEncoding(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TreeEncoding.prototype._read = function() {
      this.treeEncodingTag = this._io.readU1();
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__0) {
        this.case0 = new Case00(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__1) {
        this.case1 = new Case1(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__2) {
        this.case2 = new Case2(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__3) {
        this.case3 = new Case3(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__129) {
        this.case129 = new Case129Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__130) {
        this.case130 = new Case130Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__131) {
        this.case131 = new Case1311(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__192) {
        this.case192 = new Case1920(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__193) {
        this.case193 = new Case1930(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__195) {
        this.case195 = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__200) {
        this.case200 = this._io.readBytes(32);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__208) {
        this.case208 = this._io.readBytes(32);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__216) {
        this.case216 = new Case216(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__217) {
        this.case217 = new Case217(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__218) {
        this.case218 = new Case218(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id019PtparisaSmartRollupWasm200OutputProof.TreeEncodingTag.CASE__219) {
        this.case219 = new Case219(this._io, this, this._root);
      }
    }

    return TreeEncoding;
  })();

  var Case208 = Id019PtparisaSmartRollupWasm200OutputProof.Case208 = (function() {
    function Case208(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case208.prototype._read = function() {
      this.case208Field0 = this._io.readU1();
      this.case208Field1 = new Case208Field10(this._io, this, this._root);
      this.case208Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case208;
  })();

  var Case1311 = Id019PtparisaSmartRollupWasm200OutputProof.Case1311 = (function() {
    function Case1311(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1311.prototype._read = function() {
      this.lenCase131 = this._io.readU4be();
      if (!(this.lenCase131 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase131, this._io, "/types/case__131_1/seq/0");
      }
      this._raw_case131 = this._io.readBytes(this.lenCase131);
      var _io__raw_case131 = new KaitaiStream(this._raw_case131);
      this.case131 = new Case131(_io__raw_case131, this, this._root);
    }

    return Case1311;
  })();

  var PrimGeneric = Id019PtparisaSmartRollupWasm200OutputProof.PrimGeneric = (function() {
    function PrimGeneric(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrimGeneric.prototype._read = function() {
      this.prim = new Id019PtparisaMichelsonV1Primitives(this._io, this, this._root);
      this.args = new Args0(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return PrimGeneric;
  })();

  var Case131 = Id019PtparisaSmartRollupWasm200OutputProof.Case131 = (function() {
    function Case131(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131.prototype._read = function() {
      this.case131Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case131Entries.push(new Case131Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case131;
  })();

  var Id019PtparisaMichelsonV1Primitives = Id019PtparisaSmartRollupWasm200OutputProof.Id019PtparisaMichelsonV1Primitives = (function() {
    function Id019PtparisaMichelsonV1Primitives(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaMichelsonV1Primitives.prototype._read = function() {
      this.id019PtparisaMichelsonV1Primitives = this._io.readU1();
    }

    return Id019PtparisaMichelsonV1Primitives;
  })();

  var Case01 = Id019PtparisaSmartRollupWasm200OutputProof.Case01 = (function() {
    function Case01(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case01.prototype._read = function() {
      this.case0Field0 = this._io.readS2be();
      this.case0Field1 = this._io.readBytes(32);
      this.case0Field2 = this._io.readBytes(32);
      this.case0Field3 = new TreeEncoding(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    /**
     * tree_encoding
     */

    return Case01;
  })();

  var Case2 = Id019PtparisaSmartRollupWasm200OutputProof.Case2 = (function() {
    function Case2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case2.prototype._read = function() {
      this.case2Field0 = this._io.readS4be();
      this.case2Field1 = new Case2Field1(this._io, this, this._root);
    }

    return Case2;
  })();

  var TransactionsEntries0 = Id019PtparisaSmartRollupWasm200OutputProof.TransactionsEntries0 = (function() {
    function TransactionsEntries0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TransactionsEntries0.prototype._read = function() {
      this.parameters = new Micheline019PtparisaMichelsonV1Expression(this._io, this, this._root);
      this.parametersTy = new Micheline019PtparisaMichelsonV1Expression(this._io, this, this._root);
      this.destination = new Id019PtparisaContractIdOriginated(this._io, this, this._root);
      this.entrypoint = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A contract handle -- originated account: A contract notation as given to an RPC or inside scripts. Can be a base58 originated contract hash.
     */

    return TransactionsEntries0;
  })();

  var Transactions0 = Id019PtparisaSmartRollupWasm200OutputProof.Transactions0 = (function() {
    function Transactions0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transactions0.prototype._read = function() {
      this.lenTransactions = this._io.readU4be();
      if (!(this.lenTransactions <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenTransactions, this._io, "/types/transactions_0/seq/0");
      }
      this._raw_transactions = this._io.readBytes(this.lenTransactions);
      var _io__raw_transactions = new KaitaiStream(this._raw_transactions);
      this.transactions = new Transactions(_io__raw_transactions, this, this._root);
    }

    return Transactions0;
  })();

  var Case129Entries = Id019PtparisaSmartRollupWasm200OutputProof.Case129Entries = (function() {
    function Case129Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129Entries.prototype._read = function() {
      this.case129EltField0 = new Case129EltField00(this._io, this, this._root);
      this.case129EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case129Entries;
  })();

  var Id019PtparisaContractIdOriginated = Id019PtparisaSmartRollupWasm200OutputProof.Id019PtparisaContractIdOriginated = (function() {
    function Id019PtparisaContractIdOriginated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaContractIdOriginated.prototype._read = function() {
      this.id019PtparisaContractIdOriginatedTag = this._io.readU1();
      if (this.id019PtparisaContractIdOriginatedTag == Id019PtparisaSmartRollupWasm200OutputProof.Id019PtparisaContractIdOriginatedTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    return Id019PtparisaContractIdOriginated;
  })();

  var Case217 = Id019PtparisaSmartRollupWasm200OutputProof.Case217 = (function() {
    function Case217(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case217.prototype._read = function() {
      this.case217Field0 = this._io.readU2be();
      this.case217Field1 = new Case217Field10(this._io, this, this._root);
      this.case217Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case217;
  })();

  var Case219Field1 = Id019PtparisaSmartRollupWasm200OutputProof.Case219Field1 = (function() {
    function Case219Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case219Field1.prototype._read = function() {
      this.case219Field1 = this._io.readBytesFull();
    }

    return Case219Field1;
  })();

  var Case130EltField0 = Id019PtparisaSmartRollupWasm200OutputProof.Case130EltField0 = (function() {
    function Case130EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130EltField0.prototype._read = function() {
      this.case130EltField0 = this._io.readBytesFull();
    }

    return Case130EltField0;
  })();

  var Sequence0 = Id019PtparisaSmartRollupWasm200OutputProof.Sequence0 = (function() {
    function Sequence0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sequence0.prototype._read = function() {
      this.lenSequence = this._io.readU4be();
      if (!(this.lenSequence <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenSequence, this._io, "/types/sequence_0/seq/0");
      }
      this._raw_sequence = this._io.readBytes(this.lenSequence);
      var _io__raw_sequence = new KaitaiStream(this._raw_sequence);
      this.sequence = new Sequence(_io__raw_sequence, this, this._root);
    }

    return Sequence0;
  })();

  var Sequence = Id019PtparisaSmartRollupWasm200OutputProof.Sequence = (function() {
    function Sequence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sequence.prototype._read = function() {
      this.sequenceEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.sequenceEntries.push(new SequenceEntries(this._io, this, this._root));
        i++;
      }
    }

    return Sequence;
  })();

  var Case131EltField0 = Id019PtparisaSmartRollupWasm200OutputProof.Case131EltField0 = (function() {
    function Case131EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131EltField0.prototype._read = function() {
      this.case131EltField0 = this._io.readBytesFull();
    }

    return Case131EltField0;
  })();

  var Case3 = Id019PtparisaSmartRollupWasm200OutputProof.Case3 = (function() {
    function Case3(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case3.prototype._read = function() {
      this.case3Field0 = this._io.readS8be();
      this.case3Field1 = new Case3Field1(this._io, this, this._root);
    }

    return Case3;
  })();

  var Case211Field10 = Id019PtparisaSmartRollupWasm200OutputProof.Case211Field10 = (function() {
    function Case211Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case211Field10.prototype._read = function() {
      this.lenCase211Field1 = this._io.readU1();
      if (!(this.lenCase211Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase211Field1, this._io, "/types/case__211_field1_0/seq/0");
      }
      this._raw_case211Field1 = this._io.readBytes(this.lenCase211Field1);
      var _io__raw_case211Field1 = new KaitaiStream(this._raw_case211Field1);
      this.case211Field1 = new Case211Field1(_io__raw_case211Field1, this, this._root);
    }

    return Case211Field10;
  })();

  var Case1930 = Id019PtparisaSmartRollupWasm200OutputProof.Case1930 = (function() {
    function Case1930(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1930.prototype._read = function() {
      this.lenCase193 = this._io.readU2be();
      if (!(this.lenCase193 <= 65535)) {
        throw new KaitaiStream.ValidationGreaterThanError(65535, this.lenCase193, this._io, "/types/case__193_0/seq/0");
      }
      this._raw_case193 = this._io.readBytes(this.lenCase193);
      var _io__raw_case193 = new KaitaiStream(this._raw_case193);
      this.case193 = new Case193(_io__raw_case193, this, this._root);
    }

    return Case1930;
  })();

  var BytesDynUint30 = Id019PtparisaSmartRollupWasm200OutputProof.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Case131Entries = Id019PtparisaSmartRollupWasm200OutputProof.Case131Entries = (function() {
    function Case131Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131Entries.prototype._read = function() {
      this.case131EltField0 = new Case131EltField00(this._io, this, this._root);
      this.case131EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case131Entries;
  })();

  var Case209Field10 = Id019PtparisaSmartRollupWasm200OutputProof.Case209Field10 = (function() {
    function Case209Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case209Field10.prototype._read = function() {
      this.lenCase209Field1 = this._io.readU1();
      if (!(this.lenCase209Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase209Field1, this._io, "/types/case__209_field1_0/seq/0");
      }
      this._raw_case209Field1 = this._io.readBytes(this.lenCase209Field1);
      var _io__raw_case209Field1 = new KaitaiStream(this._raw_case209Field1);
      this.case209Field1 = new Case209Field1(_io__raw_case209Field1, this, this._root);
    }

    return Case209Field10;
  })();

  var WhitelistUpdate = Id019PtparisaSmartRollupWasm200OutputProof.WhitelistUpdate = (function() {
    function WhitelistUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    WhitelistUpdate.prototype._read = function() {
      this.whitelistTag = this._io.readU1();
      if (this.whitelistTag == Id019PtparisaSmartRollupWasm200OutputProof.Bool.TRUE) {
        this.whitelist = new Whitelist0(this._io, this, this._root);
      }
    }

    return WhitelistUpdate;
  })();

  var Case209 = Id019PtparisaSmartRollupWasm200OutputProof.Case209 = (function() {
    function Case209(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case209.prototype._read = function() {
      this.case209Field0 = this._io.readU2be();
      this.case209Field1 = new Case209Field10(this._io, this, this._root);
      this.case209Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case209;
  })();

  var Case00 = Id019PtparisaSmartRollupWasm200OutputProof.Case00 = (function() {
    function Case00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case00.prototype._read = function() {
      this.case0Field0 = this._io.readU1();
      this.case0Field1 = new Case0Field10(this._io, this, this._root);
    }

    return Case00;
  })();

  var Message = Id019PtparisaSmartRollupWasm200OutputProof.Message = (function() {
    function Message(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message.prototype._read = function() {
      this.messageTag = this._io.readU1();
      if (this.messageTag == Id019PtparisaSmartRollupWasm200OutputProof.MessageTag.ATOMIC_TRANSACTION_BATCH) {
        this.atomicTransactionBatch = new Transactions0(this._io, this, this._root);
      }
      if (this.messageTag == Id019PtparisaSmartRollupWasm200OutputProof.MessageTag.ATOMIC_TRANSACTION_BATCH_TYPED) {
        this.atomicTransactionBatchTyped = new Transactions2(this._io, this, this._root);
      }
      if (this.messageTag == Id019PtparisaSmartRollupWasm200OutputProof.MessageTag.WHITELIST_UPDATE) {
        this.whitelistUpdate = new WhitelistUpdate(this._io, this, this._root);
      }
    }

    return Message;
  })();

  var Whitelist = Id019PtparisaSmartRollupWasm200OutputProof.Whitelist = (function() {
    function Whitelist(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Whitelist.prototype._read = function() {
      this.whitelistEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.whitelistEntries.push(new WhitelistEntries(this._io, this, this._root));
        i++;
      }
    }

    return Whitelist;
  })();

  var Args = Id019PtparisaSmartRollupWasm200OutputProof.Args = (function() {
    function Args(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Args.prototype._read = function() {
      this.argsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.argsEntries.push(new ArgsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Args;
  })();

  var Case209Field1 = Id019PtparisaSmartRollupWasm200OutputProof.Case209Field1 = (function() {
    function Case209Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case209Field1.prototype._read = function() {
      this.case209Field1 = this._io.readBytesFull();
    }

    return Case209Field1;
  })();

  var Case216 = Id019PtparisaSmartRollupWasm200OutputProof.Case216 = (function() {
    function Case216(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case216.prototype._read = function() {
      this.case216Field0 = this._io.readU1();
      this.case216Field1 = new Case216Field10(this._io, this, this._root);
      this.case216Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case216;
  })();

  var Case216Field10 = Id019PtparisaSmartRollupWasm200OutputProof.Case216Field10 = (function() {
    function Case216Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case216Field10.prototype._read = function() {
      this.lenCase216Field1 = this._io.readU1();
      if (!(this.lenCase216Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase216Field1, this._io, "/types/case__216_field1_0/seq/0");
      }
      this._raw_case216Field1 = this._io.readBytes(this.lenCase216Field1);
      var _io__raw_case216Field1 = new KaitaiStream(this._raw_case216Field1);
      this.case216Field1 = new Case216Field1(_io__raw_case216Field1, this, this._root);
    }

    return Case216Field10;
  })();

  var Case218Field1 = Id019PtparisaSmartRollupWasm200OutputProof.Case218Field1 = (function() {
    function Case218Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case218Field1.prototype._read = function() {
      this.case218Field1 = this._io.readBytesFull();
    }

    return Case218Field1;
  })();

  var Prim1ArgNoAnnots = Id019PtparisaSmartRollupWasm200OutputProof.Prim1ArgNoAnnots = (function() {
    function Prim1ArgNoAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim1ArgNoAnnots.prototype._read = function() {
      this.prim = new Id019PtparisaMichelsonV1Primitives(this._io, this, this._root);
      this.arg = new Micheline019PtparisaMichelsonV1Expression(this._io, this, this._root);
    }

    return Prim1ArgNoAnnots;
  })();

  var SequenceEntries = Id019PtparisaSmartRollupWasm200OutputProof.SequenceEntries = (function() {
    function SequenceEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SequenceEntries.prototype._read = function() {
      this.sequenceElt = new Micheline019PtparisaMichelsonV1Expression(this._io, this, this._root);
    }

    return SequenceEntries;
  })();

  var NChunk = Id019PtparisaSmartRollupWasm200OutputProof.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Prim2ArgsNoAnnots = Id019PtparisaSmartRollupWasm200OutputProof.Prim2ArgsNoAnnots = (function() {
    function Prim2ArgsNoAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim2ArgsNoAnnots.prototype._read = function() {
      this.prim = new Id019PtparisaMichelsonV1Primitives(this._io, this, this._root);
      this.arg1 = new Micheline019PtparisaMichelsonV1Expression(this._io, this, this._root);
      this.arg2 = new Micheline019PtparisaMichelsonV1Expression(this._io, this, this._root);
    }

    return Prim2ArgsNoAnnots;
  })();

  var Transactions1 = Id019PtparisaSmartRollupWasm200OutputProof.Transactions1 = (function() {
    function Transactions1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transactions1.prototype._read = function() {
      this.transactionsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.transactionsEntries.push(new TransactionsEntries0(this._io, this, this._root));
        i++;
      }
    }

    return Transactions1;
  })();

  var Case3Field1 = Id019PtparisaSmartRollupWasm200OutputProof.Case3Field1 = (function() {
    function Case3Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case3Field1.prototype._read = function() {
      this.case3Field1Field0 = new InodeTree(this._io, this, this._root);
      this.case3Field1Field1 = new InodeTree(this._io, this, this._root);
    }

    return Case3Field1;
  })();

  var ArgsEntries = Id019PtparisaSmartRollupWasm200OutputProof.ArgsEntries = (function() {
    function ArgsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ArgsEntries.prototype._read = function() {
      this.argsElt = new Micheline019PtparisaMichelsonV1Expression(this._io, this, this._root);
    }

    return ArgsEntries;
  })();

  var Args0 = Id019PtparisaSmartRollupWasm200OutputProof.Args0 = (function() {
    function Args0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Args0.prototype._read = function() {
      this.lenArgs = this._io.readU4be();
      if (!(this.lenArgs <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenArgs, this._io, "/types/args_0/seq/0");
      }
      this._raw_args = this._io.readBytes(this.lenArgs);
      var _io__raw_args = new KaitaiStream(this._raw_args);
      this.args = new Args(_io__raw_args, this, this._root);
    }

    return Args0;
  })();

  var Case218Field10 = Id019PtparisaSmartRollupWasm200OutputProof.Case218Field10 = (function() {
    function Case218Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case218Field10.prototype._read = function() {
      this.lenCase218Field1 = this._io.readU1();
      if (!(this.lenCase218Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase218Field1, this._io, "/types/case__218_field1_0/seq/0");
      }
      this._raw_case218Field1 = this._io.readBytes(this.lenCase218Field1);
      var _io__raw_case218Field1 = new KaitaiStream(this._raw_case218Field1);
      this.case218Field1 = new Case218Field1(_io__raw_case218Field1, this, this._root);
    }

    return Case218Field10;
  })();

  var Prim2ArgsSomeAnnots = Id019PtparisaSmartRollupWasm200OutputProof.Prim2ArgsSomeAnnots = (function() {
    function Prim2ArgsSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim2ArgsSomeAnnots.prototype._read = function() {
      this.prim = new Id019PtparisaMichelsonV1Primitives(this._io, this, this._root);
      this.arg1 = new Micheline019PtparisaMichelsonV1Expression(this._io, this, this._root);
      this.arg2 = new Micheline019PtparisaMichelsonV1Expression(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return Prim2ArgsSomeAnnots;
  })();

  var Case1920 = Id019PtparisaSmartRollupWasm200OutputProof.Case1920 = (function() {
    function Case1920(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1920.prototype._read = function() {
      this.lenCase192 = this._io.readU1();
      if (!(this.lenCase192 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase192, this._io, "/types/case__192_0/seq/0");
      }
      this._raw_case192 = this._io.readBytes(this.lenCase192);
      var _io__raw_case192 = new KaitaiStream(this._raw_case192);
      this.case192 = new Case192(_io__raw_case192, this, this._root);
    }

    return Case1920;
  })();

  var Case130EltField00 = Id019PtparisaSmartRollupWasm200OutputProof.Case130EltField00 = (function() {
    function Case130EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130EltField00.prototype._read = function() {
      this.lenCase130EltField0 = this._io.readU1();
      if (!(this.lenCase130EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase130EltField0, this._io, "/types/case__130_elt_field0_0/seq/0");
      }
      this._raw_case130EltField0 = this._io.readBytes(this.lenCase130EltField0);
      var _io__raw_case130EltField0 = new KaitaiStream(this._raw_case130EltField0);
      this.case130EltField0 = new Case130EltField0(_io__raw_case130EltField0, this, this._root);
    }

    return Case130EltField00;
  })();

  var Micheline019PtparisaMichelsonV1Expression = Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1Expression = (function() {
    function Micheline019PtparisaMichelsonV1Expression(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Micheline019PtparisaMichelsonV1Expression.prototype._read = function() {
      this.micheline019PtparisaMichelsonV1ExpressionTag = this._io.readU1();
      if (this.micheline019PtparisaMichelsonV1ExpressionTag == Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1ExpressionTag.INT) {
        this.int = new Z(this._io, this, this._root);
      }
      if (this.micheline019PtparisaMichelsonV1ExpressionTag == Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1ExpressionTag.STRING) {
        this.string = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.micheline019PtparisaMichelsonV1ExpressionTag == Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1ExpressionTag.SEQUENCE) {
        this.sequence = new Sequence0(this._io, this, this._root);
      }
      if (this.micheline019PtparisaMichelsonV1ExpressionTag == Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1ExpressionTag.PRIM__NO_ARGS__NO_ANNOTS) {
        this.primNoArgsNoAnnots = new Id019PtparisaMichelsonV1Primitives(this._io, this, this._root);
      }
      if (this.micheline019PtparisaMichelsonV1ExpressionTag == Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1ExpressionTag.PRIM__NO_ARGS__SOME_ANNOTS) {
        this.primNoArgsSomeAnnots = new PrimNoArgsSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline019PtparisaMichelsonV1ExpressionTag == Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1ExpressionTag.PRIM__1_ARG__NO_ANNOTS) {
        this.prim1ArgNoAnnots = new Prim1ArgNoAnnots(this._io, this, this._root);
      }
      if (this.micheline019PtparisaMichelsonV1ExpressionTag == Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1ExpressionTag.PRIM__1_ARG__SOME_ANNOTS) {
        this.prim1ArgSomeAnnots = new Prim1ArgSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline019PtparisaMichelsonV1ExpressionTag == Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1ExpressionTag.PRIM__2_ARGS__NO_ANNOTS) {
        this.prim2ArgsNoAnnots = new Prim2ArgsNoAnnots(this._io, this, this._root);
      }
      if (this.micheline019PtparisaMichelsonV1ExpressionTag == Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1ExpressionTag.PRIM__2_ARGS__SOME_ANNOTS) {
        this.prim2ArgsSomeAnnots = new Prim2ArgsSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline019PtparisaMichelsonV1ExpressionTag == Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1ExpressionTag.PRIM__GENERIC) {
        this.primGeneric = new PrimGeneric(this._io, this, this._root);
      }
      if (this.micheline019PtparisaMichelsonV1ExpressionTag == Id019PtparisaSmartRollupWasm200OutputProof.Micheline019PtparisaMichelsonV1ExpressionTag.BYTES) {
        this.bytes = new BytesDynUint30(this._io, this, this._root);
      }
    }

    return Micheline019PtparisaMichelsonV1Expression;
  })();

  var PublicKeyHash = Id019PtparisaSmartRollupWasm200OutputProof.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id019PtparisaSmartRollupWasm200OutputProof.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaSmartRollupWasm200OutputProof.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaSmartRollupWasm200OutputProof.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaSmartRollupWasm200OutputProof.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Transactions = Id019PtparisaSmartRollupWasm200OutputProof.Transactions = (function() {
    function Transactions(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transactions.prototype._read = function() {
      this.transactionsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.transactionsEntries.push(new TransactionsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Transactions;
  })();

  var Z = Id019PtparisaSmartRollupWasm200OutputProof.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  return Id019PtparisaSmartRollupWasm200OutputProof;
})();
return Id019PtparisaSmartRollupWasm200OutputProof;
}));
