// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id016PtmumbaiBlockHeaderContents = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 016-PtMumbai.block_header.contents
 */

var Id016PtmumbaiBlockHeaderContents = (function() {
  Id016PtmumbaiBlockHeaderContents.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id016PtmumbaiBlockHeaderContents(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiBlockHeaderContents.prototype._read = function() {
    this.id016PtmumbaiBlockHeaderAlphaUnsignedContents = new Id016PtmumbaiBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id016PtmumbaiBlockHeaderAlphaUnsignedContents = Id016PtmumbaiBlockHeaderContents.Id016PtmumbaiBlockHeaderAlphaUnsignedContents = (function() {
    function Id016PtmumbaiBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id016PtmumbaiBlockHeaderContents.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id016PtmumbaiLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id016PtmumbaiBlockHeaderAlphaUnsignedContents;
  })();

  var Id016PtmumbaiLiquidityBakingToggleVote = Id016PtmumbaiBlockHeaderContents.Id016PtmumbaiLiquidityBakingToggleVote = (function() {
    function Id016PtmumbaiLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiLiquidityBakingToggleVote.prototype._read = function() {
      this.id016PtmumbaiLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id016PtmumbaiLiquidityBakingToggleVote;
  })();

  return Id016PtmumbaiBlockHeaderContents;
})();
return Id016PtmumbaiBlockHeaderContents;
}));
