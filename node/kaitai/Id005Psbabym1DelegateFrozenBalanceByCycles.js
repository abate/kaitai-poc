// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id005Psbabym1DelegateFrozenBalanceByCycles = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 005-PsBabyM1.delegate.frozen_balance_by_cycles
 */

var Id005Psbabym1DelegateFrozenBalanceByCycles = (function() {
  function Id005Psbabym1DelegateFrozenBalanceByCycles(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1DelegateFrozenBalanceByCycles.prototype._read = function() {
    this.lenId005Psbabym1DelegateFrozenBalanceByCycles = this._io.readU4be();
    if (!(this.lenId005Psbabym1DelegateFrozenBalanceByCycles <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId005Psbabym1DelegateFrozenBalanceByCycles, this._io, "/seq/0");
    }
    this._raw_id005Psbabym1DelegateFrozenBalanceByCycles = this._io.readBytes(this.lenId005Psbabym1DelegateFrozenBalanceByCycles);
    var _io__raw_id005Psbabym1DelegateFrozenBalanceByCycles = new KaitaiStream(this._raw_id005Psbabym1DelegateFrozenBalanceByCycles);
    this.id005Psbabym1DelegateFrozenBalanceByCycles = new Id005Psbabym1DelegateFrozenBalanceByCycles(_io__raw_id005Psbabym1DelegateFrozenBalanceByCycles, this, this._root);
  }

  var N = Id005Psbabym1DelegateFrozenBalanceByCycles.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id005Psbabym1Mutez = Id005Psbabym1DelegateFrozenBalanceByCycles.Id005Psbabym1Mutez = (function() {
    function Id005Psbabym1Mutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1Mutez.prototype._read = function() {
      this.id005Psbabym1Mutez = new N(this._io, this, this._root);
    }

    return Id005Psbabym1Mutez;
  })();

  var Id005Psbabym1DelegateFrozenBalanceByCycles = Id005Psbabym1DelegateFrozenBalanceByCycles.Id005Psbabym1DelegateFrozenBalanceByCycles = (function() {
    function Id005Psbabym1DelegateFrozenBalanceByCycles(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1DelegateFrozenBalanceByCycles.prototype._read = function() {
      this.id005Psbabym1DelegateFrozenBalanceByCyclesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id005Psbabym1DelegateFrozenBalanceByCyclesEntries.push(new Id005Psbabym1DelegateFrozenBalanceByCyclesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id005Psbabym1DelegateFrozenBalanceByCycles;
  })();

  var NChunk = Id005Psbabym1DelegateFrozenBalanceByCycles.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Id005Psbabym1DelegateFrozenBalanceByCyclesEntries = Id005Psbabym1DelegateFrozenBalanceByCycles.Id005Psbabym1DelegateFrozenBalanceByCyclesEntries = (function() {
    function Id005Psbabym1DelegateFrozenBalanceByCyclesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1DelegateFrozenBalanceByCyclesEntries.prototype._read = function() {
      this.cycle = this._io.readS4be();
      this.deposit = new Id005Psbabym1Mutez(this._io, this, this._root);
      this.fees = new Id005Psbabym1Mutez(this._io, this, this._root);
      this.rewards = new Id005Psbabym1Mutez(this._io, this, this._root);
    }

    return Id005Psbabym1DelegateFrozenBalanceByCyclesEntries;
  })();

  return Id005Psbabym1DelegateFrozenBalanceByCycles;
})();
return Id005Psbabym1DelegateFrozenBalanceByCycles;
}));
