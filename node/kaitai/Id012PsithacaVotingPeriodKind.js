// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id012PsithacaVotingPeriodKind = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 012-Psithaca.voting_period.kind
 */

var Id012PsithacaVotingPeriodKind = (function() {
  Id012PsithacaVotingPeriodKind.Id012PsithacaVotingPeriodKindTag = Object.freeze({
    PROPOSAL: 0,
    EXPLORATION: 1,
    COOLDOWN: 2,
    PROMOTION: 3,
    ADOPTION: 4,

    0: "PROPOSAL",
    1: "EXPLORATION",
    2: "COOLDOWN",
    3: "PROMOTION",
    4: "ADOPTION",
  });

  function Id012PsithacaVotingPeriodKind(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id012PsithacaVotingPeriodKind.prototype._read = function() {
    this.id012PsithacaVotingPeriodKindTag = this._io.readU1();
  }

  return Id012PsithacaVotingPeriodKind;
})();
return Id012PsithacaVotingPeriodKind;
}));
