// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './TimestampSystem', './P2pAddress'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./TimestampSystem'), require('./P2pAddress'));
  } else {
    root.P2pPeerPoolEvent = factory(root.KaitaiStream, root.TimestampSystem, root.P2pAddress);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, TimestampSystem, P2pAddress) {
/**
 * Encoding id: p2p_peer.pool_event
 * Description: An event that may happen during maintenance of and other operations on the connection to a specific peer.
 */

var P2pPeerPoolEvent = (function() {
  P2pPeerPoolEvent.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  P2pPeerPoolEvent.Kind = Object.freeze({
    INCOMING_REQUEST: 0,
    REJECTING_REQUEST: 1,
    REQUEST_REJECTED: 2,
    CONNECTION_ESTABLISHED: 3,
    DISCONNECTION: 4,
    EXTERNAL_DISCONNECTION: 5,

    0: "INCOMING_REQUEST",
    1: "REJECTING_REQUEST",
    2: "REQUEST_REJECTED",
    3: "CONNECTION_ESTABLISHED",
    4: "DISCONNECTION",
    5: "EXTERNAL_DISCONNECTION",
  });

  function P2pPeerPoolEvent(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  P2pPeerPoolEvent.prototype._read = function() {
    this.kind = this._io.readU1();
    this.timestamp = new TimestampSystem(this._io, this, null);
    this.addr = new P2pAddress(this._io, this, null);
    this.portTag = this._io.readU1();
    if (this.portTag == P2pPeerPoolEvent.Bool.TRUE) {
      this.port = this._io.readU2be();
    }
  }

  return P2pPeerPoolEvent;
})();
return P2pPeerPoolEvent;
}));
