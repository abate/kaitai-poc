// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './DistributedDbVersionName', './P2pVersion', './DistributedDbVersion'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./DistributedDbVersionName'), require('./P2pVersion'), require('./DistributedDbVersion'));
  } else {
    root.NetworkVersion = factory(root.KaitaiStream, root.DistributedDbVersionName, root.P2pVersion, root.DistributedDbVersion);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, DistributedDbVersionName, P2pVersion, DistributedDbVersion) {
/**
 * Encoding id: network_version
 * Description: A version number for the network protocol (includes distributed DB version and p2p version)
 */

var NetworkVersion = (function() {
  function NetworkVersion(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  NetworkVersion.prototype._read = function() {
    this.chainName = new DistributedDbVersionName(this._io, this, null);
    this.distributedDbVersion = new DistributedDbVersion(this._io, this, null);
    this.p2pVersion = new P2pVersion(this._io, this, null);
  }

  return NetworkVersion;
})();
return NetworkVersion;
}));
