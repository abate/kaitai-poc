// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id005Psbabym1BlockHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 005-PsBabyM1.block_header
 */

var Id005Psbabym1BlockHeader = (function() {
  Id005Psbabym1BlockHeader.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id005Psbabym1BlockHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1BlockHeader.prototype._read = function() {
    this.id005Psbabym1BlockHeaderAlphaFullHeader = new Id005Psbabym1BlockHeaderAlphaFullHeader(this._io, this, this._root);
  }

  var Id005Psbabym1BlockHeaderAlphaFullHeader = Id005Psbabym1BlockHeader.Id005Psbabym1BlockHeaderAlphaFullHeader = (function() {
    function Id005Psbabym1BlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1BlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id005Psbabym1BlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id005Psbabym1BlockHeaderAlphaSignedContents = new Id005Psbabym1BlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id005Psbabym1BlockHeaderAlphaFullHeader;
  })();

  var Id005Psbabym1BlockHeaderAlphaSignedContents = Id005Psbabym1BlockHeader.Id005Psbabym1BlockHeaderAlphaSignedContents = (function() {
    function Id005Psbabym1BlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1BlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id005Psbabym1BlockHeaderAlphaUnsignedContents = new Id005Psbabym1BlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id005Psbabym1BlockHeaderAlphaSignedContents;
  })();

  var Id005Psbabym1BlockHeaderAlphaUnsignedContents = Id005Psbabym1BlockHeader.Id005Psbabym1BlockHeaderAlphaUnsignedContents = (function() {
    function Id005Psbabym1BlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1BlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id005Psbabym1BlockHeader.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
    }

    return Id005Psbabym1BlockHeaderAlphaUnsignedContents;
  })();

  return Id005Psbabym1BlockHeader;
})();
return Id005Psbabym1BlockHeader;
}));
