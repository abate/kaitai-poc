// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './TimestampProtocol'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./TimestampProtocol'));
  } else {
    root.Id014PtkathmaTimestamp = factory(root.KaitaiStream, root.TimestampProtocol);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, TimestampProtocol) {
/**
 * Encoding id: 014-PtKathma.timestamp
 */

var Id014PtkathmaTimestamp = (function() {
  function Id014PtkathmaTimestamp(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id014PtkathmaTimestamp.prototype._read = function() {
    this.id014PtkathmaTimestamp = new TimestampProtocol(this._io, this, null);
  }

  return Id014PtkathmaTimestamp;
})();
return Id014PtkathmaTimestamp;
}));
