// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id013PtjakartConstantsParametric = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 013-PtJakart.constants.parametric
 */

var Id013PtjakartConstantsParametric = (function() {
  Id013PtjakartConstantsParametric.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id013PtjakartConstantsParametric(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id013PtjakartConstantsParametric.prototype._read = function() {
    this.preservedCycles = this._io.readU1();
    this.blocksPerCycle = this._io.readS4be();
    this.blocksPerCommitment = this._io.readS4be();
    this.blocksPerStakeSnapshot = this._io.readS4be();
    this.cyclesPerVotingPeriod = this._io.readS4be();
    this.hardGasLimitPerOperation = new Z(this._io, this, this._root);
    this.hardGasLimitPerBlock = new Z(this._io, this, this._root);
    this.proofOfWorkThreshold = this._io.readS8be();
    this.tokensPerRoll = new Id013PtjakartMutez(this._io, this, this._root);
    this.seedNonceRevelationTip = new Id013PtjakartMutez(this._io, this, this._root);
    this.originationSize = new Int31(this._io, this, this._root);
    this.bakingRewardFixedPortion = new Id013PtjakartMutez(this._io, this, this._root);
    this.bakingRewardBonusPerSlot = new Id013PtjakartMutez(this._io, this, this._root);
    this.endorsingRewardPerSlot = new Id013PtjakartMutez(this._io, this, this._root);
    this.costPerByte = new Id013PtjakartMutez(this._io, this, this._root);
    this.hardStorageLimitPerOperation = new Z(this._io, this, this._root);
    this.quorumMin = this._io.readS4be();
    this.quorumMax = this._io.readS4be();
    this.minProposalQuorum = this._io.readS4be();
    this.liquidityBakingSubsidy = new Id013PtjakartMutez(this._io, this, this._root);
    this.liquidityBakingSunsetLevel = this._io.readS4be();
    this.liquidityBakingToggleEmaThreshold = this._io.readS4be();
    this.maxOperationsTimeToLive = this._io.readS2be();
    this.minimalBlockDelay = this._io.readS8be();
    this.delayIncrementPerRound = this._io.readS8be();
    this.consensusCommitteeSize = new Int31(this._io, this, this._root);
    this.consensusThreshold = new Int31(this._io, this, this._root);
    this.minimalParticipationRatio = new MinimalParticipationRatio(this._io, this, this._root);
    this.maxSlashingPeriod = new Int31(this._io, this, this._root);
    this.frozenDepositsPercentage = new Int31(this._io, this, this._root);
    this.doubleBakingPunishment = new Id013PtjakartMutez(this._io, this, this._root);
    this.ratioOfFrozenDepositsSlashedPerDoubleEndorsement = new RatioOfFrozenDepositsSlashedPerDoubleEndorsement(this._io, this, this._root);
    this.initialSeedTag = this._io.readU1();
    if (this.initialSeedTag == Id013PtjakartConstantsParametric.Bool.TRUE) {
      this.initialSeed = this._io.readBytes(32);
    }
    this.cacheScriptSize = new Int31(this._io, this, this._root);
    this.cacheStakeDistributionCycles = this._io.readS1();
    this.cacheSamplerStateCycles = this._io.readS1();
    this.txRollupEnable = this._io.readU1();
    this.txRollupOriginationSize = new Int31(this._io, this, this._root);
    this.txRollupHardSizeLimitPerInbox = new Int31(this._io, this, this._root);
    this.txRollupHardSizeLimitPerMessage = new Int31(this._io, this, this._root);
    this.txRollupMaxWithdrawalsPerBatch = new Int31(this._io, this, this._root);
    this.txRollupCommitmentBond = new Id013PtjakartMutez(this._io, this, this._root);
    this.txRollupFinalityPeriod = new Int31(this._io, this, this._root);
    this.txRollupWithdrawPeriod = new Int31(this._io, this, this._root);
    this.txRollupMaxInboxesCount = new Int31(this._io, this, this._root);
    this.txRollupMaxMessagesPerInbox = new Int31(this._io, this, this._root);
    this.txRollupMaxCommitmentsCount = new Int31(this._io, this, this._root);
    this.txRollupCostPerByteEmaFactor = new Int31(this._io, this, this._root);
    this.txRollupMaxTicketPayloadSize = new Int31(this._io, this, this._root);
    this.txRollupRejectionMaxProofSize = new Int31(this._io, this, this._root);
    this.txRollupSunsetLevel = this._io.readS4be();
    this.scRollupEnable = this._io.readU1();
    this.scRollupOriginationSize = new Int31(this._io, this, this._root);
    this.scRollupChallengeWindowInBlocks = new Int31(this._io, this, this._root);
    this.scRollupMaxAvailableMessages = new Int31(this._io, this, this._root);
  }

  var RatioOfFrozenDepositsSlashedPerDoubleEndorsement = Id013PtjakartConstantsParametric.RatioOfFrozenDepositsSlashedPerDoubleEndorsement = (function() {
    function RatioOfFrozenDepositsSlashedPerDoubleEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RatioOfFrozenDepositsSlashedPerDoubleEndorsement.prototype._read = function() {
      this.numerator = this._io.readU2be();
      this.denominator = this._io.readU2be();
    }

    return RatioOfFrozenDepositsSlashedPerDoubleEndorsement;
  })();

  var N = Id013PtjakartConstantsParametric.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var MinimalParticipationRatio = Id013PtjakartConstantsParametric.MinimalParticipationRatio = (function() {
    function MinimalParticipationRatio(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MinimalParticipationRatio.prototype._read = function() {
      this.numerator = this._io.readU2be();
      this.denominator = this._io.readU2be();
    }

    return MinimalParticipationRatio;
  })();

  var Id013PtjakartMutez = Id013PtjakartConstantsParametric.Id013PtjakartMutez = (function() {
    function Id013PtjakartMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartMutez.prototype._read = function() {
      this.id013PtjakartMutez = new N(this._io, this, this._root);
    }

    return Id013PtjakartMutez;
  })();

  var Int31 = Id013PtjakartConstantsParametric.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id013PtjakartConstantsParametric.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Z = Id013PtjakartConstantsParametric.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  return Id013PtjakartConstantsParametric;
})();
return Id013PtjakartConstantsParametric;
}));
