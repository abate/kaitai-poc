// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './OperationShellHeader', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./OperationShellHeader'), require('./BlockHeaderShell'));
  } else {
    root.Id012PsithacaOperation = factory(root.KaitaiStream, root.OperationShellHeader, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, OperationShellHeader, BlockHeaderShell) {
/**
 * Encoding id: 012-Psithaca.operation
 */

var Id012PsithacaOperation = (function() {
  Id012PsithacaOperation.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id012PsithacaOperation.Id012PsithacaEntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    255: "NAMED",
  });

  Id012PsithacaOperation.Id012PsithacaContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id012PsithacaOperation.Id012PsithacaInlinedEndorsementMempoolContentsTag = Object.freeze({
    ENDORSEMENT: 21,

    21: "ENDORSEMENT",
  });

  Id012PsithacaOperation.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id012PsithacaOperation.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id012PsithacaOperation.Id012PsithacaInlinedPreendorsementContentsTag = Object.freeze({
    PREENDORSEMENT: 20,

    20: "PREENDORSEMENT",
  });

  Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag = Object.freeze({
    SEED_NONCE_REVELATION: 1,
    DOUBLE_ENDORSEMENT_EVIDENCE: 2,
    DOUBLE_BAKING_EVIDENCE: 3,
    ACTIVATE_ACCOUNT: 4,
    PROPOSALS: 5,
    BALLOT: 6,
    DOUBLE_PREENDORSEMENT_EVIDENCE: 7,
    FAILING_NOOP: 17,
    PREENDORSEMENT: 20,
    ENDORSEMENT: 21,
    REVEAL: 107,
    TRANSACTION: 108,
    ORIGINATION: 109,
    DELEGATION: 110,
    REGISTER_GLOBAL_CONSTANT: 111,
    SET_DEPOSITS_LIMIT: 112,

    1: "SEED_NONCE_REVELATION",
    2: "DOUBLE_ENDORSEMENT_EVIDENCE",
    3: "DOUBLE_BAKING_EVIDENCE",
    4: "ACTIVATE_ACCOUNT",
    5: "PROPOSALS",
    6: "BALLOT",
    7: "DOUBLE_PREENDORSEMENT_EVIDENCE",
    17: "FAILING_NOOP",
    20: "PREENDORSEMENT",
    21: "ENDORSEMENT",
    107: "REVEAL",
    108: "TRANSACTION",
    109: "ORIGINATION",
    110: "DELEGATION",
    111: "REGISTER_GLOBAL_CONSTANT",
    112: "SET_DEPOSITS_LIMIT",
  });

  function Id012PsithacaOperation(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id012PsithacaOperation.prototype._read = function() {
    this.id012PsithacaOperation = new OperationShellHeader(this._io, this, null);
    this.id012PsithacaOperationAlphaContentsAndSignature = new Id012PsithacaOperationAlphaContentsAndSignature(this._io, this, this._root);
  }

  var Op20 = Id012PsithacaOperation.Op20 = (function() {
    function Op20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op20.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_0/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op2(_io__raw_op2, this, this._root);
    }

    return Op20;
  })();

  var ActivateAccount = Id012PsithacaOperation.ActivateAccount = (function() {
    function ActivateAccount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ActivateAccount.prototype._read = function() {
      this.pkh = this._io.readBytes(20);
      this.secret = this._io.readBytes(20);
    }

    return ActivateAccount;
  })();

  var Id012PsithacaContractId = Id012PsithacaOperation.Id012PsithacaContractId = (function() {
    function Id012PsithacaContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaContractId.prototype._read = function() {
      this.id012PsithacaContractIdTag = this._io.readU1();
      if (this.id012PsithacaContractIdTag == Id012PsithacaOperation.Id012PsithacaContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id012PsithacaContractIdTag == Id012PsithacaOperation.Id012PsithacaContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id012PsithacaContractId;
  })();

  var DoubleEndorsementEvidence = Id012PsithacaOperation.DoubleEndorsementEvidence = (function() {
    function DoubleEndorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleEndorsementEvidence.prototype._read = function() {
      this.op1 = new Op10(this._io, this, this._root);
      this.op2 = new Op20(this._io, this, this._root);
    }

    return DoubleEndorsementEvidence;
  })();

  var Id012PsithacaScriptedContracts = Id012PsithacaOperation.Id012PsithacaScriptedContracts = (function() {
    function Id012PsithacaScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id012PsithacaScriptedContracts;
  })();

  var Id012PsithacaEntrypoint = Id012PsithacaOperation.Id012PsithacaEntrypoint = (function() {
    function Id012PsithacaEntrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaEntrypoint.prototype._read = function() {
      this.id012PsithacaEntrypointTag = this._io.readU1();
      if (this.id012PsithacaEntrypointTag == Id012PsithacaOperation.Id012PsithacaEntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id012PsithacaEntrypoint;
  })();

  var Originated = Id012PsithacaOperation.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Id012PsithacaInlinedEndorsementMempoolContents = Id012PsithacaOperation.Id012PsithacaInlinedEndorsementMempoolContents = (function() {
    function Id012PsithacaInlinedEndorsementMempoolContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaInlinedEndorsementMempoolContents.prototype._read = function() {
      this.id012PsithacaInlinedEndorsementMempoolContentsTag = this._io.readU1();
      if (this.id012PsithacaInlinedEndorsementMempoolContentsTag == Id012PsithacaOperation.Id012PsithacaInlinedEndorsementMempoolContentsTag.ENDORSEMENT) {
        this.endorsement = new Endorsement(this._io, this, this._root);
      }
    }

    return Id012PsithacaInlinedEndorsementMempoolContents;
  })();

  var N = Id012PsithacaOperation.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Endorsement = Id012PsithacaOperation.Endorsement = (function() {
    function Endorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Endorsement.prototype._read = function() {
      this.slot = this._io.readU2be();
      this.level = this._io.readS4be();
      this.round = this._io.readS4be();
      this.blockPayloadHash = this._io.readBytes(32);
    }

    return Endorsement;
  })();

  var Proposals0 = Id012PsithacaOperation.Proposals0 = (function() {
    function Proposals0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals0.prototype._read = function() {
      this.lenProposals = this._io.readU4be();
      if (!(this.lenProposals <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenProposals, this._io, "/types/proposals_0/seq/0");
      }
      this._raw_proposals = this._io.readBytes(this.lenProposals);
      var _io__raw_proposals = new KaitaiStream(this._raw_proposals);
      this.proposals = new Proposals(_io__raw_proposals, this, this._root);
    }

    return Proposals0;
  })();

  var Op12 = Id012PsithacaOperation.Op12 = (function() {
    function Op12(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op12.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_2/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op11(_io__raw_op1, this, this._root);
    }

    return Op12;
  })();

  var Id012PsithacaBlockHeaderAlphaSignedContents = Id012PsithacaOperation.Id012PsithacaBlockHeaderAlphaSignedContents = (function() {
    function Id012PsithacaBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id012PsithacaBlockHeaderAlphaUnsignedContents = new Id012PsithacaBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id012PsithacaBlockHeaderAlphaSignedContents;
  })();

  var Id012PsithacaBlockHeaderAlphaFullHeader = Id012PsithacaOperation.Id012PsithacaBlockHeaderAlphaFullHeader = (function() {
    function Id012PsithacaBlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaBlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id012PsithacaBlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id012PsithacaBlockHeaderAlphaSignedContents = new Id012PsithacaBlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id012PsithacaBlockHeaderAlphaFullHeader;
  })();

  var Reveal = Id012PsithacaOperation.Reveal = (function() {
    function Reveal(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Reveal.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id012PsithacaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.publicKey = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Reveal;
  })();

  var Id012PsithacaMutez = Id012PsithacaOperation.Id012PsithacaMutez = (function() {
    function Id012PsithacaMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaMutez.prototype._read = function() {
      this.id012PsithacaMutez = new N(this._io, this, this._root);
    }

    return Id012PsithacaMutez;
  })();

  var SeedNonceRevelation = Id012PsithacaOperation.SeedNonceRevelation = (function() {
    function SeedNonceRevelation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SeedNonceRevelation.prototype._read = function() {
      this.level = this._io.readS4be();
      this.nonce = this._io.readBytes(32);
    }

    return SeedNonceRevelation;
  })();

  var Preendorsement = Id012PsithacaOperation.Preendorsement = (function() {
    function Preendorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Preendorsement.prototype._read = function() {
      this.slot = this._io.readU2be();
      this.level = this._io.readS4be();
      this.round = this._io.readS4be();
      this.blockPayloadHash = this._io.readBytes(32);
    }

    return Preendorsement;
  })();

  var DoubleBakingEvidence = Id012PsithacaOperation.DoubleBakingEvidence = (function() {
    function DoubleBakingEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleBakingEvidence.prototype._read = function() {
      this.bh1 = new Bh10(this._io, this, this._root);
      this.bh2 = new Bh20(this._io, this, this._root);
    }

    return DoubleBakingEvidence;
  })();

  var PublicKey = Id012PsithacaOperation.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id012PsithacaOperation.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id012PsithacaOperation.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id012PsithacaOperation.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var Bh2 = Id012PsithacaOperation.Bh2 = (function() {
    function Bh2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh2.prototype._read = function() {
      this.id012PsithacaBlockHeaderAlphaFullHeader = new Id012PsithacaBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh2;
  })();

  var Named = Id012PsithacaOperation.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Bh20 = Id012PsithacaOperation.Bh20 = (function() {
    function Bh20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh20.prototype._read = function() {
      this.lenBh2 = this._io.readU4be();
      if (!(this.lenBh2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh2, this._io, "/types/bh2_0/seq/0");
      }
      this._raw_bh2 = this._io.readBytes(this.lenBh2);
      var _io__raw_bh2 = new KaitaiStream(this._raw_bh2);
      this.bh2 = new Bh2(_io__raw_bh2, this, this._root);
    }

    return Bh20;
  })();

  var Delegation = Id012PsithacaOperation.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id012PsithacaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id012PsithacaOperation.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var RegisterGlobalConstant = Id012PsithacaOperation.RegisterGlobalConstant = (function() {
    function RegisterGlobalConstant(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RegisterGlobalConstant.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id012PsithacaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return RegisterGlobalConstant;
  })();

  var ContentsEntries = Id012PsithacaOperation.ContentsEntries = (function() {
    function ContentsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ContentsEntries.prototype._read = function() {
      this.id012PsithacaOperationAlphaContents = new Id012PsithacaOperationAlphaContents(this._io, this, this._root);
    }

    return ContentsEntries;
  })();

  var Id012PsithacaInlinedPreendorsementContents = Id012PsithacaOperation.Id012PsithacaInlinedPreendorsementContents = (function() {
    function Id012PsithacaInlinedPreendorsementContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaInlinedPreendorsementContents.prototype._read = function() {
      this.id012PsithacaInlinedPreendorsementContentsTag = this._io.readU1();
      if (this.id012PsithacaInlinedPreendorsementContentsTag == Id012PsithacaOperation.Id012PsithacaInlinedPreendorsementContentsTag.PREENDORSEMENT) {
        this.preendorsement = new Preendorsement(this._io, this, this._root);
      }
    }

    return Id012PsithacaInlinedPreendorsementContents;
  })();

  var Bh10 = Id012PsithacaOperation.Bh10 = (function() {
    function Bh10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh10.prototype._read = function() {
      this.lenBh1 = this._io.readU4be();
      if (!(this.lenBh1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh1, this._io, "/types/bh1_0/seq/0");
      }
      this._raw_bh1 = this._io.readBytes(this.lenBh1);
      var _io__raw_bh1 = new KaitaiStream(this._raw_bh1);
      this.bh1 = new Bh1(_io__raw_bh1, this, this._root);
    }

    return Bh10;
  })();

  var Op22 = Id012PsithacaOperation.Op22 = (function() {
    function Op22(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op22.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_2/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op21(_io__raw_op2, this, this._root);
    }

    return Op22;
  })();

  var BytesDynUint30 = Id012PsithacaOperation.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Bh1 = Id012PsithacaOperation.Bh1 = (function() {
    function Bh1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh1.prototype._read = function() {
      this.id012PsithacaBlockHeaderAlphaFullHeader = new Id012PsithacaBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh1;
  })();

  var SetDepositsLimit = Id012PsithacaOperation.SetDepositsLimit = (function() {
    function SetDepositsLimit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SetDepositsLimit.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id012PsithacaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.limitTag = this._io.readU1();
      if (this.limitTag == Id012PsithacaOperation.Bool.TRUE) {
        this.limit = new Id012PsithacaMutez(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return SetDepositsLimit;
  })();

  var Id012PsithacaInlinedEndorsement = Id012PsithacaOperation.Id012PsithacaInlinedEndorsement = (function() {
    function Id012PsithacaInlinedEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaInlinedEndorsement.prototype._read = function() {
      this.id012PsithacaInlinedEndorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id012PsithacaInlinedEndorsementMempoolContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id012PsithacaOperation.Bool.TRUE) {
        this.signature = this._io.readBytes(64);
      }
    }

    return Id012PsithacaInlinedEndorsement;
  })();

  var Op10 = Id012PsithacaOperation.Op10 = (function() {
    function Op10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op10.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_0/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op1(_io__raw_op1, this, this._root);
    }

    return Op10;
  })();

  var Op21 = Id012PsithacaOperation.Op21 = (function() {
    function Op21(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op21.prototype._read = function() {
      this.id012PsithacaInlinedPreendorsement = new Id012PsithacaInlinedPreendorsement(this._io, this, this._root);
    }

    return Op21;
  })();

  var Origination = Id012PsithacaOperation.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id012PsithacaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.balance = new Id012PsithacaMutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id012PsithacaOperation.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id012PsithacaScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var Op2 = Id012PsithacaOperation.Op2 = (function() {
    function Op2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op2.prototype._read = function() {
      this.id012PsithacaInlinedEndorsement = new Id012PsithacaInlinedEndorsement(this._io, this, this._root);
    }

    return Op2;
  })();

  var Id012PsithacaOperationAlphaContentsAndSignature = Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsAndSignature = (function() {
    function Id012PsithacaOperationAlphaContentsAndSignature(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaOperationAlphaContentsAndSignature.prototype._read = function() {
      this.contents = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.contents.push(new ContentsEntries(this._io, this, this._root));
        i++;
      }
      this.signature = this._io.readBytes(64);
    }

    return Id012PsithacaOperationAlphaContentsAndSignature;
  })();

  var NChunk = Id012PsithacaOperation.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Id012PsithacaInlinedPreendorsement = Id012PsithacaOperation.Id012PsithacaInlinedPreendorsement = (function() {
    function Id012PsithacaInlinedPreendorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaInlinedPreendorsement.prototype._read = function() {
      this.id012PsithacaInlinedPreendorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id012PsithacaInlinedPreendorsementContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id012PsithacaOperation.Bool.TRUE) {
        this.signature = this._io.readBytes(64);
      }
    }

    return Id012PsithacaInlinedPreendorsement;
  })();

  var Named0 = Id012PsithacaOperation.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id012PsithacaOperation.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id012PsithacaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.amount = new Id012PsithacaMutez(this._io, this, this._root);
      this.destination = new Id012PsithacaContractId(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id012PsithacaOperation.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Transaction;
  })();

  var Id012PsithacaOperationAlphaContents = Id012PsithacaOperation.Id012PsithacaOperationAlphaContents = (function() {
    function Id012PsithacaOperationAlphaContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaOperationAlphaContents.prototype._read = function() {
      this.id012PsithacaOperationAlphaContentsTag = this._io.readU1();
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.ENDORSEMENT) {
        this.endorsement = new Endorsement(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.PREENDORSEMENT) {
        this.preendorsement = new Preendorsement(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.SEED_NONCE_REVELATION) {
        this.seedNonceRevelation = new SeedNonceRevelation(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.DOUBLE_ENDORSEMENT_EVIDENCE) {
        this.doubleEndorsementEvidence = new DoubleEndorsementEvidence(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.DOUBLE_PREENDORSEMENT_EVIDENCE) {
        this.doublePreendorsementEvidence = new DoublePreendorsementEvidence(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.DOUBLE_BAKING_EVIDENCE) {
        this.doubleBakingEvidence = new DoubleBakingEvidence(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.ACTIVATE_ACCOUNT) {
        this.activateAccount = new ActivateAccount(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.PROPOSALS) {
        this.proposals = new Proposals1(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.BALLOT) {
        this.ballot = new Ballot(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.REVEAL) {
        this.reveal = new Reveal(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.SET_DEPOSITS_LIMIT) {
        this.setDepositsLimit = new SetDepositsLimit(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.FAILING_NOOP) {
        this.failingNoop = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationAlphaContentsTag == Id012PsithacaOperation.Id012PsithacaOperationAlphaContentsTag.REGISTER_GLOBAL_CONSTANT) {
        this.registerGlobalConstant = new RegisterGlobalConstant(this._io, this, this._root);
      }
    }

    return Id012PsithacaOperationAlphaContents;
  })();

  var Parameters = Id012PsithacaOperation.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id012PsithacaEntrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var Proposals1 = Id012PsithacaOperation.Proposals1 = (function() {
    function Proposals1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals1.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposals = new Proposals0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Proposals1;
  })();

  var Id012PsithacaBlockHeaderAlphaUnsignedContents = Id012PsithacaOperation.Id012PsithacaBlockHeaderAlphaUnsignedContents = (function() {
    function Id012PsithacaBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id012PsithacaOperation.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingEscapeVote = this._io.readU1();
    }

    return Id012PsithacaBlockHeaderAlphaUnsignedContents;
  })();

  var Ballot = Id012PsithacaOperation.Ballot = (function() {
    function Ballot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Ballot.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposal = this._io.readBytes(32);
      this.ballot = this._io.readS1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Ballot;
  })();

  var PublicKeyHash = Id012PsithacaOperation.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id012PsithacaOperation.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id012PsithacaOperation.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id012PsithacaOperation.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Op11 = Id012PsithacaOperation.Op11 = (function() {
    function Op11(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op11.prototype._read = function() {
      this.id012PsithacaInlinedPreendorsement = new Id012PsithacaInlinedPreendorsement(this._io, this, this._root);
    }

    return Op11;
  })();

  var Proposals = Id012PsithacaOperation.Proposals = (function() {
    function Proposals(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals.prototype._read = function() {
      this.proposalsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.proposalsEntries.push(new ProposalsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Proposals;
  })();

  var ProposalsEntries = Id012PsithacaOperation.ProposalsEntries = (function() {
    function ProposalsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ProposalsEntries.prototype._read = function() {
      this.protocolHash = this._io.readBytes(32);
    }

    return ProposalsEntries;
  })();

  var Op1 = Id012PsithacaOperation.Op1 = (function() {
    function Op1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op1.prototype._read = function() {
      this.id012PsithacaInlinedEndorsement = new Id012PsithacaInlinedEndorsement(this._io, this, this._root);
    }

    return Op1;
  })();

  var DoublePreendorsementEvidence = Id012PsithacaOperation.DoublePreendorsementEvidence = (function() {
    function DoublePreendorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoublePreendorsementEvidence.prototype._read = function() {
      this.op1 = new Op12(this._io, this, this._root);
      this.op2 = new Op22(this._io, this, this._root);
    }

    return DoublePreendorsementEvidence;
  })();

  return Id012PsithacaOperation;
})();
return Id012PsithacaOperation;
}));
