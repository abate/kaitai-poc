// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaBlockHeaderProtocolData = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.block_header.protocol_data
 */

var AlphaBlockHeaderProtocolData = (function() {
  AlphaBlockHeaderProtocolData.AlphaPerBlockVotesTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
  });

  AlphaBlockHeaderProtocolData.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function AlphaBlockHeaderProtocolData(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaBlockHeaderProtocolData.prototype._read = function() {
    this.alphaBlockHeaderAlphaSignedContents = new AlphaBlockHeaderAlphaSignedContents(this._io, this, this._root);
  }

  var AlphaBlockHeaderAlphaSignedContents = AlphaBlockHeaderProtocolData.AlphaBlockHeaderAlphaSignedContents = (function() {
    function AlphaBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.alphaBlockHeaderAlphaUnsignedContents = new AlphaBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytesFull();
    }

    return AlphaBlockHeaderAlphaSignedContents;
  })();

  var AlphaBlockHeaderAlphaUnsignedContents = AlphaBlockHeaderProtocolData.AlphaBlockHeaderAlphaUnsignedContents = (function() {
    function AlphaBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == AlphaBlockHeaderProtocolData.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.perBlockVotes = new AlphaPerBlockVotes(this._io, this, this._root);
    }

    return AlphaBlockHeaderAlphaUnsignedContents;
  })();

  var AlphaPerBlockVotes = AlphaBlockHeaderProtocolData.AlphaPerBlockVotes = (function() {
    function AlphaPerBlockVotes(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaPerBlockVotes.prototype._read = function() {
      this.alphaPerBlockVotesTag = this._io.readU1();
    }

    return AlphaPerBlockVotes;
  })();

  return AlphaBlockHeaderProtocolData;
})();
return AlphaBlockHeaderProtocolData;
}));
