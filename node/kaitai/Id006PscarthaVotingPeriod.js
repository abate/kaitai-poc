// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id006PscarthaVotingPeriod = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 006-PsCARTHA.voting_period
 */

var Id006PscarthaVotingPeriod = (function() {
  function Id006PscarthaVotingPeriod(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaVotingPeriod.prototype._read = function() {
    this.id006PscarthaVotingPeriod = this._io.readS4be();
  }

  return Id006PscarthaVotingPeriod;
})();
return Id006PscarthaVotingPeriod;
}));
