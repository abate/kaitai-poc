// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id008Ptedo2zkVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 008-PtEdo2Zk.vote.listings
 */

var Id008Ptedo2zkVoteListings = (function() {
  Id008Ptedo2zkVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id008Ptedo2zkVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkVoteListings.prototype._read = function() {
    this.lenId008Ptedo2zkVoteListings = this._io.readU4be();
    if (!(this.lenId008Ptedo2zkVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId008Ptedo2zkVoteListings, this._io, "/seq/0");
    }
    this._raw_id008Ptedo2zkVoteListings = this._io.readBytes(this.lenId008Ptedo2zkVoteListings);
    var _io__raw_id008Ptedo2zkVoteListings = new KaitaiStream(this._raw_id008Ptedo2zkVoteListings);
    this.id008Ptedo2zkVoteListings = new Id008Ptedo2zkVoteListings(_io__raw_id008Ptedo2zkVoteListings, this, this._root);
  }

  var Id008Ptedo2zkVoteListings = Id008Ptedo2zkVoteListings.Id008Ptedo2zkVoteListings = (function() {
    function Id008Ptedo2zkVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkVoteListings.prototype._read = function() {
      this.id008Ptedo2zkVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id008Ptedo2zkVoteListingsEntries.push(new Id008Ptedo2zkVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id008Ptedo2zkVoteListings;
  })();

  var Id008Ptedo2zkVoteListingsEntries = Id008Ptedo2zkVoteListings.Id008Ptedo2zkVoteListingsEntries = (function() {
    function Id008Ptedo2zkVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.rolls = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id008Ptedo2zkVoteListingsEntries;
  })();

  var PublicKeyHash = Id008Ptedo2zkVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id008Ptedo2zkVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id008Ptedo2zkVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id008Ptedo2zkVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id008Ptedo2zkVoteListings;
})();
return Id008Ptedo2zkVoteListings;
}));
