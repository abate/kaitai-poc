// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaSmartRollupKind = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.smart_rollup.kind
 */

var AlphaSmartRollupKind = (function() {
  AlphaSmartRollupKind.AlphaSmartRollupKind = Object.freeze({
    ARITH: 0,
    WASM_2_0_0: 1,
    RISCV: 2,

    0: "ARITH",
    1: "WASM_2_0_0",
    2: "RISCV",
  });

  function AlphaSmartRollupKind(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaSmartRollupKind.prototype._read = function() {
    this.alphaSmartRollupKind = this._io.readU1();
  }

  return AlphaSmartRollupKind;
})();
return AlphaSmartRollupKind;
}));
