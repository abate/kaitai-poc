// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeader'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeader'));
  } else {
    root.Id019PtparisaBlockHeaderRaw = factory(root.KaitaiStream, root.BlockHeader);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeader) {
/**
 * Encoding id: 019-PtParisA.block_header.raw
 */

var Id019PtparisaBlockHeaderRaw = (function() {
  function Id019PtparisaBlockHeaderRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaBlockHeaderRaw.prototype._read = function() {
    this.id019PtparisaBlockHeaderRaw = new BlockHeader(this._io, this, null);
  }

  return Id019PtparisaBlockHeaderRaw;
})();
return Id019PtparisaBlockHeaderRaw;
}));
