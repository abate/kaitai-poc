// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaSmartRollupCommmitment = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.smart_rollup.commmitment
 */

var Id019PtparisaSmartRollupCommmitment = (function() {
  function Id019PtparisaSmartRollupCommmitment(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaSmartRollupCommmitment.prototype._read = function() {
    this.compressedState = this._io.readBytes(32);
    this.inboxLevel = this._io.readS4be();
    this.predecessor = this._io.readBytes(32);
    this.numberOfTicks = this._io.readS8be();
  }

  return Id019PtparisaSmartRollupCommmitment;
})();
return Id019PtparisaSmartRollupCommmitment;
}));
