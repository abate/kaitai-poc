// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id014PtkathmaVoteBallots = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 014-PtKathma.vote.ballots
 */

var Id014PtkathmaVoteBallots = (function() {
  function Id014PtkathmaVoteBallots(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id014PtkathmaVoteBallots.prototype._read = function() {
    this.yay = this._io.readS8be();
    this.nay = this._io.readS8be();
    this.pass = this._io.readS8be();
  }

  return Id014PtkathmaVoteBallots;
})();
return Id014PtkathmaVoteBallots;
}));
