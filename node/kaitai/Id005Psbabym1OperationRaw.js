// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './Operation'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./Operation'));
  } else {
    root.Id005Psbabym1OperationRaw = factory(root.KaitaiStream, root.Operation);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, Operation) {
/**
 * Encoding id: 005-PsBabyM1.operation.raw
 */

var Id005Psbabym1OperationRaw = (function() {
  function Id005Psbabym1OperationRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1OperationRaw.prototype._read = function() {
    this.id005Psbabym1OperationRaw = new Operation(this._io, this, null);
  }

  return Id005Psbabym1OperationRaw;
})();
return Id005Psbabym1OperationRaw;
}));
