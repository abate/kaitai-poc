// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.vote.listings
 */

var AlphaVoteListings = (function() {
  AlphaVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function AlphaVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaVoteListings.prototype._read = function() {
    this.lenAlphaVoteListings = this._io.readU4be();
    if (!(this.lenAlphaVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenAlphaVoteListings, this._io, "/seq/0");
    }
    this._raw_alphaVoteListings = this._io.readBytes(this.lenAlphaVoteListings);
    var _io__raw_alphaVoteListings = new KaitaiStream(this._raw_alphaVoteListings);
    this.alphaVoteListings = new AlphaVoteListings(_io__raw_alphaVoteListings, this, this._root);
  }

  var AlphaVoteListings = AlphaVoteListings.AlphaVoteListings = (function() {
    function AlphaVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaVoteListings.prototype._read = function() {
      this.alphaVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.alphaVoteListingsEntries.push(new AlphaVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return AlphaVoteListings;
  })();

  var AlphaVoteListingsEntries = AlphaVoteListings.AlphaVoteListingsEntries = (function() {
    function AlphaVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.votingPower = this._io.readS8be();
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return AlphaVoteListingsEntries;
  })();

  var PublicKeyHash = AlphaVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == AlphaVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaVoteListings.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return AlphaVoteListings;
})();
return AlphaVoteListings;
}));
