// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './Operation'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./Operation'));
  } else {
    root.Id015PtlimaptOperationRaw = factory(root.KaitaiStream, root.Operation);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, Operation) {
/**
 * Encoding id: 015-PtLimaPt.operation.raw
 */

var Id015PtlimaptOperationRaw = (function() {
  function Id015PtlimaptOperationRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id015PtlimaptOperationRaw.prototype._read = function() {
    this.id015PtlimaptOperationRaw = new Operation(this._io, this, null);
  }

  return Id015PtlimaptOperationRaw;
})();
return Id015PtlimaptOperationRaw;
}));
