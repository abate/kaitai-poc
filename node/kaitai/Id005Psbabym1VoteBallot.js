// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id005Psbabym1VoteBallot = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 005-PsBabyM1.vote.ballot
 */

var Id005Psbabym1VoteBallot = (function() {
  function Id005Psbabym1VoteBallot(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1VoteBallot.prototype._read = function() {
    this.id005Psbabym1VoteBallot = this._io.readS1();
  }

  return Id005Psbabym1VoteBallot;
})();
return Id005Psbabym1VoteBallot;
}));
