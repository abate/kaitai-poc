// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './TimestampProtocol'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./TimestampProtocol'));
  } else {
    root.Id018ProxfordTimestamp = factory(root.KaitaiStream, root.TimestampProtocol);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, TimestampProtocol) {
/**
 * Encoding id: 018-Proxford.timestamp
 */

var Id018ProxfordTimestamp = (function() {
  function Id018ProxfordTimestamp(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id018ProxfordTimestamp.prototype._read = function() {
    this.id018ProxfordTimestamp = new TimestampProtocol(this._io, this, null);
  }

  return Id018ProxfordTimestamp;
})();
return Id018ProxfordTimestamp;
}));
