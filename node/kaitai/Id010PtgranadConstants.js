// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id010PtgranadConstants = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 010-PtGRANAD.constants
 */

var Id010PtgranadConstants = (function() {
  function Id010PtgranadConstants(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id010PtgranadConstants.prototype._read = function() {
    this.proofOfWorkNonceSize = this._io.readU1();
    this.nonceLength = this._io.readU1();
    this.maxAnonOpsPerBlock = this._io.readU1();
    this.maxOperationDataLength = new Int31(this._io, this, this._root);
    this.maxProposalsPerDelegate = this._io.readU1();
    this.preservedCycles = this._io.readU1();
    this.blocksPerCycle = this._io.readS4be();
    this.blocksPerCommitment = this._io.readS4be();
    this.blocksPerRollSnapshot = this._io.readS4be();
    this.blocksPerVotingPeriod = this._io.readS4be();
    this.timeBetweenBlocks = new TimeBetweenBlocks0(this._io, this, this._root);
    this.endorsersPerBlock = this._io.readU2be();
    this.hardGasLimitPerOperation = new Z(this._io, this, this._root);
    this.hardGasLimitPerBlock = new Z(this._io, this, this._root);
    this.proofOfWorkThreshold = this._io.readS8be();
    this.tokensPerRoll = new Id010PtgranadMutez(this._io, this, this._root);
    this.michelsonMaximumTypeSize = this._io.readU2be();
    this.seedNonceRevelationTip = new Id010PtgranadMutez(this._io, this, this._root);
    this.originationSize = new Int31(this._io, this, this._root);
    this.blockSecurityDeposit = new Id010PtgranadMutez(this._io, this, this._root);
    this.endorsementSecurityDeposit = new Id010PtgranadMutez(this._io, this, this._root);
    this.bakingRewardPerEndorsement = new BakingRewardPerEndorsement0(this._io, this, this._root);
    this.endorsementReward = new EndorsementReward0(this._io, this, this._root);
    this.costPerByte = new Id010PtgranadMutez(this._io, this, this._root);
    this.hardStorageLimitPerOperation = new Z(this._io, this, this._root);
    this.quorumMin = this._io.readS4be();
    this.quorumMax = this._io.readS4be();
    this.minProposalQuorum = this._io.readS4be();
    this.initialEndorsers = this._io.readU2be();
    this.delayPerMissingEndorsement = this._io.readS8be();
    this.minimalBlockDelay = this._io.readS8be();
    this.liquidityBakingSubsidy = new Id010PtgranadMutez(this._io, this, this._root);
    this.liquidityBakingSunsetLevel = this._io.readS4be();
    this.liquidityBakingEscapeEmaThreshold = this._io.readS4be();
  }

  var TimeBetweenBlocksEntries = Id010PtgranadConstants.TimeBetweenBlocksEntries = (function() {
    function TimeBetweenBlocksEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TimeBetweenBlocksEntries.prototype._read = function() {
      this.timeBetweenBlocksElt = this._io.readS8be();
    }

    return TimeBetweenBlocksEntries;
  })();

  var N = Id010PtgranadConstants.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var TimeBetweenBlocks = Id010PtgranadConstants.TimeBetweenBlocks = (function() {
    function TimeBetweenBlocks(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TimeBetweenBlocks.prototype._read = function() {
      this.timeBetweenBlocksEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.timeBetweenBlocksEntries.push(new TimeBetweenBlocksEntries(this._io, this, this._root));
        i++;
      }
    }

    return TimeBetweenBlocks;
  })();

  var EndorsementReward = Id010PtgranadConstants.EndorsementReward = (function() {
    function EndorsementReward(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    EndorsementReward.prototype._read = function() {
      this.endorsementRewardEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.endorsementRewardEntries.push(new EndorsementRewardEntries(this._io, this, this._root));
        i++;
      }
    }

    return EndorsementReward;
  })();

  var EndorsementReward0 = Id010PtgranadConstants.EndorsementReward0 = (function() {
    function EndorsementReward0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    EndorsementReward0.prototype._read = function() {
      this.lenEndorsementReward = this._io.readU4be();
      if (!(this.lenEndorsementReward <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenEndorsementReward, this._io, "/types/endorsement_reward_0/seq/0");
      }
      this._raw_endorsementReward = this._io.readBytes(this.lenEndorsementReward);
      var _io__raw_endorsementReward = new KaitaiStream(this._raw_endorsementReward);
      this.endorsementReward = new EndorsementReward(_io__raw_endorsementReward, this, this._root);
    }

    return EndorsementReward0;
  })();

  var EndorsementRewardEntries = Id010PtgranadConstants.EndorsementRewardEntries = (function() {
    function EndorsementRewardEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    EndorsementRewardEntries.prototype._read = function() {
      this.id010PtgranadMutez = new Id010PtgranadMutez(this._io, this, this._root);
    }

    return EndorsementRewardEntries;
  })();

  var BakingRewardPerEndorsement0 = Id010PtgranadConstants.BakingRewardPerEndorsement0 = (function() {
    function BakingRewardPerEndorsement0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BakingRewardPerEndorsement0.prototype._read = function() {
      this.lenBakingRewardPerEndorsement = this._io.readU4be();
      if (!(this.lenBakingRewardPerEndorsement <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBakingRewardPerEndorsement, this._io, "/types/baking_reward_per_endorsement_0/seq/0");
      }
      this._raw_bakingRewardPerEndorsement = this._io.readBytes(this.lenBakingRewardPerEndorsement);
      var _io__raw_bakingRewardPerEndorsement = new KaitaiStream(this._raw_bakingRewardPerEndorsement);
      this.bakingRewardPerEndorsement = new BakingRewardPerEndorsement(_io__raw_bakingRewardPerEndorsement, this, this._root);
    }

    return BakingRewardPerEndorsement0;
  })();

  var TimeBetweenBlocks0 = Id010PtgranadConstants.TimeBetweenBlocks0 = (function() {
    function TimeBetweenBlocks0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TimeBetweenBlocks0.prototype._read = function() {
      this.lenTimeBetweenBlocks = this._io.readU4be();
      if (!(this.lenTimeBetweenBlocks <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenTimeBetweenBlocks, this._io, "/types/time_between_blocks_0/seq/0");
      }
      this._raw_timeBetweenBlocks = this._io.readBytes(this.lenTimeBetweenBlocks);
      var _io__raw_timeBetweenBlocks = new KaitaiStream(this._raw_timeBetweenBlocks);
      this.timeBetweenBlocks = new TimeBetweenBlocks(_io__raw_timeBetweenBlocks, this, this._root);
    }

    return TimeBetweenBlocks0;
  })();

  var BakingRewardPerEndorsement = Id010PtgranadConstants.BakingRewardPerEndorsement = (function() {
    function BakingRewardPerEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BakingRewardPerEndorsement.prototype._read = function() {
      this.bakingRewardPerEndorsementEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bakingRewardPerEndorsementEntries.push(new BakingRewardPerEndorsementEntries(this._io, this, this._root));
        i++;
      }
    }

    return BakingRewardPerEndorsement;
  })();

  var Int31 = Id010PtgranadConstants.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id010PtgranadConstants.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Id010PtgranadMutez = Id010PtgranadConstants.Id010PtgranadMutez = (function() {
    function Id010PtgranadMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id010PtgranadMutez.prototype._read = function() {
      this.id010PtgranadMutez = new N(this._io, this, this._root);
    }

    return Id010PtgranadMutez;
  })();

  var Z = Id010PtgranadConstants.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  var BakingRewardPerEndorsementEntries = Id010PtgranadConstants.BakingRewardPerEndorsementEntries = (function() {
    function BakingRewardPerEndorsementEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BakingRewardPerEndorsementEntries.prototype._read = function() {
      this.id010PtgranadMutez = new Id010PtgranadMutez(this._io, this, this._root);
    }

    return BakingRewardPerEndorsementEntries;
  })();

  return Id010PtgranadConstants;
})();
return Id010PtgranadConstants;
}));
