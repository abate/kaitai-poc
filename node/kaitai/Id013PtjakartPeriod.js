// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id013PtjakartPeriod = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 013-PtJakart.period
 */

var Id013PtjakartPeriod = (function() {
  function Id013PtjakartPeriod(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id013PtjakartPeriod.prototype._read = function() {
    this.id013PtjakartPeriod = this._io.readS8be();
  }

  return Id013PtjakartPeriod;
})();
return Id013PtjakartPeriod;
}));
