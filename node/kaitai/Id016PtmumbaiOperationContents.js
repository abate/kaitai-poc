// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './OperationShellHeader', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./OperationShellHeader'), require('./BlockHeaderShell'));
  } else {
    root.Id016PtmumbaiOperationContents = factory(root.KaitaiStream, root.OperationShellHeader, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, OperationShellHeader, BlockHeaderShell) {
/**
 * Encoding id: 016-PtMumbai.operation.contents
 */

var Id016PtmumbaiOperationContents = (function() {
  Id016PtmumbaiOperationContents.RevealProofTag = Object.freeze({
    RAW__DATA__PROOF: 0,
    METADATA__PROOF: 1,
    DAL__PAGE__PROOF: 2,

    0: "RAW__DATA__PROOF",
    1: "METADATA__PROOF",
    2: "DAL__PAGE__PROOF",
  });

  Id016PtmumbaiOperationContents.Id016PtmumbaiInlinedPreendorsementContentsTag = Object.freeze({
    PREENDORSEMENT: 20,

    20: "PREENDORSEMENT",
  });

  Id016PtmumbaiOperationContents.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  Id016PtmumbaiOperationContents.Id016PtmumbaiMichelsonV1Primitives = Object.freeze({
    PARAMETER: 0,
    STORAGE: 1,
    CODE: 2,
    FALSE: 3,
    ELT: 4,
    LEFT: 5,
    NONE_0: 6,
    PAIR_1: 7,
    RIGHT: 8,
    SOME_0: 9,
    TRUE: 10,
    UNIT_0: 11,
    PACK: 12,
    UNPACK: 13,
    BLAKE2B: 14,
    SHA256: 15,
    SHA512: 16,
    ABS: 17,
    ADD: 18,
    AMOUNT: 19,
    AND: 20,
    BALANCE: 21,
    CAR: 22,
    CDR: 23,
    CHECK_SIGNATURE: 24,
    COMPARE: 25,
    CONCAT: 26,
    CONS: 27,
    CREATE_ACCOUNT: 28,
    CREATE_CONTRACT: 29,
    IMPLICIT_ACCOUNT: 30,
    DIP: 31,
    DROP: 32,
    DUP: 33,
    EDIV: 34,
    EMPTY_MAP: 35,
    EMPTY_SET: 36,
    EQ: 37,
    EXEC: 38,
    FAILWITH: 39,
    GE: 40,
    GET: 41,
    GT: 42,
    HASH_KEY: 43,
    IF: 44,
    IF_CONS: 45,
    IF_LEFT: 46,
    IF_NONE: 47,
    INT_0: 48,
    LAMBDA_0: 49,
    LE: 50,
    LEFT_0: 51,
    LOOP: 52,
    LSL: 53,
    LSR: 54,
    LT: 55,
    MAP_0: 56,
    MEM: 57,
    MUL: 58,
    NEG: 59,
    NEQ: 60,
    NIL: 61,
    NONE: 62,
    NOT: 63,
    NOW: 64,
    OR_0: 65,
    PAIR_0: 66,
    PUSH: 67,
    RIGHT_0: 68,
    SIZE: 69,
    SOME: 70,
    SOURCE: 71,
    SENDER: 72,
    SELF: 73,
    STEPS_TO_QUOTA: 74,
    SUB: 75,
    SWAP: 76,
    TRANSFER_TOKENS: 77,
    SET_DELEGATE: 78,
    UNIT_1: 79,
    UPDATE: 80,
    XOR: 81,
    ITER: 82,
    LOOP_LEFT: 83,
    ADDRESS_0: 84,
    CONTRACT_0: 85,
    ISNAT: 86,
    CAST: 87,
    RENAME: 88,
    BOOL: 89,
    CONTRACT: 90,
    INT: 91,
    KEY: 92,
    KEY_HASH: 93,
    LAMBDA: 94,
    LIST: 95,
    MAP: 96,
    BIG_MAP: 97,
    NAT: 98,
    OPTION: 99,
    OR: 100,
    PAIR: 101,
    SET: 102,
    SIGNATURE: 103,
    STRING: 104,
    BYTES: 105,
    MUTEZ: 106,
    TIMESTAMP: 107,
    UNIT: 108,
    OPERATION: 109,
    ADDRESS: 110,
    SLICE: 111,
    DIG: 112,
    DUG: 113,
    EMPTY_BIG_MAP: 114,
    APPLY: 115,
    CHAIN_ID: 116,
    CHAIN_ID_0: 117,
    LEVEL: 118,
    SELF_ADDRESS: 119,
    NEVER: 120,
    NEVER_0: 121,
    UNPAIR: 122,
    VOTING_POWER: 123,
    TOTAL_VOTING_POWER: 124,
    KECCAK: 125,
    SHA3: 126,
    PAIRING_CHECK: 127,
    BLS12_381_G1: 128,
    BLS12_381_G2: 129,
    BLS12_381_FR: 130,
    SAPLING_STATE: 131,
    SAPLING_TRANSACTION_DEPRECATED: 132,
    SAPLING_EMPTY_STATE: 133,
    SAPLING_VERIFY_UPDATE: 134,
    TICKET: 135,
    TICKET_DEPRECATED: 136,
    READ_TICKET: 137,
    SPLIT_TICKET: 138,
    JOIN_TICKETS: 139,
    GET_AND_UPDATE: 140,
    CHEST: 141,
    CHEST_KEY: 142,
    OPEN_CHEST: 143,
    VIEW_0: 144,
    VIEW: 145,
    CONSTANT: 146,
    SUB_MUTEZ: 147,
    TX_ROLLUP_L2_ADDRESS: 148,
    MIN_BLOCK_TIME: 149,
    SAPLING_TRANSACTION: 150,
    EMIT: 151,
    LAMBDA_REC: 152,
    LAMBDA_REC_0: 153,
    TICKET_0: 154,
    BYTES_0: 155,
    NAT_0: 156,

    0: "PARAMETER",
    1: "STORAGE",
    2: "CODE",
    3: "FALSE",
    4: "ELT",
    5: "LEFT",
    6: "NONE_0",
    7: "PAIR_1",
    8: "RIGHT",
    9: "SOME_0",
    10: "TRUE",
    11: "UNIT_0",
    12: "PACK",
    13: "UNPACK",
    14: "BLAKE2B",
    15: "SHA256",
    16: "SHA512",
    17: "ABS",
    18: "ADD",
    19: "AMOUNT",
    20: "AND",
    21: "BALANCE",
    22: "CAR",
    23: "CDR",
    24: "CHECK_SIGNATURE",
    25: "COMPARE",
    26: "CONCAT",
    27: "CONS",
    28: "CREATE_ACCOUNT",
    29: "CREATE_CONTRACT",
    30: "IMPLICIT_ACCOUNT",
    31: "DIP",
    32: "DROP",
    33: "DUP",
    34: "EDIV",
    35: "EMPTY_MAP",
    36: "EMPTY_SET",
    37: "EQ",
    38: "EXEC",
    39: "FAILWITH",
    40: "GE",
    41: "GET",
    42: "GT",
    43: "HASH_KEY",
    44: "IF",
    45: "IF_CONS",
    46: "IF_LEFT",
    47: "IF_NONE",
    48: "INT_0",
    49: "LAMBDA_0",
    50: "LE",
    51: "LEFT_0",
    52: "LOOP",
    53: "LSL",
    54: "LSR",
    55: "LT",
    56: "MAP_0",
    57: "MEM",
    58: "MUL",
    59: "NEG",
    60: "NEQ",
    61: "NIL",
    62: "NONE",
    63: "NOT",
    64: "NOW",
    65: "OR_0",
    66: "PAIR_0",
    67: "PUSH",
    68: "RIGHT_0",
    69: "SIZE",
    70: "SOME",
    71: "SOURCE",
    72: "SENDER",
    73: "SELF",
    74: "STEPS_TO_QUOTA",
    75: "SUB",
    76: "SWAP",
    77: "TRANSFER_TOKENS",
    78: "SET_DELEGATE",
    79: "UNIT_1",
    80: "UPDATE",
    81: "XOR",
    82: "ITER",
    83: "LOOP_LEFT",
    84: "ADDRESS_0",
    85: "CONTRACT_0",
    86: "ISNAT",
    87: "CAST",
    88: "RENAME",
    89: "BOOL",
    90: "CONTRACT",
    91: "INT",
    92: "KEY",
    93: "KEY_HASH",
    94: "LAMBDA",
    95: "LIST",
    96: "MAP",
    97: "BIG_MAP",
    98: "NAT",
    99: "OPTION",
    100: "OR",
    101: "PAIR",
    102: "SET",
    103: "SIGNATURE",
    104: "STRING",
    105: "BYTES",
    106: "MUTEZ",
    107: "TIMESTAMP",
    108: "UNIT",
    109: "OPERATION",
    110: "ADDRESS",
    111: "SLICE",
    112: "DIG",
    113: "DUG",
    114: "EMPTY_BIG_MAP",
    115: "APPLY",
    116: "CHAIN_ID",
    117: "CHAIN_ID_0",
    118: "LEVEL",
    119: "SELF_ADDRESS",
    120: "NEVER",
    121: "NEVER_0",
    122: "UNPAIR",
    123: "VOTING_POWER",
    124: "TOTAL_VOTING_POWER",
    125: "KECCAK",
    126: "SHA3",
    127: "PAIRING_CHECK",
    128: "BLS12_381_G1",
    129: "BLS12_381_G2",
    130: "BLS12_381_FR",
    131: "SAPLING_STATE",
    132: "SAPLING_TRANSACTION_DEPRECATED",
    133: "SAPLING_EMPTY_STATE",
    134: "SAPLING_VERIFY_UPDATE",
    135: "TICKET",
    136: "TICKET_DEPRECATED",
    137: "READ_TICKET",
    138: "SPLIT_TICKET",
    139: "JOIN_TICKETS",
    140: "GET_AND_UPDATE",
    141: "CHEST",
    142: "CHEST_KEY",
    143: "OPEN_CHEST",
    144: "VIEW_0",
    145: "VIEW",
    146: "CONSTANT",
    147: "SUB_MUTEZ",
    148: "TX_ROLLUP_L2_ADDRESS",
    149: "MIN_BLOCK_TIME",
    150: "SAPLING_TRANSACTION",
    151: "EMIT",
    152: "LAMBDA_REC",
    153: "LAMBDA_REC_0",
    154: "TICKET_0",
    155: "BYTES_0",
    156: "NAT_0",
  });

  Id016PtmumbaiOperationContents.PvmKind = Object.freeze({
    ARITH: 0,
    WASM_2_0_0: 1,

    0: "ARITH",
    1: "WASM_2_0_0",
  });

  Id016PtmumbaiOperationContents.RefutationTag = Object.freeze({
    START: 0,
    MOVE: 1,

    0: "START",
    1: "MOVE",
  });

  Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag = Object.freeze({
    SEED_NONCE_REVELATION: 1,
    DOUBLE_ENDORSEMENT_EVIDENCE: 2,
    DOUBLE_BAKING_EVIDENCE: 3,
    ACTIVATE_ACCOUNT: 4,
    PROPOSALS: 5,
    BALLOT: 6,
    DOUBLE_PREENDORSEMENT_EVIDENCE: 7,
    VDF_REVELATION: 8,
    DRAIN_DELEGATE: 9,
    FAILING_NOOP: 17,
    PREENDORSEMENT: 20,
    ENDORSEMENT: 21,
    DAL_ATTESTATION: 22,
    REVEAL: 107,
    TRANSACTION: 108,
    ORIGINATION: 109,
    DELEGATION: 110,
    REGISTER_GLOBAL_CONSTANT: 111,
    SET_DEPOSITS_LIMIT: 112,
    INCREASE_PAID_STORAGE: 113,
    UPDATE_CONSENSUS_KEY: 114,
    TX_ROLLUP_ORIGINATION: 150,
    TX_ROLLUP_SUBMIT_BATCH: 151,
    TX_ROLLUP_COMMIT: 152,
    TX_ROLLUP_RETURN_BOND: 153,
    TX_ROLLUP_FINALIZE_COMMITMENT: 154,
    TX_ROLLUP_REMOVE_COMMITMENT: 155,
    TX_ROLLUP_REJECTION: 156,
    TX_ROLLUP_DISPATCH_TICKETS: 157,
    TRANSFER_TICKET: 158,
    SMART_ROLLUP_ORIGINATE: 200,
    SMART_ROLLUP_ADD_MESSAGES: 201,
    SMART_ROLLUP_CEMENT: 202,
    SMART_ROLLUP_PUBLISH: 203,
    SMART_ROLLUP_REFUTE: 204,
    SMART_ROLLUP_TIMEOUT: 205,
    SMART_ROLLUP_EXECUTE_OUTBOX_MESSAGE: 206,
    SMART_ROLLUP_RECOVER_BOND: 207,
    DAL_PUBLISH_SLOT_HEADER: 230,
    ZK_ROLLUP_ORIGINATION: 250,
    ZK_ROLLUP_PUBLISH: 251,
    ZK_ROLLUP_UPDATE: 252,

    1: "SEED_NONCE_REVELATION",
    2: "DOUBLE_ENDORSEMENT_EVIDENCE",
    3: "DOUBLE_BAKING_EVIDENCE",
    4: "ACTIVATE_ACCOUNT",
    5: "PROPOSALS",
    6: "BALLOT",
    7: "DOUBLE_PREENDORSEMENT_EVIDENCE",
    8: "VDF_REVELATION",
    9: "DRAIN_DELEGATE",
    17: "FAILING_NOOP",
    20: "PREENDORSEMENT",
    21: "ENDORSEMENT",
    22: "DAL_ATTESTATION",
    107: "REVEAL",
    108: "TRANSACTION",
    109: "ORIGINATION",
    110: "DELEGATION",
    111: "REGISTER_GLOBAL_CONSTANT",
    112: "SET_DEPOSITS_LIMIT",
    113: "INCREASE_PAID_STORAGE",
    114: "UPDATE_CONSENSUS_KEY",
    150: "TX_ROLLUP_ORIGINATION",
    151: "TX_ROLLUP_SUBMIT_BATCH",
    152: "TX_ROLLUP_COMMIT",
    153: "TX_ROLLUP_RETURN_BOND",
    154: "TX_ROLLUP_FINALIZE_COMMITMENT",
    155: "TX_ROLLUP_REMOVE_COMMITMENT",
    156: "TX_ROLLUP_REJECTION",
    157: "TX_ROLLUP_DISPATCH_TICKETS",
    158: "TRANSFER_TICKET",
    200: "SMART_ROLLUP_ORIGINATE",
    201: "SMART_ROLLUP_ADD_MESSAGES",
    202: "SMART_ROLLUP_CEMENT",
    203: "SMART_ROLLUP_PUBLISH",
    204: "SMART_ROLLUP_REFUTE",
    205: "SMART_ROLLUP_TIMEOUT",
    206: "SMART_ROLLUP_EXECUTE_OUTBOX_MESSAGE",
    207: "SMART_ROLLUP_RECOVER_BOND",
    230: "DAL_PUBLISH_SLOT_HEADER",
    250: "ZK_ROLLUP_ORIGINATION",
    251: "ZK_ROLLUP_PUBLISH",
    252: "ZK_ROLLUP_UPDATE",
  });

  Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1ExpressionTag = Object.freeze({
    INT: 0,
    STRING: 1,
    SEQUENCE: 2,
    PRIM__NO_ARGS__NO_ANNOTS: 3,
    PRIM__NO_ARGS__SOME_ANNOTS: 4,
    PRIM__1_ARG__NO_ANNOTS: 5,
    PRIM__1_ARG__SOME_ANNOTS: 6,
    PRIM__2_ARGS__NO_ANNOTS: 7,
    PRIM__2_ARGS__SOME_ANNOTS: 8,
    PRIM__GENERIC: 9,
    BYTES: 10,

    0: "INT",
    1: "STRING",
    2: "SEQUENCE",
    3: "PRIM__NO_ARGS__NO_ANNOTS",
    4: "PRIM__NO_ARGS__SOME_ANNOTS",
    5: "PRIM__1_ARG__NO_ANNOTS",
    6: "PRIM__1_ARG__SOME_ANNOTS",
    7: "PRIM__2_ARGS__NO_ANNOTS",
    8: "PRIM__2_ARGS__SOME_ANNOTS",
    9: "PRIM__GENERIC",
    10: "BYTES",
  });

  Id016PtmumbaiOperationContents.OpEltField1Tag = Object.freeze({
    NONE: 0,
    SOME: 1,

    0: "NONE",
    1: "SOME",
  });

  Id016PtmumbaiOperationContents.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  Id016PtmumbaiOperationContents.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id016PtmumbaiOperationContents.CircuitsInfoEltField1Tag = Object.freeze({
    PUBLIC: 0,
    PRIVATE: 1,
    FEE: 2,

    0: "PUBLIC",
    1: "PRIVATE",
    2: "FEE",
  });

  Id016PtmumbaiOperationContents.Id016PtmumbaiContractIdOriginatedTag = Object.freeze({
    ORIGINATED: 1,

    1: "ORIGINATED",
  });

  Id016PtmumbaiOperationContents.PredecessorTag = Object.freeze({
    NONE: 0,
    SOME: 1,

    0: "NONE",
    1: "SOME",
  });

  Id016PtmumbaiOperationContents.Id016PtmumbaiContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id016PtmumbaiOperationContents.Id016PtmumbaiEntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    DEPOSIT: 5,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    5: "DEPOSIT",
    255: "NAMED",
  });

  Id016PtmumbaiOperationContents.AmountTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
  });

  Id016PtmumbaiOperationContents.StepTag = Object.freeze({
    DISSECTION: 0,
    PROOF: 1,

    0: "DISSECTION",
    1: "PROOF",
  });

  Id016PtmumbaiOperationContents.MessageTag = Object.freeze({
    BATCH: 0,
    DEPOSIT: 1,

    0: "BATCH",
    1: "DEPOSIT",
  });

  Id016PtmumbaiOperationContents.Id016PtmumbaiInlinedEndorsementMempoolContentsTag = Object.freeze({
    ENDORSEMENT: 21,

    21: "ENDORSEMENT",
  });

  Id016PtmumbaiOperationContents.InputProofTag = Object.freeze({
    INBOX__PROOF: 0,
    REVEAL__PROOF: 1,
    FIRST__INPUT: 2,

    0: "INBOX__PROOF",
    1: "REVEAL__PROOF",
    2: "FIRST__INPUT",
  });

  function Id016PtmumbaiOperationContents(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiOperationContents.prototype._read = function() {
    this.id016PtmumbaiOperationAlphaContents = new Id016PtmumbaiOperationAlphaContents(this._io, this, this._root);
  }

  var Op20 = Id016PtmumbaiOperationContents.Op20 = (function() {
    function Op20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op20.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_0/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op2(_io__raw_op2, this, this._root);
    }

    return Op20;
  })();

  var Messages = Id016PtmumbaiOperationContents.Messages = (function() {
    function Messages(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Messages.prototype._read = function() {
      this.messagesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messagesEntries.push(new MessagesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Messages;
  })();

  var DalPageId = Id016PtmumbaiOperationContents.DalPageId = (function() {
    function DalPageId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalPageId.prototype._read = function() {
      this.publishedLevel = this._io.readS4be();
      this.slotIndex = this._io.readU1();
      this.pageIndex = this._io.readS2be();
    }

    return DalPageId;
  })();

  var SmartRollupExecuteOutboxMessage = Id016PtmumbaiOperationContents.SmartRollupExecuteOutboxMessage = (function() {
    function SmartRollupExecuteOutboxMessage(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SmartRollupExecuteOutboxMessage.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id016PtmumbaiSmartRollupAddress(this._io, this, this._root);
      this.cementedCommitment = this._io.readBytes(32);
      this.outputProof = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A smart rollup address: A smart rollup is identified by a base58 address starting with sr1
     */

    return SmartRollupExecuteOutboxMessage;
  })();

  var SmartRollupOriginate = Id016PtmumbaiOperationContents.SmartRollupOriginate = (function() {
    function SmartRollupOriginate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SmartRollupOriginate.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.pvmKind = this._io.readU1();
      this.kernel = new BytesDynUint30(this._io, this, this._root);
      this.originationProof = new BytesDynUint30(this._io, this, this._root);
      this.parametersTy = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return SmartRollupOriginate;
  })();

  var Stakers = Id016PtmumbaiOperationContents.Stakers = (function() {
    function Stakers(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Stakers.prototype._read = function() {
      this.alice = new PublicKeyHash(this._io, this, this._root);
      this.bob = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Stakers;
  })();

  var MessageResultPathEntries = Id016PtmumbaiOperationContents.MessageResultPathEntries = (function() {
    function MessageResultPathEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageResultPathEntries.prototype._read = function() {
      this.messageResultListHash = this._io.readBytes(32);
    }

    return MessageResultPathEntries;
  })();

  var DalPageProof = Id016PtmumbaiOperationContents.DalPageProof = (function() {
    function DalPageProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalPageProof.prototype._read = function() {
      this.dalPageId = new DalPageId(this._io, this, this._root);
      this.dalProof = new BytesDynUint30(this._io, this, this._root);
    }

    return DalPageProof;
  })();

  var IncreasePaidStorage = Id016PtmumbaiOperationContents.IncreasePaidStorage = (function() {
    function IncreasePaidStorage(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IncreasePaidStorage.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.amount = new Z(this._io, this, this._root);
      this.destination = new Id016PtmumbaiContractIdOriginated(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A contract handle -- originated account: A contract notation as given to an RPC or inside scripts. Can be a base58 originated contract hash.
     */

    return IncreasePaidStorage;
  })();

  var Move = Id016PtmumbaiOperationContents.Move = (function() {
    function Move(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Move.prototype._read = function() {
      this.choice = new N(this._io, this, this._root);
      this.step = new Step(this._io, this, this._root);
    }

    return Move;
  })();

  var InputProof = Id016PtmumbaiOperationContents.InputProof = (function() {
    function InputProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InputProof.prototype._read = function() {
      this.inputProofTag = this._io.readU1();
      if (this.inputProofTag == Id016PtmumbaiOperationContents.InputProofTag.INBOX__PROOF) {
        this.inboxProof = new InboxProof(this._io, this, this._root);
      }
      if (this.inputProofTag == Id016PtmumbaiOperationContents.InputProofTag.REVEAL__PROOF) {
        this.revealProof = new RevealProof(this._io, this, this._root);
      }
    }

    return InputProof;
  })();

  var SmartRollupTimeout = Id016PtmumbaiOperationContents.SmartRollupTimeout = (function() {
    function SmartRollupTimeout(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SmartRollupTimeout.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id016PtmumbaiSmartRollupAddress(this._io, this, this._root);
      this.stakers = new Stakers(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A smart rollup address: A smart rollup is identified by a base58 address starting with sr1
     */

    return SmartRollupTimeout;
  })();

  var ActivateAccount = Id016PtmumbaiOperationContents.ActivateAccount = (function() {
    function ActivateAccount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ActivateAccount.prototype._read = function() {
      this.pkh = this._io.readBytes(20);
      this.secret = this._io.readBytes(20);
    }

    return ActivateAccount;
  })();

  var Id016PtmumbaiSmartRollupAddress = Id016PtmumbaiOperationContents.Id016PtmumbaiSmartRollupAddress = (function() {
    function Id016PtmumbaiSmartRollupAddress(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiSmartRollupAddress.prototype._read = function() {
      this.smartRollupHash = this._io.readBytes(20);
    }

    return Id016PtmumbaiSmartRollupAddress;
  })();

  var DoubleEndorsementEvidence = Id016PtmumbaiOperationContents.DoubleEndorsementEvidence = (function() {
    function DoubleEndorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleEndorsementEvidence.prototype._read = function() {
      this.op1 = new Op10(this._io, this, this._root);
      this.op2 = new Op20(this._io, this, this._root);
    }

    return DoubleEndorsementEvidence;
  })();

  var Step = Id016PtmumbaiOperationContents.Step = (function() {
    function Step(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Step.prototype._read = function() {
      this.stepTag = this._io.readU1();
      if (this.stepTag == Id016PtmumbaiOperationContents.StepTag.DISSECTION) {
        this.dissection = new Dissection0(this._io, this, this._root);
      }
      if (this.stepTag == Id016PtmumbaiOperationContents.StepTag.PROOF) {
        this.proof = new Proof(this._io, this, this._root);
      }
    }

    return Step;
  })();

  var PendingPisEltField1 = Id016PtmumbaiOperationContents.PendingPisEltField1 = (function() {
    function PendingPisEltField1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PendingPisEltField1.prototype._read = function() {
      this.newState = new NewState0(this._io, this, this._root);
      this.fee = this._io.readBytes(32);
      this.exitValidity = this._io.readU1();
    }

    return PendingPisEltField1;
  })();

  var NewStateEntries = Id016PtmumbaiOperationContents.NewStateEntries = (function() {
    function NewStateEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NewStateEntries.prototype._read = function() {
      this.newStateElt = this._io.readBytes(32);
    }

    return NewStateEntries;
  })();

  var Originated = Id016PtmumbaiOperationContents.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var N = Id016PtmumbaiOperationContents.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id016PtmumbaiMichelsonV1Primitives = Id016PtmumbaiOperationContents.Id016PtmumbaiMichelsonV1Primitives = (function() {
    function Id016PtmumbaiMichelsonV1Primitives(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiMichelsonV1Primitives.prototype._read = function() {
      this.id016PtmumbaiMichelsonV1Primitives = this._io.readU1();
    }

    return Id016PtmumbaiMichelsonV1Primitives;
  })();

  var Endorsement = Id016PtmumbaiOperationContents.Endorsement = (function() {
    function Endorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Endorsement.prototype._read = function() {
      this.slot = this._io.readU2be();
      this.level = this._io.readS4be();
      this.round = this._io.readS4be();
      this.blockPayloadHash = this._io.readBytes(32);
    }

    return Endorsement;
  })();

  var Proposals0 = Id016PtmumbaiOperationContents.Proposals0 = (function() {
    function Proposals0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals0.prototype._read = function() {
      this.lenProposals = this._io.readU4be();
      if (!(this.lenProposals <= 640)) {
        throw new KaitaiStream.ValidationGreaterThanError(640, this.lenProposals, this._io, "/types/proposals_0/seq/0");
      }
      this._raw_proposals = this._io.readBytes(this.lenProposals);
      var _io__raw_proposals = new KaitaiStream(this._raw_proposals);
      this.proposals = new Proposals(_io__raw_proposals, this, this._root);
    }

    return Proposals0;
  })();

  var MessagesEntries = Id016PtmumbaiOperationContents.MessagesEntries = (function() {
    function MessagesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagesEntries.prototype._read = function() {
      this.messageResultHash = this._io.readBytes(32);
    }

    return MessagesEntries;
  })();

  var Id016PtmumbaiBlockHeaderAlphaSignedContents = Id016PtmumbaiOperationContents.Id016PtmumbaiBlockHeaderAlphaSignedContents = (function() {
    function Id016PtmumbaiBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id016PtmumbaiBlockHeaderAlphaUnsignedContents = new Id016PtmumbaiBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytesFull();
    }

    return Id016PtmumbaiBlockHeaderAlphaSignedContents;
  })();

  var TransferTicket = Id016PtmumbaiOperationContents.TransferTicket = (function() {
    function TransferTicket(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TransferTicket.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.ticketContents = new BytesDynUint30(this._io, this, this._root);
      this.ticketTy = new BytesDynUint30(this._io, this, this._root);
      this.ticketTicketer = new Id016PtmumbaiContractId(this._io, this, this._root);
      this.ticketAmount = new N(this._io, this, this._root);
      this.destination = new Id016PtmumbaiContractId(this._io, this, this._root);
      this.entrypoint = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return TransferTicket;
  })();

  var PendingPis = Id016PtmumbaiOperationContents.PendingPis = (function() {
    function PendingPis(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PendingPis.prototype._read = function() {
      this.pendingPisEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.pendingPisEntries.push(new PendingPisEntries(this._io, this, this._root));
        i++;
      }
    }

    return PendingPis;
  })();

  var Payload = Id016PtmumbaiOperationContents.Payload = (function() {
    function Payload(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Payload.prototype._read = function() {
      this.payloadEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.payloadEntries.push(new PayloadEntries(this._io, this, this._root));
        i++;
      }
    }

    return Payload;
  })();

  var SmartRollupCement = Id016PtmumbaiOperationContents.SmartRollupCement = (function() {
    function SmartRollupCement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SmartRollupCement.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id016PtmumbaiSmartRollupAddress(this._io, this, this._root);
      this.commitment = this._io.readBytes(32);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A smart rollup address: A smart rollup is identified by a base58 address starting with sr1
     */

    return SmartRollupCement;
  })();

  var CircuitsInfo = Id016PtmumbaiOperationContents.CircuitsInfo = (function() {
    function CircuitsInfo(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CircuitsInfo.prototype._read = function() {
      this.circuitsInfoEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.circuitsInfoEntries.push(new CircuitsInfoEntries(this._io, this, this._root));
        i++;
      }
    }

    return CircuitsInfo;
  })();

  var Op12 = Id016PtmumbaiOperationContents.Op12 = (function() {
    function Op12(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op12.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_2/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op11(_io__raw_op1, this, this._root);
    }

    return Op12;
  })();

  var PendingPisEntries = Id016PtmumbaiOperationContents.PendingPisEntries = (function() {
    function PendingPisEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PendingPisEntries.prototype._read = function() {
      this.pendingPisEltField0 = new BytesDynUint30(this._io, this, this._root);
      this.pendingPisEltField1 = new PendingPisEltField1(this._io, this, this._root);
    }

    return PendingPisEntries;
  })();

  var TxRollupRemoveCommitment = Id016PtmumbaiOperationContents.TxRollupRemoveCommitment = (function() {
    function TxRollupRemoveCommitment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupRemoveCommitment.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id016PtmumbaiTxRollupId(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupRemoveCommitment;
  })();

  var SmartRollupRecoverBond = Id016PtmumbaiOperationContents.SmartRollupRecoverBond = (function() {
    function SmartRollupRecoverBond(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SmartRollupRecoverBond.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = this._io.readBytes(20);
      this.staker = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return SmartRollupRecoverBond;
  })();

  var NewState0 = Id016PtmumbaiOperationContents.NewState0 = (function() {
    function NewState0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NewState0.prototype._read = function() {
      this.lenNewState = this._io.readU4be();
      if (!(this.lenNewState <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenNewState, this._io, "/types/new_state_0/seq/0");
      }
      this._raw_newState = this._io.readBytes(this.lenNewState);
      var _io__raw_newState = new KaitaiStream(this._raw_newState);
      this.newState = new NewState(_io__raw_newState, this, this._root);
    }

    return NewState0;
  })();

  var Id016PtmumbaiInlinedEndorsementMempoolContents = Id016PtmumbaiOperationContents.Id016PtmumbaiInlinedEndorsementMempoolContents = (function() {
    function Id016PtmumbaiInlinedEndorsementMempoolContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiInlinedEndorsementMempoolContents.prototype._read = function() {
      this.id016PtmumbaiInlinedEndorsementMempoolContentsTag = this._io.readU1();
      if (this.id016PtmumbaiInlinedEndorsementMempoolContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiInlinedEndorsementMempoolContentsTag.ENDORSEMENT) {
        this.endorsement = new Endorsement(this._io, this, this._root);
      }
    }

    return Id016PtmumbaiInlinedEndorsementMempoolContents;
  })();

  var Prim1ArgSomeAnnots = Id016PtmumbaiOperationContents.Prim1ArgSomeAnnots = (function() {
    function Prim1ArgSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim1ArgSomeAnnots.prototype._read = function() {
      this.prim = new Id016PtmumbaiMichelsonV1Primitives(this._io, this, this._root);
      this.arg = new Micheline016PtmumbaiMichelsonV1Expression(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return Prim1ArgSomeAnnots;
  })();

  var Reveal = Id016PtmumbaiOperationContents.Reveal = (function() {
    function Reveal(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Reveal.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.publicKey = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Reveal;
  })();

  var RevealProof = Id016PtmumbaiOperationContents.RevealProof = (function() {
    function RevealProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RevealProof.prototype._read = function() {
      this.revealProofTag = this._io.readU1();
      if (this.revealProofTag == Id016PtmumbaiOperationContents.RevealProofTag.RAW__DATA__PROOF) {
        this.rawDataProof = new RawData0(this._io, this, this._root);
      }
      if (this.revealProofTag == Id016PtmumbaiOperationContents.RevealProofTag.DAL__PAGE__PROOF) {
        this.dalPageProof = new DalPageProof(this._io, this, this._root);
      }
    }

    return RevealProof;
  })();

  var RawData = Id016PtmumbaiOperationContents.RawData = (function() {
    function RawData(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RawData.prototype._read = function() {
      this.rawData = this._io.readBytesFull();
    }

    return RawData;
  })();

  var SmartRollupAddMessages = Id016PtmumbaiOperationContents.SmartRollupAddMessages = (function() {
    function SmartRollupAddMessages(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SmartRollupAddMessages.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.message = new Message1(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return SmartRollupAddMessages;
  })();

  var CircuitsInfo0 = Id016PtmumbaiOperationContents.CircuitsInfo0 = (function() {
    function CircuitsInfo0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CircuitsInfo0.prototype._read = function() {
      this.lenCircuitsInfo = this._io.readU4be();
      if (!(this.lenCircuitsInfo <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCircuitsInfo, this._io, "/types/circuits_info_0/seq/0");
      }
      this._raw_circuitsInfo = this._io.readBytes(this.lenCircuitsInfo);
      var _io__raw_circuitsInfo = new KaitaiStream(this._raw_circuitsInfo);
      this.circuitsInfo = new CircuitsInfo(_io__raw_circuitsInfo, this, this._root);
    }

    return CircuitsInfo0;
  })();

  var Price = Id016PtmumbaiOperationContents.Price = (function() {
    function Price(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Price.prototype._read = function() {
      this.id = this._io.readBytes(32);
      this.amount = new Z(this._io, this, this._root);
    }

    return Price;
  })();

  var MessageEntries = Id016PtmumbaiOperationContents.MessageEntries = (function() {
    function MessageEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageEntries.prototype._read = function() {
      this.messageElt = new BytesDynUint30(this._io, this, this._root);
    }

    return MessageEntries;
  })();

  var OpEltField0 = Id016PtmumbaiOperationContents.OpEltField0 = (function() {
    function OpEltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    OpEltField0.prototype._read = function() {
      this.opCode = new Int31(this._io, this, this._root);
      this.price = new Price(this._io, this, this._root);
      this.l1Dst = new PublicKeyHash(this._io, this, this._root);
      this.rollupId = this._io.readBytes(20);
      this.payload = new Payload0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return OpEltField0;
  })();

  var PrivatePisEltField1 = Id016PtmumbaiOperationContents.PrivatePisEltField1 = (function() {
    function PrivatePisEltField1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrivatePisEltField1.prototype._read = function() {
      this.newState = new NewState0(this._io, this, this._root);
      this.fee = this._io.readBytes(32);
    }

    return PrivatePisEltField1;
  })();

  var Id016PtmumbaiTxRollupId = Id016PtmumbaiOperationContents.Id016PtmumbaiTxRollupId = (function() {
    function Id016PtmumbaiTxRollupId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiTxRollupId.prototype._read = function() {
      this.rollupHash = this._io.readBytes(20);
    }

    return Id016PtmumbaiTxRollupId;
  })();

  var Refutation = Id016PtmumbaiOperationContents.Refutation = (function() {
    function Refutation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Refutation.prototype._read = function() {
      this.refutationTag = this._io.readU1();
      if (this.refutationTag == Id016PtmumbaiOperationContents.RefutationTag.START) {
        this.start = new Start(this._io, this, this._root);
      }
      if (this.refutationTag == Id016PtmumbaiOperationContents.RefutationTag.MOVE) {
        this.move = new Move(this._io, this, this._root);
      }
    }

    return Refutation;
  })();

  var SeedNonceRevelation = Id016PtmumbaiOperationContents.SeedNonceRevelation = (function() {
    function SeedNonceRevelation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SeedNonceRevelation.prototype._read = function() {
      this.level = this._io.readS4be();
      this.nonce = this._io.readBytes(32);
    }

    return SeedNonceRevelation;
  })();

  var Preendorsement = Id016PtmumbaiOperationContents.Preendorsement = (function() {
    function Preendorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Preendorsement.prototype._read = function() {
      this.slot = this._io.readU2be();
      this.level = this._io.readS4be();
      this.round = this._io.readS4be();
      this.blockPayloadHash = this._io.readBytes(32);
    }

    return Preendorsement;
  })();

  var PrimNoArgsSomeAnnots = Id016PtmumbaiOperationContents.PrimNoArgsSomeAnnots = (function() {
    function PrimNoArgsSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrimNoArgsSomeAnnots.prototype._read = function() {
      this.prim = new Id016PtmumbaiMichelsonV1Primitives(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return PrimNoArgsSomeAnnots;
  })();

  var TxRollupCommit = Id016PtmumbaiOperationContents.TxRollupCommit = (function() {
    function TxRollupCommit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupCommit.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id016PtmumbaiTxRollupId(this._io, this, this._root);
      this.commitment = new Commitment(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupCommit;
  })();

  var DoubleBakingEvidence = Id016PtmumbaiOperationContents.DoubleBakingEvidence = (function() {
    function DoubleBakingEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleBakingEvidence.prototype._read = function() {
      this.bh1 = new Bh10(this._io, this, this._root);
      this.bh2 = new Bh20(this._io, this, this._root);
    }

    return DoubleBakingEvidence;
  })();

  var Id016PtmumbaiInlinedPreendorsement = Id016PtmumbaiOperationContents.Id016PtmumbaiInlinedPreendorsement = (function() {
    function Id016PtmumbaiInlinedPreendorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiInlinedPreendorsement.prototype._read = function() {
      this.id016PtmumbaiInlinedPreendorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id016PtmumbaiInlinedPreendorsementContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id016PtmumbaiOperationContents.Bool.TRUE) {
        this.signature = this._io.readBytesFull();
      }
    }

    return Id016PtmumbaiInlinedPreendorsement;
  })();

  var PublicKey = Id016PtmumbaiOperationContents.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id016PtmumbaiOperationContents.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id016PtmumbaiOperationContents.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id016PtmumbaiOperationContents.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id016PtmumbaiOperationContents.PublicKeyTag.BLS) {
        this.bls = this._io.readBytes(48);
      }
    }

    return PublicKey;
  })();

  var TxRollupDispatchTickets = Id016PtmumbaiOperationContents.TxRollupDispatchTickets = (function() {
    function TxRollupDispatchTickets(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupDispatchTickets.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.txRollup = new Id016PtmumbaiTxRollupId(this._io, this, this._root);
      this.level = this._io.readS4be();
      this.contextHash = this._io.readBytes(32);
      this.messageIndex = new Int31(this._io, this, this._root);
      this.messageResultPath = new MessageResultPath0(this._io, this, this._root);
      this.ticketsInfo = new TicketsInfo0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupDispatchTickets;
  })();

  var Bh2 = Id016PtmumbaiOperationContents.Bh2 = (function() {
    function Bh2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh2.prototype._read = function() {
      this.id016PtmumbaiBlockHeaderAlphaFullHeader = new Id016PtmumbaiBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh2;
  })();

  var PreviousMessageResultPathEntries = Id016PtmumbaiOperationContents.PreviousMessageResultPathEntries = (function() {
    function PreviousMessageResultPathEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResultPathEntries.prototype._read = function() {
      this.messageResultListHash = this._io.readBytes(32);
    }

    return PreviousMessageResultPathEntries;
  })();

  var Named = Id016PtmumbaiOperationContents.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Amount = Id016PtmumbaiOperationContents.Amount = (function() {
    function Amount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Amount.prototype._read = function() {
      this.amountTag = this._io.readU1();
      if (this.amountTag == Id016PtmumbaiOperationContents.AmountTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.amountTag == Id016PtmumbaiOperationContents.AmountTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.amountTag == Id016PtmumbaiOperationContents.AmountTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.amountTag == Id016PtmumbaiOperationContents.AmountTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
    }

    return Amount;
  })();

  var PrimGeneric = Id016PtmumbaiOperationContents.PrimGeneric = (function() {
    function PrimGeneric(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrimGeneric.prototype._read = function() {
      this.prim = new Id016PtmumbaiMichelsonV1Primitives(this._io, this, this._root);
      this.args = new Args0(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return PrimGeneric;
  })();

  var SlotHeader = Id016PtmumbaiOperationContents.SlotHeader = (function() {
    function SlotHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SlotHeader.prototype._read = function() {
      this.level = this._io.readS4be();
      this.index = this._io.readU1();
      this.commitment = this._io.readBytes(48);
      this.commitmentProof = this._io.readBytes(96);
    }

    return SlotHeader;
  })();

  var Bh20 = Id016PtmumbaiOperationContents.Bh20 = (function() {
    function Bh20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh20.prototype._read = function() {
      this.lenBh2 = this._io.readU4be();
      if (!(this.lenBh2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh2, this._io, "/types/bh2_0/seq/0");
      }
      this._raw_bh2 = this._io.readBytes(this.lenBh2);
      var _io__raw_bh2 = new KaitaiStream(this._raw_bh2);
      this.bh2 = new Bh2(_io__raw_bh2, this, this._root);
    }

    return Bh20;
  })();

  var Commitment = Id016PtmumbaiOperationContents.Commitment = (function() {
    function Commitment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitment.prototype._read = function() {
      this.level = this._io.readS4be();
      this.messages = new Messages0(this._io, this, this._root);
      this.predecessor = new Predecessor(this._io, this, this._root);
      this.inboxMerkleRoot = this._io.readBytes(32);
    }

    return Commitment;
  })();

  var TicketsInfoEntries = Id016PtmumbaiOperationContents.TicketsInfoEntries = (function() {
    function TicketsInfoEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TicketsInfoEntries.prototype._read = function() {
      this.contents = new BytesDynUint30(this._io, this, this._root);
      this.ty = new BytesDynUint30(this._io, this, this._root);
      this.ticketer = new Id016PtmumbaiContractId(this._io, this, this._root);
      this.amount = new Amount(this._io, this, this._root);
      this.claimer = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return TicketsInfoEntries;
  })();

  var Delegation = Id016PtmumbaiOperationContents.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id016PtmumbaiOperationContents.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Delegation;
  })();

  var RegisterGlobalConstant = Id016PtmumbaiOperationContents.RegisterGlobalConstant = (function() {
    function RegisterGlobalConstant(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RegisterGlobalConstant.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return RegisterGlobalConstant;
  })();

  var Solution = Id016PtmumbaiOperationContents.Solution = (function() {
    function Solution(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Solution.prototype._read = function() {
      this.solutionField0 = this._io.readBytes(100);
      this.solutionField1 = this._io.readBytes(100);
    }

    return Solution;
  })();

  var PreviousMessageResult = Id016PtmumbaiOperationContents.PreviousMessageResult = (function() {
    function PreviousMessageResult(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResult.prototype._read = function() {
      this.contextHash = this._io.readBytes(32);
      this.withdrawListHash = this._io.readBytes(32);
    }

    return PreviousMessageResult;
  })();

  var PayloadEntries = Id016PtmumbaiOperationContents.PayloadEntries = (function() {
    function PayloadEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PayloadEntries.prototype._read = function() {
      this.payloadElt = this._io.readBytes(32);
    }

    return PayloadEntries;
  })();

  var Sequence0 = Id016PtmumbaiOperationContents.Sequence0 = (function() {
    function Sequence0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sequence0.prototype._read = function() {
      this.lenSequence = this._io.readU4be();
      if (!(this.lenSequence <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenSequence, this._io, "/types/sequence_0/seq/0");
      }
      this._raw_sequence = this._io.readBytes(this.lenSequence);
      var _io__raw_sequence = new KaitaiStream(this._raw_sequence);
      this.sequence = new Sequence(_io__raw_sequence, this, this._root);
    }

    return Sequence0;
  })();

  var Id016PtmumbaiContractIdOriginated = Id016PtmumbaiOperationContents.Id016PtmumbaiContractIdOriginated = (function() {
    function Id016PtmumbaiContractIdOriginated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiContractIdOriginated.prototype._read = function() {
      this.id016PtmumbaiContractIdOriginatedTag = this._io.readU1();
      if (this.id016PtmumbaiContractIdOriginatedTag == Id016PtmumbaiOperationContents.Id016PtmumbaiContractIdOriginatedTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    return Id016PtmumbaiContractIdOriginated;
  })();

  var PendingPis0 = Id016PtmumbaiOperationContents.PendingPis0 = (function() {
    function PendingPis0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PendingPis0.prototype._read = function() {
      this.lenPendingPis = this._io.readU4be();
      if (!(this.lenPendingPis <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenPendingPis, this._io, "/types/pending_pis_0/seq/0");
      }
      this._raw_pendingPis = this._io.readBytes(this.lenPendingPis);
      var _io__raw_pendingPis = new KaitaiStream(this._raw_pendingPis);
      this.pendingPis = new PendingPis(_io__raw_pendingPis, this, this._root);
    }

    return PendingPis0;
  })();

  var PrivatePis0 = Id016PtmumbaiOperationContents.PrivatePis0 = (function() {
    function PrivatePis0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrivatePis0.prototype._read = function() {
      this.lenPrivatePis = this._io.readU4be();
      if (!(this.lenPrivatePis <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenPrivatePis, this._io, "/types/private_pis_0/seq/0");
      }
      this._raw_privatePis = this._io.readBytes(this.lenPrivatePis);
      var _io__raw_privatePis = new KaitaiStream(this._raw_privatePis);
      this.privatePis = new PrivatePis(_io__raw_privatePis, this, this._root);
    }

    return PrivatePis0;
  })();

  var PreviousMessageResultPath = Id016PtmumbaiOperationContents.PreviousMessageResultPath = (function() {
    function PreviousMessageResultPath(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResultPath.prototype._read = function() {
      this.previousMessageResultPathEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.previousMessageResultPathEntries.push(new PreviousMessageResultPathEntries(this._io, this, this._root));
        i++;
      }
    }

    return PreviousMessageResultPath;
  })();

  var PrivatePisEntries = Id016PtmumbaiOperationContents.PrivatePisEntries = (function() {
    function PrivatePisEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrivatePisEntries.prototype._read = function() {
      this.privatePisEltField0 = new BytesDynUint30(this._io, this, this._root);
      this.privatePisEltField1 = new PrivatePisEltField1(this._io, this, this._root);
    }

    return PrivatePisEntries;
  })();

  var Sequence = Id016PtmumbaiOperationContents.Sequence = (function() {
    function Sequence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sequence.prototype._read = function() {
      this.sequenceEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.sequenceEntries.push(new SequenceEntries(this._io, this, this._root));
        i++;
      }
    }

    return Sequence;
  })();

  var PreviousMessageResultPath0 = Id016PtmumbaiOperationContents.PreviousMessageResultPath0 = (function() {
    function PreviousMessageResultPath0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResultPath0.prototype._read = function() {
      this.lenPreviousMessageResultPath = this._io.readU4be();
      if (!(this.lenPreviousMessageResultPath <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenPreviousMessageResultPath, this._io, "/types/previous_message_result_path_0/seq/0");
      }
      this._raw_previousMessageResultPath = this._io.readBytes(this.lenPreviousMessageResultPath);
      var _io__raw_previousMessageResultPath = new KaitaiStream(this._raw_previousMessageResultPath);
      this.previousMessageResultPath = new PreviousMessageResultPath(_io__raw_previousMessageResultPath, this, this._root);
    }

    return PreviousMessageResultPath0;
  })();

  var Bh10 = Id016PtmumbaiOperationContents.Bh10 = (function() {
    function Bh10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh10.prototype._read = function() {
      this.lenBh1 = this._io.readU4be();
      if (!(this.lenBh1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh1, this._io, "/types/bh1_0/seq/0");
      }
      this._raw_bh1 = this._io.readBytes(this.lenBh1);
      var _io__raw_bh1 = new KaitaiStream(this._raw_bh1);
      this.bh1 = new Bh1(_io__raw_bh1, this, this._root);
    }

    return Bh10;
  })();

  var Op22 = Id016PtmumbaiOperationContents.Op22 = (function() {
    function Op22(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op22.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_2/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op21(_io__raw_op2, this, this._root);
    }

    return Op22;
  })();

  var Id016PtmumbaiEntrypoint = Id016PtmumbaiOperationContents.Id016PtmumbaiEntrypoint = (function() {
    function Id016PtmumbaiEntrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiEntrypoint.prototype._read = function() {
      this.id016PtmumbaiEntrypointTag = this._io.readU1();
      if (this.id016PtmumbaiEntrypointTag == Id016PtmumbaiOperationContents.Id016PtmumbaiEntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id016PtmumbaiEntrypoint;
  })();

  var CircuitsInfoEntries = Id016PtmumbaiOperationContents.CircuitsInfoEntries = (function() {
    function CircuitsInfoEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CircuitsInfoEntries.prototype._read = function() {
      this.circuitsInfoEltField0 = new BytesDynUint30(this._io, this, this._root);
      this.circuitsInfoEltField1 = this._io.readU1();
    }

    /**
     * circuits_info_elt_field1_tag
     */

    return CircuitsInfoEntries;
  })();

  var Message0 = Id016PtmumbaiOperationContents.Message0 = (function() {
    function Message0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message0.prototype._read = function() {
      this.messageEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messageEntries.push(new MessageEntries(this._io, this, this._root));
        i++;
      }
    }

    return Message0;
  })();

  var InboxProof = Id016PtmumbaiOperationContents.InboxProof = (function() {
    function InboxProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InboxProof.prototype._read = function() {
      this.level = this._io.readS4be();
      this.messageCounter = new N(this._io, this, this._root);
      this.serializedProof = new BytesDynUint30(this._io, this, this._root);
    }

    return InboxProof;
  })();

  var OpEltField1 = Id016PtmumbaiOperationContents.OpEltField1 = (function() {
    function OpEltField1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    OpEltField1.prototype._read = function() {
      this.opEltField1Tag = this._io.readU1();
      if (this.opEltField1Tag == Id016PtmumbaiOperationContents.OpEltField1Tag.SOME) {
        this.some = new Some(this._io, this, this._root);
      }
    }

    return OpEltField1;
  })();

  var BytesDynUint30 = Id016PtmumbaiOperationContents.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Op0 = Id016PtmumbaiOperationContents.Op0 = (function() {
    function Op0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op0.prototype._read = function() {
      this.lenOp = this._io.readU4be();
      if (!(this.lenOp <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp, this._io, "/types/op_0/seq/0");
      }
      this._raw_op = this._io.readBytes(this.lenOp);
      var _io__raw_op = new KaitaiStream(this._raw_op);
      this.op = new Op(_io__raw_op, this, this._root);
    }

    return Op0;
  })();

  var NewState = Id016PtmumbaiOperationContents.NewState = (function() {
    function NewState(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NewState.prototype._read = function() {
      this.newStateEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.newStateEntries.push(new NewStateEntries(this._io, this, this._root));
        i++;
      }
    }

    return NewState;
  })();

  var SmartRollupPublish = Id016PtmumbaiOperationContents.SmartRollupPublish = (function() {
    function SmartRollupPublish(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SmartRollupPublish.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id016PtmumbaiSmartRollupAddress(this._io, this, this._root);
      this.commitment = new Commitment0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A smart rollup address: A smart rollup is identified by a base58 address starting with sr1
     */

    return SmartRollupPublish;
  })();

  var Bh1 = Id016PtmumbaiOperationContents.Bh1 = (function() {
    function Bh1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh1.prototype._read = function() {
      this.id016PtmumbaiBlockHeaderAlphaFullHeader = new Id016PtmumbaiBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh1;
  })();

  var Payload0 = Id016PtmumbaiOperationContents.Payload0 = (function() {
    function Payload0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Payload0.prototype._read = function() {
      this.lenPayload = this._io.readU4be();
      if (!(this.lenPayload <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenPayload, this._io, "/types/payload_0/seq/0");
      }
      this._raw_payload = this._io.readBytes(this.lenPayload);
      var _io__raw_payload = new KaitaiStream(this._raw_payload);
      this.payload = new Payload(_io__raw_payload, this, this._root);
    }

    return Payload0;
  })();

  var Dissection0 = Id016PtmumbaiOperationContents.Dissection0 = (function() {
    function Dissection0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Dissection0.prototype._read = function() {
      this.lenDissection = this._io.readU4be();
      if (!(this.lenDissection <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenDissection, this._io, "/types/dissection_0/seq/0");
      }
      this._raw_dissection = this._io.readBytes(this.lenDissection);
      var _io__raw_dissection = new KaitaiStream(this._raw_dissection);
      this.dissection = new Dissection(_io__raw_dissection, this, this._root);
    }

    return Dissection0;
  })();

  var TxRollupRejection = Id016PtmumbaiOperationContents.TxRollupRejection = (function() {
    function TxRollupRejection(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupRejection.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id016PtmumbaiTxRollupId(this._io, this, this._root);
      this.level = this._io.readS4be();
      this.message = new Message(this._io, this, this._root);
      this.messagePosition = new N(this._io, this, this._root);
      this.messagePath = new MessagePath0(this._io, this, this._root);
      this.messageResultHash = this._io.readBytes(32);
      this.messageResultPath = new MessageResultPath0(this._io, this, this._root);
      this.previousMessageResult = new PreviousMessageResult(this._io, this, this._root);
      this.previousMessageResultPath = new PreviousMessageResultPath0(this._io, this, this._root);
      this.proof = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupRejection;
  })();

  var SetDepositsLimit = Id016PtmumbaiOperationContents.SetDepositsLimit = (function() {
    function SetDepositsLimit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SetDepositsLimit.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.limitTag = this._io.readU1();
      if (this.limitTag == Id016PtmumbaiOperationContents.Bool.TRUE) {
        this.limit = new Id016PtmumbaiMutez(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return SetDepositsLimit;
  })();

  var Message1 = Id016PtmumbaiOperationContents.Message1 = (function() {
    function Message1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message1.prototype._read = function() {
      this.lenMessage = this._io.readU4be();
      if (!(this.lenMessage <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessage, this._io, "/types/message_1/seq/0");
      }
      this._raw_message = this._io.readBytes(this.lenMessage);
      var _io__raw_message = new KaitaiStream(this._raw_message);
      this.message = new Message0(_io__raw_message, this, this._root);
    }

    return Message1;
  })();

  var Op10 = Id016PtmumbaiOperationContents.Op10 = (function() {
    function Op10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op10.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_0/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op1(_io__raw_op1, this, this._root);
    }

    return Op10;
  })();

  var InitState0 = Id016PtmumbaiOperationContents.InitState0 = (function() {
    function InitState0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InitState0.prototype._read = function() {
      this.lenInitState = this._io.readU4be();
      if (!(this.lenInitState <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenInitState, this._io, "/types/init_state_0/seq/0");
      }
      this._raw_initState = this._io.readBytes(this.lenInitState);
      var _io__raw_initState = new KaitaiStream(this._raw_initState);
      this.initState = new InitState(_io__raw_initState, this, this._root);
    }

    return InitState0;
  })();

  var Op21 = Id016PtmumbaiOperationContents.Op21 = (function() {
    function Op21(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op21.prototype._read = function() {
      this.id016PtmumbaiInlinedPreendorsement = new Id016PtmumbaiInlinedPreendorsement(this._io, this, this._root);
    }

    return Op21;
  })();

  var Proof = Id016PtmumbaiOperationContents.Proof = (function() {
    function Proof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proof.prototype._read = function() {
      this.pvmStep = new BytesDynUint30(this._io, this, this._root);
      this.inputProofTag = this._io.readU1();
      if (this.inputProofTag == Id016PtmumbaiOperationContents.Bool.TRUE) {
        this.inputProof = new InputProof(this._io, this, this._root);
      }
    }

    return Proof;
  })();

  var SmartRollupRefute = Id016PtmumbaiOperationContents.SmartRollupRefute = (function() {
    function SmartRollupRefute(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SmartRollupRefute.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id016PtmumbaiSmartRollupAddress(this._io, this, this._root);
      this.opponent = new PublicKeyHash(this._io, this, this._root);
      this.refutation = new Refutation(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A smart rollup address: A smart rollup is identified by a base58 address starting with sr1
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return SmartRollupRefute;
  })();

  var Commitment0 = Id016PtmumbaiOperationContents.Commitment0 = (function() {
    function Commitment0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitment0.prototype._read = function() {
      this.compressedState = this._io.readBytes(32);
      this.inboxLevel = this._io.readS4be();
      this.predecessor = this._io.readBytes(32);
      this.numberOfTicks = this._io.readS8be();
    }

    return Commitment0;
  })();

  var Origination = Id016PtmumbaiOperationContents.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.balance = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id016PtmumbaiOperationContents.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id016PtmumbaiScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Origination;
  })();

  var Id016PtmumbaiScriptedContracts = Id016PtmumbaiOperationContents.Id016PtmumbaiScriptedContracts = (function() {
    function Id016PtmumbaiScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id016PtmumbaiScriptedContracts;
  })();

  var DrainDelegate = Id016PtmumbaiOperationContents.DrainDelegate = (function() {
    function DrainDelegate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DrainDelegate.prototype._read = function() {
      this.consensusKey = new PublicKeyHash(this._io, this, this._root);
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.destination = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return DrainDelegate;
  })();

  var ZkRollupUpdate = Id016PtmumbaiOperationContents.ZkRollupUpdate = (function() {
    function ZkRollupUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ZkRollupUpdate.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.zkRollup = this._io.readBytes(20);
      this.update = new Update(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return ZkRollupUpdate;
  })();

  var Message = Id016PtmumbaiOperationContents.Message = (function() {
    function Message(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message.prototype._read = function() {
      this.messageTag = this._io.readU1();
      if (this.messageTag == Id016PtmumbaiOperationContents.MessageTag.BATCH) {
        this.batch = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.messageTag == Id016PtmumbaiOperationContents.MessageTag.DEPOSIT) {
        this.deposit = new Deposit(this._io, this, this._root);
      }
    }

    return Message;
  })();

  var Id016PtmumbaiContractId = Id016PtmumbaiOperationContents.Id016PtmumbaiContractId = (function() {
    function Id016PtmumbaiContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiContractId.prototype._read = function() {
      this.id016PtmumbaiContractIdTag = this._io.readU1();
      if (this.id016PtmumbaiContractIdTag == Id016PtmumbaiOperationContents.Id016PtmumbaiContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id016PtmumbaiContractIdTag == Id016PtmumbaiOperationContents.Id016PtmumbaiContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id016PtmumbaiContractId;
  })();

  var InitState = Id016PtmumbaiOperationContents.InitState = (function() {
    function InitState(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InitState.prototype._read = function() {
      this.initStateEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.initStateEntries.push(new InitStateEntries(this._io, this, this._root));
        i++;
      }
    }

    return InitState;
  })();

  var Args = Id016PtmumbaiOperationContents.Args = (function() {
    function Args(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Args.prototype._read = function() {
      this.argsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.argsEntries.push(new ArgsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Args;
  })();

  var Micheline016PtmumbaiMichelsonV1Expression = Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1Expression = (function() {
    function Micheline016PtmumbaiMichelsonV1Expression(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Micheline016PtmumbaiMichelsonV1Expression.prototype._read = function() {
      this.micheline016PtmumbaiMichelsonV1ExpressionTag = this._io.readU1();
      if (this.micheline016PtmumbaiMichelsonV1ExpressionTag == Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1ExpressionTag.INT) {
        this.int = new Z(this._io, this, this._root);
      }
      if (this.micheline016PtmumbaiMichelsonV1ExpressionTag == Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1ExpressionTag.STRING) {
        this.string = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.micheline016PtmumbaiMichelsonV1ExpressionTag == Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1ExpressionTag.SEQUENCE) {
        this.sequence = new Sequence0(this._io, this, this._root);
      }
      if (this.micheline016PtmumbaiMichelsonV1ExpressionTag == Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1ExpressionTag.PRIM__NO_ARGS__NO_ANNOTS) {
        this.primNoArgsNoAnnots = new Id016PtmumbaiMichelsonV1Primitives(this._io, this, this._root);
      }
      if (this.micheline016PtmumbaiMichelsonV1ExpressionTag == Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1ExpressionTag.PRIM__NO_ARGS__SOME_ANNOTS) {
        this.primNoArgsSomeAnnots = new PrimNoArgsSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline016PtmumbaiMichelsonV1ExpressionTag == Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1ExpressionTag.PRIM__1_ARG__NO_ANNOTS) {
        this.prim1ArgNoAnnots = new Prim1ArgNoAnnots(this._io, this, this._root);
      }
      if (this.micheline016PtmumbaiMichelsonV1ExpressionTag == Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1ExpressionTag.PRIM__1_ARG__SOME_ANNOTS) {
        this.prim1ArgSomeAnnots = new Prim1ArgSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline016PtmumbaiMichelsonV1ExpressionTag == Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1ExpressionTag.PRIM__2_ARGS__NO_ANNOTS) {
        this.prim2ArgsNoAnnots = new Prim2ArgsNoAnnots(this._io, this, this._root);
      }
      if (this.micheline016PtmumbaiMichelsonV1ExpressionTag == Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1ExpressionTag.PRIM__2_ARGS__SOME_ANNOTS) {
        this.prim2ArgsSomeAnnots = new Prim2ArgsSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline016PtmumbaiMichelsonV1ExpressionTag == Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1ExpressionTag.PRIM__GENERIC) {
        this.primGeneric = new PrimGeneric(this._io, this, this._root);
      }
      if (this.micheline016PtmumbaiMichelsonV1ExpressionTag == Id016PtmumbaiOperationContents.Micheline016PtmumbaiMichelsonV1ExpressionTag.BYTES) {
        this.bytes = new BytesDynUint30(this._io, this, this._root);
      }
    }

    return Micheline016PtmumbaiMichelsonV1Expression;
  })();

  var ZkRollupOrigination = Id016PtmumbaiOperationContents.ZkRollupOrigination = (function() {
    function ZkRollupOrigination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ZkRollupOrigination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.publicParameters = new BytesDynUint30(this._io, this, this._root);
      this.circuitsInfo = new CircuitsInfo0(this._io, this, this._root);
      this.initState = new InitState0(this._io, this, this._root);
      this.nbOps = new Int31(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return ZkRollupOrigination;
  })();

  var Op2 = Id016PtmumbaiOperationContents.Op2 = (function() {
    function Op2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op2.prototype._read = function() {
      this.id016PtmumbaiInlinedEndorsement = new Id016PtmumbaiInlinedEndorsement(this._io, this, this._root);
    }

    return Op2;
  })();

  var Id016PtmumbaiInlinedEndorsement = Id016PtmumbaiOperationContents.Id016PtmumbaiInlinedEndorsement = (function() {
    function Id016PtmumbaiInlinedEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiInlinedEndorsement.prototype._read = function() {
      this.id016PtmumbaiInlinedEndorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id016PtmumbaiInlinedEndorsementMempoolContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id016PtmumbaiOperationContents.Bool.TRUE) {
        this.signature = this._io.readBytesFull();
      }
    }

    return Id016PtmumbaiInlinedEndorsement;
  })();

  var TxRollupReturnBond = Id016PtmumbaiOperationContents.TxRollupReturnBond = (function() {
    function TxRollupReturnBond(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupReturnBond.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id016PtmumbaiTxRollupId(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupReturnBond;
  })();

  var Dissection = Id016PtmumbaiOperationContents.Dissection = (function() {
    function Dissection(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Dissection.prototype._read = function() {
      this.dissectionEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.dissectionEntries.push(new DissectionEntries(this._io, this, this._root));
        i++;
      }
    }

    return Dissection;
  })();

  var TicketsInfo0 = Id016PtmumbaiOperationContents.TicketsInfo0 = (function() {
    function TicketsInfo0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TicketsInfo0.prototype._read = function() {
      this.lenTicketsInfo = this._io.readU4be();
      if (!(this.lenTicketsInfo <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenTicketsInfo, this._io, "/types/tickets_info_0/seq/0");
      }
      this._raw_ticketsInfo = this._io.readBytes(this.lenTicketsInfo);
      var _io__raw_ticketsInfo = new KaitaiStream(this._raw_ticketsInfo);
      this.ticketsInfo = new TicketsInfo(_io__raw_ticketsInfo, this, this._root);
    }

    return TicketsInfo0;
  })();

  var TxRollupFinalizeCommitment = Id016PtmumbaiOperationContents.TxRollupFinalizeCommitment = (function() {
    function TxRollupFinalizeCommitment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupFinalizeCommitment.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id016PtmumbaiTxRollupId(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupFinalizeCommitment;
  })();

  var PrivatePis = Id016PtmumbaiOperationContents.PrivatePis = (function() {
    function PrivatePis(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrivatePis.prototype._read = function() {
      this.privatePisEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.privatePisEntries.push(new PrivatePisEntries(this._io, this, this._root));
        i++;
      }
    }

    return PrivatePis;
  })();

  var UpdateConsensusKey = Id016PtmumbaiOperationContents.UpdateConsensusKey = (function() {
    function UpdateConsensusKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    UpdateConsensusKey.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.pk = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return UpdateConsensusKey;
  })();

  var TxRollupOrigination = Id016PtmumbaiOperationContents.TxRollupOrigination = (function() {
    function TxRollupOrigination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupOrigination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return TxRollupOrigination;
  })();

  var Prim1ArgNoAnnots = Id016PtmumbaiOperationContents.Prim1ArgNoAnnots = (function() {
    function Prim1ArgNoAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim1ArgNoAnnots.prototype._read = function() {
      this.prim = new Id016PtmumbaiMichelsonV1Primitives(this._io, this, this._root);
      this.arg = new Micheline016PtmumbaiMichelsonV1Expression(this._io, this, this._root);
    }

    return Prim1ArgNoAnnots;
  })();

  var MessageResultPath = Id016PtmumbaiOperationContents.MessageResultPath = (function() {
    function MessageResultPath(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageResultPath.prototype._read = function() {
      this.messageResultPathEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messageResultPathEntries.push(new MessageResultPathEntries(this._io, this, this._root));
        i++;
      }
    }

    return MessageResultPath;
  })();

  var Id016PtmumbaiLiquidityBakingToggleVote = Id016PtmumbaiOperationContents.Id016PtmumbaiLiquidityBakingToggleVote = (function() {
    function Id016PtmumbaiLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiLiquidityBakingToggleVote.prototype._read = function() {
      this.id016PtmumbaiLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id016PtmumbaiLiquidityBakingToggleVote;
  })();

  var Update = Id016PtmumbaiOperationContents.Update = (function() {
    function Update(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Update.prototype._read = function() {
      this.pendingPis = new PendingPis0(this._io, this, this._root);
      this.privatePis = new PrivatePis0(this._io, this, this._root);
      this.feePi = new NewState0(this._io, this, this._root);
      this.proof = new BytesDynUint30(this._io, this, this._root);
    }

    return Update;
  })();

  var SequenceEntries = Id016PtmumbaiOperationContents.SequenceEntries = (function() {
    function SequenceEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SequenceEntries.prototype._read = function() {
      this.sequenceElt = new Micheline016PtmumbaiMichelsonV1Expression(this._io, this, this._root);
    }

    return SequenceEntries;
  })();

  var Int31 = Id016PtmumbaiOperationContents.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id016PtmumbaiOperationContents.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Prim2ArgsNoAnnots = Id016PtmumbaiOperationContents.Prim2ArgsNoAnnots = (function() {
    function Prim2ArgsNoAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim2ArgsNoAnnots.prototype._read = function() {
      this.prim = new Id016PtmumbaiMichelsonV1Primitives(this._io, this, this._root);
      this.arg1 = new Micheline016PtmumbaiMichelsonV1Expression(this._io, this, this._root);
      this.arg2 = new Micheline016PtmumbaiMichelsonV1Expression(this._io, this, this._root);
    }

    return Prim2ArgsNoAnnots;
  })();

  var MessageResultPath0 = Id016PtmumbaiOperationContents.MessageResultPath0 = (function() {
    function MessageResultPath0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageResultPath0.prototype._read = function() {
      this.lenMessageResultPath = this._io.readU4be();
      if (!(this.lenMessageResultPath <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessageResultPath, this._io, "/types/message_result_path_0/seq/0");
      }
      this._raw_messageResultPath = this._io.readBytes(this.lenMessageResultPath);
      var _io__raw_messageResultPath = new KaitaiStream(this._raw_messageResultPath);
      this.messageResultPath = new MessageResultPath(_io__raw_messageResultPath, this, this._root);
    }

    return MessageResultPath0;
  })();

  var Id016PtmumbaiOperationAlphaContents = Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContents = (function() {
    function Id016PtmumbaiOperationAlphaContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiOperationAlphaContents.prototype._read = function() {
      this.id016PtmumbaiOperationAlphaContentsTag = this._io.readU1();
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.ENDORSEMENT) {
        this.endorsement = new Endorsement(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.PREENDORSEMENT) {
        this.preendorsement = new Preendorsement(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.DAL_ATTESTATION) {
        this.dalAttestation = new DalAttestation(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.SEED_NONCE_REVELATION) {
        this.seedNonceRevelation = new SeedNonceRevelation(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.VDF_REVELATION) {
        this.vdfRevelation = new Solution(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.DOUBLE_ENDORSEMENT_EVIDENCE) {
        this.doubleEndorsementEvidence = new DoubleEndorsementEvidence(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.DOUBLE_PREENDORSEMENT_EVIDENCE) {
        this.doublePreendorsementEvidence = new DoublePreendorsementEvidence(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.DOUBLE_BAKING_EVIDENCE) {
        this.doubleBakingEvidence = new DoubleBakingEvidence(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.ACTIVATE_ACCOUNT) {
        this.activateAccount = new ActivateAccount(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.PROPOSALS) {
        this.proposals = new Proposals1(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.BALLOT) {
        this.ballot = new Ballot(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.REVEAL) {
        this.reveal = new Reveal(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.SET_DEPOSITS_LIMIT) {
        this.setDepositsLimit = new SetDepositsLimit(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.INCREASE_PAID_STORAGE) {
        this.increasePaidStorage = new IncreasePaidStorage(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.UPDATE_CONSENSUS_KEY) {
        this.updateConsensusKey = new UpdateConsensusKey(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.DRAIN_DELEGATE) {
        this.drainDelegate = new DrainDelegate(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.FAILING_NOOP) {
        this.failingNoop = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.REGISTER_GLOBAL_CONSTANT) {
        this.registerGlobalConstant = new RegisterGlobalConstant(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.TX_ROLLUP_ORIGINATION) {
        this.txRollupOrigination = new TxRollupOrigination(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.TX_ROLLUP_SUBMIT_BATCH) {
        this.txRollupSubmitBatch = new TxRollupSubmitBatch(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.TX_ROLLUP_COMMIT) {
        this.txRollupCommit = new TxRollupCommit(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.TX_ROLLUP_RETURN_BOND) {
        this.txRollupReturnBond = new TxRollupReturnBond(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.TX_ROLLUP_FINALIZE_COMMITMENT) {
        this.txRollupFinalizeCommitment = new TxRollupFinalizeCommitment(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.TX_ROLLUP_REMOVE_COMMITMENT) {
        this.txRollupRemoveCommitment = new TxRollupRemoveCommitment(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.TX_ROLLUP_REJECTION) {
        this.txRollupRejection = new TxRollupRejection(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.TX_ROLLUP_DISPATCH_TICKETS) {
        this.txRollupDispatchTickets = new TxRollupDispatchTickets(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.TRANSFER_TICKET) {
        this.transferTicket = new TransferTicket(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.DAL_PUBLISH_SLOT_HEADER) {
        this.dalPublishSlotHeader = new DalPublishSlotHeader(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.SMART_ROLLUP_ORIGINATE) {
        this.smartRollupOriginate = new SmartRollupOriginate(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.SMART_ROLLUP_ADD_MESSAGES) {
        this.smartRollupAddMessages = new SmartRollupAddMessages(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.SMART_ROLLUP_CEMENT) {
        this.smartRollupCement = new SmartRollupCement(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.SMART_ROLLUP_PUBLISH) {
        this.smartRollupPublish = new SmartRollupPublish(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.SMART_ROLLUP_REFUTE) {
        this.smartRollupRefute = new SmartRollupRefute(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.SMART_ROLLUP_TIMEOUT) {
        this.smartRollupTimeout = new SmartRollupTimeout(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.SMART_ROLLUP_EXECUTE_OUTBOX_MESSAGE) {
        this.smartRollupExecuteOutboxMessage = new SmartRollupExecuteOutboxMessage(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.SMART_ROLLUP_RECOVER_BOND) {
        this.smartRollupRecoverBond = new SmartRollupRecoverBond(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.ZK_ROLLUP_ORIGINATION) {
        this.zkRollupOrigination = new ZkRollupOrigination(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.ZK_ROLLUP_PUBLISH) {
        this.zkRollupPublish = new ZkRollupPublish(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationAlphaContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiOperationAlphaContentsTag.ZK_ROLLUP_UPDATE) {
        this.zkRollupUpdate = new ZkRollupUpdate(this._io, this, this._root);
      }
    }

    return Id016PtmumbaiOperationAlphaContents;
  })();

  var MessagePathEntries = Id016PtmumbaiOperationContents.MessagePathEntries = (function() {
    function MessagePathEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagePathEntries.prototype._read = function() {
      this.inboxListHash = this._io.readBytes(32);
    }

    return MessagePathEntries;
  })();

  var Start = Id016PtmumbaiOperationContents.Start = (function() {
    function Start(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Start.prototype._read = function() {
      this.playerCommitmentHash = this._io.readBytes(32);
      this.opponentCommitmentHash = this._io.readBytes(32);
    }

    return Start;
  })();

  var InitStateEntries = Id016PtmumbaiOperationContents.InitStateEntries = (function() {
    function InitStateEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InitStateEntries.prototype._read = function() {
      this.initStateElt = this._io.readBytes(32);
    }

    return InitStateEntries;
  })();

  var Id016PtmumbaiBlockHeaderAlphaFullHeader = Id016PtmumbaiOperationContents.Id016PtmumbaiBlockHeaderAlphaFullHeader = (function() {
    function Id016PtmumbaiBlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiBlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id016PtmumbaiBlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id016PtmumbaiBlockHeaderAlphaSignedContents = new Id016PtmumbaiBlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id016PtmumbaiBlockHeaderAlphaFullHeader;
  })();

  var ArgsEntries = Id016PtmumbaiOperationContents.ArgsEntries = (function() {
    function ArgsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ArgsEntries.prototype._read = function() {
      this.argsElt = new Micheline016PtmumbaiMichelsonV1Expression(this._io, this, this._root);
    }

    return ArgsEntries;
  })();

  var RawData0 = Id016PtmumbaiOperationContents.RawData0 = (function() {
    function RawData0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RawData0.prototype._read = function() {
      this.lenRawData = this._io.readU2be();
      if (!(this.lenRawData <= 4096)) {
        throw new KaitaiStream.ValidationGreaterThanError(4096, this.lenRawData, this._io, "/types/raw_data_0/seq/0");
      }
      this._raw_rawData = this._io.readBytes(this.lenRawData);
      var _io__raw_rawData = new KaitaiStream(this._raw_rawData);
      this.rawData = new RawData(_io__raw_rawData, this, this._root);
    }

    return RawData0;
  })();

  var Deposit = Id016PtmumbaiOperationContents.Deposit = (function() {
    function Deposit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposit.prototype._read = function() {
      this.sender = new PublicKeyHash(this._io, this, this._root);
      this.destination = this._io.readBytes(20);
      this.ticketHash = this._io.readBytes(32);
      this.amount = new Amount(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Deposit;
  })();

  var MessagePath = Id016PtmumbaiOperationContents.MessagePath = (function() {
    function MessagePath(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagePath.prototype._read = function() {
      this.messagePathEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messagePathEntries.push(new MessagePathEntries(this._io, this, this._root));
        i++;
      }
    }

    return MessagePath;
  })();

  var Args0 = Id016PtmumbaiOperationContents.Args0 = (function() {
    function Args0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Args0.prototype._read = function() {
      this.lenArgs = this._io.readU4be();
      if (!(this.lenArgs <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenArgs, this._io, "/types/args_0/seq/0");
      }
      this._raw_args = this._io.readBytes(this.lenArgs);
      var _io__raw_args = new KaitaiStream(this._raw_args);
      this.args = new Args(_io__raw_args, this, this._root);
    }

    return Args0;
  })();

  var Named0 = Id016PtmumbaiOperationContents.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id016PtmumbaiOperationContents.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.amount = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.destination = new Id016PtmumbaiContractId(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id016PtmumbaiOperationContents.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Transaction;
  })();

  var DalAttestation = Id016PtmumbaiOperationContents.DalAttestation = (function() {
    function DalAttestation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalAttestation.prototype._read = function() {
      this.attestor = new PublicKeyHash(this._io, this, this._root);
      this.attestation = new Z(this._io, this, this._root);
      this.level = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return DalAttestation;
  })();

  var Parameters = Id016PtmumbaiOperationContents.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id016PtmumbaiEntrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var Some = Id016PtmumbaiOperationContents.Some = (function() {
    function Some(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Some.prototype._read = function() {
      this.contents = new Micheline016PtmumbaiMichelsonV1Expression(this._io, this, this._root);
      this.ty = new Micheline016PtmumbaiMichelsonV1Expression(this._io, this, this._root);
      this.ticketer = new Id016PtmumbaiContractId(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Some;
  })();

  var Messages0 = Id016PtmumbaiOperationContents.Messages0 = (function() {
    function Messages0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Messages0.prototype._read = function() {
      this.lenMessages = this._io.readU4be();
      if (!(this.lenMessages <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessages, this._io, "/types/messages_0/seq/0");
      }
      this._raw_messages = this._io.readBytes(this.lenMessages);
      var _io__raw_messages = new KaitaiStream(this._raw_messages);
      this.messages = new Messages(_io__raw_messages, this, this._root);
    }

    return Messages0;
  })();

  var Prim2ArgsSomeAnnots = Id016PtmumbaiOperationContents.Prim2ArgsSomeAnnots = (function() {
    function Prim2ArgsSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim2ArgsSomeAnnots.prototype._read = function() {
      this.prim = new Id016PtmumbaiMichelsonV1Primitives(this._io, this, this._root);
      this.arg1 = new Micheline016PtmumbaiMichelsonV1Expression(this._io, this, this._root);
      this.arg2 = new Micheline016PtmumbaiMichelsonV1Expression(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return Prim2ArgsSomeAnnots;
  })();

  var Proposals1 = Id016PtmumbaiOperationContents.Proposals1 = (function() {
    function Proposals1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals1.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposals = new Proposals0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Proposals1;
  })();

  var TxRollupSubmitBatch = Id016PtmumbaiOperationContents.TxRollupSubmitBatch = (function() {
    function TxRollupSubmitBatch(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupSubmitBatch.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id016PtmumbaiTxRollupId(this._io, this, this._root);
      this.content = new BytesDynUint30(this._io, this, this._root);
      this.burnLimitTag = this._io.readU1();
      if (this.burnLimitTag == Id016PtmumbaiOperationContents.Bool.TRUE) {
        this.burnLimit = new Id016PtmumbaiMutez(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupSubmitBatch;
  })();

  var DissectionEntries = Id016PtmumbaiOperationContents.DissectionEntries = (function() {
    function DissectionEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DissectionEntries.prototype._read = function() {
      this.stateTag = this._io.readU1();
      if (this.stateTag == Id016PtmumbaiOperationContents.Bool.TRUE) {
        this.state = this._io.readBytes(32);
      }
      this.tick = new N(this._io, this, this._root);
    }

    return DissectionEntries;
  })();

  var TicketsInfo = Id016PtmumbaiOperationContents.TicketsInfo = (function() {
    function TicketsInfo(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TicketsInfo.prototype._read = function() {
      this.ticketsInfoEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.ticketsInfoEntries.push(new TicketsInfoEntries(this._io, this, this._root));
        i++;
      }
    }

    return TicketsInfo;
  })();

  var Id016PtmumbaiInlinedPreendorsementContents = Id016PtmumbaiOperationContents.Id016PtmumbaiInlinedPreendorsementContents = (function() {
    function Id016PtmumbaiInlinedPreendorsementContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiInlinedPreendorsementContents.prototype._read = function() {
      this.id016PtmumbaiInlinedPreendorsementContentsTag = this._io.readU1();
      if (this.id016PtmumbaiInlinedPreendorsementContentsTag == Id016PtmumbaiOperationContents.Id016PtmumbaiInlinedPreendorsementContentsTag.PREENDORSEMENT) {
        this.preendorsement = new Preendorsement(this._io, this, this._root);
      }
    }

    return Id016PtmumbaiInlinedPreendorsementContents;
  })();

  var Ballot = Id016PtmumbaiOperationContents.Ballot = (function() {
    function Ballot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Ballot.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposal = this._io.readBytes(32);
      this.ballot = this._io.readS1();
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Ballot;
  })();

  var Predecessor = Id016PtmumbaiOperationContents.Predecessor = (function() {
    function Predecessor(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Predecessor.prototype._read = function() {
      this.predecessorTag = this._io.readU1();
      if (this.predecessorTag == Id016PtmumbaiOperationContents.PredecessorTag.SOME) {
        this.some = this._io.readBytes(32);
      }
    }

    return Predecessor;
  })();

  var DalPublishSlotHeader = Id016PtmumbaiOperationContents.DalPublishSlotHeader = (function() {
    function DalPublishSlotHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalPublishSlotHeader.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.slotHeader = new SlotHeader(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return DalPublishSlotHeader;
  })();

  var PublicKeyHash = Id016PtmumbaiOperationContents.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id016PtmumbaiOperationContents.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id016PtmumbaiOperationContents.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id016PtmumbaiOperationContents.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id016PtmumbaiOperationContents.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var MessagePath0 = Id016PtmumbaiOperationContents.MessagePath0 = (function() {
    function MessagePath0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagePath0.prototype._read = function() {
      this.lenMessagePath = this._io.readU4be();
      if (!(this.lenMessagePath <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessagePath, this._io, "/types/message_path_0/seq/0");
      }
      this._raw_messagePath = this._io.readBytes(this.lenMessagePath);
      var _io__raw_messagePath = new KaitaiStream(this._raw_messagePath);
      this.messagePath = new MessagePath(_io__raw_messagePath, this, this._root);
    }

    return MessagePath0;
  })();

  var Id016PtmumbaiMutez = Id016PtmumbaiOperationContents.Id016PtmumbaiMutez = (function() {
    function Id016PtmumbaiMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiMutez.prototype._read = function() {
      this.id016PtmumbaiMutez = new N(this._io, this, this._root);
    }

    return Id016PtmumbaiMutez;
  })();

  var Op11 = Id016PtmumbaiOperationContents.Op11 = (function() {
    function Op11(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op11.prototype._read = function() {
      this.id016PtmumbaiInlinedPreendorsement = new Id016PtmumbaiInlinedPreendorsement(this._io, this, this._root);
    }

    return Op11;
  })();

  var Proposals = Id016PtmumbaiOperationContents.Proposals = (function() {
    function Proposals(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals.prototype._read = function() {
      this.proposalsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.proposalsEntries.push(new ProposalsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Proposals;
  })();

  var Id016PtmumbaiBlockHeaderAlphaUnsignedContents = Id016PtmumbaiOperationContents.Id016PtmumbaiBlockHeaderAlphaUnsignedContents = (function() {
    function Id016PtmumbaiBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id016PtmumbaiOperationContents.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id016PtmumbaiLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id016PtmumbaiBlockHeaderAlphaUnsignedContents;
  })();

  var Z = Id016PtmumbaiOperationContents.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  var OpEntries = Id016PtmumbaiOperationContents.OpEntries = (function() {
    function OpEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    OpEntries.prototype._read = function() {
      this.opEltField0 = new OpEltField0(this._io, this, this._root);
      this.opEltField1 = new OpEltField1(this._io, this, this._root);
    }

    return OpEntries;
  })();

  var ProposalsEntries = Id016PtmumbaiOperationContents.ProposalsEntries = (function() {
    function ProposalsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ProposalsEntries.prototype._read = function() {
      this.protocolHash = this._io.readBytes(32);
    }

    return ProposalsEntries;
  })();

  var Op1 = Id016PtmumbaiOperationContents.Op1 = (function() {
    function Op1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op1.prototype._read = function() {
      this.id016PtmumbaiInlinedEndorsement = new Id016PtmumbaiInlinedEndorsement(this._io, this, this._root);
    }

    return Op1;
  })();

  var ZkRollupPublish = Id016PtmumbaiOperationContents.ZkRollupPublish = (function() {
    function ZkRollupPublish(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ZkRollupPublish.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id016PtmumbaiMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.zkRollup = this._io.readBytes(20);
      this.op = new Op0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return ZkRollupPublish;
  })();

  var Op = Id016PtmumbaiOperationContents.Op = (function() {
    function Op(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op.prototype._read = function() {
      this.opEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.opEntries.push(new OpEntries(this._io, this, this._root));
        i++;
      }
    }

    return Op;
  })();

  var DoublePreendorsementEvidence = Id016PtmumbaiOperationContents.DoublePreendorsementEvidence = (function() {
    function DoublePreendorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoublePreendorsementEvidence.prototype._read = function() {
      this.op1 = new Op12(this._io, this, this._root);
      this.op2 = new Op22(this._io, this, this._root);
    }

    return DoublePreendorsementEvidence;
  })();

  return Id016PtmumbaiOperationContents;
})();
return Id016PtmumbaiOperationContents;
}));
