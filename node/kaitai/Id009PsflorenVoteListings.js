// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id009PsflorenVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 009-PsFLoren.vote.listings
 */

var Id009PsflorenVoteListings = (function() {
  Id009PsflorenVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id009PsflorenVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id009PsflorenVoteListings.prototype._read = function() {
    this.lenId009PsflorenVoteListings = this._io.readU4be();
    if (!(this.lenId009PsflorenVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId009PsflorenVoteListings, this._io, "/seq/0");
    }
    this._raw_id009PsflorenVoteListings = this._io.readBytes(this.lenId009PsflorenVoteListings);
    var _io__raw_id009PsflorenVoteListings = new KaitaiStream(this._raw_id009PsflorenVoteListings);
    this.id009PsflorenVoteListings = new Id009PsflorenVoteListings(_io__raw_id009PsflorenVoteListings, this, this._root);
  }

  var Id009PsflorenVoteListings = Id009PsflorenVoteListings.Id009PsflorenVoteListings = (function() {
    function Id009PsflorenVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenVoteListings.prototype._read = function() {
      this.id009PsflorenVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id009PsflorenVoteListingsEntries.push(new Id009PsflorenVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id009PsflorenVoteListings;
  })();

  var Id009PsflorenVoteListingsEntries = Id009PsflorenVoteListings.Id009PsflorenVoteListingsEntries = (function() {
    function Id009PsflorenVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.rolls = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id009PsflorenVoteListingsEntries;
  })();

  var PublicKeyHash = Id009PsflorenVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id009PsflorenVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id009PsflorenVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id009PsflorenVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id009PsflorenVoteListings;
})();
return Id009PsflorenVoteListings;
}));
