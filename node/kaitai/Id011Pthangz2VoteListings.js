// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id011Pthangz2VoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 011-PtHangz2.vote.listings
 */

var Id011Pthangz2VoteListings = (function() {
  Id011Pthangz2VoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id011Pthangz2VoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2VoteListings.prototype._read = function() {
    this.lenId011Pthangz2VoteListings = this._io.readU4be();
    if (!(this.lenId011Pthangz2VoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId011Pthangz2VoteListings, this._io, "/seq/0");
    }
    this._raw_id011Pthangz2VoteListings = this._io.readBytes(this.lenId011Pthangz2VoteListings);
    var _io__raw_id011Pthangz2VoteListings = new KaitaiStream(this._raw_id011Pthangz2VoteListings);
    this.id011Pthangz2VoteListings = new Id011Pthangz2VoteListings(_io__raw_id011Pthangz2VoteListings, this, this._root);
  }

  var Id011Pthangz2VoteListings = Id011Pthangz2VoteListings.Id011Pthangz2VoteListings = (function() {
    function Id011Pthangz2VoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2VoteListings.prototype._read = function() {
      this.id011Pthangz2VoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id011Pthangz2VoteListingsEntries.push(new Id011Pthangz2VoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id011Pthangz2VoteListings;
  })();

  var Id011Pthangz2VoteListingsEntries = Id011Pthangz2VoteListings.Id011Pthangz2VoteListingsEntries = (function() {
    function Id011Pthangz2VoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2VoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.rolls = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id011Pthangz2VoteListingsEntries;
  })();

  var PublicKeyHash = Id011Pthangz2VoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id011Pthangz2VoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id011Pthangz2VoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id011Pthangz2VoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id011Pthangz2VoteListings;
})();
return Id011Pthangz2VoteListings;
}));
