// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaSmartRollupReveal = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.smart_rollup.reveal
 */

var Id019PtparisaSmartRollupReveal = (function() {
  Id019PtparisaSmartRollupReveal.Id019PtparisaSmartRollupRevealTag = Object.freeze({
    REVEAL_RAW_DATA: 0,
    REVEAL_METADATA: 1,
    REQUEST_DAL_PAGE: 2,
    REVEAL_DAL_PARAMETERS: 3,

    0: "REVEAL_RAW_DATA",
    1: "REVEAL_METADATA",
    2: "REQUEST_DAL_PAGE",
    3: "REVEAL_DAL_PARAMETERS",
  });

  Id019PtparisaSmartRollupReveal.InputHashTag = Object.freeze({
    REVEAL_DATA_HASH_V0: 0,

    0: "REVEAL_DATA_HASH_V0",
  });

  function Id019PtparisaSmartRollupReveal(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaSmartRollupReveal.prototype._read = function() {
    this.id019PtparisaSmartRollupRevealTag = this._io.readU1();
    if (this.id019PtparisaSmartRollupRevealTag == Id019PtparisaSmartRollupReveal.Id019PtparisaSmartRollupRevealTag.REVEAL_RAW_DATA) {
      this.revealRawData = new InputHash(this._io, this, this._root);
    }
    if (this.id019PtparisaSmartRollupRevealTag == Id019PtparisaSmartRollupReveal.Id019PtparisaSmartRollupRevealTag.REQUEST_DAL_PAGE) {
      this.requestDalPage = new PageId(this._io, this, this._root);
    }
  }

  var InputHash = Id019PtparisaSmartRollupReveal.InputHash = (function() {
    function InputHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InputHash.prototype._read = function() {
      this.inputHashTag = this._io.readU1();
      if (this.inputHashTag == Id019PtparisaSmartRollupReveal.InputHashTag.REVEAL_DATA_HASH_V0) {
        this.revealDataHashV0 = this._io.readBytes(32);
      }
    }

    return InputHash;
  })();

  var PageId = Id019PtparisaSmartRollupReveal.PageId = (function() {
    function PageId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PageId.prototype._read = function() {
      this.publishedLevel = this._io.readS4be();
      this.slotIndex = this._io.readU1();
      this.pageIndex = this._io.readS2be();
    }

    return PageId;
  })();

  return Id019PtparisaSmartRollupReveal;
})();
return Id019PtparisaSmartRollupReveal;
}));
