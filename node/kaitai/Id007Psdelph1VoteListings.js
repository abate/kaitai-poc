// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id007Psdelph1VoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 007-PsDELPH1.vote.listings
 */

var Id007Psdelph1VoteListings = (function() {
  Id007Psdelph1VoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id007Psdelph1VoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1VoteListings.prototype._read = function() {
    this.lenId007Psdelph1VoteListings = this._io.readU4be();
    if (!(this.lenId007Psdelph1VoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId007Psdelph1VoteListings, this._io, "/seq/0");
    }
    this._raw_id007Psdelph1VoteListings = this._io.readBytes(this.lenId007Psdelph1VoteListings);
    var _io__raw_id007Psdelph1VoteListings = new KaitaiStream(this._raw_id007Psdelph1VoteListings);
    this.id007Psdelph1VoteListings = new Id007Psdelph1VoteListings(_io__raw_id007Psdelph1VoteListings, this, this._root);
  }

  var Id007Psdelph1VoteListings = Id007Psdelph1VoteListings.Id007Psdelph1VoteListings = (function() {
    function Id007Psdelph1VoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1VoteListings.prototype._read = function() {
      this.id007Psdelph1VoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id007Psdelph1VoteListingsEntries.push(new Id007Psdelph1VoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id007Psdelph1VoteListings;
  })();

  var Id007Psdelph1VoteListingsEntries = Id007Psdelph1VoteListings.Id007Psdelph1VoteListingsEntries = (function() {
    function Id007Psdelph1VoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1VoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.rolls = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id007Psdelph1VoteListingsEntries;
  })();

  var PublicKeyHash = Id007Psdelph1VoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id007Psdelph1VoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id007Psdelph1VoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id007Psdelph1VoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id007Psdelph1VoteListings;
})();
return Id007Psdelph1VoteListings;
}));
