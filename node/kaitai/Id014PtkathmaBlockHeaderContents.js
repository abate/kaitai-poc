// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id014PtkathmaBlockHeaderContents = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 014-PtKathma.block_header.contents
 */

var Id014PtkathmaBlockHeaderContents = (function() {
  Id014PtkathmaBlockHeaderContents.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id014PtkathmaBlockHeaderContents(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id014PtkathmaBlockHeaderContents.prototype._read = function() {
    this.id014PtkathmaBlockHeaderAlphaUnsignedContents = new Id014PtkathmaBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id014PtkathmaBlockHeaderAlphaUnsignedContents = Id014PtkathmaBlockHeaderContents.Id014PtkathmaBlockHeaderAlphaUnsignedContents = (function() {
    function Id014PtkathmaBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id014PtkathmaBlockHeaderContents.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id014PtkathmaLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id014PtkathmaBlockHeaderAlphaUnsignedContents;
  })();

  var Id014PtkathmaLiquidityBakingToggleVote = Id014PtkathmaBlockHeaderContents.Id014PtkathmaLiquidityBakingToggleVote = (function() {
    function Id014PtkathmaLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaLiquidityBakingToggleVote.prototype._read = function() {
      this.id014PtkathmaLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id014PtkathmaLiquidityBakingToggleVote;
  })();

  return Id014PtkathmaBlockHeaderContents;
})();
return Id014PtkathmaBlockHeaderContents;
}));
