// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id008Ptedo2zkContractBigMapDiff = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 008-PtEdo2Zk.contract.big_map_diff
 */

var Id008Ptedo2zkContractBigMapDiff = (function() {
  Id008Ptedo2zkContractBigMapDiff.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiffEltTag = Object.freeze({
    UPDATE: 0,
    REMOVE: 1,
    COPY: 2,
    ALLOC: 3,

    0: "UPDATE",
    1: "REMOVE",
    2: "COPY",
    3: "ALLOC",
  });

  Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkMichelsonV1Primitives = Object.freeze({
    PARAMETER: 0,
    STORAGE: 1,
    CODE: 2,
    FALSE: 3,
    ELT: 4,
    LEFT_0: 5,
    NONE: 6,
    PAIR_1: 7,
    RIGHT_0: 8,
    SOME: 9,
    TRUE: 10,
    UNIT_0: 11,
    PACK: 12,
    UNPACK: 13,
    BLAKE2B: 14,
    SHA256: 15,
    SHA512: 16,
    ABS: 17,
    ADD: 18,
    AMOUNT: 19,
    AND: 20,
    BALANCE: 21,
    CAR: 22,
    CDR: 23,
    CHECK_SIGNATURE: 24,
    COMPARE: 25,
    CONCAT: 26,
    CONS: 27,
    CREATE_ACCOUNT: 28,
    CREATE_CONTRACT: 29,
    IMPLICIT_ACCOUNT: 30,
    DIP: 31,
    DROP: 32,
    DUP: 33,
    EDIV: 34,
    EMPTY_MAP: 35,
    EMPTY_SET: 36,
    EQ: 37,
    EXEC: 38,
    FAILWITH: 39,
    GE: 40,
    GET: 41,
    GT: 42,
    HASH_KEY: 43,
    IF: 44,
    IF_CONS: 45,
    IF_LEFT: 46,
    IF_NONE: 47,
    INT_0: 48,
    LAMBDA_0: 49,
    LE: 50,
    LEFT: 51,
    LOOP: 52,
    LSL: 53,
    LSR: 54,
    LT: 55,
    MAP_0: 56,
    MEM: 57,
    MUL: 58,
    NEG: 59,
    NEQ: 60,
    NIL: 61,
    NONE_0: 62,
    NOT: 63,
    NOW: 64,
    OR_0: 65,
    PAIR_0: 66,
    PUSH: 67,
    RIGHT: 68,
    SIZE: 69,
    SOME_0: 70,
    SOURCE: 71,
    SENDER: 72,
    SELF: 73,
    STEPS_TO_QUOTA: 74,
    SUB: 75,
    SWAP: 76,
    TRANSFER_TOKENS: 77,
    SET_DELEGATE: 78,
    UNIT_1: 79,
    UPDATE: 80,
    XOR: 81,
    ITER: 82,
    LOOP_LEFT: 83,
    ADDRESS_0: 84,
    CONTRACT_0: 85,
    ISNAT: 86,
    CAST: 87,
    RENAME: 88,
    BOOL: 89,
    CONTRACT: 90,
    INT: 91,
    KEY: 92,
    KEY_HASH: 93,
    LAMBDA: 94,
    LIST: 95,
    MAP: 96,
    BIG_MAP: 97,
    NAT: 98,
    OPTION: 99,
    OR: 100,
    PAIR: 101,
    SET: 102,
    SIGNATURE: 103,
    STRING: 104,
    BYTES: 105,
    MUTEZ: 106,
    TIMESTAMP: 107,
    UNIT: 108,
    OPERATION: 109,
    ADDRESS: 110,
    SLICE: 111,
    DIG: 112,
    DUG: 113,
    EMPTY_BIG_MAP: 114,
    APPLY: 115,
    CHAIN_ID: 116,
    CHAIN_ID_0: 117,
    LEVEL: 118,
    SELF_ADDRESS: 119,
    NEVER: 120,
    NEVER_0: 121,
    UNPAIR: 122,
    VOTING_POWER: 123,
    TOTAL_VOTING_POWER: 124,
    KECCAK: 125,
    SHA3: 126,
    PAIRING_CHECK: 127,
    BLS12_381_G1: 128,
    BLS12_381_G2: 129,
    BLS12_381_FR: 130,
    SAPLING_STATE: 131,
    SAPLING_TRANSACTION: 132,
    SAPLING_EMPTY_STATE: 133,
    SAPLING_VERIFY_UPDATE: 134,
    TICKET: 135,
    TICKET_0: 136,
    READ_TICKET: 137,
    SPLIT_TICKET: 138,
    JOIN_TICKETS: 139,
    GET_AND_UPDATE: 140,

    0: "PARAMETER",
    1: "STORAGE",
    2: "CODE",
    3: "FALSE",
    4: "ELT",
    5: "LEFT_0",
    6: "NONE",
    7: "PAIR_1",
    8: "RIGHT_0",
    9: "SOME",
    10: "TRUE",
    11: "UNIT_0",
    12: "PACK",
    13: "UNPACK",
    14: "BLAKE2B",
    15: "SHA256",
    16: "SHA512",
    17: "ABS",
    18: "ADD",
    19: "AMOUNT",
    20: "AND",
    21: "BALANCE",
    22: "CAR",
    23: "CDR",
    24: "CHECK_SIGNATURE",
    25: "COMPARE",
    26: "CONCAT",
    27: "CONS",
    28: "CREATE_ACCOUNT",
    29: "CREATE_CONTRACT",
    30: "IMPLICIT_ACCOUNT",
    31: "DIP",
    32: "DROP",
    33: "DUP",
    34: "EDIV",
    35: "EMPTY_MAP",
    36: "EMPTY_SET",
    37: "EQ",
    38: "EXEC",
    39: "FAILWITH",
    40: "GE",
    41: "GET",
    42: "GT",
    43: "HASH_KEY",
    44: "IF",
    45: "IF_CONS",
    46: "IF_LEFT",
    47: "IF_NONE",
    48: "INT_0",
    49: "LAMBDA_0",
    50: "LE",
    51: "LEFT",
    52: "LOOP",
    53: "LSL",
    54: "LSR",
    55: "LT",
    56: "MAP_0",
    57: "MEM",
    58: "MUL",
    59: "NEG",
    60: "NEQ",
    61: "NIL",
    62: "NONE_0",
    63: "NOT",
    64: "NOW",
    65: "OR_0",
    66: "PAIR_0",
    67: "PUSH",
    68: "RIGHT",
    69: "SIZE",
    70: "SOME_0",
    71: "SOURCE",
    72: "SENDER",
    73: "SELF",
    74: "STEPS_TO_QUOTA",
    75: "SUB",
    76: "SWAP",
    77: "TRANSFER_TOKENS",
    78: "SET_DELEGATE",
    79: "UNIT_1",
    80: "UPDATE",
    81: "XOR",
    82: "ITER",
    83: "LOOP_LEFT",
    84: "ADDRESS_0",
    85: "CONTRACT_0",
    86: "ISNAT",
    87: "CAST",
    88: "RENAME",
    89: "BOOL",
    90: "CONTRACT",
    91: "INT",
    92: "KEY",
    93: "KEY_HASH",
    94: "LAMBDA",
    95: "LIST",
    96: "MAP",
    97: "BIG_MAP",
    98: "NAT",
    99: "OPTION",
    100: "OR",
    101: "PAIR",
    102: "SET",
    103: "SIGNATURE",
    104: "STRING",
    105: "BYTES",
    106: "MUTEZ",
    107: "TIMESTAMP",
    108: "UNIT",
    109: "OPERATION",
    110: "ADDRESS",
    111: "SLICE",
    112: "DIG",
    113: "DUG",
    114: "EMPTY_BIG_MAP",
    115: "APPLY",
    116: "CHAIN_ID",
    117: "CHAIN_ID_0",
    118: "LEVEL",
    119: "SELF_ADDRESS",
    120: "NEVER",
    121: "NEVER_0",
    122: "UNPAIR",
    123: "VOTING_POWER",
    124: "TOTAL_VOTING_POWER",
    125: "KECCAK",
    126: "SHA3",
    127: "PAIRING_CHECK",
    128: "BLS12_381_G1",
    129: "BLS12_381_G2",
    130: "BLS12_381_FR",
    131: "SAPLING_STATE",
    132: "SAPLING_TRANSACTION",
    133: "SAPLING_EMPTY_STATE",
    134: "SAPLING_VERIFY_UPDATE",
    135: "TICKET",
    136: "TICKET_0",
    137: "READ_TICKET",
    138: "SPLIT_TICKET",
    139: "JOIN_TICKETS",
    140: "GET_AND_UPDATE",
  });

  Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag = Object.freeze({
    INT: 0,
    STRING: 1,
    SEQUENCE: 2,
    PRIM__NO_ARGS__NO_ANNOTS: 3,
    PRIM__NO_ARGS__SOME_ANNOTS: 4,
    PRIM__1_ARG__NO_ANNOTS: 5,
    PRIM__1_ARG__SOME_ANNOTS: 6,
    PRIM__2_ARGS__NO_ANNOTS: 7,
    PRIM__2_ARGS__SOME_ANNOTS: 8,
    PRIM__GENERIC: 9,
    BYTES: 10,

    0: "INT",
    1: "STRING",
    2: "SEQUENCE",
    3: "PRIM__NO_ARGS__NO_ANNOTS",
    4: "PRIM__NO_ARGS__SOME_ANNOTS",
    5: "PRIM__1_ARG__NO_ANNOTS",
    6: "PRIM__1_ARG__SOME_ANNOTS",
    7: "PRIM__2_ARGS__NO_ANNOTS",
    8: "PRIM__2_ARGS__SOME_ANNOTS",
    9: "PRIM__GENERIC",
    10: "BYTES",
  });

  function Id008Ptedo2zkContractBigMapDiff(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkContractBigMapDiff.prototype._read = function() {
    this.lenId008Ptedo2zkContractBigMapDiff = this._io.readU4be();
    if (!(this.lenId008Ptedo2zkContractBigMapDiff <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId008Ptedo2zkContractBigMapDiff, this._io, "/seq/0");
    }
    this._raw_id008Ptedo2zkContractBigMapDiff = this._io.readBytes(this.lenId008Ptedo2zkContractBigMapDiff);
    var _io__raw_id008Ptedo2zkContractBigMapDiff = new KaitaiStream(this._raw_id008Ptedo2zkContractBigMapDiff);
    this.id008Ptedo2zkContractBigMapDiff = new Id008Ptedo2zkContractBigMapDiff(_io__raw_id008Ptedo2zkContractBigMapDiff, this, this._root);
  }

  var Alloc = Id008Ptedo2zkContractBigMapDiff.Alloc = (function() {
    function Alloc(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Alloc.prototype._read = function() {
      this.bigMap = new Z(this._io, this, this._root);
      this.keyType = new Micheline008Ptedo2zkMichelsonV1Expression(this._io, this, this._root);
      this.valueType = new Micheline008Ptedo2zkMichelsonV1Expression(this._io, this, this._root);
    }

    return Alloc;
  })();

  var Prim1ArgSomeAnnots = Id008Ptedo2zkContractBigMapDiff.Prim1ArgSomeAnnots = (function() {
    function Prim1ArgSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim1ArgSomeAnnots.prototype._read = function() {
      this.prim = new Id008Ptedo2zkMichelsonV1Primitives(this._io, this, this._root);
      this.arg = new Micheline008Ptedo2zkMichelsonV1Expression(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return Prim1ArgSomeAnnots;
  })();

  var PrimNoArgsSomeAnnots = Id008Ptedo2zkContractBigMapDiff.PrimNoArgsSomeAnnots = (function() {
    function PrimNoArgsSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrimNoArgsSomeAnnots.prototype._read = function() {
      this.prim = new Id008Ptedo2zkMichelsonV1Primitives(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return PrimNoArgsSomeAnnots;
  })();

  var Id008Ptedo2zkContractBigMapDiffEntries = Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiffEntries = (function() {
    function Id008Ptedo2zkContractBigMapDiffEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkContractBigMapDiffEntries.prototype._read = function() {
      this.id008Ptedo2zkContractBigMapDiffEltTag = this._io.readU1();
      if (this.id008Ptedo2zkContractBigMapDiffEltTag == Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiffEltTag.UPDATE) {
        this.update = new Update(this._io, this, this._root);
      }
      if (this.id008Ptedo2zkContractBigMapDiffEltTag == Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiffEltTag.REMOVE) {
        this.remove = new Z(this._io, this, this._root);
      }
      if (this.id008Ptedo2zkContractBigMapDiffEltTag == Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiffEltTag.COPY) {
        this.copy = new Copy(this._io, this, this._root);
      }
      if (this.id008Ptedo2zkContractBigMapDiffEltTag == Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiffEltTag.ALLOC) {
        this.alloc = new Alloc(this._io, this, this._root);
      }
    }

    return Id008Ptedo2zkContractBigMapDiffEntries;
  })();

  var PrimGeneric = Id008Ptedo2zkContractBigMapDiff.PrimGeneric = (function() {
    function PrimGeneric(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrimGeneric.prototype._read = function() {
      this.prim = new Id008Ptedo2zkMichelsonV1Primitives(this._io, this, this._root);
      this.args = new Args0(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return PrimGeneric;
  })();

  var Sequence0 = Id008Ptedo2zkContractBigMapDiff.Sequence0 = (function() {
    function Sequence0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sequence0.prototype._read = function() {
      this.lenSequence = this._io.readU4be();
      if (!(this.lenSequence <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenSequence, this._io, "/types/sequence_0/seq/0");
      }
      this._raw_sequence = this._io.readBytes(this.lenSequence);
      var _io__raw_sequence = new KaitaiStream(this._raw_sequence);
      this.sequence = new Sequence(_io__raw_sequence, this, this._root);
    }

    return Sequence0;
  })();

  var Sequence = Id008Ptedo2zkContractBigMapDiff.Sequence = (function() {
    function Sequence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sequence.prototype._read = function() {
      this.sequenceEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.sequenceEntries.push(new SequenceEntries(this._io, this, this._root));
        i++;
      }
    }

    return Sequence;
  })();

  var BytesDynUint30 = Id008Ptedo2zkContractBigMapDiff.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Copy = Id008Ptedo2zkContractBigMapDiff.Copy = (function() {
    function Copy(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Copy.prototype._read = function() {
      this.sourceBigMap = new Z(this._io, this, this._root);
      this.destinationBigMap = new Z(this._io, this, this._root);
    }

    return Copy;
  })();

  var Args = Id008Ptedo2zkContractBigMapDiff.Args = (function() {
    function Args(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Args.prototype._read = function() {
      this.argsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.argsEntries.push(new ArgsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Args;
  })();

  var Micheline008Ptedo2zkMichelsonV1Expression = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression = (function() {
    function Micheline008Ptedo2zkMichelsonV1Expression(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Micheline008Ptedo2zkMichelsonV1Expression.prototype._read = function() {
      this.micheline008Ptedo2zkMichelsonV1ExpressionTag = this._io.readU1();
      if (this.micheline008Ptedo2zkMichelsonV1ExpressionTag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.INT) {
        this.int = new Z(this._io, this, this._root);
      }
      if (this.micheline008Ptedo2zkMichelsonV1ExpressionTag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.STRING) {
        this.string = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.micheline008Ptedo2zkMichelsonV1ExpressionTag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.SEQUENCE) {
        this.sequence = new Sequence0(this._io, this, this._root);
      }
      if (this.micheline008Ptedo2zkMichelsonV1ExpressionTag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.PRIM__NO_ARGS__NO_ANNOTS) {
        this.primNoArgsNoAnnots = new Id008Ptedo2zkMichelsonV1Primitives(this._io, this, this._root);
      }
      if (this.micheline008Ptedo2zkMichelsonV1ExpressionTag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.PRIM__NO_ARGS__SOME_ANNOTS) {
        this.primNoArgsSomeAnnots = new PrimNoArgsSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline008Ptedo2zkMichelsonV1ExpressionTag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.PRIM__1_ARG__NO_ANNOTS) {
        this.prim1ArgNoAnnots = new Prim1ArgNoAnnots(this._io, this, this._root);
      }
      if (this.micheline008Ptedo2zkMichelsonV1ExpressionTag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.PRIM__1_ARG__SOME_ANNOTS) {
        this.prim1ArgSomeAnnots = new Prim1ArgSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline008Ptedo2zkMichelsonV1ExpressionTag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.PRIM__2_ARGS__NO_ANNOTS) {
        this.prim2ArgsNoAnnots = new Prim2ArgsNoAnnots(this._io, this, this._root);
      }
      if (this.micheline008Ptedo2zkMichelsonV1ExpressionTag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.PRIM__2_ARGS__SOME_ANNOTS) {
        this.prim2ArgsSomeAnnots = new Prim2ArgsSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline008Ptedo2zkMichelsonV1ExpressionTag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.PRIM__GENERIC) {
        this.primGeneric = new PrimGeneric(this._io, this, this._root);
      }
      if (this.micheline008Ptedo2zkMichelsonV1ExpressionTag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.BYTES) {
        this.bytes = new BytesDynUint30(this._io, this, this._root);
      }
    }

    return Micheline008Ptedo2zkMichelsonV1Expression;
  })();

  var Prim1ArgNoAnnots = Id008Ptedo2zkContractBigMapDiff.Prim1ArgNoAnnots = (function() {
    function Prim1ArgNoAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim1ArgNoAnnots.prototype._read = function() {
      this.prim = new Id008Ptedo2zkMichelsonV1Primitives(this._io, this, this._root);
      this.arg = new Micheline008Ptedo2zkMichelsonV1Expression(this._io, this, this._root);
    }

    return Prim1ArgNoAnnots;
  })();

  var Update = Id008Ptedo2zkContractBigMapDiff.Update = (function() {
    function Update(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Update.prototype._read = function() {
      this.bigMap = new Z(this._io, this, this._root);
      this.keyHash = this._io.readBytes(32);
      this.key = new Micheline008Ptedo2zkMichelsonV1Expression(this._io, this, this._root);
      this.valueTag = this._io.readU1();
      if (this.valueTag == Id008Ptedo2zkContractBigMapDiff.Bool.TRUE) {
        this.value = new Micheline008Ptedo2zkMichelsonV1Expression(this._io, this, this._root);
      }
    }

    return Update;
  })();

  var SequenceEntries = Id008Ptedo2zkContractBigMapDiff.SequenceEntries = (function() {
    function SequenceEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SequenceEntries.prototype._read = function() {
      this.sequenceElt = new Micheline008Ptedo2zkMichelsonV1Expression(this._io, this, this._root);
    }

    return SequenceEntries;
  })();

  var NChunk = Id008Ptedo2zkContractBigMapDiff.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Prim2ArgsNoAnnots = Id008Ptedo2zkContractBigMapDiff.Prim2ArgsNoAnnots = (function() {
    function Prim2ArgsNoAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim2ArgsNoAnnots.prototype._read = function() {
      this.prim = new Id008Ptedo2zkMichelsonV1Primitives(this._io, this, this._root);
      this.arg1 = new Micheline008Ptedo2zkMichelsonV1Expression(this._io, this, this._root);
      this.arg2 = new Micheline008Ptedo2zkMichelsonV1Expression(this._io, this, this._root);
    }

    return Prim2ArgsNoAnnots;
  })();

  var Id008Ptedo2zkMichelsonV1Primitives = Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkMichelsonV1Primitives = (function() {
    function Id008Ptedo2zkMichelsonV1Primitives(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkMichelsonV1Primitives.prototype._read = function() {
      this.id008Ptedo2zkMichelsonV1Primitives = this._io.readU1();
    }

    return Id008Ptedo2zkMichelsonV1Primitives;
  })();

  var ArgsEntries = Id008Ptedo2zkContractBigMapDiff.ArgsEntries = (function() {
    function ArgsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ArgsEntries.prototype._read = function() {
      this.argsElt = new Micheline008Ptedo2zkMichelsonV1Expression(this._io, this, this._root);
    }

    return ArgsEntries;
  })();

  var Id008Ptedo2zkContractBigMapDiff = Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiff = (function() {
    function Id008Ptedo2zkContractBigMapDiff(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkContractBigMapDiff.prototype._read = function() {
      this.id008Ptedo2zkContractBigMapDiffEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id008Ptedo2zkContractBigMapDiffEntries.push(new Id008Ptedo2zkContractBigMapDiffEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id008Ptedo2zkContractBigMapDiff;
  })();

  var Args0 = Id008Ptedo2zkContractBigMapDiff.Args0 = (function() {
    function Args0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Args0.prototype._read = function() {
      this.lenArgs = this._io.readU4be();
      if (!(this.lenArgs <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenArgs, this._io, "/types/args_0/seq/0");
      }
      this._raw_args = this._io.readBytes(this.lenArgs);
      var _io__raw_args = new KaitaiStream(this._raw_args);
      this.args = new Args(_io__raw_args, this, this._root);
    }

    return Args0;
  })();

  var Prim2ArgsSomeAnnots = Id008Ptedo2zkContractBigMapDiff.Prim2ArgsSomeAnnots = (function() {
    function Prim2ArgsSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim2ArgsSomeAnnots.prototype._read = function() {
      this.prim = new Id008Ptedo2zkMichelsonV1Primitives(this._io, this, this._root);
      this.arg1 = new Micheline008Ptedo2zkMichelsonV1Expression(this._io, this, this._root);
      this.arg2 = new Micheline008Ptedo2zkMichelsonV1Expression(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return Prim2ArgsSomeAnnots;
  })();

  var Z = Id008Ptedo2zkContractBigMapDiff.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  return Id008Ptedo2zkContractBigMapDiff;
})();
return Id008Ptedo2zkContractBigMapDiff;
}));
