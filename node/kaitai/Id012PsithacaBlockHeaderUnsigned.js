// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id012PsithacaBlockHeaderUnsigned = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 012-Psithaca.block_header.unsigned
 */

var Id012PsithacaBlockHeaderUnsigned = (function() {
  Id012PsithacaBlockHeaderUnsigned.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id012PsithacaBlockHeaderUnsigned(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id012PsithacaBlockHeaderUnsigned.prototype._read = function() {
    this.id012PsithacaBlockHeaderUnsigned = new BlockHeaderShell(this._io, this, null);
    this.id012PsithacaBlockHeaderAlphaUnsignedContents = new Id012PsithacaBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id012PsithacaBlockHeaderAlphaUnsignedContents = Id012PsithacaBlockHeaderUnsigned.Id012PsithacaBlockHeaderAlphaUnsignedContents = (function() {
    function Id012PsithacaBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id012PsithacaBlockHeaderUnsigned.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingEscapeVote = this._io.readU1();
    }

    return Id012PsithacaBlockHeaderAlphaUnsignedContents;
  })();

  return Id012PsithacaBlockHeaderUnsigned;
})();
return Id012PsithacaBlockHeaderUnsigned;
}));
