// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.SaplingTransactionCommitmentValue = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: sapling.transaction.commitment_value
 */

var SaplingTransactionCommitmentValue = (function() {
  function SaplingTransactionCommitmentValue(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  SaplingTransactionCommitmentValue.prototype._read = function() {
    this.saplingTransactionCommitmentValue = this._io.readBytes(32);
  }

  return SaplingTransactionCommitmentValue;
})();
return SaplingTransactionCommitmentValue;
}));
