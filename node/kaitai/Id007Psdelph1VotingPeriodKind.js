// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id007Psdelph1VotingPeriodKind = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 007-PsDELPH1.voting_period.kind
 */

var Id007Psdelph1VotingPeriodKind = (function() {
  Id007Psdelph1VotingPeriodKind.Id007Psdelph1VotingPeriodKindTag = Object.freeze({
    PROPOSAL: 0,
    TESTING_VOTE: 1,
    TESTING: 2,
    PROMOTION_VOTE: 3,

    0: "PROPOSAL",
    1: "TESTING_VOTE",
    2: "TESTING",
    3: "PROMOTION_VOTE",
  });

  function Id007Psdelph1VotingPeriodKind(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1VotingPeriodKind.prototype._read = function() {
    this.id007Psdelph1VotingPeriodKindTag = this._io.readU1();
  }

  return Id007Psdelph1VotingPeriodKind;
})();
return Id007Psdelph1VotingPeriodKind;
}));
