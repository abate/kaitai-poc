// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './TimestampSystem'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./TimestampSystem'));
  } else {
    root.P2pPointPoolEvent = factory(root.KaitaiStream, root.TimestampSystem);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, TimestampSystem) {
/**
 * Encoding id: p2p_point.pool_event
 * Description: Events happening during maintenance of and operations on a peer point pool (such as connections, disconnections, connection requests).
 */

var P2pPointPoolEvent = (function() {
  P2pPointPoolEvent.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  P2pPointPoolEvent.P2pPointPoolEventField1Tag = Object.freeze({
    OUTGOING_REQUEST: 0,
    ACCEPTING_REQUEST: 1,
    REJECTING_REQUEST: 2,
    REJECTING_REJECTED: 3,
    CONNECTION_ESTABLISHED: 4,
    DISCONNECTION: 5,
    EXTERNAL_DISCONNECTION: 6,

    0: "OUTGOING_REQUEST",
    1: "ACCEPTING_REQUEST",
    2: "REJECTING_REQUEST",
    3: "REJECTING_REJECTED",
    4: "CONNECTION_ESTABLISHED",
    5: "DISCONNECTION",
    6: "EXTERNAL_DISCONNECTION",
  });

  function P2pPointPoolEvent(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  P2pPointPoolEvent.prototype._read = function() {
    this.p2pPointPoolEventField0 = new TimestampSystem(this._io, this, null);
    this.p2pPointPoolEventField1 = new P2pPointPoolEventField1(this._io, this, this._root);
  }

  var P2pPointPoolEventField1 = P2pPointPoolEvent.P2pPointPoolEventField1 = (function() {
    function P2pPointPoolEventField1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    P2pPointPoolEventField1.prototype._read = function() {
      this.p2pPointPoolEventField1Tag = this._io.readU1();
      if (this.p2pPointPoolEventField1Tag == P2pPointPoolEvent.P2pPointPoolEventField1Tag.ACCEPTING_REQUEST) {
        this.acceptingRequest = this._io.readBytes(16);
      }
      if (this.p2pPointPoolEventField1Tag == P2pPointPoolEvent.P2pPointPoolEventField1Tag.REJECTING_REQUEST) {
        this.rejectingRequest = this._io.readBytes(16);
      }
      if (this.p2pPointPoolEventField1Tag == P2pPointPoolEvent.P2pPointPoolEventField1Tag.REJECTING_REJECTED) {
        this.rejectingRejected = new RejectingRejected(this._io, this, this._root);
      }
      if (this.p2pPointPoolEventField1Tag == P2pPointPoolEvent.P2pPointPoolEventField1Tag.CONNECTION_ESTABLISHED) {
        this.connectionEstablished = this._io.readBytes(16);
      }
      if (this.p2pPointPoolEventField1Tag == P2pPointPoolEvent.P2pPointPoolEventField1Tag.DISCONNECTION) {
        this.disconnection = this._io.readBytes(16);
      }
      if (this.p2pPointPoolEventField1Tag == P2pPointPoolEvent.P2pPointPoolEventField1Tag.EXTERNAL_DISCONNECTION) {
        this.externalDisconnection = this._io.readBytes(16);
      }
    }

    return P2pPointPoolEventField1;
  })();

  var RejectingRejected = P2pPointPoolEvent.RejectingRejected = (function() {
    function RejectingRejected(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RejectingRejected.prototype._read = function() {
      this.p2pPeerIdTag = this._io.readU1();
      if (this.p2pPeerIdTag == P2pPointPoolEvent.Bool.TRUE) {
        this.p2pPeerId = this._io.readBytes(16);
      }
    }

    return RejectingRejected;
  })();

  return P2pPointPoolEvent;
})();
return P2pPointPoolEvent;
}));
