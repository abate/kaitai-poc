// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id005Psbabym1DelegateFrozenBalance = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 005-PsBabyM1.delegate.frozen_balance
 */

var Id005Psbabym1DelegateFrozenBalance = (function() {
  function Id005Psbabym1DelegateFrozenBalance(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1DelegateFrozenBalance.prototype._read = function() {
    this.deposit = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.fees = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.rewards = new Id005Psbabym1Mutez(this._io, this, this._root);
  }

  var Id005Psbabym1Mutez = Id005Psbabym1DelegateFrozenBalance.Id005Psbabym1Mutez = (function() {
    function Id005Psbabym1Mutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1Mutez.prototype._read = function() {
      this.id005Psbabym1Mutez = new N(this._io, this, this._root);
    }

    return Id005Psbabym1Mutez;
  })();

  var N = Id005Psbabym1DelegateFrozenBalance.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var NChunk = Id005Psbabym1DelegateFrozenBalance.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  return Id005Psbabym1DelegateFrozenBalance;
})();
return Id005Psbabym1DelegateFrozenBalance;
}));
