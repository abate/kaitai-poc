// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id006PscarthaOperationInternal = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 006-PsCARTHA.operation.internal
 */

var Id006PscarthaOperationInternal = (function() {
  Id006PscarthaOperationInternal.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id006PscarthaOperationInternal.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id006PscarthaOperationInternal.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id006PscarthaOperationInternal.Id006PscarthaEntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    255: "NAMED",
  });

  Id006PscarthaOperationInternal.Id006PscarthaOperationAlphaInternalOperationTag = Object.freeze({
    REVEAL: 0,
    TRANSACTION: 1,
    ORIGINATION: 2,
    DELEGATION: 3,

    0: "REVEAL",
    1: "TRANSACTION",
    2: "ORIGINATION",
    3: "DELEGATION",
  });

  Id006PscarthaOperationInternal.Id006PscarthaContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  function Id006PscarthaOperationInternal(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaOperationInternal.prototype._read = function() {
    this.id006PscarthaOperationAlphaInternalOperation = new Id006PscarthaOperationAlphaInternalOperation(this._io, this, this._root);
  }

  var Originated = Id006PscarthaOperationInternal.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var N = Id006PscarthaOperationInternal.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id006PscarthaContractId = Id006PscarthaOperationInternal.Id006PscarthaContractId = (function() {
    function Id006PscarthaContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaContractId.prototype._read = function() {
      this.id006PscarthaContractIdTag = this._io.readU1();
      if (this.id006PscarthaContractIdTag == Id006PscarthaOperationInternal.Id006PscarthaContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id006PscarthaContractIdTag == Id006PscarthaOperationInternal.Id006PscarthaContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id006PscarthaContractId;
  })();

  var Id006PscarthaOperationAlphaInternalOperation = Id006PscarthaOperationInternal.Id006PscarthaOperationAlphaInternalOperation = (function() {
    function Id006PscarthaOperationAlphaInternalOperation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaOperationAlphaInternalOperation.prototype._read = function() {
      this.source = new Id006PscarthaContractId(this._io, this, this._root);
      this.nonce = this._io.readU2be();
      this.id006PscarthaOperationAlphaInternalOperationTag = this._io.readU1();
      if (this.id006PscarthaOperationAlphaInternalOperationTag == Id006PscarthaOperationInternal.Id006PscarthaOperationAlphaInternalOperationTag.REVEAL) {
        this.reveal = new PublicKey(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationAlphaInternalOperationTag == Id006PscarthaOperationInternal.Id006PscarthaOperationAlphaInternalOperationTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationAlphaInternalOperationTag == Id006PscarthaOperationInternal.Id006PscarthaOperationAlphaInternalOperationTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationAlphaInternalOperationTag == Id006PscarthaOperationInternal.Id006PscarthaOperationAlphaInternalOperationTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Id006PscarthaOperationAlphaInternalOperation;
  })();

  var PublicKey = Id006PscarthaOperationInternal.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id006PscarthaOperationInternal.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id006PscarthaOperationInternal.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id006PscarthaOperationInternal.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var Named = Id006PscarthaOperationInternal.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Delegation = Id006PscarthaOperationInternal.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id006PscarthaOperationInternal.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var BytesDynUint30 = Id006PscarthaOperationInternal.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Id006PscarthaEntrypoint = Id006PscarthaOperationInternal.Id006PscarthaEntrypoint = (function() {
    function Id006PscarthaEntrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaEntrypoint.prototype._read = function() {
      this.id006PscarthaEntrypointTag = this._io.readU1();
      if (this.id006PscarthaEntrypointTag == Id006PscarthaOperationInternal.Id006PscarthaEntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id006PscarthaEntrypoint;
  })();

  var Origination = Id006PscarthaOperationInternal.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.balance = new Id006PscarthaMutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id006PscarthaOperationInternal.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id006PscarthaScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var Id006PscarthaMutez = Id006PscarthaOperationInternal.Id006PscarthaMutez = (function() {
    function Id006PscarthaMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaMutez.prototype._read = function() {
      this.id006PscarthaMutez = new N(this._io, this, this._root);
    }

    return Id006PscarthaMutez;
  })();

  var NChunk = Id006PscarthaOperationInternal.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Named0 = Id006PscarthaOperationInternal.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id006PscarthaOperationInternal.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.amount = new Id006PscarthaMutez(this._io, this, this._root);
      this.destination = new Id006PscarthaContractId(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id006PscarthaOperationInternal.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Transaction;
  })();

  var Id006PscarthaScriptedContracts = Id006PscarthaOperationInternal.Id006PscarthaScriptedContracts = (function() {
    function Id006PscarthaScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id006PscarthaScriptedContracts;
  })();

  var Parameters = Id006PscarthaOperationInternal.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id006PscarthaEntrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var PublicKeyHash = Id006PscarthaOperationInternal.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id006PscarthaOperationInternal.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id006PscarthaOperationInternal.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id006PscarthaOperationInternal.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id006PscarthaOperationInternal;
})();
return Id006PscarthaOperationInternal;
}));
