// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id015PtlimaptVoteBallot = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 015-PtLimaPt.vote.ballot
 */

var Id015PtlimaptVoteBallot = (function() {
  function Id015PtlimaptVoteBallot(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id015PtlimaptVoteBallot.prototype._read = function() {
    this.id015PtlimaptVoteBallot = this._io.readS1();
  }

  return Id015PtlimaptVoteBallot;
})();
return Id015PtlimaptVoteBallot;
}));
