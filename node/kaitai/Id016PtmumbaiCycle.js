// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id016PtmumbaiCycle = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 016-PtMumbai.cycle
 */

var Id016PtmumbaiCycle = (function() {
  function Id016PtmumbaiCycle(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiCycle.prototype._read = function() {
    this.id016PtmumbaiCycle = this._io.readS4be();
  }

  return Id016PtmumbaiCycle;
})();
return Id016PtmumbaiCycle;
}));
