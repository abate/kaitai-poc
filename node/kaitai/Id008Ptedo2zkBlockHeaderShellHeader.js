// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id008Ptedo2zkBlockHeaderShellHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 008-PtEdo2Zk.block_header.shell_header
 */

var Id008Ptedo2zkBlockHeaderShellHeader = (function() {
  function Id008Ptedo2zkBlockHeaderShellHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkBlockHeaderShellHeader.prototype._read = function() {
    this.id008Ptedo2zkBlockHeaderShellHeader = new BlockHeaderShell(this._io, this, null);
  }

  return Id008Ptedo2zkBlockHeaderShellHeader;
})();
return Id008Ptedo2zkBlockHeaderShellHeader;
}));
