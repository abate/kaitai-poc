// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Mempool = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: mempool
 * Description: A batch of operation. This format is used to gossip operations between peers.
 */

var Mempool = (function() {
  function Mempool(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Mempool.prototype._read = function() {
    this.knownValid = new KnownValid0(this._io, this, this._root);
    this.pending = new Pending1(this._io, this, this._root);
  }

  var Pending0 = Mempool.Pending0 = (function() {
    function Pending0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Pending0.prototype._read = function() {
      this.lenPending = this._io.readU4be();
      if (!(this.lenPending <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenPending, this._io, "/types/pending_0/seq/0");
      }
      this._raw_pending = this._io.readBytes(this.lenPending);
      var _io__raw_pending = new KaitaiStream(this._raw_pending);
      this.pending = new Pending(_io__raw_pending, this, this._root);
    }

    return Pending0;
  })();

  var KnownValid = Mempool.KnownValid = (function() {
    function KnownValid(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    KnownValid.prototype._read = function() {
      this.knownValidEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.knownValidEntries.push(new KnownValidEntries(this._io, this, this._root));
        i++;
      }
    }

    return KnownValid;
  })();

  var Pending1 = Mempool.Pending1 = (function() {
    function Pending1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Pending1.prototype._read = function() {
      this.lenPending = this._io.readU4be();
      if (!(this.lenPending <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenPending, this._io, "/types/pending_1/seq/0");
      }
      this._raw_pending = this._io.readBytes(this.lenPending);
      var _io__raw_pending = new KaitaiStream(this._raw_pending);
      this.pending = new Pending0(_io__raw_pending, this, this._root);
    }

    return Pending1;
  })();

  var KnownValid0 = Mempool.KnownValid0 = (function() {
    function KnownValid0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    KnownValid0.prototype._read = function() {
      this.lenKnownValid = this._io.readU4be();
      if (!(this.lenKnownValid <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenKnownValid, this._io, "/types/known_valid_0/seq/0");
      }
      this._raw_knownValid = this._io.readBytes(this.lenKnownValid);
      var _io__raw_knownValid = new KaitaiStream(this._raw_knownValid);
      this.knownValid = new KnownValid(_io__raw_knownValid, this, this._root);
    }

    return KnownValid0;
  })();

  var PendingEntries = Mempool.PendingEntries = (function() {
    function PendingEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PendingEntries.prototype._read = function() {
      this.operationHash = this._io.readBytes(32);
    }

    return PendingEntries;
  })();

  var Pending = Mempool.Pending = (function() {
    function Pending(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Pending.prototype._read = function() {
      this.pendingEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.pendingEntries.push(new PendingEntries(this._io, this, this._root));
        i++;
      }
    }

    return Pending;
  })();

  var KnownValidEntries = Mempool.KnownValidEntries = (function() {
    function KnownValidEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    KnownValidEntries.prototype._read = function() {
      this.operationHash = this._io.readBytes(32);
    }

    return KnownValidEntries;
  })();

  return Mempool;
})();
return Mempool;
}));
