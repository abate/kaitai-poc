// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './Operation'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./Operation'));
  } else {
    root.Id016PtmumbaiOperationRaw = factory(root.KaitaiStream, root.Operation);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, Operation) {
/**
 * Encoding id: 016-PtMumbai.operation.raw
 */

var Id016PtmumbaiOperationRaw = (function() {
  function Id016PtmumbaiOperationRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiOperationRaw.prototype._read = function() {
    this.id016PtmumbaiOperationRaw = new Operation(this._io, this, null);
  }

  return Id016PtmumbaiOperationRaw;
})();
return Id016PtmumbaiOperationRaw;
}));
