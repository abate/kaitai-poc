// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id012PsithacaPeriod = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 012-Psithaca.period
 */

var Id012PsithacaPeriod = (function() {
  function Id012PsithacaPeriod(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id012PsithacaPeriod.prototype._read = function() {
    this.id012PsithacaPeriod = this._io.readS8be();
  }

  return Id012PsithacaPeriod;
})();
return Id012PsithacaPeriod;
}));
