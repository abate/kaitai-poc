// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.GroundUint16 = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: ground.uint16
 * Description: Unsigned 16 bit integers
 */

var GroundUint16 = (function() {
  function GroundUint16(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  GroundUint16.prototype._read = function() {
    this.groundUint16 = this._io.readU2be();
  }

  return GroundUint16;
})();
return GroundUint16;
}));
