// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.P2pPointState = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: p2p_point.state
 * Description: The state a connection to a peer point can be in: requested (connection open from here), accepted (handshake), running (connection already established), disconnected (no connection).
 */

var P2pPointState = (function() {
  P2pPointState.P2pPointStateTag = Object.freeze({
    REQUESTED: 0,
    ACCEPTED: 1,
    RUNNING: 2,
    DISCONNECTED: 3,

    0: "REQUESTED",
    1: "ACCEPTED",
    2: "RUNNING",
    3: "DISCONNECTED",
  });

  function P2pPointState(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  P2pPointState.prototype._read = function() {
    this.p2pPointStateTag = this._io.readU1();
    if (this.p2pPointStateTag == P2pPointState.P2pPointStateTag.ACCEPTED) {
      this.accepted = this._io.readBytes(16);
    }
    if (this.p2pPointStateTag == P2pPointState.P2pPointStateTag.RUNNING) {
      this.running = this._io.readBytes(16);
    }
  }

  return P2pPointState;
})();
return P2pPointState;
}));
