// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id005Psbabym1BlockHeaderContents = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 005-PsBabyM1.block_header.contents
 */

var Id005Psbabym1BlockHeaderContents = (function() {
  Id005Psbabym1BlockHeaderContents.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id005Psbabym1BlockHeaderContents(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1BlockHeaderContents.prototype._read = function() {
    this.id005Psbabym1BlockHeaderAlphaUnsignedContents = new Id005Psbabym1BlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id005Psbabym1BlockHeaderAlphaUnsignedContents = Id005Psbabym1BlockHeaderContents.Id005Psbabym1BlockHeaderAlphaUnsignedContents = (function() {
    function Id005Psbabym1BlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1BlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id005Psbabym1BlockHeaderContents.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
    }

    return Id005Psbabym1BlockHeaderAlphaUnsignedContents;
  })();

  return Id005Psbabym1BlockHeaderContents;
})();
return Id005Psbabym1BlockHeaderContents;
}));
