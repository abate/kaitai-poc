// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id012PsithacaReceiptBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 012-Psithaca.receipt.balance_updates
 */

var Id012PsithacaReceiptBalanceUpdates = (function() {
  Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag = Object.freeze({
    CONTRACT: 0,
    LEGACY_REWARDS: 1,
    BLOCK_FEES: 2,
    LEGACY_DEPOSITS: 3,
    DEPOSITS: 4,
    NONCE_REVELATION_REWARDS: 5,
    DOUBLE_SIGNING_EVIDENCE_REWARDS: 6,
    ENDORSING_REWARDS: 7,
    BAKING_REWARDS: 8,
    BAKING_BONUSES: 9,
    LEGACY_FEES: 10,
    STORAGE_FEES: 11,
    DOUBLE_SIGNING_PUNISHMENTS: 12,
    LOST_ENDORSING_REWARDS: 13,
    LIQUIDITY_BAKING_SUBSIDIES: 14,
    BURNED: 15,
    COMMITMENTS: 16,
    BOOTSTRAP: 17,
    INVOICE: 18,
    INITIAL_COMMITMENTS: 19,
    MINTED: 20,

    0: "CONTRACT",
    1: "LEGACY_REWARDS",
    2: "BLOCK_FEES",
    3: "LEGACY_DEPOSITS",
    4: "DEPOSITS",
    5: "NONCE_REVELATION_REWARDS",
    6: "DOUBLE_SIGNING_EVIDENCE_REWARDS",
    7: "ENDORSING_REWARDS",
    8: "BAKING_REWARDS",
    9: "BAKING_BONUSES",
    10: "LEGACY_FEES",
    11: "STORAGE_FEES",
    12: "DOUBLE_SIGNING_PUNISHMENTS",
    13: "LOST_ENDORSING_REWARDS",
    14: "LIQUIDITY_BAKING_SUBSIDIES",
    15: "BURNED",
    16: "COMMITMENTS",
    17: "BOOTSTRAP",
    18: "INVOICE",
    19: "INITIAL_COMMITMENTS",
    20: "MINTED",
  });

  Id012PsithacaReceiptBalanceUpdates.Id012PsithacaContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id012PsithacaReceiptBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id012PsithacaReceiptBalanceUpdates.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id012PsithacaReceiptBalanceUpdates.OriginTag = Object.freeze({
    BLOCK_APPLICATION: 0,
    PROTOCOL_MIGRATION: 1,
    SUBSIDY: 2,
    SIMULATION: 3,

    0: "BLOCK_APPLICATION",
    1: "PROTOCOL_MIGRATION",
    2: "SUBSIDY",
    3: "SIMULATION",
  });

  function Id012PsithacaReceiptBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id012PsithacaReceiptBalanceUpdates.prototype._read = function() {
    this.id012PsithacaOperationMetadataAlphaBalanceUpdates = new Id012PsithacaOperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Id012PsithacaOperationMetadataAlphaBalanceUpdate = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceUpdate = (function() {
    function Id012PsithacaOperationMetadataAlphaBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaOperationMetadataAlphaBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id012PsithacaOperationMetadataAlphaBalanceUpdate;
  })();

  var Id012PsithacaOperationMetadataAlphaBalanceUpdatesEntries = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id012PsithacaOperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaOperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id012PsithacaOperationMetadataAlphaBalance = new Id012PsithacaOperationMetadataAlphaBalance(this._io, this, this._root);
      this.id012PsithacaOperationMetadataAlphaBalanceUpdate = new Id012PsithacaOperationMetadataAlphaBalanceUpdate(this._io, this, this._root);
      this.id012PsithacaOperationMetadataAlphaUpdateOrigin = new Id012PsithacaOperationMetadataAlphaUpdateOrigin(this._io, this, this._root);
    }

    return Id012PsithacaOperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var Id012PsithacaOperationMetadataAlphaUpdateOrigin = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaUpdateOrigin = (function() {
    function Id012PsithacaOperationMetadataAlphaUpdateOrigin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaOperationMetadataAlphaUpdateOrigin.prototype._read = function() {
      this.origin = this._io.readU1();
    }

    return Id012PsithacaOperationMetadataAlphaUpdateOrigin;
  })();

  var Id012PsithacaContractId = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaContractId = (function() {
    function Id012PsithacaContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaContractId.prototype._read = function() {
      this.id012PsithacaContractIdTag = this._io.readU1();
      if (this.id012PsithacaContractIdTag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id012PsithacaContractIdTag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id012PsithacaContractId;
  })();

  var Originated = Id012PsithacaReceiptBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var LostEndorsingRewards = Id012PsithacaReceiptBalanceUpdates.LostEndorsingRewards = (function() {
    function LostEndorsingRewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LostEndorsingRewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.participation = this._io.readU1();
      this.revelation = this._io.readU1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return LostEndorsingRewards;
  })();

  var LegacyDeposits = Id012PsithacaReceiptBalanceUpdates.LegacyDeposits = (function() {
    function LegacyDeposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LegacyDeposits.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return LegacyDeposits;
  })();

  var Id012PsithacaOperationMetadataAlphaBalance = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalance = (function() {
    function Id012PsithacaOperationMetadataAlphaBalance(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaOperationMetadataAlphaBalance.prototype._read = function() {
      this.id012PsithacaOperationMetadataAlphaBalanceTag = this._io.readU1();
      if (this.id012PsithacaOperationMetadataAlphaBalanceTag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.CONTRACT) {
        this.contract = new Id012PsithacaContractId(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationMetadataAlphaBalanceTag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.LEGACY_REWARDS) {
        this.legacyRewards = new LegacyRewards(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationMetadataAlphaBalanceTag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.LEGACY_DEPOSITS) {
        this.legacyDeposits = new LegacyDeposits(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationMetadataAlphaBalanceTag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.DEPOSITS) {
        this.deposits = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationMetadataAlphaBalanceTag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.LEGACY_FEES) {
        this.legacyFees = new LegacyFees(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationMetadataAlphaBalanceTag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.LOST_ENDORSING_REWARDS) {
        this.lostEndorsingRewards = new LostEndorsingRewards(this._io, this, this._root);
      }
      if (this.id012PsithacaOperationMetadataAlphaBalanceTag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.COMMITMENTS) {
        this.commitments = this._io.readBytes(20);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id012PsithacaOperationMetadataAlphaBalance;
  })();

  var LegacyRewards = Id012PsithacaReceiptBalanceUpdates.LegacyRewards = (function() {
    function LegacyRewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LegacyRewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return LegacyRewards;
  })();

  var Id012PsithacaOperationMetadataAlphaBalanceUpdates0 = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id012PsithacaOperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaOperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId012PsithacaOperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId012PsithacaOperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId012PsithacaOperationMetadataAlphaBalanceUpdates, this._io, "/types/id_012__psithaca__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id012PsithacaOperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId012PsithacaOperationMetadataAlphaBalanceUpdates);
      var _io__raw_id012PsithacaOperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id012PsithacaOperationMetadataAlphaBalanceUpdates);
      this.id012PsithacaOperationMetadataAlphaBalanceUpdates = new Id012PsithacaOperationMetadataAlphaBalanceUpdates(_io__raw_id012PsithacaOperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id012PsithacaOperationMetadataAlphaBalanceUpdates0;
  })();

  var LegacyFees = Id012PsithacaReceiptBalanceUpdates.LegacyFees = (function() {
    function LegacyFees(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LegacyFees.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return LegacyFees;
  })();

  var Id012PsithacaOperationMetadataAlphaBalanceUpdates = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceUpdates = (function() {
    function Id012PsithacaOperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaOperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id012PsithacaOperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id012PsithacaOperationMetadataAlphaBalanceUpdatesEntries.push(new Id012PsithacaOperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id012PsithacaOperationMetadataAlphaBalanceUpdates;
  })();

  var PublicKeyHash = Id012PsithacaReceiptBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id012PsithacaReceiptBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id012PsithacaReceiptBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id012PsithacaReceiptBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id012PsithacaReceiptBalanceUpdates;
})();
return Id012PsithacaReceiptBalanceUpdates;
}));
