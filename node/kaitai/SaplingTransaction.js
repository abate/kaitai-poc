// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './SaplingTransactionInput', './SaplingTransactionCommitmentHash', './SaplingTransactionOutput', './SaplingTransactionBindingSig'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./SaplingTransactionInput'), require('./SaplingTransactionCommitmentHash'), require('./SaplingTransactionOutput'), require('./SaplingTransactionBindingSig'));
  } else {
    root.SaplingTransaction = factory(root.KaitaiStream, root.SaplingTransactionInput, root.SaplingTransactionCommitmentHash, root.SaplingTransactionOutput, root.SaplingTransactionBindingSig);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, SaplingTransactionInput, SaplingTransactionCommitmentHash, SaplingTransactionOutput, SaplingTransactionBindingSig) {
/**
 * Encoding id: sapling.transaction
 * Description: A Sapling transaction with inputs, outputs, balance, root, bound_data and binding sig.
 */

var SaplingTransaction = (function() {
  function SaplingTransaction(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  SaplingTransaction.prototype._read = function() {
    this.inputs = new Inputs0(this._io, this, this._root);
    this.outputs = new Outputs0(this._io, this, this._root);
    this.bindingSig = new SaplingTransactionBindingSig(this._io, this, null);
    this.balance = this._io.readS8be();
    this.root = new SaplingTransactionCommitmentHash(this._io, this, null);
    this.boundData = new BytesDynUint30(this._io, this, this._root);
  }

  var Inputs0 = SaplingTransaction.Inputs0 = (function() {
    function Inputs0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Inputs0.prototype._read = function() {
      this.lenInputs = this._io.readU4be();
      if (!(this.lenInputs <= 1833216)) {
        throw new KaitaiStream.ValidationGreaterThanError(1833216, this.lenInputs, this._io, "/types/inputs_0/seq/0");
      }
      this._raw_inputs = this._io.readBytes(this.lenInputs);
      var _io__raw_inputs = new KaitaiStream(this._raw_inputs);
      this.inputs = new Inputs(_io__raw_inputs, this, this._root);
    }

    return Inputs0;
  })();

  var Outputs = SaplingTransaction.Outputs = (function() {
    function Outputs(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Outputs.prototype._read = function() {
      this.outputsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.outputsEntries.push(new OutputsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Outputs;
  })();

  var InputsEntries = SaplingTransaction.InputsEntries = (function() {
    function InputsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InputsEntries.prototype._read = function() {
      this.inputsElt = new SaplingTransactionInput(this._io, this, null);
    }

    return InputsEntries;
  })();

  var Inputs = SaplingTransaction.Inputs = (function() {
    function Inputs(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Inputs.prototype._read = function() {
      this.inputsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.inputsEntries.push(new InputsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Inputs;
  })();

  var BytesDynUint30 = SaplingTransaction.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Outputs0 = SaplingTransaction.Outputs0 = (function() {
    function Outputs0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Outputs0.prototype._read = function() {
      this.lenOutputs = this._io.readU4be();
      if (!(this.lenOutputs <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOutputs, this._io, "/types/outputs_0/seq/0");
      }
      this._raw_outputs = this._io.readBytes(this.lenOutputs);
      var _io__raw_outputs = new KaitaiStream(this._raw_outputs);
      this.outputs = new Outputs(_io__raw_outputs, this, this._root);
    }

    return Outputs0;
  })();

  var OutputsEntries = SaplingTransaction.OutputsEntries = (function() {
    function OutputsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    OutputsEntries.prototype._read = function() {
      this.outputsElt = new SaplingTransactionOutput(this._io, this, null);
    }

    return OutputsEntries;
  })();

  return SaplingTransaction;
})();
return SaplingTransaction;
}));
