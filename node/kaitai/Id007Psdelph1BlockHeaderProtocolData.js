// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id007Psdelph1BlockHeaderProtocolData = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 007-PsDELPH1.block_header.protocol_data
 */

var Id007Psdelph1BlockHeaderProtocolData = (function() {
  Id007Psdelph1BlockHeaderProtocolData.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id007Psdelph1BlockHeaderProtocolData(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1BlockHeaderProtocolData.prototype._read = function() {
    this.id007Psdelph1BlockHeaderAlphaSignedContents = new Id007Psdelph1BlockHeaderAlphaSignedContents(this._io, this, this._root);
  }

  var Id007Psdelph1BlockHeaderAlphaSignedContents = Id007Psdelph1BlockHeaderProtocolData.Id007Psdelph1BlockHeaderAlphaSignedContents = (function() {
    function Id007Psdelph1BlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1BlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id007Psdelph1BlockHeaderAlphaUnsignedContents = new Id007Psdelph1BlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id007Psdelph1BlockHeaderAlphaSignedContents;
  })();

  var Id007Psdelph1BlockHeaderAlphaUnsignedContents = Id007Psdelph1BlockHeaderProtocolData.Id007Psdelph1BlockHeaderAlphaUnsignedContents = (function() {
    function Id007Psdelph1BlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1BlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id007Psdelph1BlockHeaderProtocolData.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
    }

    return Id007Psdelph1BlockHeaderAlphaUnsignedContents;
  })();

  return Id007Psdelph1BlockHeaderProtocolData;
})();
return Id007Psdelph1BlockHeaderProtocolData;
}));
