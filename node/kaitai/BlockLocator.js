// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeader'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeader'));
  } else {
    root.BlockLocator = factory(root.KaitaiStream, root.BlockHeader);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeader) {
/**
 * Encoding id: block_locator
 * Description: A sparse block locator à la Bitcoin
 */

var BlockLocator = (function() {
  function BlockLocator(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  BlockLocator.prototype._read = function() {
    this.currentHead = new CurrentHead0(this._io, this, this._root);
    this.history = [];
    var i = 0;
    while (!this._io.isEof()) {
      this.history.push(new HistoryEntries(this._io, this, this._root));
      i++;
    }
  }

  var CurrentHead = BlockLocator.CurrentHead = (function() {
    function CurrentHead(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CurrentHead.prototype._read = function() {
      this.currentHead = new BlockHeader(this._io, this, null);
    }

    return CurrentHead;
  })();

  var CurrentHead0 = BlockLocator.CurrentHead0 = (function() {
    function CurrentHead0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CurrentHead0.prototype._read = function() {
      this.lenCurrentHead = this._io.readU4be();
      if (!(this.lenCurrentHead <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCurrentHead, this._io, "/types/current_head_0/seq/0");
      }
      this._raw_currentHead = this._io.readBytes(this.lenCurrentHead);
      var _io__raw_currentHead = new KaitaiStream(this._raw_currentHead);
      this.currentHead = new CurrentHead(_io__raw_currentHead, this, this._root);
    }

    return CurrentHead0;
  })();

  var HistoryEntries = BlockLocator.HistoryEntries = (function() {
    function HistoryEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    HistoryEntries.prototype._read = function() {
      this.blockHash = this._io.readBytes(32);
    }

    return HistoryEntries;
  })();

  return BlockLocator;
})();
return BlockLocator;
}));
