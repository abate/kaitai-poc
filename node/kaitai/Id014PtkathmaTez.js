// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id014PtkathmaTez = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 014-PtKathma.tez
 */

var Id014PtkathmaTez = (function() {
  function Id014PtkathmaTez(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id014PtkathmaTez.prototype._read = function() {
    this.id014PtkathmaMutez = new Id014PtkathmaMutez(this._io, this, this._root);
  }

  var Id014PtkathmaMutez = Id014PtkathmaTez.Id014PtkathmaMutez = (function() {
    function Id014PtkathmaMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaMutez.prototype._read = function() {
      this.id014PtkathmaMutez = new N(this._io, this, this._root);
    }

    return Id014PtkathmaMutez;
  })();

  var N = Id014PtkathmaTez.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var NChunk = Id014PtkathmaTez.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  return Id014PtkathmaTez;
})();
return Id014PtkathmaTez;
}));
