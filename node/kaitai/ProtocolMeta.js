// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.ProtocolMeta = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: protocol.meta
 * Description: Protocol metadata: the hash of the protocol, the expected environment version and the list of modules comprising the protocol.
 */

var ProtocolMeta = (function() {
  ProtocolMeta.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function ProtocolMeta(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  ProtocolMeta.prototype._read = function() {
    this.hashTag = this._io.readU1();
    if (this.hashTag == ProtocolMeta.Bool.TRUE) {
      this.hash = this._io.readBytes(32);
    }
    this.expectedEnvVersionTag = this._io.readU1();
    if (this.expectedEnvVersionTag == ProtocolMeta.Bool.TRUE) {
      this.expectedEnvVersion = new ProtocolEnvironmentVersion(this._io, this, this._root);
    }
    this.modules = new Modules0(this._io, this, this._root);
  }

  var ModulesEntries = ProtocolMeta.ModulesEntries = (function() {
    function ModulesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ModulesEntries.prototype._read = function() {
      this.modulesElt = new BytesDynUint30(this._io, this, this._root);
    }

    return ModulesEntries;
  })();

  var Modules0 = ProtocolMeta.Modules0 = (function() {
    function Modules0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Modules0.prototype._read = function() {
      this.lenModules = this._io.readU4be();
      if (!(this.lenModules <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenModules, this._io, "/types/modules_0/seq/0");
      }
      this._raw_modules = this._io.readBytes(this.lenModules);
      var _io__raw_modules = new KaitaiStream(this._raw_modules);
      this.modules = new Modules(_io__raw_modules, this, this._root);
    }

    return Modules0;
  })();

  var Modules = ProtocolMeta.Modules = (function() {
    function Modules(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Modules.prototype._read = function() {
      this.modulesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.modulesEntries.push(new ModulesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Modules;
  })();

  var BytesDynUint30 = ProtocolMeta.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var ProtocolEnvironmentVersion = ProtocolMeta.ProtocolEnvironmentVersion = (function() {
    function ProtocolEnvironmentVersion(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ProtocolEnvironmentVersion.prototype._read = function() {
      this.protocolEnvironmentVersion = this._io.readU2be();
    }

    return ProtocolEnvironmentVersion;
  })();

  /**
   * Used to force the hash of the protocol
   */

  /**
   * Modules comprising the protocol
   */

  return ProtocolMeta;
})();
return ProtocolMeta;
}));
