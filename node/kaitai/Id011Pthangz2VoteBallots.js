// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id011Pthangz2VoteBallots = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 011-PtHangz2.vote.ballots
 */

var Id011Pthangz2VoteBallots = (function() {
  function Id011Pthangz2VoteBallots(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2VoteBallots.prototype._read = function() {
    this.yay = this._io.readS4be();
    this.nay = this._io.readS4be();
    this.pass = this._io.readS4be();
  }

  return Id011Pthangz2VoteBallots;
})();
return Id011Pthangz2VoteBallots;
}));
