// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id009PsflorenReceiptBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 009-PsFLoren.receipt.balance_updates
 */

var Id009PsflorenReceiptBalanceUpdates = (function() {
  Id009PsflorenReceiptBalanceUpdates.Id009PsflorenContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceTag = Object.freeze({
    CONTRACT: 0,
    REWARDS: 1,
    FEES: 2,
    DEPOSITS: 3,

    0: "CONTRACT",
    1: "REWARDS",
    2: "FEES",
    3: "DEPOSITS",
  });

  Id009PsflorenReceiptBalanceUpdates.OriginTag = Object.freeze({
    BLOCK_APPLICATION: 0,
    PROTOCOL_MIGRATION: 1,

    0: "BLOCK_APPLICATION",
    1: "PROTOCOL_MIGRATION",
  });

  Id009PsflorenReceiptBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id009PsflorenReceiptBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id009PsflorenReceiptBalanceUpdates.prototype._read = function() {
    this.id009PsflorenOperationMetadataAlphaBalanceUpdates = new Id009PsflorenOperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Id009PsflorenOperationMetadataAlphaUpdateOrigin = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaUpdateOrigin = (function() {
    function Id009PsflorenOperationMetadataAlphaUpdateOrigin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenOperationMetadataAlphaUpdateOrigin.prototype._read = function() {
      this.origin = this._io.readU1();
    }

    return Id009PsflorenOperationMetadataAlphaUpdateOrigin;
  })();

  var Id009PsflorenContractId = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenContractId = (function() {
    function Id009PsflorenContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenContractId.prototype._read = function() {
      this.id009PsflorenContractIdTag = this._io.readU1();
      if (this.id009PsflorenContractIdTag == Id009PsflorenReceiptBalanceUpdates.Id009PsflorenContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id009PsflorenContractIdTag == Id009PsflorenReceiptBalanceUpdates.Id009PsflorenContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id009PsflorenContractId;
  })();

  var Id009PsflorenOperationMetadataAlphaBalanceUpdates = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceUpdates = (function() {
    function Id009PsflorenOperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenOperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id009PsflorenOperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id009PsflorenOperationMetadataAlphaBalanceUpdatesEntries.push(new Id009PsflorenOperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id009PsflorenOperationMetadataAlphaBalanceUpdates;
  })();

  var Originated = Id009PsflorenReceiptBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Id009PsflorenOperationMetadataAlphaBalanceUpdates0 = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id009PsflorenOperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenOperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId009PsflorenOperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId009PsflorenOperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId009PsflorenOperationMetadataAlphaBalanceUpdates, this._io, "/types/id_009__psfloren__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id009PsflorenOperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId009PsflorenOperationMetadataAlphaBalanceUpdates);
      var _io__raw_id009PsflorenOperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id009PsflorenOperationMetadataAlphaBalanceUpdates);
      this.id009PsflorenOperationMetadataAlphaBalanceUpdates = new Id009PsflorenOperationMetadataAlphaBalanceUpdates(_io__raw_id009PsflorenOperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id009PsflorenOperationMetadataAlphaBalanceUpdates0;
  })();

  var Rewards = Id009PsflorenReceiptBalanceUpdates.Rewards = (function() {
    function Rewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Rewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Rewards;
  })();

  var Deposits = Id009PsflorenReceiptBalanceUpdates.Deposits = (function() {
    function Deposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposits.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Deposits;
  })();

  var Id009PsflorenOperationMetadataAlphaBalance = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalance = (function() {
    function Id009PsflorenOperationMetadataAlphaBalance(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenOperationMetadataAlphaBalance.prototype._read = function() {
      this.id009PsflorenOperationMetadataAlphaBalanceTag = this._io.readU1();
      if (this.id009PsflorenOperationMetadataAlphaBalanceTag == Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceTag.CONTRACT) {
        this.contract = new Id009PsflorenContractId(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationMetadataAlphaBalanceTag == Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceTag.REWARDS) {
        this.rewards = new Rewards(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationMetadataAlphaBalanceTag == Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceTag.FEES) {
        this.fees = new Fees(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationMetadataAlphaBalanceTag == Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceTag.DEPOSITS) {
        this.deposits = new Deposits(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Id009PsflorenOperationMetadataAlphaBalance;
  })();

  var Fees = Id009PsflorenReceiptBalanceUpdates.Fees = (function() {
    function Fees(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Fees.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Fees;
  })();

  var Id009PsflorenOperationMetadataAlphaBalanceUpdate = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceUpdate = (function() {
    function Id009PsflorenOperationMetadataAlphaBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenOperationMetadataAlphaBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id009PsflorenOperationMetadataAlphaBalanceUpdate;
  })();

  var Id009PsflorenOperationMetadataAlphaBalanceUpdatesEntries = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id009PsflorenOperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenOperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id009PsflorenOperationMetadataAlphaBalance = new Id009PsflorenOperationMetadataAlphaBalance(this._io, this, this._root);
      this.id009PsflorenOperationMetadataAlphaBalanceUpdate = new Id009PsflorenOperationMetadataAlphaBalanceUpdate(this._io, this, this._root);
      this.id009PsflorenOperationMetadataAlphaUpdateOrigin = new Id009PsflorenOperationMetadataAlphaUpdateOrigin(this._io, this, this._root);
    }

    return Id009PsflorenOperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var PublicKeyHash = Id009PsflorenReceiptBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id009PsflorenReceiptBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id009PsflorenReceiptBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id009PsflorenReceiptBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id009PsflorenReceiptBalanceUpdates;
})();
return Id009PsflorenReceiptBalanceUpdates;
}));
