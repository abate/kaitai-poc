// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id011Pthangz2BlockHeaderUnsigned = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 011-PtHangz2.block_header.unsigned
 */

var Id011Pthangz2BlockHeaderUnsigned = (function() {
  Id011Pthangz2BlockHeaderUnsigned.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id011Pthangz2BlockHeaderUnsigned(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2BlockHeaderUnsigned.prototype._read = function() {
    this.id011Pthangz2BlockHeaderUnsigned = new BlockHeaderShell(this._io, this, null);
    this.id011Pthangz2BlockHeaderAlphaUnsignedContents = new Id011Pthangz2BlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id011Pthangz2BlockHeaderAlphaUnsignedContents = Id011Pthangz2BlockHeaderUnsigned.Id011Pthangz2BlockHeaderAlphaUnsignedContents = (function() {
    function Id011Pthangz2BlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2BlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id011Pthangz2BlockHeaderUnsigned.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingEscapeVote = this._io.readU1();
    }

    return Id011Pthangz2BlockHeaderAlphaUnsignedContents;
  })();

  return Id011Pthangz2BlockHeaderUnsigned;
})();
return Id011Pthangz2BlockHeaderUnsigned;
}));
