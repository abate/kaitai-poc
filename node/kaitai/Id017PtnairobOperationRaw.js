// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './Operation'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./Operation'));
  } else {
    root.Id017PtnairobOperationRaw = factory(root.KaitaiStream, root.Operation);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, Operation) {
/**
 * Encoding id: 017-PtNairob.operation.raw
 */

var Id017PtnairobOperationRaw = (function() {
  function Id017PtnairobOperationRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id017PtnairobOperationRaw.prototype._read = function() {
    this.id017PtnairobOperationRaw = new Operation(this._io, this, null);
  }

  return Id017PtnairobOperationRaw;
})();
return Id017PtnairobOperationRaw;
}));
