// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id018ProxfordSmartRollupWhitelist = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 018-Proxford.smart_rollup.whitelist
 */

var Id018ProxfordSmartRollupWhitelist = (function() {
  Id018ProxfordSmartRollupWhitelist.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function Id018ProxfordSmartRollupWhitelist(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id018ProxfordSmartRollupWhitelist.prototype._read = function() {
    this.lenId018ProxfordSmartRollupWhitelist = this._io.readU4be();
    if (!(this.lenId018ProxfordSmartRollupWhitelist <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId018ProxfordSmartRollupWhitelist, this._io, "/seq/0");
    }
    this._raw_id018ProxfordSmartRollupWhitelist = this._io.readBytes(this.lenId018ProxfordSmartRollupWhitelist);
    var _io__raw_id018ProxfordSmartRollupWhitelist = new KaitaiStream(this._raw_id018ProxfordSmartRollupWhitelist);
    this.id018ProxfordSmartRollupWhitelist = new Id018ProxfordSmartRollupWhitelist(_io__raw_id018ProxfordSmartRollupWhitelist, this, this._root);
  }

  var Id018ProxfordSmartRollupWhitelist = Id018ProxfordSmartRollupWhitelist.Id018ProxfordSmartRollupWhitelist = (function() {
    function Id018ProxfordSmartRollupWhitelist(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordSmartRollupWhitelist.prototype._read = function() {
      this.id018ProxfordSmartRollupWhitelistEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id018ProxfordSmartRollupWhitelistEntries.push(new Id018ProxfordSmartRollupWhitelistEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id018ProxfordSmartRollupWhitelist;
  })();

  var Id018ProxfordSmartRollupWhitelistEntries = Id018ProxfordSmartRollupWhitelist.Id018ProxfordSmartRollupWhitelistEntries = (function() {
    function Id018ProxfordSmartRollupWhitelistEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordSmartRollupWhitelistEntries.prototype._read = function() {
      this.signaturePublicKeyHash = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id018ProxfordSmartRollupWhitelistEntries;
  })();

  var PublicKeyHash = Id018ProxfordSmartRollupWhitelist.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id018ProxfordSmartRollupWhitelist.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id018ProxfordSmartRollupWhitelist.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id018ProxfordSmartRollupWhitelist.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id018ProxfordSmartRollupWhitelist.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id018ProxfordSmartRollupWhitelist;
})();
return Id018ProxfordSmartRollupWhitelist;
}));
