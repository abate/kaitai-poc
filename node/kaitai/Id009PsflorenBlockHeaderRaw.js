// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeader'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeader'));
  } else {
    root.Id009PsflorenBlockHeaderRaw = factory(root.KaitaiStream, root.BlockHeader);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeader) {
/**
 * Encoding id: 009-PsFLoren.block_header.raw
 */

var Id009PsflorenBlockHeaderRaw = (function() {
  function Id009PsflorenBlockHeaderRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id009PsflorenBlockHeaderRaw.prototype._read = function() {
    this.id009PsflorenBlockHeaderRaw = new BlockHeader(this._io, this, null);
  }

  return Id009PsflorenBlockHeaderRaw;
})();
return Id009PsflorenBlockHeaderRaw;
}));
