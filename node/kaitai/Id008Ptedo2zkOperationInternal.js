// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id008Ptedo2zkOperationInternal = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 008-PtEdo2Zk.operation.internal
 */

var Id008Ptedo2zkOperationInternal = (function() {
  Id008Ptedo2zkOperationInternal.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id008Ptedo2zkOperationInternal.Id008Ptedo2zkContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id008Ptedo2zkOperationInternal.Id008Ptedo2zkOperationAlphaInternalOperationTag = Object.freeze({
    REVEAL: 0,
    TRANSACTION: 1,
    ORIGINATION: 2,
    DELEGATION: 3,

    0: "REVEAL",
    1: "TRANSACTION",
    2: "ORIGINATION",
    3: "DELEGATION",
  });

  Id008Ptedo2zkOperationInternal.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id008Ptedo2zkOperationInternal.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id008Ptedo2zkOperationInternal.Id008Ptedo2zkEntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    255: "NAMED",
  });

  function Id008Ptedo2zkOperationInternal(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkOperationInternal.prototype._read = function() {
    this.id008Ptedo2zkOperationAlphaInternalOperation = new Id008Ptedo2zkOperationAlphaInternalOperation(this._io, this, this._root);
  }

  var Id008Ptedo2zkScriptedContracts = Id008Ptedo2zkOperationInternal.Id008Ptedo2zkScriptedContracts = (function() {
    function Id008Ptedo2zkScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id008Ptedo2zkScriptedContracts;
  })();

  var Id008Ptedo2zkMutez = Id008Ptedo2zkOperationInternal.Id008Ptedo2zkMutez = (function() {
    function Id008Ptedo2zkMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkMutez.prototype._read = function() {
      this.id008Ptedo2zkMutez = new N(this._io, this, this._root);
    }

    return Id008Ptedo2zkMutez;
  })();

  var Originated = Id008Ptedo2zkOperationInternal.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var N = Id008Ptedo2zkOperationInternal.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id008Ptedo2zkOperationAlphaInternalOperation = Id008Ptedo2zkOperationInternal.Id008Ptedo2zkOperationAlphaInternalOperation = (function() {
    function Id008Ptedo2zkOperationAlphaInternalOperation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkOperationAlphaInternalOperation.prototype._read = function() {
      this.source = new Id008Ptedo2zkContractId(this._io, this, this._root);
      this.nonce = this._io.readU2be();
      this.id008Ptedo2zkOperationAlphaInternalOperationTag = this._io.readU1();
      if (this.id008Ptedo2zkOperationAlphaInternalOperationTag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkOperationAlphaInternalOperationTag.REVEAL) {
        this.reveal = new PublicKey(this._io, this, this._root);
      }
      if (this.id008Ptedo2zkOperationAlphaInternalOperationTag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkOperationAlphaInternalOperationTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id008Ptedo2zkOperationAlphaInternalOperationTag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkOperationAlphaInternalOperationTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id008Ptedo2zkOperationAlphaInternalOperationTag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkOperationAlphaInternalOperationTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Id008Ptedo2zkOperationAlphaInternalOperation;
  })();

  var PublicKey = Id008Ptedo2zkOperationInternal.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id008Ptedo2zkOperationInternal.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id008Ptedo2zkOperationInternal.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id008Ptedo2zkOperationInternal.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var Named = Id008Ptedo2zkOperationInternal.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Id008Ptedo2zkEntrypoint = Id008Ptedo2zkOperationInternal.Id008Ptedo2zkEntrypoint = (function() {
    function Id008Ptedo2zkEntrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkEntrypoint.prototype._read = function() {
      this.id008Ptedo2zkEntrypointTag = this._io.readU1();
      if (this.id008Ptedo2zkEntrypointTag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkEntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id008Ptedo2zkEntrypoint;
  })();

  var Delegation = Id008Ptedo2zkOperationInternal.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id008Ptedo2zkOperationInternal.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var Id008Ptedo2zkContractId = Id008Ptedo2zkOperationInternal.Id008Ptedo2zkContractId = (function() {
    function Id008Ptedo2zkContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkContractId.prototype._read = function() {
      this.id008Ptedo2zkContractIdTag = this._io.readU1();
      if (this.id008Ptedo2zkContractIdTag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id008Ptedo2zkContractIdTag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id008Ptedo2zkContractId;
  })();

  var BytesDynUint30 = Id008Ptedo2zkOperationInternal.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Origination = Id008Ptedo2zkOperationInternal.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.balance = new Id008Ptedo2zkMutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id008Ptedo2zkOperationInternal.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id008Ptedo2zkScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var NChunk = Id008Ptedo2zkOperationInternal.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Named0 = Id008Ptedo2zkOperationInternal.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id008Ptedo2zkOperationInternal.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.amount = new Id008Ptedo2zkMutez(this._io, this, this._root);
      this.destination = new Id008Ptedo2zkContractId(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id008Ptedo2zkOperationInternal.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Transaction;
  })();

  var Parameters = Id008Ptedo2zkOperationInternal.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id008Ptedo2zkEntrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var PublicKeyHash = Id008Ptedo2zkOperationInternal.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id008Ptedo2zkOperationInternal.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id008Ptedo2zkOperationInternal.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id008Ptedo2zkOperationInternal.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id008Ptedo2zkOperationInternal;
})();
return Id008Ptedo2zkOperationInternal;
}));
