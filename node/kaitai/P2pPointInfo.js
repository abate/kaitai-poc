// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './TimestampSystem', './P2pPointState'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./TimestampSystem'), require('./P2pPointState'));
  } else {
    root.P2pPointInfo = factory(root.KaitaiStream, root.TimestampSystem, root.P2pPointState);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, TimestampSystem, P2pPointState) {
/**
 * Encoding id: p2p_point.info
 * Description: Information about a peer point. Includes flags, state, and records about past events.
 */

var P2pPointInfo = (function() {
  P2pPointInfo.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function P2pPointInfo(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  P2pPointInfo.prototype._read = function() {
    this.trusted = this._io.readU1();
    this.greylistedUntilTag = this._io.readU1();
    if (this.greylistedUntilTag == P2pPointInfo.Bool.TRUE) {
      this.greylistedUntil = new TimestampSystem(this._io, this, null);
    }
    this.state = new P2pPointState(this._io, this, null);
    this.p2pPeerIdTag = this._io.readU1();
    if (this.p2pPeerIdTag == P2pPointInfo.Bool.TRUE) {
      this.p2pPeerId = this._io.readBytes(16);
    }
    this.lastFailedConnectionTag = this._io.readU1();
    if (this.lastFailedConnectionTag == P2pPointInfo.Bool.TRUE) {
      this.lastFailedConnection = new TimestampSystem(this._io, this, null);
    }
    this.lastRejectedConnectionTag = this._io.readU1();
    if (this.lastRejectedConnectionTag == P2pPointInfo.Bool.TRUE) {
      this.lastRejectedConnection = new LastRejectedConnection(this._io, this, this._root);
    }
    this.lastEstablishedConnectionTag = this._io.readU1();
    if (this.lastEstablishedConnectionTag == P2pPointInfo.Bool.TRUE) {
      this.lastEstablishedConnection = new LastEstablishedConnection(this._io, this, this._root);
    }
    this.lastDisconnectionTag = this._io.readU1();
    if (this.lastDisconnectionTag == P2pPointInfo.Bool.TRUE) {
      this.lastDisconnection = new LastDisconnection(this._io, this, this._root);
    }
    this.lastSeenTag = this._io.readU1();
    if (this.lastSeenTag == P2pPointInfo.Bool.TRUE) {
      this.lastSeen = new LastSeen(this._io, this, this._root);
    }
    this.lastMissTag = this._io.readU1();
    if (this.lastMissTag == P2pPointInfo.Bool.TRUE) {
      this.lastMiss = new TimestampSystem(this._io, this, null);
    }
    this.expectedPeerIdTag = this._io.readU1();
    if (this.expectedPeerIdTag == P2pPointInfo.Bool.TRUE) {
      this.expectedPeerId = this._io.readBytes(16);
    }
  }

  var LastDisconnection = P2pPointInfo.LastDisconnection = (function() {
    function LastDisconnection(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LastDisconnection.prototype._read = function() {
      this.lastDisconnectionField0 = this._io.readBytes(16);
      this.lastDisconnectionField1 = new TimestampSystem(this._io, this, null);
    }

    /**
     * crypto_box__public_key_hash
     */

    return LastDisconnection;
  })();

  var LastEstablishedConnection = P2pPointInfo.LastEstablishedConnection = (function() {
    function LastEstablishedConnection(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LastEstablishedConnection.prototype._read = function() {
      this.lastEstablishedConnectionField0 = this._io.readBytes(16);
      this.lastEstablishedConnectionField1 = new TimestampSystem(this._io, this, null);
    }

    /**
     * crypto_box__public_key_hash
     */

    return LastEstablishedConnection;
  })();

  var LastRejectedConnection = P2pPointInfo.LastRejectedConnection = (function() {
    function LastRejectedConnection(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LastRejectedConnection.prototype._read = function() {
      this.lastRejectedConnectionField0 = this._io.readBytes(16);
      this.lastRejectedConnectionField1 = new TimestampSystem(this._io, this, null);
    }

    /**
     * crypto_box__public_key_hash
     */

    return LastRejectedConnection;
  })();

  var LastSeen = P2pPointInfo.LastSeen = (function() {
    function LastSeen(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LastSeen.prototype._read = function() {
      this.lastSeenField0 = this._io.readBytes(16);
      this.lastSeenField1 = new TimestampSystem(this._io, this, null);
    }

    /**
     * crypto_box__public_key_hash
     */

    return LastSeen;
  })();

  return P2pPointInfo;
})();
return P2pPointInfo;
}));
