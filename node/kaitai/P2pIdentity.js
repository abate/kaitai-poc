// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.P2pIdentity = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: p2p_identity
 * Description: The identity of a peer. This includes cryptographic keys as well as a proof-of-work.
 */

var P2pIdentity = (function() {
  P2pIdentity.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function P2pIdentity(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  P2pIdentity.prototype._read = function() {
    this.peerIdTag = this._io.readU1();
    if (this.peerIdTag == P2pIdentity.Bool.TRUE) {
      this.peerId = this._io.readBytes(16);
    }
    this.publicKey = this._io.readBytes(32);
    this.secretKey = this._io.readBytes(32);
    this.proofOfWorkStamp = this._io.readBytes(24);
  }

  return P2pIdentity;
})();
return P2pIdentity;
}));
