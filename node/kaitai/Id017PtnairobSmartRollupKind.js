// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id017PtnairobSmartRollupKind = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 017-PtNairob.smart_rollup.kind
 */

var Id017PtnairobSmartRollupKind = (function() {
  Id017PtnairobSmartRollupKind.Id017PtnairobSmartRollupKind = Object.freeze({
    ARITH: 0,
    WASM_2_0_0: 1,

    0: "ARITH",
    1: "WASM_2_0_0",
  });

  function Id017PtnairobSmartRollupKind(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id017PtnairobSmartRollupKind.prototype._read = function() {
    this.id017PtnairobSmartRollupKind = this._io.readU1();
  }

  return Id017PtnairobSmartRollupKind;
})();
return Id017PtnairobSmartRollupKind;
}));
