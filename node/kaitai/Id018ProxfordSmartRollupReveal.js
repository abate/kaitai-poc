// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id018ProxfordSmartRollupReveal = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 018-Proxford.smart_rollup.reveal
 */

var Id018ProxfordSmartRollupReveal = (function() {
  Id018ProxfordSmartRollupReveal.Id018ProxfordSmartRollupRevealTag = Object.freeze({
    REVEAL_RAW_DATA: 0,
    REVEAL_METADATA: 1,
    REQUEST_DAL_PAGE: 2,
    REVEAL_DAL_PARAMETERS: 3,

    0: "REVEAL_RAW_DATA",
    1: "REVEAL_METADATA",
    2: "REQUEST_DAL_PAGE",
    3: "REVEAL_DAL_PARAMETERS",
  });

  Id018ProxfordSmartRollupReveal.InputHashTag = Object.freeze({
    REVEAL_DATA_HASH_V0: 0,

    0: "REVEAL_DATA_HASH_V0",
  });

  function Id018ProxfordSmartRollupReveal(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id018ProxfordSmartRollupReveal.prototype._read = function() {
    this.id018ProxfordSmartRollupRevealTag = this._io.readU1();
    if (this.id018ProxfordSmartRollupRevealTag == Id018ProxfordSmartRollupReveal.Id018ProxfordSmartRollupRevealTag.REVEAL_RAW_DATA) {
      this.revealRawData = new InputHash(this._io, this, this._root);
    }
    if (this.id018ProxfordSmartRollupRevealTag == Id018ProxfordSmartRollupReveal.Id018ProxfordSmartRollupRevealTag.REQUEST_DAL_PAGE) {
      this.requestDalPage = new PageId(this._io, this, this._root);
    }
  }

  var InputHash = Id018ProxfordSmartRollupReveal.InputHash = (function() {
    function InputHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InputHash.prototype._read = function() {
      this.inputHashTag = this._io.readU1();
      if (this.inputHashTag == Id018ProxfordSmartRollupReveal.InputHashTag.REVEAL_DATA_HASH_V0) {
        this.revealDataHashV0 = this._io.readBytes(32);
      }
    }

    return InputHash;
  })();

  var PageId = Id018ProxfordSmartRollupReveal.PageId = (function() {
    function PageId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PageId.prototype._read = function() {
      this.publishedLevel = this._io.readS4be();
      this.slotIndex = this._io.readU1();
      this.pageIndex = this._io.readS2be();
    }

    return PageId;
  })();

  return Id018ProxfordSmartRollupReveal;
})();
return Id018ProxfordSmartRollupReveal;
}));
