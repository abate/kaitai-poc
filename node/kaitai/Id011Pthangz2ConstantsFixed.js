// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id011Pthangz2ConstantsFixed = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 011-PtHangz2.constants.fixed
 */

var Id011Pthangz2ConstantsFixed = (function() {
  function Id011Pthangz2ConstantsFixed(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2ConstantsFixed.prototype._read = function() {
    this.proofOfWorkNonceSize = this._io.readU1();
    this.nonceLength = this._io.readU1();
    this.maxAnonOpsPerBlock = this._io.readU1();
    this.maxOperationDataLength = new Int31(this._io, this, this._root);
    this.maxProposalsPerDelegate = this._io.readU1();
    this.maxMichelineNodeCount = new Int31(this._io, this, this._root);
    this.maxMichelineBytesLimit = new Int31(this._io, this, this._root);
    this.maxAllowedGlobalConstantsDepth = new Int31(this._io, this, this._root);
    this.cacheLayout = new CacheLayout0(this._io, this, this._root);
    this.michelsonMaximumTypeSize = this._io.readU2be();
  }

  var CacheLayout = Id011Pthangz2ConstantsFixed.CacheLayout = (function() {
    function CacheLayout(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CacheLayout.prototype._read = function() {
      this.cacheLayoutEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.cacheLayoutEntries.push(new CacheLayoutEntries(this._io, this, this._root));
        i++;
      }
    }

    return CacheLayout;
  })();

  var CacheLayout0 = Id011Pthangz2ConstantsFixed.CacheLayout0 = (function() {
    function CacheLayout0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CacheLayout0.prototype._read = function() {
      this.lenCacheLayout = this._io.readU4be();
      if (!(this.lenCacheLayout <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCacheLayout, this._io, "/types/cache_layout_0/seq/0");
      }
      this._raw_cacheLayout = this._io.readBytes(this.lenCacheLayout);
      var _io__raw_cacheLayout = new KaitaiStream(this._raw_cacheLayout);
      this.cacheLayout = new CacheLayout(_io__raw_cacheLayout, this, this._root);
    }

    return CacheLayout0;
  })();

  var CacheLayoutEntries = Id011Pthangz2ConstantsFixed.CacheLayoutEntries = (function() {
    function CacheLayoutEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CacheLayoutEntries.prototype._read = function() {
      this.cacheLayoutElt = this._io.readS8be();
    }

    return CacheLayoutEntries;
  })();

  var Int31 = Id011Pthangz2ConstantsFixed.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  return Id011Pthangz2ConstantsFixed;
})();
return Id011Pthangz2ConstantsFixed;
}));
