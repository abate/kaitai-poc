// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id006PscarthaDelegateFrozenBalance = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 006-PsCARTHA.delegate.frozen_balance
 */

var Id006PscarthaDelegateFrozenBalance = (function() {
  function Id006PscarthaDelegateFrozenBalance(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaDelegateFrozenBalance.prototype._read = function() {
    this.deposit = new Id006PscarthaMutez(this._io, this, this._root);
    this.fees = new Id006PscarthaMutez(this._io, this, this._root);
    this.rewards = new Id006PscarthaMutez(this._io, this, this._root);
  }

  var Id006PscarthaMutez = Id006PscarthaDelegateFrozenBalance.Id006PscarthaMutez = (function() {
    function Id006PscarthaMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaMutez.prototype._read = function() {
      this.id006PscarthaMutez = new N(this._io, this, this._root);
    }

    return Id006PscarthaMutez;
  })();

  var N = Id006PscarthaDelegateFrozenBalance.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var NChunk = Id006PscarthaDelegateFrozenBalance.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  return Id006PscarthaDelegateFrozenBalance;
})();
return Id006PscarthaDelegateFrozenBalance;
}));
