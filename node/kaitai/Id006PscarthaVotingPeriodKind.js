// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id006PscarthaVotingPeriodKind = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 006-PsCARTHA.voting_period.kind
 */

var Id006PscarthaVotingPeriodKind = (function() {
  Id006PscarthaVotingPeriodKind.Id006PscarthaVotingPeriodKindTag = Object.freeze({
    PROPOSAL: 0,
    TESTING_VOTE: 1,
    TESTING: 2,
    PROMOTION_VOTE: 3,

    0: "PROPOSAL",
    1: "TESTING_VOTE",
    2: "TESTING",
    3: "PROMOTION_VOTE",
  });

  function Id006PscarthaVotingPeriodKind(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaVotingPeriodKind.prototype._read = function() {
    this.id006PscarthaVotingPeriodKindTag = this._io.readU1();
  }

  return Id006PscarthaVotingPeriodKind;
})();
return Id006PscarthaVotingPeriodKind;
}));
