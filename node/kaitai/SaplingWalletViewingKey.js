// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.SaplingWalletViewingKey = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: sapling.wallet.viewing_key
 */

var SaplingWalletViewingKey = (function() {
  function SaplingWalletViewingKey(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  SaplingWalletViewingKey.prototype._read = function() {
    this.depth = this._io.readBytes(1);
    this.parentFvkTag = this._io.readBytes(4);
    this.childIndex = this._io.readBytes(4);
    this.chainCode = this._io.readBytes(32);
    this.expsk = new SaplingWalletFullViewingKey(this._io, this, this._root);
    this.dk = this._io.readBytes(32);
  }

  var SaplingWalletFullViewingKey = SaplingWalletViewingKey.SaplingWalletFullViewingKey = (function() {
    function SaplingWalletFullViewingKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SaplingWalletFullViewingKey.prototype._read = function() {
      this.ak = this._io.readBytes(32);
      this.nk = this._io.readBytes(32);
      this.ovk = this._io.readBytes(32);
    }

    return SaplingWalletFullViewingKey;
  })();

  return SaplingWalletViewingKey;
})();
return SaplingWalletViewingKey;
}));
