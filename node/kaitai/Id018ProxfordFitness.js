// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id018ProxfordFitness = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 018-Proxford.fitness
 */

var Id018ProxfordFitness = (function() {
  Id018ProxfordFitness.LockedRoundTag = Object.freeze({
    NONE: 0,
    SOME: 1,

    0: "NONE",
    1: "SOME",
  });

  function Id018ProxfordFitness(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id018ProxfordFitness.prototype._read = function() {
    this.level = this._io.readS4be();
    this.lockedRound = new LockedRound(this._io, this, this._root);
    this.predecessorRound = this._io.readS4be();
    this.round = this._io.readS4be();
  }

  var LockedRound = Id018ProxfordFitness.LockedRound = (function() {
    function LockedRound(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LockedRound.prototype._read = function() {
      this.lockedRoundTag = this._io.readU1();
      if (this.lockedRoundTag == Id018ProxfordFitness.LockedRoundTag.SOME) {
        this.some = this._io.readS4be();
      }
    }

    return LockedRound;
  })();

  return Id018ProxfordFitness;
})();
return Id018ProxfordFitness;
}));
