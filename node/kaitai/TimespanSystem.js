// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.TimespanSystem = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: timespan.system
 * Description: A span of time, as seen by the local computer.
 */

var TimespanSystem = (function() {
  function TimespanSystem(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  TimespanSystem.prototype._read = function() {
    this.timespanSystem = this._io.readF8be();
  }

  return TimespanSystem;
})();
return TimespanSystem;
}));
