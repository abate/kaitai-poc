// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './OperationShellHeader', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./OperationShellHeader'), require('./BlockHeaderShell'));
  } else {
    root.Id014PtkathmaOperationUnsigned = factory(root.KaitaiStream, root.OperationShellHeader, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, OperationShellHeader, BlockHeaderShell) {
/**
 * Encoding id: 014-PtKathma.operation.unsigned
 */

var Id014PtkathmaOperationUnsigned = (function() {
  Id014PtkathmaOperationUnsigned.AfterTag = Object.freeze({
    VALUE: 0,
    NODE: 1,

    0: "VALUE",
    1: "NODE",
  });

  Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag = Object.freeze({
    SEED_NONCE_REVELATION: 1,
    DOUBLE_ENDORSEMENT_EVIDENCE: 2,
    DOUBLE_BAKING_EVIDENCE: 3,
    ACTIVATE_ACCOUNT: 4,
    PROPOSALS: 5,
    BALLOT: 6,
    DOUBLE_PREENDORSEMENT_EVIDENCE: 7,
    VDF_REVELATION: 8,
    FAILING_NOOP: 17,
    PREENDORSEMENT: 20,
    ENDORSEMENT: 21,
    DAL_SLOT_AVAILABILITY: 22,
    REVEAL: 107,
    TRANSACTION: 108,
    ORIGINATION: 109,
    DELEGATION: 110,
    REGISTER_GLOBAL_CONSTANT: 111,
    SET_DEPOSITS_LIMIT: 112,
    INCREASE_PAID_STORAGE: 113,
    TX_ROLLUP_ORIGINATION: 150,
    TX_ROLLUP_SUBMIT_BATCH: 151,
    TX_ROLLUP_COMMIT: 152,
    TX_ROLLUP_RETURN_BOND: 153,
    TX_ROLLUP_FINALIZE_COMMITMENT: 154,
    TX_ROLLUP_REMOVE_COMMITMENT: 155,
    TX_ROLLUP_REJECTION: 156,
    TX_ROLLUP_DISPATCH_TICKETS: 157,
    TRANSFER_TICKET: 158,
    SC_ROLLUP_ORIGINATE: 200,
    SC_ROLLUP_ADD_MESSAGES: 201,
    SC_ROLLUP_CEMENT: 202,
    SC_ROLLUP_PUBLISH: 203,
    SC_ROLLUP_REFUTE: 204,
    SC_ROLLUP_TIMEOUT: 205,
    SC_ROLLUP_EXECUTE_OUTBOX_MESSAGE: 206,
    SC_ROLLUP_RECOVER_BOND: 207,
    SC_ROLLUP_DAL_SLOT_SUBSCRIBE: 208,
    DAL_PUBLISH_SLOT_HEADER: 230,

    1: "SEED_NONCE_REVELATION",
    2: "DOUBLE_ENDORSEMENT_EVIDENCE",
    3: "DOUBLE_BAKING_EVIDENCE",
    4: "ACTIVATE_ACCOUNT",
    5: "PROPOSALS",
    6: "BALLOT",
    7: "DOUBLE_PREENDORSEMENT_EVIDENCE",
    8: "VDF_REVELATION",
    17: "FAILING_NOOP",
    20: "PREENDORSEMENT",
    21: "ENDORSEMENT",
    22: "DAL_SLOT_AVAILABILITY",
    107: "REVEAL",
    108: "TRANSACTION",
    109: "ORIGINATION",
    110: "DELEGATION",
    111: "REGISTER_GLOBAL_CONSTANT",
    112: "SET_DEPOSITS_LIMIT",
    113: "INCREASE_PAID_STORAGE",
    150: "TX_ROLLUP_ORIGINATION",
    151: "TX_ROLLUP_SUBMIT_BATCH",
    152: "TX_ROLLUP_COMMIT",
    153: "TX_ROLLUP_RETURN_BOND",
    154: "TX_ROLLUP_FINALIZE_COMMITMENT",
    155: "TX_ROLLUP_REMOVE_COMMITMENT",
    156: "TX_ROLLUP_REJECTION",
    157: "TX_ROLLUP_DISPATCH_TICKETS",
    158: "TRANSFER_TICKET",
    200: "SC_ROLLUP_ORIGINATE",
    201: "SC_ROLLUP_ADD_MESSAGES",
    202: "SC_ROLLUP_CEMENT",
    203: "SC_ROLLUP_PUBLISH",
    204: "SC_ROLLUP_REFUTE",
    205: "SC_ROLLUP_TIMEOUT",
    206: "SC_ROLLUP_EXECUTE_OUTBOX_MESSAGE",
    207: "SC_ROLLUP_RECOVER_BOND",
    208: "SC_ROLLUP_DAL_SLOT_SUBSCRIBE",
    230: "DAL_PUBLISH_SLOT_HEADER",
  });

  Id014PtkathmaOperationUnsigned.BeforeTag = Object.freeze({
    VALUE: 0,
    NODE: 1,

    0: "VALUE",
    1: "NODE",
  });

  Id014PtkathmaOperationUnsigned.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id014PtkathmaOperationUnsigned.Id014PtkathmaContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id014PtkathmaOperationUnsigned.PvmStepTag = Object.freeze({
    ARITHMETIC__PVM__WITH__PROOF: 0,
    WASM__2__0__0__PVM__WITH__PROOF: 1,

    0: "ARITHMETIC__PVM__WITH__PROOF",
    1: "WASM__2__0__0__PVM__WITH__PROOF",
  });

  Id014PtkathmaOperationUnsigned.GivenTag = Object.freeze({
    NONE: 0,
    SOME: 1,

    0: "NONE",
    1: "SOME",
  });

  Id014PtkathmaOperationUnsigned.DissectionEltField0Tag = Object.freeze({
    NONE: 0,
    SOME: 1,

    0: "NONE",
    1: "SOME",
  });

  Id014PtkathmaOperationUnsigned.Case131EltField1Tag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,

    0: "CASE__0",
    1: "CASE__1",
  });

  Id014PtkathmaOperationUnsigned.ProofTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
  });

  Id014PtkathmaOperationUnsigned.TreeEncodingTag = Object.freeze({
    VALUE: 0,
    BLINDED_VALUE: 1,
    NODE: 2,
    BLINDED_NODE: 3,
    INODE: 4,
    EXTENDER: 5,
    NONE: 6,

    0: "VALUE",
    1: "BLINDED_VALUE",
    2: "NODE",
    3: "BLINDED_NODE",
    4: "INODE",
    5: "EXTENDER",
    6: "NONE",
  });

  Id014PtkathmaOperationUnsigned.Id014PtkathmaInlinedPreendorsementContentsTag = Object.freeze({
    PREENDORSEMENT: 20,

    20: "PREENDORSEMENT",
  });

  Id014PtkathmaOperationUnsigned.InboxTag = Object.freeze({
    NONE: 0,
    SOME: 1,

    0: "NONE",
    1: "SOME",
  });

  Id014PtkathmaOperationUnsigned.Case129EltField1Tag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,

    0: "CASE__0",
    1: "CASE__1",
  });

  Id014PtkathmaOperationUnsigned.Id014PtkathmaContractIdOriginatedTag = Object.freeze({
    ORIGINATED: 1,

    1: "ORIGINATED",
  });

  Id014PtkathmaOperationUnsigned.RequestedTag = Object.freeze({
    NO_INPUT_REQUIRED: 0,
    INITIAL: 1,
    FIRST_AFTER: 2,

    0: "NO_INPUT_REQUIRED",
    1: "INITIAL",
    2: "FIRST_AFTER",
  });

  Id014PtkathmaOperationUnsigned.KindTag = Object.freeze({
    EXAMPLE_ARITH__SMART__CONTRACT__ROLLUP__KIND: 0,
    WASM__2__0__0__SMART__CONTRACT__ROLLUP__KIND: 1,

    0: "EXAMPLE_ARITH__SMART__CONTRACT__ROLLUP__KIND",
    1: "WASM__2__0__0__SMART__CONTRACT__ROLLUP__KIND",
  });

  Id014PtkathmaOperationUnsigned.Case3Field3EltTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__7: 7,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,
    CASE__11: 11,
    CASE__12: 12,
    CASE__13: 13,
    CASE__14: 14,
    CASE__15: 15,
    CASE__128: 128,
    CASE__129: 129,
    CASE__130: 130,
    CASE__131: 131,
    CASE__192: 192,
    CASE__193: 193,
    CASE__195: 195,
    CASE__224: 224,
    CASE__225: 225,
    CASE__226: 226,
    CASE__227: 227,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    7: "CASE__7",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
    11: "CASE__11",
    12: "CASE__12",
    13: "CASE__13",
    14: "CASE__14",
    15: "CASE__15",
    128: "CASE__128",
    129: "CASE__129",
    130: "CASE__130",
    131: "CASE__131",
    192: "CASE__192",
    193: "CASE__193",
    195: "CASE__195",
    224: "CASE__224",
    225: "CASE__225",
    226: "CASE__226",
    227: "CASE__227",
  });

  Id014PtkathmaOperationUnsigned.Case1Field3EltTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__7: 7,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,
    CASE__11: 11,
    CASE__12: 12,
    CASE__13: 13,
    CASE__14: 14,
    CASE__15: 15,
    CASE__128: 128,
    CASE__129: 129,
    CASE__130: 130,
    CASE__131: 131,
    CASE__192: 192,
    CASE__193: 193,
    CASE__195: 195,
    CASE__224: 224,
    CASE__225: 225,
    CASE__226: 226,
    CASE__227: 227,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    7: "CASE__7",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
    11: "CASE__11",
    12: "CASE__12",
    13: "CASE__13",
    14: "CASE__14",
    15: "CASE__15",
    128: "CASE__128",
    129: "CASE__129",
    130: "CASE__130",
    131: "CASE__131",
    192: "CASE__192",
    193: "CASE__193",
    195: "CASE__195",
    224: "CASE__224",
    225: "CASE__225",
    226: "CASE__226",
    227: "CASE__227",
  });

  Id014PtkathmaOperationUnsigned.Id014PtkathmaInlinedEndorsementMempoolContentsTag = Object.freeze({
    ENDORSEMENT: 21,

    21: "ENDORSEMENT",
  });

  Id014PtkathmaOperationUnsigned.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id014PtkathmaOperationUnsigned.ProofsTag = Object.freeze({
    SPARSE_PROOF: 0,
    DENSE_PROOF: 1,

    0: "SPARSE_PROOF",
    1: "DENSE_PROOF",
  });

  Id014PtkathmaOperationUnsigned.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id014PtkathmaOperationUnsigned.Case130EltField1Tag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,

    0: "CASE__0",
    1: "CASE__1",
  });

  Id014PtkathmaOperationUnsigned.PredecessorTag = Object.freeze({
    NONE: 0,
    SOME: 1,

    0: "NONE",
    1: "SOME",
  });

  Id014PtkathmaOperationUnsigned.AmountTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
  });

  Id014PtkathmaOperationUnsigned.Case2Field3EltTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__7: 7,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,
    CASE__11: 11,
    CASE__12: 12,
    CASE__13: 13,
    CASE__14: 14,
    CASE__15: 15,
    CASE__128: 128,
    CASE__129: 129,
    CASE__130: 130,
    CASE__131: 131,
    CASE__192: 192,
    CASE__193: 193,
    CASE__195: 195,
    CASE__224: 224,
    CASE__225: 225,
    CASE__226: 226,
    CASE__227: 227,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    7: "CASE__7",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
    11: "CASE__11",
    12: "CASE__12",
    13: "CASE__13",
    14: "CASE__14",
    15: "CASE__15",
    128: "CASE__128",
    129: "CASE__129",
    130: "CASE__130",
    131: "CASE__131",
    192: "CASE__192",
    193: "CASE__193",
    195: "CASE__195",
    224: "CASE__224",
    225: "CASE__225",
    226: "CASE__226",
    227: "CASE__227",
  });

  Id014PtkathmaOperationUnsigned.InodeTreeTag = Object.freeze({
    BLINDED_INODE: 0,
    INODE_VALUES: 1,
    INODE_TREE: 2,
    INODE_EXTENDER: 3,
    NONE: 4,

    0: "BLINDED_INODE",
    1: "INODE_VALUES",
    2: "INODE_TREE",
    3: "INODE_EXTENDER",
    4: "NONE",
  });

  Id014PtkathmaOperationUnsigned.StepTag = Object.freeze({
    DISSECTION: 0,
    PROOF: 1,

    0: "DISSECTION",
    1: "PROOF",
  });

  Id014PtkathmaOperationUnsigned.MessageTag = Object.freeze({
    BATCH: 0,
    DEPOSIT: 1,

    0: "BATCH",
    1: "DEPOSIT",
  });

  Id014PtkathmaOperationUnsigned.Id014PtkathmaEntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    255: "NAMED",
  });

  Id014PtkathmaOperationUnsigned.Case0Field3EltTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__7: 7,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,
    CASE__11: 11,
    CASE__12: 12,
    CASE__13: 13,
    CASE__14: 14,
    CASE__15: 15,
    CASE__128: 128,
    CASE__129: 129,
    CASE__130: 130,
    CASE__131: 131,
    CASE__192: 192,
    CASE__193: 193,
    CASE__195: 195,
    CASE__224: 224,
    CASE__225: 225,
    CASE__226: 226,
    CASE__227: 227,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    7: "CASE__7",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
    11: "CASE__11",
    12: "CASE__12",
    13: "CASE__13",
    14: "CASE__14",
    15: "CASE__15",
    128: "CASE__128",
    129: "CASE__129",
    130: "CASE__130",
    131: "CASE__131",
    192: "CASE__192",
    193: "CASE__193",
    195: "CASE__195",
    224: "CASE__224",
    225: "CASE__225",
    226: "CASE__226",
    227: "CASE__227",
  });

  function Id014PtkathmaOperationUnsigned(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id014PtkathmaOperationUnsigned.prototype._read = function() {
    this.id014PtkathmaOperationAlphaUnsignedOperation = new Id014PtkathmaOperationAlphaUnsignedOperation(this._io, this, this._root);
  }

  var Case131EltField00 = Id014PtkathmaOperationUnsigned.Case131EltField00 = (function() {
    function Case131EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131EltField00.prototype._read = function() {
      this.lenCase131EltField0 = this._io.readU1();
      if (!(this.lenCase131EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase131EltField0, this._io, "/types/case__131_elt_field0_0/seq/0");
      }
      this._raw_case131EltField0 = this._io.readBytes(this.lenCase131EltField0);
      var _io__raw_case131EltField0 = new KaitaiStream(this._raw_case131EltField0);
      this.case131EltField0 = new Case131EltField0(_io__raw_case131EltField0, this, this._root);
    }

    return Case131EltField00;
  })();

  var ArithmeticPvmWithProof = Id014PtkathmaOperationUnsigned.ArithmeticPvmWithProof = (function() {
    function ArithmeticPvmWithProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ArithmeticPvmWithProof.prototype._read = function() {
      this.treeProof = new TreeProof(this._io, this, this._root);
      this.given = new Given(this._io, this, this._root);
      this.requested = new Requested(this._io, this, this._root);
    }

    return ArithmeticPvmWithProof;
  })();

  var Op20 = Id014PtkathmaOperationUnsigned.Op20 = (function() {
    function Op20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op20.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_0/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op2(_io__raw_op2, this, this._root);
    }

    return Op20;
  })();

  var Case1 = Id014PtkathmaOperationUnsigned.Case1 = (function() {
    function Case1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1.prototype._read = function() {
      this.case1Field0 = this._io.readS2be();
      this.case1Field1 = this._io.readBytes(32);
      this.case1Field2 = this._io.readBytes(32);
      this.case1Field3 = new Case1Field30(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case1;
  })();

  var Wasm200PvmWithProof = Id014PtkathmaOperationUnsigned.Wasm200PvmWithProof = (function() {
    function Wasm200PvmWithProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Wasm200PvmWithProof.prototype._read = function() {
      this.treeProof = new TreeProof(this._io, this, this._root);
      this.given = new Given(this._io, this, this._root);
      this.requested = new Requested(this._io, this, this._root);
    }

    return Wasm200PvmWithProof;
  })();

  var Case192 = Id014PtkathmaOperationUnsigned.Case192 = (function() {
    function Case192(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case192.prototype._read = function() {
      this.case192 = this._io.readBytesFull();
    }

    return Case192;
  })();

  var Messages = Id014PtkathmaOperationUnsigned.Messages = (function() {
    function Messages(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Messages.prototype._read = function() {
      this.messagesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messagesEntries.push(new MessagesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Messages;
  })();

  var DenseProofEntries = Id014PtkathmaOperationUnsigned.DenseProofEntries = (function() {
    function DenseProofEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DenseProofEntries.prototype._read = function() {
      this.denseProofElt = new InodeTree(this._io, this, this._root);
    }

    return DenseProofEntries;
  })();

  var Stakers = Id014PtkathmaOperationUnsigned.Stakers = (function() {
    function Stakers(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Stakers.prototype._read = function() {
      this.alice = new PublicKeyHash(this._io, this, this._root);
      this.bob = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Stakers;
  })();

  var MessageResultPathEntries = Id014PtkathmaOperationUnsigned.MessageResultPathEntries = (function() {
    function MessageResultPathEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageResultPathEntries.prototype._read = function() {
      this.messageResultListHash = this._io.readBytes(32);
    }

    return MessageResultPathEntries;
  })();

  var InodeTree = Id014PtkathmaOperationUnsigned.InodeTree = (function() {
    function InodeTree(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InodeTree.prototype._read = function() {
      this.length = this._io.readS8be();
      this.proofs = new Proofs(this._io, this, this._root);
    }

    return InodeTree;
  })();

  var IncreasePaidStorage = Id014PtkathmaOperationUnsigned.IncreasePaidStorage = (function() {
    function IncreasePaidStorage(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IncreasePaidStorage.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.amount = new Z(this._io, this, this._root);
      this.destination = new Id014PtkathmaContractIdOriginated(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A contract handle -- originated account: A contract notation as given to an RPC or inside scripts. Can be a base58 originated contract hash.
     */

    return IncreasePaidStorage;
  })();

  var Id014PtkathmaRollupAddress = Id014PtkathmaOperationUnsigned.Id014PtkathmaRollupAddress = (function() {
    function Id014PtkathmaRollupAddress(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaRollupAddress.prototype._read = function() {
      this.id014PtkathmaRollupAddress = new BytesDynUint30(this._io, this, this._root);
    }

    return Id014PtkathmaRollupAddress;
  })();

  var ScRollupPublish = Id014PtkathmaOperationUnsigned.ScRollupPublish = (function() {
    function ScRollupPublish(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupPublish.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaRollupAddress(this._io, this, this._root);
      this.commitment = new Commitment0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupPublish;
  })();

  var Case1933 = Id014PtkathmaOperationUnsigned.Case1933 = (function() {
    function Case1933(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1933.prototype._read = function() {
      this.lenCase193 = this._io.readU2be();
      if (!(this.lenCase193 <= 65535)) {
        throw new KaitaiStream.ValidationGreaterThanError(65535, this.lenCase193, this._io, "/types/case__193_3/seq/0");
      }
      this._raw_case193 = this._io.readBytes(this.lenCase193);
      var _io__raw_case193 = new KaitaiStream(this._raw_case193);
      this.case193 = new Case193(_io__raw_case193, this, this._root);
    }

    return Case1933;
  })();

  var SparseProof2 = Id014PtkathmaOperationUnsigned.SparseProof2 = (function() {
    function SparseProof2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SparseProof2.prototype._read = function() {
      this.lenSparseProof = this._io.readU4be();
      if (!(this.lenSparseProof <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenSparseProof, this._io, "/types/sparse_proof_2/seq/0");
      }
      this._raw_sparseProof = this._io.readBytes(this.lenSparseProof);
      var _io__raw_sparseProof = new KaitaiStream(this._raw_sparseProof);
      this.sparseProof = new SparseProof1(_io__raw_sparseProof, this, this._root);
    }

    return SparseProof2;
  })();

  var Case8 = Id014PtkathmaOperationUnsigned.Case8 = (function() {
    function Case8(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case8.prototype._read = function() {
      this.case8Field0 = this._io.readU1();
      this.case8Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__8_field1_field1
     */

    return Case8;
  })();

  var Case10 = Id014PtkathmaOperationUnsigned.Case10 = (function() {
    function Case10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case10.prototype._read = function() {
      this.case10Field0 = this._io.readS4be();
      this.case10Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__10_field1_field1
     */

    return Case10;
  })();

  var Case2Field3Entries = Id014PtkathmaOperationUnsigned.Case2Field3Entries = (function() {
    function Case2Field3Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case2Field3Entries.prototype._read = function() {
      this.case2Field3EltTag = this._io.readU1();
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__8) {
        this.case8 = new Case8(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__4) {
        this.case4 = new Case4(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__12) {
        this.case12 = new Case12(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__9) {
        this.case9 = new Case9(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__5) {
        this.case5 = new Case5(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__13) {
        this.case13 = new Case13(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__10) {
        this.case10 = new Case10(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__6) {
        this.case6 = new Case6(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__14) {
        this.case14 = new Case14(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__11) {
        this.case11 = new Case11(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__7) {
        this.case7 = new Case7(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__15) {
        this.case15 = new Case15(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__129) {
        this.case129 = new Case129Entries(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__130) {
        this.case130 = new Case130Entries(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__131) {
        this.case131 = new Case1311(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__192) {
        this.case192 = new Case1921(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__193) {
        this.case193 = new Case1931(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__195) {
        this.case195 = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__224) {
        this.case224 = new Case224(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__225) {
        this.case225 = new Case225(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__226) {
        this.case226 = new Case226(this._io, this, this._root);
      }
      if (this.case2Field3EltTag == Id014PtkathmaOperationUnsigned.Case2Field3EltTag.CASE__227) {
        this.case227 = new Case227(this._io, this, this._root);
      }
    }

    return Case2Field3Entries;
  })();

  var Case3Field30 = Id014PtkathmaOperationUnsigned.Case3Field30 = (function() {
    function Case3Field30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case3Field30.prototype._read = function() {
      this.lenCase3Field3 = this._io.readU4be();
      if (!(this.lenCase3Field3 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase3Field3, this._io, "/types/case__3_field3_0/seq/0");
      }
      this._raw_case3Field3 = this._io.readBytes(this.lenCase3Field3);
      var _io__raw_case3Field3 = new KaitaiStream(this._raw_case3Field3);
      this.case3Field3 = new Case3Field3(_io__raw_case3Field3, this, this._root);
    }

    return Case3Field30;
  })();

  var ActivateAccount = Id014PtkathmaOperationUnsigned.ActivateAccount = (function() {
    function ActivateAccount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ActivateAccount.prototype._read = function() {
      this.pkh = this._io.readBytes(20);
      this.secret = this._io.readBytes(20);
    }

    return ActivateAccount;
  })();

  var Case12Field1 = Id014PtkathmaOperationUnsigned.Case12Field1 = (function() {
    function Case12Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case12Field1.prototype._read = function() {
      this.case12Field1Field0 = this._io.readBytes(32);
      this.case12Field1Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case12Field1;
  })();

  var DoubleEndorsementEvidence = Id014PtkathmaOperationUnsigned.DoubleEndorsementEvidence = (function() {
    function DoubleEndorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleEndorsementEvidence.prototype._read = function() {
      this.op1 = new Op10(this._io, this, this._root);
      this.op2 = new Op20(this._io, this, this._root);
    }

    return DoubleEndorsementEvidence;
  })();

  var Step = Id014PtkathmaOperationUnsigned.Step = (function() {
    function Step(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Step.prototype._read = function() {
      this.stepTag = this._io.readU1();
      if (this.stepTag == Id014PtkathmaOperationUnsigned.StepTag.DISSECTION) {
        this.dissection = new Dissection0(this._io, this, this._root);
      }
      if (this.stepTag == Id014PtkathmaOperationUnsigned.StepTag.PROOF) {
        this.proof = new Proof0(this._io, this, this._root);
      }
    }

    return Step;
  })();

  var BackPointers0 = Id014PtkathmaOperationUnsigned.BackPointers0 = (function() {
    function BackPointers0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BackPointers0.prototype._read = function() {
      this.lenBackPointers = this._io.readU4be();
      if (!(this.lenBackPointers <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBackPointers, this._io, "/types/back_pointers_0/seq/0");
      }
      this._raw_backPointers = this._io.readBytes(this.lenBackPointers);
      var _io__raw_backPointers = new KaitaiStream(this._raw_backPointers);
      this.backPointers = new BackPointers(_io__raw_backPointers, this, this._root);
    }

    return BackPointers0;
  })();

  var Case226 = Id014PtkathmaOperationUnsigned.Case226 = (function() {
    function Case226(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case226.prototype._read = function() {
      this.case226Field0 = this._io.readS4be();
      this.case226Field1 = new Case226Field10(this._io, this, this._root);
      this.case226Field2 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    return Case226;
  })();

  var Case9 = Id014PtkathmaOperationUnsigned.Case9 = (function() {
    function Case9(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case9.prototype._read = function() {
      this.case9Field0 = this._io.readU2be();
      this.case9Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__9_field1_field1
     */

    return Case9;
  })();

  var Case13 = Id014PtkathmaOperationUnsigned.Case13 = (function() {
    function Case13(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case13.prototype._read = function() {
      this.case13Field0 = this._io.readU2be();
      this.case13Field1 = new Case13Field1(this._io, this, this._root);
    }

    return Case13;
  })();

  var Originated = Id014PtkathmaOperationUnsigned.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Given = Id014PtkathmaOperationUnsigned.Given = (function() {
    function Given(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Given.prototype._read = function() {
      this.givenTag = this._io.readU1();
      if (this.givenTag == Id014PtkathmaOperationUnsigned.GivenTag.SOME) {
        this.some = new Some(this._io, this, this._root);
      }
    }

    return Given;
  })();

  var Case226Field1 = Id014PtkathmaOperationUnsigned.Case226Field1 = (function() {
    function Case226Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case226Field1.prototype._read = function() {
      this.case226Field1 = this._io.readBytesFull();
    }

    return Case226Field1;
  })();

  var N = Id014PtkathmaOperationUnsigned.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Case131EltField1 = Id014PtkathmaOperationUnsigned.Case131EltField1 = (function() {
    function Case131EltField1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131EltField1.prototype._read = function() {
      this.case131EltField1Tag = this._io.readU1();
      if (this.case131EltField1Tag == Id014PtkathmaOperationUnsigned.Case131EltField1Tag.CASE__0) {
        this.case0 = this._io.readBytes(32);
      }
      if (this.case131EltField1Tag == Id014PtkathmaOperationUnsigned.Case131EltField1Tag.CASE__1) {
        this.case1 = this._io.readBytes(32);
      }
    }

    return Case131EltField1;
  })();

  var Case227 = Id014PtkathmaOperationUnsigned.Case227 = (function() {
    function Case227(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case227.prototype._read = function() {
      this.case227Field0 = this._io.readS8be();
      this.case227Field1 = new Case227Field10(this._io, this, this._root);
      this.case227Field2 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    return Case227;
  })();

  var Case4 = Id014PtkathmaOperationUnsigned.Case4 = (function() {
    function Case4(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case4.prototype._read = function() {
      this.case4Field0 = this._io.readU1();
      this.case4Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__4_field1_field0
     */

    return Case4;
  })();

  var Endorsement = Id014PtkathmaOperationUnsigned.Endorsement = (function() {
    function Endorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Endorsement.prototype._read = function() {
      this.slot = this._io.readU2be();
      this.level = this._io.readS4be();
      this.round = this._io.readS4be();
      this.blockPayloadHash = this._io.readBytes(32);
    }

    return Endorsement;
  })();

  var Case130Entries = Id014PtkathmaOperationUnsigned.Case130Entries = (function() {
    function Case130Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130Entries.prototype._read = function() {
      this.case130EltField0 = new Case130EltField00(this._io, this, this._root);
      this.case130EltField1 = new Case130EltField1(this._io, this, this._root);
    }

    return Case130Entries;
  })();

  var Id014PtkathmaInlinedEndorsement = Id014PtkathmaOperationUnsigned.Id014PtkathmaInlinedEndorsement = (function() {
    function Id014PtkathmaInlinedEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaInlinedEndorsement.prototype._read = function() {
      this.id014PtkathmaInlinedEndorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id014PtkathmaInlinedEndorsementMempoolContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id014PtkathmaOperationUnsigned.Bool.TRUE) {
        this.signature = this._io.readBytes(64);
      }
    }

    return Id014PtkathmaInlinedEndorsement;
  })();

  var Requested = Id014PtkathmaOperationUnsigned.Requested = (function() {
    function Requested(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Requested.prototype._read = function() {
      this.requestedTag = this._io.readU1();
      if (this.requestedTag == Id014PtkathmaOperationUnsigned.RequestedTag.FIRST_AFTER) {
        this.firstAfter = new FirstAfter(this._io, this, this._root);
      }
    }

    return Requested;
  })();

  var Proposals0 = Id014PtkathmaOperationUnsigned.Proposals0 = (function() {
    function Proposals0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals0.prototype._read = function() {
      this.lenProposals = this._io.readU4be();
      if (!(this.lenProposals <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenProposals, this._io, "/types/proposals_0/seq/0");
      }
      this._raw_proposals = this._io.readBytes(this.lenProposals);
      var _io__raw_proposals = new KaitaiStream(this._raw_proposals);
      this.proposals = new Proposals(_io__raw_proposals, this, this._root);
    }

    return Proposals0;
  })();

  var MessagesEntries = Id014PtkathmaOperationUnsigned.MessagesEntries = (function() {
    function MessagesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagesEntries.prototype._read = function() {
      this.messageResultHash = this._io.readBytes(32);
    }

    return MessagesEntries;
  })();

  var Case13Field1 = Id014PtkathmaOperationUnsigned.Case13Field1 = (function() {
    function Case13Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case13Field1.prototype._read = function() {
      this.case13Field1Field0 = this._io.readBytes(32);
      this.case13Field1Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case13Field1;
  })();

  var NodeEltField00 = Id014PtkathmaOperationUnsigned.NodeEltField00 = (function() {
    function NodeEltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NodeEltField00.prototype._read = function() {
      this.lenNodeEltField0 = this._io.readU1();
      if (!(this.lenNodeEltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenNodeEltField0, this._io, "/types/node_elt_field0_0/seq/0");
      }
      this._raw_nodeEltField0 = this._io.readBytes(this.lenNodeEltField0);
      var _io__raw_nodeEltField0 = new KaitaiStream(this._raw_nodeEltField0);
      this.nodeEltField0 = new NodeEltField0(_io__raw_nodeEltField0, this, this._root);
    }

    return NodeEltField00;
  })();

  var TransferTicket = Id014PtkathmaOperationUnsigned.TransferTicket = (function() {
    function TransferTicket(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TransferTicket.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.ticketContents = new BytesDynUint30(this._io, this, this._root);
      this.ticketTy = new BytesDynUint30(this._io, this, this._root);
      this.ticketTicketer = new Id014PtkathmaContractId(this._io, this, this._root);
      this.ticketAmount = new N(this._io, this, this._root);
      this.destination = new Id014PtkathmaContractId(this._io, this, this._root);
      this.entrypoint = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return TransferTicket;
  })();

  var Case129EltField0 = Id014PtkathmaOperationUnsigned.Case129EltField0 = (function() {
    function Case129EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129EltField0.prototype._read = function() {
      this.case129EltField0 = this._io.readBytesFull();
    }

    return Case129EltField0;
  })();

  var Case1310 = Id014PtkathmaOperationUnsigned.Case1310 = (function() {
    function Case1310(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1310.prototype._read = function() {
      this.lenCase131 = this._io.readU4be();
      if (!(this.lenCase131 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase131, this._io, "/types/case__131_0/seq/0");
      }
      this._raw_case131 = this._io.readBytes(this.lenCase131);
      var _io__raw_case131 = new KaitaiStream(this._raw_case131);
      this.case131 = new Case131(_io__raw_case131, this, this._root);
    }

    return Case1310;
  })();

  var Inbox = Id014PtkathmaOperationUnsigned.Inbox = (function() {
    function Inbox(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Inbox.prototype._read = function() {
      this.inboxTag = this._io.readU1();
      if (this.inboxTag == Id014PtkathmaOperationUnsigned.InboxTag.SOME) {
        this.some = new Some0(this._io, this, this._root);
      }
    }

    return Inbox;
  })();

  var Case15Field1 = Id014PtkathmaOperationUnsigned.Case15Field1 = (function() {
    function Case15Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case15Field1.prototype._read = function() {
      this.case15Field1Field0 = this._io.readBytes(32);
      this.case15Field1Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case15Field1;
  })();

  var NodeEltField0 = Id014PtkathmaOperationUnsigned.NodeEltField0 = (function() {
    function NodeEltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NodeEltField0.prototype._read = function() {
      this.nodeEltField0 = this._io.readBytesFull();
    }

    return NodeEltField0;
  })();

  var Op12 = Id014PtkathmaOperationUnsigned.Op12 = (function() {
    function Op12(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op12.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_2/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op11(_io__raw_op1, this, this._root);
    }

    return Op12;
  })();

  var ScRollupExecuteOutboxMessage = Id014PtkathmaOperationUnsigned.ScRollupExecuteOutboxMessage = (function() {
    function ScRollupExecuteOutboxMessage(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupExecuteOutboxMessage.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaRollupAddress(this._io, this, this._root);
      this.cementedCommitment = this._io.readBytes(32);
      this.outboxLevel = this._io.readS4be();
      this.messageIndex = new Int31(this._io, this, this._root);
      this.inclusionProof = new BytesDynUint30(this._io, this, this._root);
      this.message = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupExecuteOutboxMessage;
  })();

  var MessageProof = Id014PtkathmaOperationUnsigned.MessageProof = (function() {
    function MessageProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageProof.prototype._read = function() {
      this.version = this._io.readS2be();
      this.before = new Before(this._io, this, this._root);
      this.after = new After(this._io, this, this._root);
      this.state = new TreeEncoding(this._io, this, this._root);
    }

    return MessageProof;
  })();

  var TxRollupRemoveCommitment = Id014PtkathmaOperationUnsigned.TxRollupRemoveCommitment = (function() {
    function TxRollupRemoveCommitment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupRemoveCommitment.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaTxRollupId(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupRemoveCommitment;
  })();

  var Case0 = Id014PtkathmaOperationUnsigned.Case0 = (function() {
    function Case0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case0.prototype._read = function() {
      this.case0Field0 = this._io.readS2be();
      this.case0Field1 = this._io.readBytes(32);
      this.case0Field2 = this._io.readBytes(32);
      this.case0Field3 = new Case0Field30(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case0;
  })();

  var Node0 = Id014PtkathmaOperationUnsigned.Node0 = (function() {
    function Node0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Node0.prototype._read = function() {
      this.lenNode = this._io.readU4be();
      if (!(this.lenNode <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenNode, this._io, "/types/node_0/seq/0");
      }
      this._raw_node = this._io.readBytes(this.lenNode);
      var _io__raw_node = new KaitaiStream(this._raw_node);
      this.node = new Node(_io__raw_node, this, this._root);
    }

    return Node0;
  })();

  var Case1921 = Id014PtkathmaOperationUnsigned.Case1921 = (function() {
    function Case1921(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1921.prototype._read = function() {
      this.lenCase192 = this._io.readU1();
      if (!(this.lenCase192 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase192, this._io, "/types/case__192_1/seq/0");
      }
      this._raw_case192 = this._io.readBytes(this.lenCase192);
      var _io__raw_case192 = new KaitaiStream(this._raw_case192);
      this.case192 = new Case192(_io__raw_case192, this, this._root);
    }

    return Case1921;
  })();

  var DissectionEltField0 = Id014PtkathmaOperationUnsigned.DissectionEltField0 = (function() {
    function DissectionEltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DissectionEltField0.prototype._read = function() {
      this.dissectionEltField0Tag = this._io.readU1();
      if (this.dissectionEltField0Tag == Id014PtkathmaOperationUnsigned.DissectionEltField0Tag.SOME) {
        this.some = this._io.readBytes(32);
      }
    }

    return DissectionEltField0;
  })();

  var Id014PtkathmaBlockHeaderAlphaUnsignedContents = Id014PtkathmaOperationUnsigned.Id014PtkathmaBlockHeaderAlphaUnsignedContents = (function() {
    function Id014PtkathmaBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id014PtkathmaOperationUnsigned.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id014PtkathmaLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id014PtkathmaBlockHeaderAlphaUnsignedContents;
  })();

  var Reveal = Id014PtkathmaOperationUnsigned.Reveal = (function() {
    function Reveal(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Reveal.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.publicKey = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Reveal;
  })();

  var Case1922 = Id014PtkathmaOperationUnsigned.Case1922 = (function() {
    function Case1922(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1922.prototype._read = function() {
      this.lenCase192 = this._io.readU1();
      if (!(this.lenCase192 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase192, this._io, "/types/case__192_2/seq/0");
      }
      this._raw_case192 = this._io.readBytes(this.lenCase192);
      var _io__raw_case192 = new KaitaiStream(this._raw_case192);
      this.case192 = new Case192(_io__raw_case192, this, this._root);
    }

    return Case1922;
  })();

  var Id014PtkathmaLiquidityBakingToggleVote = Id014PtkathmaOperationUnsigned.Id014PtkathmaLiquidityBakingToggleVote = (function() {
    function Id014PtkathmaLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaLiquidityBakingToggleVote.prototype._read = function() {
      this.id014PtkathmaLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id014PtkathmaLiquidityBakingToggleVote;
  })();

  var MessageEntries = Id014PtkathmaOperationUnsigned.MessageEntries = (function() {
    function MessageEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageEntries.prototype._read = function() {
      this.messageElt = new BytesDynUint30(this._io, this, this._root);
    }

    return MessageEntries;
  })();

  var Before = Id014PtkathmaOperationUnsigned.Before = (function() {
    function Before(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Before.prototype._read = function() {
      this.beforeTag = this._io.readU1();
      if (this.beforeTag == Id014PtkathmaOperationUnsigned.BeforeTag.VALUE) {
        this.value = this._io.readBytes(32);
      }
      if (this.beforeTag == Id014PtkathmaOperationUnsigned.BeforeTag.NODE) {
        this.node = this._io.readBytes(32);
      }
    }

    return Before;
  })();

  var Id014PtkathmaMutez = Id014PtkathmaOperationUnsigned.Id014PtkathmaMutez = (function() {
    function Id014PtkathmaMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaMutez.prototype._read = function() {
      this.id014PtkathmaMutez = new N(this._io, this, this._root);
    }

    return Id014PtkathmaMutez;
  })();

  var Case5 = Id014PtkathmaOperationUnsigned.Case5 = (function() {
    function Case5(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case5.prototype._read = function() {
      this.case5Field0 = this._io.readU2be();
      this.case5Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__5_field1_field0
     */

    return Case5;
  })();

  var Case193 = Id014PtkathmaOperationUnsigned.Case193 = (function() {
    function Case193(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case193.prototype._read = function() {
      this.case193 = this._io.readBytesFull();
    }

    return Case193;
  })();

  var Refutation = Id014PtkathmaOperationUnsigned.Refutation = (function() {
    function Refutation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Refutation.prototype._read = function() {
      this.choice = new N(this._io, this, this._root);
      this.step = new Step(this._io, this, this._root);
    }

    return Refutation;
  })();

  var ScRollupDalSlotSubscribe = Id014PtkathmaOperationUnsigned.ScRollupDalSlotSubscribe = (function() {
    function ScRollupDalSlotSubscribe(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupDalSlotSubscribe.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaRollupAddress(this._io, this, this._root);
      this.slotIndex = this._io.readU1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupDalSlotSubscribe;
  })();

  var SparseProof = Id014PtkathmaOperationUnsigned.SparseProof = (function() {
    function SparseProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SparseProof.prototype._read = function() {
      this.sparseProofEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.sparseProofEntries.push(new SparseProofEntries(this._io, this, this._root));
        i++;
      }
    }

    return SparseProof;
  })();

  var Id014PtkathmaContractId = Id014PtkathmaOperationUnsigned.Id014PtkathmaContractId = (function() {
    function Id014PtkathmaContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaContractId.prototype._read = function() {
      this.id014PtkathmaContractIdTag = this._io.readU1();
      if (this.id014PtkathmaContractIdTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id014PtkathmaContractIdTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id014PtkathmaContractId;
  })();

  var ScRollupOriginate = Id014PtkathmaOperationUnsigned.ScRollupOriginate = (function() {
    function ScRollupOriginate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupOriginate.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.kind = this._io.readU2be();
      this.bootSector = new BytesDynUint30(this._io, this, this._root);
      this.parametersTy = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return ScRollupOriginate;
  })();

  var SeedNonceRevelation = Id014PtkathmaOperationUnsigned.SeedNonceRevelation = (function() {
    function SeedNonceRevelation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SeedNonceRevelation.prototype._read = function() {
      this.level = this._io.readS4be();
      this.nonce = this._io.readBytes(32);
    }

    return SeedNonceRevelation;
  })();

  var Case3Field3 = Id014PtkathmaOperationUnsigned.Case3Field3 = (function() {
    function Case3Field3(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case3Field3.prototype._read = function() {
      this.case3Field3Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case3Field3Entries.push(new Case3Field3Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case3Field3;
  })();

  var Case225Field10 = Id014PtkathmaOperationUnsigned.Case225Field10 = (function() {
    function Case225Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case225Field10.prototype._read = function() {
      this.lenCase225Field1 = this._io.readU1();
      if (!(this.lenCase225Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase225Field1, this._io, "/types/case__225_field1_0/seq/0");
      }
      this._raw_case225Field1 = this._io.readBytes(this.lenCase225Field1);
      var _io__raw_case225Field1 = new KaitaiStream(this._raw_case225Field1);
      this.case225Field1 = new Case225Field1(_io__raw_case225Field1, this, this._root);
    }

    return Case225Field10;
  })();

  var Preendorsement = Id014PtkathmaOperationUnsigned.Preendorsement = (function() {
    function Preendorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Preendorsement.prototype._read = function() {
      this.slot = this._io.readU2be();
      this.level = this._io.readS4be();
      this.round = this._io.readS4be();
      this.blockPayloadHash = this._io.readBytes(32);
    }

    return Preendorsement;
  })();

  var Inode = Id014PtkathmaOperationUnsigned.Inode = (function() {
    function Inode(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Inode.prototype._read = function() {
      this.length = this._io.readS8be();
      this.proofs = new Proofs0(this._io, this, this._root);
    }

    return Inode;
  })();

  var Case11 = Id014PtkathmaOperationUnsigned.Case11 = (function() {
    function Case11(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case11.prototype._read = function() {
      this.case11Field0 = this._io.readS8be();
      this.case11Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__11_field1_field1
     */

    return Case11;
  })();

  var SkipsEntries = Id014PtkathmaOperationUnsigned.SkipsEntries = (function() {
    function SkipsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SkipsEntries.prototype._read = function() {
      this.skipsEltField0 = new SkipsEltField0(this._io, this, this._root);
      this.skipsEltField1 = new SkipsEltField10(this._io, this, this._root);
    }

    return SkipsEntries;
  })();

  var TxRollupCommit = Id014PtkathmaOperationUnsigned.TxRollupCommit = (function() {
    function TxRollupCommit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupCommit.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaTxRollupId(this._io, this, this._root);
      this.commitment = new Commitment(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupCommit;
  })();

  var DoubleBakingEvidence = Id014PtkathmaOperationUnsigned.DoubleBakingEvidence = (function() {
    function DoubleBakingEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleBakingEvidence.prototype._read = function() {
      this.bh1 = new Bh10(this._io, this, this._root);
      this.bh2 = new Bh20(this._io, this, this._root);
    }

    return DoubleBakingEvidence;
  })();

  var Id014PtkathmaBlockHeaderAlphaSignedContents = Id014PtkathmaOperationUnsigned.Id014PtkathmaBlockHeaderAlphaSignedContents = (function() {
    function Id014PtkathmaBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id014PtkathmaBlockHeaderAlphaUnsignedContents = new Id014PtkathmaBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id014PtkathmaBlockHeaderAlphaSignedContents;
  })();

  var Extender = Id014PtkathmaOperationUnsigned.Extender = (function() {
    function Extender(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Extender.prototype._read = function() {
      this.length = this._io.readS8be();
      this.segment = new Segment0(this._io, this, this._root);
      this.proof = new InodeTree(this._io, this, this._root);
    }

    return Extender;
  })();

  var Case225Field1 = Id014PtkathmaOperationUnsigned.Case225Field1 = (function() {
    function Case225Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case225Field1.prototype._read = function() {
      this.case225Field1 = this._io.readBytesFull();
    }

    return Case225Field1;
  })();

  var Case129EltField00 = Id014PtkathmaOperationUnsigned.Case129EltField00 = (function() {
    function Case129EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129EltField00.prototype._read = function() {
      this.lenCase129EltField0 = this._io.readU1();
      if (!(this.lenCase129EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase129EltField0, this._io, "/types/case__129_elt_field0_0/seq/0");
      }
      this._raw_case129EltField0 = this._io.readBytes(this.lenCase129EltField0);
      var _io__raw_case129EltField0 = new KaitaiStream(this._raw_case129EltField0);
      this.case129EltField0 = new Case129EltField0(_io__raw_case129EltField0, this, this._root);
    }

    return Case129EltField00;
  })();

  var Case3Field3Entries = Id014PtkathmaOperationUnsigned.Case3Field3Entries = (function() {
    function Case3Field3Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case3Field3Entries.prototype._read = function() {
      this.case3Field3EltTag = this._io.readU1();
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__8) {
        this.case8 = new Case8(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__4) {
        this.case4 = new Case4(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__12) {
        this.case12 = new Case12(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__9) {
        this.case9 = new Case9(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__5) {
        this.case5 = new Case5(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__13) {
        this.case13 = new Case13(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__10) {
        this.case10 = new Case10(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__6) {
        this.case6 = new Case6(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__14) {
        this.case14 = new Case14(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__11) {
        this.case11 = new Case11(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__7) {
        this.case7 = new Case7(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__15) {
        this.case15 = new Case15(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__129) {
        this.case129 = new Case129Entries(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__130) {
        this.case130 = new Case130Entries(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__131) {
        this.case131 = new Case1313(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__192) {
        this.case192 = new Case1923(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__193) {
        this.case193 = new Case1933(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__195) {
        this.case195 = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__224) {
        this.case224 = new Case224(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__225) {
        this.case225 = new Case225(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__226) {
        this.case226 = new Case226(this._io, this, this._root);
      }
      if (this.case3Field3EltTag == Id014PtkathmaOperationUnsigned.Case3Field3EltTag.CASE__227) {
        this.case227 = new Case227(this._io, this, this._root);
      }
    }

    return Case3Field3Entries;
  })();

  var PublicKey = Id014PtkathmaOperationUnsigned.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id014PtkathmaOperationUnsigned.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id014PtkathmaOperationUnsigned.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id014PtkathmaOperationUnsigned.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var Skips0 = Id014PtkathmaOperationUnsigned.Skips0 = (function() {
    function Skips0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Skips0.prototype._read = function() {
      this.lenSkips = this._io.readU4be();
      if (!(this.lenSkips <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenSkips, this._io, "/types/skips_0/seq/0");
      }
      this._raw_skips = this._io.readBytes(this.lenSkips);
      var _io__raw_skips = new KaitaiStream(this._raw_skips);
      this.skips = new Skips(_io__raw_skips, this, this._root);
    }

    return Skips0;
  })();

  var Case12 = Id014PtkathmaOperationUnsigned.Case12 = (function() {
    function Case12(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case12.prototype._read = function() {
      this.case12Field0 = this._io.readU1();
      this.case12Field1 = new Case12Field1(this._io, this, this._root);
    }

    return Case12;
  })();

  var TxRollupDispatchTickets = Id014PtkathmaOperationUnsigned.TxRollupDispatchTickets = (function() {
    function TxRollupDispatchTickets(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupDispatchTickets.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.txRollup = new Id014PtkathmaTxRollupId(this._io, this, this._root);
      this.level = this._io.readS4be();
      this.contextHash = this._io.readBytes(32);
      this.messageIndex = new Int31(this._io, this, this._root);
      this.messageResultPath = new MessageResultPath0(this._io, this, this._root);
      this.ticketsInfo = new TicketsInfo0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupDispatchTickets;
  })();

  var Bh2 = Id014PtkathmaOperationUnsigned.Bh2 = (function() {
    function Bh2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh2.prototype._read = function() {
      this.id014PtkathmaBlockHeaderAlphaFullHeader = new Id014PtkathmaBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh2;
  })();

  var SkipsEltField1 = Id014PtkathmaOperationUnsigned.SkipsEltField1 = (function() {
    function SkipsEltField1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SkipsEltField1.prototype._read = function() {
      this.skipsEltField1Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.skipsEltField1Entries.push(new SkipsEltField1Entries(this._io, this, this._root));
        i++;
      }
    }

    return SkipsEltField1;
  })();

  var PreviousMessageResultPathEntries = Id014PtkathmaOperationUnsigned.PreviousMessageResultPathEntries = (function() {
    function PreviousMessageResultPathEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResultPathEntries.prototype._read = function() {
      this.messageResultListHash = this._io.readBytes(32);
    }

    return PreviousMessageResultPathEntries;
  })();

  var Segment = Id014PtkathmaOperationUnsigned.Segment = (function() {
    function Segment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Segment.prototype._read = function() {
      this.segment = this._io.readBytesFull();
    }

    return Segment;
  })();

  var TreeEncoding = Id014PtkathmaOperationUnsigned.TreeEncoding = (function() {
    function TreeEncoding(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TreeEncoding.prototype._read = function() {
      this.treeEncodingTag = this._io.readU1();
      if (this.treeEncodingTag == Id014PtkathmaOperationUnsigned.TreeEncodingTag.VALUE) {
        this.value = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id014PtkathmaOperationUnsigned.TreeEncodingTag.BLINDED_VALUE) {
        this.blindedValue = this._io.readBytes(32);
      }
      if (this.treeEncodingTag == Id014PtkathmaOperationUnsigned.TreeEncodingTag.NODE) {
        this.node = new Node0(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id014PtkathmaOperationUnsigned.TreeEncodingTag.BLINDED_NODE) {
        this.blindedNode = this._io.readBytes(32);
      }
      if (this.treeEncodingTag == Id014PtkathmaOperationUnsigned.TreeEncodingTag.INODE) {
        this.inode = new Inode(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id014PtkathmaOperationUnsigned.TreeEncodingTag.EXTENDER) {
        this.extender = new Extender(this._io, this, this._root);
      }
    }

    return TreeEncoding;
  })();

  var PvmStep = Id014PtkathmaOperationUnsigned.PvmStep = (function() {
    function PvmStep(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PvmStep.prototype._read = function() {
      this.pvmStepTag = this._io.readU1();
      if (this.pvmStepTag == Id014PtkathmaOperationUnsigned.PvmStepTag.ARITHMETIC__PVM__WITH__PROOF) {
        this.arithmeticPvmWithProof = new ArithmeticPvmWithProof(this._io, this, this._root);
      }
      if (this.pvmStepTag == Id014PtkathmaOperationUnsigned.PvmStepTag.WASM__2__0__0__PVM__WITH__PROOF) {
        this.wasm200PvmWithProof = new Wasm200PvmWithProof(this._io, this, this._root);
      }
    }

    return PvmStep;
  })();

  var Case2Field30 = Id014PtkathmaOperationUnsigned.Case2Field30 = (function() {
    function Case2Field30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case2Field30.prototype._read = function() {
      this.lenCase2Field3 = this._io.readU4be();
      if (!(this.lenCase2Field3 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase2Field3, this._io, "/types/case__2_field3_0/seq/0");
      }
      this._raw_case2Field3 = this._io.readBytes(this.lenCase2Field3);
      var _io__raw_case2Field3 = new KaitaiStream(this._raw_case2Field3);
      this.case2Field3 = new Case2Field3(_io__raw_case2Field3, this, this._root);
    }

    return Case2Field30;
  })();

  var ScRollupCement = Id014PtkathmaOperationUnsigned.ScRollupCement = (function() {
    function ScRollupCement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupCement.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaRollupAddress(this._io, this, this._root);
      this.commitment = this._io.readBytes(32);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupCement;
  })();

  var Named = Id014PtkathmaOperationUnsigned.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var InodeValues = Id014PtkathmaOperationUnsigned.InodeValues = (function() {
    function InodeValues(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InodeValues.prototype._read = function() {
      this.inodeValuesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.inodeValuesEntries.push(new InodeValuesEntries(this._io, this, this._root));
        i++;
      }
    }

    return InodeValues;
  })();

  var Amount = Id014PtkathmaOperationUnsigned.Amount = (function() {
    function Amount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Amount.prototype._read = function() {
      this.amountTag = this._io.readU1();
      if (this.amountTag == Id014PtkathmaOperationUnsigned.AmountTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.amountTag == Id014PtkathmaOperationUnsigned.AmountTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.amountTag == Id014PtkathmaOperationUnsigned.AmountTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.amountTag == Id014PtkathmaOperationUnsigned.AmountTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
    }

    return Amount;
  })();

  var Case1311 = Id014PtkathmaOperationUnsigned.Case1311 = (function() {
    function Case1311(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1311.prototype._read = function() {
      this.lenCase131 = this._io.readU4be();
      if (!(this.lenCase131 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase131, this._io, "/types/case__131_1/seq/0");
      }
      this._raw_case131 = this._io.readBytes(this.lenCase131);
      var _io__raw_case131 = new KaitaiStream(this._raw_case131);
      this.case131 = new Case131(_io__raw_case131, this, this._root);
    }

    return Case1311;
  })();

  var Case129EltField1 = Id014PtkathmaOperationUnsigned.Case129EltField1 = (function() {
    function Case129EltField1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129EltField1.prototype._read = function() {
      this.case129EltField1Tag = this._io.readU1();
      if (this.case129EltField1Tag == Id014PtkathmaOperationUnsigned.Case129EltField1Tag.CASE__0) {
        this.case0 = this._io.readBytes(32);
      }
      if (this.case129EltField1Tag == Id014PtkathmaOperationUnsigned.Case129EltField1Tag.CASE__1) {
        this.case1 = this._io.readBytes(32);
      }
    }

    return Case129EltField1;
  })();

  var Case131 = Id014PtkathmaOperationUnsigned.Case131 = (function() {
    function Case131(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131.prototype._read = function() {
      this.case131Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case131Entries.push(new Case131Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case131;
  })();

  var Bh20 = Id014PtkathmaOperationUnsigned.Bh20 = (function() {
    function Bh20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh20.prototype._read = function() {
      this.lenBh2 = this._io.readU4be();
      if (!(this.lenBh2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh2, this._io, "/types/bh2_0/seq/0");
      }
      this._raw_bh2 = this._io.readBytes(this.lenBh2);
      var _io__raw_bh2 = new KaitaiStream(this._raw_bh2);
      this.bh2 = new Bh2(_io__raw_bh2, this, this._root);
    }

    return Bh20;
  })();

  var Commitment = Id014PtkathmaOperationUnsigned.Commitment = (function() {
    function Commitment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitment.prototype._read = function() {
      this.level = this._io.readS4be();
      this.messages = new Messages0(this._io, this, this._root);
      this.predecessor = new Predecessor(this._io, this, this._root);
      this.inboxMerkleRoot = this._io.readBytes(32);
    }

    return Commitment;
  })();

  var Case2 = Id014PtkathmaOperationUnsigned.Case2 = (function() {
    function Case2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case2.prototype._read = function() {
      this.case2Field0 = this._io.readS2be();
      this.case2Field1 = this._io.readBytes(32);
      this.case2Field2 = this._io.readBytes(32);
      this.case2Field3 = new Case2Field30(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case2;
  })();

  var TicketsInfoEntries = Id014PtkathmaOperationUnsigned.TicketsInfoEntries = (function() {
    function TicketsInfoEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TicketsInfoEntries.prototype._read = function() {
      this.contents = new BytesDynUint30(this._io, this, this._root);
      this.ty = new BytesDynUint30(this._io, this, this._root);
      this.ticketer = new Id014PtkathmaContractId(this._io, this, this._root);
      this.amount = new Amount(this._io, this, this._root);
      this.claimer = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return TicketsInfoEntries;
  })();

  var Delegation = Id014PtkathmaOperationUnsigned.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id014PtkathmaOperationUnsigned.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var Id014PtkathmaOperationAlphaContents = Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContents = (function() {
    function Id014PtkathmaOperationAlphaContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaOperationAlphaContents.prototype._read = function() {
      this.id014PtkathmaOperationAlphaContentsTag = this._io.readU1();
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.ENDORSEMENT) {
        this.endorsement = new Endorsement(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.PREENDORSEMENT) {
        this.preendorsement = new Preendorsement(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.DAL_SLOT_AVAILABILITY) {
        this.dalSlotAvailability = new DalSlotAvailability(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.SEED_NONCE_REVELATION) {
        this.seedNonceRevelation = new SeedNonceRevelation(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.VDF_REVELATION) {
        this.vdfRevelation = new Solution(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.DOUBLE_ENDORSEMENT_EVIDENCE) {
        this.doubleEndorsementEvidence = new DoubleEndorsementEvidence(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.DOUBLE_PREENDORSEMENT_EVIDENCE) {
        this.doublePreendorsementEvidence = new DoublePreendorsementEvidence(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.DOUBLE_BAKING_EVIDENCE) {
        this.doubleBakingEvidence = new DoubleBakingEvidence(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.ACTIVATE_ACCOUNT) {
        this.activateAccount = new ActivateAccount(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.PROPOSALS) {
        this.proposals = new Proposals1(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.BALLOT) {
        this.ballot = new Ballot(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.REVEAL) {
        this.reveal = new Reveal(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.SET_DEPOSITS_LIMIT) {
        this.setDepositsLimit = new SetDepositsLimit(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.INCREASE_PAID_STORAGE) {
        this.increasePaidStorage = new IncreasePaidStorage(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.FAILING_NOOP) {
        this.failingNoop = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.REGISTER_GLOBAL_CONSTANT) {
        this.registerGlobalConstant = new RegisterGlobalConstant(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.TX_ROLLUP_ORIGINATION) {
        this.txRollupOrigination = new TxRollupOrigination(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.TX_ROLLUP_SUBMIT_BATCH) {
        this.txRollupSubmitBatch = new TxRollupSubmitBatch(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.TX_ROLLUP_COMMIT) {
        this.txRollupCommit = new TxRollupCommit(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.TX_ROLLUP_RETURN_BOND) {
        this.txRollupReturnBond = new TxRollupReturnBond(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.TX_ROLLUP_FINALIZE_COMMITMENT) {
        this.txRollupFinalizeCommitment = new TxRollupFinalizeCommitment(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.TX_ROLLUP_REMOVE_COMMITMENT) {
        this.txRollupRemoveCommitment = new TxRollupRemoveCommitment(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.TX_ROLLUP_REJECTION) {
        this.txRollupRejection = new TxRollupRejection(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.TX_ROLLUP_DISPATCH_TICKETS) {
        this.txRollupDispatchTickets = new TxRollupDispatchTickets(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.TRANSFER_TICKET) {
        this.transferTicket = new TransferTicket(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.DAL_PUBLISH_SLOT_HEADER) {
        this.dalPublishSlotHeader = new DalPublishSlotHeader(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.SC_ROLLUP_ORIGINATE) {
        this.scRollupOriginate = new ScRollupOriginate(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.SC_ROLLUP_ADD_MESSAGES) {
        this.scRollupAddMessages = new ScRollupAddMessages(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.SC_ROLLUP_CEMENT) {
        this.scRollupCement = new ScRollupCement(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.SC_ROLLUP_PUBLISH) {
        this.scRollupPublish = new ScRollupPublish(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.SC_ROLLUP_REFUTE) {
        this.scRollupRefute = new ScRollupRefute(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.SC_ROLLUP_TIMEOUT) {
        this.scRollupTimeout = new ScRollupTimeout(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.SC_ROLLUP_EXECUTE_OUTBOX_MESSAGE) {
        this.scRollupExecuteOutboxMessage = new ScRollupExecuteOutboxMessage(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.SC_ROLLUP_RECOVER_BOND) {
        this.scRollupRecoverBond = new ScRollupRecoverBond(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationAlphaContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaContentsTag.SC_ROLLUP_DAL_SLOT_SUBSCRIBE) {
        this.scRollupDalSlotSubscribe = new ScRollupDalSlotSubscribe(this._io, this, this._root);
      }
    }

    return Id014PtkathmaOperationAlphaContents;
  })();

  var Case1923 = Id014PtkathmaOperationUnsigned.Case1923 = (function() {
    function Case1923(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1923.prototype._read = function() {
      this.lenCase192 = this._io.readU1();
      if (!(this.lenCase192 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase192, this._io, "/types/case__192_3/seq/0");
      }
      this._raw_case192 = this._io.readBytes(this.lenCase192);
      var _io__raw_case192 = new KaitaiStream(this._raw_case192);
      this.case192 = new Case192(_io__raw_case192, this, this._root);
    }

    return Case1923;
  })();

  var Segment0 = Id014PtkathmaOperationUnsigned.Segment0 = (function() {
    function Segment0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Segment0.prototype._read = function() {
      this.lenSegment = this._io.readU1();
      if (!(this.lenSegment <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenSegment, this._io, "/types/segment_0/seq/0");
      }
      this._raw_segment = this._io.readBytes(this.lenSegment);
      var _io__raw_segment = new KaitaiStream(this._raw_segment);
      this.segment = new Segment(_io__raw_segment, this, this._root);
    }

    return Segment0;
  })();

  var RegisterGlobalConstant = Id014PtkathmaOperationUnsigned.RegisterGlobalConstant = (function() {
    function RegisterGlobalConstant(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RegisterGlobalConstant.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return RegisterGlobalConstant;
  })();

  var Solution = Id014PtkathmaOperationUnsigned.Solution = (function() {
    function Solution(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Solution.prototype._read = function() {
      this.solutionField0 = this._io.readBytes(100);
      this.solutionField1 = this._io.readBytes(100);
    }

    return Solution;
  })();

  var Case129Entries = Id014PtkathmaOperationUnsigned.Case129Entries = (function() {
    function Case129Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129Entries.prototype._read = function() {
      this.case129EltField0 = new Case129EltField00(this._io, this, this._root);
      this.case129EltField1 = new Case129EltField1(this._io, this, this._root);
    }

    return Case129Entries;
  })();

  var ScRollupRecoverBond = Id014PtkathmaOperationUnsigned.ScRollupRecoverBond = (function() {
    function ScRollupRecoverBond(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupRecoverBond.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = this._io.readBytes(20);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return ScRollupRecoverBond;
  })();

  var FirstAfter = Id014PtkathmaOperationUnsigned.FirstAfter = (function() {
    function FirstAfter(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    FirstAfter.prototype._read = function() {
      this.firstAfterField0 = this._io.readS4be();
      this.firstAfterField1 = new N(this._io, this, this._root);
    }

    return FirstAfter;
  })();

  var Case2Field3 = Id014PtkathmaOperationUnsigned.Case2Field3 = (function() {
    function Case2Field3(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case2Field3.prototype._read = function() {
      this.case2Field3Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case2Field3Entries.push(new Case2Field3Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case2Field3;
  })();

  var PreviousMessageResult = Id014PtkathmaOperationUnsigned.PreviousMessageResult = (function() {
    function PreviousMessageResult(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResult.prototype._read = function() {
      this.contextHash = this._io.readBytes(32);
      this.withdrawListHash = this._io.readBytes(32);
    }

    return PreviousMessageResult;
  })();

  var Case1Field30 = Id014PtkathmaOperationUnsigned.Case1Field30 = (function() {
    function Case1Field30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1Field30.prototype._read = function() {
      this.lenCase1Field3 = this._io.readU4be();
      if (!(this.lenCase1Field3 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase1Field3, this._io, "/types/case__1_field3_0/seq/0");
      }
      this._raw_case1Field3 = this._io.readBytes(this.lenCase1Field3);
      var _io__raw_case1Field3 = new KaitaiStream(this._raw_case1Field3);
      this.case1Field3 = new Case1Field3(_io__raw_case1Field3, this, this._root);
    }

    return Case1Field30;
  })();

  var SparseProofEntries = Id014PtkathmaOperationUnsigned.SparseProofEntries = (function() {
    function SparseProofEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SparseProofEntries.prototype._read = function() {
      this.sparseProofEltField0 = this._io.readU1();
      this.sparseProofEltField1 = new InodeTree(this._io, this, this._root);
    }

    return SparseProofEntries;
  })();

  var Case130EltField0 = Id014PtkathmaOperationUnsigned.Case130EltField0 = (function() {
    function Case130EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130EltField0.prototype._read = function() {
      this.case130EltField0 = this._io.readBytesFull();
    }

    return Case130EltField0;
  })();

  var SkipsEltField10 = Id014PtkathmaOperationUnsigned.SkipsEltField10 = (function() {
    function SkipsEltField10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SkipsEltField10.prototype._read = function() {
      this.lenSkipsEltField1 = this._io.readU4be();
      if (!(this.lenSkipsEltField1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenSkipsEltField1, this._io, "/types/skips_elt_field1_0/seq/0");
      }
      this._raw_skipsEltField1 = this._io.readBytes(this.lenSkipsEltField1);
      var _io__raw_skipsEltField1 = new KaitaiStream(this._raw_skipsEltField1);
      this.skipsEltField1 = new SkipsEltField1(_io__raw_skipsEltField1, this, this._root);
    }

    return SkipsEltField10;
  })();

  var Case6 = Id014PtkathmaOperationUnsigned.Case6 = (function() {
    function Case6(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case6.prototype._read = function() {
      this.case6Field0 = this._io.readS4be();
      this.case6Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__6_field1_field0
     */

    return Case6;
  })();

  var ContentsEntries = Id014PtkathmaOperationUnsigned.ContentsEntries = (function() {
    function ContentsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ContentsEntries.prototype._read = function() {
      this.id014PtkathmaOperationAlphaContents = new Id014PtkathmaOperationAlphaContents(this._io, this, this._root);
    }

    return ContentsEntries;
  })();

  var PreviousMessageResultPath = Id014PtkathmaOperationUnsigned.PreviousMessageResultPath = (function() {
    function PreviousMessageResultPath(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResultPath.prototype._read = function() {
      this.previousMessageResultPathEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.previousMessageResultPathEntries.push(new PreviousMessageResultPathEntries(this._io, this, this._root));
        i++;
      }
    }

    return PreviousMessageResultPath;
  })();

  var IncEntries = Id014PtkathmaOperationUnsigned.IncEntries = (function() {
    function IncEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IncEntries.prototype._read = function() {
      this.index = new Int31(this._io, this, this._root);
      this.content = this._io.readBytes(32);
      this.backPointers = new BackPointers0(this._io, this, this._root);
    }

    return IncEntries;
  })();

  var ScRollupAddMessages = Id014PtkathmaOperationUnsigned.ScRollupAddMessages = (function() {
    function ScRollupAddMessages(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupAddMessages.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaRollupAddress(this._io, this, this._root);
      this.message = new Message1(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupAddMessages;
  })();

  var Case1Field3Entries = Id014PtkathmaOperationUnsigned.Case1Field3Entries = (function() {
    function Case1Field3Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1Field3Entries.prototype._read = function() {
      this.case1Field3EltTag = this._io.readU1();
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__8) {
        this.case8 = new Case8(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__4) {
        this.case4 = new Case4(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__12) {
        this.case12 = new Case12(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__9) {
        this.case9 = new Case9(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__5) {
        this.case5 = new Case5(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__13) {
        this.case13 = new Case13(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__10) {
        this.case10 = new Case10(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__6) {
        this.case6 = new Case6(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__14) {
        this.case14 = new Case14(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__11) {
        this.case11 = new Case11(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__7) {
        this.case7 = new Case7(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__15) {
        this.case15 = new Case15(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__129) {
        this.case129 = new Case129Entries(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__130) {
        this.case130 = new Case130Entries(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__131) {
        this.case131 = new Case1312(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__192) {
        this.case192 = new Case1922(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__193) {
        this.case193 = new Case1932(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__195) {
        this.case195 = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__224) {
        this.case224 = new Case224(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__225) {
        this.case225 = new Case225(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__226) {
        this.case226 = new Case226(this._io, this, this._root);
      }
      if (this.case1Field3EltTag == Id014PtkathmaOperationUnsigned.Case1Field3EltTag.CASE__227) {
        this.case227 = new Case227(this._io, this, this._root);
      }
    }

    return Case1Field3Entries;
  })();

  var PreviousMessageResultPath0 = Id014PtkathmaOperationUnsigned.PreviousMessageResultPath0 = (function() {
    function PreviousMessageResultPath0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResultPath0.prototype._read = function() {
      this.lenPreviousMessageResultPath = this._io.readU4be();
      if (!(this.lenPreviousMessageResultPath <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenPreviousMessageResultPath, this._io, "/types/previous_message_result_path_0/seq/0");
      }
      this._raw_previousMessageResultPath = this._io.readBytes(this.lenPreviousMessageResultPath);
      var _io__raw_previousMessageResultPath = new KaitaiStream(this._raw_previousMessageResultPath);
      this.previousMessageResultPath = new PreviousMessageResultPath(_io__raw_previousMessageResultPath, this, this._root);
    }

    return PreviousMessageResultPath0;
  })();

  var Case0Field3Entries = Id014PtkathmaOperationUnsigned.Case0Field3Entries = (function() {
    function Case0Field3Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case0Field3Entries.prototype._read = function() {
      this.case0Field3EltTag = this._io.readU1();
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__8) {
        this.case8 = new Case8(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__4) {
        this.case4 = new Case4(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__12) {
        this.case12 = new Case12(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__9) {
        this.case9 = new Case9(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__5) {
        this.case5 = new Case5(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__13) {
        this.case13 = new Case13(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__10) {
        this.case10 = new Case10(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__6) {
        this.case6 = new Case6(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__14) {
        this.case14 = new Case14(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__11) {
        this.case11 = new Case11(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__7) {
        this.case7 = new Case7(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__15) {
        this.case15 = new Case15(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__129) {
        this.case129 = new Case129Entries(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__130) {
        this.case130 = new Case130Entries(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__131) {
        this.case131 = new Case1310(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__192) {
        this.case192 = new Case1920(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__193) {
        this.case193 = new Case1930(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__195) {
        this.case195 = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__224) {
        this.case224 = new Case224(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__225) {
        this.case225 = new Case225(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__226) {
        this.case226 = new Case226(this._io, this, this._root);
      }
      if (this.case0Field3EltTag == Id014PtkathmaOperationUnsigned.Case0Field3EltTag.CASE__227) {
        this.case227 = new Case227(this._io, this, this._root);
      }
    }

    return Case0Field3Entries;
  })();

  var Bh10 = Id014PtkathmaOperationUnsigned.Bh10 = (function() {
    function Bh10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh10.prototype._read = function() {
      this.lenBh1 = this._io.readU4be();
      if (!(this.lenBh1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh1, this._io, "/types/bh1_0/seq/0");
      }
      this._raw_bh1 = this._io.readBytes(this.lenBh1);
      var _io__raw_bh1 = new KaitaiStream(this._raw_bh1);
      this.bh1 = new Bh1(_io__raw_bh1, this, this._root);
    }

    return Bh10;
  })();

  var Some0 = Id014PtkathmaOperationUnsigned.Some0 = (function() {
    function Some0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Some0.prototype._read = function() {
      this.skips = new Skips0(this._io, this, this._root);
      this.level = new Level(this._io, this, this._root);
      this.inc = new Inc0(this._io, this, this._root);
      this.messageProof = new MessageProof(this._io, this, this._root);
    }

    return Some0;
  })();

  var OldLevelsMessages = Id014PtkathmaOperationUnsigned.OldLevelsMessages = (function() {
    function OldLevelsMessages(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    OldLevelsMessages.prototype._read = function() {
      this.index = new Int31(this._io, this, this._root);
      this.content = this._io.readBytes(32);
      this.backPointers = new BackPointers0(this._io, this, this._root);
    }

    return OldLevelsMessages;
  })();

  var After = Id014PtkathmaOperationUnsigned.After = (function() {
    function After(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    After.prototype._read = function() {
      this.afterTag = this._io.readU1();
      if (this.afterTag == Id014PtkathmaOperationUnsigned.AfterTag.VALUE) {
        this.value = this._io.readBytes(32);
      }
      if (this.afterTag == Id014PtkathmaOperationUnsigned.AfterTag.NODE) {
        this.node = this._io.readBytes(32);
      }
    }

    return After;
  })();

  var Case131EltField0 = Id014PtkathmaOperationUnsigned.Case131EltField0 = (function() {
    function Case131EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131EltField0.prototype._read = function() {
      this.case131EltField0 = this._io.readBytesFull();
    }

    return Case131EltField0;
  })();

  var Case3 = Id014PtkathmaOperationUnsigned.Case3 = (function() {
    function Case3(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case3.prototype._read = function() {
      this.case3Field0 = this._io.readS2be();
      this.case3Field1 = this._io.readBytes(32);
      this.case3Field2 = this._io.readBytes(32);
      this.case3Field3 = new Case3Field30(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case3;
  })();

  var Case224 = Id014PtkathmaOperationUnsigned.Case224 = (function() {
    function Case224(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case224.prototype._read = function() {
      this.case224Field0 = this._io.readU1();
      this.case224Field1 = new Case224Field10(this._io, this, this._root);
      this.case224Field2 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    return Case224;
  })();

  var Op22 = Id014PtkathmaOperationUnsigned.Op22 = (function() {
    function Op22(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op22.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_2/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op21(_io__raw_op2, this, this._root);
    }

    return Op22;
  })();

  var Case14Field1 = Id014PtkathmaOperationUnsigned.Case14Field1 = (function() {
    function Case14Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case14Field1.prototype._read = function() {
      this.case14Field1Field0 = this._io.readBytes(32);
      this.case14Field1Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case14Field1;
  })();

  var Skips = Id014PtkathmaOperationUnsigned.Skips = (function() {
    function Skips(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Skips.prototype._read = function() {
      this.skipsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.skipsEntries.push(new SkipsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Skips;
  })();

  var Inc0 = Id014PtkathmaOperationUnsigned.Inc0 = (function() {
    function Inc0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Inc0.prototype._read = function() {
      this.lenInc = this._io.readU4be();
      if (!(this.lenInc <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenInc, this._io, "/types/inc_0/seq/0");
      }
      this._raw_inc = this._io.readBytes(this.lenInc);
      var _io__raw_inc = new KaitaiStream(this._raw_inc);
      this.inc = new Inc(_io__raw_inc, this, this._root);
    }

    return Inc0;
  })();

  var ScRollupTimeout = Id014PtkathmaOperationUnsigned.ScRollupTimeout = (function() {
    function ScRollupTimeout(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupTimeout.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaRollupAddress(this._io, this, this._root);
      this.stakers = new Stakers(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupTimeout;
  })();

  var Case227Field10 = Id014PtkathmaOperationUnsigned.Case227Field10 = (function() {
    function Case227Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case227Field10.prototype._read = function() {
      this.lenCase227Field1 = this._io.readU1();
      if (!(this.lenCase227Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase227Field1, this._io, "/types/case__227_field1_0/seq/0");
      }
      this._raw_case227Field1 = this._io.readBytes(this.lenCase227Field1);
      var _io__raw_case227Field1 = new KaitaiStream(this._raw_case227Field1);
      this.case227Field1 = new Case227Field1(_io__raw_case227Field1, this, this._root);
    }

    return Case227Field10;
  })();

  var SparseProof0 = Id014PtkathmaOperationUnsigned.SparseProof0 = (function() {
    function SparseProof0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SparseProof0.prototype._read = function() {
      this.lenSparseProof = this._io.readU4be();
      if (!(this.lenSparseProof <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenSparseProof, this._io, "/types/sparse_proof_0/seq/0");
      }
      this._raw_sparseProof = this._io.readBytes(this.lenSparseProof);
      var _io__raw_sparseProof = new KaitaiStream(this._raw_sparseProof);
      this.sparseProof = new SparseProof(_io__raw_sparseProof, this, this._root);
    }

    return SparseProof0;
  })();

  var Message0 = Id014PtkathmaOperationUnsigned.Message0 = (function() {
    function Message0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message0.prototype._read = function() {
      this.messageEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messageEntries.push(new MessageEntries(this._io, this, this._root));
        i++;
      }
    }

    return Message0;
  })();

  var Case1930 = Id014PtkathmaOperationUnsigned.Case1930 = (function() {
    function Case1930(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1930.prototype._read = function() {
      this.lenCase193 = this._io.readU2be();
      if (!(this.lenCase193 <= 65535)) {
        throw new KaitaiStream.ValidationGreaterThanError(65535, this.lenCase193, this._io, "/types/case__193_0/seq/0");
      }
      this._raw_case193 = this._io.readBytes(this.lenCase193);
      var _io__raw_case193 = new KaitaiStream(this._raw_case193);
      this.case193 = new Case193(_io__raw_case193, this, this._root);
    }

    return Case1930;
  })();

  var BytesDynUint30 = Id014PtkathmaOperationUnsigned.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Case131Entries = Id014PtkathmaOperationUnsigned.Case131Entries = (function() {
    function Case131Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131Entries.prototype._read = function() {
      this.case131EltField0 = new Case131EltField00(this._io, this, this._root);
      this.case131EltField1 = new Case131EltField1(this._io, this, this._root);
    }

    return Case131Entries;
  })();

  var Bh1 = Id014PtkathmaOperationUnsigned.Bh1 = (function() {
    function Bh1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh1.prototype._read = function() {
      this.id014PtkathmaBlockHeaderAlphaFullHeader = new Id014PtkathmaBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh1;
  })();

  var SkipsEltField0 = Id014PtkathmaOperationUnsigned.SkipsEltField0 = (function() {
    function SkipsEltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SkipsEltField0.prototype._read = function() {
      this.rollup = new Id014PtkathmaRollupAddress(this._io, this, this._root);
      this.messageCounter = new N(this._io, this, this._root);
      this.nbAvailableMessages = this._io.readS8be();
      this.nbMessagesInCommitmentPeriod = this._io.readS8be();
      this.startingLevelOfCurrentCommitmentPeriod = this._io.readS4be();
      this.level = this._io.readS4be();
      this.currentMessagesHash = this._io.readBytes(32);
      this.oldLevelsMessages = new OldLevelsMessages(this._io, this, this._root);
    }

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return SkipsEltField0;
  })();

  var Dissection0 = Id014PtkathmaOperationUnsigned.Dissection0 = (function() {
    function Dissection0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Dissection0.prototype._read = function() {
      this.lenDissection = this._io.readU4be();
      if (!(this.lenDissection <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenDissection, this._io, "/types/dissection_0/seq/0");
      }
      this._raw_dissection = this._io.readBytes(this.lenDissection);
      var _io__raw_dissection = new KaitaiStream(this._raw_dissection);
      this.dissection = new Dissection(_io__raw_dissection, this, this._root);
    }

    return Dissection0;
  })();

  var Case0Field3 = Id014PtkathmaOperationUnsigned.Case0Field3 = (function() {
    function Case0Field3(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case0Field3.prototype._read = function() {
      this.case0Field3Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case0Field3Entries.push(new Case0Field3Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case0Field3;
  })();

  var TxRollupRejection = Id014PtkathmaOperationUnsigned.TxRollupRejection = (function() {
    function TxRollupRejection(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupRejection.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaTxRollupId(this._io, this, this._root);
      this.level = this._io.readS4be();
      this.message = new Message(this._io, this, this._root);
      this.messagePosition = new N(this._io, this, this._root);
      this.messagePath = new MessagePath0(this._io, this, this._root);
      this.messageResultHash = this._io.readBytes(32);
      this.messageResultPath = new MessageResultPath0(this._io, this, this._root);
      this.previousMessageResult = new PreviousMessageResult(this._io, this, this._root);
      this.previousMessageResultPath = new PreviousMessageResultPath0(this._io, this, this._root);
      this.proof = new Proof(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupRejection;
  })();

  var SetDepositsLimit = Id014PtkathmaOperationUnsigned.SetDepositsLimit = (function() {
    function SetDepositsLimit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SetDepositsLimit.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.limitTag = this._io.readU1();
      if (this.limitTag == Id014PtkathmaOperationUnsigned.Bool.TRUE) {
        this.limit = new Id014PtkathmaMutez(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return SetDepositsLimit;
  })();

  var Case15 = Id014PtkathmaOperationUnsigned.Case15 = (function() {
    function Case15(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case15.prototype._read = function() {
      this.case15Field0 = this._io.readS8be();
      this.case15Field1 = new Case15Field1(this._io, this, this._root);
    }

    return Case15;
  })();

  var Message1 = Id014PtkathmaOperationUnsigned.Message1 = (function() {
    function Message1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message1.prototype._read = function() {
      this.lenMessage = this._io.readU4be();
      if (!(this.lenMessage <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessage, this._io, "/types/message_1/seq/0");
      }
      this._raw_message = this._io.readBytes(this.lenMessage);
      var _io__raw_message = new KaitaiStream(this._raw_message);
      this.message = new Message0(_io__raw_message, this, this._root);
    }

    return Message1;
  })();

  var ScRollupRefute = Id014PtkathmaOperationUnsigned.ScRollupRefute = (function() {
    function ScRollupRefute(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupRefute.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaRollupAddress(this._io, this, this._root);
      this.opponent = new PublicKeyHash(this._io, this, this._root);
      this.refutation = new Refutation(this._io, this, this._root);
      this.isOpeningMove = this._io.readU1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return ScRollupRefute;
  })();

  var Op10 = Id014PtkathmaOperationUnsigned.Op10 = (function() {
    function Op10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op10.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_0/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op1(_io__raw_op1, this, this._root);
    }

    return Op10;
  })();

  var InodeExtender = Id014PtkathmaOperationUnsigned.InodeExtender = (function() {
    function InodeExtender(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InodeExtender.prototype._read = function() {
      this.length = this._io.readS8be();
      this.segment = new Segment0(this._io, this, this._root);
      this.proof = new InodeTree(this._io, this, this._root);
    }

    return InodeExtender;
  })();

  var Op21 = Id014PtkathmaOperationUnsigned.Op21 = (function() {
    function Op21(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op21.prototype._read = function() {
      this.id014PtkathmaInlinedPreendorsement = new Id014PtkathmaInlinedPreendorsement(this._io, this, this._root);
    }

    return Op21;
  })();

  var Proof = Id014PtkathmaOperationUnsigned.Proof = (function() {
    function Proof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proof.prototype._read = function() {
      this.proofTag = this._io.readU1();
      if (this.proofTag == Id014PtkathmaOperationUnsigned.ProofTag.CASE__0) {
        this.case0 = new Case0(this._io, this, this._root);
      }
      if (this.proofTag == Id014PtkathmaOperationUnsigned.ProofTag.CASE__2) {
        this.case2 = new Case2(this._io, this, this._root);
      }
      if (this.proofTag == Id014PtkathmaOperationUnsigned.ProofTag.CASE__1) {
        this.case1 = new Case1(this._io, this, this._root);
      }
      if (this.proofTag == Id014PtkathmaOperationUnsigned.ProofTag.CASE__3) {
        this.case3 = new Case3(this._io, this, this._root);
      }
    }

    return Proof;
  })();

  var Id014PtkathmaContractIdOriginated = Id014PtkathmaOperationUnsigned.Id014PtkathmaContractIdOriginated = (function() {
    function Id014PtkathmaContractIdOriginated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaContractIdOriginated.prototype._read = function() {
      this.id014PtkathmaContractIdOriginatedTag = this._io.readU1();
      if (this.id014PtkathmaContractIdOriginatedTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaContractIdOriginatedTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    return Id014PtkathmaContractIdOriginated;
  })();

  var Id014PtkathmaOperationAlphaUnsignedOperation = Id014PtkathmaOperationUnsigned.Id014PtkathmaOperationAlphaUnsignedOperation = (function() {
    function Id014PtkathmaOperationAlphaUnsignedOperation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaOperationAlphaUnsignedOperation.prototype._read = function() {
      this.id014PtkathmaOperationAlphaUnsignedOperation = new OperationShellHeader(this._io, this, null);
      this.contents = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.contents.push(new ContentsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id014PtkathmaOperationAlphaUnsignedOperation;
  })();

  var Commitment0 = Id014PtkathmaOperationUnsigned.Commitment0 = (function() {
    function Commitment0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitment0.prototype._read = function() {
      this.compressedState = this._io.readBytes(32);
      this.inboxLevel = this._io.readS4be();
      this.predecessor = this._io.readBytes(32);
      this.numberOfMessages = this._io.readS4be();
      this.numberOfTicks = this._io.readS4be();
    }

    return Commitment0;
  })();

  var Inc = Id014PtkathmaOperationUnsigned.Inc = (function() {
    function Inc(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Inc.prototype._read = function() {
      this.incEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.incEntries.push(new IncEntries(this._io, this, this._root));
        i++;
      }
    }

    return Inc;
  })();

  var Origination = Id014PtkathmaOperationUnsigned.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.balance = new Id014PtkathmaMutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id014PtkathmaOperationUnsigned.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id014PtkathmaScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var Case226Field10 = Id014PtkathmaOperationUnsigned.Case226Field10 = (function() {
    function Case226Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case226Field10.prototype._read = function() {
      this.lenCase226Field1 = this._io.readU1();
      if (!(this.lenCase226Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase226Field1, this._io, "/types/case__226_field1_0/seq/0");
      }
      this._raw_case226Field1 = this._io.readBytes(this.lenCase226Field1);
      var _io__raw_case226Field1 = new KaitaiStream(this._raw_case226Field1);
      this.case226Field1 = new Case226Field1(_io__raw_case226Field1, this, this._root);
    }

    return Case226Field10;
  })();

  var BackPointers = Id014PtkathmaOperationUnsigned.BackPointers = (function() {
    function BackPointers(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BackPointers.prototype._read = function() {
      this.backPointersEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.backPointersEntries.push(new BackPointersEntries(this._io, this, this._root));
        i++;
      }
    }

    return BackPointers;
  })();

  var Case1312 = Id014PtkathmaOperationUnsigned.Case1312 = (function() {
    function Case1312(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1312.prototype._read = function() {
      this.lenCase131 = this._io.readU4be();
      if (!(this.lenCase131 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase131, this._io, "/types/case__131_2/seq/0");
      }
      this._raw_case131 = this._io.readBytes(this.lenCase131);
      var _io__raw_case131 = new KaitaiStream(this._raw_case131);
      this.case131 = new Case131(_io__raw_case131, this, this._root);
    }

    return Case1312;
  })();

  var Case130EltField1 = Id014PtkathmaOperationUnsigned.Case130EltField1 = (function() {
    function Case130EltField1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130EltField1.prototype._read = function() {
      this.case130EltField1Tag = this._io.readU1();
      if (this.case130EltField1Tag == Id014PtkathmaOperationUnsigned.Case130EltField1Tag.CASE__0) {
        this.case0 = this._io.readBytes(32);
      }
      if (this.case130EltField1Tag == Id014PtkathmaOperationUnsigned.Case130EltField1Tag.CASE__1) {
        this.case1 = this._io.readBytes(32);
      }
    }

    return Case130EltField1;
  })();

  var Message = Id014PtkathmaOperationUnsigned.Message = (function() {
    function Message(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message.prototype._read = function() {
      this.messageTag = this._io.readU1();
      if (this.messageTag == Id014PtkathmaOperationUnsigned.MessageTag.BATCH) {
        this.batch = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.messageTag == Id014PtkathmaOperationUnsigned.MessageTag.DEPOSIT) {
        this.deposit = new Deposit(this._io, this, this._root);
      }
    }

    return Message;
  })();

  var Case1932 = Id014PtkathmaOperationUnsigned.Case1932 = (function() {
    function Case1932(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1932.prototype._read = function() {
      this.lenCase193 = this._io.readU2be();
      if (!(this.lenCase193 <= 65535)) {
        throw new KaitaiStream.ValidationGreaterThanError(65535, this.lenCase193, this._io, "/types/case__193_2/seq/0");
      }
      this._raw_case193 = this._io.readBytes(this.lenCase193);
      var _io__raw_case193 = new KaitaiStream(this._raw_case193);
      this.case193 = new Case193(_io__raw_case193, this, this._root);
    }

    return Case1932;
  })();

  var Node = Id014PtkathmaOperationUnsigned.Node = (function() {
    function Node(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Node.prototype._read = function() {
      this.nodeEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.nodeEntries.push(new NodeEntries(this._io, this, this._root));
        i++;
      }
    }

    return Node;
  })();

  var Op2 = Id014PtkathmaOperationUnsigned.Op2 = (function() {
    function Op2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op2.prototype._read = function() {
      this.id014PtkathmaInlinedEndorsement = new Id014PtkathmaInlinedEndorsement(this._io, this, this._root);
    }

    return Op2;
  })();

  var Proofs0 = Id014PtkathmaOperationUnsigned.Proofs0 = (function() {
    function Proofs0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proofs0.prototype._read = function() {
      this.proofsTag = this._io.readU1();
      if (this.proofsTag == Id014PtkathmaOperationUnsigned.ProofsTag.SPARSE_PROOF) {
        this.sparseProof = new SparseProof2(this._io, this, this._root);
      }
      if (this.proofsTag == Id014PtkathmaOperationUnsigned.ProofsTag.DENSE_PROOF) {
        this.denseProof = new DenseProofEntries(this._io, this, this._root);
      }
    }

    return Proofs0;
  })();

  var Case7 = Id014PtkathmaOperationUnsigned.Case7 = (function() {
    function Case7(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case7.prototype._read = function() {
      this.case7Field0 = this._io.readS8be();
      this.case7Field1 = this._io.readBytes(32);
    }

    /**
     * context_hash
     * 
     * case__7_field1_field0
     */

    return Case7;
  })();

  var NodeEntries = Id014PtkathmaOperationUnsigned.NodeEntries = (function() {
    function NodeEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NodeEntries.prototype._read = function() {
      this.nodeEltField0 = new NodeEltField00(this._io, this, this._root);
      this.nodeEltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return NodeEntries;
  })();

  var TxRollupReturnBond = Id014PtkathmaOperationUnsigned.TxRollupReturnBond = (function() {
    function TxRollupReturnBond(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupReturnBond.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaTxRollupId(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupReturnBond;
  })();

  var Dissection = Id014PtkathmaOperationUnsigned.Dissection = (function() {
    function Dissection(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Dissection.prototype._read = function() {
      this.dissectionEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.dissectionEntries.push(new DissectionEntries(this._io, this, this._root));
        i++;
      }
    }

    return Dissection;
  })();

  var Case224Field10 = Id014PtkathmaOperationUnsigned.Case224Field10 = (function() {
    function Case224Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case224Field10.prototype._read = function() {
      this.lenCase224Field1 = this._io.readU1();
      if (!(this.lenCase224Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase224Field1, this._io, "/types/case__224_field1_0/seq/0");
      }
      this._raw_case224Field1 = this._io.readBytes(this.lenCase224Field1);
      var _io__raw_case224Field1 = new KaitaiStream(this._raw_case224Field1);
      this.case224Field1 = new Case224Field1(_io__raw_case224Field1, this, this._root);
    }

    return Case224Field10;
  })();

  var TicketsInfo0 = Id014PtkathmaOperationUnsigned.TicketsInfo0 = (function() {
    function TicketsInfo0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TicketsInfo0.prototype._read = function() {
      this.lenTicketsInfo = this._io.readU4be();
      if (!(this.lenTicketsInfo <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenTicketsInfo, this._io, "/types/tickets_info_0/seq/0");
      }
      this._raw_ticketsInfo = this._io.readBytes(this.lenTicketsInfo);
      var _io__raw_ticketsInfo = new KaitaiStream(this._raw_ticketsInfo);
      this.ticketsInfo = new TicketsInfo(_io__raw_ticketsInfo, this, this._root);
    }

    return TicketsInfo0;
  })();

  var TxRollupFinalizeCommitment = Id014PtkathmaOperationUnsigned.TxRollupFinalizeCommitment = (function() {
    function TxRollupFinalizeCommitment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupFinalizeCommitment.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaTxRollupId(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupFinalizeCommitment;
  })();

  var Id014PtkathmaScriptedContracts = Id014PtkathmaOperationUnsigned.Id014PtkathmaScriptedContracts = (function() {
    function Id014PtkathmaScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id014PtkathmaScriptedContracts;
  })();

  var InodeTree0 = Id014PtkathmaOperationUnsigned.InodeTree0 = (function() {
    function InodeTree0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InodeTree0.prototype._read = function() {
      this.inodeTreeTag = this._io.readU1();
      if (this.inodeTreeTag == Id014PtkathmaOperationUnsigned.InodeTreeTag.BLINDED_INODE) {
        this.blindedInode = this._io.readBytes(32);
      }
      if (this.inodeTreeTag == Id014PtkathmaOperationUnsigned.InodeTreeTag.INODE_VALUES) {
        this.inodeValues = new InodeValues0(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id014PtkathmaOperationUnsigned.InodeTreeTag.INODE_TREE) {
        this.inodeTree = new InodeTree(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id014PtkathmaOperationUnsigned.InodeTreeTag.INODE_EXTENDER) {
        this.inodeExtender = new InodeExtender(this._io, this, this._root);
      }
    }

    return InodeTree0;
  })();

  var TxRollupOrigination = Id014PtkathmaOperationUnsigned.TxRollupOrigination = (function() {
    function TxRollupOrigination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupOrigination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return TxRollupOrigination;
  })();

  var Case227Field1 = Id014PtkathmaOperationUnsigned.Case227Field1 = (function() {
    function Case227Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case227Field1.prototype._read = function() {
      this.case227Field1 = this._io.readBytesFull();
    }

    return Case227Field1;
  })();

  var MessageResultPath = Id014PtkathmaOperationUnsigned.MessageResultPath = (function() {
    function MessageResultPath(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageResultPath.prototype._read = function() {
      this.messageResultPathEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messageResultPathEntries.push(new MessageResultPathEntries(this._io, this, this._root));
        i++;
      }
    }

    return MessageResultPath;
  })();

  var Id014PtkathmaInlinedEndorsementMempoolContents = Id014PtkathmaOperationUnsigned.Id014PtkathmaInlinedEndorsementMempoolContents = (function() {
    function Id014PtkathmaInlinedEndorsementMempoolContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaInlinedEndorsementMempoolContents.prototype._read = function() {
      this.id014PtkathmaInlinedEndorsementMempoolContentsTag = this._io.readU1();
      if (this.id014PtkathmaInlinedEndorsementMempoolContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaInlinedEndorsementMempoolContentsTag.ENDORSEMENT) {
        this.endorsement = new Endorsement(this._io, this, this._root);
      }
    }

    return Id014PtkathmaInlinedEndorsementMempoolContents;
  })();

  var Int31 = Id014PtkathmaOperationUnsigned.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id014PtkathmaOperationUnsigned.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var MessageResultPath0 = Id014PtkathmaOperationUnsigned.MessageResultPath0 = (function() {
    function MessageResultPath0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageResultPath0.prototype._read = function() {
      this.lenMessageResultPath = this._io.readU4be();
      if (!(this.lenMessageResultPath <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessageResultPath, this._io, "/types/message_result_path_0/seq/0");
      }
      this._raw_messageResultPath = this._io.readBytes(this.lenMessageResultPath);
      var _io__raw_messageResultPath = new KaitaiStream(this._raw_messageResultPath);
      this.messageResultPath = new MessageResultPath(_io__raw_messageResultPath, this, this._root);
    }

    return MessageResultPath0;
  })();

  var InodeValuesEltField0 = Id014PtkathmaOperationUnsigned.InodeValuesEltField0 = (function() {
    function InodeValuesEltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InodeValuesEltField0.prototype._read = function() {
      this.inodeValuesEltField0 = this._io.readBytesFull();
    }

    return InodeValuesEltField0;
  })();

  var InodeValuesEltField00 = Id014PtkathmaOperationUnsigned.InodeValuesEltField00 = (function() {
    function InodeValuesEltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InodeValuesEltField00.prototype._read = function() {
      this.lenInodeValuesEltField0 = this._io.readU1();
      if (!(this.lenInodeValuesEltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenInodeValuesEltField0, this._io, "/types/inode_values_elt_field0_0/seq/0");
      }
      this._raw_inodeValuesEltField0 = this._io.readBytes(this.lenInodeValuesEltField0);
      var _io__raw_inodeValuesEltField0 = new KaitaiStream(this._raw_inodeValuesEltField0);
      this.inodeValuesEltField0 = new InodeValuesEltField0(_io__raw_inodeValuesEltField0, this, this._root);
    }

    return InodeValuesEltField00;
  })();

  var MessagePathEntries = Id014PtkathmaOperationUnsigned.MessagePathEntries = (function() {
    function MessagePathEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagePathEntries.prototype._read = function() {
      this.inboxListHash = this._io.readBytes(32);
    }

    return MessagePathEntries;
  })();

  var Id014PtkathmaEntrypoint = Id014PtkathmaOperationUnsigned.Id014PtkathmaEntrypoint = (function() {
    function Id014PtkathmaEntrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaEntrypoint.prototype._read = function() {
      this.id014PtkathmaEntrypointTag = this._io.readU1();
      if (this.id014PtkathmaEntrypointTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaEntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id014PtkathmaEntrypoint;
  })();

  var Proofs = Id014PtkathmaOperationUnsigned.Proofs = (function() {
    function Proofs(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proofs.prototype._read = function() {
      this.proofsTag = this._io.readU1();
      if (this.proofsTag == Id014PtkathmaOperationUnsigned.ProofsTag.SPARSE_PROOF) {
        this.sparseProof = new SparseProof0(this._io, this, this._root);
      }
      if (this.proofsTag == Id014PtkathmaOperationUnsigned.ProofsTag.DENSE_PROOF) {
        this.denseProof = new DenseProofEntries(this._io, this, this._root);
      }
    }

    return Proofs;
  })();

  var SparseProof1 = Id014PtkathmaOperationUnsigned.SparseProof1 = (function() {
    function SparseProof1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SparseProof1.prototype._read = function() {
      this.sparseProofEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.sparseProofEntries.push(new SparseProofEntries0(this._io, this, this._root));
        i++;
      }
    }

    return SparseProof1;
  })();

  var SkipsEltField1Entries = Id014PtkathmaOperationUnsigned.SkipsEltField1Entries = (function() {
    function SkipsEltField1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SkipsEltField1Entries.prototype._read = function() {
      this.index = new Int31(this._io, this, this._root);
      this.content = this._io.readBytes(32);
      this.backPointers = new BackPointers0(this._io, this, this._root);
    }

    return SkipsEltField1Entries;
  })();

  var TreeProof = Id014PtkathmaOperationUnsigned.TreeProof = (function() {
    function TreeProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TreeProof.prototype._read = function() {
      this.version = this._io.readS2be();
      this.before = new Before(this._io, this, this._root);
      this.after = new After(this._io, this, this._root);
      this.state = new TreeEncoding(this._io, this, this._root);
    }

    return TreeProof;
  })();

  var DalSlotAvailability = Id014PtkathmaOperationUnsigned.DalSlotAvailability = (function() {
    function DalSlotAvailability(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalSlotAvailability.prototype._read = function() {
      this.endorser = new PublicKeyHash(this._io, this, this._root);
      this.endorsement = new Z(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return DalSlotAvailability;
  })();

  var Id014PtkathmaTxRollupId = Id014PtkathmaOperationUnsigned.Id014PtkathmaTxRollupId = (function() {
    function Id014PtkathmaTxRollupId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaTxRollupId.prototype._read = function() {
      this.rollupHash = this._io.readBytes(20);
    }

    return Id014PtkathmaTxRollupId;
  })();

  var Slot = Id014PtkathmaOperationUnsigned.Slot = (function() {
    function Slot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Slot.prototype._read = function() {
      this.level = this._io.readS4be();
      this.index = this._io.readU1();
      this.header = new Int31(this._io, this, this._root);
    }

    return Slot;
  })();

  var Id014PtkathmaInlinedPreendorsementContents = Id014PtkathmaOperationUnsigned.Id014PtkathmaInlinedPreendorsementContents = (function() {
    function Id014PtkathmaInlinedPreendorsementContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaInlinedPreendorsementContents.prototype._read = function() {
      this.id014PtkathmaInlinedPreendorsementContentsTag = this._io.readU1();
      if (this.id014PtkathmaInlinedPreendorsementContentsTag == Id014PtkathmaOperationUnsigned.Id014PtkathmaInlinedPreendorsementContentsTag.PREENDORSEMENT) {
        this.preendorsement = new Preendorsement(this._io, this, this._root);
      }
    }

    return Id014PtkathmaInlinedPreendorsementContents;
  })();

  var Deposit = Id014PtkathmaOperationUnsigned.Deposit = (function() {
    function Deposit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposit.prototype._read = function() {
      this.sender = new PublicKeyHash(this._io, this, this._root);
      this.destination = this._io.readBytes(20);
      this.ticketHash = this._io.readBytes(32);
      this.amount = new Amount(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Deposit;
  })();

  var MessagePath = Id014PtkathmaOperationUnsigned.MessagePath = (function() {
    function MessagePath(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagePath.prototype._read = function() {
      this.messagePathEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messagePathEntries.push(new MessagePathEntries(this._io, this, this._root));
        i++;
      }
    }

    return MessagePath;
  })();

  var Named0 = Id014PtkathmaOperationUnsigned.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id014PtkathmaOperationUnsigned.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.amount = new Id014PtkathmaMutez(this._io, this, this._root);
      this.destination = new Id014PtkathmaContractId(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id014PtkathmaOperationUnsigned.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Transaction;
  })();

  var Case14 = Id014PtkathmaOperationUnsigned.Case14 = (function() {
    function Case14(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case14.prototype._read = function() {
      this.case14Field0 = this._io.readS4be();
      this.case14Field1 = new Case14Field1(this._io, this, this._root);
    }

    return Case14;
  })();

  var Id014PtkathmaBlockHeaderAlphaFullHeader = Id014PtkathmaOperationUnsigned.Id014PtkathmaBlockHeaderAlphaFullHeader = (function() {
    function Id014PtkathmaBlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaBlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id014PtkathmaBlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id014PtkathmaBlockHeaderAlphaSignedContents = new Id014PtkathmaBlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id014PtkathmaBlockHeaderAlphaFullHeader;
  })();

  var Parameters = Id014PtkathmaOperationUnsigned.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id014PtkathmaEntrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var Some = Id014PtkathmaOperationUnsigned.Some = (function() {
    function Some(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Some.prototype._read = function() {
      this.inboxLevel = this._io.readS4be();
      this.messageCounter = new N(this._io, this, this._root);
      this.payload = new BytesDynUint30(this._io, this, this._root);
    }

    return Some;
  })();

  var BackPointersEntries = Id014PtkathmaOperationUnsigned.BackPointersEntries = (function() {
    function BackPointersEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BackPointersEntries.prototype._read = function() {
      this.inboxHash = this._io.readBytes(32);
    }

    return BackPointersEntries;
  })();

  var Case225 = Id014PtkathmaOperationUnsigned.Case225 = (function() {
    function Case225(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case225.prototype._read = function() {
      this.case225Field0 = this._io.readU2be();
      this.case225Field1 = new Case225Field10(this._io, this, this._root);
      this.case225Field2 = this._io.readBytes(32);
    }

    /**
     * context_hash
     */

    return Case225;
  })();

  var Case224Field1 = Id014PtkathmaOperationUnsigned.Case224Field1 = (function() {
    function Case224Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case224Field1.prototype._read = function() {
      this.case224Field1 = this._io.readBytesFull();
    }

    return Case224Field1;
  })();

  var Messages0 = Id014PtkathmaOperationUnsigned.Messages0 = (function() {
    function Messages0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Messages0.prototype._read = function() {
      this.lenMessages = this._io.readU4be();
      if (!(this.lenMessages <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessages, this._io, "/types/messages_0/seq/0");
      }
      this._raw_messages = this._io.readBytes(this.lenMessages);
      var _io__raw_messages = new KaitaiStream(this._raw_messages);
      this.messages = new Messages(_io__raw_messages, this, this._root);
    }

    return Messages0;
  })();

  var InodeValuesEntries = Id014PtkathmaOperationUnsigned.InodeValuesEntries = (function() {
    function InodeValuesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InodeValuesEntries.prototype._read = function() {
      this.inodeValuesEltField0 = new InodeValuesEltField00(this._io, this, this._root);
      this.inodeValuesEltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return InodeValuesEntries;
  })();

  var Case1920 = Id014PtkathmaOperationUnsigned.Case1920 = (function() {
    function Case1920(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1920.prototype._read = function() {
      this.lenCase192 = this._io.readU1();
      if (!(this.lenCase192 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase192, this._io, "/types/case__192_0/seq/0");
      }
      this._raw_case192 = this._io.readBytes(this.lenCase192);
      var _io__raw_case192 = new KaitaiStream(this._raw_case192);
      this.case192 = new Case192(_io__raw_case192, this, this._root);
    }

    return Case1920;
  })();

  var Proof0 = Id014PtkathmaOperationUnsigned.Proof0 = (function() {
    function Proof0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proof0.prototype._read = function() {
      this.pvmStep = new PvmStep(this._io, this, this._root);
      this.inbox = new Inbox(this._io, this, this._root);
    }

    return Proof0;
  })();

  var Case1313 = Id014PtkathmaOperationUnsigned.Case1313 = (function() {
    function Case1313(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1313.prototype._read = function() {
      this.lenCase131 = this._io.readU4be();
      if (!(this.lenCase131 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase131, this._io, "/types/case__131_3/seq/0");
      }
      this._raw_case131 = this._io.readBytes(this.lenCase131);
      var _io__raw_case131 = new KaitaiStream(this._raw_case131);
      this.case131 = new Case131(_io__raw_case131, this, this._root);
    }

    return Case1313;
  })();

  var Proposals1 = Id014PtkathmaOperationUnsigned.Proposals1 = (function() {
    function Proposals1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals1.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposals = new Proposals0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Proposals1;
  })();

  var TxRollupSubmitBatch = Id014PtkathmaOperationUnsigned.TxRollupSubmitBatch = (function() {
    function TxRollupSubmitBatch(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupSubmitBatch.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id014PtkathmaTxRollupId(this._io, this, this._root);
      this.content = new BytesDynUint30(this._io, this, this._root);
      this.burnLimitTag = this._io.readU1();
      if (this.burnLimitTag == Id014PtkathmaOperationUnsigned.Bool.TRUE) {
        this.burnLimit = new Id014PtkathmaMutez(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupSubmitBatch;
  })();

  var Id014PtkathmaInlinedPreendorsement = Id014PtkathmaOperationUnsigned.Id014PtkathmaInlinedPreendorsement = (function() {
    function Id014PtkathmaInlinedPreendorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaInlinedPreendorsement.prototype._read = function() {
      this.id014PtkathmaInlinedPreendorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id014PtkathmaInlinedPreendorsementContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id014PtkathmaOperationUnsigned.Bool.TRUE) {
        this.signature = this._io.readBytes(64);
      }
    }

    return Id014PtkathmaInlinedPreendorsement;
  })();

  var DissectionEntries = Id014PtkathmaOperationUnsigned.DissectionEntries = (function() {
    function DissectionEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DissectionEntries.prototype._read = function() {
      this.dissectionEltField0 = new DissectionEltField0(this._io, this, this._root);
      this.dissectionEltField1 = new N(this._io, this, this._root);
    }

    return DissectionEntries;
  })();

  var Case130EltField00 = Id014PtkathmaOperationUnsigned.Case130EltField00 = (function() {
    function Case130EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130EltField00.prototype._read = function() {
      this.lenCase130EltField0 = this._io.readU1();
      if (!(this.lenCase130EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase130EltField0, this._io, "/types/case__130_elt_field0_0/seq/0");
      }
      this._raw_case130EltField0 = this._io.readBytes(this.lenCase130EltField0);
      var _io__raw_case130EltField0 = new KaitaiStream(this._raw_case130EltField0);
      this.case130EltField0 = new Case130EltField0(_io__raw_case130EltField0, this, this._root);
    }

    return Case130EltField00;
  })();

  var TicketsInfo = Id014PtkathmaOperationUnsigned.TicketsInfo = (function() {
    function TicketsInfo(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TicketsInfo.prototype._read = function() {
      this.ticketsInfoEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.ticketsInfoEntries.push(new TicketsInfoEntries(this._io, this, this._root));
        i++;
      }
    }

    return TicketsInfo;
  })();

  var Ballot = Id014PtkathmaOperationUnsigned.Ballot = (function() {
    function Ballot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Ballot.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposal = this._io.readBytes(32);
      this.ballot = this._io.readS1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Ballot;
  })();

  var Predecessor = Id014PtkathmaOperationUnsigned.Predecessor = (function() {
    function Predecessor(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Predecessor.prototype._read = function() {
      this.predecessorTag = this._io.readU1();
      if (this.predecessorTag == Id014PtkathmaOperationUnsigned.PredecessorTag.SOME) {
        this.some = this._io.readBytes(32);
      }
    }

    return Predecessor;
  })();

  var DalPublishSlotHeader = Id014PtkathmaOperationUnsigned.DalPublishSlotHeader = (function() {
    function DalPublishSlotHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalPublishSlotHeader.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id014PtkathmaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.slot = new Slot(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return DalPublishSlotHeader;
  })();

  var PublicKeyHash = Id014PtkathmaOperationUnsigned.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id014PtkathmaOperationUnsigned.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id014PtkathmaOperationUnsigned.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id014PtkathmaOperationUnsigned.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Case1Field3 = Id014PtkathmaOperationUnsigned.Case1Field3 = (function() {
    function Case1Field3(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1Field3.prototype._read = function() {
      this.case1Field3Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case1Field3Entries.push(new Case1Field3Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case1Field3;
  })();

  var MessagePath0 = Id014PtkathmaOperationUnsigned.MessagePath0 = (function() {
    function MessagePath0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagePath0.prototype._read = function() {
      this.lenMessagePath = this._io.readU4be();
      if (!(this.lenMessagePath <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessagePath, this._io, "/types/message_path_0/seq/0");
      }
      this._raw_messagePath = this._io.readBytes(this.lenMessagePath);
      var _io__raw_messagePath = new KaitaiStream(this._raw_messagePath);
      this.messagePath = new MessagePath(_io__raw_messagePath, this, this._root);
    }

    return MessagePath0;
  })();

  var Op11 = Id014PtkathmaOperationUnsigned.Op11 = (function() {
    function Op11(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op11.prototype._read = function() {
      this.id014PtkathmaInlinedPreendorsement = new Id014PtkathmaInlinedPreendorsement(this._io, this, this._root);
    }

    return Op11;
  })();

  var Proposals = Id014PtkathmaOperationUnsigned.Proposals = (function() {
    function Proposals(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals.prototype._read = function() {
      this.proposalsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.proposalsEntries.push(new ProposalsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Proposals;
  })();

  var Case1931 = Id014PtkathmaOperationUnsigned.Case1931 = (function() {
    function Case1931(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1931.prototype._read = function() {
      this.lenCase193 = this._io.readU2be();
      if (!(this.lenCase193 <= 65535)) {
        throw new KaitaiStream.ValidationGreaterThanError(65535, this.lenCase193, this._io, "/types/case__193_1/seq/0");
      }
      this._raw_case193 = this._io.readBytes(this.lenCase193);
      var _io__raw_case193 = new KaitaiStream(this._raw_case193);
      this.case193 = new Case193(_io__raw_case193, this, this._root);
    }

    return Case1931;
  })();

  var Z = Id014PtkathmaOperationUnsigned.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  var SparseProofEntries0 = Id014PtkathmaOperationUnsigned.SparseProofEntries0 = (function() {
    function SparseProofEntries0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SparseProofEntries0.prototype._read = function() {
      this.sparseProofEltField0 = this._io.readU1();
      this.sparseProofEltField1 = new InodeTree0(this._io, this, this._root);
    }

    /**
     * inode_tree
     */

    return SparseProofEntries0;
  })();

  var ProposalsEntries = Id014PtkathmaOperationUnsigned.ProposalsEntries = (function() {
    function ProposalsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ProposalsEntries.prototype._read = function() {
      this.protocolHash = this._io.readBytes(32);
    }

    return ProposalsEntries;
  })();

  var Op1 = Id014PtkathmaOperationUnsigned.Op1 = (function() {
    function Op1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op1.prototype._read = function() {
      this.id014PtkathmaInlinedEndorsement = new Id014PtkathmaInlinedEndorsement(this._io, this, this._root);
    }

    return Op1;
  })();

  var Level = Id014PtkathmaOperationUnsigned.Level = (function() {
    function Level(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Level.prototype._read = function() {
      this.rollup = new Id014PtkathmaRollupAddress(this._io, this, this._root);
      this.messageCounter = new N(this._io, this, this._root);
      this.nbAvailableMessages = this._io.readS8be();
      this.nbMessagesInCommitmentPeriod = this._io.readS8be();
      this.startingLevelOfCurrentCommitmentPeriod = this._io.readS4be();
      this.level = this._io.readS4be();
      this.currentMessagesHash = this._io.readBytes(32);
      this.oldLevelsMessages = new OldLevelsMessages(this._io, this, this._root);
    }

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return Level;
  })();

  var Case0Field30 = Id014PtkathmaOperationUnsigned.Case0Field30 = (function() {
    function Case0Field30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case0Field30.prototype._read = function() {
      this.lenCase0Field3 = this._io.readU4be();
      if (!(this.lenCase0Field3 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase0Field3, this._io, "/types/case__0_field3_0/seq/0");
      }
      this._raw_case0Field3 = this._io.readBytes(this.lenCase0Field3);
      var _io__raw_case0Field3 = new KaitaiStream(this._raw_case0Field3);
      this.case0Field3 = new Case0Field3(_io__raw_case0Field3, this, this._root);
    }

    return Case0Field30;
  })();

  var DoublePreendorsementEvidence = Id014PtkathmaOperationUnsigned.DoublePreendorsementEvidence = (function() {
    function DoublePreendorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoublePreendorsementEvidence.prototype._read = function() {
      this.op1 = new Op12(this._io, this, this._root);
      this.op2 = new Op22(this._io, this, this._root);
    }

    return DoublePreendorsementEvidence;
  })();

  var InodeValues0 = Id014PtkathmaOperationUnsigned.InodeValues0 = (function() {
    function InodeValues0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InodeValues0.prototype._read = function() {
      this.lenInodeValues = this._io.readU4be();
      if (!(this.lenInodeValues <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenInodeValues, this._io, "/types/inode_values_0/seq/0");
      }
      this._raw_inodeValues = this._io.readBytes(this.lenInodeValues);
      var _io__raw_inodeValues = new KaitaiStream(this._raw_inodeValues);
      this.inodeValues = new InodeValues(_io__raw_inodeValues, this, this._root);
    }

    return InodeValues0;
  })();

  return Id014PtkathmaOperationUnsigned;
})();
return Id014PtkathmaOperationUnsigned;
}));
