// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './OperationShellHeader', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./OperationShellHeader'), require('./BlockHeaderShell'));
  } else {
    root.Id005Psbabym1OperationProtocolData = factory(root.KaitaiStream, root.OperationShellHeader, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, OperationShellHeader, BlockHeaderShell) {
/**
 * Encoding id: 005-PsBabyM1.operation.protocol_data
 */

var Id005Psbabym1OperationProtocolData = (function() {
  Id005Psbabym1OperationProtocolData.Id005Psbabym1InlinedEndorsementContentsTag = Object.freeze({
    ENDORSEMENT: 0,

    0: "ENDORSEMENT",
  });

  Id005Psbabym1OperationProtocolData.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id005Psbabym1OperationProtocolData.Id005Psbabym1EntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    255: "NAMED",
  });

  Id005Psbabym1OperationProtocolData.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id005Psbabym1OperationProtocolData.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id005Psbabym1OperationProtocolData.Id005Psbabym1ContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsTag = Object.freeze({
    ENDORSEMENT: 0,
    SEED_NONCE_REVELATION: 1,
    DOUBLE_ENDORSEMENT_EVIDENCE: 2,
    DOUBLE_BAKING_EVIDENCE: 3,
    ACTIVATE_ACCOUNT: 4,
    PROPOSALS: 5,
    BALLOT: 6,
    REVEAL: 107,
    TRANSACTION: 108,
    ORIGINATION: 109,
    DELEGATION: 110,

    0: "ENDORSEMENT",
    1: "SEED_NONCE_REVELATION",
    2: "DOUBLE_ENDORSEMENT_EVIDENCE",
    3: "DOUBLE_BAKING_EVIDENCE",
    4: "ACTIVATE_ACCOUNT",
    5: "PROPOSALS",
    6: "BALLOT",
    107: "REVEAL",
    108: "TRANSACTION",
    109: "ORIGINATION",
    110: "DELEGATION",
  });

  function Id005Psbabym1OperationProtocolData(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1OperationProtocolData.prototype._read = function() {
    this.id005Psbabym1OperationAlphaContentsAndSignature = new Id005Psbabym1OperationAlphaContentsAndSignature(this._io, this, this._root);
  }

  var Op20 = Id005Psbabym1OperationProtocolData.Op20 = (function() {
    function Op20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op20.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_0/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op2(_io__raw_op2, this, this._root);
    }

    return Op20;
  })();

  var Id005Psbabym1Entrypoint = Id005Psbabym1OperationProtocolData.Id005Psbabym1Entrypoint = (function() {
    function Id005Psbabym1Entrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1Entrypoint.prototype._read = function() {
      this.id005Psbabym1EntrypointTag = this._io.readU1();
      if (this.id005Psbabym1EntrypointTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1EntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id005Psbabym1Entrypoint;
  })();

  var ActivateAccount = Id005Psbabym1OperationProtocolData.ActivateAccount = (function() {
    function ActivateAccount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ActivateAccount.prototype._read = function() {
      this.pkh = this._io.readBytes(20);
      this.secret = this._io.readBytes(20);
    }

    return ActivateAccount;
  })();

  var DoubleEndorsementEvidence = Id005Psbabym1OperationProtocolData.DoubleEndorsementEvidence = (function() {
    function DoubleEndorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleEndorsementEvidence.prototype._read = function() {
      this.op1 = new Op10(this._io, this, this._root);
      this.op2 = new Op20(this._io, this, this._root);
    }

    return DoubleEndorsementEvidence;
  })();

  var Originated = Id005Psbabym1OperationProtocolData.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var N = Id005Psbabym1OperationProtocolData.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Proposals0 = Id005Psbabym1OperationProtocolData.Proposals0 = (function() {
    function Proposals0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals0.prototype._read = function() {
      this.lenProposals = this._io.readU4be();
      if (!(this.lenProposals <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenProposals, this._io, "/types/proposals_0/seq/0");
      }
      this._raw_proposals = this._io.readBytes(this.lenProposals);
      var _io__raw_proposals = new KaitaiStream(this._raw_proposals);
      this.proposals = new Proposals(_io__raw_proposals, this, this._root);
    }

    return Proposals0;
  })();

  var Reveal = Id005Psbabym1OperationProtocolData.Reveal = (function() {
    function Reveal(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Reveal.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id005Psbabym1Mutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.publicKey = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Reveal;
  })();

  var Id005Psbabym1Mutez = Id005Psbabym1OperationProtocolData.Id005Psbabym1Mutez = (function() {
    function Id005Psbabym1Mutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1Mutez.prototype._read = function() {
      this.id005Psbabym1Mutez = new N(this._io, this, this._root);
    }

    return Id005Psbabym1Mutez;
  })();

  var Id005Psbabym1OperationAlphaContents = Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContents = (function() {
    function Id005Psbabym1OperationAlphaContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1OperationAlphaContents.prototype._read = function() {
      this.id005Psbabym1OperationAlphaContentsTag = this._io.readU1();
      if (this.id005Psbabym1OperationAlphaContentsTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsTag.ENDORSEMENT) {
        this.endorsement = this._io.readS4be();
      }
      if (this.id005Psbabym1OperationAlphaContentsTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsTag.SEED_NONCE_REVELATION) {
        this.seedNonceRevelation = new SeedNonceRevelation(this._io, this, this._root);
      }
      if (this.id005Psbabym1OperationAlphaContentsTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsTag.DOUBLE_ENDORSEMENT_EVIDENCE) {
        this.doubleEndorsementEvidence = new DoubleEndorsementEvidence(this._io, this, this._root);
      }
      if (this.id005Psbabym1OperationAlphaContentsTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsTag.DOUBLE_BAKING_EVIDENCE) {
        this.doubleBakingEvidence = new DoubleBakingEvidence(this._io, this, this._root);
      }
      if (this.id005Psbabym1OperationAlphaContentsTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsTag.ACTIVATE_ACCOUNT) {
        this.activateAccount = new ActivateAccount(this._io, this, this._root);
      }
      if (this.id005Psbabym1OperationAlphaContentsTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsTag.PROPOSALS) {
        this.proposals = new Proposals1(this._io, this, this._root);
      }
      if (this.id005Psbabym1OperationAlphaContentsTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsTag.BALLOT) {
        this.ballot = new Ballot(this._io, this, this._root);
      }
      if (this.id005Psbabym1OperationAlphaContentsTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsTag.REVEAL) {
        this.reveal = new Reveal(this._io, this, this._root);
      }
      if (this.id005Psbabym1OperationAlphaContentsTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id005Psbabym1OperationAlphaContentsTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id005Psbabym1OperationAlphaContentsTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
    }

    return Id005Psbabym1OperationAlphaContents;
  })();

  var SeedNonceRevelation = Id005Psbabym1OperationProtocolData.SeedNonceRevelation = (function() {
    function SeedNonceRevelation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SeedNonceRevelation.prototype._read = function() {
      this.level = this._io.readS4be();
      this.nonce = this._io.readBytes(32);
    }

    return SeedNonceRevelation;
  })();

  var Id005Psbabym1InlinedEndorsement = Id005Psbabym1OperationProtocolData.Id005Psbabym1InlinedEndorsement = (function() {
    function Id005Psbabym1InlinedEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1InlinedEndorsement.prototype._read = function() {
      this.id005Psbabym1InlinedEndorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id005Psbabym1InlinedEndorsementContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id005Psbabym1OperationProtocolData.Bool.TRUE) {
        this.signature = this._io.readBytes(64);
      }
    }

    return Id005Psbabym1InlinedEndorsement;
  })();

  var DoubleBakingEvidence = Id005Psbabym1OperationProtocolData.DoubleBakingEvidence = (function() {
    function DoubleBakingEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleBakingEvidence.prototype._read = function() {
      this.bh1 = new Bh10(this._io, this, this._root);
      this.bh2 = new Bh20(this._io, this, this._root);
    }

    return DoubleBakingEvidence;
  })();

  var PublicKey = Id005Psbabym1OperationProtocolData.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id005Psbabym1OperationProtocolData.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id005Psbabym1OperationProtocolData.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id005Psbabym1OperationProtocolData.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var Id005Psbabym1BlockHeaderAlphaUnsignedContents = Id005Psbabym1OperationProtocolData.Id005Psbabym1BlockHeaderAlphaUnsignedContents = (function() {
    function Id005Psbabym1BlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1BlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id005Psbabym1OperationProtocolData.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
    }

    return Id005Psbabym1BlockHeaderAlphaUnsignedContents;
  })();

  var Id005Psbabym1OperationAlphaContentsAndSignature = Id005Psbabym1OperationProtocolData.Id005Psbabym1OperationAlphaContentsAndSignature = (function() {
    function Id005Psbabym1OperationAlphaContentsAndSignature(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1OperationAlphaContentsAndSignature.prototype._read = function() {
      this.contents = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.contents.push(new ContentsEntries(this._io, this, this._root));
        i++;
      }
      this.signature = this._io.readBytes(64);
    }

    return Id005Psbabym1OperationAlphaContentsAndSignature;
  })();

  var Bh2 = Id005Psbabym1OperationProtocolData.Bh2 = (function() {
    function Bh2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh2.prototype._read = function() {
      this.id005Psbabym1BlockHeaderAlphaFullHeader = new Id005Psbabym1BlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh2;
  })();

  var Named = Id005Psbabym1OperationProtocolData.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Bh20 = Id005Psbabym1OperationProtocolData.Bh20 = (function() {
    function Bh20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh20.prototype._read = function() {
      this.lenBh2 = this._io.readU4be();
      if (!(this.lenBh2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh2, this._io, "/types/bh2_0/seq/0");
      }
      this._raw_bh2 = this._io.readBytes(this.lenBh2);
      var _io__raw_bh2 = new KaitaiStream(this._raw_bh2);
      this.bh2 = new Bh2(_io__raw_bh2, this, this._root);
    }

    return Bh20;
  })();

  var Id005Psbabym1InlinedEndorsementContents = Id005Psbabym1OperationProtocolData.Id005Psbabym1InlinedEndorsementContents = (function() {
    function Id005Psbabym1InlinedEndorsementContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1InlinedEndorsementContents.prototype._read = function() {
      this.id005Psbabym1InlinedEndorsementContentsTag = this._io.readU1();
      if (this.id005Psbabym1InlinedEndorsementContentsTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1InlinedEndorsementContentsTag.ENDORSEMENT) {
        this.endorsement = this._io.readS4be();
      }
    }

    return Id005Psbabym1InlinedEndorsementContents;
  })();

  var Delegation = Id005Psbabym1OperationProtocolData.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id005Psbabym1Mutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id005Psbabym1OperationProtocolData.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var ContentsEntries = Id005Psbabym1OperationProtocolData.ContentsEntries = (function() {
    function ContentsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ContentsEntries.prototype._read = function() {
      this.id005Psbabym1OperationAlphaContents = new Id005Psbabym1OperationAlphaContents(this._io, this, this._root);
    }

    return ContentsEntries;
  })();

  var Bh10 = Id005Psbabym1OperationProtocolData.Bh10 = (function() {
    function Bh10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh10.prototype._read = function() {
      this.lenBh1 = this._io.readU4be();
      if (!(this.lenBh1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh1, this._io, "/types/bh1_0/seq/0");
      }
      this._raw_bh1 = this._io.readBytes(this.lenBh1);
      var _io__raw_bh1 = new KaitaiStream(this._raw_bh1);
      this.bh1 = new Bh1(_io__raw_bh1, this, this._root);
    }

    return Bh10;
  })();

  var BytesDynUint30 = Id005Psbabym1OperationProtocolData.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Bh1 = Id005Psbabym1OperationProtocolData.Bh1 = (function() {
    function Bh1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh1.prototype._read = function() {
      this.id005Psbabym1BlockHeaderAlphaFullHeader = new Id005Psbabym1BlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh1;
  })();

  var Op10 = Id005Psbabym1OperationProtocolData.Op10 = (function() {
    function Op10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op10.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_0/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op1(_io__raw_op1, this, this._root);
    }

    return Op10;
  })();

  var Origination = Id005Psbabym1OperationProtocolData.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id005Psbabym1Mutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.balance = new Id005Psbabym1Mutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id005Psbabym1OperationProtocolData.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id005Psbabym1ScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var Id005Psbabym1ScriptedContracts = Id005Psbabym1OperationProtocolData.Id005Psbabym1ScriptedContracts = (function() {
    function Id005Psbabym1ScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1ScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id005Psbabym1ScriptedContracts;
  })();

  var Op2 = Id005Psbabym1OperationProtocolData.Op2 = (function() {
    function Op2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op2.prototype._read = function() {
      this.id005Psbabym1InlinedEndorsement = new Id005Psbabym1InlinedEndorsement(this._io, this, this._root);
    }

    return Op2;
  })();

  var Id005Psbabym1ContractId = Id005Psbabym1OperationProtocolData.Id005Psbabym1ContractId = (function() {
    function Id005Psbabym1ContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1ContractId.prototype._read = function() {
      this.id005Psbabym1ContractIdTag = this._io.readU1();
      if (this.id005Psbabym1ContractIdTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1ContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id005Psbabym1ContractIdTag == Id005Psbabym1OperationProtocolData.Id005Psbabym1ContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id005Psbabym1ContractId;
  })();

  var Id005Psbabym1BlockHeaderAlphaFullHeader = Id005Psbabym1OperationProtocolData.Id005Psbabym1BlockHeaderAlphaFullHeader = (function() {
    function Id005Psbabym1BlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1BlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id005Psbabym1BlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id005Psbabym1BlockHeaderAlphaSignedContents = new Id005Psbabym1BlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id005Psbabym1BlockHeaderAlphaFullHeader;
  })();

  var Id005Psbabym1BlockHeaderAlphaSignedContents = Id005Psbabym1OperationProtocolData.Id005Psbabym1BlockHeaderAlphaSignedContents = (function() {
    function Id005Psbabym1BlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1BlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id005Psbabym1BlockHeaderAlphaUnsignedContents = new Id005Psbabym1BlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id005Psbabym1BlockHeaderAlphaSignedContents;
  })();

  var NChunk = Id005Psbabym1OperationProtocolData.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Named0 = Id005Psbabym1OperationProtocolData.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id005Psbabym1OperationProtocolData.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id005Psbabym1Mutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.amount = new Id005Psbabym1Mutez(this._io, this, this._root);
      this.destination = new Id005Psbabym1ContractId(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id005Psbabym1OperationProtocolData.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Transaction;
  })();

  var Parameters = Id005Psbabym1OperationProtocolData.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id005Psbabym1Entrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var Proposals1 = Id005Psbabym1OperationProtocolData.Proposals1 = (function() {
    function Proposals1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals1.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposals = new Proposals0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Proposals1;
  })();

  var Ballot = Id005Psbabym1OperationProtocolData.Ballot = (function() {
    function Ballot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Ballot.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposal = this._io.readBytes(32);
      this.ballot = this._io.readS1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Ballot;
  })();

  var PublicKeyHash = Id005Psbabym1OperationProtocolData.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id005Psbabym1OperationProtocolData.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id005Psbabym1OperationProtocolData.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id005Psbabym1OperationProtocolData.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Proposals = Id005Psbabym1OperationProtocolData.Proposals = (function() {
    function Proposals(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals.prototype._read = function() {
      this.proposalsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.proposalsEntries.push(new ProposalsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Proposals;
  })();

  var ProposalsEntries = Id005Psbabym1OperationProtocolData.ProposalsEntries = (function() {
    function ProposalsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ProposalsEntries.prototype._read = function() {
      this.protocolHash = this._io.readBytes(32);
    }

    return ProposalsEntries;
  })();

  var Op1 = Id005Psbabym1OperationProtocolData.Op1 = (function() {
    function Op1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op1.prototype._read = function() {
      this.id005Psbabym1InlinedEndorsement = new Id005Psbabym1InlinedEndorsement(this._io, this, this._root);
    }

    return Op1;
  })();

  return Id005Psbabym1OperationProtocolData;
})();
return Id005Psbabym1OperationProtocolData;
}));
