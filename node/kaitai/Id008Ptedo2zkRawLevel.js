// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id008Ptedo2zkRawLevel = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 008-PtEdo2Zk.raw_level
 */

var Id008Ptedo2zkRawLevel = (function() {
  function Id008Ptedo2zkRawLevel(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkRawLevel.prototype._read = function() {
    this.id008Ptedo2zkRawLevel = this._io.readS4be();
  }

  return Id008Ptedo2zkRawLevel;
})();
return Id008Ptedo2zkRawLevel;
}));
