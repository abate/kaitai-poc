// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.SaplingTransactionBindingSig = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: sapling.transaction.binding_sig
 * Description: Binding signature of a transaction
 */

var SaplingTransactionBindingSig = (function() {
  function SaplingTransactionBindingSig(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  SaplingTransactionBindingSig.prototype._read = function() {
    this.saplingTransactionBindingSig = this._io.readBytes(64);
  }

  return SaplingTransactionBindingSig;
})();
return SaplingTransactionBindingSig;
}));
