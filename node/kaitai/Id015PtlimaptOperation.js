// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './OperationShellHeader', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./OperationShellHeader'), require('./BlockHeaderShell'));
  } else {
    root.Id015PtlimaptOperation = factory(root.KaitaiStream, root.OperationShellHeader, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, OperationShellHeader, BlockHeaderShell) {
/**
 * Encoding id: 015-PtLimaPt.operation
 */

var Id015PtlimaptOperation = (function() {
  Id015PtlimaptOperation.RevealProofTag = Object.freeze({
    RAW__DATA__PROOF: 0,

    0: "RAW__DATA__PROOF",
  });

  Id015PtlimaptOperation.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id015PtlimaptOperation.PvmKind = Object.freeze({
    ARITH_PVM_KIND: 0,
    WASM_2_0_0_PVM_KIND: 1,

    0: "ARITH_PVM_KIND",
    1: "WASM_2_0_0_PVM_KIND",
  });

  Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1ExpressionTag = Object.freeze({
    INT: 0,
    STRING: 1,
    SEQUENCE: 2,
    PRIM__NO_ARGS__NO_ANNOTS: 3,
    PRIM__NO_ARGS__SOME_ANNOTS: 4,
    PRIM__1_ARG__NO_ANNOTS: 5,
    PRIM__1_ARG__SOME_ANNOTS: 6,
    PRIM__2_ARGS__NO_ANNOTS: 7,
    PRIM__2_ARGS__SOME_ANNOTS: 8,
    PRIM__GENERIC: 9,
    BYTES: 10,

    0: "INT",
    1: "STRING",
    2: "SEQUENCE",
    3: "PRIM__NO_ARGS__NO_ANNOTS",
    4: "PRIM__NO_ARGS__SOME_ANNOTS",
    5: "PRIM__1_ARG__NO_ANNOTS",
    6: "PRIM__1_ARG__SOME_ANNOTS",
    7: "PRIM__2_ARGS__NO_ANNOTS",
    8: "PRIM__2_ARGS__SOME_ANNOTS",
    9: "PRIM__GENERIC",
    10: "BYTES",
  });

  Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag = Object.freeze({
    SEED_NONCE_REVELATION: 1,
    DOUBLE_ENDORSEMENT_EVIDENCE: 2,
    DOUBLE_BAKING_EVIDENCE: 3,
    ACTIVATE_ACCOUNT: 4,
    PROPOSALS: 5,
    BALLOT: 6,
    DOUBLE_PREENDORSEMENT_EVIDENCE: 7,
    VDF_REVELATION: 8,
    DRAIN_DELEGATE: 9,
    FAILING_NOOP: 17,
    PREENDORSEMENT: 20,
    ENDORSEMENT: 21,
    DAL_SLOT_AVAILABILITY: 22,
    REVEAL: 107,
    TRANSACTION: 108,
    ORIGINATION: 109,
    DELEGATION: 110,
    REGISTER_GLOBAL_CONSTANT: 111,
    SET_DEPOSITS_LIMIT: 112,
    INCREASE_PAID_STORAGE: 113,
    UPDATE_CONSENSUS_KEY: 114,
    TX_ROLLUP_ORIGINATION: 150,
    TX_ROLLUP_SUBMIT_BATCH: 151,
    TX_ROLLUP_COMMIT: 152,
    TX_ROLLUP_RETURN_BOND: 153,
    TX_ROLLUP_FINALIZE_COMMITMENT: 154,
    TX_ROLLUP_REMOVE_COMMITMENT: 155,
    TX_ROLLUP_REJECTION: 156,
    TX_ROLLUP_DISPATCH_TICKETS: 157,
    TRANSFER_TICKET: 158,
    SC_ROLLUP_ORIGINATE: 200,
    SC_ROLLUP_ADD_MESSAGES: 201,
    SC_ROLLUP_CEMENT: 202,
    SC_ROLLUP_PUBLISH: 203,
    SC_ROLLUP_REFUTE: 204,
    SC_ROLLUP_TIMEOUT: 205,
    SC_ROLLUP_EXECUTE_OUTBOX_MESSAGE: 206,
    SC_ROLLUP_RECOVER_BOND: 207,
    SC_ROLLUP_DAL_SLOT_SUBSCRIBE: 208,
    DAL_PUBLISH_SLOT_HEADER: 230,
    ZK_ROLLUP_ORIGINATION: 250,
    ZK_ROLLUP_PUBLISH: 251,

    1: "SEED_NONCE_REVELATION",
    2: "DOUBLE_ENDORSEMENT_EVIDENCE",
    3: "DOUBLE_BAKING_EVIDENCE",
    4: "ACTIVATE_ACCOUNT",
    5: "PROPOSALS",
    6: "BALLOT",
    7: "DOUBLE_PREENDORSEMENT_EVIDENCE",
    8: "VDF_REVELATION",
    9: "DRAIN_DELEGATE",
    17: "FAILING_NOOP",
    20: "PREENDORSEMENT",
    21: "ENDORSEMENT",
    22: "DAL_SLOT_AVAILABILITY",
    107: "REVEAL",
    108: "TRANSACTION",
    109: "ORIGINATION",
    110: "DELEGATION",
    111: "REGISTER_GLOBAL_CONSTANT",
    112: "SET_DEPOSITS_LIMIT",
    113: "INCREASE_PAID_STORAGE",
    114: "UPDATE_CONSENSUS_KEY",
    150: "TX_ROLLUP_ORIGINATION",
    151: "TX_ROLLUP_SUBMIT_BATCH",
    152: "TX_ROLLUP_COMMIT",
    153: "TX_ROLLUP_RETURN_BOND",
    154: "TX_ROLLUP_FINALIZE_COMMITMENT",
    155: "TX_ROLLUP_REMOVE_COMMITMENT",
    156: "TX_ROLLUP_REJECTION",
    157: "TX_ROLLUP_DISPATCH_TICKETS",
    158: "TRANSFER_TICKET",
    200: "SC_ROLLUP_ORIGINATE",
    201: "SC_ROLLUP_ADD_MESSAGES",
    202: "SC_ROLLUP_CEMENT",
    203: "SC_ROLLUP_PUBLISH",
    204: "SC_ROLLUP_REFUTE",
    205: "SC_ROLLUP_TIMEOUT",
    206: "SC_ROLLUP_EXECUTE_OUTBOX_MESSAGE",
    207: "SC_ROLLUP_RECOVER_BOND",
    208: "SC_ROLLUP_DAL_SLOT_SUBSCRIBE",
    230: "DAL_PUBLISH_SLOT_HEADER",
    250: "ZK_ROLLUP_ORIGINATION",
    251: "ZK_ROLLUP_PUBLISH",
  });

  Id015PtlimaptOperation.PvmStepTag = Object.freeze({
    ARITHMETIC__PVM__WITH__PROOF: 0,
    WASM__2__0__0__PVM__WITH__PROOF: 1,
    UNENCODABLE: 255,

    0: "ARITHMETIC__PVM__WITH__PROOF",
    1: "WASM__2__0__0__PVM__WITH__PROOF",
    255: "UNENCODABLE",
  });

  Id015PtlimaptOperation.Id015PtlimaptInlinedEndorsementMempoolContentsTag = Object.freeze({
    ENDORSEMENT: 21,

    21: "ENDORSEMENT",
  });

  Id015PtlimaptOperation.ProofTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
  });

  Id015PtlimaptOperation.TreeEncodingTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__7: 7,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,
    CASE__11: 11,
    CASE__12: 12,
    CASE__13: 13,
    CASE__14: 14,
    CASE__15: 15,
    CASE__16: 16,
    CASE__17: 17,
    CASE__18: 18,
    CASE__19: 19,
    CASE__20: 20,
    CASE__21: 21,
    CASE__22: 22,
    CASE__23: 23,
    CASE__24: 24,
    CASE__25: 25,
    CASE__26: 26,
    CASE__27: 27,
    CASE__28: 28,
    CASE__29: 29,
    CASE__30: 30,
    CASE__31: 31,
    CASE__32: 32,
    CASE__33: 33,
    CASE__34: 34,
    CASE__35: 35,
    CASE__36: 36,
    CASE__37: 37,
    CASE__38: 38,
    CASE__39: 39,
    CASE__40: 40,
    CASE__41: 41,
    CASE__42: 42,
    CASE__43: 43,
    CASE__44: 44,
    CASE__45: 45,
    CASE__46: 46,
    CASE__47: 47,
    CASE__48: 48,
    CASE__49: 49,
    CASE__50: 50,
    CASE__51: 51,
    CASE__52: 52,
    CASE__53: 53,
    CASE__54: 54,
    CASE__55: 55,
    CASE__56: 56,
    CASE__57: 57,
    CASE__58: 58,
    CASE__59: 59,
    CASE__60: 60,
    CASE__61: 61,
    CASE__62: 62,
    CASE__63: 63,
    CASE__64: 64,
    CASE__65: 65,
    CASE__66: 66,
    CASE__67: 67,
    CASE__128: 128,
    CASE__129: 129,
    CASE__130: 130,
    CASE__131: 131,
    CASE__132: 132,
    CASE__133: 133,
    CASE__134: 134,
    CASE__135: 135,
    CASE__136: 136,
    CASE__137: 137,
    CASE__138: 138,
    CASE__139: 139,
    CASE__140: 140,
    CASE__141: 141,
    CASE__142: 142,
    CASE__143: 143,
    CASE__144: 144,
    CASE__145: 145,
    CASE__146: 146,
    CASE__147: 147,
    CASE__148: 148,
    CASE__149: 149,
    CASE__150: 150,
    CASE__151: 151,
    CASE__152: 152,
    CASE__153: 153,
    CASE__154: 154,
    CASE__155: 155,
    CASE__156: 156,
    CASE__157: 157,
    CASE__158: 158,
    CASE__159: 159,
    CASE__160: 160,
    CASE__161: 161,
    CASE__162: 162,
    CASE__163: 163,
    CASE__164: 164,
    CASE__165: 165,
    CASE__166: 166,
    CASE__167: 167,
    CASE__168: 168,
    CASE__169: 169,
    CASE__170: 170,
    CASE__171: 171,
    CASE__172: 172,
    CASE__173: 173,
    CASE__174: 174,
    CASE__175: 175,
    CASE__176: 176,
    CASE__177: 177,
    CASE__178: 178,
    CASE__179: 179,
    CASE__180: 180,
    CASE__181: 181,
    CASE__182: 182,
    CASE__183: 183,
    CASE__184: 184,
    CASE__185: 185,
    CASE__186: 186,
    CASE__187: 187,
    CASE__188: 188,
    CASE__189: 189,
    CASE__190: 190,
    CASE__191: 191,
    CASE__192: 192,
    CASE__193: 193,
    CASE__195: 195,
    CASE__200: 200,
    CASE__208: 208,
    CASE__216: 216,
    CASE__217: 217,
    CASE__218: 218,
    CASE__219: 219,
    CASE__224: 224,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    7: "CASE__7",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
    11: "CASE__11",
    12: "CASE__12",
    13: "CASE__13",
    14: "CASE__14",
    15: "CASE__15",
    16: "CASE__16",
    17: "CASE__17",
    18: "CASE__18",
    19: "CASE__19",
    20: "CASE__20",
    21: "CASE__21",
    22: "CASE__22",
    23: "CASE__23",
    24: "CASE__24",
    25: "CASE__25",
    26: "CASE__26",
    27: "CASE__27",
    28: "CASE__28",
    29: "CASE__29",
    30: "CASE__30",
    31: "CASE__31",
    32: "CASE__32",
    33: "CASE__33",
    34: "CASE__34",
    35: "CASE__35",
    36: "CASE__36",
    37: "CASE__37",
    38: "CASE__38",
    39: "CASE__39",
    40: "CASE__40",
    41: "CASE__41",
    42: "CASE__42",
    43: "CASE__43",
    44: "CASE__44",
    45: "CASE__45",
    46: "CASE__46",
    47: "CASE__47",
    48: "CASE__48",
    49: "CASE__49",
    50: "CASE__50",
    51: "CASE__51",
    52: "CASE__52",
    53: "CASE__53",
    54: "CASE__54",
    55: "CASE__55",
    56: "CASE__56",
    57: "CASE__57",
    58: "CASE__58",
    59: "CASE__59",
    60: "CASE__60",
    61: "CASE__61",
    62: "CASE__62",
    63: "CASE__63",
    64: "CASE__64",
    65: "CASE__65",
    66: "CASE__66",
    67: "CASE__67",
    128: "CASE__128",
    129: "CASE__129",
    130: "CASE__130",
    131: "CASE__131",
    132: "CASE__132",
    133: "CASE__133",
    134: "CASE__134",
    135: "CASE__135",
    136: "CASE__136",
    137: "CASE__137",
    138: "CASE__138",
    139: "CASE__139",
    140: "CASE__140",
    141: "CASE__141",
    142: "CASE__142",
    143: "CASE__143",
    144: "CASE__144",
    145: "CASE__145",
    146: "CASE__146",
    147: "CASE__147",
    148: "CASE__148",
    149: "CASE__149",
    150: "CASE__150",
    151: "CASE__151",
    152: "CASE__152",
    153: "CASE__153",
    154: "CASE__154",
    155: "CASE__155",
    156: "CASE__156",
    157: "CASE__157",
    158: "CASE__158",
    159: "CASE__159",
    160: "CASE__160",
    161: "CASE__161",
    162: "CASE__162",
    163: "CASE__163",
    164: "CASE__164",
    165: "CASE__165",
    166: "CASE__166",
    167: "CASE__167",
    168: "CASE__168",
    169: "CASE__169",
    170: "CASE__170",
    171: "CASE__171",
    172: "CASE__172",
    173: "CASE__173",
    174: "CASE__174",
    175: "CASE__175",
    176: "CASE__176",
    177: "CASE__177",
    178: "CASE__178",
    179: "CASE__179",
    180: "CASE__180",
    181: "CASE__181",
    182: "CASE__182",
    183: "CASE__183",
    184: "CASE__184",
    185: "CASE__185",
    186: "CASE__186",
    187: "CASE__187",
    188: "CASE__188",
    189: "CASE__189",
    190: "CASE__190",
    191: "CASE__191",
    192: "CASE__192",
    193: "CASE__193",
    195: "CASE__195",
    200: "CASE__200",
    208: "CASE__208",
    216: "CASE__216",
    217: "CASE__217",
    218: "CASE__218",
    219: "CASE__219",
    224: "CASE__224",
  });

  Id015PtlimaptOperation.OpEltField1Tag = Object.freeze({
    NONE: 0,
    SOME: 1,

    0: "NONE",
    1: "SOME",
  });

  Id015PtlimaptOperation.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id015PtlimaptOperation.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id015PtlimaptOperation.Id015PtlimaptEntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    DEPOSIT: 5,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    5: "DEPOSIT",
    255: "NAMED",
  });

  Id015PtlimaptOperation.Id015PtlimaptInlinedPreendorsementContentsTag = Object.freeze({
    PREENDORSEMENT: 20,

    20: "PREENDORSEMENT",
  });

  Id015PtlimaptOperation.PredecessorTag = Object.freeze({
    NONE: 0,
    SOME: 1,

    0: "NONE",
    1: "SOME",
  });

  Id015PtlimaptOperation.Id015PtlimaptContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id015PtlimaptOperation.AmountTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
  });

  Id015PtlimaptOperation.Id015PtlimaptMichelsonV1Primitives = Object.freeze({
    PARAMETER: 0,
    STORAGE: 1,
    CODE: 2,
    FALSE: 3,
    ELT: 4,
    LEFT: 5,
    NONE_0: 6,
    PAIR_1: 7,
    RIGHT: 8,
    SOME_0: 9,
    TRUE: 10,
    UNIT_0: 11,
    PACK: 12,
    UNPACK: 13,
    BLAKE2B: 14,
    SHA256: 15,
    SHA512: 16,
    ABS: 17,
    ADD: 18,
    AMOUNT: 19,
    AND: 20,
    BALANCE: 21,
    CAR: 22,
    CDR: 23,
    CHECK_SIGNATURE: 24,
    COMPARE: 25,
    CONCAT: 26,
    CONS: 27,
    CREATE_ACCOUNT: 28,
    CREATE_CONTRACT: 29,
    IMPLICIT_ACCOUNT: 30,
    DIP: 31,
    DROP: 32,
    DUP: 33,
    EDIV: 34,
    EMPTY_MAP: 35,
    EMPTY_SET: 36,
    EQ: 37,
    EXEC: 38,
    FAILWITH: 39,
    GE: 40,
    GET: 41,
    GT: 42,
    HASH_KEY: 43,
    IF: 44,
    IF_CONS: 45,
    IF_LEFT: 46,
    IF_NONE: 47,
    INT_0: 48,
    LAMBDA_0: 49,
    LE: 50,
    LEFT_0: 51,
    LOOP: 52,
    LSL: 53,
    LSR: 54,
    LT: 55,
    MAP_0: 56,
    MEM: 57,
    MUL: 58,
    NEG: 59,
    NEQ: 60,
    NIL: 61,
    NONE: 62,
    NOT: 63,
    NOW: 64,
    OR_0: 65,
    PAIR_0: 66,
    PUSH: 67,
    RIGHT_0: 68,
    SIZE: 69,
    SOME: 70,
    SOURCE: 71,
    SENDER: 72,
    SELF: 73,
    STEPS_TO_QUOTA: 74,
    SUB: 75,
    SWAP: 76,
    TRANSFER_TOKENS: 77,
    SET_DELEGATE: 78,
    UNIT_1: 79,
    UPDATE: 80,
    XOR: 81,
    ITER: 82,
    LOOP_LEFT: 83,
    ADDRESS_0: 84,
    CONTRACT_0: 85,
    ISNAT: 86,
    CAST: 87,
    RENAME: 88,
    BOOL: 89,
    CONTRACT: 90,
    INT: 91,
    KEY: 92,
    KEY_HASH: 93,
    LAMBDA: 94,
    LIST: 95,
    MAP: 96,
    BIG_MAP: 97,
    NAT: 98,
    OPTION: 99,
    OR: 100,
    PAIR: 101,
    SET: 102,
    SIGNATURE: 103,
    STRING: 104,
    BYTES: 105,
    MUTEZ: 106,
    TIMESTAMP: 107,
    UNIT: 108,
    OPERATION: 109,
    ADDRESS: 110,
    SLICE: 111,
    DIG: 112,
    DUG: 113,
    EMPTY_BIG_MAP: 114,
    APPLY: 115,
    CHAIN_ID: 116,
    CHAIN_ID_0: 117,
    LEVEL: 118,
    SELF_ADDRESS: 119,
    NEVER: 120,
    NEVER_0: 121,
    UNPAIR: 122,
    VOTING_POWER: 123,
    TOTAL_VOTING_POWER: 124,
    KECCAK: 125,
    SHA3: 126,
    PAIRING_CHECK: 127,
    BLS12_381_G1: 128,
    BLS12_381_G2: 129,
    BLS12_381_FR: 130,
    SAPLING_STATE: 131,
    SAPLING_TRANSACTION_DEPRECATED: 132,
    SAPLING_EMPTY_STATE: 133,
    SAPLING_VERIFY_UPDATE: 134,
    TICKET: 135,
    TICKET_DEPRECATED: 136,
    READ_TICKET: 137,
    SPLIT_TICKET: 138,
    JOIN_TICKETS: 139,
    GET_AND_UPDATE: 140,
    CHEST: 141,
    CHEST_KEY: 142,
    OPEN_CHEST: 143,
    VIEW_0: 144,
    VIEW: 145,
    CONSTANT: 146,
    SUB_MUTEZ: 147,
    TX_ROLLUP_L2_ADDRESS: 148,
    MIN_BLOCK_TIME: 149,
    SAPLING_TRANSACTION: 150,
    EMIT: 151,
    LAMBDA_REC: 152,
    LAMBDA_REC_0: 153,
    TICKET_0: 154,

    0: "PARAMETER",
    1: "STORAGE",
    2: "CODE",
    3: "FALSE",
    4: "ELT",
    5: "LEFT",
    6: "NONE_0",
    7: "PAIR_1",
    8: "RIGHT",
    9: "SOME_0",
    10: "TRUE",
    11: "UNIT_0",
    12: "PACK",
    13: "UNPACK",
    14: "BLAKE2B",
    15: "SHA256",
    16: "SHA512",
    17: "ABS",
    18: "ADD",
    19: "AMOUNT",
    20: "AND",
    21: "BALANCE",
    22: "CAR",
    23: "CDR",
    24: "CHECK_SIGNATURE",
    25: "COMPARE",
    26: "CONCAT",
    27: "CONS",
    28: "CREATE_ACCOUNT",
    29: "CREATE_CONTRACT",
    30: "IMPLICIT_ACCOUNT",
    31: "DIP",
    32: "DROP",
    33: "DUP",
    34: "EDIV",
    35: "EMPTY_MAP",
    36: "EMPTY_SET",
    37: "EQ",
    38: "EXEC",
    39: "FAILWITH",
    40: "GE",
    41: "GET",
    42: "GT",
    43: "HASH_KEY",
    44: "IF",
    45: "IF_CONS",
    46: "IF_LEFT",
    47: "IF_NONE",
    48: "INT_0",
    49: "LAMBDA_0",
    50: "LE",
    51: "LEFT_0",
    52: "LOOP",
    53: "LSL",
    54: "LSR",
    55: "LT",
    56: "MAP_0",
    57: "MEM",
    58: "MUL",
    59: "NEG",
    60: "NEQ",
    61: "NIL",
    62: "NONE",
    63: "NOT",
    64: "NOW",
    65: "OR_0",
    66: "PAIR_0",
    67: "PUSH",
    68: "RIGHT_0",
    69: "SIZE",
    70: "SOME",
    71: "SOURCE",
    72: "SENDER",
    73: "SELF",
    74: "STEPS_TO_QUOTA",
    75: "SUB",
    76: "SWAP",
    77: "TRANSFER_TOKENS",
    78: "SET_DELEGATE",
    79: "UNIT_1",
    80: "UPDATE",
    81: "XOR",
    82: "ITER",
    83: "LOOP_LEFT",
    84: "ADDRESS_0",
    85: "CONTRACT_0",
    86: "ISNAT",
    87: "CAST",
    88: "RENAME",
    89: "BOOL",
    90: "CONTRACT",
    91: "INT",
    92: "KEY",
    93: "KEY_HASH",
    94: "LAMBDA",
    95: "LIST",
    96: "MAP",
    97: "BIG_MAP",
    98: "NAT",
    99: "OPTION",
    100: "OR",
    101: "PAIR",
    102: "SET",
    103: "SIGNATURE",
    104: "STRING",
    105: "BYTES",
    106: "MUTEZ",
    107: "TIMESTAMP",
    108: "UNIT",
    109: "OPERATION",
    110: "ADDRESS",
    111: "SLICE",
    112: "DIG",
    113: "DUG",
    114: "EMPTY_BIG_MAP",
    115: "APPLY",
    116: "CHAIN_ID",
    117: "CHAIN_ID_0",
    118: "LEVEL",
    119: "SELF_ADDRESS",
    120: "NEVER",
    121: "NEVER_0",
    122: "UNPAIR",
    123: "VOTING_POWER",
    124: "TOTAL_VOTING_POWER",
    125: "KECCAK",
    126: "SHA3",
    127: "PAIRING_CHECK",
    128: "BLS12_381_G1",
    129: "BLS12_381_G2",
    130: "BLS12_381_FR",
    131: "SAPLING_STATE",
    132: "SAPLING_TRANSACTION_DEPRECATED",
    133: "SAPLING_EMPTY_STATE",
    134: "SAPLING_VERIFY_UPDATE",
    135: "TICKET",
    136: "TICKET_DEPRECATED",
    137: "READ_TICKET",
    138: "SPLIT_TICKET",
    139: "JOIN_TICKETS",
    140: "GET_AND_UPDATE",
    141: "CHEST",
    142: "CHEST_KEY",
    143: "OPEN_CHEST",
    144: "VIEW_0",
    145: "VIEW",
    146: "CONSTANT",
    147: "SUB_MUTEZ",
    148: "TX_ROLLUP_L2_ADDRESS",
    149: "MIN_BLOCK_TIME",
    150: "SAPLING_TRANSACTION",
    151: "EMIT",
    152: "LAMBDA_REC",
    153: "LAMBDA_REC_0",
    154: "TICKET_0",
  });

  Id015PtlimaptOperation.InodeTreeTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__3: 3,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__7: 7,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,
    CASE__11: 11,
    CASE__12: 12,
    CASE__13: 13,
    CASE__14: 14,
    CASE__15: 15,
    CASE__16: 16,
    CASE__17: 17,
    CASE__18: 18,
    CASE__19: 19,
    CASE__20: 20,
    CASE__21: 21,
    CASE__22: 22,
    CASE__23: 23,
    CASE__24: 24,
    CASE__25: 25,
    CASE__26: 26,
    CASE__27: 27,
    CASE__28: 28,
    CASE__29: 29,
    CASE__30: 30,
    CASE__31: 31,
    CASE__32: 32,
    CASE__33: 33,
    CASE__34: 34,
    CASE__35: 35,
    CASE__36: 36,
    CASE__37: 37,
    CASE__38: 38,
    CASE__39: 39,
    CASE__40: 40,
    CASE__41: 41,
    CASE__42: 42,
    CASE__43: 43,
    CASE__44: 44,
    CASE__45: 45,
    CASE__46: 46,
    CASE__47: 47,
    CASE__48: 48,
    CASE__49: 49,
    CASE__50: 50,
    CASE__51: 51,
    CASE__52: 52,
    CASE__53: 53,
    CASE__54: 54,
    CASE__55: 55,
    CASE__56: 56,
    CASE__57: 57,
    CASE__58: 58,
    CASE__59: 59,
    CASE__60: 60,
    CASE__61: 61,
    CASE__62: 62,
    CASE__63: 63,
    CASE__64: 64,
    CASE__65: 65,
    CASE__66: 66,
    CASE__67: 67,
    CASE__128: 128,
    CASE__129: 129,
    CASE__130: 130,
    CASE__131: 131,
    CASE__132: 132,
    CASE__133: 133,
    CASE__134: 134,
    CASE__135: 135,
    CASE__136: 136,
    CASE__137: 137,
    CASE__138: 138,
    CASE__139: 139,
    CASE__140: 140,
    CASE__141: 141,
    CASE__142: 142,
    CASE__143: 143,
    CASE__144: 144,
    CASE__145: 145,
    CASE__146: 146,
    CASE__147: 147,
    CASE__148: 148,
    CASE__149: 149,
    CASE__150: 150,
    CASE__151: 151,
    CASE__152: 152,
    CASE__153: 153,
    CASE__154: 154,
    CASE__155: 155,
    CASE__156: 156,
    CASE__157: 157,
    CASE__158: 158,
    CASE__159: 159,
    CASE__160: 160,
    CASE__161: 161,
    CASE__162: 162,
    CASE__163: 163,
    CASE__164: 164,
    CASE__165: 165,
    CASE__166: 166,
    CASE__167: 167,
    CASE__168: 168,
    CASE__169: 169,
    CASE__170: 170,
    CASE__171: 171,
    CASE__172: 172,
    CASE__173: 173,
    CASE__174: 174,
    CASE__175: 175,
    CASE__176: 176,
    CASE__177: 177,
    CASE__178: 178,
    CASE__179: 179,
    CASE__180: 180,
    CASE__181: 181,
    CASE__182: 182,
    CASE__183: 183,
    CASE__184: 184,
    CASE__185: 185,
    CASE__186: 186,
    CASE__187: 187,
    CASE__188: 188,
    CASE__189: 189,
    CASE__190: 190,
    CASE__191: 191,
    CASE__192: 192,
    CASE__208: 208,
    CASE__209: 209,
    CASE__210: 210,
    CASE__211: 211,
    CASE__224: 224,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    3: "CASE__3",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    7: "CASE__7",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
    11: "CASE__11",
    12: "CASE__12",
    13: "CASE__13",
    14: "CASE__14",
    15: "CASE__15",
    16: "CASE__16",
    17: "CASE__17",
    18: "CASE__18",
    19: "CASE__19",
    20: "CASE__20",
    21: "CASE__21",
    22: "CASE__22",
    23: "CASE__23",
    24: "CASE__24",
    25: "CASE__25",
    26: "CASE__26",
    27: "CASE__27",
    28: "CASE__28",
    29: "CASE__29",
    30: "CASE__30",
    31: "CASE__31",
    32: "CASE__32",
    33: "CASE__33",
    34: "CASE__34",
    35: "CASE__35",
    36: "CASE__36",
    37: "CASE__37",
    38: "CASE__38",
    39: "CASE__39",
    40: "CASE__40",
    41: "CASE__41",
    42: "CASE__42",
    43: "CASE__43",
    44: "CASE__44",
    45: "CASE__45",
    46: "CASE__46",
    47: "CASE__47",
    48: "CASE__48",
    49: "CASE__49",
    50: "CASE__50",
    51: "CASE__51",
    52: "CASE__52",
    53: "CASE__53",
    54: "CASE__54",
    55: "CASE__55",
    56: "CASE__56",
    57: "CASE__57",
    58: "CASE__58",
    59: "CASE__59",
    60: "CASE__60",
    61: "CASE__61",
    62: "CASE__62",
    63: "CASE__63",
    64: "CASE__64",
    65: "CASE__65",
    66: "CASE__66",
    67: "CASE__67",
    128: "CASE__128",
    129: "CASE__129",
    130: "CASE__130",
    131: "CASE__131",
    132: "CASE__132",
    133: "CASE__133",
    134: "CASE__134",
    135: "CASE__135",
    136: "CASE__136",
    137: "CASE__137",
    138: "CASE__138",
    139: "CASE__139",
    140: "CASE__140",
    141: "CASE__141",
    142: "CASE__142",
    143: "CASE__143",
    144: "CASE__144",
    145: "CASE__145",
    146: "CASE__146",
    147: "CASE__147",
    148: "CASE__148",
    149: "CASE__149",
    150: "CASE__150",
    151: "CASE__151",
    152: "CASE__152",
    153: "CASE__153",
    154: "CASE__154",
    155: "CASE__155",
    156: "CASE__156",
    157: "CASE__157",
    158: "CASE__158",
    159: "CASE__159",
    160: "CASE__160",
    161: "CASE__161",
    162: "CASE__162",
    163: "CASE__163",
    164: "CASE__164",
    165: "CASE__165",
    166: "CASE__166",
    167: "CASE__167",
    168: "CASE__168",
    169: "CASE__169",
    170: "CASE__170",
    171: "CASE__171",
    172: "CASE__172",
    173: "CASE__173",
    174: "CASE__174",
    175: "CASE__175",
    176: "CASE__176",
    177: "CASE__177",
    178: "CASE__178",
    179: "CASE__179",
    180: "CASE__180",
    181: "CASE__181",
    182: "CASE__182",
    183: "CASE__183",
    184: "CASE__184",
    185: "CASE__185",
    186: "CASE__186",
    187: "CASE__187",
    188: "CASE__188",
    189: "CASE__189",
    190: "CASE__190",
    191: "CASE__191",
    192: "CASE__192",
    208: "CASE__208",
    209: "CASE__209",
    210: "CASE__210",
    211: "CASE__211",
    224: "CASE__224",
  });

  Id015PtlimaptOperation.StepTag = Object.freeze({
    DISSECTION: 0,
    PROOF: 1,

    0: "DISSECTION",
    1: "PROOF",
  });

  Id015PtlimaptOperation.MessageTag = Object.freeze({
    BATCH: 0,
    DEPOSIT: 1,

    0: "BATCH",
    1: "DEPOSIT",
  });

  Id015PtlimaptOperation.Id015PtlimaptContractIdOriginatedTag = Object.freeze({
    ORIGINATED: 1,

    1: "ORIGINATED",
  });

  Id015PtlimaptOperation.InputProofTag = Object.freeze({
    INBOX__PROOF: 0,
    REVEAL__PROOF: 1,

    0: "INBOX__PROOF",
    1: "REVEAL__PROOF",
  });

  function Id015PtlimaptOperation(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id015PtlimaptOperation.prototype._read = function() {
    this.id015PtlimaptOperation = new OperationShellHeader(this._io, this, null);
    this.id015PtlimaptOperationAlphaContentsAndSignature = new Id015PtlimaptOperationAlphaContentsAndSignature(this._io, this, this._root);
  }

  var Case131EltField00 = Id015PtlimaptOperation.Case131EltField00 = (function() {
    function Case131EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131EltField00.prototype._read = function() {
      this.lenCase131EltField0 = this._io.readU1();
      if (!(this.lenCase131EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase131EltField0, this._io, "/types/case__131_elt_field0_0/seq/0");
      }
      this._raw_case131EltField0 = this._io.readBytes(this.lenCase131EltField0);
      var _io__raw_case131EltField0 = new KaitaiStream(this._raw_case131EltField0);
      this.case131EltField0 = new Case131EltField0(_io__raw_case131EltField0, this, this._root);
    }

    return Case131EltField00;
  })();

  var Case24 = Id015PtlimaptOperation.Case24 = (function() {
    function Case24(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case24.prototype._read = function() {
      this.case24Field0 = this._io.readU1();
      this.case24Field1 = [];
      for (var i = 0; i < 6; i++) {
        this.case24Field1.push(new Case24Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__24_field1_entries
     */

    return Case24;
  })();

  var Case189Entries = Id015PtlimaptOperation.Case189Entries = (function() {
    function Case189Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case189Entries.prototype._read = function() {
      this.case189EltField0 = new Case189EltField00(this._io, this, this._root);
      this.case189EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case189Entries;
  })();

  var Case211Field1 = Id015PtlimaptOperation.Case211Field1 = (function() {
    function Case211Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case211Field1.prototype._read = function() {
      this.case211Field1 = this._io.readBytesFull();
    }

    return Case211Field1;
  })();

  var Case23Field1Entries = Id015PtlimaptOperation.Case23Field1Entries = (function() {
    function Case23Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case23Field1Entries.prototype._read = function() {
      this.case23Field1EltField0 = this._io.readU1();
      this.case23Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case23Field1Entries;
  })();

  var Case138EltField0 = Id015PtlimaptOperation.Case138EltField0 = (function() {
    function Case138EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case138EltField0.prototype._read = function() {
      this.case138EltField0 = this._io.readBytesFull();
    }

    return Case138EltField0;
  })();

  var Id015PtlimaptInlinedPreendorsement = Id015PtlimaptOperation.Id015PtlimaptInlinedPreendorsement = (function() {
    function Id015PtlimaptInlinedPreendorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptInlinedPreendorsement.prototype._read = function() {
      this.id015PtlimaptInlinedPreendorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id015PtlimaptInlinedPreendorsementContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id015PtlimaptOperation.Bool.TRUE) {
        this.signature = this._io.readBytes(64);
      }
    }

    return Id015PtlimaptInlinedPreendorsement;
  })();

  var Op20 = Id015PtlimaptOperation.Op20 = (function() {
    function Op20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op20.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_0/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op2(_io__raw_op2, this, this._root);
    }

    return Op20;
  })();

  var Case177Entries = Id015PtlimaptOperation.Case177Entries = (function() {
    function Case177Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case177Entries.prototype._read = function() {
      this.case177EltField0 = new Case177EltField00(this._io, this, this._root);
      this.case177EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case177Entries;
  })();

  var Case1 = Id015PtlimaptOperation.Case1 = (function() {
    function Case1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1.prototype._read = function() {
      this.case1Field0 = this._io.readS2be();
      this.case1Field1 = this._io.readBytes(32);
      this.case1Field2 = this._io.readBytes(32);
      this.case1Field3 = new TreeEncoding(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case1;
  })();

  var Case42Field1Entries = Id015PtlimaptOperation.Case42Field1Entries = (function() {
    function Case42Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case42Field1Entries.prototype._read = function() {
      this.case42Field1EltField0 = this._io.readU1();
      this.case42Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case42Field1Entries;
  })();

  var Case163Entries = Id015PtlimaptOperation.Case163Entries = (function() {
    function Case163Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case163Entries.prototype._read = function() {
      this.case163EltField0 = new Case163EltField00(this._io, this, this._root);
      this.case163EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case163Entries;
  })();

  var Case192 = Id015PtlimaptOperation.Case192 = (function() {
    function Case192(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case192.prototype._read = function() {
      this.case192 = this._io.readBytesFull();
    }

    return Case192;
  })();

  var Case183Entries = Id015PtlimaptOperation.Case183Entries = (function() {
    function Case183Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case183Entries.prototype._read = function() {
      this.case183EltField0 = new Case183EltField00(this._io, this, this._root);
      this.case183EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case183Entries;
  })();

  var Case55Field1Entries = Id015PtlimaptOperation.Case55Field1Entries = (function() {
    function Case55Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case55Field1Entries.prototype._read = function() {
      this.case55Field1EltField0 = this._io.readU1();
      this.case55Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case55Field1Entries;
  })();

  var Messages = Id015PtlimaptOperation.Messages = (function() {
    function Messages(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Messages.prototype._read = function() {
      this.messagesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messagesEntries.push(new MessagesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Messages;
  })();

  var Case65Field1Entries = Id015PtlimaptOperation.Case65Field1Entries = (function() {
    function Case65Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case65Field1Entries.prototype._read = function() {
      this.case65Field1Elt = new InodeTree(this._io, this, this._root);
    }

    return Case65Field1Entries;
  })();

  var Case211 = Id015PtlimaptOperation.Case211 = (function() {
    function Case211(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case211.prototype._read = function() {
      this.case211Field0 = this._io.readS8be();
      this.case211Field1 = new Case211Field10(this._io, this, this._root);
      this.case211Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case211;
  })();

  var Case43 = Id015PtlimaptOperation.Case43 = (function() {
    function Case43(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case43.prototype._read = function() {
      this.case43Field0 = this._io.readS8be();
      this.case43Field1 = [];
      for (var i = 0; i < 10; i++) {
        this.case43Field1.push(new Case43Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__43_field1_entries
     */

    return Case43;
  })();

  var Case54 = Id015PtlimaptOperation.Case54 = (function() {
    function Case54(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case54.prototype._read = function() {
      this.case54Field0 = this._io.readS4be();
      this.case54Field1 = [];
      for (var i = 0; i < 13; i++) {
        this.case54Field1.push(new Case54Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__54_field1_entries
     */

    return Case54;
  })();

  var Case138EltField00 = Id015PtlimaptOperation.Case138EltField00 = (function() {
    function Case138EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case138EltField00.prototype._read = function() {
      this.lenCase138EltField0 = this._io.readU1();
      if (!(this.lenCase138EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase138EltField0, this._io, "/types/case__138_elt_field0_0/seq/0");
      }
      this._raw_case138EltField0 = this._io.readBytes(this.lenCase138EltField0);
      var _io__raw_case138EltField0 = new KaitaiStream(this._raw_case138EltField0);
      this.case138EltField0 = new Case138EltField0(_io__raw_case138EltField0, this, this._root);
    }

    return Case138EltField00;
  })();

  var Case134EltField00 = Id015PtlimaptOperation.Case134EltField00 = (function() {
    function Case134EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case134EltField00.prototype._read = function() {
      this.lenCase134EltField0 = this._io.readU1();
      if (!(this.lenCase134EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase134EltField0, this._io, "/types/case__134_elt_field0_0/seq/0");
      }
      this._raw_case134EltField0 = this._io.readBytes(this.lenCase134EltField0);
      var _io__raw_case134EltField0 = new KaitaiStream(this._raw_case134EltField0);
      this.case134EltField0 = new Case134EltField0(_io__raw_case134EltField0, this, this._root);
    }

    return Case134EltField00;
  })();

  var Case32 = Id015PtlimaptOperation.Case32 = (function() {
    function Case32(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case32.prototype._read = function() {
      this.case32Field0 = this._io.readU1();
      this.case32Field1 = [];
      for (var i = 0; i < 8; i++) {
        this.case32Field1.push(new Case32Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__32_field1_entries
     */

    return Case32;
  })();

  var Case59Field1Entries = Id015PtlimaptOperation.Case59Field1Entries = (function() {
    function Case59Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case59Field1Entries.prototype._read = function() {
      this.case59Field1EltField0 = this._io.readU1();
      this.case59Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case59Field1Entries;
  })();

  var Case65 = Id015PtlimaptOperation.Case65 = (function() {
    function Case65(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case65.prototype._read = function() {
      this.case65Field0 = this._io.readU2be();
      this.case65Field1 = [];
      for (var i = 0; i < 32; i++) {
        this.case65Field1.push(new Case65Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__65_field1_entries
     */

    return Case65;
  })();

  var Case181Entries = Id015PtlimaptOperation.Case181Entries = (function() {
    function Case181Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case181Entries.prototype._read = function() {
      this.case181EltField0 = new Case181EltField00(this._io, this, this._root);
      this.case181EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case181Entries;
  })();

  var Stakers = Id015PtlimaptOperation.Stakers = (function() {
    function Stakers(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Stakers.prototype._read = function() {
      this.alice = new PublicKeyHash(this._io, this, this._root);
      this.bob = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Stakers;
  })();

  var MessageResultPathEntries = Id015PtlimaptOperation.MessageResultPathEntries = (function() {
    function MessageResultPathEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageResultPathEntries.prototype._read = function() {
      this.messageResultListHash = this._io.readBytes(32);
    }

    return MessageResultPathEntries;
  })();

  var Case66Field1Entries = Id015PtlimaptOperation.Case66Field1Entries = (function() {
    function Case66Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case66Field1Entries.prototype._read = function() {
      this.case66Field1Elt = new InodeTree(this._io, this, this._root);
    }

    return Case66Field1Entries;
  })();

  var InodeTree = Id015PtlimaptOperation.InodeTree = (function() {
    function InodeTree(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InodeTree.prototype._read = function() {
      this.inodeTreeTag = this._io.readU1();
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__4) {
        this.case4 = new Case4(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__8) {
        this.case8 = new Case8(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__12) {
        this.case12 = new Case12(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__16) {
        this.case16 = new Case16(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__20) {
        this.case20 = new Case20(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__24) {
        this.case24 = new Case24(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__28) {
        this.case28 = new Case28(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__32) {
        this.case32 = new Case32(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__36) {
        this.case36 = new Case36(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__40) {
        this.case40 = new Case40(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__44) {
        this.case44 = new Case44(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__48) {
        this.case48 = new Case48(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__52) {
        this.case52 = new Case52(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__56) {
        this.case56 = new Case56(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__60) {
        this.case60 = new Case60(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__64) {
        this.case64 = new Case64(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__5) {
        this.case5 = new Case5(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__9) {
        this.case9 = new Case9(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__13) {
        this.case13 = new Case13(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__17) {
        this.case17 = new Case17(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__21) {
        this.case21 = new Case21(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__25) {
        this.case25 = new Case25(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__29) {
        this.case29 = new Case29(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__33) {
        this.case33 = new Case33(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__37) {
        this.case37 = new Case37(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__41) {
        this.case41 = new Case41(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__45) {
        this.case45 = new Case45(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__49) {
        this.case49 = new Case49(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__53) {
        this.case53 = new Case53(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__57) {
        this.case57 = new Case57(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__61) {
        this.case61 = new Case61(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__65) {
        this.case65 = new Case65(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__6) {
        this.case6 = new Case6(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__10) {
        this.case10 = new Case10(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__14) {
        this.case14 = new Case14(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__18) {
        this.case18 = new Case18(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__22) {
        this.case22 = new Case22(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__26) {
        this.case26 = new Case26(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__30) {
        this.case30 = new Case30(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__34) {
        this.case34 = new Case34(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__38) {
        this.case38 = new Case38(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__42) {
        this.case42 = new Case42(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__46) {
        this.case46 = new Case46(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__50) {
        this.case50 = new Case50(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__54) {
        this.case54 = new Case54(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__58) {
        this.case58 = new Case58(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__62) {
        this.case62 = new Case62(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__66) {
        this.case66 = new Case66(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__7) {
        this.case7 = new Case7(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__11) {
        this.case11 = new Case11(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__15) {
        this.case15 = new Case15(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__19) {
        this.case19 = new Case19(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__23) {
        this.case23 = new Case23(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__27) {
        this.case27 = new Case27(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__31) {
        this.case31 = new Case31(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__35) {
        this.case35 = new Case35(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__39) {
        this.case39 = new Case39(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__43) {
        this.case43 = new Case43(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__47) {
        this.case47 = new Case47(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__51) {
        this.case51 = new Case51(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__55) {
        this.case55 = new Case55(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__59) {
        this.case59 = new Case59(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__63) {
        this.case63 = new Case63(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__67) {
        this.case67 = new Case67(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__129) {
        this.case129 = new Case129Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__130) {
        this.case130 = new Case130Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__131) {
        this.case131 = new Case131Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__132) {
        this.case132 = new Case132Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__133) {
        this.case133 = new Case133Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__134) {
        this.case134 = new Case134Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__135) {
        this.case135 = new Case135Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__136) {
        this.case136 = new Case136Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__137) {
        this.case137 = new Case137Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__138) {
        this.case138 = new Case138Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__139) {
        this.case139 = new Case139Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__140) {
        this.case140 = new Case140Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__141) {
        this.case141 = new Case141Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__142) {
        this.case142 = new Case142Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__143) {
        this.case143 = new Case143Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__144) {
        this.case144 = new Case144Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__145) {
        this.case145 = new Case145Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__146) {
        this.case146 = new Case146Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__147) {
        this.case147 = new Case147Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__148) {
        this.case148 = new Case148Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__149) {
        this.case149 = new Case149Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__150) {
        this.case150 = new Case150Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__151) {
        this.case151 = new Case151Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__152) {
        this.case152 = new Case152Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__153) {
        this.case153 = new Case153Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__154) {
        this.case154 = new Case154Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__155) {
        this.case155 = new Case155Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__156) {
        this.case156 = new Case156Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__157) {
        this.case157 = new Case157Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__158) {
        this.case158 = new Case158Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__159) {
        this.case159 = new Case159Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__160) {
        this.case160 = new Case160Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__161) {
        this.case161 = new Case161Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__162) {
        this.case162 = new Case162Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__163) {
        this.case163 = new Case163Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__164) {
        this.case164 = new Case164Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__165) {
        this.case165 = new Case165Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__166) {
        this.case166 = new Case166Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__167) {
        this.case167 = new Case167Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__168) {
        this.case168 = new Case168Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__169) {
        this.case169 = new Case169Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__170) {
        this.case170 = new Case170Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__171) {
        this.case171 = new Case171Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__172) {
        this.case172 = new Case172Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__173) {
        this.case173 = new Case173Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__174) {
        this.case174 = new Case174Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__175) {
        this.case175 = new Case175Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__176) {
        this.case176 = new Case176Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__177) {
        this.case177 = new Case177Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__178) {
        this.case178 = new Case178Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__179) {
        this.case179 = new Case179Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__180) {
        this.case180 = new Case180Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__181) {
        this.case181 = new Case181Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__182) {
        this.case182 = new Case182Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__183) {
        this.case183 = new Case183Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__184) {
        this.case184 = new Case184Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__185) {
        this.case185 = new Case185Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__186) {
        this.case186 = new Case186Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__187) {
        this.case187 = new Case187Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__188) {
        this.case188 = new Case188Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__189) {
        this.case189 = new Case189Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__190) {
        this.case190 = new Case190Entries(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__191) {
        this.case191 = new Case1910(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__192) {
        this.case192 = this._io.readBytes(32);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__208) {
        this.case208 = new Case208(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__209) {
        this.case209 = new Case209(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__210) {
        this.case210 = new Case210(this._io, this, this._root);
      }
      if (this.inodeTreeTag == Id015PtlimaptOperation.InodeTreeTag.CASE__211) {
        this.case211 = new Case211(this._io, this, this._root);
      }
    }

    return InodeTree;
  })();

  var Case171EltField0 = Id015PtlimaptOperation.Case171EltField0 = (function() {
    function Case171EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case171EltField0.prototype._read = function() {
      this.case171EltField0 = this._io.readBytesFull();
    }

    return Case171EltField0;
  })();

  var Id015PtlimaptScriptedContracts = Id015PtlimaptOperation.Id015PtlimaptScriptedContracts = (function() {
    function Id015PtlimaptScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id015PtlimaptScriptedContracts;
  })();

  var Case35 = Id015PtlimaptOperation.Case35 = (function() {
    function Case35(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case35.prototype._read = function() {
      this.case35Field0 = this._io.readS8be();
      this.case35Field1 = [];
      for (var i = 0; i < 8; i++) {
        this.case35Field1.push(new Case35Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__35_field1_entries
     */

    return Case35;
  })();

  var IncreasePaidStorage = Id015PtlimaptOperation.IncreasePaidStorage = (function() {
    function IncreasePaidStorage(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IncreasePaidStorage.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.amount = new Z(this._io, this, this._root);
      this.destination = new Id015PtlimaptContractIdOriginated(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A contract handle -- originated account: A contract notation as given to an RPC or inside scripts. Can be a base58 originated contract hash.
     */

    return IncreasePaidStorage;
  })();

  var Case180EltField00 = Id015PtlimaptOperation.Case180EltField00 = (function() {
    function Case180EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case180EltField00.prototype._read = function() {
      this.lenCase180EltField0 = this._io.readU1();
      if (!(this.lenCase180EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase180EltField0, this._io, "/types/case__180_elt_field0_0/seq/0");
      }
      this._raw_case180EltField0 = this._io.readBytes(this.lenCase180EltField0);
      var _io__raw_case180EltField0 = new KaitaiStream(this._raw_case180EltField0);
      this.case180EltField0 = new Case180EltField0(_io__raw_case180EltField0, this, this._root);
    }

    return Case180EltField00;
  })();

  var Case149EltField0 = Id015PtlimaptOperation.Case149EltField0 = (function() {
    function Case149EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case149EltField0.prototype._read = function() {
      this.case149EltField0 = this._io.readBytesFull();
    }

    return Case149EltField0;
  })();

  var Case51Field1Entries = Id015PtlimaptOperation.Case51Field1Entries = (function() {
    function Case51Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case51Field1Entries.prototype._read = function() {
      this.case51Field1EltField0 = this._io.readU1();
      this.case51Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case51Field1Entries;
  })();

  var Case17 = Id015PtlimaptOperation.Case17 = (function() {
    function Case17(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case17.prototype._read = function() {
      this.case17Field0 = this._io.readU2be();
      this.case17Field1 = [];
      for (var i = 0; i < 4; i++) {
        this.case17Field1.push(new Case17Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__17_field1_entries
     */

    return Case17;
  })();

  var ScRollupPublish = Id015PtlimaptOperation.ScRollupPublish = (function() {
    function ScRollupPublish(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupPublish.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptRollupAddress(this._io, this, this._root);
      this.commitment = new Commitment0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupPublish;
  })();

  var InputProof = Id015PtlimaptOperation.InputProof = (function() {
    function InputProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InputProof.prototype._read = function() {
      this.inputProofTag = this._io.readU1();
      if (this.inputProofTag == Id015PtlimaptOperation.InputProofTag.INBOX__PROOF) {
        this.inboxProof = new InboxProof(this._io, this, this._root);
      }
      if (this.inputProofTag == Id015PtlimaptOperation.InputProofTag.REVEAL__PROOF) {
        this.revealProof = new RevealProof(this._io, this, this._root);
      }
    }

    return InputProof;
  })();

  var Case28 = Id015PtlimaptOperation.Case28 = (function() {
    function Case28(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case28.prototype._read = function() {
      this.case28Field0 = this._io.readU1();
      this.case28Field1 = [];
      for (var i = 0; i < 7; i++) {
        this.case28Field1.push(new Case28Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__28_field1_entries
     */

    return Case28;
  })();

  var Id015PtlimaptBlockHeaderAlphaFullHeader = Id015PtlimaptOperation.Id015PtlimaptBlockHeaderAlphaFullHeader = (function() {
    function Id015PtlimaptBlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptBlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id015PtlimaptBlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id015PtlimaptBlockHeaderAlphaSignedContents = new Id015PtlimaptBlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id015PtlimaptBlockHeaderAlphaFullHeader;
  })();

  var Case53Field1Entries = Id015PtlimaptOperation.Case53Field1Entries = (function() {
    function Case53Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case53Field1Entries.prototype._read = function() {
      this.case53Field1EltField0 = this._io.readU1();
      this.case53Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case53Field1Entries;
  })();

  var Case8 = Id015PtlimaptOperation.Case8 = (function() {
    function Case8(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case8.prototype._read = function() {
      this.case8Field0 = this._io.readU1();
      this.case8Field1 = [];
      for (var i = 0; i < 2; i++) {
        this.case8Field1.push(new Case8Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__8_field1_entries
     */

    return Case8;
  })();

  var Case10Field1Entries = Id015PtlimaptOperation.Case10Field1Entries = (function() {
    function Case10Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case10Field1Entries.prototype._read = function() {
      this.case10Field1EltField0 = this._io.readU1();
      this.case10Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case10Field1Entries;
  })();

  var Id015PtlimaptLiquidityBakingToggleVote = Id015PtlimaptOperation.Id015PtlimaptLiquidityBakingToggleVote = (function() {
    function Id015PtlimaptLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptLiquidityBakingToggleVote.prototype._read = function() {
      this.id015PtlimaptLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id015PtlimaptLiquidityBakingToggleVote;
  })();

  var Id015PtlimaptTxRollupId = Id015PtlimaptOperation.Id015PtlimaptTxRollupId = (function() {
    function Id015PtlimaptTxRollupId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptTxRollupId.prototype._read = function() {
      this.rollupHash = this._io.readBytes(20);
    }

    return Id015PtlimaptTxRollupId;
  })();

  var Case189EltField0 = Id015PtlimaptOperation.Case189EltField0 = (function() {
    function Case189EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case189EltField0.prototype._read = function() {
      this.case189EltField0 = this._io.readBytesFull();
    }

    return Case189EltField0;
  })();

  var Case150EltField0 = Id015PtlimaptOperation.Case150EltField0 = (function() {
    function Case150EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case150EltField0.prototype._read = function() {
      this.case150EltField0 = this._io.readBytesFull();
    }

    return Case150EltField0;
  })();

  var Case10 = Id015PtlimaptOperation.Case10 = (function() {
    function Case10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case10.prototype._read = function() {
      this.case10Field0 = this._io.readS4be();
      this.case10Field1 = [];
      for (var i = 0; i < 2; i++) {
        this.case10Field1.push(new Case10Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__10_field1_entries
     */

    return Case10;
  })();

  var Case180EltField0 = Id015PtlimaptOperation.Case180EltField0 = (function() {
    function Case180EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case180EltField0.prototype._read = function() {
      this.case180EltField0 = this._io.readBytesFull();
    }

    return Case180EltField0;
  })();

  var Case60Field10 = Id015PtlimaptOperation.Case60Field10 = (function() {
    function Case60Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case60Field10.prototype._read = function() {
      this.lenCase60Field1 = this._io.readU4be();
      if (!(this.lenCase60Field1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase60Field1, this._io, "/types/case__60_field1_0/seq/0");
      }
      this._raw_case60Field1 = this._io.readBytes(this.lenCase60Field1);
      var _io__raw_case60Field1 = new KaitaiStream(this._raw_case60Field1);
      this.case60Field1 = new Case60Field1(_io__raw_case60Field1, this, this._root);
    }

    return Case60Field10;
  })();

  var Case61Field1 = Id015PtlimaptOperation.Case61Field1 = (function() {
    function Case61Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case61Field1.prototype._read = function() {
      this.case61Field1Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case61Field1Entries.push(new Case61Field1Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case61Field1;
  })();

  var ActivateAccount = Id015PtlimaptOperation.ActivateAccount = (function() {
    function ActivateAccount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ActivateAccount.prototype._read = function() {
      this.pkh = this._io.readBytes(20);
      this.secret = this._io.readBytes(20);
    }

    return ActivateAccount;
  })();

  var Case61 = Id015PtlimaptOperation.Case61 = (function() {
    function Case61(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case61.prototype._read = function() {
      this.case61Field0 = this._io.readU2be();
      this.case61Field1 = new Case61Field10(this._io, this, this._root);
    }

    return Case61;
  })();

  var Case152EltField0 = Id015PtlimaptOperation.Case152EltField0 = (function() {
    function Case152EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case152EltField0.prototype._read = function() {
      this.case152EltField0 = this._io.readBytesFull();
    }

    return Case152EltField0;
  })();

  var Case57Field1Entries = Id015PtlimaptOperation.Case57Field1Entries = (function() {
    function Case57Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case57Field1Entries.prototype._read = function() {
      this.case57Field1EltField0 = this._io.readU1();
      this.case57Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case57Field1Entries;
  })();

  var Case147Entries = Id015PtlimaptOperation.Case147Entries = (function() {
    function Case147Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case147Entries.prototype._read = function() {
      this.case147EltField0 = new Case147EltField00(this._io, this, this._root);
      this.case147EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case147Entries;
  })();

  var Case62Field1Entries = Id015PtlimaptOperation.Case62Field1Entries = (function() {
    function Case62Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case62Field1Entries.prototype._read = function() {
      this.case62Field1EltField0 = this._io.readU1();
      this.case62Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case62Field1Entries;
  })();

  var DoubleEndorsementEvidence = Id015PtlimaptOperation.DoubleEndorsementEvidence = (function() {
    function DoubleEndorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleEndorsementEvidence.prototype._read = function() {
      this.op1 = new Op10(this._io, this, this._root);
      this.op2 = new Op20(this._io, this, this._root);
    }

    return DoubleEndorsementEvidence;
  })();

  var Step = Id015PtlimaptOperation.Step = (function() {
    function Step(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Step.prototype._read = function() {
      this.stepTag = this._io.readU1();
      if (this.stepTag == Id015PtlimaptOperation.StepTag.DISSECTION) {
        this.dissection = new Dissection0(this._io, this, this._root);
      }
      if (this.stepTag == Id015PtlimaptOperation.StepTag.PROOF) {
        this.proof = new Proof1(this._io, this, this._root);
      }
    }

    return Step;
  })();

  var Id015PtlimaptRollupAddress = Id015PtlimaptOperation.Id015PtlimaptRollupAddress = (function() {
    function Id015PtlimaptRollupAddress(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptRollupAddress.prototype._read = function() {
      this.id015PtlimaptRollupAddress = new BytesDynUint30(this._io, this, this._root);
    }

    return Id015PtlimaptRollupAddress;
  })();

  var Case39 = Id015PtlimaptOperation.Case39 = (function() {
    function Case39(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case39.prototype._read = function() {
      this.case39Field0 = this._io.readS8be();
      this.case39Field1 = [];
      for (var i = 0; i < 9; i++) {
        this.case39Field1.push(new Case39Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__39_field1_entries
     */

    return Case39;
  })();

  var Case9 = Id015PtlimaptOperation.Case9 = (function() {
    function Case9(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case9.prototype._read = function() {
      this.case9Field0 = this._io.readU2be();
      this.case9Field1 = [];
      for (var i = 0; i < 2; i++) {
        this.case9Field1.push(new Case9Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__9_field1_entries
     */

    return Case9;
  })();

  var Case18Field1Entries = Id015PtlimaptOperation.Case18Field1Entries = (function() {
    function Case18Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case18Field1Entries.prototype._read = function() {
      this.case18Field1EltField0 = this._io.readU1();
      this.case18Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case18Field1Entries;
  })();

  var Case13 = Id015PtlimaptOperation.Case13 = (function() {
    function Case13(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case13.prototype._read = function() {
      this.case13Field0 = this._io.readU2be();
      this.case13Field1 = [];
      for (var i = 0; i < 3; i++) {
        this.case13Field1.push(new Case13Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__13_field1_entries
     */

    return Case13;
  })();

  var Originated = Id015PtlimaptOperation.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Case170EltField00 = Id015PtlimaptOperation.Case170EltField00 = (function() {
    function Case170EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case170EltField00.prototype._read = function() {
      this.lenCase170EltField0 = this._io.readU1();
      if (!(this.lenCase170EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase170EltField0, this._io, "/types/case__170_elt_field0_0/seq/0");
      }
      this._raw_case170EltField0 = this._io.readBytes(this.lenCase170EltField0);
      var _io__raw_case170EltField0 = new KaitaiStream(this._raw_case170EltField0);
      this.case170EltField0 = new Case170EltField0(_io__raw_case170EltField0, this, this._root);
    }

    return Case170EltField00;
  })();

  var Case29Field1Entries = Id015PtlimaptOperation.Case29Field1Entries = (function() {
    function Case29Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case29Field1Entries.prototype._read = function() {
      this.case29Field1EltField0 = this._io.readU1();
      this.case29Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case29Field1Entries;
  })();

  var Case145EltField00 = Id015PtlimaptOperation.Case145EltField00 = (function() {
    function Case145EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case145EltField00.prototype._read = function() {
      this.lenCase145EltField0 = this._io.readU1();
      if (!(this.lenCase145EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase145EltField0, this._io, "/types/case__145_elt_field0_0/seq/0");
      }
      this._raw_case145EltField0 = this._io.readBytes(this.lenCase145EltField0);
      var _io__raw_case145EltField0 = new KaitaiStream(this._raw_case145EltField0);
      this.case145EltField0 = new Case145EltField0(_io__raw_case145EltField0, this, this._root);
    }

    return Case145EltField00;
  })();

  var N = Id015PtlimaptOperation.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id015PtlimaptInlinedEndorsementMempoolContents = Id015PtlimaptOperation.Id015PtlimaptInlinedEndorsementMempoolContents = (function() {
    function Id015PtlimaptInlinedEndorsementMempoolContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptInlinedEndorsementMempoolContents.prototype._read = function() {
      this.id015PtlimaptInlinedEndorsementMempoolContentsTag = this._io.readU1();
      if (this.id015PtlimaptInlinedEndorsementMempoolContentsTag == Id015PtlimaptOperation.Id015PtlimaptInlinedEndorsementMempoolContentsTag.ENDORSEMENT) {
        this.endorsement = new Endorsement(this._io, this, this._root);
      }
    }

    return Id015PtlimaptInlinedEndorsementMempoolContents;
  })();

  var Case186Entries = Id015PtlimaptOperation.Case186Entries = (function() {
    function Case186Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case186Entries.prototype._read = function() {
      this.case186EltField0 = new Case186EltField00(this._io, this, this._root);
      this.case186EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case186Entries;
  })();

  var Case4 = Id015PtlimaptOperation.Case4 = (function() {
    function Case4(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case4.prototype._read = function() {
      this.case4Field0 = this._io.readU1();
      this.case4Field1 = [];
      for (var i = 0; i < 1; i++) {
        this.case4Field1.push(new Case4Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__4_field1_entries
     */

    return Case4;
  })();

  var Endorsement = Id015PtlimaptOperation.Endorsement = (function() {
    function Endorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Endorsement.prototype._read = function() {
      this.slot = this._io.readU2be();
      this.level = this._io.readS4be();
      this.round = this._io.readS4be();
      this.blockPayloadHash = this._io.readBytes(32);
    }

    return Endorsement;
  })();

  var Case187EltField0 = Id015PtlimaptOperation.Case187EltField0 = (function() {
    function Case187EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case187EltField0.prototype._read = function() {
      this.case187EltField0 = this._io.readBytesFull();
    }

    return Case187EltField0;
  })();

  var Case60Field1Entries = Id015PtlimaptOperation.Case60Field1Entries = (function() {
    function Case60Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case60Field1Entries.prototype._read = function() {
      this.case60Field1EltField0 = this._io.readU1();
      this.case60Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case60Field1Entries;
  })();

  var Case130Entries = Id015PtlimaptOperation.Case130Entries = (function() {
    function Case130Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130Entries.prototype._read = function() {
      this.case130EltField0 = new Case130EltField00(this._io, this, this._root);
      this.case130EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case130Entries;
  })();

  var Case136Entries = Id015PtlimaptOperation.Case136Entries = (function() {
    function Case136Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case136Entries.prototype._read = function() {
      this.case136EltField0 = new Case136EltField00(this._io, this, this._root);
      this.case136EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case136Entries;
  })();

  var Case62Field1 = Id015PtlimaptOperation.Case62Field1 = (function() {
    function Case62Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case62Field1.prototype._read = function() {
      this.case62Field1Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case62Field1Entries.push(new Case62Field1Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case62Field1;
  })();

  var Case63Field1 = Id015PtlimaptOperation.Case63Field1 = (function() {
    function Case63Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case63Field1.prototype._read = function() {
      this.case63Field1Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case63Field1Entries.push(new Case63Field1Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case63Field1;
  })();

  var Case163EltField00 = Id015PtlimaptOperation.Case163EltField00 = (function() {
    function Case163EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case163EltField00.prototype._read = function() {
      this.lenCase163EltField0 = this._io.readU1();
      if (!(this.lenCase163EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase163EltField0, this._io, "/types/case__163_elt_field0_0/seq/0");
      }
      this._raw_case163EltField0 = this._io.readBytes(this.lenCase163EltField0);
      var _io__raw_case163EltField0 = new KaitaiStream(this._raw_case163EltField0);
      this.case163EltField0 = new Case163EltField0(_io__raw_case163EltField0, this, this._root);
    }

    return Case163EltField00;
  })();

  var Proposals0 = Id015PtlimaptOperation.Proposals0 = (function() {
    function Proposals0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals0.prototype._read = function() {
      this.lenProposals = this._io.readU4be();
      if (!(this.lenProposals <= 640)) {
        throw new KaitaiStream.ValidationGreaterThanError(640, this.lenProposals, this._io, "/types/proposals_0/seq/0");
      }
      this._raw_proposals = this._io.readBytes(this.lenProposals);
      var _io__raw_proposals = new KaitaiStream(this._raw_proposals);
      this.proposals = new Proposals(_io__raw_proposals, this, this._root);
    }

    return Proposals0;
  })();

  var MessagesEntries = Id015PtlimaptOperation.MessagesEntries = (function() {
    function MessagesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagesEntries.prototype._read = function() {
      this.messageResultHash = this._io.readBytes(32);
    }

    return MessagesEntries;
  })();

  var Case157Entries = Id015PtlimaptOperation.Case157Entries = (function() {
    function Case157Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case157Entries.prototype._read = function() {
      this.case157EltField0 = new Case157EltField00(this._io, this, this._root);
      this.case157EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case157Entries;
  })();

  var Case1910 = Id015PtlimaptOperation.Case1910 = (function() {
    function Case1910(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1910.prototype._read = function() {
      this.lenCase191 = this._io.readU4be();
      if (!(this.lenCase191 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase191, this._io, "/types/case__191_0/seq/0");
      }
      this._raw_case191 = this._io.readBytes(this.lenCase191);
      var _io__raw_case191 = new KaitaiStream(this._raw_case191);
      this.case191 = new Case191(_io__raw_case191, this, this._root);
    }

    return Case1910;
  })();

  var TransferTicket = Id015PtlimaptOperation.TransferTicket = (function() {
    function TransferTicket(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TransferTicket.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.ticketContents = new BytesDynUint30(this._io, this, this._root);
      this.ticketTy = new BytesDynUint30(this._io, this, this._root);
      this.ticketTicketer = new Id015PtlimaptContractId(this._io, this, this._root);
      this.ticketAmount = new N(this._io, this, this._root);
      this.destination = new Id015PtlimaptContractId(this._io, this, this._root);
      this.entrypoint = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return TransferTicket;
  })();

  var Case208Field1 = Id015PtlimaptOperation.Case208Field1 = (function() {
    function Case208Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case208Field1.prototype._read = function() {
      this.case208Field1 = this._io.readBytesFull();
    }

    return Case208Field1;
  })();

  var Case129EltField0 = Id015PtlimaptOperation.Case129EltField0 = (function() {
    function Case129EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129EltField0.prototype._read = function() {
      this.case129EltField0 = this._io.readBytesFull();
    }

    return Case129EltField0;
  })();

  var Case218 = Id015PtlimaptOperation.Case218 = (function() {
    function Case218(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case218.prototype._read = function() {
      this.case218Field0 = this._io.readS4be();
      this.case218Field1 = new Case218Field10(this._io, this, this._root);
      this.case218Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case218;
  })();

  var Payload = Id015PtlimaptOperation.Payload = (function() {
    function Payload(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Payload.prototype._read = function() {
      this.payloadEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.payloadEntries.push(new PayloadEntries(this._io, this, this._root));
        i++;
      }
    }

    return Payload;
  })();

  var Case13Field1Entries = Id015PtlimaptOperation.Case13Field1Entries = (function() {
    function Case13Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case13Field1Entries.prototype._read = function() {
      this.case13Field1EltField0 = this._io.readU1();
      this.case13Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case13Field1Entries;
  })();

  var Case140EltField0 = Id015PtlimaptOperation.Case140EltField0 = (function() {
    function Case140EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case140EltField0.prototype._read = function() {
      this.case140EltField0 = this._io.readBytesFull();
    }

    return Case140EltField0;
  })();

  var Case48Field1Entries = Id015PtlimaptOperation.Case48Field1Entries = (function() {
    function Case48Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case48Field1Entries.prototype._read = function() {
      this.case48Field1EltField0 = this._io.readU1();
      this.case48Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case48Field1Entries;
  })();

  var Case60Field1 = Id015PtlimaptOperation.Case60Field1 = (function() {
    function Case60Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case60Field1.prototype._read = function() {
      this.case60Field1Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case60Field1Entries.push(new Case60Field1Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case60Field1;
  })();

  var Case145Entries = Id015PtlimaptOperation.Case145Entries = (function() {
    function Case145Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case145Entries.prototype._read = function() {
      this.case145EltField0 = new Case145EltField00(this._io, this, this._root);
      this.case145EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case145Entries;
  })();

  var Case155EltField00 = Id015PtlimaptOperation.Case155EltField00 = (function() {
    function Case155EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case155EltField00.prototype._read = function() {
      this.lenCase155EltField0 = this._io.readU1();
      if (!(this.lenCase155EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase155EltField0, this._io, "/types/case__155_elt_field0_0/seq/0");
      }
      this._raw_case155EltField0 = this._io.readBytes(this.lenCase155EltField0);
      var _io__raw_case155EltField0 = new KaitaiStream(this._raw_case155EltField0);
      this.case155EltField0 = new Case155EltField0(_io__raw_case155EltField0, this, this._root);
    }

    return Case155EltField00;
  })();

  var CircuitsInfo = Id015PtlimaptOperation.CircuitsInfo = (function() {
    function CircuitsInfo(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CircuitsInfo.prototype._read = function() {
      this.circuitsInfoEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.circuitsInfoEntries.push(new CircuitsInfoEntries(this._io, this, this._root));
        i++;
      }
    }

    return CircuitsInfo;
  })();

  var Case57 = Id015PtlimaptOperation.Case57 = (function() {
    function Case57(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case57.prototype._read = function() {
      this.case57Field0 = this._io.readU2be();
      this.case57Field1 = [];
      for (var i = 0; i < 14; i++) {
        this.case57Field1.push(new Case57Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__57_field1_entries
     */

    return Case57;
  })();

  var Op12 = Id015PtlimaptOperation.Op12 = (function() {
    function Op12(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op12.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_2/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op11(_io__raw_op1, this, this._root);
    }

    return Op12;
  })();

  var Case62Field10 = Id015PtlimaptOperation.Case62Field10 = (function() {
    function Case62Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case62Field10.prototype._read = function() {
      this.lenCase62Field1 = this._io.readU4be();
      if (!(this.lenCase62Field1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase62Field1, this._io, "/types/case__62_field1_0/seq/0");
      }
      this._raw_case62Field1 = this._io.readBytes(this.lenCase62Field1);
      var _io__raw_case62Field1 = new KaitaiStream(this._raw_case62Field1);
      this.case62Field1 = new Case62Field1(_io__raw_case62Field1, this, this._root);
    }

    return Case62Field10;
  })();

  var Case40 = Id015PtlimaptOperation.Case40 = (function() {
    function Case40(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case40.prototype._read = function() {
      this.case40Field0 = this._io.readU1();
      this.case40Field1 = [];
      for (var i = 0; i < 10; i++) {
        this.case40Field1.push(new Case40Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__40_field1_entries
     */

    return Case40;
  })();

  var ScRollupExecuteOutboxMessage = Id015PtlimaptOperation.ScRollupExecuteOutboxMessage = (function() {
    function ScRollupExecuteOutboxMessage(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupExecuteOutboxMessage.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptRollupAddress(this._io, this, this._root);
      this.cementedCommitment = this._io.readBytes(32);
      this.outputProof = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupExecuteOutboxMessage;
  })();

  var Case132EltField00 = Id015PtlimaptOperation.Case132EltField00 = (function() {
    function Case132EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case132EltField00.prototype._read = function() {
      this.lenCase132EltField0 = this._io.readU1();
      if (!(this.lenCase132EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase132EltField0, this._io, "/types/case__132_elt_field0_0/seq/0");
      }
      this._raw_case132EltField0 = this._io.readBytes(this.lenCase132EltField0);
      var _io__raw_case132EltField0 = new KaitaiStream(this._raw_case132EltField0);
      this.case132EltField0 = new Case132EltField0(_io__raw_case132EltField0, this, this._root);
    }

    return Case132EltField00;
  })();

  var Case9Field1Entries = Id015PtlimaptOperation.Case9Field1Entries = (function() {
    function Case9Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case9Field1Entries.prototype._read = function() {
      this.case9Field1EltField0 = this._io.readU1();
      this.case9Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case9Field1Entries;
  })();

  var Id015PtlimaptEntrypoint = Id015PtlimaptOperation.Id015PtlimaptEntrypoint = (function() {
    function Id015PtlimaptEntrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptEntrypoint.prototype._read = function() {
      this.id015PtlimaptEntrypointTag = this._io.readU1();
      if (this.id015PtlimaptEntrypointTag == Id015PtlimaptOperation.Id015PtlimaptEntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id015PtlimaptEntrypoint;
  })();

  var TxRollupRemoveCommitment = Id015PtlimaptOperation.TxRollupRemoveCommitment = (function() {
    function TxRollupRemoveCommitment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupRemoveCommitment.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptTxRollupId(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupRemoveCommitment;
  })();

  var Case0 = Id015PtlimaptOperation.Case0 = (function() {
    function Case0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case0.prototype._read = function() {
      this.case0Field0 = this._io.readS2be();
      this.case0Field1 = this._io.readBytes(32);
      this.case0Field2 = this._io.readBytes(32);
      this.case0Field3 = new TreeEncoding(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    /**
     * tree_encoding
     */

    return Case0;
  })();

  var Case210Field1 = Id015PtlimaptOperation.Case210Field1 = (function() {
    function Case210Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case210Field1.prototype._read = function() {
      this.case210Field1 = this._io.readBytesFull();
    }

    return Case210Field1;
  })();

  var Case164Entries = Id015PtlimaptOperation.Case164Entries = (function() {
    function Case164Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case164Entries.prototype._read = function() {
      this.case164EltField0 = new Case164EltField00(this._io, this, this._root);
      this.case164EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case164Entries;
  })();

  var Case165EltField0 = Id015PtlimaptOperation.Case165EltField0 = (function() {
    function Case165EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case165EltField0.prototype._read = function() {
      this.case165EltField0 = this._io.readBytesFull();
    }

    return Case165EltField0;
  })();

  var Case191EltField00 = Id015PtlimaptOperation.Case191EltField00 = (function() {
    function Case191EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case191EltField00.prototype._read = function() {
      this.lenCase191EltField0 = this._io.readU1();
      if (!(this.lenCase191EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase191EltField0, this._io, "/types/case__191_elt_field0_0/seq/0");
      }
      this._raw_case191EltField0 = this._io.readBytes(this.lenCase191EltField0);
      var _io__raw_case191EltField0 = new KaitaiStream(this._raw_case191EltField0);
      this.case191EltField0 = new Case191EltField0(_io__raw_case191EltField0, this, this._root);
    }

    return Case191EltField00;
  })();

  var Case51 = Id015PtlimaptOperation.Case51 = (function() {
    function Case51(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case51.prototype._read = function() {
      this.case51Field0 = this._io.readS8be();
      this.case51Field1 = [];
      for (var i = 0; i < 12; i++) {
        this.case51Field1.push(new Case51Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__51_field1_entries
     */

    return Case51;
  })();

  var Case178EltField00 = Id015PtlimaptOperation.Case178EltField00 = (function() {
    function Case178EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case178EltField00.prototype._read = function() {
      this.lenCase178EltField0 = this._io.readU1();
      if (!(this.lenCase178EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase178EltField0, this._io, "/types/case__178_elt_field0_0/seq/0");
      }
      this._raw_case178EltField0 = this._io.readBytes(this.lenCase178EltField0);
      var _io__raw_case178EltField0 = new KaitaiStream(this._raw_case178EltField0);
      this.case178EltField0 = new Case178EltField0(_io__raw_case178EltField0, this, this._root);
    }

    return Case178EltField00;
  })();

  var Case146EltField0 = Id015PtlimaptOperation.Case146EltField0 = (function() {
    function Case146EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case146EltField0.prototype._read = function() {
      this.case146EltField0 = this._io.readBytesFull();
    }

    return Case146EltField0;
  })();

  var Case22Field1Entries = Id015PtlimaptOperation.Case22Field1Entries = (function() {
    function Case22Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case22Field1Entries.prototype._read = function() {
      this.case22Field1EltField0 = this._io.readU1();
      this.case22Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case22Field1Entries;
  })();

  var Case21 = Id015PtlimaptOperation.Case21 = (function() {
    function Case21(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case21.prototype._read = function() {
      this.case21Field0 = this._io.readU2be();
      this.case21Field1 = [];
      for (var i = 0; i < 5; i++) {
        this.case21Field1.push(new Case21Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__21_field1_entries
     */

    return Case21;
  })();

  var Case46 = Id015PtlimaptOperation.Case46 = (function() {
    function Case46(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case46.prototype._read = function() {
      this.case46Field0 = this._io.readS4be();
      this.case46Field1 = [];
      for (var i = 0; i < 11; i++) {
        this.case46Field1.push(new Case46Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__46_field1_entries
     */

    return Case46;
  })();

  var Case159Entries = Id015PtlimaptOperation.Case159Entries = (function() {
    function Case159Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case159Entries.prototype._read = function() {
      this.case159EltField0 = new Case159EltField00(this._io, this, this._root);
      this.case159EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case159Entries;
  })();

  var Case134Entries = Id015PtlimaptOperation.Case134Entries = (function() {
    function Case134Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case134Entries.prototype._read = function() {
      this.case134EltField0 = new Case134EltField00(this._io, this, this._root);
      this.case134EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case134Entries;
  })();

  var Case155Entries = Id015PtlimaptOperation.Case155Entries = (function() {
    function Case155Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case155Entries.prototype._read = function() {
      this.case155EltField0 = new Case155EltField00(this._io, this, this._root);
      this.case155EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case155Entries;
  })();

  var Case148EltField00 = Id015PtlimaptOperation.Case148EltField00 = (function() {
    function Case148EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case148EltField00.prototype._read = function() {
      this.lenCase148EltField0 = this._io.readU1();
      if (!(this.lenCase148EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase148EltField0, this._io, "/types/case__148_elt_field0_0/seq/0");
      }
      this._raw_case148EltField0 = this._io.readBytes(this.lenCase148EltField0);
      var _io__raw_case148EltField0 = new KaitaiStream(this._raw_case148EltField0);
      this.case148EltField0 = new Case148EltField0(_io__raw_case148EltField0, this, this._root);
    }

    return Case148EltField00;
  })();

  var Case54Field1Entries = Id015PtlimaptOperation.Case54Field1Entries = (function() {
    function Case54Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case54Field1Entries.prototype._read = function() {
      this.case54Field1EltField0 = this._io.readU1();
      this.case54Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case54Field1Entries;
  })();

  var Prim1ArgSomeAnnots = Id015PtlimaptOperation.Prim1ArgSomeAnnots = (function() {
    function Prim1ArgSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim1ArgSomeAnnots.prototype._read = function() {
      this.prim = new Id015PtlimaptMichelsonV1Primitives(this._io, this, this._root);
      this.arg = new Micheline015PtlimaptMichelsonV1Expression(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return Prim1ArgSomeAnnots;
  })();

  var Case172Entries = Id015PtlimaptOperation.Case172Entries = (function() {
    function Case172Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case172Entries.prototype._read = function() {
      this.case172EltField0 = new Case172EltField00(this._io, this, this._root);
      this.case172EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case172Entries;
  })();

  var Case135Entries = Id015PtlimaptOperation.Case135Entries = (function() {
    function Case135Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case135Entries.prototype._read = function() {
      this.case135EltField0 = new Case135EltField00(this._io, this, this._root);
      this.case135EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case135Entries;
  })();

  var Reveal = Id015PtlimaptOperation.Reveal = (function() {
    function Reveal(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Reveal.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.publicKey = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Reveal;
  })();

  var Case148EltField0 = Id015PtlimaptOperation.Case148EltField0 = (function() {
    function Case148EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case148EltField0.prototype._read = function() {
      this.case148EltField0 = this._io.readBytesFull();
    }

    return Case148EltField0;
  })();

  var Case168EltField0 = Id015PtlimaptOperation.Case168EltField0 = (function() {
    function Case168EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case168EltField0.prototype._read = function() {
      this.case168EltField0 = this._io.readBytesFull();
    }

    return Case168EltField0;
  })();

  var Case210 = Id015PtlimaptOperation.Case210 = (function() {
    function Case210(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case210.prototype._read = function() {
      this.case210Field0 = this._io.readS4be();
      this.case210Field1 = new Case210Field10(this._io, this, this._root);
      this.case210Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case210;
  })();

  var Case34 = Id015PtlimaptOperation.Case34 = (function() {
    function Case34(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case34.prototype._read = function() {
      this.case34Field0 = this._io.readS4be();
      this.case34Field1 = [];
      for (var i = 0; i < 8; i++) {
        this.case34Field1.push(new Case34Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__34_field1_entries
     */

    return Case34;
  })();

  var RevealProof = Id015PtlimaptOperation.RevealProof = (function() {
    function RevealProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RevealProof.prototype._read = function() {
      this.revealProofTag = this._io.readU1();
      if (this.revealProofTag == Id015PtlimaptOperation.RevealProofTag.RAW__DATA__PROOF) {
        this.rawDataProof = new BytesDynUint30(this._io, this, this._root);
      }
    }

    return RevealProof;
  })();

  var Id015PtlimaptInlinedPreendorsementContents = Id015PtlimaptOperation.Id015PtlimaptInlinedPreendorsementContents = (function() {
    function Id015PtlimaptInlinedPreendorsementContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptInlinedPreendorsementContents.prototype._read = function() {
      this.id015PtlimaptInlinedPreendorsementContentsTag = this._io.readU1();
      if (this.id015PtlimaptInlinedPreendorsementContentsTag == Id015PtlimaptOperation.Id015PtlimaptInlinedPreendorsementContentsTag.PREENDORSEMENT) {
        this.preendorsement = new Preendorsement(this._io, this, this._root);
      }
    }

    return Id015PtlimaptInlinedPreendorsementContents;
  })();

  var Case31Field1Entries = Id015PtlimaptOperation.Case31Field1Entries = (function() {
    function Case31Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case31Field1Entries.prototype._read = function() {
      this.case31Field1EltField0 = this._io.readU1();
      this.case31Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case31Field1Entries;
  })();

  var Case23 = Id015PtlimaptOperation.Case23 = (function() {
    function Case23(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case23.prototype._read = function() {
      this.case23Field0 = this._io.readS8be();
      this.case23Field1 = [];
      for (var i = 0; i < 5; i++) {
        this.case23Field1.push(new Case23Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__23_field1_entries
     */

    return Case23;
  })();

  var Case44 = Id015PtlimaptOperation.Case44 = (function() {
    function Case44(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case44.prototype._read = function() {
      this.case44Field0 = this._io.readU1();
      this.case44Field1 = [];
      for (var i = 0; i < 11; i++) {
        this.case44Field1.push(new Case44Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__44_field1_entries
     */

    return Case44;
  })();

  var Case6Field1Entries = Id015PtlimaptOperation.Case6Field1Entries = (function() {
    function Case6Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case6Field1Entries.prototype._read = function() {
      this.case6Field1EltField0 = this._io.readU1();
      this.case6Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case6Field1Entries;
  })();

  var CircuitsInfo0 = Id015PtlimaptOperation.CircuitsInfo0 = (function() {
    function CircuitsInfo0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CircuitsInfo0.prototype._read = function() {
      this.lenCircuitsInfo = this._io.readU4be();
      if (!(this.lenCircuitsInfo <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCircuitsInfo, this._io, "/types/circuits_info_0/seq/0");
      }
      this._raw_circuitsInfo = this._io.readBytes(this.lenCircuitsInfo);
      var _io__raw_circuitsInfo = new KaitaiStream(this._raw_circuitsInfo);
      this.circuitsInfo = new CircuitsInfo(_io__raw_circuitsInfo, this, this._root);
    }

    return CircuitsInfo0;
  })();

  var Case188Entries = Id015PtlimaptOperation.Case188Entries = (function() {
    function Case188Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case188Entries.prototype._read = function() {
      this.case188EltField0 = new Case188EltField00(this._io, this, this._root);
      this.case188EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case188Entries;
  })();

  var Price = Id015PtlimaptOperation.Price = (function() {
    function Price(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Price.prototype._read = function() {
      this.id = this._io.readBytes(32);
      this.amount = new Z(this._io, this, this._root);
    }

    return Price;
  })();

  var Case33 = Id015PtlimaptOperation.Case33 = (function() {
    function Case33(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case33.prototype._read = function() {
      this.case33Field0 = this._io.readU2be();
      this.case33Field1 = [];
      for (var i = 0; i < 8; i++) {
        this.case33Field1.push(new Case33Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__33_field1_entries
     */

    return Case33;
  })();

  var Case66 = Id015PtlimaptOperation.Case66 = (function() {
    function Case66(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case66.prototype._read = function() {
      this.case66Field0 = this._io.readS4be();
      this.case66Field1 = [];
      for (var i = 0; i < 32; i++) {
        this.case66Field1.push(new Case66Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__66_field1_entries
     */

    return Case66;
  })();

  var Case150Entries = Id015PtlimaptOperation.Case150Entries = (function() {
    function Case150Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case150Entries.prototype._read = function() {
      this.case150EltField0 = new Case150EltField00(this._io, this, this._root);
      this.case150EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case150Entries;
  })();

  var Case185EltField00 = Id015PtlimaptOperation.Case185EltField00 = (function() {
    function Case185EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case185EltField00.prototype._read = function() {
      this.lenCase185EltField0 = this._io.readU1();
      if (!(this.lenCase185EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase185EltField0, this._io, "/types/case__185_elt_field0_0/seq/0");
      }
      this._raw_case185EltField0 = this._io.readBytes(this.lenCase185EltField0);
      var _io__raw_case185EltField0 = new KaitaiStream(this._raw_case185EltField0);
      this.case185EltField0 = new Case185EltField0(_io__raw_case185EltField0, this, this._root);
    }

    return Case185EltField00;
  })();

  var Id015PtlimaptBlockHeaderAlphaSignedContents = Id015PtlimaptOperation.Id015PtlimaptBlockHeaderAlphaSignedContents = (function() {
    function Id015PtlimaptBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id015PtlimaptBlockHeaderAlphaUnsignedContents = new Id015PtlimaptBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id015PtlimaptBlockHeaderAlphaSignedContents;
  })();

  var Case142Entries = Id015PtlimaptOperation.Case142Entries = (function() {
    function Case142Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case142Entries.prototype._read = function() {
      this.case142EltField0 = new Case142EltField00(this._io, this, this._root);
      this.case142EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case142Entries;
  })();

  var Case22 = Id015PtlimaptOperation.Case22 = (function() {
    function Case22(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case22.prototype._read = function() {
      this.case22Field0 = this._io.readS4be();
      this.case22Field1 = [];
      for (var i = 0; i < 5; i++) {
        this.case22Field1.push(new Case22Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__22_field1_entries
     */

    return Case22;
  })();

  var Case175EltField00 = Id015PtlimaptOperation.Case175EltField00 = (function() {
    function Case175EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case175EltField00.prototype._read = function() {
      this.lenCase175EltField0 = this._io.readU1();
      if (!(this.lenCase175EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase175EltField0, this._io, "/types/case__175_elt_field0_0/seq/0");
      }
      this._raw_case175EltField0 = this._io.readBytes(this.lenCase175EltField0);
      var _io__raw_case175EltField0 = new KaitaiStream(this._raw_case175EltField0);
      this.case175EltField0 = new Case175EltField0(_io__raw_case175EltField0, this, this._root);
    }

    return Case175EltField00;
  })();

  var MessageEntries = Id015PtlimaptOperation.MessageEntries = (function() {
    function MessageEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageEntries.prototype._read = function() {
      this.messageElt = new BytesDynUint30(this._io, this, this._root);
    }

    return MessageEntries;
  })();

  var Case55 = Id015PtlimaptOperation.Case55 = (function() {
    function Case55(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case55.prototype._read = function() {
      this.case55Field0 = this._io.readS8be();
      this.case55Field1 = [];
      for (var i = 0; i < 13; i++) {
        this.case55Field1.push(new Case55Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__55_field1_entries
     */

    return Case55;
  })();

  var Case40Field1Entries = Id015PtlimaptOperation.Case40Field1Entries = (function() {
    function Case40Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case40Field1Entries.prototype._read = function() {
      this.case40Field1EltField0 = this._io.readU1();
      this.case40Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case40Field1Entries;
  })();

  var OpEltField0 = Id015PtlimaptOperation.OpEltField0 = (function() {
    function OpEltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    OpEltField0.prototype._read = function() {
      this.opCode = new Int31(this._io, this, this._root);
      this.price = new Price(this._io, this, this._root);
      this.l1Dst = new PublicKeyHash(this._io, this, this._root);
      this.rollupId = this._io.readBytes(20);
      this.payload = new Payload0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return OpEltField0;
  })();

  var Case191Entries = Id015PtlimaptOperation.Case191Entries = (function() {
    function Case191Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case191Entries.prototype._read = function() {
      this.case191EltField0 = new Case191EltField00(this._io, this, this._root);
      this.case191EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case191Entries;
  })();

  var Case154EltField0 = Id015PtlimaptOperation.Case154EltField0 = (function() {
    function Case154EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case154EltField0.prototype._read = function() {
      this.case154EltField0 = this._io.readBytesFull();
    }

    return Case154EltField0;
  })();

  var Case216Field1 = Id015PtlimaptOperation.Case216Field1 = (function() {
    function Case216Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case216Field1.prototype._read = function() {
      this.case216Field1 = this._io.readBytesFull();
    }

    return Case216Field1;
  })();

  var Case144Entries = Id015PtlimaptOperation.Case144Entries = (function() {
    function Case144Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case144Entries.prototype._read = function() {
      this.case144EltField0 = new Case144EltField00(this._io, this, this._root);
      this.case144EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case144Entries;
  })();

  var Case187Entries = Id015PtlimaptOperation.Case187Entries = (function() {
    function Case187Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case187Entries.prototype._read = function() {
      this.case187EltField0 = new Case187EltField00(this._io, this, this._root);
      this.case187EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case187Entries;
  })();

  var Case132EltField0 = Id015PtlimaptOperation.Case132EltField0 = (function() {
    function Case132EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case132EltField0.prototype._read = function() {
      this.case132EltField0 = this._io.readBytesFull();
    }

    return Case132EltField0;
  })();

  var Case217Field1 = Id015PtlimaptOperation.Case217Field1 = (function() {
    function Case217Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case217Field1.prototype._read = function() {
      this.case217Field1 = this._io.readBytesFull();
    }

    return Case217Field1;
  })();

  var Case61Field10 = Id015PtlimaptOperation.Case61Field10 = (function() {
    function Case61Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case61Field10.prototype._read = function() {
      this.lenCase61Field1 = this._io.readU4be();
      if (!(this.lenCase61Field1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase61Field1, this._io, "/types/case__61_field1_0/seq/0");
      }
      this._raw_case61Field1 = this._io.readBytes(this.lenCase61Field1);
      var _io__raw_case61Field1 = new KaitaiStream(this._raw_case61Field1);
      this.case61Field1 = new Case61Field1(_io__raw_case61Field1, this, this._root);
    }

    return Case61Field10;
  })();

  var Case52 = Id015PtlimaptOperation.Case52 = (function() {
    function Case52(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case52.prototype._read = function() {
      this.case52Field0 = this._io.readU1();
      this.case52Field1 = [];
      for (var i = 0; i < 13; i++) {
        this.case52Field1.push(new Case52Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__52_field1_entries
     */

    return Case52;
  })();

  var Case5 = Id015PtlimaptOperation.Case5 = (function() {
    function Case5(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case5.prototype._read = function() {
      this.case5Field0 = this._io.readU2be();
      this.case5Field1 = [];
      for (var i = 0; i < 1; i++) {
        this.case5Field1.push(new Case5Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__5_field1_entries
     */

    return Case5;
  })();

  var Case7Field1Entries = Id015PtlimaptOperation.Case7Field1Entries = (function() {
    function Case7Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case7Field1Entries.prototype._read = function() {
      this.case7Field1EltField0 = this._io.readU1();
      this.case7Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case7Field1Entries;
  })();

  var Case162Entries = Id015PtlimaptOperation.Case162Entries = (function() {
    function Case162Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case162Entries.prototype._read = function() {
      this.case162EltField0 = new Case162EltField00(this._io, this, this._root);
      this.case162EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case162Entries;
  })();

  var Case193 = Id015PtlimaptOperation.Case193 = (function() {
    function Case193(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case193.prototype._read = function() {
      this.case193 = this._io.readBytesFull();
    }

    return Case193;
  })();

  var Case38 = Id015PtlimaptOperation.Case38 = (function() {
    function Case38(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case38.prototype._read = function() {
      this.case38Field0 = this._io.readS4be();
      this.case38Field1 = [];
      for (var i = 0; i < 9; i++) {
        this.case38Field1.push(new Case38Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__38_field1_entries
     */

    return Case38;
  })();

  var Id015PtlimaptOperationAlphaContents = Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContents = (function() {
    function Id015PtlimaptOperationAlphaContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptOperationAlphaContents.prototype._read = function() {
      this.id015PtlimaptOperationAlphaContentsTag = this._io.readU1();
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.ENDORSEMENT) {
        this.endorsement = new Endorsement(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.PREENDORSEMENT) {
        this.preendorsement = new Preendorsement(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.DAL_SLOT_AVAILABILITY) {
        this.dalSlotAvailability = new DalSlotAvailability(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.SEED_NONCE_REVELATION) {
        this.seedNonceRevelation = new SeedNonceRevelation(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.VDF_REVELATION) {
        this.vdfRevelation = new Solution(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.DOUBLE_ENDORSEMENT_EVIDENCE) {
        this.doubleEndorsementEvidence = new DoubleEndorsementEvidence(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.DOUBLE_PREENDORSEMENT_EVIDENCE) {
        this.doublePreendorsementEvidence = new DoublePreendorsementEvidence(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.DOUBLE_BAKING_EVIDENCE) {
        this.doubleBakingEvidence = new DoubleBakingEvidence(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.ACTIVATE_ACCOUNT) {
        this.activateAccount = new ActivateAccount(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.PROPOSALS) {
        this.proposals = new Proposals1(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.BALLOT) {
        this.ballot = new Ballot(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.REVEAL) {
        this.reveal = new Reveal(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.SET_DEPOSITS_LIMIT) {
        this.setDepositsLimit = new SetDepositsLimit(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.INCREASE_PAID_STORAGE) {
        this.increasePaidStorage = new IncreasePaidStorage(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.UPDATE_CONSENSUS_KEY) {
        this.updateConsensusKey = new UpdateConsensusKey(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.DRAIN_DELEGATE) {
        this.drainDelegate = new DrainDelegate(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.FAILING_NOOP) {
        this.failingNoop = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.REGISTER_GLOBAL_CONSTANT) {
        this.registerGlobalConstant = new RegisterGlobalConstant(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.TX_ROLLUP_ORIGINATION) {
        this.txRollupOrigination = new TxRollupOrigination(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.TX_ROLLUP_SUBMIT_BATCH) {
        this.txRollupSubmitBatch = new TxRollupSubmitBatch(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.TX_ROLLUP_COMMIT) {
        this.txRollupCommit = new TxRollupCommit(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.TX_ROLLUP_RETURN_BOND) {
        this.txRollupReturnBond = new TxRollupReturnBond(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.TX_ROLLUP_FINALIZE_COMMITMENT) {
        this.txRollupFinalizeCommitment = new TxRollupFinalizeCommitment(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.TX_ROLLUP_REMOVE_COMMITMENT) {
        this.txRollupRemoveCommitment = new TxRollupRemoveCommitment(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.TX_ROLLUP_REJECTION) {
        this.txRollupRejection = new TxRollupRejection(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.TX_ROLLUP_DISPATCH_TICKETS) {
        this.txRollupDispatchTickets = new TxRollupDispatchTickets(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.TRANSFER_TICKET) {
        this.transferTicket = new TransferTicket(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.DAL_PUBLISH_SLOT_HEADER) {
        this.dalPublishSlotHeader = new DalPublishSlotHeader(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.SC_ROLLUP_ORIGINATE) {
        this.scRollupOriginate = new ScRollupOriginate(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.SC_ROLLUP_ADD_MESSAGES) {
        this.scRollupAddMessages = new ScRollupAddMessages(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.SC_ROLLUP_CEMENT) {
        this.scRollupCement = new ScRollupCement(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.SC_ROLLUP_PUBLISH) {
        this.scRollupPublish = new ScRollupPublish(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.SC_ROLLUP_REFUTE) {
        this.scRollupRefute = new ScRollupRefute(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.SC_ROLLUP_TIMEOUT) {
        this.scRollupTimeout = new ScRollupTimeout(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.SC_ROLLUP_EXECUTE_OUTBOX_MESSAGE) {
        this.scRollupExecuteOutboxMessage = new ScRollupExecuteOutboxMessage(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.SC_ROLLUP_RECOVER_BOND) {
        this.scRollupRecoverBond = new ScRollupRecoverBond(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.SC_ROLLUP_DAL_SLOT_SUBSCRIBE) {
        this.scRollupDalSlotSubscribe = new ScRollupDalSlotSubscribe(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.ZK_ROLLUP_ORIGINATION) {
        this.zkRollupOrigination = new ZkRollupOrigination(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationAlphaContentsTag == Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsTag.ZK_ROLLUP_PUBLISH) {
        this.zkRollupPublish = new ZkRollupPublish(this._io, this, this._root);
      }
    }

    return Id015PtlimaptOperationAlphaContents;
  })();

  var Case58Field1Entries = Id015PtlimaptOperation.Case58Field1Entries = (function() {
    function Case58Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case58Field1Entries.prototype._read = function() {
      this.case58Field1EltField0 = this._io.readU1();
      this.case58Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case58Field1Entries;
  })();

  var Refutation = Id015PtlimaptOperation.Refutation = (function() {
    function Refutation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Refutation.prototype._read = function() {
      this.choice = new N(this._io, this, this._root);
      this.step = new Step(this._io, this, this._root);
    }

    return Refutation;
  })();

  var ScRollupDalSlotSubscribe = Id015PtlimaptOperation.ScRollupDalSlotSubscribe = (function() {
    function ScRollupDalSlotSubscribe(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupDalSlotSubscribe.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptRollupAddress(this._io, this, this._root);
      this.slotIndex = this._io.readU1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupDalSlotSubscribe;
  })();

  var Case166Entries = Id015PtlimaptOperation.Case166Entries = (function() {
    function Case166Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case166Entries.prototype._read = function() {
      this.case166EltField0 = new Case166EltField00(this._io, this, this._root);
      this.case166EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case166Entries;
  })();

  var Case188EltField00 = Id015PtlimaptOperation.Case188EltField00 = (function() {
    function Case188EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case188EltField00.prototype._read = function() {
      this.lenCase188EltField0 = this._io.readU1();
      if (!(this.lenCase188EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase188EltField0, this._io, "/types/case__188_elt_field0_0/seq/0");
      }
      this._raw_case188EltField0 = this._io.readBytes(this.lenCase188EltField0);
      var _io__raw_case188EltField0 = new KaitaiStream(this._raw_case188EltField0);
      this.case188EltField0 = new Case188EltField0(_io__raw_case188EltField0, this, this._root);
    }

    return Case188EltField00;
  })();

  var ScRollupOriginate = Id015PtlimaptOperation.ScRollupOriginate = (function() {
    function ScRollupOriginate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupOriginate.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.pvmKind = this._io.readU1();
      this.bootSector = new BytesDynUint30(this._io, this, this._root);
      this.originationProof = new BytesDynUint30(this._io, this, this._root);
      this.parametersTy = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return ScRollupOriginate;
  })();

  var Case62 = Id015PtlimaptOperation.Case62 = (function() {
    function Case62(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case62.prototype._read = function() {
      this.case62Field0 = this._io.readS4be();
      this.case62Field1 = new Case62Field10(this._io, this, this._root);
    }

    return Case62;
  })();

  var Case40 = Id015PtlimaptOperation.Case40 = (function() {
    function Case40(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case40.prototype._read = function() {
      this.case4Field0 = this._io.readU1();
      this.case4Field1 = [];
      for (var i = 0; i < 1; i++) {
        this.case4Field1.push(new Case4Field1Entries0(this._io, this, this._root));
      }
    }

    /**
     * case__4_field1_entries
     */

    return Case40;
  })();

  var SeedNonceRevelation = Id015PtlimaptOperation.SeedNonceRevelation = (function() {
    function SeedNonceRevelation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SeedNonceRevelation.prototype._read = function() {
      this.level = this._io.readS4be();
      this.nonce = this._io.readBytes(32);
    }

    return SeedNonceRevelation;
  })();

  var Case27 = Id015PtlimaptOperation.Case27 = (function() {
    function Case27(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case27.prototype._read = function() {
      this.case27Field0 = this._io.readS8be();
      this.case27Field1 = [];
      for (var i = 0; i < 6; i++) {
        this.case27Field1.push(new Case27Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__27_field1_entries
     */

    return Case27;
  })();

  var Case136EltField00 = Id015PtlimaptOperation.Case136EltField00 = (function() {
    function Case136EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case136EltField00.prototype._read = function() {
      this.lenCase136EltField0 = this._io.readU1();
      if (!(this.lenCase136EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase136EltField0, this._io, "/types/case__136_elt_field0_0/seq/0");
      }
      this._raw_case136EltField0 = this._io.readBytes(this.lenCase136EltField0);
      var _io__raw_case136EltField0 = new KaitaiStream(this._raw_case136EltField0);
      this.case136EltField0 = new Case136EltField0(_io__raw_case136EltField0, this, this._root);
    }

    return Case136EltField00;
  })();

  var Case208Field10 = Id015PtlimaptOperation.Case208Field10 = (function() {
    function Case208Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case208Field10.prototype._read = function() {
      this.lenCase208Field1 = this._io.readU1();
      if (!(this.lenCase208Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase208Field1, this._io, "/types/case__208_field1_0/seq/0");
      }
      this._raw_case208Field1 = this._io.readBytes(this.lenCase208Field1);
      var _io__raw_case208Field1 = new KaitaiStream(this._raw_case208Field1);
      this.case208Field1 = new Case208Field1(_io__raw_case208Field1, this, this._root);
    }

    return Case208Field10;
  })();

  var Preendorsement = Id015PtlimaptOperation.Preendorsement = (function() {
    function Preendorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Preendorsement.prototype._read = function() {
      this.slot = this._io.readU2be();
      this.level = this._io.readS4be();
      this.round = this._io.readS4be();
      this.blockPayloadHash = this._io.readBytes(32);
    }

    return Preendorsement;
  })();

  var Case142EltField00 = Id015PtlimaptOperation.Case142EltField00 = (function() {
    function Case142EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case142EltField00.prototype._read = function() {
      this.lenCase142EltField0 = this._io.readU1();
      if (!(this.lenCase142EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase142EltField0, this._io, "/types/case__142_elt_field0_0/seq/0");
      }
      this._raw_case142EltField0 = this._io.readBytes(this.lenCase142EltField0);
      var _io__raw_case142EltField0 = new KaitaiStream(this._raw_case142EltField0);
      this.case142EltField0 = new Case142EltField0(_io__raw_case142EltField0, this, this._root);
    }

    return Case142EltField00;
  })();

  var Case179EltField00 = Id015PtlimaptOperation.Case179EltField00 = (function() {
    function Case179EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case179EltField00.prototype._read = function() {
      this.lenCase179EltField0 = this._io.readU1();
      if (!(this.lenCase179EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase179EltField0, this._io, "/types/case__179_elt_field0_0/seq/0");
      }
      this._raw_case179EltField0 = this._io.readBytes(this.lenCase179EltField0);
      var _io__raw_case179EltField0 = new KaitaiStream(this._raw_case179EltField0);
      this.case179EltField0 = new Case179EltField0(_io__raw_case179EltField0, this, this._root);
    }

    return Case179EltField00;
  })();

  var Case143EltField0 = Id015PtlimaptOperation.Case143EltField0 = (function() {
    function Case143EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case143EltField0.prototype._read = function() {
      this.case143EltField0 = this._io.readBytesFull();
    }

    return Case143EltField0;
  })();

  var Case135EltField00 = Id015PtlimaptOperation.Case135EltField00 = (function() {
    function Case135EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case135EltField00.prototype._read = function() {
      this.lenCase135EltField0 = this._io.readU1();
      if (!(this.lenCase135EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase135EltField0, this._io, "/types/case__135_elt_field0_0/seq/0");
      }
      this._raw_case135EltField0 = this._io.readBytes(this.lenCase135EltField0);
      var _io__raw_case135EltField0 = new KaitaiStream(this._raw_case135EltField0);
      this.case135EltField0 = new Case135EltField0(_io__raw_case135EltField0, this, this._root);
    }

    return Case135EltField00;
  })();

  var PrimNoArgsSomeAnnots = Id015PtlimaptOperation.PrimNoArgsSomeAnnots = (function() {
    function PrimNoArgsSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrimNoArgsSomeAnnots.prototype._read = function() {
      this.prim = new Id015PtlimaptMichelsonV1Primitives(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return PrimNoArgsSomeAnnots;
  })();

  var Case49 = Id015PtlimaptOperation.Case49 = (function() {
    function Case49(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case49.prototype._read = function() {
      this.case49Field0 = this._io.readU2be();
      this.case49Field1 = [];
      for (var i = 0; i < 12; i++) {
        this.case49Field1.push(new Case49Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__49_field1_entries
     */

    return Case49;
  })();

  var Case11 = Id015PtlimaptOperation.Case11 = (function() {
    function Case11(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case11.prototype._read = function() {
      this.case11Field0 = this._io.readS8be();
      this.case11Field1 = [];
      for (var i = 0; i < 2; i++) {
        this.case11Field1.push(new Case11Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__11_field1_entries
     */

    return Case11;
  })();

  var Case153EltField00 = Id015PtlimaptOperation.Case153EltField00 = (function() {
    function Case153EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case153EltField00.prototype._read = function() {
      this.lenCase153EltField0 = this._io.readU1();
      if (!(this.lenCase153EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase153EltField0, this._io, "/types/case__153_elt_field0_0/seq/0");
      }
      this._raw_case153EltField0 = this._io.readBytes(this.lenCase153EltField0);
      var _io__raw_case153EltField0 = new KaitaiStream(this._raw_case153EltField0);
      this.case153EltField0 = new Case153EltField0(_io__raw_case153EltField0, this, this._root);
    }

    return Case153EltField00;
  })();

  var Case178Entries = Id015PtlimaptOperation.Case178Entries = (function() {
    function Case178Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case178Entries.prototype._read = function() {
      this.case178EltField0 = new Case178EltField00(this._io, this, this._root);
      this.case178EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case178Entries;
  })();

  var Case184EltField0 = Id015PtlimaptOperation.Case184EltField0 = (function() {
    function Case184EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case184EltField0.prototype._read = function() {
      this.case184EltField0 = this._io.readBytesFull();
    }

    return Case184EltField0;
  })();

  var Case16 = Id015PtlimaptOperation.Case16 = (function() {
    function Case16(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case16.prototype._read = function() {
      this.case16Field0 = this._io.readU1();
      this.case16Field1 = [];
      for (var i = 0; i < 4; i++) {
        this.case16Field1.push(new Case16Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__16_field1_entries
     */

    return Case16;
  })();

  var Case162EltField00 = Id015PtlimaptOperation.Case162EltField00 = (function() {
    function Case162EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case162EltField00.prototype._read = function() {
      this.lenCase162EltField0 = this._io.readU1();
      if (!(this.lenCase162EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase162EltField0, this._io, "/types/case__162_elt_field0_0/seq/0");
      }
      this._raw_case162EltField0 = this._io.readBytes(this.lenCase162EltField0);
      var _io__raw_case162EltField0 = new KaitaiStream(this._raw_case162EltField0);
      this.case162EltField0 = new Case162EltField0(_io__raw_case162EltField0, this, this._root);
    }

    return Case162EltField00;
  })();

  var Case5Field1Entries = Id015PtlimaptOperation.Case5Field1Entries = (function() {
    function Case5Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case5Field1Entries.prototype._read = function() {
      this.case5Field1EltField0 = this._io.readU1();
      this.case5Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case5Field1Entries;
  })();

  var Case190EltField0 = Id015PtlimaptOperation.Case190EltField0 = (function() {
    function Case190EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case190EltField0.prototype._read = function() {
      this.case190EltField0 = this._io.readBytesFull();
    }

    return Case190EltField0;
  })();

  var Case163EltField0 = Id015PtlimaptOperation.Case163EltField0 = (function() {
    function Case163EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case163EltField0.prototype._read = function() {
      this.case163EltField0 = this._io.readBytesFull();
    }

    return Case163EltField0;
  })();

  var Case187EltField00 = Id015PtlimaptOperation.Case187EltField00 = (function() {
    function Case187EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case187EltField00.prototype._read = function() {
      this.lenCase187EltField0 = this._io.readU1();
      if (!(this.lenCase187EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase187EltField0, this._io, "/types/case__187_elt_field0_0/seq/0");
      }
      this._raw_case187EltField0 = this._io.readBytes(this.lenCase187EltField0);
      var _io__raw_case187EltField0 = new KaitaiStream(this._raw_case187EltField0);
      this.case187EltField0 = new Case187EltField0(_io__raw_case187EltField0, this, this._root);
    }

    return Case187EltField00;
  })();

  var Case167EltField0 = Id015PtlimaptOperation.Case167EltField0 = (function() {
    function Case167EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case167EltField0.prototype._read = function() {
      this.case167EltField0 = this._io.readBytesFull();
    }

    return Case167EltField0;
  })();

  var Case161Entries = Id015PtlimaptOperation.Case161Entries = (function() {
    function Case161Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case161Entries.prototype._read = function() {
      this.case161EltField0 = new Case161EltField00(this._io, this, this._root);
      this.case161EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case161Entries;
  })();

  var Case39Field1Entries = Id015PtlimaptOperation.Case39Field1Entries = (function() {
    function Case39Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case39Field1Entries.prototype._read = function() {
      this.case39Field1EltField0 = this._io.readU1();
      this.case39Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case39Field1Entries;
  })();

  var Case219 = Id015PtlimaptOperation.Case219 = (function() {
    function Case219(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case219.prototype._read = function() {
      this.case219Field0 = this._io.readS8be();
      this.case219Field1 = new Case219Field10(this._io, this, this._root);
      this.case219Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case219;
  })();

  var Case50Field1Entries = Id015PtlimaptOperation.Case50Field1Entries = (function() {
    function Case50Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case50Field1Entries.prototype._read = function() {
      this.case50Field1EltField0 = this._io.readU1();
      this.case50Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case50Field1Entries;
  })();

  var Case157EltField0 = Id015PtlimaptOperation.Case157EltField0 = (function() {
    function Case157EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case157EltField0.prototype._read = function() {
      this.case157EltField0 = this._io.readBytesFull();
    }

    return Case157EltField0;
  })();

  var Case11Field1Entries = Id015PtlimaptOperation.Case11Field1Entries = (function() {
    function Case11Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case11Field1Entries.prototype._read = function() {
      this.case11Field1EltField0 = this._io.readU1();
      this.case11Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case11Field1Entries;
  })();

  var Id015PtlimaptContractId = Id015PtlimaptOperation.Id015PtlimaptContractId = (function() {
    function Id015PtlimaptContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptContractId.prototype._read = function() {
      this.id015PtlimaptContractIdTag = this._io.readU1();
      if (this.id015PtlimaptContractIdTag == Id015PtlimaptOperation.Id015PtlimaptContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id015PtlimaptContractIdTag == Id015PtlimaptOperation.Id015PtlimaptContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id015PtlimaptContractId;
  })();

  var Case182EltField00 = Id015PtlimaptOperation.Case182EltField00 = (function() {
    function Case182EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case182EltField00.prototype._read = function() {
      this.lenCase182EltField0 = this._io.readU1();
      if (!(this.lenCase182EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase182EltField0, this._io, "/types/case__182_elt_field0_0/seq/0");
      }
      this._raw_case182EltField0 = this._io.readBytes(this.lenCase182EltField0);
      var _io__raw_case182EltField0 = new KaitaiStream(this._raw_case182EltField0);
      this.case182EltField0 = new Case182EltField0(_io__raw_case182EltField0, this, this._root);
    }

    return Case182EltField00;
  })();

  var Case137EltField0 = Id015PtlimaptOperation.Case137EltField0 = (function() {
    function Case137EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case137EltField0.prototype._read = function() {
      this.case137EltField0 = this._io.readBytesFull();
    }

    return Case137EltField0;
  })();

  var Case219Field10 = Id015PtlimaptOperation.Case219Field10 = (function() {
    function Case219Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case219Field10.prototype._read = function() {
      this.lenCase219Field1 = this._io.readU1();
      if (!(this.lenCase219Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase219Field1, this._io, "/types/case__219_field1_0/seq/0");
      }
      this._raw_case219Field1 = this._io.readBytes(this.lenCase219Field1);
      var _io__raw_case219Field1 = new KaitaiStream(this._raw_case219Field1);
      this.case219Field1 = new Case219Field1(_io__raw_case219Field1, this, this._root);
    }

    return Case219Field10;
  })();

  var TxRollupCommit = Id015PtlimaptOperation.TxRollupCommit = (function() {
    function TxRollupCommit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupCommit.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptTxRollupId(this._io, this, this._root);
      this.commitment = new Commitment(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupCommit;
  })();

  var DoubleBakingEvidence = Id015PtlimaptOperation.DoubleBakingEvidence = (function() {
    function DoubleBakingEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleBakingEvidence.prototype._read = function() {
      this.bh1 = new Bh10(this._io, this, this._root);
      this.bh2 = new Bh20(this._io, this, this._root);
    }

    return DoubleBakingEvidence;
  })();

  var Case1911 = Id015PtlimaptOperation.Case1911 = (function() {
    function Case1911(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1911.prototype._read = function() {
      this.lenCase191 = this._io.readU4be();
      if (!(this.lenCase191 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase191, this._io, "/types/case__191_1/seq/0");
      }
      this._raw_case191 = this._io.readBytes(this.lenCase191);
      var _io__raw_case191 = new KaitaiStream(this._raw_case191);
      this.case191 = new Case191(_io__raw_case191, this, this._root);
    }

    return Case1911;
  })();

  var Case45 = Id015PtlimaptOperation.Case45 = (function() {
    function Case45(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case45.prototype._read = function() {
      this.case45Field0 = this._io.readU2be();
      this.case45Field1 = [];
      for (var i = 0; i < 11; i++) {
        this.case45Field1.push(new Case45Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__45_field1_entries
     */

    return Case45;
  })();

  var Case217Field10 = Id015PtlimaptOperation.Case217Field10 = (function() {
    function Case217Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case217Field10.prototype._read = function() {
      this.lenCase217Field1 = this._io.readU1();
      if (!(this.lenCase217Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase217Field1, this._io, "/types/case__217_field1_0/seq/0");
      }
      this._raw_case217Field1 = this._io.readBytes(this.lenCase217Field1);
      var _io__raw_case217Field1 = new KaitaiStream(this._raw_case217Field1);
      this.case217Field1 = new Case217Field1(_io__raw_case217Field1, this, this._root);
    }

    return Case217Field10;
  })();

  var Case129EltField00 = Id015PtlimaptOperation.Case129EltField00 = (function() {
    function Case129EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129EltField00.prototype._read = function() {
      this.lenCase129EltField0 = this._io.readU1();
      if (!(this.lenCase129EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase129EltField0, this._io, "/types/case__129_elt_field0_0/seq/0");
      }
      this._raw_case129EltField0 = this._io.readBytes(this.lenCase129EltField0);
      var _io__raw_case129EltField0 = new KaitaiStream(this._raw_case129EltField0);
      this.case129EltField0 = new Case129EltField0(_io__raw_case129EltField0, this, this._root);
    }

    return Case129EltField00;
  })();

  var Case152EltField00 = Id015PtlimaptOperation.Case152EltField00 = (function() {
    function Case152EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case152EltField00.prototype._read = function() {
      this.lenCase152EltField0 = this._io.readU1();
      if (!(this.lenCase152EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase152EltField0, this._io, "/types/case__152_elt_field0_0/seq/0");
      }
      this._raw_case152EltField0 = this._io.readBytes(this.lenCase152EltField0);
      var _io__raw_case152EltField0 = new KaitaiStream(this._raw_case152EltField0);
      this.case152EltField0 = new Case152EltField0(_io__raw_case152EltField0, this, this._root);
    }

    return Case152EltField00;
  })();

  var Case27Field1Entries = Id015PtlimaptOperation.Case27Field1Entries = (function() {
    function Case27Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case27Field1Entries.prototype._read = function() {
      this.case27Field1EltField0 = this._io.readU1();
      this.case27Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case27Field1Entries;
  })();

  var Case160Entries = Id015PtlimaptOperation.Case160Entries = (function() {
    function Case160Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case160Entries.prototype._read = function() {
      this.case160EltField0 = new Case160EltField00(this._io, this, this._root);
      this.case160EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case160Entries;
  })();

  var PublicKey = Id015PtlimaptOperation.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id015PtlimaptOperation.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id015PtlimaptOperation.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id015PtlimaptOperation.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var Case165Entries = Id015PtlimaptOperation.Case165Entries = (function() {
    function Case165Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case165Entries.prototype._read = function() {
      this.case165EltField0 = new Case165EltField00(this._io, this, this._root);
      this.case165EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case165Entries;
  })();

  var Case12 = Id015PtlimaptOperation.Case12 = (function() {
    function Case12(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case12.prototype._read = function() {
      this.case12Field0 = this._io.readU1();
      this.case12Field1 = [];
      for (var i = 0; i < 3; i++) {
        this.case12Field1.push(new Case12Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__12_field1_entries
     */

    return Case12;
  })();

  var TxRollupDispatchTickets = Id015PtlimaptOperation.TxRollupDispatchTickets = (function() {
    function TxRollupDispatchTickets(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupDispatchTickets.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.txRollup = new Id015PtlimaptTxRollupId(this._io, this, this._root);
      this.level = this._io.readS4be();
      this.contextHash = this._io.readBytes(32);
      this.messageIndex = new Int31(this._io, this, this._root);
      this.messageResultPath = new MessageResultPath0(this._io, this, this._root);
      this.ticketsInfo = new TicketsInfo0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupDispatchTickets;
  })();

  var Case210Field10 = Id015PtlimaptOperation.Case210Field10 = (function() {
    function Case210Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case210Field10.prototype._read = function() {
      this.lenCase210Field1 = this._io.readU1();
      if (!(this.lenCase210Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase210Field1, this._io, "/types/case__210_field1_0/seq/0");
      }
      this._raw_case210Field1 = this._io.readBytes(this.lenCase210Field1);
      var _io__raw_case210Field1 = new KaitaiStream(this._raw_case210Field1);
      this.case210Field1 = new Case210Field1(_io__raw_case210Field1, this, this._root);
    }

    return Case210Field10;
  })();

  var Bh2 = Id015PtlimaptOperation.Bh2 = (function() {
    function Bh2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh2.prototype._read = function() {
      this.id015PtlimaptBlockHeaderAlphaFullHeader = new Id015PtlimaptBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh2;
  })();

  var Case139EltField00 = Id015PtlimaptOperation.Case139EltField00 = (function() {
    function Case139EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case139EltField00.prototype._read = function() {
      this.lenCase139EltField0 = this._io.readU1();
      if (!(this.lenCase139EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase139EltField0, this._io, "/types/case__139_elt_field0_0/seq/0");
      }
      this._raw_case139EltField0 = this._io.readBytes(this.lenCase139EltField0);
      var _io__raw_case139EltField0 = new KaitaiStream(this._raw_case139EltField0);
      this.case139EltField0 = new Case139EltField0(_io__raw_case139EltField0, this, this._root);
    }

    return Case139EltField00;
  })();

  var PreviousMessageResultPathEntries = Id015PtlimaptOperation.PreviousMessageResultPathEntries = (function() {
    function PreviousMessageResultPathEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResultPathEntries.prototype._read = function() {
      this.messageResultListHash = this._io.readBytes(32);
    }

    return PreviousMessageResultPathEntries;
  })();

  var Case36Field1Entries = Id015PtlimaptOperation.Case36Field1Entries = (function() {
    function Case36Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case36Field1Entries.prototype._read = function() {
      this.case36Field1EltField0 = this._io.readU1();
      this.case36Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case36Field1Entries;
  })();

  var Case141EltField00 = Id015PtlimaptOperation.Case141EltField00 = (function() {
    function Case141EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case141EltField00.prototype._read = function() {
      this.lenCase141EltField0 = this._io.readU1();
      if (!(this.lenCase141EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase141EltField0, this._io, "/types/case__141_elt_field0_0/seq/0");
      }
      this._raw_case141EltField0 = this._io.readBytes(this.lenCase141EltField0);
      var _io__raw_case141EltField0 = new KaitaiStream(this._raw_case141EltField0);
      this.case141EltField0 = new Case141EltField0(_io__raw_case141EltField0, this, this._root);
    }

    return Case141EltField00;
  })();

  var Case157EltField00 = Id015PtlimaptOperation.Case157EltField00 = (function() {
    function Case157EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case157EltField00.prototype._read = function() {
      this.lenCase157EltField0 = this._io.readU1();
      if (!(this.lenCase157EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase157EltField0, this._io, "/types/case__157_elt_field0_0/seq/0");
      }
      this._raw_case157EltField0 = this._io.readBytes(this.lenCase157EltField0);
      var _io__raw_case157EltField0 = new KaitaiStream(this._raw_case157EltField0);
      this.case157EltField0 = new Case157EltField0(_io__raw_case157EltField0, this, this._root);
    }

    return Case157EltField00;
  })();

  var Case147EltField00 = Id015PtlimaptOperation.Case147EltField00 = (function() {
    function Case147EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case147EltField00.prototype._read = function() {
      this.lenCase147EltField0 = this._io.readU1();
      if (!(this.lenCase147EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase147EltField0, this._io, "/types/case__147_elt_field0_0/seq/0");
      }
      this._raw_case147EltField0 = this._io.readBytes(this.lenCase147EltField0);
      var _io__raw_case147EltField0 = new KaitaiStream(this._raw_case147EltField0);
      this.case147EltField0 = new Case147EltField0(_io__raw_case147EltField0, this, this._root);
    }

    return Case147EltField00;
  })();

  var Id015PtlimaptMutez = Id015PtlimaptOperation.Id015PtlimaptMutez = (function() {
    function Id015PtlimaptMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptMutez.prototype._read = function() {
      this.id015PtlimaptMutez = new N(this._io, this, this._root);
    }

    return Id015PtlimaptMutez;
  })();

  var TreeEncoding = Id015PtlimaptOperation.TreeEncoding = (function() {
    function TreeEncoding(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TreeEncoding.prototype._read = function() {
      this.treeEncodingTag = this._io.readU1();
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__4) {
        this.case4 = new Case40(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__8) {
        this.case8 = new Case8(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__12) {
        this.case12 = new Case12(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__16) {
        this.case16 = new Case16(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__20) {
        this.case20 = new Case20(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__24) {
        this.case24 = new Case24(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__28) {
        this.case28 = new Case28(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__32) {
        this.case32 = new Case32(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__36) {
        this.case36 = new Case36(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__40) {
        this.case40 = new Case40(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__44) {
        this.case44 = new Case44(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__48) {
        this.case48 = new Case48(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__52) {
        this.case52 = new Case52(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__56) {
        this.case56 = new Case56(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__60) {
        this.case60 = new Case60(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__64) {
        this.case64 = new Case64(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__5) {
        this.case5 = new Case5(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__9) {
        this.case9 = new Case9(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__13) {
        this.case13 = new Case13(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__17) {
        this.case17 = new Case17(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__21) {
        this.case21 = new Case21(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__25) {
        this.case25 = new Case25(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__29) {
        this.case29 = new Case29(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__33) {
        this.case33 = new Case33(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__37) {
        this.case37 = new Case37(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__41) {
        this.case41 = new Case41(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__45) {
        this.case45 = new Case45(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__49) {
        this.case49 = new Case49(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__53) {
        this.case53 = new Case53(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__57) {
        this.case57 = new Case57(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__61) {
        this.case61 = new Case61(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__65) {
        this.case65 = new Case65(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__6) {
        this.case6 = new Case6(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__10) {
        this.case10 = new Case10(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__14) {
        this.case14 = new Case14(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__18) {
        this.case18 = new Case18(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__22) {
        this.case22 = new Case22(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__26) {
        this.case26 = new Case26(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__30) {
        this.case30 = new Case30(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__34) {
        this.case34 = new Case34(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__38) {
        this.case38 = new Case38(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__42) {
        this.case42 = new Case42(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__46) {
        this.case46 = new Case46(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__50) {
        this.case50 = new Case50(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__54) {
        this.case54 = new Case54(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__58) {
        this.case58 = new Case58(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__62) {
        this.case62 = new Case62(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__66) {
        this.case66 = new Case66(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__7) {
        this.case7 = new Case7(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__11) {
        this.case11 = new Case11(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__15) {
        this.case15 = new Case15(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__19) {
        this.case19 = new Case19(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__23) {
        this.case23 = new Case23(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__27) {
        this.case27 = new Case27(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__31) {
        this.case31 = new Case31(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__35) {
        this.case35 = new Case35(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__39) {
        this.case39 = new Case39(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__43) {
        this.case43 = new Case43(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__47) {
        this.case47 = new Case47(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__51) {
        this.case51 = new Case51(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__55) {
        this.case55 = new Case55(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__59) {
        this.case59 = new Case59(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__63) {
        this.case63 = new Case63(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__67) {
        this.case67 = new Case67(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__129) {
        this.case129 = new Case129Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__130) {
        this.case130 = new Case130Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__131) {
        this.case131 = new Case131Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__132) {
        this.case132 = new Case132Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__133) {
        this.case133 = new Case133Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__134) {
        this.case134 = new Case134Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__135) {
        this.case135 = new Case135Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__136) {
        this.case136 = new Case136Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__137) {
        this.case137 = new Case137Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__138) {
        this.case138 = new Case138Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__139) {
        this.case139 = new Case139Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__140) {
        this.case140 = new Case140Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__141) {
        this.case141 = new Case141Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__142) {
        this.case142 = new Case142Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__143) {
        this.case143 = new Case143Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__144) {
        this.case144 = new Case144Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__145) {
        this.case145 = new Case145Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__146) {
        this.case146 = new Case146Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__147) {
        this.case147 = new Case147Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__148) {
        this.case148 = new Case148Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__149) {
        this.case149 = new Case149Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__150) {
        this.case150 = new Case150Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__151) {
        this.case151 = new Case151Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__152) {
        this.case152 = new Case152Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__153) {
        this.case153 = new Case153Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__154) {
        this.case154 = new Case154Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__155) {
        this.case155 = new Case155Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__156) {
        this.case156 = new Case156Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__157) {
        this.case157 = new Case157Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__158) {
        this.case158 = new Case158Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__159) {
        this.case159 = new Case159Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__160) {
        this.case160 = new Case160Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__161) {
        this.case161 = new Case161Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__162) {
        this.case162 = new Case162Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__163) {
        this.case163 = new Case163Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__164) {
        this.case164 = new Case164Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__165) {
        this.case165 = new Case165Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__166) {
        this.case166 = new Case166Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__167) {
        this.case167 = new Case167Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__168) {
        this.case168 = new Case168Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__169) {
        this.case169 = new Case169Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__170) {
        this.case170 = new Case170Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__171) {
        this.case171 = new Case171Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__172) {
        this.case172 = new Case172Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__173) {
        this.case173 = new Case173Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__174) {
        this.case174 = new Case174Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__175) {
        this.case175 = new Case175Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__176) {
        this.case176 = new Case176Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__177) {
        this.case177 = new Case177Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__178) {
        this.case178 = new Case178Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__179) {
        this.case179 = new Case179Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__180) {
        this.case180 = new Case180Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__181) {
        this.case181 = new Case181Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__182) {
        this.case182 = new Case182Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__183) {
        this.case183 = new Case183Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__184) {
        this.case184 = new Case184Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__185) {
        this.case185 = new Case185Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__186) {
        this.case186 = new Case186Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__187) {
        this.case187 = new Case187Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__188) {
        this.case188 = new Case188Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__189) {
        this.case189 = new Case189Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__190) {
        this.case190 = new Case190Entries(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__191) {
        this.case191 = new Case1911(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__192) {
        this.case192 = new Case1920(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__193) {
        this.case193 = new Case1930(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__195) {
        this.case195 = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__200) {
        this.case200 = this._io.readBytes(32);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__208) {
        this.case208 = this._io.readBytes(32);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__216) {
        this.case216 = new Case216(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__217) {
        this.case217 = new Case217(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__218) {
        this.case218 = new Case218(this._io, this, this._root);
      }
      if (this.treeEncodingTag == Id015PtlimaptOperation.TreeEncodingTag.CASE__219) {
        this.case219 = new Case219(this._io, this, this._root);
      }
    }

    return TreeEncoding;
  })();

  var PvmStep = Id015PtlimaptOperation.PvmStep = (function() {
    function PvmStep(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PvmStep.prototype._read = function() {
      this.pvmStepTag = this._io.readU1();
      if (this.pvmStepTag == Id015PtlimaptOperation.PvmStepTag.ARITHMETIC__PVM__WITH__PROOF) {
        this.arithmeticPvmWithProof = new Proof(this._io, this, this._root);
      }
      if (this.pvmStepTag == Id015PtlimaptOperation.PvmStepTag.WASM__2__0__0__PVM__WITH__PROOF) {
        this.wasm200PvmWithProof = new Proof0(this._io, this, this._root);
      }
    }

    return PvmStep;
  })();

  var Case208 = Id015PtlimaptOperation.Case208 = (function() {
    function Case208(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case208.prototype._read = function() {
      this.case208Field0 = this._io.readU1();
      this.case208Field1 = new Case208Field10(this._io, this, this._root);
      this.case208Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case208;
  })();

  var Case49Field1Entries = Id015PtlimaptOperation.Case49Field1Entries = (function() {
    function Case49Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case49Field1Entries.prototype._read = function() {
      this.case49Field1EltField0 = this._io.readU1();
      this.case49Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case49Field1Entries;
  })();

  var ScRollupCement = Id015PtlimaptOperation.ScRollupCement = (function() {
    function ScRollupCement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupCement.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptRollupAddress(this._io, this, this._root);
      this.commitment = this._io.readBytes(32);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupCement;
  })();

  var Named = Id015PtlimaptOperation.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Case30 = Id015PtlimaptOperation.Case30 = (function() {
    function Case30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case30.prototype._read = function() {
      this.case30Field0 = this._io.readS4be();
      this.case30Field1 = [];
      for (var i = 0; i < 7; i++) {
        this.case30Field1.push(new Case30Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__30_field1_entries
     */

    return Case30;
  })();

  var Case67 = Id015PtlimaptOperation.Case67 = (function() {
    function Case67(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case67.prototype._read = function() {
      this.case67Field0 = this._io.readS8be();
      this.case67Field1 = [];
      for (var i = 0; i < 32; i++) {
        this.case67Field1.push(new Case67Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__67_field1_entries
     */

    return Case67;
  })();

  var Case143Entries = Id015PtlimaptOperation.Case143Entries = (function() {
    function Case143Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case143Entries.prototype._read = function() {
      this.case143EltField0 = new Case143EltField00(this._io, this, this._root);
      this.case143EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case143Entries;
  })();

  var Amount = Id015PtlimaptOperation.Amount = (function() {
    function Amount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Amount.prototype._read = function() {
      this.amountTag = this._io.readU1();
      if (this.amountTag == Id015PtlimaptOperation.AmountTag.CASE__0) {
        this.case0 = this._io.readU1();
      }
      if (this.amountTag == Id015PtlimaptOperation.AmountTag.CASE__1) {
        this.case1 = this._io.readU2be();
      }
      if (this.amountTag == Id015PtlimaptOperation.AmountTag.CASE__2) {
        this.case2 = this._io.readS4be();
      }
      if (this.amountTag == Id015PtlimaptOperation.AmountTag.CASE__3) {
        this.case3 = this._io.readS8be();
      }
    }

    return Amount;
  })();

  var Id015PtlimaptOperationAlphaContentsAndSignature = Id015PtlimaptOperation.Id015PtlimaptOperationAlphaContentsAndSignature = (function() {
    function Id015PtlimaptOperationAlphaContentsAndSignature(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptOperationAlphaContentsAndSignature.prototype._read = function() {
      this.contents = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.contents.push(new ContentsEntries(this._io, this, this._root));
        i++;
      }
      this.signature = this._io.readBytes(64);
    }

    return Id015PtlimaptOperationAlphaContentsAndSignature;
  })();

  var Case170Entries = Id015PtlimaptOperation.Case170Entries = (function() {
    function Case170Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case170Entries.prototype._read = function() {
      this.case170EltField0 = new Case170EltField00(this._io, this, this._root);
      this.case170EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case170Entries;
  })();

  var PrimGeneric = Id015PtlimaptOperation.PrimGeneric = (function() {
    function PrimGeneric(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrimGeneric.prototype._read = function() {
      this.prim = new Id015PtlimaptMichelsonV1Primitives(this._io, this, this._root);
      this.args = new Args0(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return PrimGeneric;
  })();

  var Case41Field1Entries = Id015PtlimaptOperation.Case41Field1Entries = (function() {
    function Case41Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case41Field1Entries.prototype._read = function() {
      this.case41Field1EltField0 = this._io.readU1();
      this.case41Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case41Field1Entries;
  })();

  var Case185Entries = Id015PtlimaptOperation.Case185Entries = (function() {
    function Case185Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case185Entries.prototype._read = function() {
      this.case185EltField0 = new Case185EltField00(this._io, this, this._root);
      this.case185EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case185Entries;
  })();

  var Case158EltField00 = Id015PtlimaptOperation.Case158EltField00 = (function() {
    function Case158EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case158EltField00.prototype._read = function() {
      this.lenCase158EltField0 = this._io.readU1();
      if (!(this.lenCase158EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase158EltField0, this._io, "/types/case__158_elt_field0_0/seq/0");
      }
      this._raw_case158EltField0 = this._io.readBytes(this.lenCase158EltField0);
      var _io__raw_case158EltField0 = new KaitaiStream(this._raw_case158EltField0);
      this.case158EltField0 = new Case158EltField0(_io__raw_case158EltField0, this, this._root);
    }

    return Case158EltField00;
  })();

  var Case189EltField00 = Id015PtlimaptOperation.Case189EltField00 = (function() {
    function Case189EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case189EltField00.prototype._read = function() {
      this.lenCase189EltField0 = this._io.readU1();
      if (!(this.lenCase189EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase189EltField0, this._io, "/types/case__189_elt_field0_0/seq/0");
      }
      this._raw_case189EltField0 = this._io.readBytes(this.lenCase189EltField0);
      var _io__raw_case189EltField0 = new KaitaiStream(this._raw_case189EltField0);
      this.case189EltField0 = new Case189EltField0(_io__raw_case189EltField0, this, this._root);
    }

    return Case189EltField00;
  })();

  var Case175EltField0 = Id015PtlimaptOperation.Case175EltField0 = (function() {
    function Case175EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case175EltField0.prototype._read = function() {
      this.case175EltField0 = this._io.readBytesFull();
    }

    return Case175EltField0;
  })();

  var Bh20 = Id015PtlimaptOperation.Bh20 = (function() {
    function Bh20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh20.prototype._read = function() {
      this.lenBh2 = this._io.readU4be();
      if (!(this.lenBh2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh2, this._io, "/types/bh2_0/seq/0");
      }
      this._raw_bh2 = this._io.readBytes(this.lenBh2);
      var _io__raw_bh2 = new KaitaiStream(this._raw_bh2);
      this.bh2 = new Bh2(_io__raw_bh2, this, this._root);
    }

    return Bh20;
  })();

  var Case151Entries = Id015PtlimaptOperation.Case151Entries = (function() {
    function Case151Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case151Entries.prototype._read = function() {
      this.case151EltField0 = new Case151EltField00(this._io, this, this._root);
      this.case151EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case151Entries;
  })();

  var Case173Entries = Id015PtlimaptOperation.Case173Entries = (function() {
    function Case173Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case173Entries.prototype._read = function() {
      this.case173EltField0 = new Case173EltField00(this._io, this, this._root);
      this.case173EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case173Entries;
  })();

  var Case190EltField00 = Id015PtlimaptOperation.Case190EltField00 = (function() {
    function Case190EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case190EltField00.prototype._read = function() {
      this.lenCase190EltField0 = this._io.readU1();
      if (!(this.lenCase190EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase190EltField0, this._io, "/types/case__190_elt_field0_0/seq/0");
      }
      this._raw_case190EltField0 = this._io.readBytes(this.lenCase190EltField0);
      var _io__raw_case190EltField0 = new KaitaiStream(this._raw_case190EltField0);
      this.case190EltField0 = new Case190EltField0(_io__raw_case190EltField0, this, this._root);
    }

    return Case190EltField00;
  })();

  var Case28Field1Entries = Id015PtlimaptOperation.Case28Field1Entries = (function() {
    function Case28Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case28Field1Entries.prototype._read = function() {
      this.case28Field1EltField0 = this._io.readU1();
      this.case28Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case28Field1Entries;
  })();

  var Commitment = Id015PtlimaptOperation.Commitment = (function() {
    function Commitment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitment.prototype._read = function() {
      this.level = this._io.readS4be();
      this.messages = new Messages0(this._io, this, this._root);
      this.predecessor = new Predecessor(this._io, this, this._root);
      this.inboxMerkleRoot = this._io.readBytes(32);
    }

    return Commitment;
  })();

  var Case2 = Id015PtlimaptOperation.Case2 = (function() {
    function Case2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case2.prototype._read = function() {
      this.case2Field0 = this._io.readS2be();
      this.case2Field1 = this._io.readBytes(32);
      this.case2Field2 = this._io.readBytes(32);
      this.case2Field3 = new TreeEncoding(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case2;
  })();

  var Case162EltField0 = Id015PtlimaptOperation.Case162EltField0 = (function() {
    function Case162EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case162EltField0.prototype._read = function() {
      this.case162EltField0 = this._io.readBytesFull();
    }

    return Case162EltField0;
  })();

  var TicketsInfoEntries = Id015PtlimaptOperation.TicketsInfoEntries = (function() {
    function TicketsInfoEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TicketsInfoEntries.prototype._read = function() {
      this.contents = new BytesDynUint30(this._io, this, this._root);
      this.ty = new BytesDynUint30(this._io, this, this._root);
      this.ticketer = new Id015PtlimaptContractId(this._io, this, this._root);
      this.amount = new Amount(this._io, this, this._root);
      this.claimer = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return TicketsInfoEntries;
  })();

  var Case184EltField00 = Id015PtlimaptOperation.Case184EltField00 = (function() {
    function Case184EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case184EltField00.prototype._read = function() {
      this.lenCase184EltField0 = this._io.readU1();
      if (!(this.lenCase184EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase184EltField0, this._io, "/types/case__184_elt_field0_0/seq/0");
      }
      this._raw_case184EltField0 = this._io.readBytes(this.lenCase184EltField0);
      var _io__raw_case184EltField0 = new KaitaiStream(this._raw_case184EltField0);
      this.case184EltField0 = new Case184EltField0(_io__raw_case184EltField0, this, this._root);
    }

    return Case184EltField00;
  })();

  var Delegation = Id015PtlimaptOperation.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id015PtlimaptOperation.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var Case56 = Id015PtlimaptOperation.Case56 = (function() {
    function Case56(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case56.prototype._read = function() {
      this.case56Field0 = this._io.readU1();
      this.case56Field1 = [];
      for (var i = 0; i < 14; i++) {
        this.case56Field1.push(new Case56Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__56_field1_entries
     */

    return Case56;
  })();

  var Case167Entries = Id015PtlimaptOperation.Case167Entries = (function() {
    function Case167Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case167Entries.prototype._read = function() {
      this.case167EltField0 = new Case167EltField00(this._io, this, this._root);
      this.case167EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case167Entries;
  })();

  var RegisterGlobalConstant = Id015PtlimaptOperation.RegisterGlobalConstant = (function() {
    function RegisterGlobalConstant(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RegisterGlobalConstant.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return RegisterGlobalConstant;
  })();

  var Case53 = Id015PtlimaptOperation.Case53 = (function() {
    function Case53(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case53.prototype._read = function() {
      this.case53Field0 = this._io.readU2be();
      this.case53Field1 = [];
      for (var i = 0; i < 13; i++) {
        this.case53Field1.push(new Case53Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__53_field1_entries
     */

    return Case53;
  })();

  var Case161EltField0 = Id015PtlimaptOperation.Case161EltField0 = (function() {
    function Case161EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case161EltField0.prototype._read = function() {
      this.case161EltField0 = this._io.readBytesFull();
    }

    return Case161EltField0;
  })();

  var Solution = Id015PtlimaptOperation.Solution = (function() {
    function Solution(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Solution.prototype._read = function() {
      this.solutionField0 = this._io.readBytes(100);
      this.solutionField1 = this._io.readBytes(100);
    }

    return Solution;
  })();

  var Case67Field1Entries = Id015PtlimaptOperation.Case67Field1Entries = (function() {
    function Case67Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case67Field1Entries.prototype._read = function() {
      this.case67Field1Elt = new InodeTree(this._io, this, this._root);
    }

    return Case67Field1Entries;
  })();

  var Case32Field1Entries = Id015PtlimaptOperation.Case32Field1Entries = (function() {
    function Case32Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case32Field1Entries.prototype._read = function() {
      this.case32Field1EltField0 = this._io.readU1();
      this.case32Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case32Field1Entries;
  })();

  var Case166EltField0 = Id015PtlimaptOperation.Case166EltField0 = (function() {
    function Case166EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case166EltField0.prototype._read = function() {
      this.case166EltField0 = this._io.readBytesFull();
    }

    return Case166EltField0;
  })();

  var Case64Field1Entries = Id015PtlimaptOperation.Case64Field1Entries = (function() {
    function Case64Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case64Field1Entries.prototype._read = function() {
      this.case64Field1Elt = new InodeTree(this._io, this, this._root);
    }

    return Case64Field1Entries;
  })();

  var Case129Entries = Id015PtlimaptOperation.Case129Entries = (function() {
    function Case129Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case129Entries.prototype._read = function() {
      this.case129EltField0 = new Case129EltField00(this._io, this, this._root);
      this.case129EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case129Entries;
  })();

  var Case19Field1Entries = Id015PtlimaptOperation.Case19Field1Entries = (function() {
    function Case19Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case19Field1Entries.prototype._read = function() {
      this.case19Field1EltField0 = this._io.readU1();
      this.case19Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case19Field1Entries;
  })();

  var Case176Entries = Id015PtlimaptOperation.Case176Entries = (function() {
    function Case176Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case176Entries.prototype._read = function() {
      this.case176EltField0 = new Case176EltField00(this._io, this, this._root);
      this.case176EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case176Entries;
  })();

  var Case186EltField00 = Id015PtlimaptOperation.Case186EltField00 = (function() {
    function Case186EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case186EltField00.prototype._read = function() {
      this.lenCase186EltField0 = this._io.readU1();
      if (!(this.lenCase186EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase186EltField0, this._io, "/types/case__186_elt_field0_0/seq/0");
      }
      this._raw_case186EltField0 = this._io.readBytes(this.lenCase186EltField0);
      var _io__raw_case186EltField0 = new KaitaiStream(this._raw_case186EltField0);
      this.case186EltField0 = new Case186EltField0(_io__raw_case186EltField0, this, this._root);
    }

    return Case186EltField00;
  })();

  var ScRollupRecoverBond = Id015PtlimaptOperation.ScRollupRecoverBond = (function() {
    function ScRollupRecoverBond(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupRecoverBond.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = this._io.readBytes(20);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return ScRollupRecoverBond;
  })();

  var Case37Field1Entries = Id015PtlimaptOperation.Case37Field1Entries = (function() {
    function Case37Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case37Field1Entries.prototype._read = function() {
      this.case37Field1EltField0 = this._io.readU1();
      this.case37Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case37Field1Entries;
  })();

  var Case176EltField0 = Id015PtlimaptOperation.Case176EltField0 = (function() {
    function Case176EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case176EltField0.prototype._read = function() {
      this.case176EltField0 = this._io.readBytesFull();
    }

    return Case176EltField0;
  })();

  var PreviousMessageResult = Id015PtlimaptOperation.PreviousMessageResult = (function() {
    function PreviousMessageResult(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResult.prototype._read = function() {
      this.contextHash = this._io.readBytes(32);
      this.withdrawListHash = this._io.readBytes(32);
    }

    return PreviousMessageResult;
  })();

  var Case20Field1Entries = Id015PtlimaptOperation.Case20Field1Entries = (function() {
    function Case20Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case20Field1Entries.prototype._read = function() {
      this.case20Field1EltField0 = this._io.readU1();
      this.case20Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case20Field1Entries;
  })();

  var PayloadEntries = Id015PtlimaptOperation.PayloadEntries = (function() {
    function PayloadEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PayloadEntries.prototype._read = function() {
      this.payloadElt = this._io.readBytes(32);
    }

    return PayloadEntries;
  })();

  var Case217 = Id015PtlimaptOperation.Case217 = (function() {
    function Case217(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case217.prototype._read = function() {
      this.case217Field0 = this._io.readU2be();
      this.case217Field1 = new Case217Field10(this._io, this, this._root);
      this.case217Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case217;
  })();

  var Case219Field1 = Id015PtlimaptOperation.Case219Field1 = (function() {
    function Case219Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case219Field1.prototype._read = function() {
      this.case219Field1 = this._io.readBytesFull();
    }

    return Case219Field1;
  })();

  var Case130EltField0 = Id015PtlimaptOperation.Case130EltField0 = (function() {
    function Case130EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130EltField0.prototype._read = function() {
      this.case130EltField0 = this._io.readBytesFull();
    }

    return Case130EltField0;
  })();

  var Sequence0 = Id015PtlimaptOperation.Sequence0 = (function() {
    function Sequence0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sequence0.prototype._read = function() {
      this.lenSequence = this._io.readU4be();
      if (!(this.lenSequence <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenSequence, this._io, "/types/sequence_0/seq/0");
      }
      this._raw_sequence = this._io.readBytes(this.lenSequence);
      var _io__raw_sequence = new KaitaiStream(this._raw_sequence);
      this.sequence = new Sequence(_io__raw_sequence, this, this._root);
    }

    return Sequence0;
  })();

  var Case179Entries = Id015PtlimaptOperation.Case179Entries = (function() {
    function Case179Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case179Entries.prototype._read = function() {
      this.case179EltField0 = new Case179EltField00(this._io, this, this._root);
      this.case179EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case179Entries;
  })();

  var Case170EltField0 = Id015PtlimaptOperation.Case170EltField0 = (function() {
    function Case170EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case170EltField0.prototype._read = function() {
      this.case170EltField0 = this._io.readBytesFull();
    }

    return Case170EltField0;
  })();

  var Case173EltField00 = Id015PtlimaptOperation.Case173EltField00 = (function() {
    function Case173EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case173EltField00.prototype._read = function() {
      this.lenCase173EltField0 = this._io.readU1();
      if (!(this.lenCase173EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase173EltField0, this._io, "/types/case__173_elt_field0_0/seq/0");
      }
      this._raw_case173EltField0 = this._io.readBytes(this.lenCase173EltField0);
      var _io__raw_case173EltField0 = new KaitaiStream(this._raw_case173EltField0);
      this.case173EltField0 = new Case173EltField0(_io__raw_case173EltField0, this, this._root);
    }

    return Case173EltField00;
  })();

  var Case6 = Id015PtlimaptOperation.Case6 = (function() {
    function Case6(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case6.prototype._read = function() {
      this.case6Field0 = this._io.readS4be();
      this.case6Field1 = [];
      for (var i = 0; i < 1; i++) {
        this.case6Field1.push(new Case6Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__6_field1_entries
     */

    return Case6;
  })();

  var Case139Entries = Id015PtlimaptOperation.Case139Entries = (function() {
    function Case139Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case139Entries.prototype._read = function() {
      this.case139EltField0 = new Case139EltField00(this._io, this, this._root);
      this.case139EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case139Entries;
  })();

  var ContentsEntries = Id015PtlimaptOperation.ContentsEntries = (function() {
    function ContentsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ContentsEntries.prototype._read = function() {
      this.id015PtlimaptOperationAlphaContents = new Id015PtlimaptOperationAlphaContents(this._io, this, this._root);
    }

    return ContentsEntries;
  })();

  var Case158EltField0 = Id015PtlimaptOperation.Case158EltField0 = (function() {
    function Case158EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case158EltField0.prototype._read = function() {
      this.case158EltField0 = this._io.readBytesFull();
    }

    return Case158EltField0;
  })();

  var Case4Field1Entries0 = Id015PtlimaptOperation.Case4Field1Entries0 = (function() {
    function Case4Field1Entries0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case4Field1Entries0.prototype._read = function() {
      this.case4Field1EltField0 = this._io.readU1();
      this.case4Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    /**
     * inode_tree
     */

    return Case4Field1Entries0;
  })();

  var PreviousMessageResultPath = Id015PtlimaptOperation.PreviousMessageResultPath = (function() {
    function PreviousMessageResultPath(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResultPath.prototype._read = function() {
      this.previousMessageResultPathEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.previousMessageResultPathEntries.push(new PreviousMessageResultPathEntries(this._io, this, this._root));
        i++;
      }
    }

    return PreviousMessageResultPath;
  })();

  var Case168Entries = Id015PtlimaptOperation.Case168Entries = (function() {
    function Case168Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case168Entries.prototype._read = function() {
      this.case168EltField0 = new Case168EltField00(this._io, this, this._root);
      this.case168EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case168Entries;
  })();

  var Case186EltField0 = Id015PtlimaptOperation.Case186EltField0 = (function() {
    function Case186EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case186EltField0.prototype._read = function() {
      this.case186EltField0 = this._io.readBytesFull();
    }

    return Case186EltField0;
  })();

  var ScRollupAddMessages = Id015PtlimaptOperation.ScRollupAddMessages = (function() {
    function ScRollupAddMessages(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupAddMessages.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptRollupAddress(this._io, this, this._root);
      this.message = new Message1(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupAddMessages;
  })();

  var Case172EltField00 = Id015PtlimaptOperation.Case172EltField00 = (function() {
    function Case172EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case172EltField00.prototype._read = function() {
      this.lenCase172EltField0 = this._io.readU1();
      if (!(this.lenCase172EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase172EltField0, this._io, "/types/case__172_elt_field0_0/seq/0");
      }
      this._raw_case172EltField0 = this._io.readBytes(this.lenCase172EltField0);
      var _io__raw_case172EltField0 = new KaitaiStream(this._raw_case172EltField0);
      this.case172EltField0 = new Case172EltField0(_io__raw_case172EltField0, this, this._root);
    }

    return Case172EltField00;
  })();

  var Case174EltField0 = Id015PtlimaptOperation.Case174EltField0 = (function() {
    function Case174EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case174EltField0.prototype._read = function() {
      this.case174EltField0 = this._io.readBytesFull();
    }

    return Case174EltField0;
  })();

  var Case38Field1Entries = Id015PtlimaptOperation.Case38Field1Entries = (function() {
    function Case38Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case38Field1Entries.prototype._read = function() {
      this.case38Field1EltField0 = this._io.readU1();
      this.case38Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case38Field1Entries;
  })();

  var Case150EltField00 = Id015PtlimaptOperation.Case150EltField00 = (function() {
    function Case150EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case150EltField00.prototype._read = function() {
      this.lenCase150EltField0 = this._io.readU1();
      if (!(this.lenCase150EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase150EltField0, this._io, "/types/case__150_elt_field0_0/seq/0");
      }
      this._raw_case150EltField0 = this._io.readBytes(this.lenCase150EltField0);
      var _io__raw_case150EltField0 = new KaitaiStream(this._raw_case150EltField0);
      this.case150EltField0 = new Case150EltField0(_io__raw_case150EltField0, this, this._root);
    }

    return Case150EltField00;
  })();

  var Case174Entries = Id015PtlimaptOperation.Case174Entries = (function() {
    function Case174Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case174Entries.prototype._read = function() {
      this.case174EltField0 = new Case174EltField00(this._io, this, this._root);
      this.case174EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case174Entries;
  })();

  var Sequence = Id015PtlimaptOperation.Sequence = (function() {
    function Sequence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sequence.prototype._read = function() {
      this.sequenceEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.sequenceEntries.push(new SequenceEntries(this._io, this, this._root));
        i++;
      }
    }

    return Sequence;
  })();

  var Case48 = Id015PtlimaptOperation.Case48 = (function() {
    function Case48(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case48.prototype._read = function() {
      this.case48Field0 = this._io.readU1();
      this.case48Field1 = [];
      for (var i = 0; i < 12; i++) {
        this.case48Field1.push(new Case48Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__48_field1_entries
     */

    return Case48;
  })();

  var PreviousMessageResultPath0 = Id015PtlimaptOperation.PreviousMessageResultPath0 = (function() {
    function PreviousMessageResultPath0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PreviousMessageResultPath0.prototype._read = function() {
      this.lenPreviousMessageResultPath = this._io.readU4be();
      if (!(this.lenPreviousMessageResultPath <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenPreviousMessageResultPath, this._io, "/types/previous_message_result_path_0/seq/0");
      }
      this._raw_previousMessageResultPath = this._io.readBytes(this.lenPreviousMessageResultPath);
      var _io__raw_previousMessageResultPath = new KaitaiStream(this._raw_previousMessageResultPath);
      this.previousMessageResultPath = new PreviousMessageResultPath(_io__raw_previousMessageResultPath, this, this._root);
    }

    return PreviousMessageResultPath0;
  })();

  var Case133EltField0 = Id015PtlimaptOperation.Case133EltField0 = (function() {
    function Case133EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case133EltField0.prototype._read = function() {
      this.case133EltField0 = this._io.readBytesFull();
    }

    return Case133EltField0;
  })();

  var Case149EltField00 = Id015PtlimaptOperation.Case149EltField00 = (function() {
    function Case149EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case149EltField00.prototype._read = function() {
      this.lenCase149EltField0 = this._io.readU1();
      if (!(this.lenCase149EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase149EltField0, this._io, "/types/case__149_elt_field0_0/seq/0");
      }
      this._raw_case149EltField0 = this._io.readBytes(this.lenCase149EltField0);
      var _io__raw_case149EltField0 = new KaitaiStream(this._raw_case149EltField0);
      this.case149EltField0 = new Case149EltField0(_io__raw_case149EltField0, this, this._root);
    }

    return Case149EltField00;
  })();

  var Bh10 = Id015PtlimaptOperation.Bh10 = (function() {
    function Bh10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh10.prototype._read = function() {
      this.lenBh1 = this._io.readU4be();
      if (!(this.lenBh1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh1, this._io, "/types/bh1_0/seq/0");
      }
      this._raw_bh1 = this._io.readBytes(this.lenBh1);
      var _io__raw_bh1 = new KaitaiStream(this._raw_bh1);
      this.bh1 = new Bh1(_io__raw_bh1, this, this._root);
    }

    return Bh10;
  })();

  var Case144EltField0 = Id015PtlimaptOperation.Case144EltField0 = (function() {
    function Case144EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case144EltField0.prototype._read = function() {
      this.case144EltField0 = this._io.readBytesFull();
    }

    return Case144EltField0;
  })();

  var Case133Entries = Id015PtlimaptOperation.Case133Entries = (function() {
    function Case133Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case133Entries.prototype._read = function() {
      this.case133EltField0 = new Case133EltField00(this._io, this, this._root);
      this.case133EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case133Entries;
  })();

  var Case140EltField00 = Id015PtlimaptOperation.Case140EltField00 = (function() {
    function Case140EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case140EltField00.prototype._read = function() {
      this.lenCase140EltField0 = this._io.readU1();
      if (!(this.lenCase140EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase140EltField0, this._io, "/types/case__140_elt_field0_0/seq/0");
      }
      this._raw_case140EltField0 = this._io.readBytes(this.lenCase140EltField0);
      var _io__raw_case140EltField0 = new KaitaiStream(this._raw_case140EltField0);
      this.case140EltField0 = new Case140EltField0(_io__raw_case140EltField0, this, this._root);
    }

    return Case140EltField00;
  })();

  var Case183EltField0 = Id015PtlimaptOperation.Case183EltField0 = (function() {
    function Case183EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case183EltField0.prototype._read = function() {
      this.case183EltField0 = this._io.readBytesFull();
    }

    return Case183EltField0;
  })();

  var Case131EltField0 = Id015PtlimaptOperation.Case131EltField0 = (function() {
    function Case131EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131EltField0.prototype._read = function() {
      this.case131EltField0 = this._io.readBytesFull();
    }

    return Case131EltField0;
  })();

  var Case3 = Id015PtlimaptOperation.Case3 = (function() {
    function Case3(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case3.prototype._read = function() {
      this.case3Field0 = this._io.readS2be();
      this.case3Field1 = this._io.readBytes(32);
      this.case3Field2 = this._io.readBytes(32);
      this.case3Field3 = new TreeEncoding(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case3;
  })();

  var Op22 = Id015PtlimaptOperation.Op22 = (function() {
    function Op22(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op22.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_2/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op21(_io__raw_op2, this, this._root);
    }

    return Op22;
  })();

  var Case136EltField0 = Id015PtlimaptOperation.Case136EltField0 = (function() {
    function Case136EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case136EltField0.prototype._read = function() {
      this.case136EltField0 = this._io.readBytesFull();
    }

    return Case136EltField0;
  })();

  var Case45Field1Entries = Id015PtlimaptOperation.Case45Field1Entries = (function() {
    function Case45Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case45Field1Entries.prototype._read = function() {
      this.case45Field1EltField0 = this._io.readU1();
      this.case45Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case45Field1Entries;
  })();

  var Case59 = Id015PtlimaptOperation.Case59 = (function() {
    function Case59(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case59.prototype._read = function() {
      this.case59Field0 = this._io.readS8be();
      this.case59Field1 = [];
      for (var i = 0; i < 14; i++) {
        this.case59Field1.push(new Case59Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__59_field1_entries
     */

    return Case59;
  })();

  var ScRollupTimeout = Id015PtlimaptOperation.ScRollupTimeout = (function() {
    function ScRollupTimeout(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupTimeout.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptRollupAddress(this._io, this, this._root);
      this.stakers = new Stakers(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return ScRollupTimeout;
  })();

  var CircuitsInfoEntries = Id015PtlimaptOperation.CircuitsInfoEntries = (function() {
    function CircuitsInfoEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CircuitsInfoEntries.prototype._read = function() {
      this.circuitsInfoEltField0 = new BytesDynUint30(this._io, this, this._root);
      this.circuitsInfoEltField1 = this._io.readU1();
    }

    return CircuitsInfoEntries;
  })();

  var Case190Entries = Id015PtlimaptOperation.Case190Entries = (function() {
    function Case190Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case190Entries.prototype._read = function() {
      this.case190EltField0 = new Case190EltField00(this._io, this, this._root);
      this.case190EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case190Entries;
  })();

  var Case29 = Id015PtlimaptOperation.Case29 = (function() {
    function Case29(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case29.prototype._read = function() {
      this.case29Field0 = this._io.readU2be();
      this.case29Field1 = [];
      for (var i = 0; i < 7; i++) {
        this.case29Field1.push(new Case29Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__29_field1_entries
     */

    return Case29;
  })();

  var Message0 = Id015PtlimaptOperation.Message0 = (function() {
    function Message0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message0.prototype._read = function() {
      this.messageEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messageEntries.push(new MessageEntries(this._io, this, this._root));
        i++;
      }
    }

    return Message0;
  })();

  var Case26Field1Entries = Id015PtlimaptOperation.Case26Field1Entries = (function() {
    function Case26Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case26Field1Entries.prototype._read = function() {
      this.case26Field1EltField0 = this._io.readU1();
      this.case26Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case26Field1Entries;
  })();

  var InboxProof = Id015PtlimaptOperation.InboxProof = (function() {
    function InboxProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InboxProof.prototype._read = function() {
      this.level = this._io.readS4be();
      this.messageCounter = new N(this._io, this, this._root);
      this.serializedProof = new BytesDynUint30(this._io, this, this._root);
    }

    return InboxProof;
  })();

  var Id015PtlimaptInlinedEndorsement = Id015PtlimaptOperation.Id015PtlimaptInlinedEndorsement = (function() {
    function Id015PtlimaptInlinedEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptInlinedEndorsement.prototype._read = function() {
      this.id015PtlimaptInlinedEndorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id015PtlimaptInlinedEndorsementMempoolContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id015PtlimaptOperation.Bool.TRUE) {
        this.signature = this._io.readBytes(64);
      }
    }

    return Id015PtlimaptInlinedEndorsement;
  })();

  var Case16Field1Entries = Id015PtlimaptOperation.Case16Field1Entries = (function() {
    function Case16Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case16Field1Entries.prototype._read = function() {
      this.case16Field1EltField0 = this._io.readU1();
      this.case16Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case16Field1Entries;
  })();

  var Case211Field10 = Id015PtlimaptOperation.Case211Field10 = (function() {
    function Case211Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case211Field10.prototype._read = function() {
      this.lenCase211Field1 = this._io.readU1();
      if (!(this.lenCase211Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase211Field1, this._io, "/types/case__211_field1_0/seq/0");
      }
      this._raw_case211Field1 = this._io.readBytes(this.lenCase211Field1);
      var _io__raw_case211Field1 = new KaitaiStream(this._raw_case211Field1);
      this.case211Field1 = new Case211Field1(_io__raw_case211Field1, this, this._root);
    }

    return Case211Field10;
  })();

  var Case1930 = Id015PtlimaptOperation.Case1930 = (function() {
    function Case1930(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1930.prototype._read = function() {
      this.lenCase193 = this._io.readU2be();
      if (!(this.lenCase193 <= 65535)) {
        throw new KaitaiStream.ValidationGreaterThanError(65535, this.lenCase193, this._io, "/types/case__193_0/seq/0");
      }
      this._raw_case193 = this._io.readBytes(this.lenCase193);
      var _io__raw_case193 = new KaitaiStream(this._raw_case193);
      this.case193 = new Case193(_io__raw_case193, this, this._root);
    }

    return Case1930;
  })();

  var OpEltField1 = Id015PtlimaptOperation.OpEltField1 = (function() {
    function OpEltField1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    OpEltField1.prototype._read = function() {
      this.opEltField1Tag = this._io.readU1();
      if (this.opEltField1Tag == Id015PtlimaptOperation.OpEltField1Tag.SOME) {
        this.some = new Some(this._io, this, this._root);
      }
    }

    return OpEltField1;
  })();

  var BytesDynUint30 = Id015PtlimaptOperation.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Case131Entries = Id015PtlimaptOperation.Case131Entries = (function() {
    function Case131Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case131Entries.prototype._read = function() {
      this.case131EltField0 = new Case131EltField00(this._io, this, this._root);
      this.case131EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case131Entries;
  })();

  var Case138Entries = Id015PtlimaptOperation.Case138Entries = (function() {
    function Case138Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case138Entries.prototype._read = function() {
      this.case138EltField0 = new Case138EltField00(this._io, this, this._root);
      this.case138EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case138Entries;
  })();

  var Op0 = Id015PtlimaptOperation.Op0 = (function() {
    function Op0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op0.prototype._read = function() {
      this.lenOp = this._io.readU4be();
      if (!(this.lenOp <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp, this._io, "/types/op_0/seq/0");
      }
      this._raw_op = this._io.readBytes(this.lenOp);
      var _io__raw_op = new KaitaiStream(this._raw_op);
      this.op = new Op(_io__raw_op, this, this._root);
    }

    return Op0;
  })();

  var Proof1 = Id015PtlimaptOperation.Proof1 = (function() {
    function Proof1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proof1.prototype._read = function() {
      this.pvmStep = new PvmStep(this._io, this, this._root);
      this.inputProofTag = this._io.readU1();
      if (this.inputProofTag == Id015PtlimaptOperation.Bool.TRUE) {
        this.inputProof = new InputProof(this._io, this, this._root);
      }
    }

    return Proof1;
  })();

  var Bh1 = Id015PtlimaptOperation.Bh1 = (function() {
    function Bh1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh1.prototype._read = function() {
      this.id015PtlimaptBlockHeaderAlphaFullHeader = new Id015PtlimaptBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh1;
  })();

  var Case63 = Id015PtlimaptOperation.Case63 = (function() {
    function Case63(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case63.prototype._read = function() {
      this.case63Field0 = this._io.readS8be();
      this.case63Field1 = new Case63Field10(this._io, this, this._root);
    }

    return Case63;
  })();

  var Case34Field1Entries = Id015PtlimaptOperation.Case34Field1Entries = (function() {
    function Case34Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case34Field1Entries.prototype._read = function() {
      this.case34Field1EltField0 = this._io.readU1();
      this.case34Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case34Field1Entries;
  })();

  var Payload0 = Id015PtlimaptOperation.Payload0 = (function() {
    function Payload0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Payload0.prototype._read = function() {
      this.lenPayload = this._io.readU4be();
      if (!(this.lenPayload <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenPayload, this._io, "/types/payload_0/seq/0");
      }
      this._raw_payload = this._io.readBytes(this.lenPayload);
      var _io__raw_payload = new KaitaiStream(this._raw_payload);
      this.payload = new Payload(_io__raw_payload, this, this._root);
    }

    return Payload0;
  })();

  var Case161EltField00 = Id015PtlimaptOperation.Case161EltField00 = (function() {
    function Case161EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case161EltField00.prototype._read = function() {
      this.lenCase161EltField0 = this._io.readU1();
      if (!(this.lenCase161EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase161EltField0, this._io, "/types/case__161_elt_field0_0/seq/0");
      }
      this._raw_case161EltField0 = this._io.readBytes(this.lenCase161EltField0);
      var _io__raw_case161EltField0 = new KaitaiStream(this._raw_case161EltField0);
      this.case161EltField0 = new Case161EltField0(_io__raw_case161EltField0, this, this._root);
    }

    return Case161EltField00;
  })();

  var Case156EltField00 = Id015PtlimaptOperation.Case156EltField00 = (function() {
    function Case156EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case156EltField00.prototype._read = function() {
      this.lenCase156EltField0 = this._io.readU1();
      if (!(this.lenCase156EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase156EltField0, this._io, "/types/case__156_elt_field0_0/seq/0");
      }
      this._raw_case156EltField0 = this._io.readBytes(this.lenCase156EltField0);
      var _io__raw_case156EltField0 = new KaitaiStream(this._raw_case156EltField0);
      this.case156EltField0 = new Case156EltField0(_io__raw_case156EltField0, this, this._root);
    }

    return Case156EltField00;
  })();

  var Dissection0 = Id015PtlimaptOperation.Dissection0 = (function() {
    function Dissection0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Dissection0.prototype._read = function() {
      this.lenDissection = this._io.readU4be();
      if (!(this.lenDissection <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenDissection, this._io, "/types/dissection_0/seq/0");
      }
      this._raw_dissection = this._io.readBytes(this.lenDissection);
      var _io__raw_dissection = new KaitaiStream(this._raw_dissection);
      this.dissection = new Dissection(_io__raw_dissection, this, this._root);
    }

    return Dissection0;
  })();

  var Case177EltField0 = Id015PtlimaptOperation.Case177EltField0 = (function() {
    function Case177EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case177EltField0.prototype._read = function() {
      this.case177EltField0 = this._io.readBytesFull();
    }

    return Case177EltField0;
  })();

  var Case142EltField0 = Id015PtlimaptOperation.Case142EltField0 = (function() {
    function Case142EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case142EltField0.prototype._read = function() {
      this.case142EltField0 = this._io.readBytesFull();
    }

    return Case142EltField0;
  })();

  var Case25Field1Entries = Id015PtlimaptOperation.Case25Field1Entries = (function() {
    function Case25Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case25Field1Entries.prototype._read = function() {
      this.case25Field1EltField0 = this._io.readU1();
      this.case25Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case25Field1Entries;
  })();

  var TxRollupRejection = Id015PtlimaptOperation.TxRollupRejection = (function() {
    function TxRollupRejection(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupRejection.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptTxRollupId(this._io, this, this._root);
      this.level = this._io.readS4be();
      this.message = new Message(this._io, this, this._root);
      this.messagePosition = new N(this._io, this, this._root);
      this.messagePath = new MessagePath0(this._io, this, this._root);
      this.messageResultHash = this._io.readBytes(32);
      this.messageResultPath = new MessageResultPath0(this._io, this, this._root);
      this.previousMessageResult = new PreviousMessageResult(this._io, this, this._root);
      this.previousMessageResultPath = new PreviousMessageResultPath0(this._io, this, this._root);
      this.proof = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupRejection;
  })();

  var SetDepositsLimit = Id015PtlimaptOperation.SetDepositsLimit = (function() {
    function SetDepositsLimit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SetDepositsLimit.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.limitTag = this._io.readU1();
      if (this.limitTag == Id015PtlimaptOperation.Bool.TRUE) {
        this.limit = new Id015PtlimaptMutez(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return SetDepositsLimit;
  })();

  var Case184Entries = Id015PtlimaptOperation.Case184Entries = (function() {
    function Case184Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case184Entries.prototype._read = function() {
      this.case184EltField0 = new Case184EltField00(this._io, this, this._root);
      this.case184EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case184Entries;
  })();

  var Case185EltField0 = Id015PtlimaptOperation.Case185EltField0 = (function() {
    function Case185EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case185EltField0.prototype._read = function() {
      this.case185EltField0 = this._io.readBytesFull();
    }

    return Case185EltField0;
  })();

  var Case159EltField00 = Id015PtlimaptOperation.Case159EltField00 = (function() {
    function Case159EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case159EltField00.prototype._read = function() {
      this.lenCase159EltField0 = this._io.readU1();
      if (!(this.lenCase159EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase159EltField0, this._io, "/types/case__159_elt_field0_0/seq/0");
      }
      this._raw_case159EltField0 = this._io.readBytes(this.lenCase159EltField0);
      var _io__raw_case159EltField0 = new KaitaiStream(this._raw_case159EltField0);
      this.case159EltField0 = new Case159EltField0(_io__raw_case159EltField0, this, this._root);
    }

    return Case159EltField00;
  })();

  var Case169Entries = Id015PtlimaptOperation.Case169Entries = (function() {
    function Case169Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case169Entries.prototype._read = function() {
      this.case169EltField0 = new Case169EltField00(this._io, this, this._root);
      this.case169EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case169Entries;
  })();

  var Case137Entries = Id015PtlimaptOperation.Case137Entries = (function() {
    function Case137Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case137Entries.prototype._read = function() {
      this.case137EltField0 = new Case137EltField00(this._io, this, this._root);
      this.case137EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case137Entries;
  })();

  var Case15 = Id015PtlimaptOperation.Case15 = (function() {
    function Case15(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case15.prototype._read = function() {
      this.case15Field0 = this._io.readS8be();
      this.case15Field1 = [];
      for (var i = 0; i < 3; i++) {
        this.case15Field1.push(new Case15Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__15_field1_entries
     */

    return Case15;
  })();

  var Case133EltField00 = Id015PtlimaptOperation.Case133EltField00 = (function() {
    function Case133EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case133EltField00.prototype._read = function() {
      this.lenCase133EltField0 = this._io.readU1();
      if (!(this.lenCase133EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase133EltField0, this._io, "/types/case__133_elt_field0_0/seq/0");
      }
      this._raw_case133EltField0 = this._io.readBytes(this.lenCase133EltField0);
      var _io__raw_case133EltField0 = new KaitaiStream(this._raw_case133EltField0);
      this.case133EltField0 = new Case133EltField0(_io__raw_case133EltField0, this, this._root);
    }

    return Case133EltField00;
  })();

  var Message1 = Id015PtlimaptOperation.Message1 = (function() {
    function Message1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message1.prototype._read = function() {
      this.lenMessage = this._io.readU4be();
      if (!(this.lenMessage <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessage, this._io, "/types/message_1/seq/0");
      }
      this._raw_message = this._io.readBytes(this.lenMessage);
      var _io__raw_message = new KaitaiStream(this._raw_message);
      this.message = new Message0(_io__raw_message, this, this._root);
    }

    return Message1;
  })();

  var Case176EltField00 = Id015PtlimaptOperation.Case176EltField00 = (function() {
    function Case176EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case176EltField00.prototype._read = function() {
      this.lenCase176EltField0 = this._io.readU1();
      if (!(this.lenCase176EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase176EltField0, this._io, "/types/case__176_elt_field0_0/seq/0");
      }
      this._raw_case176EltField0 = this._io.readBytes(this.lenCase176EltField0);
      var _io__raw_case176EltField0 = new KaitaiStream(this._raw_case176EltField0);
      this.case176EltField0 = new Case176EltField0(_io__raw_case176EltField0, this, this._root);
    }

    return Case176EltField00;
  })();

  var Case152Entries = Id015PtlimaptOperation.Case152Entries = (function() {
    function Case152Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case152Entries.prototype._read = function() {
      this.case152EltField0 = new Case152EltField00(this._io, this, this._root);
      this.case152EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case152Entries;
  })();

  var ScRollupRefute = Id015PtlimaptOperation.ScRollupRefute = (function() {
    function ScRollupRefute(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ScRollupRefute.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptRollupAddress(this._io, this, this._root);
      this.opponent = new PublicKeyHash(this._io, this, this._root);
      this.refutationTag = this._io.readU1();
      if (this.refutationTag == Id015PtlimaptOperation.Bool.TRUE) {
        this.refutation = new Refutation(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return ScRollupRefute;
  })();

  var Op10 = Id015PtlimaptOperation.Op10 = (function() {
    function Op10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op10.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_0/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op1(_io__raw_op1, this, this._root);
    }

    return Op10;
  })();

  var Case63Field1Entries = Id015PtlimaptOperation.Case63Field1Entries = (function() {
    function Case63Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case63Field1Entries.prototype._read = function() {
      this.case63Field1EltField0 = this._io.readU1();
      this.case63Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case63Field1Entries;
  })();

  var InitState0 = Id015PtlimaptOperation.InitState0 = (function() {
    function InitState0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InitState0.prototype._read = function() {
      this.lenInitState = this._io.readU4be();
      if (!(this.lenInitState <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenInitState, this._io, "/types/init_state_0/seq/0");
      }
      this._raw_initState = this._io.readBytes(this.lenInitState);
      var _io__raw_initState = new KaitaiStream(this._raw_initState);
      this.initState = new InitState(_io__raw_initState, this, this._root);
    }

    return InitState0;
  })();

  var Case156Entries = Id015PtlimaptOperation.Case156Entries = (function() {
    function Case156Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case156Entries.prototype._read = function() {
      this.case156EltField0 = new Case156EltField00(this._io, this, this._root);
      this.case156EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case156Entries;
  })();

  var Case209Field10 = Id015PtlimaptOperation.Case209Field10 = (function() {
    function Case209Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case209Field10.prototype._read = function() {
      this.lenCase209Field1 = this._io.readU1();
      if (!(this.lenCase209Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase209Field1, this._io, "/types/case__209_field1_0/seq/0");
      }
      this._raw_case209Field1 = this._io.readBytes(this.lenCase209Field1);
      var _io__raw_case209Field1 = new KaitaiStream(this._raw_case209Field1);
      this.case209Field1 = new Case209Field1(_io__raw_case209Field1, this, this._root);
    }

    return Case209Field10;
  })();

  var Case147EltField0 = Id015PtlimaptOperation.Case147EltField0 = (function() {
    function Case147EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case147EltField0.prototype._read = function() {
      this.case147EltField0 = this._io.readBytesFull();
    }

    return Case147EltField0;
  })();

  var Op21 = Id015PtlimaptOperation.Op21 = (function() {
    function Op21(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op21.prototype._read = function() {
      this.id015PtlimaptInlinedPreendorsement = new Id015PtlimaptInlinedPreendorsement(this._io, this, this._root);
    }

    return Op21;
  })();

  var Case20 = Id015PtlimaptOperation.Case20 = (function() {
    function Case20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case20.prototype._read = function() {
      this.case20Field0 = this._io.readU1();
      this.case20Field1 = [];
      for (var i = 0; i < 5; i++) {
        this.case20Field1.push(new Case20Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__20_field1_entries
     */

    return Case20;
  })();

  var Case155EltField0 = Id015PtlimaptOperation.Case155EltField0 = (function() {
    function Case155EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case155EltField0.prototype._read = function() {
      this.case155EltField0 = this._io.readBytesFull();
    }

    return Case155EltField0;
  })();

  var Proof = Id015PtlimaptOperation.Proof = (function() {
    function Proof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proof.prototype._read = function() {
      this.proofTag = this._io.readU1();
      if (this.proofTag == Id015PtlimaptOperation.ProofTag.CASE__0) {
        this.case0 = new Case0(this._io, this, this._root);
      }
      if (this.proofTag == Id015PtlimaptOperation.ProofTag.CASE__2) {
        this.case2 = new Case2(this._io, this, this._root);
      }
      if (this.proofTag == Id015PtlimaptOperation.ProofTag.CASE__1) {
        this.case1 = new Case1(this._io, this, this._root);
      }
      if (this.proofTag == Id015PtlimaptOperation.ProofTag.CASE__3) {
        this.case3 = new Case3(this._io, this, this._root);
      }
    }

    return Proof;
  })();

  var Case180Entries = Id015PtlimaptOperation.Case180Entries = (function() {
    function Case180Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case180Entries.prototype._read = function() {
      this.case180EltField0 = new Case180EltField00(this._io, this, this._root);
      this.case180EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case180Entries;
  })();

  var Case151EltField0 = Id015PtlimaptOperation.Case151EltField0 = (function() {
    function Case151EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case151EltField0.prototype._read = function() {
      this.case151EltField0 = this._io.readBytesFull();
    }

    return Case151EltField0;
  })();

  var Case191EltField0 = Id015PtlimaptOperation.Case191EltField0 = (function() {
    function Case191EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case191EltField0.prototype._read = function() {
      this.case191EltField0 = this._io.readBytesFull();
    }

    return Case191EltField0;
  })();

  var Commitment0 = Id015PtlimaptOperation.Commitment0 = (function() {
    function Commitment0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitment0.prototype._read = function() {
      this.compressedState = this._io.readBytes(32);
      this.inboxLevel = this._io.readS4be();
      this.predecessor = this._io.readBytes(32);
      this.numberOfTicks = this._io.readS8be();
    }

    return Commitment0;
  })();

  var Case143EltField00 = Id015PtlimaptOperation.Case143EltField00 = (function() {
    function Case143EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case143EltField00.prototype._read = function() {
      this.lenCase143EltField0 = this._io.readU1();
      if (!(this.lenCase143EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase143EltField0, this._io, "/types/case__143_elt_field0_0/seq/0");
      }
      this._raw_case143EltField0 = this._io.readBytes(this.lenCase143EltField0);
      var _io__raw_case143EltField0 = new KaitaiStream(this._raw_case143EltField0);
      this.case143EltField0 = new Case143EltField0(_io__raw_case143EltField0, this, this._root);
    }

    return Case143EltField00;
  })();

  var Case37 = Id015PtlimaptOperation.Case37 = (function() {
    function Case37(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case37.prototype._read = function() {
      this.case37Field0 = this._io.readU2be();
      this.case37Field1 = [];
      for (var i = 0; i < 9; i++) {
        this.case37Field1.push(new Case37Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__37_field1_entries
     */

    return Case37;
  })();

  var Case35Field1Entries = Id015PtlimaptOperation.Case35Field1Entries = (function() {
    function Case35Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case35Field1Entries.prototype._read = function() {
      this.case35Field1EltField0 = this._io.readU1();
      this.case35Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case35Field1Entries;
  })();

  var Origination = Id015PtlimaptOperation.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.balance = new Id015PtlimaptMutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id015PtlimaptOperation.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id015PtlimaptScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var Case149Entries = Id015PtlimaptOperation.Case149Entries = (function() {
    function Case149Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case149Entries.prototype._read = function() {
      this.case149EltField0 = new Case149EltField00(this._io, this, this._root);
      this.case149EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case149Entries;
  })();

  var DrainDelegate = Id015PtlimaptOperation.DrainDelegate = (function() {
    function DrainDelegate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DrainDelegate.prototype._read = function() {
      this.consensusKey = new PublicKeyHash(this._io, this, this._root);
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.destination = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return DrainDelegate;
  })();

  var Case46Field1Entries = Id015PtlimaptOperation.Case46Field1Entries = (function() {
    function Case46Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case46Field1Entries.prototype._read = function() {
      this.case46Field1EltField0 = this._io.readU1();
      this.case46Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case46Field1Entries;
  })();

  var Case164EltField0 = Id015PtlimaptOperation.Case164EltField0 = (function() {
    function Case164EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case164EltField0.prototype._read = function() {
      this.case164EltField0 = this._io.readBytesFull();
    }

    return Case164EltField0;
  })();

  var Case26 = Id015PtlimaptOperation.Case26 = (function() {
    function Case26(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case26.prototype._read = function() {
      this.case26Field0 = this._io.readS4be();
      this.case26Field1 = [];
      for (var i = 0; i < 6; i++) {
        this.case26Field1.push(new Case26Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__26_field1_entries
     */

    return Case26;
  })();

  var Case31 = Id015PtlimaptOperation.Case31 = (function() {
    function Case31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case31.prototype._read = function() {
      this.case31Field0 = this._io.readS8be();
      this.case31Field1 = [];
      for (var i = 0; i < 7; i++) {
        this.case31Field1.push(new Case31Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__31_field1_entries
     */

    return Case31;
  })();

  var Case19 = Id015PtlimaptOperation.Case19 = (function() {
    function Case19(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case19.prototype._read = function() {
      this.case19Field0 = this._io.readS8be();
      this.case19Field1 = [];
      for (var i = 0; i < 4; i++) {
        this.case19Field1.push(new Case19Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__19_field1_entries
     */

    return Case19;
  })();

  var Id015PtlimaptContractIdOriginated = Id015PtlimaptOperation.Id015PtlimaptContractIdOriginated = (function() {
    function Id015PtlimaptContractIdOriginated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptContractIdOriginated.prototype._read = function() {
      this.id015PtlimaptContractIdOriginatedTag = this._io.readU1();
      if (this.id015PtlimaptContractIdOriginatedTag == Id015PtlimaptOperation.Id015PtlimaptContractIdOriginatedTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    return Id015PtlimaptContractIdOriginated;
  })();

  var Micheline015PtlimaptMichelsonV1Expression = Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1Expression = (function() {
    function Micheline015PtlimaptMichelsonV1Expression(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Micheline015PtlimaptMichelsonV1Expression.prototype._read = function() {
      this.micheline015PtlimaptMichelsonV1ExpressionTag = this._io.readU1();
      if (this.micheline015PtlimaptMichelsonV1ExpressionTag == Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1ExpressionTag.INT) {
        this.int = new Z(this._io, this, this._root);
      }
      if (this.micheline015PtlimaptMichelsonV1ExpressionTag == Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1ExpressionTag.STRING) {
        this.string = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.micheline015PtlimaptMichelsonV1ExpressionTag == Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1ExpressionTag.SEQUENCE) {
        this.sequence = new Sequence0(this._io, this, this._root);
      }
      if (this.micheline015PtlimaptMichelsonV1ExpressionTag == Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1ExpressionTag.PRIM__NO_ARGS__NO_ANNOTS) {
        this.primNoArgsNoAnnots = new Id015PtlimaptMichelsonV1Primitives(this._io, this, this._root);
      }
      if (this.micheline015PtlimaptMichelsonV1ExpressionTag == Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1ExpressionTag.PRIM__NO_ARGS__SOME_ANNOTS) {
        this.primNoArgsSomeAnnots = new PrimNoArgsSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline015PtlimaptMichelsonV1ExpressionTag == Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1ExpressionTag.PRIM__1_ARG__NO_ANNOTS) {
        this.prim1ArgNoAnnots = new Prim1ArgNoAnnots(this._io, this, this._root);
      }
      if (this.micheline015PtlimaptMichelsonV1ExpressionTag == Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1ExpressionTag.PRIM__1_ARG__SOME_ANNOTS) {
        this.prim1ArgSomeAnnots = new Prim1ArgSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline015PtlimaptMichelsonV1ExpressionTag == Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1ExpressionTag.PRIM__2_ARGS__NO_ANNOTS) {
        this.prim2ArgsNoAnnots = new Prim2ArgsNoAnnots(this._io, this, this._root);
      }
      if (this.micheline015PtlimaptMichelsonV1ExpressionTag == Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1ExpressionTag.PRIM__2_ARGS__SOME_ANNOTS) {
        this.prim2ArgsSomeAnnots = new Prim2ArgsSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline015PtlimaptMichelsonV1ExpressionTag == Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1ExpressionTag.PRIM__GENERIC) {
        this.primGeneric = new PrimGeneric(this._io, this, this._root);
      }
      if (this.micheline015PtlimaptMichelsonV1ExpressionTag == Id015PtlimaptOperation.Micheline015PtlimaptMichelsonV1ExpressionTag.BYTES) {
        this.bytes = new BytesDynUint30(this._io, this, this._root);
      }
    }

    return Micheline015PtlimaptMichelsonV1Expression;
  })();

  var Case43Field1Entries = Id015PtlimaptOperation.Case43Field1Entries = (function() {
    function Case43Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case43Field1Entries.prototype._read = function() {
      this.case43Field1EltField0 = this._io.readU1();
      this.case43Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case43Field1Entries;
  })();

  var Case160EltField00 = Id015PtlimaptOperation.Case160EltField00 = (function() {
    function Case160EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case160EltField00.prototype._read = function() {
      this.lenCase160EltField0 = this._io.readU1();
      if (!(this.lenCase160EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase160EltField0, this._io, "/types/case__160_elt_field0_0/seq/0");
      }
      this._raw_case160EltField0 = this._io.readBytes(this.lenCase160EltField0);
      var _io__raw_case160EltField0 = new KaitaiStream(this._raw_case160EltField0);
      this.case160EltField0 = new Case160EltField0(_io__raw_case160EltField0, this, this._root);
    }

    return Case160EltField00;
  })();

  var Case41 = Id015PtlimaptOperation.Case41 = (function() {
    function Case41(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case41.prototype._read = function() {
      this.case41Field0 = this._io.readU2be();
      this.case41Field1 = [];
      for (var i = 0; i < 10; i++) {
        this.case41Field1.push(new Case41Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__41_field1_entries
     */

    return Case41;
  })();

  var Case209 = Id015PtlimaptOperation.Case209 = (function() {
    function Case209(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case209.prototype._read = function() {
      this.case209Field0 = this._io.readU2be();
      this.case209Field1 = new Case209Field10(this._io, this, this._root);
      this.case209Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case209;
  })();

  var Case00 = Id015PtlimaptOperation.Case00 = (function() {
    function Case00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case00.prototype._read = function() {
      this.case0Field0 = this._io.readS2be();
      this.case0Field1 = this._io.readBytes(32);
      this.case0Field2 = this._io.readBytes(32);
      this.case0Field3 = new TreeEncoding(this._io, this, this._root);
    }

    /**
     * context_hash
     */

    /**
     * context_hash
     */

    return Case00;
  })();

  var Case173EltField0 = Id015PtlimaptOperation.Case173EltField0 = (function() {
    function Case173EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case173EltField0.prototype._read = function() {
      this.case173EltField0 = this._io.readBytesFull();
    }

    return Case173EltField0;
  })();

  var Case12Field1Entries = Id015PtlimaptOperation.Case12Field1Entries = (function() {
    function Case12Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case12Field1Entries.prototype._read = function() {
      this.case12Field1EltField0 = this._io.readU1();
      this.case12Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case12Field1Entries;
  })();

  var Case14Field1Entries = Id015PtlimaptOperation.Case14Field1Entries = (function() {
    function Case14Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case14Field1Entries.prototype._read = function() {
      this.case14Field1EltField0 = this._io.readU1();
      this.case14Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case14Field1Entries;
  })();

  var Message = Id015PtlimaptOperation.Message = (function() {
    function Message(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Message.prototype._read = function() {
      this.messageTag = this._io.readU1();
      if (this.messageTag == Id015PtlimaptOperation.MessageTag.BATCH) {
        this.batch = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.messageTag == Id015PtlimaptOperation.MessageTag.DEPOSIT) {
        this.deposit = new Deposit(this._io, this, this._root);
      }
    }

    return Message;
  })();

  var Case30Field1Entries = Id015PtlimaptOperation.Case30Field1Entries = (function() {
    function Case30Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case30Field1Entries.prototype._read = function() {
      this.case30Field1EltField0 = this._io.readU1();
      this.case30Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case30Field1Entries;
  })();

  var Case141Entries = Id015PtlimaptOperation.Case141Entries = (function() {
    function Case141Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case141Entries.prototype._read = function() {
      this.case141EltField0 = new Case141EltField00(this._io, this, this._root);
      this.case141EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case141Entries;
  })();

  var Case178EltField0 = Id015PtlimaptOperation.Case178EltField0 = (function() {
    function Case178EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case178EltField0.prototype._read = function() {
      this.case178EltField0 = this._io.readBytesFull();
    }

    return Case178EltField0;
  })();

  var InitState = Id015PtlimaptOperation.InitState = (function() {
    function InitState(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InitState.prototype._read = function() {
      this.initStateEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.initStateEntries.push(new InitStateEntries(this._io, this, this._root));
        i++;
      }
    }

    return InitState;
  })();

  var Args = Id015PtlimaptOperation.Args = (function() {
    function Args(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Args.prototype._read = function() {
      this.argsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.argsEntries.push(new ArgsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Args;
  })();

  var Case172EltField0 = Id015PtlimaptOperation.Case172EltField0 = (function() {
    function Case172EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case172EltField0.prototype._read = function() {
      this.case172EltField0 = this._io.readBytesFull();
    }

    return Case172EltField0;
  })();

  var Case139EltField0 = Id015PtlimaptOperation.Case139EltField0 = (function() {
    function Case139EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case139EltField0.prototype._read = function() {
      this.case139EltField0 = this._io.readBytesFull();
    }

    return Case139EltField0;
  })();

  var Case209Field1 = Id015PtlimaptOperation.Case209Field1 = (function() {
    function Case209Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case209Field1.prototype._read = function() {
      this.case209Field1 = this._io.readBytesFull();
    }

    return Case209Field1;
  })();

  var ZkRollupOrigination = Id015PtlimaptOperation.ZkRollupOrigination = (function() {
    function ZkRollupOrigination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ZkRollupOrigination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.publicParameters = new PublicParameters(this._io, this, this._root);
      this.circuitsInfo = new CircuitsInfo0(this._io, this, this._root);
      this.initState = new InitState0(this._io, this, this._root);
      this.nbOps = new Int31(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return ZkRollupOrigination;
  })();

  var Case159EltField0 = Id015PtlimaptOperation.Case159EltField0 = (function() {
    function Case159EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case159EltField0.prototype._read = function() {
      this.case159EltField0 = this._io.readBytesFull();
    }

    return Case159EltField0;
  })();

  var Case216 = Id015PtlimaptOperation.Case216 = (function() {
    function Case216(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case216.prototype._read = function() {
      this.case216Field0 = this._io.readU1();
      this.case216Field1 = new Case216Field10(this._io, this, this._root);
      this.case216Field2 = new InodeTree(this._io, this, this._root);
    }

    return Case216;
  })();

  var Op2 = Id015PtlimaptOperation.Op2 = (function() {
    function Op2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op2.prototype._read = function() {
      this.id015PtlimaptInlinedEndorsement = new Id015PtlimaptInlinedEndorsement(this._io, this, this._root);
    }

    return Op2;
  })();

  var Case56Field1Entries = Id015PtlimaptOperation.Case56Field1Entries = (function() {
    function Case56Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case56Field1Entries.prototype._read = function() {
      this.case56Field1EltField0 = this._io.readU1();
      this.case56Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case56Field1Entries;
  })();

  var Case216Field10 = Id015PtlimaptOperation.Case216Field10 = (function() {
    function Case216Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case216Field10.prototype._read = function() {
      this.lenCase216Field1 = this._io.readU1();
      if (!(this.lenCase216Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase216Field1, this._io, "/types/case__216_field1_0/seq/0");
      }
      this._raw_case216Field1 = this._io.readBytes(this.lenCase216Field1);
      var _io__raw_case216Field1 = new KaitaiStream(this._raw_case216Field1);
      this.case216Field1 = new Case216Field1(_io__raw_case216Field1, this, this._root);
    }

    return Case216Field10;
  })();

  var Case191 = Id015PtlimaptOperation.Case191 = (function() {
    function Case191(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case191.prototype._read = function() {
      this.case191Entries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.case191Entries.push(new Case191Entries(this._io, this, this._root));
        i++;
      }
    }

    return Case191;
  })();

  var Case153Entries = Id015PtlimaptOperation.Case153Entries = (function() {
    function Case153Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case153Entries.prototype._read = function() {
      this.case153EltField0 = new Case153EltField00(this._io, this, this._root);
      this.case153EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case153Entries;
  })();

  var Case63Field10 = Id015PtlimaptOperation.Case63Field10 = (function() {
    function Case63Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case63Field10.prototype._read = function() {
      this.lenCase63Field1 = this._io.readU4be();
      if (!(this.lenCase63Field1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCase63Field1, this._io, "/types/case__63_field1_0/seq/0");
      }
      this._raw_case63Field1 = this._io.readBytes(this.lenCase63Field1);
      var _io__raw_case63Field1 = new KaitaiStream(this._raw_case63Field1);
      this.case63Field1 = new Case63Field1(_io__raw_case63Field1, this, this._root);
    }

    return Case63Field10;
  })();

  var Case7 = Id015PtlimaptOperation.Case7 = (function() {
    function Case7(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case7.prototype._read = function() {
      this.case7Field0 = this._io.readS8be();
      this.case7Field1 = [];
      for (var i = 0; i < 1; i++) {
        this.case7Field1.push(new Case7Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__7_field1_entries
     */

    return Case7;
  })();

  var Case218Field1 = Id015PtlimaptOperation.Case218Field1 = (function() {
    function Case218Field1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case218Field1.prototype._read = function() {
      this.case218Field1 = this._io.readBytesFull();
    }

    return Case218Field1;
  })();

  var Case15Field1Entries = Id015PtlimaptOperation.Case15Field1Entries = (function() {
    function Case15Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case15Field1Entries.prototype._read = function() {
      this.case15Field1EltField0 = this._io.readU1();
      this.case15Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case15Field1Entries;
  })();

  var Case156EltField0 = Id015PtlimaptOperation.Case156EltField0 = (function() {
    function Case156EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case156EltField0.prototype._read = function() {
      this.case156EltField0 = this._io.readBytesFull();
    }

    return Case156EltField0;
  })();

  var Case174EltField00 = Id015PtlimaptOperation.Case174EltField00 = (function() {
    function Case174EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case174EltField00.prototype._read = function() {
      this.lenCase174EltField0 = this._io.readU1();
      if (!(this.lenCase174EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase174EltField0, this._io, "/types/case__174_elt_field0_0/seq/0");
      }
      this._raw_case174EltField0 = this._io.readBytes(this.lenCase174EltField0);
      var _io__raw_case174EltField0 = new KaitaiStream(this._raw_case174EltField0);
      this.case174EltField0 = new Case174EltField0(_io__raw_case174EltField0, this, this._root);
    }

    return Case174EltField00;
  })();

  var Case160EltField0 = Id015PtlimaptOperation.Case160EltField0 = (function() {
    function Case160EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case160EltField0.prototype._read = function() {
      this.case160EltField0 = this._io.readBytesFull();
    }

    return Case160EltField0;
  })();

  var TxRollupReturnBond = Id015PtlimaptOperation.TxRollupReturnBond = (function() {
    function TxRollupReturnBond(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupReturnBond.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptTxRollupId(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupReturnBond;
  })();

  var Case169EltField00 = Id015PtlimaptOperation.Case169EltField00 = (function() {
    function Case169EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case169EltField00.prototype._read = function() {
      this.lenCase169EltField0 = this._io.readU1();
      if (!(this.lenCase169EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase169EltField0, this._io, "/types/case__169_elt_field0_0/seq/0");
      }
      this._raw_case169EltField0 = this._io.readBytes(this.lenCase169EltField0);
      var _io__raw_case169EltField0 = new KaitaiStream(this._raw_case169EltField0);
      this.case169EltField0 = new Case169EltField0(_io__raw_case169EltField0, this, this._root);
    }

    return Case169EltField00;
  })();

  var Dissection = Id015PtlimaptOperation.Dissection = (function() {
    function Dissection(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Dissection.prototype._read = function() {
      this.dissectionEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.dissectionEntries.push(new DissectionEntries(this._io, this, this._root));
        i++;
      }
    }

    return Dissection;
  })();

  var TicketsInfo0 = Id015PtlimaptOperation.TicketsInfo0 = (function() {
    function TicketsInfo0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TicketsInfo0.prototype._read = function() {
      this.lenTicketsInfo = this._io.readU4be();
      if (!(this.lenTicketsInfo <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenTicketsInfo, this._io, "/types/tickets_info_0/seq/0");
      }
      this._raw_ticketsInfo = this._io.readBytes(this.lenTicketsInfo);
      var _io__raw_ticketsInfo = new KaitaiStream(this._raw_ticketsInfo);
      this.ticketsInfo = new TicketsInfo(_io__raw_ticketsInfo, this, this._root);
    }

    return TicketsInfo0;
  })();

  var TxRollupFinalizeCommitment = Id015PtlimaptOperation.TxRollupFinalizeCommitment = (function() {
    function TxRollupFinalizeCommitment(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupFinalizeCommitment.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptTxRollupId(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupFinalizeCommitment;
  })();

  var Case17Field1Entries = Id015PtlimaptOperation.Case17Field1Entries = (function() {
    function Case17Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case17Field1Entries.prototype._read = function() {
      this.case17Field1EltField0 = this._io.readU1();
      this.case17Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case17Field1Entries;
  })();

  var UpdateConsensusKey = Id015PtlimaptOperation.UpdateConsensusKey = (function() {
    function UpdateConsensusKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    UpdateConsensusKey.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.pk = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return UpdateConsensusKey;
  })();

  var Case145EltField0 = Id015PtlimaptOperation.Case145EltField0 = (function() {
    function Case145EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case145EltField0.prototype._read = function() {
      this.case145EltField0 = this._io.readBytesFull();
    }

    return Case145EltField0;
  })();

  var TxRollupOrigination = Id015PtlimaptOperation.TxRollupOrigination = (function() {
    function TxRollupOrigination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupOrigination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return TxRollupOrigination;
  })();

  var Case47 = Id015PtlimaptOperation.Case47 = (function() {
    function Case47(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case47.prototype._read = function() {
      this.case47Field0 = this._io.readS8be();
      this.case47Field1 = [];
      for (var i = 0; i < 11; i++) {
        this.case47Field1.push(new Case47Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__47_field1_entries
     */

    return Case47;
  })();

  var Prim1ArgNoAnnots = Id015PtlimaptOperation.Prim1ArgNoAnnots = (function() {
    function Prim1ArgNoAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim1ArgNoAnnots.prototype._read = function() {
      this.prim = new Id015PtlimaptMichelsonV1Primitives(this._io, this, this._root);
      this.arg = new Micheline015PtlimaptMichelsonV1Expression(this._io, this, this._root);
    }

    return Prim1ArgNoAnnots;
  })();

  var Case21Field1Entries = Id015PtlimaptOperation.Case21Field1Entries = (function() {
    function Case21Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case21Field1Entries.prototype._read = function() {
      this.case21Field1EltField0 = this._io.readU1();
      this.case21Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case21Field1Entries;
  })();

  var Case60 = Id015PtlimaptOperation.Case60 = (function() {
    function Case60(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case60.prototype._read = function() {
      this.case60Field0 = this._io.readU1();
      this.case60Field1 = new Case60Field10(this._io, this, this._root);
    }

    return Case60;
  })();

  var Case52Field1Entries = Id015PtlimaptOperation.Case52Field1Entries = (function() {
    function Case52Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case52Field1Entries.prototype._read = function() {
      this.case52Field1EltField0 = this._io.readU1();
      this.case52Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case52Field1Entries;
  })();

  var MessageResultPath = Id015PtlimaptOperation.MessageResultPath = (function() {
    function MessageResultPath(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageResultPath.prototype._read = function() {
      this.messageResultPathEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messageResultPathEntries.push(new MessageResultPathEntries(this._io, this, this._root));
        i++;
      }
    }

    return MessageResultPath;
  })();

  var Id015PtlimaptMichelsonV1Primitives = Id015PtlimaptOperation.Id015PtlimaptMichelsonV1Primitives = (function() {
    function Id015PtlimaptMichelsonV1Primitives(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptMichelsonV1Primitives.prototype._read = function() {
      this.id015PtlimaptMichelsonV1Primitives = this._io.readU1();
    }

    return Id015PtlimaptMichelsonV1Primitives;
  })();

  var Case175Entries = Id015PtlimaptOperation.Case175Entries = (function() {
    function Case175Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case175Entries.prototype._read = function() {
      this.case175EltField0 = new Case175EltField00(this._io, this, this._root);
      this.case175EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case175Entries;
  })();

  var Case177EltField00 = Id015PtlimaptOperation.Case177EltField00 = (function() {
    function Case177EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case177EltField00.prototype._read = function() {
      this.lenCase177EltField0 = this._io.readU1();
      if (!(this.lenCase177EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase177EltField0, this._io, "/types/case__177_elt_field0_0/seq/0");
      }
      this._raw_case177EltField0 = this._io.readBytes(this.lenCase177EltField0);
      var _io__raw_case177EltField0 = new KaitaiStream(this._raw_case177EltField0);
      this.case177EltField0 = new Case177EltField0(_io__raw_case177EltField0, this, this._root);
    }

    return Case177EltField00;
  })();

  var SequenceEntries = Id015PtlimaptOperation.SequenceEntries = (function() {
    function SequenceEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SequenceEntries.prototype._read = function() {
      this.sequenceElt = new Micheline015PtlimaptMichelsonV1Expression(this._io, this, this._root);
    }

    return SequenceEntries;
  })();

  var Case181EltField00 = Id015PtlimaptOperation.Case181EltField00 = (function() {
    function Case181EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case181EltField00.prototype._read = function() {
      this.lenCase181EltField0 = this._io.readU1();
      if (!(this.lenCase181EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase181EltField0, this._io, "/types/case__181_elt_field0_0/seq/0");
      }
      this._raw_case181EltField0 = this._io.readBytes(this.lenCase181EltField0);
      var _io__raw_case181EltField0 = new KaitaiStream(this._raw_case181EltField0);
      this.case181EltField0 = new Case181EltField0(_io__raw_case181EltField0, this, this._root);
    }

    return Case181EltField00;
  })();

  var Case8Field1Entries = Id015PtlimaptOperation.Case8Field1Entries = (function() {
    function Case8Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case8Field1Entries.prototype._read = function() {
      this.case8Field1EltField0 = this._io.readU1();
      this.case8Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case8Field1Entries;
  })();

  var Int31 = Id015PtlimaptOperation.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id015PtlimaptOperation.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Prim2ArgsNoAnnots = Id015PtlimaptOperation.Prim2ArgsNoAnnots = (function() {
    function Prim2ArgsNoAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim2ArgsNoAnnots.prototype._read = function() {
      this.prim = new Id015PtlimaptMichelsonV1Primitives(this._io, this, this._root);
      this.arg1 = new Micheline015PtlimaptMichelsonV1Expression(this._io, this, this._root);
      this.arg2 = new Micheline015PtlimaptMichelsonV1Expression(this._io, this, this._root);
    }

    return Prim2ArgsNoAnnots;
  })();

  var MessageResultPath0 = Id015PtlimaptOperation.MessageResultPath0 = (function() {
    function MessageResultPath0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessageResultPath0.prototype._read = function() {
      this.lenMessageResultPath = this._io.readU4be();
      if (!(this.lenMessageResultPath <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessageResultPath, this._io, "/types/message_result_path_0/seq/0");
      }
      this._raw_messageResultPath = this._io.readBytes(this.lenMessageResultPath);
      var _io__raw_messageResultPath = new KaitaiStream(this._raw_messageResultPath);
      this.messageResultPath = new MessageResultPath(_io__raw_messageResultPath, this, this._root);
    }

    return MessageResultPath0;
  })();

  var Case58 = Id015PtlimaptOperation.Case58 = (function() {
    function Case58(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case58.prototype._read = function() {
      this.case58Field0 = this._io.readS4be();
      this.case58Field1 = [];
      for (var i = 0; i < 14; i++) {
        this.case58Field1.push(new Case58Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__58_field1_entries
     */

    return Case58;
  })();

  var Case181EltField0 = Id015PtlimaptOperation.Case181EltField0 = (function() {
    function Case181EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case181EltField0.prototype._read = function() {
      this.case181EltField0 = this._io.readBytesFull();
    }

    return Case181EltField0;
  })();

  var MessagePathEntries = Id015PtlimaptOperation.MessagePathEntries = (function() {
    function MessagePathEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagePathEntries.prototype._read = function() {
      this.inboxListHash = this._io.readBytes(32);
    }

    return MessagePathEntries;
  })();

  var PublicParameters = Id015PtlimaptOperation.PublicParameters = (function() {
    function PublicParameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicParameters.prototype._read = function() {
      this.publicParametersField0 = new BytesDynUint30(this._io, this, this._root);
      this.publicParametersField1 = new BytesDynUint30(this._io, this, this._root);
    }

    return PublicParameters;
  })();

  var Case24Field1Entries = Id015PtlimaptOperation.Case24Field1Entries = (function() {
    function Case24Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case24Field1Entries.prototype._read = function() {
      this.case24Field1EltField0 = this._io.readU1();
      this.case24Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case24Field1Entries;
  })();

  var Case154EltField00 = Id015PtlimaptOperation.Case154EltField00 = (function() {
    function Case154EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case154EltField00.prototype._read = function() {
      this.lenCase154EltField0 = this._io.readU1();
      if (!(this.lenCase154EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase154EltField0, this._io, "/types/case__154_elt_field0_0/seq/0");
      }
      this._raw_case154EltField0 = this._io.readBytes(this.lenCase154EltField0);
      var _io__raw_case154EltField0 = new KaitaiStream(this._raw_case154EltField0);
      this.case154EltField0 = new Case154EltField0(_io__raw_case154EltField0, this, this._root);
    }

    return Case154EltField00;
  })();

  var InitStateEntries = Id015PtlimaptOperation.InitStateEntries = (function() {
    function InitStateEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InitStateEntries.prototype._read = function() {
      this.initStateElt = this._io.readBytes(32);
    }

    return InitStateEntries;
  })();

  var Case179EltField0 = Id015PtlimaptOperation.Case179EltField0 = (function() {
    function Case179EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case179EltField0.prototype._read = function() {
      this.case179EltField0 = this._io.readBytesFull();
    }

    return Case179EltField0;
  })();

  var DalSlotAvailability = Id015PtlimaptOperation.DalSlotAvailability = (function() {
    function DalSlotAvailability(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalSlotAvailability.prototype._read = function() {
      this.endorser = new PublicKeyHash(this._io, this, this._root);
      this.endorsement = new Z(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return DalSlotAvailability;
  })();

  var Slot = Id015PtlimaptOperation.Slot = (function() {
    function Slot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Slot.prototype._read = function() {
      this.level = this._io.readS4be();
      this.index = this._io.readU1();
      this.header = this._io.readBytes(48);
    }

    return Slot;
  })();

  var ArgsEntries = Id015PtlimaptOperation.ArgsEntries = (function() {
    function ArgsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ArgsEntries.prototype._read = function() {
      this.argsElt = new Micheline015PtlimaptMichelsonV1Expression(this._io, this, this._root);
    }

    return ArgsEntries;
  })();

  var Case135EltField0 = Id015PtlimaptOperation.Case135EltField0 = (function() {
    function Case135EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case135EltField0.prototype._read = function() {
      this.case135EltField0 = this._io.readBytesFull();
    }

    return Case135EltField0;
  })();

  var Case153EltField0 = Id015PtlimaptOperation.Case153EltField0 = (function() {
    function Case153EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case153EltField0.prototype._read = function() {
      this.case153EltField0 = this._io.readBytesFull();
    }

    return Case153EltField0;
  })();

  var Deposit = Id015PtlimaptOperation.Deposit = (function() {
    function Deposit(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposit.prototype._read = function() {
      this.sender = new PublicKeyHash(this._io, this, this._root);
      this.destination = this._io.readBytes(20);
      this.ticketHash = this._io.readBytes(32);
      this.amount = new Amount(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Deposit;
  })();

  var Case44Field1Entries = Id015PtlimaptOperation.Case44Field1Entries = (function() {
    function Case44Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case44Field1Entries.prototype._read = function() {
      this.case44Field1EltField0 = this._io.readU1();
      this.case44Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case44Field1Entries;
  })();

  var Case132Entries = Id015PtlimaptOperation.Case132Entries = (function() {
    function Case132Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case132Entries.prototype._read = function() {
      this.case132EltField0 = new Case132EltField00(this._io, this, this._root);
      this.case132EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case132Entries;
  })();

  var MessagePath = Id015PtlimaptOperation.MessagePath = (function() {
    function MessagePath(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagePath.prototype._read = function() {
      this.messagePathEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.messagePathEntries.push(new MessagePathEntries(this._io, this, this._root));
        i++;
      }
    }

    return MessagePath;
  })();

  var Args0 = Id015PtlimaptOperation.Args0 = (function() {
    function Args0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Args0.prototype._read = function() {
      this.lenArgs = this._io.readU4be();
      if (!(this.lenArgs <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenArgs, this._io, "/types/args_0/seq/0");
      }
      this._raw_args = this._io.readBytes(this.lenArgs);
      var _io__raw_args = new KaitaiStream(this._raw_args);
      this.args = new Args(_io__raw_args, this, this._root);
    }

    return Args0;
  })();

  var Named0 = Id015PtlimaptOperation.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id015PtlimaptOperation.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.amount = new Id015PtlimaptMutez(this._io, this, this._root);
      this.destination = new Id015PtlimaptContractId(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id015PtlimaptOperation.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Transaction;
  })();

  var Case144EltField00 = Id015PtlimaptOperation.Case144EltField00 = (function() {
    function Case144EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case144EltField00.prototype._read = function() {
      this.lenCase144EltField0 = this._io.readU1();
      if (!(this.lenCase144EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase144EltField0, this._io, "/types/case__144_elt_field0_0/seq/0");
      }
      this._raw_case144EltField0 = this._io.readBytes(this.lenCase144EltField0);
      var _io__raw_case144EltField0 = new KaitaiStream(this._raw_case144EltField0);
      this.case144EltField0 = new Case144EltField0(_io__raw_case144EltField0, this, this._root);
    }

    return Case144EltField00;
  })();

  var Case47Field1Entries = Id015PtlimaptOperation.Case47Field1Entries = (function() {
    function Case47Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case47Field1Entries.prototype._read = function() {
      this.case47Field1EltField0 = this._io.readU1();
      this.case47Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case47Field1Entries;
  })();

  var Case14 = Id015PtlimaptOperation.Case14 = (function() {
    function Case14(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case14.prototype._read = function() {
      this.case14Field0 = this._io.readS4be();
      this.case14Field1 = [];
      for (var i = 0; i < 3; i++) {
        this.case14Field1.push(new Case14Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__14_field1_entries
     */

    return Case14;
  })();

  var Case218Field10 = Id015PtlimaptOperation.Case218Field10 = (function() {
    function Case218Field10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case218Field10.prototype._read = function() {
      this.lenCase218Field1 = this._io.readU1();
      if (!(this.lenCase218Field1 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase218Field1, this._io, "/types/case__218_field1_0/seq/0");
      }
      this._raw_case218Field1 = this._io.readBytes(this.lenCase218Field1);
      var _io__raw_case218Field1 = new KaitaiStream(this._raw_case218Field1);
      this.case218Field1 = new Case218Field1(_io__raw_case218Field1, this, this._root);
    }

    return Case218Field10;
  })();

  var Case169EltField0 = Id015PtlimaptOperation.Case169EltField0 = (function() {
    function Case169EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case169EltField0.prototype._read = function() {
      this.case169EltField0 = this._io.readBytesFull();
    }

    return Case169EltField0;
  })();

  var Case168EltField00 = Id015PtlimaptOperation.Case168EltField00 = (function() {
    function Case168EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case168EltField00.prototype._read = function() {
      this.lenCase168EltField0 = this._io.readU1();
      if (!(this.lenCase168EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase168EltField0, this._io, "/types/case__168_elt_field0_0/seq/0");
      }
      this._raw_case168EltField0 = this._io.readBytes(this.lenCase168EltField0);
      var _io__raw_case168EltField0 = new KaitaiStream(this._raw_case168EltField0);
      this.case168EltField0 = new Case168EltField0(_io__raw_case168EltField0, this, this._root);
    }

    return Case168EltField00;
  })();

  var Case140Entries = Id015PtlimaptOperation.Case140Entries = (function() {
    function Case140Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case140Entries.prototype._read = function() {
      this.case140EltField0 = new Case140EltField00(this._io, this, this._root);
      this.case140EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case140Entries;
  })();

  var Case182Entries = Id015PtlimaptOperation.Case182Entries = (function() {
    function Case182Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case182Entries.prototype._read = function() {
      this.case182EltField0 = new Case182EltField00(this._io, this, this._root);
      this.case182EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case182Entries;
  })();

  var Parameters = Id015PtlimaptOperation.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id015PtlimaptEntrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var Some = Id015PtlimaptOperation.Some = (function() {
    function Some(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Some.prototype._read = function() {
      this.contents = new Micheline015PtlimaptMichelsonV1Expression(this._io, this, this._root);
      this.ty = new Micheline015PtlimaptMichelsonV1Expression(this._io, this, this._root);
      this.ticketer = new Id015PtlimaptContractId(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Some;
  })();

  var Case25 = Id015PtlimaptOperation.Case25 = (function() {
    function Case25(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case25.prototype._read = function() {
      this.case25Field0 = this._io.readU2be();
      this.case25Field1 = [];
      for (var i = 0; i < 6; i++) {
        this.case25Field1.push(new Case25Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__25_field1_entries
     */

    return Case25;
  })();

  var Case4Field1Entries = Id015PtlimaptOperation.Case4Field1Entries = (function() {
    function Case4Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case4Field1Entries.prototype._read = function() {
      this.case4Field1EltField0 = this._io.readU1();
      this.case4Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case4Field1Entries;
  })();

  var Messages0 = Id015PtlimaptOperation.Messages0 = (function() {
    function Messages0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Messages0.prototype._read = function() {
      this.lenMessages = this._io.readU4be();
      if (!(this.lenMessages <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessages, this._io, "/types/messages_0/seq/0");
      }
      this._raw_messages = this._io.readBytes(this.lenMessages);
      var _io__raw_messages = new KaitaiStream(this._raw_messages);
      this.messages = new Messages(_io__raw_messages, this, this._root);
    }

    return Messages0;
  })();

  var Case171Entries = Id015PtlimaptOperation.Case171Entries = (function() {
    function Case171Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case171Entries.prototype._read = function() {
      this.case171EltField0 = new Case171EltField00(this._io, this, this._root);
      this.case171EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case171Entries;
  })();

  var Prim2ArgsSomeAnnots = Id015PtlimaptOperation.Prim2ArgsSomeAnnots = (function() {
    function Prim2ArgsSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim2ArgsSomeAnnots.prototype._read = function() {
      this.prim = new Id015PtlimaptMichelsonV1Primitives(this._io, this, this._root);
      this.arg1 = new Micheline015PtlimaptMichelsonV1Expression(this._io, this, this._root);
      this.arg2 = new Micheline015PtlimaptMichelsonV1Expression(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return Prim2ArgsSomeAnnots;
  })();

  var Case1920 = Id015PtlimaptOperation.Case1920 = (function() {
    function Case1920(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case1920.prototype._read = function() {
      this.lenCase192 = this._io.readU1();
      if (!(this.lenCase192 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase192, this._io, "/types/case__192_0/seq/0");
      }
      this._raw_case192 = this._io.readBytes(this.lenCase192);
      var _io__raw_case192 = new KaitaiStream(this._raw_case192);
      this.case192 = new Case192(_io__raw_case192, this, this._root);
    }

    return Case1920;
  })();

  var Proof0 = Id015PtlimaptOperation.Proof0 = (function() {
    function Proof0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proof0.prototype._read = function() {
      this.proofTag = this._io.readU1();
      if (this.proofTag == Id015PtlimaptOperation.ProofTag.CASE__0) {
        this.case0 = new Case00(this._io, this, this._root);
      }
      if (this.proofTag == Id015PtlimaptOperation.ProofTag.CASE__2) {
        this.case2 = new Case2(this._io, this, this._root);
      }
      if (this.proofTag == Id015PtlimaptOperation.ProofTag.CASE__1) {
        this.case1 = new Case1(this._io, this, this._root);
      }
      if (this.proofTag == Id015PtlimaptOperation.ProofTag.CASE__3) {
        this.case3 = new Case3(this._io, this, this._root);
      }
    }

    return Proof0;
  })();

  var Case146Entries = Id015PtlimaptOperation.Case146Entries = (function() {
    function Case146Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case146Entries.prototype._read = function() {
      this.case146EltField0 = new Case146EltField00(this._io, this, this._root);
      this.case146EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case146Entries;
  })();

  var Case167EltField00 = Id015PtlimaptOperation.Case167EltField00 = (function() {
    function Case167EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case167EltField00.prototype._read = function() {
      this.lenCase167EltField0 = this._io.readU1();
      if (!(this.lenCase167EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase167EltField0, this._io, "/types/case__167_elt_field0_0/seq/0");
      }
      this._raw_case167EltField0 = this._io.readBytes(this.lenCase167EltField0);
      var _io__raw_case167EltField0 = new KaitaiStream(this._raw_case167EltField0);
      this.case167EltField0 = new Case167EltField0(_io__raw_case167EltField0, this, this._root);
    }

    return Case167EltField00;
  })();

  var Case164EltField00 = Id015PtlimaptOperation.Case164EltField00 = (function() {
    function Case164EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case164EltField00.prototype._read = function() {
      this.lenCase164EltField0 = this._io.readU1();
      if (!(this.lenCase164EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase164EltField0, this._io, "/types/case__164_elt_field0_0/seq/0");
      }
      this._raw_case164EltField0 = this._io.readBytes(this.lenCase164EltField0);
      var _io__raw_case164EltField0 = new KaitaiStream(this._raw_case164EltField0);
      this.case164EltField0 = new Case164EltField0(_io__raw_case164EltField0, this, this._root);
    }

    return Case164EltField00;
  })();

  var Proposals1 = Id015PtlimaptOperation.Proposals1 = (function() {
    function Proposals1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals1.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposals = new Proposals0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Proposals1;
  })();

  var TxRollupSubmitBatch = Id015PtlimaptOperation.TxRollupSubmitBatch = (function() {
    function TxRollupSubmitBatch(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollupSubmitBatch.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.rollup = new Id015PtlimaptTxRollupId(this._io, this, this._root);
      this.content = new BytesDynUint30(this._io, this, this._root);
      this.burnLimitTag = this._io.readU1();
      if (this.burnLimitTag == Id015PtlimaptOperation.Bool.TRUE) {
        this.burnLimit = new Id015PtlimaptMutez(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return TxRollupSubmitBatch;
  })();

  var Case64 = Id015PtlimaptOperation.Case64 = (function() {
    function Case64(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case64.prototype._read = function() {
      this.case64Field0 = this._io.readU1();
      this.case64Field1 = [];
      for (var i = 0; i < 32; i++) {
        this.case64Field1.push(new Case64Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__64_field1_entries
     */

    return Case64;
  })();

  var Case188EltField0 = Id015PtlimaptOperation.Case188EltField0 = (function() {
    function Case188EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case188EltField0.prototype._read = function() {
      this.case188EltField0 = this._io.readBytesFull();
    }

    return Case188EltField0;
  })();

  var Case33Field1Entries = Id015PtlimaptOperation.Case33Field1Entries = (function() {
    function Case33Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case33Field1Entries.prototype._read = function() {
      this.case33Field1EltField0 = this._io.readU1();
      this.case33Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case33Field1Entries;
  })();

  var Case183EltField00 = Id015PtlimaptOperation.Case183EltField00 = (function() {
    function Case183EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case183EltField00.prototype._read = function() {
      this.lenCase183EltField0 = this._io.readU1();
      if (!(this.lenCase183EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase183EltField0, this._io, "/types/case__183_elt_field0_0/seq/0");
      }
      this._raw_case183EltField0 = this._io.readBytes(this.lenCase183EltField0);
      var _io__raw_case183EltField0 = new KaitaiStream(this._raw_case183EltField0);
      this.case183EltField0 = new Case183EltField0(_io__raw_case183EltField0, this, this._root);
    }

    return Case183EltField00;
  })();

  var Case151EltField00 = Id015PtlimaptOperation.Case151EltField00 = (function() {
    function Case151EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case151EltField00.prototype._read = function() {
      this.lenCase151EltField0 = this._io.readU1();
      if (!(this.lenCase151EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase151EltField0, this._io, "/types/case__151_elt_field0_0/seq/0");
      }
      this._raw_case151EltField0 = this._io.readBytes(this.lenCase151EltField0);
      var _io__raw_case151EltField0 = new KaitaiStream(this._raw_case151EltField0);
      this.case151EltField0 = new Case151EltField0(_io__raw_case151EltField0, this, this._root);
    }

    return Case151EltField00;
  })();

  var DissectionEntries = Id015PtlimaptOperation.DissectionEntries = (function() {
    function DissectionEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DissectionEntries.prototype._read = function() {
      this.stateTag = this._io.readU1();
      if (this.stateTag == Id015PtlimaptOperation.Bool.TRUE) {
        this.state = this._io.readBytes(32);
      }
      this.tick = new N(this._io, this, this._root);
    }

    return DissectionEntries;
  })();

  var Case130EltField00 = Id015PtlimaptOperation.Case130EltField00 = (function() {
    function Case130EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case130EltField00.prototype._read = function() {
      this.lenCase130EltField0 = this._io.readU1();
      if (!(this.lenCase130EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase130EltField0, this._io, "/types/case__130_elt_field0_0/seq/0");
      }
      this._raw_case130EltField0 = this._io.readBytes(this.lenCase130EltField0);
      var _io__raw_case130EltField0 = new KaitaiStream(this._raw_case130EltField0);
      this.case130EltField0 = new Case130EltField0(_io__raw_case130EltField0, this, this._root);
    }

    return Case130EltField00;
  })();

  var TicketsInfo = Id015PtlimaptOperation.TicketsInfo = (function() {
    function TicketsInfo(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TicketsInfo.prototype._read = function() {
      this.ticketsInfoEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.ticketsInfoEntries.push(new TicketsInfoEntries(this._io, this, this._root));
        i++;
      }
    }

    return TicketsInfo;
  })();

  var Case141EltField0 = Id015PtlimaptOperation.Case141EltField0 = (function() {
    function Case141EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case141EltField0.prototype._read = function() {
      this.case141EltField0 = this._io.readBytesFull();
    }

    return Case141EltField0;
  })();

  var Case166EltField00 = Id015PtlimaptOperation.Case166EltField00 = (function() {
    function Case166EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case166EltField00.prototype._read = function() {
      this.lenCase166EltField0 = this._io.readU1();
      if (!(this.lenCase166EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase166EltField0, this._io, "/types/case__166_elt_field0_0/seq/0");
      }
      this._raw_case166EltField0 = this._io.readBytes(this.lenCase166EltField0);
      var _io__raw_case166EltField0 = new KaitaiStream(this._raw_case166EltField0);
      this.case166EltField0 = new Case166EltField0(_io__raw_case166EltField0, this, this._root);
    }

    return Case166EltField00;
  })();

  var Case182EltField0 = Id015PtlimaptOperation.Case182EltField0 = (function() {
    function Case182EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case182EltField0.prototype._read = function() {
      this.case182EltField0 = this._io.readBytesFull();
    }

    return Case182EltField0;
  })();

  var Case42 = Id015PtlimaptOperation.Case42 = (function() {
    function Case42(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case42.prototype._read = function() {
      this.case42Field0 = this._io.readS4be();
      this.case42Field1 = [];
      for (var i = 0; i < 10; i++) {
        this.case42Field1.push(new Case42Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__42_field1_entries
     */

    return Case42;
  })();

  var Case148Entries = Id015PtlimaptOperation.Case148Entries = (function() {
    function Case148Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case148Entries.prototype._read = function() {
      this.case148EltField0 = new Case148EltField00(this._io, this, this._root);
      this.case148EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case148Entries;
  })();

  var Ballot = Id015PtlimaptOperation.Ballot = (function() {
    function Ballot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Ballot.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposal = this._io.readBytes(32);
      this.ballot = this._io.readS1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Ballot;
  })();

  var Case165EltField00 = Id015PtlimaptOperation.Case165EltField00 = (function() {
    function Case165EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case165EltField00.prototype._read = function() {
      this.lenCase165EltField0 = this._io.readU1();
      if (!(this.lenCase165EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase165EltField0, this._io, "/types/case__165_elt_field0_0/seq/0");
      }
      this._raw_case165EltField0 = this._io.readBytes(this.lenCase165EltField0);
      var _io__raw_case165EltField0 = new KaitaiStream(this._raw_case165EltField0);
      this.case165EltField0 = new Case165EltField0(_io__raw_case165EltField0, this, this._root);
    }

    return Case165EltField00;
  })();

  var Predecessor = Id015PtlimaptOperation.Predecessor = (function() {
    function Predecessor(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Predecessor.prototype._read = function() {
      this.predecessorTag = this._io.readU1();
      if (this.predecessorTag == Id015PtlimaptOperation.PredecessorTag.SOME) {
        this.some = this._io.readBytes(32);
      }
    }

    return Predecessor;
  })();

  var DalPublishSlotHeader = Id015PtlimaptOperation.DalPublishSlotHeader = (function() {
    function DalPublishSlotHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalPublishSlotHeader.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.slot = new Slot(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return DalPublishSlotHeader;
  })();

  var PublicKeyHash = Id015PtlimaptOperation.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id015PtlimaptOperation.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id015PtlimaptOperation.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id015PtlimaptOperation.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var MessagePath0 = Id015PtlimaptOperation.MessagePath0 = (function() {
    function MessagePath0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MessagePath0.prototype._read = function() {
      this.lenMessagePath = this._io.readU4be();
      if (!(this.lenMessagePath <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenMessagePath, this._io, "/types/message_path_0/seq/0");
      }
      this._raw_messagePath = this._io.readBytes(this.lenMessagePath);
      var _io__raw_messagePath = new KaitaiStream(this._raw_messagePath);
      this.messagePath = new MessagePath(_io__raw_messagePath, this, this._root);
    }

    return MessagePath0;
  })();

  var Case154Entries = Id015PtlimaptOperation.Case154Entries = (function() {
    function Case154Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case154Entries.prototype._read = function() {
      this.case154EltField0 = new Case154EltField00(this._io, this, this._root);
      this.case154EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case154Entries;
  })();

  var Case18 = Id015PtlimaptOperation.Case18 = (function() {
    function Case18(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case18.prototype._read = function() {
      this.case18Field0 = this._io.readS4be();
      this.case18Field1 = [];
      for (var i = 0; i < 4; i++) {
        this.case18Field1.push(new Case18Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__18_field1_entries
     */

    return Case18;
  })();

  var Case50 = Id015PtlimaptOperation.Case50 = (function() {
    function Case50(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case50.prototype._read = function() {
      this.case50Field0 = this._io.readS4be();
      this.case50Field1 = [];
      for (var i = 0; i < 12; i++) {
        this.case50Field1.push(new Case50Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__50_field1_entries
     */

    return Case50;
  })();

  var Op11 = Id015PtlimaptOperation.Op11 = (function() {
    function Op11(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op11.prototype._read = function() {
      this.id015PtlimaptInlinedPreendorsement = new Id015PtlimaptInlinedPreendorsement(this._io, this, this._root);
    }

    return Op11;
  })();

  var Case171EltField00 = Id015PtlimaptOperation.Case171EltField00 = (function() {
    function Case171EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case171EltField00.prototype._read = function() {
      this.lenCase171EltField0 = this._io.readU1();
      if (!(this.lenCase171EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase171EltField0, this._io, "/types/case__171_elt_field0_0/seq/0");
      }
      this._raw_case171EltField0 = this._io.readBytes(this.lenCase171EltField0);
      var _io__raw_case171EltField0 = new KaitaiStream(this._raw_case171EltField0);
      this.case171EltField0 = new Case171EltField0(_io__raw_case171EltField0, this, this._root);
    }

    return Case171EltField00;
  })();

  var Proposals = Id015PtlimaptOperation.Proposals = (function() {
    function Proposals(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals.prototype._read = function() {
      this.proposalsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.proposalsEntries.push(new ProposalsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Proposals;
  })();

  var Case146EltField00 = Id015PtlimaptOperation.Case146EltField00 = (function() {
    function Case146EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case146EltField00.prototype._read = function() {
      this.lenCase146EltField0 = this._io.readU1();
      if (!(this.lenCase146EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase146EltField0, this._io, "/types/case__146_elt_field0_0/seq/0");
      }
      this._raw_case146EltField0 = this._io.readBytes(this.lenCase146EltField0);
      var _io__raw_case146EltField0 = new KaitaiStream(this._raw_case146EltField0);
      this.case146EltField0 = new Case146EltField0(_io__raw_case146EltField0, this, this._root);
    }

    return Case146EltField00;
  })();

  var Case61Field1Entries = Id015PtlimaptOperation.Case61Field1Entries = (function() {
    function Case61Field1Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case61Field1Entries.prototype._read = function() {
      this.case61Field1EltField0 = this._io.readU1();
      this.case61Field1EltField1 = new InodeTree(this._io, this, this._root);
    }

    return Case61Field1Entries;
  })();

  var Z = Id015PtlimaptOperation.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  var Case36 = Id015PtlimaptOperation.Case36 = (function() {
    function Case36(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case36.prototype._read = function() {
      this.case36Field0 = this._io.readU1();
      this.case36Field1 = [];
      for (var i = 0; i < 9; i++) {
        this.case36Field1.push(new Case36Field1Entries(this._io, this, this._root));
      }
    }

    /**
     * case__36_field1_entries
     */

    return Case36;
  })();

  var OpEntries = Id015PtlimaptOperation.OpEntries = (function() {
    function OpEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    OpEntries.prototype._read = function() {
      this.opEltField0 = new OpEltField0(this._io, this, this._root);
      this.opEltField1 = new OpEltField1(this._io, this, this._root);
    }

    return OpEntries;
  })();

  var Case158Entries = Id015PtlimaptOperation.Case158Entries = (function() {
    function Case158Entries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case158Entries.prototype._read = function() {
      this.case158EltField0 = new Case158EltField00(this._io, this, this._root);
      this.case158EltField1 = new TreeEncoding(this._io, this, this._root);
    }

    return Case158Entries;
  })();

  var Id015PtlimaptBlockHeaderAlphaUnsignedContents = Id015PtlimaptOperation.Id015PtlimaptBlockHeaderAlphaUnsignedContents = (function() {
    function Id015PtlimaptBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id015PtlimaptOperation.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id015PtlimaptLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id015PtlimaptBlockHeaderAlphaUnsignedContents;
  })();

  var ProposalsEntries = Id015PtlimaptOperation.ProposalsEntries = (function() {
    function ProposalsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ProposalsEntries.prototype._read = function() {
      this.protocolHash = this._io.readBytes(32);
    }

    return ProposalsEntries;
  })();

  var Op1 = Id015PtlimaptOperation.Op1 = (function() {
    function Op1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op1.prototype._read = function() {
      this.id015PtlimaptInlinedEndorsement = new Id015PtlimaptInlinedEndorsement(this._io, this, this._root);
    }

    return Op1;
  })();

  var ZkRollupPublish = Id015PtlimaptOperation.ZkRollupPublish = (function() {
    function ZkRollupPublish(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ZkRollupPublish.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id015PtlimaptMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.zkRollup = this._io.readBytes(20);
      this.op = new Op0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return ZkRollupPublish;
  })();

  var Case134EltField0 = Id015PtlimaptOperation.Case134EltField0 = (function() {
    function Case134EltField0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case134EltField0.prototype._read = function() {
      this.case134EltField0 = this._io.readBytesFull();
    }

    return Case134EltField0;
  })();

  var Op = Id015PtlimaptOperation.Op = (function() {
    function Op(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op.prototype._read = function() {
      this.opEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.opEntries.push(new OpEntries(this._io, this, this._root));
        i++;
      }
    }

    return Op;
  })();

  var DoublePreendorsementEvidence = Id015PtlimaptOperation.DoublePreendorsementEvidence = (function() {
    function DoublePreendorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoublePreendorsementEvidence.prototype._read = function() {
      this.op1 = new Op12(this._io, this, this._root);
      this.op2 = new Op22(this._io, this, this._root);
    }

    return DoublePreendorsementEvidence;
  })();

  var Case137EltField00 = Id015PtlimaptOperation.Case137EltField00 = (function() {
    function Case137EltField00(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Case137EltField00.prototype._read = function() {
      this.lenCase137EltField0 = this._io.readU1();
      if (!(this.lenCase137EltField0 <= 255)) {
        throw new KaitaiStream.ValidationGreaterThanError(255, this.lenCase137EltField0, this._io, "/types/case__137_elt_field0_0/seq/0");
      }
      this._raw_case137EltField0 = this._io.readBytes(this.lenCase137EltField0);
      var _io__raw_case137EltField0 = new KaitaiStream(this._raw_case137EltField0);
      this.case137EltField0 = new Case137EltField0(_io__raw_case137EltField0, this, this._root);
    }

    return Case137EltField00;
  })();

  return Id015PtlimaptOperation;
})();
return Id015PtlimaptOperation;
}));
