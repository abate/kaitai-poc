// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id016PtmumbaiNonce = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 016-PtMumbai.nonce
 */

var Id016PtmumbaiNonce = (function() {
  function Id016PtmumbaiNonce(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiNonce.prototype._read = function() {
    this.id016PtmumbaiNonce = this._io.readBytes(32);
  }

  return Id016PtmumbaiNonce;
})();
return Id016PtmumbaiNonce;
}));
