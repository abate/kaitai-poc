// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell', './OperationShellHeader'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'), require('./OperationShellHeader'));
  } else {
    root.Id009PsflorenOperationProtocolData = factory(root.KaitaiStream, root.BlockHeaderShell, root.OperationShellHeader);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell, OperationShellHeader) {
/**
 * Encoding id: 009-PsFLoren.operation.protocol_data
 */

var Id009PsflorenOperationProtocolData = (function() {
  Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag = Object.freeze({
    ENDORSEMENT: 0,
    SEED_NONCE_REVELATION: 1,
    DOUBLE_ENDORSEMENT_EVIDENCE: 2,
    DOUBLE_BAKING_EVIDENCE: 3,
    ACTIVATE_ACCOUNT: 4,
    PROPOSALS: 5,
    BALLOT: 6,
    ENDORSEMENT_WITH_SLOT: 10,
    FAILING_NOOP: 17,
    REVEAL: 107,
    TRANSACTION: 108,
    ORIGINATION: 109,
    DELEGATION: 110,

    0: "ENDORSEMENT",
    1: "SEED_NONCE_REVELATION",
    2: "DOUBLE_ENDORSEMENT_EVIDENCE",
    3: "DOUBLE_BAKING_EVIDENCE",
    4: "ACTIVATE_ACCOUNT",
    5: "PROPOSALS",
    6: "BALLOT",
    10: "ENDORSEMENT_WITH_SLOT",
    17: "FAILING_NOOP",
    107: "REVEAL",
    108: "TRANSACTION",
    109: "ORIGINATION",
    110: "DELEGATION",
  });

  Id009PsflorenOperationProtocolData.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id009PsflorenOperationProtocolData.Id009PsflorenContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id009PsflorenOperationProtocolData.Id009PsflorenEntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    255: "NAMED",
  });

  Id009PsflorenOperationProtocolData.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id009PsflorenOperationProtocolData.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id009PsflorenOperationProtocolData.Id009PsflorenInlinedEndorsementContentsTag = Object.freeze({
    ENDORSEMENT: 0,

    0: "ENDORSEMENT",
  });

  function Id009PsflorenOperationProtocolData(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id009PsflorenOperationProtocolData.prototype._read = function() {
    this.id009PsflorenOperationAlphaContentsAndSignature = new Id009PsflorenOperationAlphaContentsAndSignature(this._io, this, this._root);
  }

  var Op20 = Id009PsflorenOperationProtocolData.Op20 = (function() {
    function Op20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op20.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_0/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op2(_io__raw_op2, this, this._root);
    }

    return Op20;
  })();

  var Id009PsflorenContractId = Id009PsflorenOperationProtocolData.Id009PsflorenContractId = (function() {
    function Id009PsflorenContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenContractId.prototype._read = function() {
      this.id009PsflorenContractIdTag = this._io.readU1();
      if (this.id009PsflorenContractIdTag == Id009PsflorenOperationProtocolData.Id009PsflorenContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id009PsflorenContractIdTag == Id009PsflorenOperationProtocolData.Id009PsflorenContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id009PsflorenContractId;
  })();

  var ActivateAccount = Id009PsflorenOperationProtocolData.ActivateAccount = (function() {
    function ActivateAccount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ActivateAccount.prototype._read = function() {
      this.pkh = this._io.readBytes(20);
      this.secret = this._io.readBytes(20);
    }

    return ActivateAccount;
  })();

  var DoubleEndorsementEvidence = Id009PsflorenOperationProtocolData.DoubleEndorsementEvidence = (function() {
    function DoubleEndorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleEndorsementEvidence.prototype._read = function() {
      this.op1 = new Op10(this._io, this, this._root);
      this.op2 = new Op20(this._io, this, this._root);
      this.slot = this._io.readU2be();
    }

    return DoubleEndorsementEvidence;
  })();

  var Id009PsflorenScriptedContracts = Id009PsflorenOperationProtocolData.Id009PsflorenScriptedContracts = (function() {
    function Id009PsflorenScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id009PsflorenScriptedContracts;
  })();

  var Originated = Id009PsflorenOperationProtocolData.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var N = Id009PsflorenOperationProtocolData.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Endorsement = Id009PsflorenOperationProtocolData.Endorsement = (function() {
    function Endorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Endorsement.prototype._read = function() {
      this.id009PsflorenInlinedEndorsement = new Id009PsflorenInlinedEndorsement(this._io, this, this._root);
    }

    return Endorsement;
  })();

  var Proposals0 = Id009PsflorenOperationProtocolData.Proposals0 = (function() {
    function Proposals0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals0.prototype._read = function() {
      this.lenProposals = this._io.readU4be();
      if (!(this.lenProposals <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenProposals, this._io, "/types/proposals_0/seq/0");
      }
      this._raw_proposals = this._io.readBytes(this.lenProposals);
      var _io__raw_proposals = new KaitaiStream(this._raw_proposals);
      this.proposals = new Proposals(_io__raw_proposals, this, this._root);
    }

    return Proposals0;
  })();

  var Reveal = Id009PsflorenOperationProtocolData.Reveal = (function() {
    function Reveal(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Reveal.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id009PsflorenMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.publicKey = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Reveal;
  })();

  var Endorsement0 = Id009PsflorenOperationProtocolData.Endorsement0 = (function() {
    function Endorsement0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Endorsement0.prototype._read = function() {
      this.lenEndorsement = this._io.readU4be();
      if (!(this.lenEndorsement <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenEndorsement, this._io, "/types/endorsement_0/seq/0");
      }
      this._raw_endorsement = this._io.readBytes(this.lenEndorsement);
      var _io__raw_endorsement = new KaitaiStream(this._raw_endorsement);
      this.endorsement = new Endorsement(_io__raw_endorsement, this, this._root);
    }

    return Endorsement0;
  })();

  var SeedNonceRevelation = Id009PsflorenOperationProtocolData.SeedNonceRevelation = (function() {
    function SeedNonceRevelation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SeedNonceRevelation.prototype._read = function() {
      this.level = this._io.readS4be();
      this.nonce = this._io.readBytes(32);
    }

    return SeedNonceRevelation;
  })();

  var Id009PsflorenBlockHeaderAlphaSignedContents = Id009PsflorenOperationProtocolData.Id009PsflorenBlockHeaderAlphaSignedContents = (function() {
    function Id009PsflorenBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id009PsflorenBlockHeaderAlphaUnsignedContents = new Id009PsflorenBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id009PsflorenBlockHeaderAlphaSignedContents;
  })();

  var Id009PsflorenMutez = Id009PsflorenOperationProtocolData.Id009PsflorenMutez = (function() {
    function Id009PsflorenMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenMutez.prototype._read = function() {
      this.id009PsflorenMutez = new N(this._io, this, this._root);
    }

    return Id009PsflorenMutez;
  })();

  var DoubleBakingEvidence = Id009PsflorenOperationProtocolData.DoubleBakingEvidence = (function() {
    function DoubleBakingEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleBakingEvidence.prototype._read = function() {
      this.bh1 = new Bh10(this._io, this, this._root);
      this.bh2 = new Bh20(this._io, this, this._root);
    }

    return DoubleBakingEvidence;
  })();

  var PublicKey = Id009PsflorenOperationProtocolData.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id009PsflorenOperationProtocolData.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id009PsflorenOperationProtocolData.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id009PsflorenOperationProtocolData.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var Bh2 = Id009PsflorenOperationProtocolData.Bh2 = (function() {
    function Bh2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh2.prototype._read = function() {
      this.id009PsflorenBlockHeaderAlphaFullHeader = new Id009PsflorenBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh2;
  })();

  var Named = Id009PsflorenOperationProtocolData.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Bh20 = Id009PsflorenOperationProtocolData.Bh20 = (function() {
    function Bh20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh20.prototype._read = function() {
      this.lenBh2 = this._io.readU4be();
      if (!(this.lenBh2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh2, this._io, "/types/bh2_0/seq/0");
      }
      this._raw_bh2 = this._io.readBytes(this.lenBh2);
      var _io__raw_bh2 = new KaitaiStream(this._raw_bh2);
      this.bh2 = new Bh2(_io__raw_bh2, this, this._root);
    }

    return Bh20;
  })();

  var Delegation = Id009PsflorenOperationProtocolData.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id009PsflorenMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id009PsflorenOperationProtocolData.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var ContentsEntries = Id009PsflorenOperationProtocolData.ContentsEntries = (function() {
    function ContentsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ContentsEntries.prototype._read = function() {
      this.id009PsflorenOperationAlphaContents = new Id009PsflorenOperationAlphaContents(this._io, this, this._root);
    }

    return ContentsEntries;
  })();

  var Id009PsflorenBlockHeaderAlphaFullHeader = Id009PsflorenOperationProtocolData.Id009PsflorenBlockHeaderAlphaFullHeader = (function() {
    function Id009PsflorenBlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenBlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id009PsflorenBlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id009PsflorenBlockHeaderAlphaSignedContents = new Id009PsflorenBlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id009PsflorenBlockHeaderAlphaFullHeader;
  })();

  var Bh10 = Id009PsflorenOperationProtocolData.Bh10 = (function() {
    function Bh10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh10.prototype._read = function() {
      this.lenBh1 = this._io.readU4be();
      if (!(this.lenBh1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh1, this._io, "/types/bh1_0/seq/0");
      }
      this._raw_bh1 = this._io.readBytes(this.lenBh1);
      var _io__raw_bh1 = new KaitaiStream(this._raw_bh1);
      this.bh1 = new Bh1(_io__raw_bh1, this, this._root);
    }

    return Bh10;
  })();

  var BytesDynUint30 = Id009PsflorenOperationProtocolData.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Bh1 = Id009PsflorenOperationProtocolData.Bh1 = (function() {
    function Bh1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh1.prototype._read = function() {
      this.id009PsflorenBlockHeaderAlphaFullHeader = new Id009PsflorenBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh1;
  })();

  var Id009PsflorenBlockHeaderAlphaUnsignedContents = Id009PsflorenOperationProtocolData.Id009PsflorenBlockHeaderAlphaUnsignedContents = (function() {
    function Id009PsflorenBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id009PsflorenOperationProtocolData.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
    }

    return Id009PsflorenBlockHeaderAlphaUnsignedContents;
  })();

  var Id009PsflorenOperationAlphaContentsAndSignature = Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsAndSignature = (function() {
    function Id009PsflorenOperationAlphaContentsAndSignature(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenOperationAlphaContentsAndSignature.prototype._read = function() {
      this.contents = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.contents.push(new ContentsEntries(this._io, this, this._root));
        i++;
      }
      this.signature = this._io.readBytes(64);
    }

    return Id009PsflorenOperationAlphaContentsAndSignature;
  })();

  var Id009PsflorenInlinedEndorsement = Id009PsflorenOperationProtocolData.Id009PsflorenInlinedEndorsement = (function() {
    function Id009PsflorenInlinedEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenInlinedEndorsement.prototype._read = function() {
      this.id009PsflorenInlinedEndorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id009PsflorenInlinedEndorsementContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id009PsflorenOperationProtocolData.Bool.TRUE) {
        this.signature = this._io.readBytes(64);
      }
    }

    return Id009PsflorenInlinedEndorsement;
  })();

  var Op10 = Id009PsflorenOperationProtocolData.Op10 = (function() {
    function Op10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op10.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_0/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op1(_io__raw_op1, this, this._root);
    }

    return Op10;
  })();

  var EndorsementWithSlot = Id009PsflorenOperationProtocolData.EndorsementWithSlot = (function() {
    function EndorsementWithSlot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    EndorsementWithSlot.prototype._read = function() {
      this.endorsement = new Endorsement0(this._io, this, this._root);
      this.slot = this._io.readU2be();
    }

    return EndorsementWithSlot;
  })();

  var Id009PsflorenEntrypoint = Id009PsflorenOperationProtocolData.Id009PsflorenEntrypoint = (function() {
    function Id009PsflorenEntrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenEntrypoint.prototype._read = function() {
      this.id009PsflorenEntrypointTag = this._io.readU1();
      if (this.id009PsflorenEntrypointTag == Id009PsflorenOperationProtocolData.Id009PsflorenEntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id009PsflorenEntrypoint;
  })();

  var Origination = Id009PsflorenOperationProtocolData.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id009PsflorenMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.balance = new Id009PsflorenMutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id009PsflorenOperationProtocolData.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id009PsflorenScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var Op2 = Id009PsflorenOperationProtocolData.Op2 = (function() {
    function Op2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op2.prototype._read = function() {
      this.id009PsflorenInlinedEndorsement = new Id009PsflorenInlinedEndorsement(this._io, this, this._root);
    }

    return Op2;
  })();

  var Id009PsflorenInlinedEndorsementContents = Id009PsflorenOperationProtocolData.Id009PsflorenInlinedEndorsementContents = (function() {
    function Id009PsflorenInlinedEndorsementContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenInlinedEndorsementContents.prototype._read = function() {
      this.id009PsflorenInlinedEndorsementContentsTag = this._io.readU1();
      if (this.id009PsflorenInlinedEndorsementContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenInlinedEndorsementContentsTag.ENDORSEMENT) {
        this.endorsement = this._io.readS4be();
      }
    }

    return Id009PsflorenInlinedEndorsementContents;
  })();

  var Id009PsflorenOperationAlphaContents = Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContents = (function() {
    function Id009PsflorenOperationAlphaContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenOperationAlphaContents.prototype._read = function() {
      this.id009PsflorenOperationAlphaContentsTag = this._io.readU1();
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.ENDORSEMENT) {
        this.endorsement = this._io.readS4be();
      }
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.SEED_NONCE_REVELATION) {
        this.seedNonceRevelation = new SeedNonceRevelation(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.ENDORSEMENT_WITH_SLOT) {
        this.endorsementWithSlot = new EndorsementWithSlot(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.DOUBLE_ENDORSEMENT_EVIDENCE) {
        this.doubleEndorsementEvidence = new DoubleEndorsementEvidence(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.DOUBLE_BAKING_EVIDENCE) {
        this.doubleBakingEvidence = new DoubleBakingEvidence(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.ACTIVATE_ACCOUNT) {
        this.activateAccount = new ActivateAccount(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.PROPOSALS) {
        this.proposals = new Proposals1(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.BALLOT) {
        this.ballot = new Ballot(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.REVEAL) {
        this.reveal = new Reveal(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
      if (this.id009PsflorenOperationAlphaContentsTag == Id009PsflorenOperationProtocolData.Id009PsflorenOperationAlphaContentsTag.FAILING_NOOP) {
        this.failingNoop = new BytesDynUint30(this._io, this, this._root);
      }
    }

    return Id009PsflorenOperationAlphaContents;
  })();

  var NChunk = Id009PsflorenOperationProtocolData.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Named0 = Id009PsflorenOperationProtocolData.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id009PsflorenOperationProtocolData.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id009PsflorenMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.amount = new Id009PsflorenMutez(this._io, this, this._root);
      this.destination = new Id009PsflorenContractId(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id009PsflorenOperationProtocolData.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Transaction;
  })();

  var Parameters = Id009PsflorenOperationProtocolData.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id009PsflorenEntrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var Proposals1 = Id009PsflorenOperationProtocolData.Proposals1 = (function() {
    function Proposals1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals1.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposals = new Proposals0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Proposals1;
  })();

  var Ballot = Id009PsflorenOperationProtocolData.Ballot = (function() {
    function Ballot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Ballot.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposal = this._io.readBytes(32);
      this.ballot = this._io.readS1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Ballot;
  })();

  var PublicKeyHash = Id009PsflorenOperationProtocolData.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id009PsflorenOperationProtocolData.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id009PsflorenOperationProtocolData.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id009PsflorenOperationProtocolData.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Proposals = Id009PsflorenOperationProtocolData.Proposals = (function() {
    function Proposals(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals.prototype._read = function() {
      this.proposalsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.proposalsEntries.push(new ProposalsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Proposals;
  })();

  var ProposalsEntries = Id009PsflorenOperationProtocolData.ProposalsEntries = (function() {
    function ProposalsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ProposalsEntries.prototype._read = function() {
      this.protocolHash = this._io.readBytes(32);
    }

    return ProposalsEntries;
  })();

  var Op1 = Id009PsflorenOperationProtocolData.Op1 = (function() {
    function Op1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op1.prototype._read = function() {
      this.id009PsflorenInlinedEndorsement = new Id009PsflorenInlinedEndorsement(this._io, this, this._root);
    }

    return Op1;
  })();

  return Id009PsflorenOperationProtocolData;
})();
return Id009PsflorenOperationProtocolData;
}));
