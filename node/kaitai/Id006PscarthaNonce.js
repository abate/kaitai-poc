// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id006PscarthaNonce = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 006-PsCARTHA.nonce
 */

var Id006PscarthaNonce = (function() {
  function Id006PscarthaNonce(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaNonce.prototype._read = function() {
    this.id006PscarthaNonce = this._io.readBytes(32);
  }

  return Id006PscarthaNonce;
})();
return Id006PscarthaNonce;
}));
