// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Protocol = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: protocol
 * Description: The environment a protocol relies on and the components a protocol is made of.
 */

var Protocol = (function() {
  Protocol.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Protocol(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Protocol.prototype._read = function() {
    this.expectedEnvVersion = new ProtocolEnvironmentVersion(this._io, this, this._root);
    this.components = new Components0(this._io, this, this._root);
  }

  var Components0 = Protocol.Components0 = (function() {
    function Components0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Components0.prototype._read = function() {
      this.lenComponents = this._io.readU4be();
      if (!(this.lenComponents <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenComponents, this._io, "/types/components_0/seq/0");
      }
      this._raw_components = this._io.readBytes(this.lenComponents);
      var _io__raw_components = new KaitaiStream(this._raw_components);
      this.components = new Components(_io__raw_components, this, this._root);
    }

    return Components0;
  })();

  var ComponentsEntries = Protocol.ComponentsEntries = (function() {
    function ComponentsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ComponentsEntries.prototype._read = function() {
      this.name = new BytesDynUint30(this._io, this, this._root);
      this.interfaceTag = this._io.readU1();
      if (this.interfaceTag == Protocol.Bool.TRUE) {
        this.interface = new BytesDynUint30(this._io, this, this._root);
      }
      this.implementation = new BytesDynUint30(this._io, this, this._root);
    }

    return ComponentsEntries;
  })();

  var Components = Protocol.Components = (function() {
    function Components(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Components.prototype._read = function() {
      this.componentsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.componentsEntries.push(new ComponentsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Components;
  })();

  var BytesDynUint30 = Protocol.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var ProtocolEnvironmentVersion = Protocol.ProtocolEnvironmentVersion = (function() {
    function ProtocolEnvironmentVersion(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ProtocolEnvironmentVersion.prototype._read = function() {
      this.protocolEnvironmentVersion = this._io.readU2be();
    }

    return ProtocolEnvironmentVersion;
  })();

  return Protocol;
})();
return Protocol;
}));
