// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id016PtmumbaiVoteBallot = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 016-PtMumbai.vote.ballot
 */

var Id016PtmumbaiVoteBallot = (function() {
  function Id016PtmumbaiVoteBallot(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiVoteBallot.prototype._read = function() {
    this.id016PtmumbaiVoteBallot = this._io.readS1();
  }

  return Id016PtmumbaiVoteBallot;
})();
return Id016PtmumbaiVoteBallot;
}));
