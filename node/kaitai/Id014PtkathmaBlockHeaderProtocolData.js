// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id014PtkathmaBlockHeaderProtocolData = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 014-PtKathma.block_header.protocol_data
 */

var Id014PtkathmaBlockHeaderProtocolData = (function() {
  Id014PtkathmaBlockHeaderProtocolData.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id014PtkathmaBlockHeaderProtocolData(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id014PtkathmaBlockHeaderProtocolData.prototype._read = function() {
    this.id014PtkathmaBlockHeaderAlphaSignedContents = new Id014PtkathmaBlockHeaderAlphaSignedContents(this._io, this, this._root);
  }

  var Id014PtkathmaBlockHeaderAlphaSignedContents = Id014PtkathmaBlockHeaderProtocolData.Id014PtkathmaBlockHeaderAlphaSignedContents = (function() {
    function Id014PtkathmaBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id014PtkathmaBlockHeaderAlphaUnsignedContents = new Id014PtkathmaBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id014PtkathmaBlockHeaderAlphaSignedContents;
  })();

  var Id014PtkathmaBlockHeaderAlphaUnsignedContents = Id014PtkathmaBlockHeaderProtocolData.Id014PtkathmaBlockHeaderAlphaUnsignedContents = (function() {
    function Id014PtkathmaBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id014PtkathmaBlockHeaderProtocolData.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id014PtkathmaLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id014PtkathmaBlockHeaderAlphaUnsignedContents;
  })();

  var Id014PtkathmaLiquidityBakingToggleVote = Id014PtkathmaBlockHeaderProtocolData.Id014PtkathmaLiquidityBakingToggleVote = (function() {
    function Id014PtkathmaLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaLiquidityBakingToggleVote.prototype._read = function() {
      this.id014PtkathmaLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id014PtkathmaLiquidityBakingToggleVote;
  })();

  return Id014PtkathmaBlockHeaderProtocolData;
})();
return Id014PtkathmaBlockHeaderProtocolData;
}));
