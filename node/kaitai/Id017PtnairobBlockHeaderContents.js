// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id017PtnairobBlockHeaderContents = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 017-PtNairob.block_header.contents
 */

var Id017PtnairobBlockHeaderContents = (function() {
  Id017PtnairobBlockHeaderContents.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id017PtnairobBlockHeaderContents(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id017PtnairobBlockHeaderContents.prototype._read = function() {
    this.id017PtnairobBlockHeaderAlphaUnsignedContents = new Id017PtnairobBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id017PtnairobBlockHeaderAlphaUnsignedContents = Id017PtnairobBlockHeaderContents.Id017PtnairobBlockHeaderAlphaUnsignedContents = (function() {
    function Id017PtnairobBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id017PtnairobBlockHeaderContents.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id017PtnairobLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id017PtnairobBlockHeaderAlphaUnsignedContents;
  })();

  var Id017PtnairobLiquidityBakingToggleVote = Id017PtnairobBlockHeaderContents.Id017PtnairobLiquidityBakingToggleVote = (function() {
    function Id017PtnairobLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobLiquidityBakingToggleVote.prototype._read = function() {
      this.id017PtnairobLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id017PtnairobLiquidityBakingToggleVote;
  })();

  return Id017PtnairobBlockHeaderContents;
})();
return Id017PtnairobBlockHeaderContents;
}));
