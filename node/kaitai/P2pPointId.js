// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.P2pPointId = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: p2p_point.id
 */

var P2pPointId = (function() {
  function P2pPointId(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  P2pPointId.prototype._read = function() {
    this.p2pPointId = new P2pPointId(this._io, this, this._root);
  }

  var BytesDynUint30 = P2pPointId.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var P2pPointId = P2pPointId.P2pPointId = (function() {
    function P2pPointId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    P2pPointId.prototype._read = function() {
      this.p2pPointId = new BytesDynUint30(this._io, this, this._root);
    }

    return P2pPointId;
  })();

  /**
   * Identifier for a peer point
   */

  return P2pPointId;
})();
return P2pPointId;
}));
