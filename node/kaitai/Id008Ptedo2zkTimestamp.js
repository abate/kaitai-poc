// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './TimestampProtocol'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./TimestampProtocol'));
  } else {
    root.Id008Ptedo2zkTimestamp = factory(root.KaitaiStream, root.TimestampProtocol);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, TimestampProtocol) {
/**
 * Encoding id: 008-PtEdo2Zk.timestamp
 */

var Id008Ptedo2zkTimestamp = (function() {
  function Id008Ptedo2zkTimestamp(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkTimestamp.prototype._read = function() {
    this.id008Ptedo2zkTimestamp = new TimestampProtocol(this._io, this, null);
  }

  return Id008Ptedo2zkTimestamp;
})();
return Id008Ptedo2zkTimestamp;
}));
