// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id009PsflorenDelegateFrozenBalance = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 009-PsFLoren.delegate.frozen_balance
 */

var Id009PsflorenDelegateFrozenBalance = (function() {
  function Id009PsflorenDelegateFrozenBalance(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id009PsflorenDelegateFrozenBalance.prototype._read = function() {
    this.deposit = new Id009PsflorenMutez(this._io, this, this._root);
    this.fees = new Id009PsflorenMutez(this._io, this, this._root);
    this.rewards = new Id009PsflorenMutez(this._io, this, this._root);
  }

  var Id009PsflorenMutez = Id009PsflorenDelegateFrozenBalance.Id009PsflorenMutez = (function() {
    function Id009PsflorenMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenMutez.prototype._read = function() {
      this.id009PsflorenMutez = new N(this._io, this, this._root);
    }

    return Id009PsflorenMutez;
  })();

  var N = Id009PsflorenDelegateFrozenBalance.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var NChunk = Id009PsflorenDelegateFrozenBalance.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  return Id009PsflorenDelegateFrozenBalance;
})();
return Id009PsflorenDelegateFrozenBalance;
}));
