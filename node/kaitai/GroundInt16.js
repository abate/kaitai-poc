// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.GroundInt16 = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: ground.int16
 * Description: Signed 16 bit integers
 */

var GroundInt16 = (function() {
  function GroundInt16(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  GroundInt16.prototype._read = function() {
    this.groundInt16 = this._io.readS2be();
  }

  return GroundInt16;
})();
return GroundInt16;
}));
