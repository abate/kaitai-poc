// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id016PtmumbaiVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 016-PtMumbai.vote.listings
 */

var Id016PtmumbaiVoteListings = (function() {
  Id016PtmumbaiVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function Id016PtmumbaiVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiVoteListings.prototype._read = function() {
    this.lenId016PtmumbaiVoteListings = this._io.readU4be();
    if (!(this.lenId016PtmumbaiVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId016PtmumbaiVoteListings, this._io, "/seq/0");
    }
    this._raw_id016PtmumbaiVoteListings = this._io.readBytes(this.lenId016PtmumbaiVoteListings);
    var _io__raw_id016PtmumbaiVoteListings = new KaitaiStream(this._raw_id016PtmumbaiVoteListings);
    this.id016PtmumbaiVoteListings = new Id016PtmumbaiVoteListings(_io__raw_id016PtmumbaiVoteListings, this, this._root);
  }

  var Id016PtmumbaiVoteListings = Id016PtmumbaiVoteListings.Id016PtmumbaiVoteListings = (function() {
    function Id016PtmumbaiVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiVoteListings.prototype._read = function() {
      this.id016PtmumbaiVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id016PtmumbaiVoteListingsEntries.push(new Id016PtmumbaiVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id016PtmumbaiVoteListings;
  })();

  var Id016PtmumbaiVoteListingsEntries = Id016PtmumbaiVoteListings.Id016PtmumbaiVoteListingsEntries = (function() {
    function Id016PtmumbaiVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.votingPower = this._io.readS8be();
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id016PtmumbaiVoteListingsEntries;
  })();

  var PublicKeyHash = Id016PtmumbaiVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id016PtmumbaiVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id016PtmumbaiVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id016PtmumbaiVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id016PtmumbaiVoteListings.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id016PtmumbaiVoteListings;
})();
return Id016PtmumbaiVoteListings;
}));
