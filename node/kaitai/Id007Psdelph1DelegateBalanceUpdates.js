// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id007Psdelph1DelegateBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 007-PsDELPH1.delegate.balance_updates
 */

var Id007Psdelph1DelegateBalanceUpdates = (function() {
  Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1ContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceTag = Object.freeze({
    CONTRACT: 0,
    REWARDS: 1,
    FEES: 2,
    DEPOSITS: 3,

    0: "CONTRACT",
    1: "REWARDS",
    2: "FEES",
    3: "DEPOSITS",
  });

  Id007Psdelph1DelegateBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id007Psdelph1DelegateBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1DelegateBalanceUpdates.prototype._read = function() {
    this.id007Psdelph1OperationMetadataAlphaBalanceUpdates = new Id007Psdelph1OperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Id007Psdelph1OperationMetadataAlphaBalance = Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalance = (function() {
    function Id007Psdelph1OperationMetadataAlphaBalance(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1OperationMetadataAlphaBalance.prototype._read = function() {
      this.id007Psdelph1OperationMetadataAlphaBalanceTag = this._io.readU1();
      if (this.id007Psdelph1OperationMetadataAlphaBalanceTag == Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceTag.CONTRACT) {
        this.contract = new Id007Psdelph1ContractId(this._io, this, this._root);
      }
      if (this.id007Psdelph1OperationMetadataAlphaBalanceTag == Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceTag.REWARDS) {
        this.rewards = new Rewards(this._io, this, this._root);
      }
      if (this.id007Psdelph1OperationMetadataAlphaBalanceTag == Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceTag.FEES) {
        this.fees = new Fees(this._io, this, this._root);
      }
      if (this.id007Psdelph1OperationMetadataAlphaBalanceTag == Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceTag.DEPOSITS) {
        this.deposits = new Deposits(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Id007Psdelph1OperationMetadataAlphaBalance;
  })();

  var Id007Psdelph1ContractId = Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1ContractId = (function() {
    function Id007Psdelph1ContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1ContractId.prototype._read = function() {
      this.id007Psdelph1ContractIdTag = this._io.readU1();
      if (this.id007Psdelph1ContractIdTag == Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1ContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id007Psdelph1ContractIdTag == Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1ContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id007Psdelph1ContractId;
  })();

  var Originated = Id007Psdelph1DelegateBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Id007Psdelph1OperationMetadataAlphaBalanceUpdates = Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceUpdates = (function() {
    function Id007Psdelph1OperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1OperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id007Psdelph1OperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id007Psdelph1OperationMetadataAlphaBalanceUpdatesEntries.push(new Id007Psdelph1OperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id007Psdelph1OperationMetadataAlphaBalanceUpdates;
  })();

  var Rewards = Id007Psdelph1DelegateBalanceUpdates.Rewards = (function() {
    function Rewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Rewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Rewards;
  })();

  var Deposits = Id007Psdelph1DelegateBalanceUpdates.Deposits = (function() {
    function Deposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposits.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Deposits;
  })();

  var Id007Psdelph1OperationMetadataAlphaBalanceUpdatesEntries = Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id007Psdelph1OperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1OperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id007Psdelph1OperationMetadataAlphaBalance = new Id007Psdelph1OperationMetadataAlphaBalance(this._io, this, this._root);
      this.id007Psdelph1OperationMetadataAlphaBalanceUpdate = new Id007Psdelph1OperationMetadataAlphaBalanceUpdate(this._io, this, this._root);
    }

    return Id007Psdelph1OperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var Id007Psdelph1OperationMetadataAlphaBalanceUpdate = Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceUpdate = (function() {
    function Id007Psdelph1OperationMetadataAlphaBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1OperationMetadataAlphaBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id007Psdelph1OperationMetadataAlphaBalanceUpdate;
  })();

  var Fees = Id007Psdelph1DelegateBalanceUpdates.Fees = (function() {
    function Fees(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Fees.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Fees;
  })();

  var PublicKeyHash = Id007Psdelph1DelegateBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id007Psdelph1DelegateBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id007Psdelph1DelegateBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id007Psdelph1DelegateBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Id007Psdelph1OperationMetadataAlphaBalanceUpdates0 = Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id007Psdelph1OperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1OperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId007Psdelph1OperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId007Psdelph1OperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId007Psdelph1OperationMetadataAlphaBalanceUpdates, this._io, "/types/id_007__psdelph1__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id007Psdelph1OperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId007Psdelph1OperationMetadataAlphaBalanceUpdates);
      var _io__raw_id007Psdelph1OperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id007Psdelph1OperationMetadataAlphaBalanceUpdates);
      this.id007Psdelph1OperationMetadataAlphaBalanceUpdates = new Id007Psdelph1OperationMetadataAlphaBalanceUpdates(_io__raw_id007Psdelph1OperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id007Psdelph1OperationMetadataAlphaBalanceUpdates0;
  })();

  return Id007Psdelph1DelegateBalanceUpdates;
})();
return Id007Psdelph1DelegateBalanceUpdates;
}));
