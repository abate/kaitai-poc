// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id009PsflorenBlockHeaderContents = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 009-PsFLoren.block_header.contents
 */

var Id009PsflorenBlockHeaderContents = (function() {
  Id009PsflorenBlockHeaderContents.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id009PsflorenBlockHeaderContents(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id009PsflorenBlockHeaderContents.prototype._read = function() {
    this.id009PsflorenBlockHeaderAlphaUnsignedContents = new Id009PsflorenBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id009PsflorenBlockHeaderAlphaUnsignedContents = Id009PsflorenBlockHeaderContents.Id009PsflorenBlockHeaderAlphaUnsignedContents = (function() {
    function Id009PsflorenBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id009PsflorenBlockHeaderContents.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
    }

    return Id009PsflorenBlockHeaderAlphaUnsignedContents;
  })();

  return Id009PsflorenBlockHeaderContents;
})();
return Id009PsflorenBlockHeaderContents;
}));
