// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id018ProxfordVoteBallots = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 018-Proxford.vote.ballots
 */

var Id018ProxfordVoteBallots = (function() {
  function Id018ProxfordVoteBallots(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id018ProxfordVoteBallots.prototype._read = function() {
    this.yay = this._io.readS8be();
    this.nay = this._io.readS8be();
    this.pass = this._io.readS8be();
  }

  return Id018ProxfordVoteBallots;
})();
return Id018ProxfordVoteBallots;
}));
