// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id014PtkathmaBlockHeaderShellHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 014-PtKathma.block_header.shell_header
 */

var Id014PtkathmaBlockHeaderShellHeader = (function() {
  function Id014PtkathmaBlockHeaderShellHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id014PtkathmaBlockHeaderShellHeader.prototype._read = function() {
    this.id014PtkathmaBlockHeaderShellHeader = new BlockHeaderShell(this._io, this, null);
  }

  return Id014PtkathmaBlockHeaderShellHeader;
})();
return Id014PtkathmaBlockHeaderShellHeader;
}));
