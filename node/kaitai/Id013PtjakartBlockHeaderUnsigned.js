// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id013PtjakartBlockHeaderUnsigned = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 013-PtJakart.block_header.unsigned
 */

var Id013PtjakartBlockHeaderUnsigned = (function() {
  Id013PtjakartBlockHeaderUnsigned.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id013PtjakartBlockHeaderUnsigned(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id013PtjakartBlockHeaderUnsigned.prototype._read = function() {
    this.id013PtjakartBlockHeaderUnsigned = new BlockHeaderShell(this._io, this, null);
    this.id013PtjakartBlockHeaderAlphaUnsignedContents = new Id013PtjakartBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id013PtjakartBlockHeaderAlphaUnsignedContents = Id013PtjakartBlockHeaderUnsigned.Id013PtjakartBlockHeaderAlphaUnsignedContents = (function() {
    function Id013PtjakartBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id013PtjakartBlockHeaderUnsigned.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id013PtjakartLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id013PtjakartBlockHeaderAlphaUnsignedContents;
  })();

  var Id013PtjakartLiquidityBakingToggleVote = Id013PtjakartBlockHeaderUnsigned.Id013PtjakartLiquidityBakingToggleVote = (function() {
    function Id013PtjakartLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartLiquidityBakingToggleVote.prototype._read = function() {
      this.id013PtjakartLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id013PtjakartLiquidityBakingToggleVote;
  })();

  return Id013PtjakartBlockHeaderUnsigned;
})();
return Id013PtjakartBlockHeaderUnsigned;
}));
