// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaOperationInternal = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.operation.internal
 */

var AlphaOperationInternal = (function() {
  AlphaOperationInternal.MichelineAlphaMichelsonV1ExpressionTag = Object.freeze({
    INT: 0,
    STRING: 1,
    SEQUENCE: 2,
    PRIM__NO_ARGS__NO_ANNOTS: 3,
    PRIM__NO_ARGS__SOME_ANNOTS: 4,
    PRIM__1_ARG__NO_ANNOTS: 5,
    PRIM__1_ARG__SOME_ANNOTS: 6,
    PRIM__2_ARGS__NO_ANNOTS: 7,
    PRIM__2_ARGS__SOME_ANNOTS: 8,
    PRIM__GENERIC: 9,
    BYTES: 10,

    0: "INT",
    1: "STRING",
    2: "SEQUENCE",
    3: "PRIM__NO_ARGS__NO_ANNOTS",
    4: "PRIM__NO_ARGS__SOME_ANNOTS",
    5: "PRIM__1_ARG__NO_ANNOTS",
    6: "PRIM__1_ARG__SOME_ANNOTS",
    7: "PRIM__2_ARGS__NO_ANNOTS",
    8: "PRIM__2_ARGS__SOME_ANNOTS",
    9: "PRIM__GENERIC",
    10: "BYTES",
  });

  AlphaOperationInternal.AlphaMichelsonV1Primitives = Object.freeze({
    PARAMETER: 0,
    STORAGE: 1,
    CODE: 2,
    FALSE: 3,
    ELT: 4,
    LEFT: 5,
    NONE: 6,
    PAIR_1: 7,
    RIGHT: 8,
    SOME_0: 9,
    TRUE: 10,
    UNIT_1: 11,
    PACK: 12,
    UNPACK: 13,
    BLAKE2B: 14,
    SHA256: 15,
    SHA512: 16,
    ABS: 17,
    ADD: 18,
    AMOUNT: 19,
    AND: 20,
    BALANCE: 21,
    CAR: 22,
    CDR: 23,
    CHECK_SIGNATURE: 24,
    COMPARE: 25,
    CONCAT: 26,
    CONS: 27,
    CREATE_ACCOUNT: 28,
    CREATE_CONTRACT: 29,
    IMPLICIT_ACCOUNT: 30,
    DIP: 31,
    DROP: 32,
    DUP: 33,
    EDIV: 34,
    EMPTY_MAP: 35,
    EMPTY_SET: 36,
    EQ: 37,
    EXEC: 38,
    FAILWITH: 39,
    GE: 40,
    GET: 41,
    GT: 42,
    HASH_KEY: 43,
    IF: 44,
    IF_CONS: 45,
    IF_LEFT: 46,
    IF_NONE: 47,
    INT_0: 48,
    LAMBDA_0: 49,
    LE: 50,
    LEFT_0: 51,
    LOOP: 52,
    LSL: 53,
    LSR: 54,
    LT: 55,
    MAP_0: 56,
    MEM: 57,
    MUL: 58,
    NEG: 59,
    NEQ: 60,
    NIL: 61,
    NONE_0: 62,
    NOT: 63,
    NOW: 64,
    OR_0: 65,
    PAIR_0: 66,
    PUSH: 67,
    RIGHT_0: 68,
    SIZE: 69,
    SOME: 70,
    SOURCE: 71,
    SENDER: 72,
    SELF: 73,
    STEPS_TO_QUOTA: 74,
    SUB: 75,
    SWAP: 76,
    TRANSFER_TOKENS: 77,
    SET_DELEGATE: 78,
    UNIT_0: 79,
    UPDATE: 80,
    XOR: 81,
    ITER: 82,
    LOOP_LEFT: 83,
    ADDRESS_0: 84,
    CONTRACT_0: 85,
    ISNAT: 86,
    CAST: 87,
    RENAME: 88,
    BOOL: 89,
    CONTRACT: 90,
    INT: 91,
    KEY: 92,
    KEY_HASH: 93,
    LAMBDA: 94,
    LIST: 95,
    MAP: 96,
    BIG_MAP: 97,
    NAT: 98,
    OPTION: 99,
    OR: 100,
    PAIR: 101,
    SET: 102,
    SIGNATURE: 103,
    STRING: 104,
    BYTES: 105,
    MUTEZ: 106,
    TIMESTAMP: 107,
    UNIT: 108,
    OPERATION: 109,
    ADDRESS: 110,
    SLICE: 111,
    DIG: 112,
    DUG: 113,
    EMPTY_BIG_MAP: 114,
    APPLY: 115,
    CHAIN_ID: 116,
    CHAIN_ID_0: 117,
    LEVEL: 118,
    SELF_ADDRESS: 119,
    NEVER: 120,
    NEVER_0: 121,
    UNPAIR: 122,
    VOTING_POWER: 123,
    TOTAL_VOTING_POWER: 124,
    KECCAK: 125,
    SHA3: 126,
    PAIRING_CHECK: 127,
    BLS12_381_G1: 128,
    BLS12_381_G2: 129,
    BLS12_381_FR: 130,
    SAPLING_STATE: 131,
    SAPLING_TRANSACTION_DEPRECATED: 132,
    SAPLING_EMPTY_STATE: 133,
    SAPLING_VERIFY_UPDATE: 134,
    TICKET: 135,
    TICKET_DEPRECATED: 136,
    READ_TICKET: 137,
    SPLIT_TICKET: 138,
    JOIN_TICKETS: 139,
    GET_AND_UPDATE: 140,
    CHEST: 141,
    CHEST_KEY: 142,
    OPEN_CHEST: 143,
    VIEW_0: 144,
    VIEW: 145,
    CONSTANT: 146,
    SUB_MUTEZ: 147,
    TX_ROLLUP_L2_ADDRESS: 148,
    MIN_BLOCK_TIME: 149,
    SAPLING_TRANSACTION: 150,
    EMIT: 151,
    LAMBDA_REC_0: 152,
    LAMBDA_REC: 153,
    TICKET_0: 154,
    BYTES_0: 155,
    NAT_0: 156,
    TICKET_1: 157,

    0: "PARAMETER",
    1: "STORAGE",
    2: "CODE",
    3: "FALSE",
    4: "ELT",
    5: "LEFT",
    6: "NONE",
    7: "PAIR_1",
    8: "RIGHT",
    9: "SOME_0",
    10: "TRUE",
    11: "UNIT_1",
    12: "PACK",
    13: "UNPACK",
    14: "BLAKE2B",
    15: "SHA256",
    16: "SHA512",
    17: "ABS",
    18: "ADD",
    19: "AMOUNT",
    20: "AND",
    21: "BALANCE",
    22: "CAR",
    23: "CDR",
    24: "CHECK_SIGNATURE",
    25: "COMPARE",
    26: "CONCAT",
    27: "CONS",
    28: "CREATE_ACCOUNT",
    29: "CREATE_CONTRACT",
    30: "IMPLICIT_ACCOUNT",
    31: "DIP",
    32: "DROP",
    33: "DUP",
    34: "EDIV",
    35: "EMPTY_MAP",
    36: "EMPTY_SET",
    37: "EQ",
    38: "EXEC",
    39: "FAILWITH",
    40: "GE",
    41: "GET",
    42: "GT",
    43: "HASH_KEY",
    44: "IF",
    45: "IF_CONS",
    46: "IF_LEFT",
    47: "IF_NONE",
    48: "INT_0",
    49: "LAMBDA_0",
    50: "LE",
    51: "LEFT_0",
    52: "LOOP",
    53: "LSL",
    54: "LSR",
    55: "LT",
    56: "MAP_0",
    57: "MEM",
    58: "MUL",
    59: "NEG",
    60: "NEQ",
    61: "NIL",
    62: "NONE_0",
    63: "NOT",
    64: "NOW",
    65: "OR_0",
    66: "PAIR_0",
    67: "PUSH",
    68: "RIGHT_0",
    69: "SIZE",
    70: "SOME",
    71: "SOURCE",
    72: "SENDER",
    73: "SELF",
    74: "STEPS_TO_QUOTA",
    75: "SUB",
    76: "SWAP",
    77: "TRANSFER_TOKENS",
    78: "SET_DELEGATE",
    79: "UNIT_0",
    80: "UPDATE",
    81: "XOR",
    82: "ITER",
    83: "LOOP_LEFT",
    84: "ADDRESS_0",
    85: "CONTRACT_0",
    86: "ISNAT",
    87: "CAST",
    88: "RENAME",
    89: "BOOL",
    90: "CONTRACT",
    91: "INT",
    92: "KEY",
    93: "KEY_HASH",
    94: "LAMBDA",
    95: "LIST",
    96: "MAP",
    97: "BIG_MAP",
    98: "NAT",
    99: "OPTION",
    100: "OR",
    101: "PAIR",
    102: "SET",
    103: "SIGNATURE",
    104: "STRING",
    105: "BYTES",
    106: "MUTEZ",
    107: "TIMESTAMP",
    108: "UNIT",
    109: "OPERATION",
    110: "ADDRESS",
    111: "SLICE",
    112: "DIG",
    113: "DUG",
    114: "EMPTY_BIG_MAP",
    115: "APPLY",
    116: "CHAIN_ID",
    117: "CHAIN_ID_0",
    118: "LEVEL",
    119: "SELF_ADDRESS",
    120: "NEVER",
    121: "NEVER_0",
    122: "UNPAIR",
    123: "VOTING_POWER",
    124: "TOTAL_VOTING_POWER",
    125: "KECCAK",
    126: "SHA3",
    127: "PAIRING_CHECK",
    128: "BLS12_381_G1",
    129: "BLS12_381_G2",
    130: "BLS12_381_FR",
    131: "SAPLING_STATE",
    132: "SAPLING_TRANSACTION_DEPRECATED",
    133: "SAPLING_EMPTY_STATE",
    134: "SAPLING_VERIFY_UPDATE",
    135: "TICKET",
    136: "TICKET_DEPRECATED",
    137: "READ_TICKET",
    138: "SPLIT_TICKET",
    139: "JOIN_TICKETS",
    140: "GET_AND_UPDATE",
    141: "CHEST",
    142: "CHEST_KEY",
    143: "OPEN_CHEST",
    144: "VIEW_0",
    145: "VIEW",
    146: "CONSTANT",
    147: "SUB_MUTEZ",
    148: "TX_ROLLUP_L2_ADDRESS",
    149: "MIN_BLOCK_TIME",
    150: "SAPLING_TRANSACTION",
    151: "EMIT",
    152: "LAMBDA_REC_0",
    153: "LAMBDA_REC",
    154: "TICKET_0",
    155: "BYTES_0",
    156: "NAT_0",
    157: "TICKET_1",
  });

  AlphaOperationInternal.AlphaEntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    DEPOSIT: 5,
    STAKE: 6,
    UNSTAKE: 7,
    FINALIZE_UNSTAKE: 8,
    SET_DELEGATE_PARAMETERS: 9,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    5: "DEPOSIT",
    6: "STAKE",
    7: "UNSTAKE",
    8: "FINALIZE_UNSTAKE",
    9: "SET_DELEGATE_PARAMETERS",
    255: "NAMED",
  });

  AlphaOperationInternal.AlphaTransactionDestinationTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,
    SMART_ROLLUP: 3,
    ZK_ROLLUP: 4,

    0: "IMPLICIT",
    1: "ORIGINATED",
    3: "SMART_ROLLUP",
    4: "ZK_ROLLUP",
  });

  AlphaOperationInternal.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  AlphaOperationInternal.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  AlphaOperationInternal.AlphaApplyInternalResultsAlphaOperationResultTag = Object.freeze({
    TRANSACTION: 1,
    ORIGINATION: 2,
    DELEGATION: 3,
    EVENT: 4,

    1: "TRANSACTION",
    2: "ORIGINATION",
    3: "DELEGATION",
    4: "EVENT",
  });

  function AlphaOperationInternal(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaOperationInternal.prototype._read = function() {
    this.alphaApplyInternalResultsAlphaOperationResult = new AlphaApplyInternalResultsAlphaOperationResult(this._io, this, this._root);
  }

  var Originated = AlphaOperationInternal.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var N = AlphaOperationInternal.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var AlphaMichelsonV1Primitives = AlphaOperationInternal.AlphaMichelsonV1Primitives = (function() {
    function AlphaMichelsonV1Primitives(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaMichelsonV1Primitives.prototype._read = function() {
      this.alphaMichelsonV1Primitives = this._io.readU1();
    }

    return AlphaMichelsonV1Primitives;
  })();

  var SmartRollup = AlphaOperationInternal.SmartRollup = (function() {
    function SmartRollup(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SmartRollup.prototype._read = function() {
      this.smartRollupAddress = this._io.readBytes(20);
      this.smartRollupPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return SmartRollup;
  })();

  var Event = AlphaOperationInternal.Event = (function() {
    function Event(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Event.prototype._read = function() {
      this.type = new MichelineAlphaMichelsonV1Expression(this._io, this, this._root);
      this.tagTag = this._io.readU1();
      if (this.tagTag == AlphaOperationInternal.Bool.TRUE) {
        this.tag = new AlphaEntrypoint(this._io, this, this._root);
      }
      this.payloadTag = this._io.readU1();
      if (this.payloadTag == AlphaOperationInternal.Bool.TRUE) {
        this.payload = new MichelineAlphaMichelsonV1Expression(this._io, this, this._root);
      }
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Event;
  })();

  var Prim1ArgSomeAnnots = AlphaOperationInternal.Prim1ArgSomeAnnots = (function() {
    function Prim1ArgSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim1ArgSomeAnnots.prototype._read = function() {
      this.prim = new AlphaMichelsonV1Primitives(this._io, this, this._root);
      this.arg = new MichelineAlphaMichelsonV1Expression(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return Prim1ArgSomeAnnots;
  })();

  var PrimNoArgsSomeAnnots = AlphaOperationInternal.PrimNoArgsSomeAnnots = (function() {
    function PrimNoArgsSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrimNoArgsSomeAnnots.prototype._read = function() {
      this.prim = new AlphaMichelsonV1Primitives(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return PrimNoArgsSomeAnnots;
  })();

  var AlphaApplyInternalResultsAlphaOperationResult = AlphaOperationInternal.AlphaApplyInternalResultsAlphaOperationResult = (function() {
    function AlphaApplyInternalResultsAlphaOperationResult(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaApplyInternalResultsAlphaOperationResult.prototype._read = function() {
      this.source = new AlphaTransactionDestination(this._io, this, this._root);
      this.nonce = this._io.readU2be();
      this.alphaApplyInternalResultsAlphaOperationResultTag = this._io.readU1();
      if (this.alphaApplyInternalResultsAlphaOperationResultTag == AlphaOperationInternal.AlphaApplyInternalResultsAlphaOperationResultTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.alphaApplyInternalResultsAlphaOperationResultTag == AlphaOperationInternal.AlphaApplyInternalResultsAlphaOperationResultTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.alphaApplyInternalResultsAlphaOperationResultTag == AlphaOperationInternal.AlphaApplyInternalResultsAlphaOperationResultTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
      if (this.alphaApplyInternalResultsAlphaOperationResultTag == AlphaOperationInternal.AlphaApplyInternalResultsAlphaOperationResultTag.EVENT) {
        this.event = new Event(this._io, this, this._root);
      }
    }

    /**
     * A destination of a transaction: A destination notation compatible with the contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash, a base58 originated contract hash, a base58 originated transaction rollup, or a base58 originated smart rollup.
     */

    return AlphaApplyInternalResultsAlphaOperationResult;
  })();

  var Named = AlphaOperationInternal.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var PrimGeneric = AlphaOperationInternal.PrimGeneric = (function() {
    function PrimGeneric(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrimGeneric.prototype._read = function() {
      this.prim = new AlphaMichelsonV1Primitives(this._io, this, this._root);
      this.args = new Args0(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return PrimGeneric;
  })();

  var Delegation = AlphaOperationInternal.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == AlphaOperationInternal.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Delegation;
  })();

  var Sequence0 = AlphaOperationInternal.Sequence0 = (function() {
    function Sequence0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sequence0.prototype._read = function() {
      this.lenSequence = this._io.readU4be();
      if (!(this.lenSequence <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenSequence, this._io, "/types/sequence_0/seq/0");
      }
      this._raw_sequence = this._io.readBytes(this.lenSequence);
      var _io__raw_sequence = new KaitaiStream(this._raw_sequence);
      this.sequence = new Sequence(_io__raw_sequence, this, this._root);
    }

    return Sequence0;
  })();

  var MichelineAlphaMichelsonV1Expression = AlphaOperationInternal.MichelineAlphaMichelsonV1Expression = (function() {
    function MichelineAlphaMichelsonV1Expression(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MichelineAlphaMichelsonV1Expression.prototype._read = function() {
      this.michelineAlphaMichelsonV1ExpressionTag = this._io.readU1();
      if (this.michelineAlphaMichelsonV1ExpressionTag == AlphaOperationInternal.MichelineAlphaMichelsonV1ExpressionTag.INT) {
        this.int = new Z(this._io, this, this._root);
      }
      if (this.michelineAlphaMichelsonV1ExpressionTag == AlphaOperationInternal.MichelineAlphaMichelsonV1ExpressionTag.STRING) {
        this.string = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.michelineAlphaMichelsonV1ExpressionTag == AlphaOperationInternal.MichelineAlphaMichelsonV1ExpressionTag.SEQUENCE) {
        this.sequence = new Sequence0(this._io, this, this._root);
      }
      if (this.michelineAlphaMichelsonV1ExpressionTag == AlphaOperationInternal.MichelineAlphaMichelsonV1ExpressionTag.PRIM__NO_ARGS__NO_ANNOTS) {
        this.primNoArgsNoAnnots = new AlphaMichelsonV1Primitives(this._io, this, this._root);
      }
      if (this.michelineAlphaMichelsonV1ExpressionTag == AlphaOperationInternal.MichelineAlphaMichelsonV1ExpressionTag.PRIM__NO_ARGS__SOME_ANNOTS) {
        this.primNoArgsSomeAnnots = new PrimNoArgsSomeAnnots(this._io, this, this._root);
      }
      if (this.michelineAlphaMichelsonV1ExpressionTag == AlphaOperationInternal.MichelineAlphaMichelsonV1ExpressionTag.PRIM__1_ARG__NO_ANNOTS) {
        this.prim1ArgNoAnnots = new Prim1ArgNoAnnots(this._io, this, this._root);
      }
      if (this.michelineAlphaMichelsonV1ExpressionTag == AlphaOperationInternal.MichelineAlphaMichelsonV1ExpressionTag.PRIM__1_ARG__SOME_ANNOTS) {
        this.prim1ArgSomeAnnots = new Prim1ArgSomeAnnots(this._io, this, this._root);
      }
      if (this.michelineAlphaMichelsonV1ExpressionTag == AlphaOperationInternal.MichelineAlphaMichelsonV1ExpressionTag.PRIM__2_ARGS__NO_ANNOTS) {
        this.prim2ArgsNoAnnots = new Prim2ArgsNoAnnots(this._io, this, this._root);
      }
      if (this.michelineAlphaMichelsonV1ExpressionTag == AlphaOperationInternal.MichelineAlphaMichelsonV1ExpressionTag.PRIM__2_ARGS__SOME_ANNOTS) {
        this.prim2ArgsSomeAnnots = new Prim2ArgsSomeAnnots(this._io, this, this._root);
      }
      if (this.michelineAlphaMichelsonV1ExpressionTag == AlphaOperationInternal.MichelineAlphaMichelsonV1ExpressionTag.PRIM__GENERIC) {
        this.primGeneric = new PrimGeneric(this._io, this, this._root);
      }
      if (this.michelineAlphaMichelsonV1ExpressionTag == AlphaOperationInternal.MichelineAlphaMichelsonV1ExpressionTag.BYTES) {
        this.bytes = new BytesDynUint30(this._io, this, this._root);
      }
    }

    return MichelineAlphaMichelsonV1Expression;
  })();

  var Sequence = AlphaOperationInternal.Sequence = (function() {
    function Sequence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sequence.prototype._read = function() {
      this.sequenceEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.sequenceEntries.push(new SequenceEntries(this._io, this, this._root));
        i++;
      }
    }

    return Sequence;
  })();

  var ZkRollup = AlphaOperationInternal.ZkRollup = (function() {
    function ZkRollup(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ZkRollup.prototype._read = function() {
      this.zkRollupHash = this._io.readBytes(20);
      this.zkRollupPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return ZkRollup;
  })();

  var BytesDynUint30 = AlphaOperationInternal.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var AlphaTransactionDestination = AlphaOperationInternal.AlphaTransactionDestination = (function() {
    function AlphaTransactionDestination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaTransactionDestination.prototype._read = function() {
      this.alphaTransactionDestinationTag = this._io.readU1();
      if (this.alphaTransactionDestinationTag == AlphaOperationInternal.AlphaTransactionDestinationTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.alphaTransactionDestinationTag == AlphaOperationInternal.AlphaTransactionDestinationTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
      if (this.alphaTransactionDestinationTag == AlphaOperationInternal.AlphaTransactionDestinationTag.SMART_ROLLUP) {
        this.smartRollup = new SmartRollup(this._io, this, this._root);
      }
      if (this.alphaTransactionDestinationTag == AlphaOperationInternal.AlphaTransactionDestinationTag.ZK_ROLLUP) {
        this.zkRollup = new ZkRollup(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return AlphaTransactionDestination;
  })();

  var AlphaMutez = AlphaOperationInternal.AlphaMutez = (function() {
    function AlphaMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaMutez.prototype._read = function() {
      this.alphaMutez = new N(this._io, this, this._root);
    }

    return AlphaMutez;
  })();

  var Origination = AlphaOperationInternal.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.balance = new AlphaMutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == AlphaOperationInternal.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new AlphaScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Origination;
  })();

  var Args = AlphaOperationInternal.Args = (function() {
    function Args(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Args.prototype._read = function() {
      this.argsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.argsEntries.push(new ArgsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Args;
  })();

  var AlphaEntrypoint = AlphaOperationInternal.AlphaEntrypoint = (function() {
    function AlphaEntrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaEntrypoint.prototype._read = function() {
      this.alphaEntrypointTag = this._io.readU1();
      if (this.alphaEntrypointTag == AlphaOperationInternal.AlphaEntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return AlphaEntrypoint;
  })();

  var Prim1ArgNoAnnots = AlphaOperationInternal.Prim1ArgNoAnnots = (function() {
    function Prim1ArgNoAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim1ArgNoAnnots.prototype._read = function() {
      this.prim = new AlphaMichelsonV1Primitives(this._io, this, this._root);
      this.arg = new MichelineAlphaMichelsonV1Expression(this._io, this, this._root);
    }

    return Prim1ArgNoAnnots;
  })();

  var SequenceEntries = AlphaOperationInternal.SequenceEntries = (function() {
    function SequenceEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SequenceEntries.prototype._read = function() {
      this.sequenceElt = new MichelineAlphaMichelsonV1Expression(this._io, this, this._root);
    }

    return SequenceEntries;
  })();

  var NChunk = AlphaOperationInternal.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Prim2ArgsNoAnnots = AlphaOperationInternal.Prim2ArgsNoAnnots = (function() {
    function Prim2ArgsNoAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim2ArgsNoAnnots.prototype._read = function() {
      this.prim = new AlphaMichelsonV1Primitives(this._io, this, this._root);
      this.arg1 = new MichelineAlphaMichelsonV1Expression(this._io, this, this._root);
      this.arg2 = new MichelineAlphaMichelsonV1Expression(this._io, this, this._root);
    }

    return Prim2ArgsNoAnnots;
  })();

  var AlphaScriptedContracts = AlphaOperationInternal.AlphaScriptedContracts = (function() {
    function AlphaScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return AlphaScriptedContracts;
  })();

  var ArgsEntries = AlphaOperationInternal.ArgsEntries = (function() {
    function ArgsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ArgsEntries.prototype._read = function() {
      this.argsElt = new MichelineAlphaMichelsonV1Expression(this._io, this, this._root);
    }

    return ArgsEntries;
  })();

  var Args0 = AlphaOperationInternal.Args0 = (function() {
    function Args0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Args0.prototype._read = function() {
      this.lenArgs = this._io.readU4be();
      if (!(this.lenArgs <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenArgs, this._io, "/types/args_0/seq/0");
      }
      this._raw_args = this._io.readBytes(this.lenArgs);
      var _io__raw_args = new KaitaiStream(this._raw_args);
      this.args = new Args(_io__raw_args, this, this._root);
    }

    return Args0;
  })();

  var Named0 = AlphaOperationInternal.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = AlphaOperationInternal.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.amount = new AlphaMutez(this._io, this, this._root);
      this.destination = new AlphaTransactionDestination(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == AlphaOperationInternal.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A destination of a transaction: A destination notation compatible with the contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash, a base58 originated contract hash, a base58 originated transaction rollup, or a base58 originated smart rollup.
     */

    return Transaction;
  })();

  var Parameters = AlphaOperationInternal.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new AlphaEntrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var Prim2ArgsSomeAnnots = AlphaOperationInternal.Prim2ArgsSomeAnnots = (function() {
    function Prim2ArgsSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim2ArgsSomeAnnots.prototype._read = function() {
      this.prim = new AlphaMichelsonV1Primitives(this._io, this, this._root);
      this.arg1 = new MichelineAlphaMichelsonV1Expression(this._io, this, this._root);
      this.arg2 = new MichelineAlphaMichelsonV1Expression(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return Prim2ArgsSomeAnnots;
  })();

  var PublicKeyHash = AlphaOperationInternal.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == AlphaOperationInternal.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaOperationInternal.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaOperationInternal.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaOperationInternal.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Z = AlphaOperationInternal.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  return AlphaOperationInternal;
})();
return AlphaOperationInternal;
}));
