// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id005Psbabym1BlockHeaderShellHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 005-PsBabyM1.block_header.shell_header
 */

var Id005Psbabym1BlockHeaderShellHeader = (function() {
  function Id005Psbabym1BlockHeaderShellHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1BlockHeaderShellHeader.prototype._read = function() {
    this.id005Psbabym1BlockHeaderShellHeader = new BlockHeaderShell(this._io, this, null);
  }

  return Id005Psbabym1BlockHeaderShellHeader;
})();
return Id005Psbabym1BlockHeaderShellHeader;
}));
