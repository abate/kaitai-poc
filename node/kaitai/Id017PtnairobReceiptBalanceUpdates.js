// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id017PtnairobReceiptBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 017-PtNairob.receipt.balance_updates
 */

var Id017PtnairobReceiptBalanceUpdates = (function() {
  Id017PtnairobReceiptBalanceUpdates.Id017PtnairobContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id017PtnairobReceiptBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  Id017PtnairobReceiptBalanceUpdates.Id017PtnairobBondIdTag = Object.freeze({
    TX_ROLLUP_BOND_ID: 0,
    SMART_ROLLUP_BOND_ID: 1,

    0: "TX_ROLLUP_BOND_ID",
    1: "SMART_ROLLUP_BOND_ID",
  });

  Id017PtnairobReceiptBalanceUpdates.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id017PtnairobReceiptBalanceUpdates.OriginTag = Object.freeze({
    BLOCK_APPLICATION: 0,
    PROTOCOL_MIGRATION: 1,
    SUBSIDY: 2,
    SIMULATION: 3,

    0: "BLOCK_APPLICATION",
    1: "PROTOCOL_MIGRATION",
    2: "SUBSIDY",
    3: "SIMULATION",
  });

  Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceTag = Object.freeze({
    CONTRACT: 0,
    BLOCK_FEES: 2,
    DEPOSITS: 4,
    NONCE_REVELATION_REWARDS: 5,
    DOUBLE_SIGNING_EVIDENCE_REWARDS: 6,
    ENDORSING_REWARDS: 7,
    BAKING_REWARDS: 8,
    BAKING_BONUSES: 9,
    STORAGE_FEES: 11,
    DOUBLE_SIGNING_PUNISHMENTS: 12,
    LOST_ENDORSING_REWARDS: 13,
    LIQUIDITY_BAKING_SUBSIDIES: 14,
    BURNED: 15,
    COMMITMENTS: 16,
    BOOTSTRAP: 17,
    INVOICE: 18,
    INITIAL_COMMITMENTS: 19,
    MINTED: 20,
    FROZEN_BONDS: 21,
    TX_ROLLUP_REJECTION_REWARDS: 22,
    TX_ROLLUP_REJECTION_PUNISHMENTS: 23,
    SMART_ROLLUP_REFUTATION_PUNISHMENTS: 24,
    SMART_ROLLUP_REFUTATION_REWARDS: 25,

    0: "CONTRACT",
    2: "BLOCK_FEES",
    4: "DEPOSITS",
    5: "NONCE_REVELATION_REWARDS",
    6: "DOUBLE_SIGNING_EVIDENCE_REWARDS",
    7: "ENDORSING_REWARDS",
    8: "BAKING_REWARDS",
    9: "BAKING_BONUSES",
    11: "STORAGE_FEES",
    12: "DOUBLE_SIGNING_PUNISHMENTS",
    13: "LOST_ENDORSING_REWARDS",
    14: "LIQUIDITY_BAKING_SUBSIDIES",
    15: "BURNED",
    16: "COMMITMENTS",
    17: "BOOTSTRAP",
    18: "INVOICE",
    19: "INITIAL_COMMITMENTS",
    20: "MINTED",
    21: "FROZEN_BONDS",
    22: "TX_ROLLUP_REJECTION_REWARDS",
    23: "TX_ROLLUP_REJECTION_PUNISHMENTS",
    24: "SMART_ROLLUP_REFUTATION_PUNISHMENTS",
    25: "SMART_ROLLUP_REFUTATION_REWARDS",
  });

  function Id017PtnairobReceiptBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id017PtnairobReceiptBalanceUpdates.prototype._read = function() {
    this.id017PtnairobOperationMetadataAlphaBalanceUpdates = new Id017PtnairobOperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var FrozenBonds = Id017PtnairobReceiptBalanceUpdates.FrozenBonds = (function() {
    function FrozenBonds(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    FrozenBonds.prototype._read = function() {
      this.contract = new Id017PtnairobContractId(this._io, this, this._root);
      this.bondId = new Id017PtnairobBondId(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return FrozenBonds;
  })();

  var Originated = Id017PtnairobReceiptBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var LostEndorsingRewards = Id017PtnairobReceiptBalanceUpdates.LostEndorsingRewards = (function() {
    function LostEndorsingRewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LostEndorsingRewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.participation = this._io.readU1();
      this.revelation = this._io.readU1();
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return LostEndorsingRewards;
  })();

  var Id017PtnairobOperationMetadataAlphaBalance = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalance = (function() {
    function Id017PtnairobOperationMetadataAlphaBalance(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobOperationMetadataAlphaBalance.prototype._read = function() {
      this.id017PtnairobOperationMetadataAlphaBalanceTag = this._io.readU1();
      if (this.id017PtnairobOperationMetadataAlphaBalanceTag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceTag.CONTRACT) {
        this.contract = new Id017PtnairobContractId(this._io, this, this._root);
      }
      if (this.id017PtnairobOperationMetadataAlphaBalanceTag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceTag.DEPOSITS) {
        this.deposits = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id017PtnairobOperationMetadataAlphaBalanceTag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceTag.LOST_ENDORSING_REWARDS) {
        this.lostEndorsingRewards = new LostEndorsingRewards(this._io, this, this._root);
      }
      if (this.id017PtnairobOperationMetadataAlphaBalanceTag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceTag.COMMITMENTS) {
        this.commitments = this._io.readBytes(20);
      }
      if (this.id017PtnairobOperationMetadataAlphaBalanceTag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceTag.FROZEN_BONDS) {
        this.frozenBonds = new FrozenBonds(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id017PtnairobOperationMetadataAlphaBalance;
  })();

  var Id017PtnairobOperationMetadataAlphaBalanceUpdate = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceUpdate = (function() {
    function Id017PtnairobOperationMetadataAlphaBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobOperationMetadataAlphaBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id017PtnairobOperationMetadataAlphaBalanceUpdate;
  })();

  var Id017PtnairobTxRollupId = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobTxRollupId = (function() {
    function Id017PtnairobTxRollupId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobTxRollupId.prototype._read = function() {
      this.rollupHash = this._io.readBytes(20);
    }

    return Id017PtnairobTxRollupId;
  })();

  var Id017PtnairobSmartRollupAddress = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobSmartRollupAddress = (function() {
    function Id017PtnairobSmartRollupAddress(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobSmartRollupAddress.prototype._read = function() {
      this.smartRollupHash = this._io.readBytes(20);
    }

    return Id017PtnairobSmartRollupAddress;
  })();

  var Id017PtnairobBondId = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobBondId = (function() {
    function Id017PtnairobBondId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobBondId.prototype._read = function() {
      this.id017PtnairobBondIdTag = this._io.readU1();
      if (this.id017PtnairobBondIdTag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobBondIdTag.TX_ROLLUP_BOND_ID) {
        this.txRollupBondId = new Id017PtnairobTxRollupId(this._io, this, this._root);
      }
      if (this.id017PtnairobBondIdTag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobBondIdTag.SMART_ROLLUP_BOND_ID) {
        this.smartRollupBondId = new Id017PtnairobSmartRollupAddress(this._io, this, this._root);
      }
    }

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    /**
     * A smart rollup address: A smart rollup is identified by a base58 address starting with sr1
     */

    return Id017PtnairobBondId;
  })();

  var Id017PtnairobOperationMetadataAlphaBalanceUpdates0 = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id017PtnairobOperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobOperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId017PtnairobOperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId017PtnairobOperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId017PtnairobOperationMetadataAlphaBalanceUpdates, this._io, "/types/id_017__ptnairob__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id017PtnairobOperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId017PtnairobOperationMetadataAlphaBalanceUpdates);
      var _io__raw_id017PtnairobOperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id017PtnairobOperationMetadataAlphaBalanceUpdates);
      this.id017PtnairobOperationMetadataAlphaBalanceUpdates = new Id017PtnairobOperationMetadataAlphaBalanceUpdates(_io__raw_id017PtnairobOperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id017PtnairobOperationMetadataAlphaBalanceUpdates0;
  })();

  var Id017PtnairobContractId = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobContractId = (function() {
    function Id017PtnairobContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobContractId.prototype._read = function() {
      this.id017PtnairobContractIdTag = this._io.readU1();
      if (this.id017PtnairobContractIdTag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id017PtnairobContractIdTag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id017PtnairobContractId;
  })();

  var Id017PtnairobOperationMetadataAlphaBalanceUpdates = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceUpdates = (function() {
    function Id017PtnairobOperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobOperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id017PtnairobOperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id017PtnairobOperationMetadataAlphaBalanceUpdatesEntries.push(new Id017PtnairobOperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id017PtnairobOperationMetadataAlphaBalanceUpdates;
  })();

  var Id017PtnairobOperationMetadataAlphaBalanceUpdatesEntries = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id017PtnairobOperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobOperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id017PtnairobOperationMetadataAlphaBalance = new Id017PtnairobOperationMetadataAlphaBalance(this._io, this, this._root);
      this.id017PtnairobOperationMetadataAlphaBalanceUpdate = new Id017PtnairobOperationMetadataAlphaBalanceUpdate(this._io, this, this._root);
      this.id017PtnairobOperationMetadataAlphaUpdateOrigin = new Id017PtnairobOperationMetadataAlphaUpdateOrigin(this._io, this, this._root);
    }

    return Id017PtnairobOperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var PublicKeyHash = Id017PtnairobReceiptBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id017PtnairobReceiptBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id017PtnairobReceiptBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id017PtnairobReceiptBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id017PtnairobReceiptBalanceUpdates.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Id017PtnairobOperationMetadataAlphaUpdateOrigin = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaUpdateOrigin = (function() {
    function Id017PtnairobOperationMetadataAlphaUpdateOrigin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobOperationMetadataAlphaUpdateOrigin.prototype._read = function() {
      this.origin = this._io.readU1();
    }

    return Id017PtnairobOperationMetadataAlphaUpdateOrigin;
  })();

  return Id017PtnairobReceiptBalanceUpdates;
})();
return Id017PtnairobReceiptBalanceUpdates;
}));
