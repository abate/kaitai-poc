// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id018ProxfordSmartRollupKind = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 018-Proxford.smart_rollup.kind
 */

var Id018ProxfordSmartRollupKind = (function() {
  Id018ProxfordSmartRollupKind.Id018ProxfordSmartRollupKind = Object.freeze({
    ARITH: 0,
    WASM_2_0_0: 1,
    RISCV: 2,

    0: "ARITH",
    1: "WASM_2_0_0",
    2: "RISCV",
  });

  function Id018ProxfordSmartRollupKind(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id018ProxfordSmartRollupKind.prototype._read = function() {
    this.id018ProxfordSmartRollupKind = this._io.readU1();
  }

  return Id018ProxfordSmartRollupKind;
})();
return Id018ProxfordSmartRollupKind;
}));
