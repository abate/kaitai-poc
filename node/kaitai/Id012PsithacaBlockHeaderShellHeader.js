// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id012PsithacaBlockHeaderShellHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 012-Psithaca.block_header.shell_header
 */

var Id012PsithacaBlockHeaderShellHeader = (function() {
  function Id012PsithacaBlockHeaderShellHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id012PsithacaBlockHeaderShellHeader.prototype._read = function() {
    this.id012PsithacaBlockHeaderShellHeader = new BlockHeaderShell(this._io, this, null);
  }

  return Id012PsithacaBlockHeaderShellHeader;
})();
return Id012PsithacaBlockHeaderShellHeader;
}));
