// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeader'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeader'));
  } else {
    root.Id006PscarthaBlockHeaderRaw = factory(root.KaitaiStream, root.BlockHeader);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeader) {
/**
 * Encoding id: 006-PsCARTHA.block_header.raw
 */

var Id006PscarthaBlockHeaderRaw = (function() {
  function Id006PscarthaBlockHeaderRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaBlockHeaderRaw.prototype._read = function() {
    this.id006PscarthaBlockHeaderRaw = new BlockHeader(this._io, this, null);
  }

  return Id006PscarthaBlockHeaderRaw;
})();
return Id006PscarthaBlockHeaderRaw;
}));
