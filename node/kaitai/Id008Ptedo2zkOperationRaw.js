// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './Operation'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./Operation'));
  } else {
    root.Id008Ptedo2zkOperationRaw = factory(root.KaitaiStream, root.Operation);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, Operation) {
/**
 * Encoding id: 008-PtEdo2Zk.operation.raw
 */

var Id008Ptedo2zkOperationRaw = (function() {
  function Id008Ptedo2zkOperationRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkOperationRaw.prototype._read = function() {
    this.id008Ptedo2zkOperationRaw = new Operation(this._io, this, null);
  }

  return Id008Ptedo2zkOperationRaw;
})();
return Id008Ptedo2zkOperationRaw;
}));
