// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.SaplingTransactionCommitmentHash = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: sapling.transaction.commitment_hash
 */

var SaplingTransactionCommitmentHash = (function() {
  function SaplingTransactionCommitmentHash(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  SaplingTransactionCommitmentHash.prototype._read = function() {
    this.saplingTransactionCommitmentHash = this._io.readBytes(32);
  }

  return SaplingTransactionCommitmentHash;
})();
return SaplingTransactionCommitmentHash;
}));
