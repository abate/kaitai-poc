// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id014PtkathmaVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 014-PtKathma.vote.listings
 */

var Id014PtkathmaVoteListings = (function() {
  Id014PtkathmaVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id014PtkathmaVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id014PtkathmaVoteListings.prototype._read = function() {
    this.lenId014PtkathmaVoteListings = this._io.readU4be();
    if (!(this.lenId014PtkathmaVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId014PtkathmaVoteListings, this._io, "/seq/0");
    }
    this._raw_id014PtkathmaVoteListings = this._io.readBytes(this.lenId014PtkathmaVoteListings);
    var _io__raw_id014PtkathmaVoteListings = new KaitaiStream(this._raw_id014PtkathmaVoteListings);
    this.id014PtkathmaVoteListings = new Id014PtkathmaVoteListings(_io__raw_id014PtkathmaVoteListings, this, this._root);
  }

  var Id014PtkathmaVoteListings = Id014PtkathmaVoteListings.Id014PtkathmaVoteListings = (function() {
    function Id014PtkathmaVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaVoteListings.prototype._read = function() {
      this.id014PtkathmaVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id014PtkathmaVoteListingsEntries.push(new Id014PtkathmaVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id014PtkathmaVoteListings;
  })();

  var Id014PtkathmaVoteListingsEntries = Id014PtkathmaVoteListings.Id014PtkathmaVoteListingsEntries = (function() {
    function Id014PtkathmaVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.votingPower = this._io.readS8be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id014PtkathmaVoteListingsEntries;
  })();

  var PublicKeyHash = Id014PtkathmaVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id014PtkathmaVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id014PtkathmaVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id014PtkathmaVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id014PtkathmaVoteListings;
})();
return Id014PtkathmaVoteListings;
}));
