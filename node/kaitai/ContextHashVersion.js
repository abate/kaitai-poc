// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.ContextHashVersion = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: context_hash_version
 * Description: A version number for the context hash computation
 */

var ContextHashVersion = (function() {
  function ContextHashVersion(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  ContextHashVersion.prototype._read = function() {
    this.contextHashVersion = this._io.readU2be();
  }

  return ContextHashVersion;
})();
return ContextHashVersion;
}));
