// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.AlphaBlockHeaderShellHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: alpha.block_header.shell_header
 */

var AlphaBlockHeaderShellHeader = (function() {
  function AlphaBlockHeaderShellHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaBlockHeaderShellHeader.prototype._read = function() {
    this.alphaBlockHeaderShellHeader = new BlockHeaderShell(this._io, this, null);
  }

  return AlphaBlockHeaderShellHeader;
})();
return AlphaBlockHeaderShellHeader;
}));
