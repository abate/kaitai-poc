// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id007Psdelph1DelegateFrozenBalance = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 007-PsDELPH1.delegate.frozen_balance
 */

var Id007Psdelph1DelegateFrozenBalance = (function() {
  function Id007Psdelph1DelegateFrozenBalance(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1DelegateFrozenBalance.prototype._read = function() {
    this.deposit = new Id007Psdelph1Mutez(this._io, this, this._root);
    this.fees = new Id007Psdelph1Mutez(this._io, this, this._root);
    this.rewards = new Id007Psdelph1Mutez(this._io, this, this._root);
  }

  var Id007Psdelph1Mutez = Id007Psdelph1DelegateFrozenBalance.Id007Psdelph1Mutez = (function() {
    function Id007Psdelph1Mutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1Mutez.prototype._read = function() {
      this.id007Psdelph1Mutez = new N(this._io, this, this._root);
    }

    return Id007Psdelph1Mutez;
  })();

  var N = Id007Psdelph1DelegateFrozenBalance.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var NChunk = Id007Psdelph1DelegateFrozenBalance.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  return Id007Psdelph1DelegateFrozenBalance;
})();
return Id007Psdelph1DelegateFrozenBalance;
}));
