// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id006PscarthaDelegateFrozenBalanceByCycles = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 006-PsCARTHA.delegate.frozen_balance_by_cycles
 */

var Id006PscarthaDelegateFrozenBalanceByCycles = (function() {
  function Id006PscarthaDelegateFrozenBalanceByCycles(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaDelegateFrozenBalanceByCycles.prototype._read = function() {
    this.lenId006PscarthaDelegateFrozenBalanceByCycles = this._io.readU4be();
    if (!(this.lenId006PscarthaDelegateFrozenBalanceByCycles <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId006PscarthaDelegateFrozenBalanceByCycles, this._io, "/seq/0");
    }
    this._raw_id006PscarthaDelegateFrozenBalanceByCycles = this._io.readBytes(this.lenId006PscarthaDelegateFrozenBalanceByCycles);
    var _io__raw_id006PscarthaDelegateFrozenBalanceByCycles = new KaitaiStream(this._raw_id006PscarthaDelegateFrozenBalanceByCycles);
    this.id006PscarthaDelegateFrozenBalanceByCycles = new Id006PscarthaDelegateFrozenBalanceByCycles(_io__raw_id006PscarthaDelegateFrozenBalanceByCycles, this, this._root);
  }

  var Id006PscarthaDelegateFrozenBalanceByCyclesEntries = Id006PscarthaDelegateFrozenBalanceByCycles.Id006PscarthaDelegateFrozenBalanceByCyclesEntries = (function() {
    function Id006PscarthaDelegateFrozenBalanceByCyclesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaDelegateFrozenBalanceByCyclesEntries.prototype._read = function() {
      this.cycle = this._io.readS4be();
      this.deposit = new Id006PscarthaMutez(this._io, this, this._root);
      this.fees = new Id006PscarthaMutez(this._io, this, this._root);
      this.rewards = new Id006PscarthaMutez(this._io, this, this._root);
    }

    return Id006PscarthaDelegateFrozenBalanceByCyclesEntries;
  })();

  var N = Id006PscarthaDelegateFrozenBalanceByCycles.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id006PscarthaMutez = Id006PscarthaDelegateFrozenBalanceByCycles.Id006PscarthaMutez = (function() {
    function Id006PscarthaMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaMutez.prototype._read = function() {
      this.id006PscarthaMutez = new N(this._io, this, this._root);
    }

    return Id006PscarthaMutez;
  })();

  var Id006PscarthaDelegateFrozenBalanceByCycles = Id006PscarthaDelegateFrozenBalanceByCycles.Id006PscarthaDelegateFrozenBalanceByCycles = (function() {
    function Id006PscarthaDelegateFrozenBalanceByCycles(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaDelegateFrozenBalanceByCycles.prototype._read = function() {
      this.id006PscarthaDelegateFrozenBalanceByCyclesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id006PscarthaDelegateFrozenBalanceByCyclesEntries.push(new Id006PscarthaDelegateFrozenBalanceByCyclesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id006PscarthaDelegateFrozenBalanceByCycles;
  })();

  var NChunk = Id006PscarthaDelegateFrozenBalanceByCycles.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  return Id006PscarthaDelegateFrozenBalanceByCycles;
})();
return Id006PscarthaDelegateFrozenBalanceByCycles;
}));
