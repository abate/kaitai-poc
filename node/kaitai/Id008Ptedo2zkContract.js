// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id008Ptedo2zkContract = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 008-PtEdo2Zk.contract
 */

var Id008Ptedo2zkContract = (function() {
  Id008Ptedo2zkContract.Id008Ptedo2zkContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id008Ptedo2zkContract.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id008Ptedo2zkContract(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkContract.prototype._read = function() {
    this.id008Ptedo2zkContractId = new Id008Ptedo2zkContractId(this._io, this, this._root);
  }

  var Id008Ptedo2zkContractId = Id008Ptedo2zkContract.Id008Ptedo2zkContractId = (function() {
    function Id008Ptedo2zkContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkContractId.prototype._read = function() {
      this.id008Ptedo2zkContractIdTag = this._io.readU1();
      if (this.id008Ptedo2zkContractIdTag == Id008Ptedo2zkContract.Id008Ptedo2zkContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id008Ptedo2zkContractIdTag == Id008Ptedo2zkContract.Id008Ptedo2zkContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id008Ptedo2zkContractId;
  })();

  var Originated = Id008Ptedo2zkContract.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var PublicKeyHash = Id008Ptedo2zkContract.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id008Ptedo2zkContract.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id008Ptedo2zkContract.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id008Ptedo2zkContract.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  /**
   * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
   */

  return Id008Ptedo2zkContract;
})();
return Id008Ptedo2zkContract;
}));
