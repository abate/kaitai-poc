// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id010PtgranadBlockHeaderProtocolData = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 010-PtGRANAD.block_header.protocol_data
 */

var Id010PtgranadBlockHeaderProtocolData = (function() {
  Id010PtgranadBlockHeaderProtocolData.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id010PtgranadBlockHeaderProtocolData(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id010PtgranadBlockHeaderProtocolData.prototype._read = function() {
    this.id010PtgranadBlockHeaderAlphaSignedContents = new Id010PtgranadBlockHeaderAlphaSignedContents(this._io, this, this._root);
  }

  var Id010PtgranadBlockHeaderAlphaSignedContents = Id010PtgranadBlockHeaderProtocolData.Id010PtgranadBlockHeaderAlphaSignedContents = (function() {
    function Id010PtgranadBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id010PtgranadBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id010PtgranadBlockHeaderAlphaUnsignedContents = new Id010PtgranadBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id010PtgranadBlockHeaderAlphaSignedContents;
  })();

  var Id010PtgranadBlockHeaderAlphaUnsignedContents = Id010PtgranadBlockHeaderProtocolData.Id010PtgranadBlockHeaderAlphaUnsignedContents = (function() {
    function Id010PtgranadBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id010PtgranadBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id010PtgranadBlockHeaderProtocolData.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingEscapeVote = this._io.readU1();
    }

    return Id010PtgranadBlockHeaderAlphaUnsignedContents;
  })();

  return Id010PtgranadBlockHeaderProtocolData;
})();
return Id010PtgranadBlockHeaderProtocolData;
}));
