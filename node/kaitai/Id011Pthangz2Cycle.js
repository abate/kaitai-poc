// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id011Pthangz2Cycle = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 011-PtHangz2.cycle
 */

var Id011Pthangz2Cycle = (function() {
  function Id011Pthangz2Cycle(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2Cycle.prototype._read = function() {
    this.id011Pthangz2Cycle = this._io.readS4be();
  }

  return Id011Pthangz2Cycle;
})();
return Id011Pthangz2Cycle;
}));
