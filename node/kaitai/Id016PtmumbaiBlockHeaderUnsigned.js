// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id016PtmumbaiBlockHeaderUnsigned = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 016-PtMumbai.block_header.unsigned
 */

var Id016PtmumbaiBlockHeaderUnsigned = (function() {
  Id016PtmumbaiBlockHeaderUnsigned.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id016PtmumbaiBlockHeaderUnsigned(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiBlockHeaderUnsigned.prototype._read = function() {
    this.id016PtmumbaiBlockHeaderUnsigned = new BlockHeaderShell(this._io, this, null);
    this.id016PtmumbaiBlockHeaderAlphaUnsignedContents = new Id016PtmumbaiBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id016PtmumbaiBlockHeaderAlphaUnsignedContents = Id016PtmumbaiBlockHeaderUnsigned.Id016PtmumbaiBlockHeaderAlphaUnsignedContents = (function() {
    function Id016PtmumbaiBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id016PtmumbaiBlockHeaderUnsigned.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id016PtmumbaiLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id016PtmumbaiBlockHeaderAlphaUnsignedContents;
  })();

  var Id016PtmumbaiLiquidityBakingToggleVote = Id016PtmumbaiBlockHeaderUnsigned.Id016PtmumbaiLiquidityBakingToggleVote = (function() {
    function Id016PtmumbaiLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiLiquidityBakingToggleVote.prototype._read = function() {
      this.id016PtmumbaiLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id016PtmumbaiLiquidityBakingToggleVote;
  })();

  return Id016PtmumbaiBlockHeaderUnsigned;
})();
return Id016PtmumbaiBlockHeaderUnsigned;
}));
