// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id012PsithacaVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 012-Psithaca.vote.listings
 */

var Id012PsithacaVoteListings = (function() {
  Id012PsithacaVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id012PsithacaVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id012PsithacaVoteListings.prototype._read = function() {
    this.lenId012PsithacaVoteListings = this._io.readU4be();
    if (!(this.lenId012PsithacaVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId012PsithacaVoteListings, this._io, "/seq/0");
    }
    this._raw_id012PsithacaVoteListings = this._io.readBytes(this.lenId012PsithacaVoteListings);
    var _io__raw_id012PsithacaVoteListings = new KaitaiStream(this._raw_id012PsithacaVoteListings);
    this.id012PsithacaVoteListings = new Id012PsithacaVoteListings(_io__raw_id012PsithacaVoteListings, this, this._root);
  }

  var Id012PsithacaVoteListings = Id012PsithacaVoteListings.Id012PsithacaVoteListings = (function() {
    function Id012PsithacaVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaVoteListings.prototype._read = function() {
      this.id012PsithacaVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id012PsithacaVoteListingsEntries.push(new Id012PsithacaVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id012PsithacaVoteListings;
  })();

  var Id012PsithacaVoteListingsEntries = Id012PsithacaVoteListings.Id012PsithacaVoteListingsEntries = (function() {
    function Id012PsithacaVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.rolls = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id012PsithacaVoteListingsEntries;
  })();

  var PublicKeyHash = Id012PsithacaVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id012PsithacaVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id012PsithacaVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id012PsithacaVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id012PsithacaVoteListings;
})();
return Id012PsithacaVoteListings;
}));
