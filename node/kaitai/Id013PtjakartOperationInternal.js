// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id013PtjakartOperationInternal = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 013-PtJakart.operation.internal
 */

var Id013PtjakartOperationInternal = (function() {
  Id013PtjakartOperationInternal.Id013PtjakartApplyResultsAlphaInternalOperationResultTag = Object.freeze({
    TRANSACTION: 1,
    ORIGINATION: 2,
    DELEGATION: 3,

    1: "TRANSACTION",
    2: "ORIGINATION",
    3: "DELEGATION",
  });

  Id013PtjakartOperationInternal.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id013PtjakartOperationInternal.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id013PtjakartOperationInternal.Id013PtjakartContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id013PtjakartOperationInternal.Id013PtjakartEntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    255: "NAMED",
  });

  Id013PtjakartOperationInternal.Id013PtjakartTransactionDestinationTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,
    TX_ROLLUP: 2,

    0: "IMPLICIT",
    1: "ORIGINATED",
    2: "TX_ROLLUP",
  });

  function Id013PtjakartOperationInternal(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id013PtjakartOperationInternal.prototype._read = function() {
    this.id013PtjakartApplyResultsAlphaInternalOperationResult = new Id013PtjakartApplyResultsAlphaInternalOperationResult(this._io, this, this._root);
  }

  var Originated = Id013PtjakartOperationInternal.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var N = Id013PtjakartOperationInternal.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id013PtjakartEntrypoint = Id013PtjakartOperationInternal.Id013PtjakartEntrypoint = (function() {
    function Id013PtjakartEntrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartEntrypoint.prototype._read = function() {
      this.id013PtjakartEntrypointTag = this._io.readU1();
      if (this.id013PtjakartEntrypointTag == Id013PtjakartOperationInternal.Id013PtjakartEntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id013PtjakartEntrypoint;
  })();

  var Id013PtjakartTxRollupId = Id013PtjakartOperationInternal.Id013PtjakartTxRollupId = (function() {
    function Id013PtjakartTxRollupId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartTxRollupId.prototype._read = function() {
      this.rollupHash = this._io.readBytes(20);
    }

    return Id013PtjakartTxRollupId;
  })();

  var Id013PtjakartContractId = Id013PtjakartOperationInternal.Id013PtjakartContractId = (function() {
    function Id013PtjakartContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartContractId.prototype._read = function() {
      this.id013PtjakartContractIdTag = this._io.readU1();
      if (this.id013PtjakartContractIdTag == Id013PtjakartOperationInternal.Id013PtjakartContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id013PtjakartContractIdTag == Id013PtjakartOperationInternal.Id013PtjakartContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id013PtjakartContractId;
  })();

  var Id013PtjakartTransactionDestination = Id013PtjakartOperationInternal.Id013PtjakartTransactionDestination = (function() {
    function Id013PtjakartTransactionDestination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartTransactionDestination.prototype._read = function() {
      this.id013PtjakartTransactionDestinationTag = this._io.readU1();
      if (this.id013PtjakartTransactionDestinationTag == Id013PtjakartOperationInternal.Id013PtjakartTransactionDestinationTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id013PtjakartTransactionDestinationTag == Id013PtjakartOperationInternal.Id013PtjakartTransactionDestinationTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
      if (this.id013PtjakartTransactionDestinationTag == Id013PtjakartOperationInternal.Id013PtjakartTransactionDestinationTag.TX_ROLLUP) {
        this.txRollup = new TxRollup(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id013PtjakartTransactionDestination;
  })();

  var Id013PtjakartScriptedContracts = Id013PtjakartOperationInternal.Id013PtjakartScriptedContracts = (function() {
    function Id013PtjakartScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id013PtjakartScriptedContracts;
  })();

  var Named = Id013PtjakartOperationInternal.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Delegation = Id013PtjakartOperationInternal.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id013PtjakartOperationInternal.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var Id013PtjakartApplyResultsAlphaInternalOperationResult = Id013PtjakartOperationInternal.Id013PtjakartApplyResultsAlphaInternalOperationResult = (function() {
    function Id013PtjakartApplyResultsAlphaInternalOperationResult(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartApplyResultsAlphaInternalOperationResult.prototype._read = function() {
      this.source = new Id013PtjakartContractId(this._io, this, this._root);
      this.nonce = this._io.readU2be();
      this.id013PtjakartApplyResultsAlphaInternalOperationResultTag = this._io.readU1();
      if (this.id013PtjakartApplyResultsAlphaInternalOperationResultTag == Id013PtjakartOperationInternal.Id013PtjakartApplyResultsAlphaInternalOperationResultTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id013PtjakartApplyResultsAlphaInternalOperationResultTag == Id013PtjakartOperationInternal.Id013PtjakartApplyResultsAlphaInternalOperationResultTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id013PtjakartApplyResultsAlphaInternalOperationResultTag == Id013PtjakartOperationInternal.Id013PtjakartApplyResultsAlphaInternalOperationResultTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Id013PtjakartApplyResultsAlphaInternalOperationResult;
  })();

  var Id013PtjakartMutez = Id013PtjakartOperationInternal.Id013PtjakartMutez = (function() {
    function Id013PtjakartMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartMutez.prototype._read = function() {
      this.id013PtjakartMutez = new N(this._io, this, this._root);
    }

    return Id013PtjakartMutez;
  })();

  var BytesDynUint30 = Id013PtjakartOperationInternal.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Origination = Id013PtjakartOperationInternal.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.balance = new Id013PtjakartMutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id013PtjakartOperationInternal.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id013PtjakartScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var NChunk = Id013PtjakartOperationInternal.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Named0 = Id013PtjakartOperationInternal.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id013PtjakartOperationInternal.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.amount = new Id013PtjakartMutez(this._io, this, this._root);
      this.destination = new Id013PtjakartTransactionDestination(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id013PtjakartOperationInternal.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A destination of a transaction: A destination notation compatible with the contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash, a base58 originated contract hash, or a base58 originated transaction rollup.
     */

    return Transaction;
  })();

  var Parameters = Id013PtjakartOperationInternal.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id013PtjakartEntrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var PublicKeyHash = Id013PtjakartOperationInternal.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id013PtjakartOperationInternal.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id013PtjakartOperationInternal.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id013PtjakartOperationInternal.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var TxRollup = Id013PtjakartOperationInternal.TxRollup = (function() {
    function TxRollup(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TxRollup.prototype._read = function() {
      this.id013PtjakartTxRollupId = new Id013PtjakartTxRollupId(this._io, this, this._root);
      this.txRollupPadding = this._io.readBytes(1);
    }

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    /**
     * This field is for padding, ignore
     */

    return TxRollup;
  })();

  return Id013PtjakartOperationInternal;
})();
return Id013PtjakartOperationInternal;
}));
