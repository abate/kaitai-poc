// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeader'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeader'));
  } else {
    root.Id011Pthangz2BlockHeaderRaw = factory(root.KaitaiStream, root.BlockHeader);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeader) {
/**
 * Encoding id: 011-PtHangz2.block_header.raw
 */

var Id011Pthangz2BlockHeaderRaw = (function() {
  function Id011Pthangz2BlockHeaderRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2BlockHeaderRaw.prototype._read = function() {
    this.id011Pthangz2BlockHeaderRaw = new BlockHeader(this._io, this, null);
  }

  return Id011Pthangz2BlockHeaderRaw;
})();
return Id011Pthangz2BlockHeaderRaw;
}));
