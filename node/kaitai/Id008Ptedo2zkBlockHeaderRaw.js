// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeader'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeader'));
  } else {
    root.Id008Ptedo2zkBlockHeaderRaw = factory(root.KaitaiStream, root.BlockHeader);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeader) {
/**
 * Encoding id: 008-PtEdo2Zk.block_header.raw
 */

var Id008Ptedo2zkBlockHeaderRaw = (function() {
  function Id008Ptedo2zkBlockHeaderRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkBlockHeaderRaw.prototype._read = function() {
    this.id008Ptedo2zkBlockHeaderRaw = new BlockHeader(this._io, this, null);
  }

  return Id008Ptedo2zkBlockHeaderRaw;
})();
return Id008Ptedo2zkBlockHeaderRaw;
}));
