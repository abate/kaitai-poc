// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id007Psdelph1Script = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 007-PsDELPH1.script
 */

var Id007Psdelph1Script = (function() {
  function Id007Psdelph1Script(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1Script.prototype._read = function() {
    this.id007Psdelph1ScriptedContracts = new Id007Psdelph1ScriptedContracts(this._io, this, this._root);
  }

  var BytesDynUint30 = Id007Psdelph1Script.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Id007Psdelph1ScriptedContracts = Id007Psdelph1Script.Id007Psdelph1ScriptedContracts = (function() {
    function Id007Psdelph1ScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1ScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id007Psdelph1ScriptedContracts;
  })();

  return Id007Psdelph1Script;
})();
return Id007Psdelph1Script;
}));
