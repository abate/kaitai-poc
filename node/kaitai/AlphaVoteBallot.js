// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaVoteBallot = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.vote.ballot
 */

var AlphaVoteBallot = (function() {
  function AlphaVoteBallot(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaVoteBallot.prototype._read = function() {
    this.alphaVoteBallot = this._io.readS1();
  }

  return AlphaVoteBallot;
})();
return AlphaVoteBallot;
}));
