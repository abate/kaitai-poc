// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id017PtnairobParameters = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 017-PtNairob.parameters
 */

var Id017PtnairobParameters = (function() {
  Id017PtnairobParameters.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id017PtnairobParameters.BootstrapAccountsEltTag = Object.freeze({
    PUBLIC_KEY_KNOWN: 0,
    PUBLIC_KEY_UNKNOWN: 1,
    PUBLIC_KEY_KNOWN_WITH_DELEGATE: 2,
    PUBLIC_KEY_UNKNOWN_WITH_DELEGATE: 3,
    PUBLIC_KEY_KNOWN_WITH_CONSENSUS_KEY: 4,

    0: "PUBLIC_KEY_KNOWN",
    1: "PUBLIC_KEY_UNKNOWN",
    2: "PUBLIC_KEY_KNOWN_WITH_DELEGATE",
    3: "PUBLIC_KEY_UNKNOWN_WITH_DELEGATE",
    4: "PUBLIC_KEY_KNOWN_WITH_CONSENSUS_KEY",
  });

  Id017PtnairobParameters.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  Id017PtnairobParameters.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function Id017PtnairobParameters(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id017PtnairobParameters.prototype._read = function() {
    this.bootstrapAccounts = new BootstrapAccounts0(this._io, this, this._root);
    this.bootstrapContracts = new BootstrapContracts0(this._io, this, this._root);
    this.commitments = new Commitments0(this._io, this, this._root);
    this.securityDepositRampUpCyclesTag = this._io.readU1();
    if (this.securityDepositRampUpCyclesTag == Id017PtnairobParameters.Bool.TRUE) {
      this.securityDepositRampUpCycles = new Int31(this._io, this, this._root);
    }
    this.noRewardCyclesTag = this._io.readU1();
    if (this.noRewardCyclesTag == Id017PtnairobParameters.Bool.TRUE) {
      this.noRewardCycles = new Int31(this._io, this, this._root);
    }
    this.preservedCycles = this._io.readU1();
    this.blocksPerCycle = this._io.readS4be();
    this.blocksPerCommitment = this._io.readS4be();
    this.nonceRevelationThreshold = this._io.readS4be();
    this.blocksPerStakeSnapshot = this._io.readS4be();
    this.cyclesPerVotingPeriod = this._io.readS4be();
    this.hardGasLimitPerOperation = new Z(this._io, this, this._root);
    this.hardGasLimitPerBlock = new Z(this._io, this, this._root);
    this.proofOfWorkThreshold = this._io.readS8be();
    this.minimalStake = new Id017PtnairobMutez(this._io, this, this._root);
    this.vdfDifficulty = this._io.readS8be();
    this.seedNonceRevelationTip = new Id017PtnairobMutez(this._io, this, this._root);
    this.originationSize = new Int31(this._io, this, this._root);
    this.bakingRewardFixedPortion = new Id017PtnairobMutez(this._io, this, this._root);
    this.bakingRewardBonusPerSlot = new Id017PtnairobMutez(this._io, this, this._root);
    this.endorsingRewardPerSlot = new Id017PtnairobMutez(this._io, this, this._root);
    this.costPerByte = new Id017PtnairobMutez(this._io, this, this._root);
    this.hardStorageLimitPerOperation = new Z(this._io, this, this._root);
    this.quorumMin = this._io.readS4be();
    this.quorumMax = this._io.readS4be();
    this.minProposalQuorum = this._io.readS4be();
    this.liquidityBakingSubsidy = new Id017PtnairobMutez(this._io, this, this._root);
    this.liquidityBakingToggleEmaThreshold = this._io.readS4be();
    this.maxOperationsTimeToLive = this._io.readS2be();
    this.minimalBlockDelay = this._io.readS8be();
    this.delayIncrementPerRound = this._io.readS8be();
    this.consensusCommitteeSize = new Int31(this._io, this, this._root);
    this.consensusThreshold = new Int31(this._io, this, this._root);
    this.minimalParticipationRatio = new MinimalParticipationRatio(this._io, this, this._root);
    this.maxSlashingPeriod = new Int31(this._io, this, this._root);
    this.frozenDepositsPercentage = new Int31(this._io, this, this._root);
    this.doubleBakingPunishment = new Id017PtnairobMutez(this._io, this, this._root);
    this.ratioOfFrozenDepositsSlashedPerDoubleEndorsement = new RatioOfFrozenDepositsSlashedPerDoubleEndorsement(this._io, this, this._root);
    this.testnetDictatorTag = this._io.readU1();
    if (this.testnetDictatorTag == Id017PtnairobParameters.Bool.TRUE) {
      this.testnetDictator = new PublicKeyHash(this._io, this, this._root);
    }
    this.initialSeedTag = this._io.readU1();
    if (this.initialSeedTag == Id017PtnairobParameters.Bool.TRUE) {
      this.initialSeed = this._io.readBytes(32);
    }
    this.cacheScriptSize = new Int31(this._io, this, this._root);
    this.cacheStakeDistributionCycles = this._io.readS1();
    this.cacheSamplerStateCycles = this._io.readS1();
    this.txRollupEnable = this._io.readU1();
    this.txRollupOriginationSize = new Int31(this._io, this, this._root);
    this.txRollupHardSizeLimitPerInbox = new Int31(this._io, this, this._root);
    this.txRollupHardSizeLimitPerMessage = new Int31(this._io, this, this._root);
    this.txRollupMaxWithdrawalsPerBatch = new Int31(this._io, this, this._root);
    this.txRollupCommitmentBond = new Id017PtnairobMutez(this._io, this, this._root);
    this.txRollupFinalityPeriod = new Int31(this._io, this, this._root);
    this.txRollupWithdrawPeriod = new Int31(this._io, this, this._root);
    this.txRollupMaxInboxesCount = new Int31(this._io, this, this._root);
    this.txRollupMaxMessagesPerInbox = new Int31(this._io, this, this._root);
    this.txRollupMaxCommitmentsCount = new Int31(this._io, this, this._root);
    this.txRollupCostPerByteEmaFactor = new Int31(this._io, this, this._root);
    this.txRollupMaxTicketPayloadSize = new Int31(this._io, this, this._root);
    this.txRollupRejectionMaxProofSize = new Int31(this._io, this, this._root);
    this.txRollupSunsetLevel = this._io.readS4be();
    this.dalParametric = new DalParametric(this._io, this, this._root);
    this.smartRollupEnable = this._io.readU1();
    this.smartRollupArithPvmEnable = this._io.readU1();
    this.smartRollupOriginationSize = new Int31(this._io, this, this._root);
    this.smartRollupChallengeWindowInBlocks = new Int31(this._io, this, this._root);
    this.smartRollupStakeAmount = new Id017PtnairobMutez(this._io, this, this._root);
    this.smartRollupCommitmentPeriodInBlocks = new Int31(this._io, this, this._root);
    this.smartRollupMaxLookaheadInBlocks = this._io.readS4be();
    this.smartRollupMaxActiveOutboxLevels = this._io.readS4be();
    this.smartRollupMaxOutboxMessagesPerLevel = new Int31(this._io, this, this._root);
    this.smartRollupNumberOfSectionsInDissection = this._io.readU1();
    this.smartRollupTimeoutPeriodInBlocks = new Int31(this._io, this, this._root);
    this.smartRollupMaxNumberOfCementedCommitments = new Int31(this._io, this, this._root);
    this.smartRollupMaxNumberOfParallelGames = new Int31(this._io, this, this._root);
    this.zkRollupEnable = this._io.readU1();
    this.zkRollupOriginationSize = new Int31(this._io, this, this._root);
    this.zkRollupMinPendingToProcess = new Int31(this._io, this, this._root);
  }

  var RatioOfFrozenDepositsSlashedPerDoubleEndorsement = Id017PtnairobParameters.RatioOfFrozenDepositsSlashedPerDoubleEndorsement = (function() {
    function RatioOfFrozenDepositsSlashedPerDoubleEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RatioOfFrozenDepositsSlashedPerDoubleEndorsement.prototype._read = function() {
      this.numerator = this._io.readU2be();
      this.denominator = this._io.readU2be();
    }

    return RatioOfFrozenDepositsSlashedPerDoubleEndorsement;
  })();

  var PublicKeyKnown = Id017PtnairobParameters.PublicKeyKnown = (function() {
    function PublicKeyKnown(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyKnown.prototype._read = function() {
      this.publicKeyKnownField0 = new PublicKey(this._io, this, this._root);
      this.publicKeyKnownField1 = new Id017PtnairobMutez(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key
     * 
     * signature__public_key
     */

    /**
     * id_017__ptnairob__mutez
     */

    return PublicKeyKnown;
  })();

  var Commitments = Id017PtnairobParameters.Commitments = (function() {
    function Commitments(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments.prototype._read = function() {
      this.commitmentsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.commitmentsEntries.push(new CommitmentsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Commitments;
  })();

  var DalParametric = Id017PtnairobParameters.DalParametric = (function() {
    function DalParametric(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalParametric.prototype._read = function() {
      this.featureEnable = this._io.readU1();
      this.numberOfSlots = this._io.readS2be();
      this.attestationLag = this._io.readS2be();
      this.attestationThreshold = this._io.readS2be();
      this.blocksPerEpoch = this._io.readS4be();
      this.redundancyFactor = this._io.readU1();
      this.pageSize = this._io.readU2be();
      this.slotSize = new Int31(this._io, this, this._root);
      this.numberOfShards = this._io.readU2be();
    }

    return DalParametric;
  })();

  var N = Id017PtnairobParameters.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var CommitmentsEntries = Id017PtnairobParameters.CommitmentsEntries = (function() {
    function CommitmentsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CommitmentsEntries.prototype._read = function() {
      this.commitmentsEltField0 = this._io.readBytes(20);
      this.commitmentsEltField1 = new Id017PtnairobMutez(this._io, this, this._root);
    }

    /**
     * blinded__public__key__hash
     */

    /**
     * id_017__ptnairob__mutez
     */

    return CommitmentsEntries;
  })();

  var Commitments0 = Id017PtnairobParameters.Commitments0 = (function() {
    function Commitments0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments0.prototype._read = function() {
      this.lenCommitments = this._io.readU4be();
      if (!(this.lenCommitments <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCommitments, this._io, "/types/commitments_0/seq/0");
      }
      this._raw_commitments = this._io.readBytes(this.lenCommitments);
      var _io__raw_commitments = new KaitaiStream(this._raw_commitments);
      this.commitments = new Commitments(_io__raw_commitments, this, this._root);
    }

    return Commitments0;
  })();

  var MinimalParticipationRatio = Id017PtnairobParameters.MinimalParticipationRatio = (function() {
    function MinimalParticipationRatio(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MinimalParticipationRatio.prototype._read = function() {
      this.numerator = this._io.readU2be();
      this.denominator = this._io.readU2be();
    }

    return MinimalParticipationRatio;
  })();

  var BootstrapAccounts0 = Id017PtnairobParameters.BootstrapAccounts0 = (function() {
    function BootstrapAccounts0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccounts0.prototype._read = function() {
      this.lenBootstrapAccounts = this._io.readU4be();
      if (!(this.lenBootstrapAccounts <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBootstrapAccounts, this._io, "/types/bootstrap_accounts_0/seq/0");
      }
      this._raw_bootstrapAccounts = this._io.readBytes(this.lenBootstrapAccounts);
      var _io__raw_bootstrapAccounts = new KaitaiStream(this._raw_bootstrapAccounts);
      this.bootstrapAccounts = new BootstrapAccounts(_io__raw_bootstrapAccounts, this, this._root);
    }

    return BootstrapAccounts0;
  })();

  var PublicKey = Id017PtnairobParameters.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id017PtnairobParameters.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id017PtnairobParameters.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id017PtnairobParameters.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id017PtnairobParameters.PublicKeyTag.BLS) {
        this.bls = this._io.readBytes(48);
      }
    }

    return PublicKey;
  })();

  var PublicKeyUnknown = Id017PtnairobParameters.PublicKeyUnknown = (function() {
    function PublicKeyUnknown(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyUnknown.prototype._read = function() {
      this.publicKeyUnknownField0 = new PublicKeyHash(this._io, this, this._root);
      this.publicKeyUnknownField1 = new Id017PtnairobMutez(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     * 
     * signature__public_key_hash
     */

    /**
     * id_017__ptnairob__mutez
     */

    return PublicKeyUnknown;
  })();

  var PublicKeyUnknownWithDelegate = Id017PtnairobParameters.PublicKeyUnknownWithDelegate = (function() {
    function PublicKeyUnknownWithDelegate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyUnknownWithDelegate.prototype._read = function() {
      this.publicKeyUnknownWithDelegateField0 = new PublicKeyHash(this._io, this, this._root);
      this.publicKeyUnknownWithDelegateField1 = new Id017PtnairobMutez(this._io, this, this._root);
      this.publicKeyUnknownWithDelegateField2 = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     * 
     * signature__public_key_hash
     */

    /**
     * id_017__ptnairob__mutez
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     * 
     * signature__public_key_hash
     */

    return PublicKeyUnknownWithDelegate;
  })();

  var BootstrapAccountsEntries = Id017PtnairobParameters.BootstrapAccountsEntries = (function() {
    function BootstrapAccountsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccountsEntries.prototype._read = function() {
      this.bootstrapAccountsEltTag = this._io.readU1();
      if (this.bootstrapAccountsEltTag == Id017PtnairobParameters.BootstrapAccountsEltTag.PUBLIC_KEY_KNOWN) {
        this.publicKeyKnown = new PublicKeyKnown(this._io, this, this._root);
      }
      if (this.bootstrapAccountsEltTag == Id017PtnairobParameters.BootstrapAccountsEltTag.PUBLIC_KEY_UNKNOWN) {
        this.publicKeyUnknown = new PublicKeyUnknown(this._io, this, this._root);
      }
      if (this.bootstrapAccountsEltTag == Id017PtnairobParameters.BootstrapAccountsEltTag.PUBLIC_KEY_KNOWN_WITH_DELEGATE) {
        this.publicKeyKnownWithDelegate = new PublicKeyKnownWithDelegate(this._io, this, this._root);
      }
      if (this.bootstrapAccountsEltTag == Id017PtnairobParameters.BootstrapAccountsEltTag.PUBLIC_KEY_UNKNOWN_WITH_DELEGATE) {
        this.publicKeyUnknownWithDelegate = new PublicKeyUnknownWithDelegate(this._io, this, this._root);
      }
      if (this.bootstrapAccountsEltTag == Id017PtnairobParameters.BootstrapAccountsEltTag.PUBLIC_KEY_KNOWN_WITH_CONSENSUS_KEY) {
        this.publicKeyKnownWithConsensusKey = new PublicKeyKnownWithConsensusKey(this._io, this, this._root);
      }
    }

    return BootstrapAccountsEntries;
  })();

  var BootstrapContracts0 = Id017PtnairobParameters.BootstrapContracts0 = (function() {
    function BootstrapContracts0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContracts0.prototype._read = function() {
      this.lenBootstrapContracts = this._io.readU4be();
      if (!(this.lenBootstrapContracts <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBootstrapContracts, this._io, "/types/bootstrap_contracts_0/seq/0");
      }
      this._raw_bootstrapContracts = this._io.readBytes(this.lenBootstrapContracts);
      var _io__raw_bootstrapContracts = new KaitaiStream(this._raw_bootstrapContracts);
      this.bootstrapContracts = new BootstrapContracts(_io__raw_bootstrapContracts, this, this._root);
    }

    return BootstrapContracts0;
  })();

  var Id017PtnairobScriptedContracts = Id017PtnairobParameters.Id017PtnairobScriptedContracts = (function() {
    function Id017PtnairobScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id017PtnairobScriptedContracts;
  })();

  var BytesDynUint30 = Id017PtnairobParameters.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var PublicKeyKnownWithDelegate = Id017PtnairobParameters.PublicKeyKnownWithDelegate = (function() {
    function PublicKeyKnownWithDelegate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyKnownWithDelegate.prototype._read = function() {
      this.publicKeyKnownWithDelegateField0 = new PublicKey(this._io, this, this._root);
      this.publicKeyKnownWithDelegateField1 = new Id017PtnairobMutez(this._io, this, this._root);
      this.publicKeyKnownWithDelegateField2 = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key
     * 
     * signature__public_key
     */

    /**
     * id_017__ptnairob__mutez
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     * 
     * signature__public_key_hash
     */

    return PublicKeyKnownWithDelegate;
  })();

  var BootstrapContractsEntries = Id017PtnairobParameters.BootstrapContractsEntries = (function() {
    function BootstrapContractsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContractsEntries.prototype._read = function() {
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id017PtnairobParameters.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.amount = new Id017PtnairobMutez(this._io, this, this._root);
      this.script = new Id017PtnairobScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return BootstrapContractsEntries;
  })();

  var BootstrapContracts = Id017PtnairobParameters.BootstrapContracts = (function() {
    function BootstrapContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContracts.prototype._read = function() {
      this.bootstrapContractsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bootstrapContractsEntries.push(new BootstrapContractsEntries(this._io, this, this._root));
        i++;
      }
    }

    return BootstrapContracts;
  })();

  var BootstrapAccounts = Id017PtnairobParameters.BootstrapAccounts = (function() {
    function BootstrapAccounts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccounts.prototype._read = function() {
      this.bootstrapAccountsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bootstrapAccountsEntries.push(new BootstrapAccountsEntries(this._io, this, this._root));
        i++;
      }
    }

    return BootstrapAccounts;
  })();

  var Int31 = Id017PtnairobParameters.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id017PtnairobParameters.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Id017PtnairobMutez = Id017PtnairobParameters.Id017PtnairobMutez = (function() {
    function Id017PtnairobMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobMutez.prototype._read = function() {
      this.id017PtnairobMutez = new N(this._io, this, this._root);
    }

    return Id017PtnairobMutez;
  })();

  var PublicKeyKnownWithConsensusKey = Id017PtnairobParameters.PublicKeyKnownWithConsensusKey = (function() {
    function PublicKeyKnownWithConsensusKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyKnownWithConsensusKey.prototype._read = function() {
      this.publicKeyKnownWithConsensusKeyField0 = new PublicKey(this._io, this, this._root);
      this.publicKeyKnownWithConsensusKeyField1 = new Id017PtnairobMutez(this._io, this, this._root);
      this.publicKeyKnownWithConsensusKeyField2 = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key
     * 
     * signature__public_key
     */

    /**
     * id_017__ptnairob__mutez
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     * 
     * signature__public_key
     */

    return PublicKeyKnownWithConsensusKey;
  })();

  var PublicKeyHash = Id017PtnairobParameters.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id017PtnairobParameters.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id017PtnairobParameters.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id017PtnairobParameters.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id017PtnairobParameters.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Z = Id017PtnairobParameters.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  /**
   * A Ed25519, Secp256k1, P256, or BLS public key hash
   */

  return Id017PtnairobParameters;
})();
return Id017PtnairobParameters;
}));
