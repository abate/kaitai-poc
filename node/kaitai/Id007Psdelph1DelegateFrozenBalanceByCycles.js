// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id007Psdelph1DelegateFrozenBalanceByCycles = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 007-PsDELPH1.delegate.frozen_balance_by_cycles
 */

var Id007Psdelph1DelegateFrozenBalanceByCycles = (function() {
  function Id007Psdelph1DelegateFrozenBalanceByCycles(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1DelegateFrozenBalanceByCycles.prototype._read = function() {
    this.lenId007Psdelph1DelegateFrozenBalanceByCycles = this._io.readU4be();
    if (!(this.lenId007Psdelph1DelegateFrozenBalanceByCycles <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId007Psdelph1DelegateFrozenBalanceByCycles, this._io, "/seq/0");
    }
    this._raw_id007Psdelph1DelegateFrozenBalanceByCycles = this._io.readBytes(this.lenId007Psdelph1DelegateFrozenBalanceByCycles);
    var _io__raw_id007Psdelph1DelegateFrozenBalanceByCycles = new KaitaiStream(this._raw_id007Psdelph1DelegateFrozenBalanceByCycles);
    this.id007Psdelph1DelegateFrozenBalanceByCycles = new Id007Psdelph1DelegateFrozenBalanceByCycles(_io__raw_id007Psdelph1DelegateFrozenBalanceByCycles, this, this._root);
  }

  var N = Id007Psdelph1DelegateFrozenBalanceByCycles.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id007Psdelph1DelegateFrozenBalanceByCyclesEntries = Id007Psdelph1DelegateFrozenBalanceByCycles.Id007Psdelph1DelegateFrozenBalanceByCyclesEntries = (function() {
    function Id007Psdelph1DelegateFrozenBalanceByCyclesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1DelegateFrozenBalanceByCyclesEntries.prototype._read = function() {
      this.cycle = this._io.readS4be();
      this.deposit = new Id007Psdelph1Mutez(this._io, this, this._root);
      this.fees = new Id007Psdelph1Mutez(this._io, this, this._root);
      this.rewards = new Id007Psdelph1Mutez(this._io, this, this._root);
    }

    return Id007Psdelph1DelegateFrozenBalanceByCyclesEntries;
  })();

  var Id007Psdelph1DelegateFrozenBalanceByCycles = Id007Psdelph1DelegateFrozenBalanceByCycles.Id007Psdelph1DelegateFrozenBalanceByCycles = (function() {
    function Id007Psdelph1DelegateFrozenBalanceByCycles(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1DelegateFrozenBalanceByCycles.prototype._read = function() {
      this.id007Psdelph1DelegateFrozenBalanceByCyclesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id007Psdelph1DelegateFrozenBalanceByCyclesEntries.push(new Id007Psdelph1DelegateFrozenBalanceByCyclesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id007Psdelph1DelegateFrozenBalanceByCycles;
  })();

  var Id007Psdelph1Mutez = Id007Psdelph1DelegateFrozenBalanceByCycles.Id007Psdelph1Mutez = (function() {
    function Id007Psdelph1Mutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id007Psdelph1Mutez.prototype._read = function() {
      this.id007Psdelph1Mutez = new N(this._io, this, this._root);
    }

    return Id007Psdelph1Mutez;
  })();

  var NChunk = Id007Psdelph1DelegateFrozenBalanceByCycles.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  return Id007Psdelph1DelegateFrozenBalanceByCycles;
})();
return Id007Psdelph1DelegateFrozenBalanceByCycles;
}));
