// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaReceiptBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.receipt.balance_updates
 */

var Id019PtparisaReceiptBalanceUpdates = (function() {
  Id019PtparisaReceiptBalanceUpdates.Id019PtparisaContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag = Object.freeze({
    CONTRACT: 0,
    BLOCK_FEES: 2,
    DEPOSITS: 4,
    NONCE_REVELATION_REWARDS: 5,
    ATTESTING_REWARDS: 7,
    BAKING_REWARDS: 8,
    BAKING_BONUSES: 9,
    STORAGE_FEES: 11,
    DOUBLE_SIGNING_PUNISHMENTS: 12,
    LOST_ATTESTING_REWARDS: 13,
    LIQUIDITY_BAKING_SUBSIDIES: 14,
    BURNED: 15,
    COMMITMENTS: 16,
    BOOTSTRAP: 17,
    INVOICE: 18,
    INITIAL_COMMITMENTS: 19,
    MINTED: 20,
    FROZEN_BONDS: 21,
    SMART_ROLLUP_REFUTATION_PUNISHMENTS: 24,
    SMART_ROLLUP_REFUTATION_REWARDS: 25,
    UNSTAKED_DEPOSITS: 26,
    STAKING_DELEGATOR_NUMERATOR: 27,
    STAKING_DELEGATE_DENOMINATOR: 28,

    0: "CONTRACT",
    2: "BLOCK_FEES",
    4: "DEPOSITS",
    5: "NONCE_REVELATION_REWARDS",
    7: "ATTESTING_REWARDS",
    8: "BAKING_REWARDS",
    9: "BAKING_BONUSES",
    11: "STORAGE_FEES",
    12: "DOUBLE_SIGNING_PUNISHMENTS",
    13: "LOST_ATTESTING_REWARDS",
    14: "LIQUIDITY_BAKING_SUBSIDIES",
    15: "BURNED",
    16: "COMMITMENTS",
    17: "BOOTSTRAP",
    18: "INVOICE",
    19: "INITIAL_COMMITMENTS",
    20: "MINTED",
    21: "FROZEN_BONDS",
    24: "SMART_ROLLUP_REFUTATION_PUNISHMENTS",
    25: "SMART_ROLLUP_REFUTATION_REWARDS",
    26: "UNSTAKED_DEPOSITS",
    27: "STAKING_DELEGATOR_NUMERATOR",
    28: "STAKING_DELEGATE_DENOMINATOR",
  });

  Id019PtparisaReceiptBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  Id019PtparisaReceiptBalanceUpdates.Id019PtparisaStakerTag = Object.freeze({
    SINGLE: 0,
    SHARED: 1,

    0: "SINGLE",
    1: "SHARED",
  });

  Id019PtparisaReceiptBalanceUpdates.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id019PtparisaReceiptBalanceUpdates.Id019PtparisaFrozenStakerTag = Object.freeze({
    SINGLE: 0,
    SHARED: 1,
    BAKER: 2,

    0: "SINGLE",
    1: "SHARED",
    2: "BAKER",
  });

  Id019PtparisaReceiptBalanceUpdates.Id019PtparisaBondIdTag = Object.freeze({
    SMART_ROLLUP_BOND_ID: 1,

    1: "SMART_ROLLUP_BOND_ID",
  });

  Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaUpdateOriginTag = Object.freeze({
    BLOCK_APPLICATION: 0,
    PROTOCOL_MIGRATION: 1,
    SUBSIDY: 2,
    SIMULATION: 3,
    DELAYED_OPERATION: 4,

    0: "BLOCK_APPLICATION",
    1: "PROTOCOL_MIGRATION",
    2: "SUBSIDY",
    3: "SIMULATION",
    4: "DELAYED_OPERATION",
  });

  function Id019PtparisaReceiptBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaReceiptBalanceUpdates.prototype._read = function() {
    this.id019PtparisaOperationMetadataAlphaBalanceUpdates = new Id019PtparisaOperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Id019PtparisaOperationMetadataAlphaBalanceAndUpdate = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdate = (function() {
    function Id019PtparisaOperationMetadataAlphaBalanceAndUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaOperationMetadataAlphaBalanceAndUpdate.prototype._read = function() {
      this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag = this._io.readU1();
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.CONTRACT) {
        this.contract = new Contract(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.BLOCK_FEES) {
        this.blockFees = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.DEPOSITS) {
        this.deposits = new Deposits(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.NONCE_REVELATION_REWARDS) {
        this.nonceRevelationRewards = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.ATTESTING_REWARDS) {
        this.attestingRewards = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.BAKING_REWARDS) {
        this.bakingRewards = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.BAKING_BONUSES) {
        this.bakingBonuses = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.STORAGE_FEES) {
        this.storageFees = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.DOUBLE_SIGNING_PUNISHMENTS) {
        this.doubleSigningPunishments = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.LOST_ATTESTING_REWARDS) {
        this.lostAttestingRewards = new LostAttestingRewards(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.LIQUIDITY_BAKING_SUBSIDIES) {
        this.liquidityBakingSubsidies = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.BURNED) {
        this.burned = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.COMMITMENTS) {
        this.commitments = new Commitments(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.BOOTSTRAP) {
        this.bootstrap = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.INVOICE) {
        this.invoice = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.INITIAL_COMMITMENTS) {
        this.initialCommitments = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.MINTED) {
        this.minted = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.FROZEN_BONDS) {
        this.frozenBonds = new FrozenBonds(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.SMART_ROLLUP_REFUTATION_PUNISHMENTS) {
        this.smartRollupRefutationPunishments = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.SMART_ROLLUP_REFUTATION_REWARDS) {
        this.smartRollupRefutationRewards = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.UNSTAKED_DEPOSITS) {
        this.unstakedDeposits = new UnstakedDeposits(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.STAKING_DELEGATOR_NUMERATOR) {
        this.stakingDelegatorNumerator = new StakingDelegatorNumerator(this._io, this, this._root);
      }
      if (this.id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.STAKING_DELEGATE_DENOMINATOR) {
        this.stakingDelegateDenominator = new StakingDelegateDenominator(this._io, this, this._root);
      }
    }

    return Id019PtparisaOperationMetadataAlphaBalanceAndUpdate;
  })();

  var Id019PtparisaStaker = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaStaker = (function() {
    function Id019PtparisaStaker(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaStaker.prototype._read = function() {
      this.id019PtparisaStakerTag = this._io.readU1();
      if (this.id019PtparisaStakerTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaStakerTag.SINGLE) {
        this.single = new Single(this._io, this, this._root);
      }
      if (this.id019PtparisaStakerTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaStakerTag.SHARED) {
        this.shared = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id019PtparisaStaker;
  })();

  var Id019PtparisaBondId = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaBondId = (function() {
    function Id019PtparisaBondId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaBondId.prototype._read = function() {
      this.id019PtparisaBondIdTag = this._io.readU1();
      if (this.id019PtparisaBondIdTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaBondIdTag.SMART_ROLLUP_BOND_ID) {
        this.smartRollupBondId = this._io.readBytes(20);
      }
    }

    return Id019PtparisaBondId;
  })();

  var Commitments = Id019PtparisaReceiptBalanceUpdates.Commitments = (function() {
    function Commitments(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments.prototype._read = function() {
      this.committer = this._io.readBytes(20);
      this.id019PtparisaOperationMetadataAlphaTezBalanceUpdate = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    return Commitments;
  })();

  var FrozenBonds = Id019PtparisaReceiptBalanceUpdates.FrozenBonds = (function() {
    function FrozenBonds(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    FrozenBonds.prototype._read = function() {
      this.contract = new Id019PtparisaContractId(this._io, this, this._root);
      this.bondId = new Id019PtparisaBondId(this._io, this, this._root);
      this.id019PtparisaOperationMetadataAlphaTezBalanceUpdate = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return FrozenBonds;
  })();

  var Originated = Id019PtparisaReceiptBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Id019PtparisaContractId = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaContractId = (function() {
    function Id019PtparisaContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaContractId.prototype._read = function() {
      this.id019PtparisaContractIdTag = this._io.readU1();
      if (this.id019PtparisaContractIdTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id019PtparisaContractIdTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id019PtparisaContractId;
  })();

  var Id019PtparisaOperationMetadataAlphaBalanceUpdates = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceUpdates = (function() {
    function Id019PtparisaOperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaOperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id019PtparisaOperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id019PtparisaOperationMetadataAlphaBalanceUpdatesEntries.push(new Id019PtparisaOperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id019PtparisaOperationMetadataAlphaBalanceUpdates;
  })();

  var Id019PtparisaOperationMetadataAlphaTezBalanceUpdate = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate = (function() {
    function Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaOperationMetadataAlphaTezBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id019PtparisaOperationMetadataAlphaTezBalanceUpdate;
  })();

  var StakingDelegateDenominator = Id019PtparisaReceiptBalanceUpdates.StakingDelegateDenominator = (function() {
    function StakingDelegateDenominator(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    StakingDelegateDenominator.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.id019PtparisaOperationMetadataAlphaStakingAbstractQuantity = new Id019PtparisaOperationMetadataAlphaStakingAbstractQuantity(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return StakingDelegateDenominator;
  })();

  var Id019PtparisaOperationMetadataAlphaUpdateOrigin = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaUpdateOrigin = (function() {
    function Id019PtparisaOperationMetadataAlphaUpdateOrigin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaOperationMetadataAlphaUpdateOrigin.prototype._read = function() {
      this.id019PtparisaOperationMetadataAlphaUpdateOriginTag = this._io.readU1();
      if (this.id019PtparisaOperationMetadataAlphaUpdateOriginTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaUpdateOriginTag.DELAYED_OPERATION) {
        this.delayedOperation = this._io.readBytes(32);
      }
    }

    return Id019PtparisaOperationMetadataAlphaUpdateOrigin;
  })();

  var Single = Id019PtparisaReceiptBalanceUpdates.Single = (function() {
    function Single(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Single.prototype._read = function() {
      this.contract = new Id019PtparisaContractId(this._io, this, this._root);
      this.delegate = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Single;
  })();

  var Deposits = Id019PtparisaReceiptBalanceUpdates.Deposits = (function() {
    function Deposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposits.prototype._read = function() {
      this.staker = new Id019PtparisaFrozenStaker(this._io, this, this._root);
      this.id019PtparisaOperationMetadataAlphaTezBalanceUpdate = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * frozen_staker: Abstract notion of staker used in operation receipts for frozen deposits, either a single staker or all the stakers delegating to some delegate.
     */

    return Deposits;
  })();

  var Id019PtparisaFrozenStaker = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaFrozenStaker = (function() {
    function Id019PtparisaFrozenStaker(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaFrozenStaker.prototype._read = function() {
      this.id019PtparisaFrozenStakerTag = this._io.readU1();
      if (this.id019PtparisaFrozenStakerTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaFrozenStakerTag.SINGLE) {
        this.single = new Single(this._io, this, this._root);
      }
      if (this.id019PtparisaFrozenStakerTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaFrozenStakerTag.SHARED) {
        this.shared = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id019PtparisaFrozenStakerTag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaFrozenStakerTag.BAKER) {
        this.baker = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id019PtparisaFrozenStaker;
  })();

  var Id019PtparisaOperationMetadataAlphaBalanceUpdates0 = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id019PtparisaOperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaOperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId019PtparisaOperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId019PtparisaOperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId019PtparisaOperationMetadataAlphaBalanceUpdates, this._io, "/types/id_019__ptparisa__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id019PtparisaOperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId019PtparisaOperationMetadataAlphaBalanceUpdates);
      var _io__raw_id019PtparisaOperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id019PtparisaOperationMetadataAlphaBalanceUpdates);
      this.id019PtparisaOperationMetadataAlphaBalanceUpdates = new Id019PtparisaOperationMetadataAlphaBalanceUpdates(_io__raw_id019PtparisaOperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id019PtparisaOperationMetadataAlphaBalanceUpdates0;
  })();

  var Id019PtparisaOperationMetadataAlphaBalanceUpdatesEntries = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id019PtparisaOperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaOperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id019PtparisaOperationMetadataAlphaBalanceAndUpdate = new Id019PtparisaOperationMetadataAlphaBalanceAndUpdate(this._io, this, this._root);
      this.id019PtparisaOperationMetadataAlphaUpdateOrigin = new Id019PtparisaOperationMetadataAlphaUpdateOrigin(this._io, this, this._root);
    }

    return Id019PtparisaOperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var LostAttestingRewards = Id019PtparisaReceiptBalanceUpdates.LostAttestingRewards = (function() {
    function LostAttestingRewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LostAttestingRewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.participation = this._io.readU1();
      this.revelation = this._io.readU1();
      this.id019PtparisaOperationMetadataAlphaTezBalanceUpdate = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return LostAttestingRewards;
  })();

  var UnstakedDeposits = Id019PtparisaReceiptBalanceUpdates.UnstakedDeposits = (function() {
    function UnstakedDeposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    UnstakedDeposits.prototype._read = function() {
      this.staker = new Id019PtparisaStaker(this._io, this, this._root);
      this.cycle = this._io.readS4be();
      this.id019PtparisaOperationMetadataAlphaTezBalanceUpdate = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * unstaked_frozen_staker: Abstract notion of staker used in operation receipts for unstaked frozen deposits, either a single staker or all the stakers delegating to some delegate.
     */

    return UnstakedDeposits;
  })();

  var Id019PtparisaOperationMetadataAlphaStakingAbstractQuantity = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaStakingAbstractQuantity = (function() {
    function Id019PtparisaOperationMetadataAlphaStakingAbstractQuantity(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaOperationMetadataAlphaStakingAbstractQuantity.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id019PtparisaOperationMetadataAlphaStakingAbstractQuantity;
  })();

  var PublicKeyHash = Id019PtparisaReceiptBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id019PtparisaReceiptBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaReceiptBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaReceiptBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaReceiptBalanceUpdates.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Contract = Id019PtparisaReceiptBalanceUpdates.Contract = (function() {
    function Contract(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Contract.prototype._read = function() {
      this.contract = new Id019PtparisaContractId(this._io, this, this._root);
      this.id019PtparisaOperationMetadataAlphaTezBalanceUpdate = new Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Contract;
  })();

  var StakingDelegatorNumerator = Id019PtparisaReceiptBalanceUpdates.StakingDelegatorNumerator = (function() {
    function StakingDelegatorNumerator(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    StakingDelegatorNumerator.prototype._read = function() {
      this.delegator = new Id019PtparisaContractId(this._io, this, this._root);
      this.id019PtparisaOperationMetadataAlphaStakingAbstractQuantity = new Id019PtparisaOperationMetadataAlphaStakingAbstractQuantity(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return StakingDelegatorNumerator;
  })();

  return Id019PtparisaReceiptBalanceUpdates;
})();
return Id019PtparisaReceiptBalanceUpdates;
}));
