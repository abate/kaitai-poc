// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './TimestampProtocol'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./TimestampProtocol'));
  } else {
    root.Id005Psbabym1Timestamp = factory(root.KaitaiStream, root.TimestampProtocol);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, TimestampProtocol) {
/**
 * Encoding id: 005-PsBabyM1.timestamp
 */

var Id005Psbabym1Timestamp = (function() {
  function Id005Psbabym1Timestamp(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1Timestamp.prototype._read = function() {
    this.id005Psbabym1Timestamp = new TimestampProtocol(this._io, this, null);
  }

  return Id005Psbabym1Timestamp;
})();
return Id005Psbabym1Timestamp;
}));
