// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id018ProxfordFrozenStaker = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 018-Proxford.frozen_staker
 */

var Id018ProxfordFrozenStaker = (function() {
  Id018ProxfordFrozenStaker.Id018ProxfordContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id018ProxfordFrozenStaker.Id018ProxfordFrozenStakerTag = Object.freeze({
    SINGLE: 0,
    SHARED: 1,
    BAKER: 2,

    0: "SINGLE",
    1: "SHARED",
    2: "BAKER",
  });

  Id018ProxfordFrozenStaker.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function Id018ProxfordFrozenStaker(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id018ProxfordFrozenStaker.prototype._read = function() {
    this.id018ProxfordFrozenStaker = new Id018ProxfordFrozenStaker(this._io, this, this._root);
  }

  var Originated = Id018ProxfordFrozenStaker.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Id018ProxfordContractId = Id018ProxfordFrozenStaker.Id018ProxfordContractId = (function() {
    function Id018ProxfordContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordContractId.prototype._read = function() {
      this.id018ProxfordContractIdTag = this._io.readU1();
      if (this.id018ProxfordContractIdTag == Id018ProxfordFrozenStaker.Id018ProxfordContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id018ProxfordContractIdTag == Id018ProxfordFrozenStaker.Id018ProxfordContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id018ProxfordContractId;
  })();

  var Single = Id018ProxfordFrozenStaker.Single = (function() {
    function Single(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Single.prototype._read = function() {
      this.contract = new Id018ProxfordContractId(this._io, this, this._root);
      this.delegate = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Single;
  })();

  var Id018ProxfordFrozenStaker = Id018ProxfordFrozenStaker.Id018ProxfordFrozenStaker = (function() {
    function Id018ProxfordFrozenStaker(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordFrozenStaker.prototype._read = function() {
      this.id018ProxfordFrozenStakerTag = this._io.readU1();
      if (this.id018ProxfordFrozenStakerTag == Id018ProxfordFrozenStaker.Id018ProxfordFrozenStakerTag.SINGLE) {
        this.single = new Single(this._io, this, this._root);
      }
      if (this.id018ProxfordFrozenStakerTag == Id018ProxfordFrozenStaker.Id018ProxfordFrozenStakerTag.SHARED) {
        this.shared = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id018ProxfordFrozenStakerTag == Id018ProxfordFrozenStaker.Id018ProxfordFrozenStakerTag.BAKER) {
        this.baker = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id018ProxfordFrozenStaker;
  })();

  var PublicKeyHash = Id018ProxfordFrozenStaker.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id018ProxfordFrozenStaker.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id018ProxfordFrozenStaker.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id018ProxfordFrozenStaker.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id018ProxfordFrozenStaker.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  /**
   * frozen_staker: Abstract notion of staker used in operation receipts for frozen deposits, either a single staker or all the stakers delegating to some delegate.
   */

  return Id018ProxfordFrozenStaker;
})();
return Id018ProxfordFrozenStaker;
}));
