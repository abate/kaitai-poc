// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeader'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeader'));
  } else {
    root.Id007Psdelph1BlockHeaderRaw = factory(root.KaitaiStream, root.BlockHeader);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeader) {
/**
 * Encoding id: 007-PsDELPH1.block_header.raw
 */

var Id007Psdelph1BlockHeaderRaw = (function() {
  function Id007Psdelph1BlockHeaderRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1BlockHeaderRaw.prototype._read = function() {
    this.id007Psdelph1BlockHeaderRaw = new BlockHeader(this._io, this, null);
  }

  return Id007Psdelph1BlockHeaderRaw;
})();
return Id007Psdelph1BlockHeaderRaw;
}));
