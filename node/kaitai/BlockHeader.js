// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.BlockHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: block_header
 * Description: Block header. It contains both shell and protocol specific data.
 */

var BlockHeader = (function() {
  function BlockHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  BlockHeader.prototype._read = function() {
    this.blockHeader = new BlockHeaderShell(this._io, this, null);
    this.protocolData = this._io.readBytesFull();
  }

  return BlockHeader;
})();
return BlockHeader;
}));
