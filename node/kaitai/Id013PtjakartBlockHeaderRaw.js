// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeader'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeader'));
  } else {
    root.Id013PtjakartBlockHeaderRaw = factory(root.KaitaiStream, root.BlockHeader);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeader) {
/**
 * Encoding id: 013-PtJakart.block_header.raw
 */

var Id013PtjakartBlockHeaderRaw = (function() {
  function Id013PtjakartBlockHeaderRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id013PtjakartBlockHeaderRaw.prototype._read = function() {
    this.id013PtjakartBlockHeaderRaw = new BlockHeader(this._io, this, null);
  }

  return Id013PtjakartBlockHeaderRaw;
})();
return Id013PtjakartBlockHeaderRaw;
}));
