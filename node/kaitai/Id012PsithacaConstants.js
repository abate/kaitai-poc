// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id012PsithacaConstants = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 012-Psithaca.constants
 */

var Id012PsithacaConstants = (function() {
  Id012PsithacaConstants.DelegateSelectionTag = Object.freeze({
    RANDOM_DELEGATE_SELECTION: 0,
    ROUND_ROBIN_OVER_DELEGATES: 1,

    0: "RANDOM_DELEGATE_SELECTION",
    1: "ROUND_ROBIN_OVER_DELEGATES",
  });

  Id012PsithacaConstants.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id012PsithacaConstants(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id012PsithacaConstants.prototype._read = function() {
    this.proofOfWorkNonceSize = this._io.readU1();
    this.nonceLength = this._io.readU1();
    this.maxAnonOpsPerBlock = this._io.readU1();
    this.maxOperationDataLength = new Int31(this._io, this, this._root);
    this.maxProposalsPerDelegate = this._io.readU1();
    this.maxMichelineNodeCount = new Int31(this._io, this, this._root);
    this.maxMichelineBytesLimit = new Int31(this._io, this, this._root);
    this.maxAllowedGlobalConstantsDepth = new Int31(this._io, this, this._root);
    this.cacheLayout = new CacheLayout0(this._io, this, this._root);
    this.michelsonMaximumTypeSize = this._io.readU2be();
    this.preservedCycles = this._io.readU1();
    this.blocksPerCycle = this._io.readS4be();
    this.blocksPerCommitment = this._io.readS4be();
    this.blocksPerStakeSnapshot = this._io.readS4be();
    this.blocksPerVotingPeriod = this._io.readS4be();
    this.hardGasLimitPerOperation = new Z(this._io, this, this._root);
    this.hardGasLimitPerBlock = new Z(this._io, this, this._root);
    this.proofOfWorkThreshold = this._io.readS8be();
    this.tokensPerRoll = new Id012PsithacaMutez(this._io, this, this._root);
    this.seedNonceRevelationTip = new Id012PsithacaMutez(this._io, this, this._root);
    this.originationSize = new Int31(this._io, this, this._root);
    this.bakingRewardFixedPortion = new Id012PsithacaMutez(this._io, this, this._root);
    this.bakingRewardBonusPerSlot = new Id012PsithacaMutez(this._io, this, this._root);
    this.endorsingRewardPerSlot = new Id012PsithacaMutez(this._io, this, this._root);
    this.costPerByte = new Id012PsithacaMutez(this._io, this, this._root);
    this.hardStorageLimitPerOperation = new Z(this._io, this, this._root);
    this.quorumMin = this._io.readS4be();
    this.quorumMax = this._io.readS4be();
    this.minProposalQuorum = this._io.readS4be();
    this.liquidityBakingSubsidy = new Id012PsithacaMutez(this._io, this, this._root);
    this.liquidityBakingSunsetLevel = this._io.readS4be();
    this.liquidityBakingEscapeEmaThreshold = this._io.readS4be();
    this.maxOperationsTimeToLive = this._io.readS2be();
    this.minimalBlockDelay = this._io.readS8be();
    this.delayIncrementPerRound = this._io.readS8be();
    this.consensusCommitteeSize = new Int31(this._io, this, this._root);
    this.consensusThreshold = new Int31(this._io, this, this._root);
    this.minimalParticipationRatio = new MinimalParticipationRatio(this._io, this, this._root);
    this.maxSlashingPeriod = new Int31(this._io, this, this._root);
    this.frozenDepositsPercentage = new Int31(this._io, this, this._root);
    this.doubleBakingPunishment = new Id012PsithacaMutez(this._io, this, this._root);
    this.ratioOfFrozenDepositsSlashedPerDoubleEndorsement = new RatioOfFrozenDepositsSlashedPerDoubleEndorsement(this._io, this, this._root);
    this.delegateSelection = new DelegateSelection(this._io, this, this._root);
  }

  var RatioOfFrozenDepositsSlashedPerDoubleEndorsement = Id012PsithacaConstants.RatioOfFrozenDepositsSlashedPerDoubleEndorsement = (function() {
    function RatioOfFrozenDepositsSlashedPerDoubleEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RatioOfFrozenDepositsSlashedPerDoubleEndorsement.prototype._read = function() {
      this.numerator = this._io.readU2be();
      this.denominator = this._io.readU2be();
    }

    return RatioOfFrozenDepositsSlashedPerDoubleEndorsement;
  })();

  var N = Id012PsithacaConstants.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var RoundRobinOverDelegatesEntries = Id012PsithacaConstants.RoundRobinOverDelegatesEntries = (function() {
    function RoundRobinOverDelegatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RoundRobinOverDelegatesEntries.prototype._read = function() {
      this.lenRoundRobinOverDelegatesElt = this._io.readU4be();
      if (!(this.lenRoundRobinOverDelegatesElt <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenRoundRobinOverDelegatesElt, this._io, "/types/round_robin_over_delegates_entries/seq/0");
      }
      this._raw_roundRobinOverDelegatesElt = this._io.readBytes(this.lenRoundRobinOverDelegatesElt);
      var _io__raw_roundRobinOverDelegatesElt = new KaitaiStream(this._raw_roundRobinOverDelegatesElt);
      this.roundRobinOverDelegatesElt = new RoundRobinOverDelegatesElt(_io__raw_roundRobinOverDelegatesElt, this, this._root);
    }

    return RoundRobinOverDelegatesEntries;
  })();

  var RoundRobinOverDelegates = Id012PsithacaConstants.RoundRobinOverDelegates = (function() {
    function RoundRobinOverDelegates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RoundRobinOverDelegates.prototype._read = function() {
      this.roundRobinOverDelegatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.roundRobinOverDelegatesEntries.push(new RoundRobinOverDelegatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return RoundRobinOverDelegates;
  })();

  var CacheLayoutEntries = Id012PsithacaConstants.CacheLayoutEntries = (function() {
    function CacheLayoutEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CacheLayoutEntries.prototype._read = function() {
      this.cacheLayoutElt = this._io.readS8be();
    }

    return CacheLayoutEntries;
  })();

  var RoundRobinOverDelegatesElt = Id012PsithacaConstants.RoundRobinOverDelegatesElt = (function() {
    function RoundRobinOverDelegatesElt(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RoundRobinOverDelegatesElt.prototype._read = function() {
      this.roundRobinOverDelegatesEltEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.roundRobinOverDelegatesEltEntries.push(new RoundRobinOverDelegatesEltEntries(this._io, this, this._root));
        i++;
      }
    }

    return RoundRobinOverDelegatesElt;
  })();

  var CacheLayout0 = Id012PsithacaConstants.CacheLayout0 = (function() {
    function CacheLayout0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CacheLayout0.prototype._read = function() {
      this.lenCacheLayout = this._io.readU4be();
      if (!(this.lenCacheLayout <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCacheLayout, this._io, "/types/cache_layout_0/seq/0");
      }
      this._raw_cacheLayout = this._io.readBytes(this.lenCacheLayout);
      var _io__raw_cacheLayout = new KaitaiStream(this._raw_cacheLayout);
      this.cacheLayout = new CacheLayout(_io__raw_cacheLayout, this, this._root);
    }

    return CacheLayout0;
  })();

  var Id012PsithacaMutez = Id012PsithacaConstants.Id012PsithacaMutez = (function() {
    function Id012PsithacaMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaMutez.prototype._read = function() {
      this.id012PsithacaMutez = new N(this._io, this, this._root);
    }

    return Id012PsithacaMutez;
  })();

  var RoundRobinOverDelegates0 = Id012PsithacaConstants.RoundRobinOverDelegates0 = (function() {
    function RoundRobinOverDelegates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RoundRobinOverDelegates0.prototype._read = function() {
      this.lenRoundRobinOverDelegates = this._io.readU4be();
      if (!(this.lenRoundRobinOverDelegates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenRoundRobinOverDelegates, this._io, "/types/round_robin_over_delegates_0/seq/0");
      }
      this._raw_roundRobinOverDelegates = this._io.readBytes(this.lenRoundRobinOverDelegates);
      var _io__raw_roundRobinOverDelegates = new KaitaiStream(this._raw_roundRobinOverDelegates);
      this.roundRobinOverDelegates = new RoundRobinOverDelegates(_io__raw_roundRobinOverDelegates, this, this._root);
    }

    return RoundRobinOverDelegates0;
  })();

  var CacheLayout = Id012PsithacaConstants.CacheLayout = (function() {
    function CacheLayout(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CacheLayout.prototype._read = function() {
      this.cacheLayoutEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.cacheLayoutEntries.push(new CacheLayoutEntries(this._io, this, this._root));
        i++;
      }
    }

    return CacheLayout;
  })();

  var MinimalParticipationRatio = Id012PsithacaConstants.MinimalParticipationRatio = (function() {
    function MinimalParticipationRatio(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MinimalParticipationRatio.prototype._read = function() {
      this.numerator = this._io.readU2be();
      this.denominator = this._io.readU2be();
    }

    return MinimalParticipationRatio;
  })();

  var PublicKey = Id012PsithacaConstants.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id012PsithacaConstants.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id012PsithacaConstants.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id012PsithacaConstants.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var DelegateSelection = Id012PsithacaConstants.DelegateSelection = (function() {
    function DelegateSelection(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DelegateSelection.prototype._read = function() {
      this.delegateSelectionTag = this._io.readU1();
      if (this.delegateSelectionTag == Id012PsithacaConstants.DelegateSelectionTag.ROUND_ROBIN_OVER_DELEGATES) {
        this.roundRobinOverDelegates = new RoundRobinOverDelegates0(this._io, this, this._root);
      }
    }

    return DelegateSelection;
  })();

  var RoundRobinOverDelegatesEltEntries = Id012PsithacaConstants.RoundRobinOverDelegatesEltEntries = (function() {
    function RoundRobinOverDelegatesEltEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RoundRobinOverDelegatesEltEntries.prototype._read = function() {
      this.signatureV0PublicKey = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return RoundRobinOverDelegatesEltEntries;
  })();

  var Int31 = Id012PsithacaConstants.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id012PsithacaConstants.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Z = Id012PsithacaConstants.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  return Id012PsithacaConstants;
})();
return Id012PsithacaConstants;
}));
