// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './TimestampProtocol'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./TimestampProtocol'));
  } else {
    root.Id010PtgranadTimestamp = factory(root.KaitaiStream, root.TimestampProtocol);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, TimestampProtocol) {
/**
 * Encoding id: 010-PtGRANAD.timestamp
 */

var Id010PtgranadTimestamp = (function() {
  function Id010PtgranadTimestamp(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id010PtgranadTimestamp.prototype._read = function() {
    this.id010PtgranadTimestamp = new TimestampProtocol(this._io, this, null);
  }

  return Id010PtgranadTimestamp;
})();
return Id010PtgranadTimestamp;
}));
