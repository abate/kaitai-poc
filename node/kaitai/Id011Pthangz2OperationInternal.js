// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id011Pthangz2OperationInternal = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 011-PtHangz2.operation.internal
 */

var Id011Pthangz2OperationInternal = (function() {
  Id011Pthangz2OperationInternal.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id011Pthangz2OperationInternal.Id011Pthangz2ContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id011Pthangz2OperationInternal.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id011Pthangz2OperationInternal.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperationTag = Object.freeze({
    REVEAL: 0,
    TRANSACTION: 1,
    ORIGINATION: 2,
    DELEGATION: 3,
    REGISTER_GLOBAL_CONSTANT: 4,

    0: "REVEAL",
    1: "TRANSACTION",
    2: "ORIGINATION",
    3: "DELEGATION",
    4: "REGISTER_GLOBAL_CONSTANT",
  });

  Id011Pthangz2OperationInternal.Id011Pthangz2EntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    255: "NAMED",
  });

  function Id011Pthangz2OperationInternal(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2OperationInternal.prototype._read = function() {
    this.id011Pthangz2OperationAlphaInternalOperation = new Id011Pthangz2OperationAlphaInternalOperation(this._io, this, this._root);
  }

  var Id011Pthangz2OperationAlphaInternalOperation = Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperation = (function() {
    function Id011Pthangz2OperationAlphaInternalOperation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2OperationAlphaInternalOperation.prototype._read = function() {
      this.source = new Id011Pthangz2ContractId(this._io, this, this._root);
      this.nonce = this._io.readU2be();
      this.id011Pthangz2OperationAlphaInternalOperationTag = this._io.readU1();
      if (this.id011Pthangz2OperationAlphaInternalOperationTag == Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperationTag.REVEAL) {
        this.reveal = new PublicKey(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaInternalOperationTag == Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperationTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaInternalOperationTag == Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperationTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaInternalOperationTag == Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperationTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaInternalOperationTag == Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperationTag.REGISTER_GLOBAL_CONSTANT) {
        this.registerGlobalConstant = new BytesDynUint30(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Id011Pthangz2OperationAlphaInternalOperation;
  })();

  var Originated = Id011Pthangz2OperationInternal.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var N = Id011Pthangz2OperationInternal.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id011Pthangz2ContractId = Id011Pthangz2OperationInternal.Id011Pthangz2ContractId = (function() {
    function Id011Pthangz2ContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2ContractId.prototype._read = function() {
      this.id011Pthangz2ContractIdTag = this._io.readU1();
      if (this.id011Pthangz2ContractIdTag == Id011Pthangz2OperationInternal.Id011Pthangz2ContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id011Pthangz2ContractIdTag == Id011Pthangz2OperationInternal.Id011Pthangz2ContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id011Pthangz2ContractId;
  })();

  var Id011Pthangz2Mutez = Id011Pthangz2OperationInternal.Id011Pthangz2Mutez = (function() {
    function Id011Pthangz2Mutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2Mutez.prototype._read = function() {
      this.id011Pthangz2Mutez = new N(this._io, this, this._root);
    }

    return Id011Pthangz2Mutez;
  })();

  var Id011Pthangz2Entrypoint = Id011Pthangz2OperationInternal.Id011Pthangz2Entrypoint = (function() {
    function Id011Pthangz2Entrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2Entrypoint.prototype._read = function() {
      this.id011Pthangz2EntrypointTag = this._io.readU1();
      if (this.id011Pthangz2EntrypointTag == Id011Pthangz2OperationInternal.Id011Pthangz2EntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id011Pthangz2Entrypoint;
  })();

  var PublicKey = Id011Pthangz2OperationInternal.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id011Pthangz2OperationInternal.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id011Pthangz2OperationInternal.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id011Pthangz2OperationInternal.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var Named = Id011Pthangz2OperationInternal.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Delegation = Id011Pthangz2OperationInternal.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id011Pthangz2OperationInternal.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var BytesDynUint30 = Id011Pthangz2OperationInternal.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Id011Pthangz2ScriptedContracts = Id011Pthangz2OperationInternal.Id011Pthangz2ScriptedContracts = (function() {
    function Id011Pthangz2ScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2ScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id011Pthangz2ScriptedContracts;
  })();

  var Origination = Id011Pthangz2OperationInternal.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.balance = new Id011Pthangz2Mutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id011Pthangz2OperationInternal.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id011Pthangz2ScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var NChunk = Id011Pthangz2OperationInternal.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Named0 = Id011Pthangz2OperationInternal.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id011Pthangz2OperationInternal.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.amount = new Id011Pthangz2Mutez(this._io, this, this._root);
      this.destination = new Id011Pthangz2ContractId(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id011Pthangz2OperationInternal.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Transaction;
  })();

  var Parameters = Id011Pthangz2OperationInternal.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id011Pthangz2Entrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var PublicKeyHash = Id011Pthangz2OperationInternal.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id011Pthangz2OperationInternal.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id011Pthangz2OperationInternal.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id011Pthangz2OperationInternal.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id011Pthangz2OperationInternal;
})();
return Id011Pthangz2OperationInternal;
}));
