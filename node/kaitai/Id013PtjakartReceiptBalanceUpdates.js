// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id013PtjakartReceiptBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 013-PtJakart.receipt.balance_updates
 */

var Id013PtjakartReceiptBalanceUpdates = (function() {
  Id013PtjakartReceiptBalanceUpdates.Id013PtjakartBondIdTag = Object.freeze({
    TX_ROLLUP_BOND_ID: 0,

    0: "TX_ROLLUP_BOND_ID",
  });

  Id013PtjakartReceiptBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id013PtjakartReceiptBalanceUpdates.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id013PtjakartReceiptBalanceUpdates.OriginTag = Object.freeze({
    BLOCK_APPLICATION: 0,
    PROTOCOL_MIGRATION: 1,
    SUBSIDY: 2,
    SIMULATION: 3,

    0: "BLOCK_APPLICATION",
    1: "PROTOCOL_MIGRATION",
    2: "SUBSIDY",
    3: "SIMULATION",
  });

  Id013PtjakartReceiptBalanceUpdates.Id013PtjakartContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceTag = Object.freeze({
    CONTRACT: 0,
    BLOCK_FEES: 2,
    DEPOSITS: 4,
    NONCE_REVELATION_REWARDS: 5,
    DOUBLE_SIGNING_EVIDENCE_REWARDS: 6,
    ENDORSING_REWARDS: 7,
    BAKING_REWARDS: 8,
    BAKING_BONUSES: 9,
    STORAGE_FEES: 11,
    DOUBLE_SIGNING_PUNISHMENTS: 12,
    LOST_ENDORSING_REWARDS: 13,
    LIQUIDITY_BAKING_SUBSIDIES: 14,
    BURNED: 15,
    COMMITMENTS: 16,
    BOOTSTRAP: 17,
    INVOICE: 18,
    INITIAL_COMMITMENTS: 19,
    MINTED: 20,
    FROZEN_BONDS: 21,
    TX_ROLLUP_REJECTION_REWARDS: 22,
    TX_ROLLUP_REJECTION_PUNISHMENTS: 23,

    0: "CONTRACT",
    2: "BLOCK_FEES",
    4: "DEPOSITS",
    5: "NONCE_REVELATION_REWARDS",
    6: "DOUBLE_SIGNING_EVIDENCE_REWARDS",
    7: "ENDORSING_REWARDS",
    8: "BAKING_REWARDS",
    9: "BAKING_BONUSES",
    11: "STORAGE_FEES",
    12: "DOUBLE_SIGNING_PUNISHMENTS",
    13: "LOST_ENDORSING_REWARDS",
    14: "LIQUIDITY_BAKING_SUBSIDIES",
    15: "BURNED",
    16: "COMMITMENTS",
    17: "BOOTSTRAP",
    18: "INVOICE",
    19: "INITIAL_COMMITMENTS",
    20: "MINTED",
    21: "FROZEN_BONDS",
    22: "TX_ROLLUP_REJECTION_REWARDS",
    23: "TX_ROLLUP_REJECTION_PUNISHMENTS",
  });

  function Id013PtjakartReceiptBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id013PtjakartReceiptBalanceUpdates.prototype._read = function() {
    this.id013PtjakartOperationMetadataAlphaBalanceUpdates = new Id013PtjakartOperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Id013PtjakartOperationMetadataAlphaBalance = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalance = (function() {
    function Id013PtjakartOperationMetadataAlphaBalance(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartOperationMetadataAlphaBalance.prototype._read = function() {
      this.id013PtjakartOperationMetadataAlphaBalanceTag = this._io.readU1();
      if (this.id013PtjakartOperationMetadataAlphaBalanceTag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceTag.CONTRACT) {
        this.contract = new Id013PtjakartContractId(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationMetadataAlphaBalanceTag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceTag.DEPOSITS) {
        this.deposits = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationMetadataAlphaBalanceTag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceTag.LOST_ENDORSING_REWARDS) {
        this.lostEndorsingRewards = new LostEndorsingRewards(this._io, this, this._root);
      }
      if (this.id013PtjakartOperationMetadataAlphaBalanceTag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceTag.COMMITMENTS) {
        this.commitments = this._io.readBytes(20);
      }
      if (this.id013PtjakartOperationMetadataAlphaBalanceTag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceTag.FROZEN_BONDS) {
        this.frozenBonds = new FrozenBonds(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id013PtjakartOperationMetadataAlphaBalance;
  })();

  var FrozenBonds = Id013PtjakartReceiptBalanceUpdates.FrozenBonds = (function() {
    function FrozenBonds(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    FrozenBonds.prototype._read = function() {
      this.contract = new Id013PtjakartContractId(this._io, this, this._root);
      this.bondId = new Id013PtjakartBondId(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return FrozenBonds;
  })();

  var Originated = Id013PtjakartReceiptBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Id013PtjakartTxRollupId = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartTxRollupId = (function() {
    function Id013PtjakartTxRollupId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartTxRollupId.prototype._read = function() {
      this.rollupHash = this._io.readBytes(20);
    }

    return Id013PtjakartTxRollupId;
  })();

  var LostEndorsingRewards = Id013PtjakartReceiptBalanceUpdates.LostEndorsingRewards = (function() {
    function LostEndorsingRewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LostEndorsingRewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.participation = this._io.readU1();
      this.revelation = this._io.readU1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return LostEndorsingRewards;
  })();

  var Id013PtjakartOperationMetadataAlphaBalanceUpdatesEntries = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id013PtjakartOperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartOperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id013PtjakartOperationMetadataAlphaBalance = new Id013PtjakartOperationMetadataAlphaBalance(this._io, this, this._root);
      this.id013PtjakartOperationMetadataAlphaBalanceUpdate = new Id013PtjakartOperationMetadataAlphaBalanceUpdate(this._io, this, this._root);
      this.id013PtjakartOperationMetadataAlphaUpdateOrigin = new Id013PtjakartOperationMetadataAlphaUpdateOrigin(this._io, this, this._root);
    }

    return Id013PtjakartOperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var Id013PtjakartContractId = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartContractId = (function() {
    function Id013PtjakartContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartContractId.prototype._read = function() {
      this.id013PtjakartContractIdTag = this._io.readU1();
      if (this.id013PtjakartContractIdTag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id013PtjakartContractIdTag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id013PtjakartContractId;
  })();

  var Id013PtjakartOperationMetadataAlphaBalanceUpdates = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceUpdates = (function() {
    function Id013PtjakartOperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartOperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id013PtjakartOperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id013PtjakartOperationMetadataAlphaBalanceUpdatesEntries.push(new Id013PtjakartOperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id013PtjakartOperationMetadataAlphaBalanceUpdates;
  })();

  var Id013PtjakartOperationMetadataAlphaBalanceUpdates0 = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id013PtjakartOperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartOperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId013PtjakartOperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId013PtjakartOperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId013PtjakartOperationMetadataAlphaBalanceUpdates, this._io, "/types/id_013__ptjakart__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id013PtjakartOperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId013PtjakartOperationMetadataAlphaBalanceUpdates);
      var _io__raw_id013PtjakartOperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id013PtjakartOperationMetadataAlphaBalanceUpdates);
      this.id013PtjakartOperationMetadataAlphaBalanceUpdates = new Id013PtjakartOperationMetadataAlphaBalanceUpdates(_io__raw_id013PtjakartOperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id013PtjakartOperationMetadataAlphaBalanceUpdates0;
  })();

  var Id013PtjakartOperationMetadataAlphaUpdateOrigin = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaUpdateOrigin = (function() {
    function Id013PtjakartOperationMetadataAlphaUpdateOrigin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartOperationMetadataAlphaUpdateOrigin.prototype._read = function() {
      this.origin = this._io.readU1();
    }

    return Id013PtjakartOperationMetadataAlphaUpdateOrigin;
  })();

  var Id013PtjakartOperationMetadataAlphaBalanceUpdate = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceUpdate = (function() {
    function Id013PtjakartOperationMetadataAlphaBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartOperationMetadataAlphaBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id013PtjakartOperationMetadataAlphaBalanceUpdate;
  })();

  var Id013PtjakartBondId = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartBondId = (function() {
    function Id013PtjakartBondId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartBondId.prototype._read = function() {
      this.id013PtjakartBondIdTag = this._io.readU1();
      if (this.id013PtjakartBondIdTag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartBondIdTag.TX_ROLLUP_BOND_ID) {
        this.txRollupBondId = new Id013PtjakartTxRollupId(this._io, this, this._root);
      }
    }

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    return Id013PtjakartBondId;
  })();

  var PublicKeyHash = Id013PtjakartReceiptBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id013PtjakartReceiptBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id013PtjakartReceiptBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id013PtjakartReceiptBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id013PtjakartReceiptBalanceUpdates;
})();
return Id013PtjakartReceiptBalanceUpdates;
}));
