// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.UserActivatedUpgrades = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: user_activated.upgrades
 * Description: User activated upgrades: at given level, switch to given protocol.
 */

var UserActivatedUpgrades = (function() {
  function UserActivatedUpgrades(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  UserActivatedUpgrades.prototype._read = function() {
    this.lenUserActivatedUpgrades = this._io.readU4be();
    if (!(this.lenUserActivatedUpgrades <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenUserActivatedUpgrades, this._io, "/seq/0");
    }
    this._raw_userActivatedUpgrades = this._io.readBytes(this.lenUserActivatedUpgrades);
    var _io__raw_userActivatedUpgrades = new KaitaiStream(this._raw_userActivatedUpgrades);
    this.userActivatedUpgrades = new UserActivatedUpgrades(_io__raw_userActivatedUpgrades, this, this._root);
  }

  var UserActivatedUpgrades = UserActivatedUpgrades.UserActivatedUpgrades = (function() {
    function UserActivatedUpgrades(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    UserActivatedUpgrades.prototype._read = function() {
      this.userActivatedUpgradesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.userActivatedUpgradesEntries.push(new UserActivatedUpgradesEntries(this._io, this, this._root));
        i++;
      }
    }

    return UserActivatedUpgrades;
  })();

  var UserActivatedUpgradesEntries = UserActivatedUpgrades.UserActivatedUpgradesEntries = (function() {
    function UserActivatedUpgradesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    UserActivatedUpgradesEntries.prototype._read = function() {
      this.level = this._io.readS4be();
      this.replacementProtocol = this._io.readBytes(32);
    }

    return UserActivatedUpgradesEntries;
  })();

  return UserActivatedUpgrades;
})();
return UserActivatedUpgrades;
}));
