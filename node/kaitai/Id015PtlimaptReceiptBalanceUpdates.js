// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id015PtlimaptReceiptBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 015-PtLimaPt.receipt.balance_updates
 */

var Id015PtlimaptReceiptBalanceUpdates = (function() {
  Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceTag = Object.freeze({
    CONTRACT: 0,
    BLOCK_FEES: 2,
    DEPOSITS: 4,
    NONCE_REVELATION_REWARDS: 5,
    DOUBLE_SIGNING_EVIDENCE_REWARDS: 6,
    ENDORSING_REWARDS: 7,
    BAKING_REWARDS: 8,
    BAKING_BONUSES: 9,
    STORAGE_FEES: 11,
    DOUBLE_SIGNING_PUNISHMENTS: 12,
    LOST_ENDORSING_REWARDS: 13,
    LIQUIDITY_BAKING_SUBSIDIES: 14,
    BURNED: 15,
    COMMITMENTS: 16,
    BOOTSTRAP: 17,
    INVOICE: 18,
    INITIAL_COMMITMENTS: 19,
    MINTED: 20,
    FROZEN_BONDS: 21,
    TX_ROLLUP_REJECTION_REWARDS: 22,
    TX_ROLLUP_REJECTION_PUNISHMENTS: 23,
    SC_ROLLUP_REFUTATION_PUNISHMENTS: 24,
    SC_ROLLUP_REFUTATION_REWARDS: 25,

    0: "CONTRACT",
    2: "BLOCK_FEES",
    4: "DEPOSITS",
    5: "NONCE_REVELATION_REWARDS",
    6: "DOUBLE_SIGNING_EVIDENCE_REWARDS",
    7: "ENDORSING_REWARDS",
    8: "BAKING_REWARDS",
    9: "BAKING_BONUSES",
    11: "STORAGE_FEES",
    12: "DOUBLE_SIGNING_PUNISHMENTS",
    13: "LOST_ENDORSING_REWARDS",
    14: "LIQUIDITY_BAKING_SUBSIDIES",
    15: "BURNED",
    16: "COMMITMENTS",
    17: "BOOTSTRAP",
    18: "INVOICE",
    19: "INITIAL_COMMITMENTS",
    20: "MINTED",
    21: "FROZEN_BONDS",
    22: "TX_ROLLUP_REJECTION_REWARDS",
    23: "TX_ROLLUP_REJECTION_PUNISHMENTS",
    24: "SC_ROLLUP_REFUTATION_PUNISHMENTS",
    25: "SC_ROLLUP_REFUTATION_REWARDS",
  });

  Id015PtlimaptReceiptBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id015PtlimaptReceiptBalanceUpdates.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id015PtlimaptReceiptBalanceUpdates.OriginTag = Object.freeze({
    BLOCK_APPLICATION: 0,
    PROTOCOL_MIGRATION: 1,
    SUBSIDY: 2,
    SIMULATION: 3,

    0: "BLOCK_APPLICATION",
    1: "PROTOCOL_MIGRATION",
    2: "SUBSIDY",
    3: "SIMULATION",
  });

  Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptBondIdTag = Object.freeze({
    TX_ROLLUP_BOND_ID: 0,
    SC_ROLLUP_BOND_ID: 1,

    0: "TX_ROLLUP_BOND_ID",
    1: "SC_ROLLUP_BOND_ID",
  });

  function Id015PtlimaptReceiptBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id015PtlimaptReceiptBalanceUpdates.prototype._read = function() {
    this.id015PtlimaptOperationMetadataAlphaBalanceUpdates = new Id015PtlimaptOperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Id015PtlimaptOperationMetadataAlphaBalanceUpdatesEntries = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id015PtlimaptOperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptOperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id015PtlimaptOperationMetadataAlphaBalance = new Id015PtlimaptOperationMetadataAlphaBalance(this._io, this, this._root);
      this.id015PtlimaptOperationMetadataAlphaBalanceUpdate = new Id015PtlimaptOperationMetadataAlphaBalanceUpdate(this._io, this, this._root);
      this.id015PtlimaptOperationMetadataAlphaUpdateOrigin = new Id015PtlimaptOperationMetadataAlphaUpdateOrigin(this._io, this, this._root);
    }

    return Id015PtlimaptOperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var Id015PtlimaptTxRollupId = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptTxRollupId = (function() {
    function Id015PtlimaptTxRollupId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptTxRollupId.prototype._read = function() {
      this.rollupHash = this._io.readBytes(20);
    }

    return Id015PtlimaptTxRollupId;
  })();

  var Id015PtlimaptRollupAddress = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptRollupAddress = (function() {
    function Id015PtlimaptRollupAddress(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptRollupAddress.prototype._read = function() {
      this.id015PtlimaptRollupAddress = new BytesDynUint30(this._io, this, this._root);
    }

    return Id015PtlimaptRollupAddress;
  })();

  var FrozenBonds = Id015PtlimaptReceiptBalanceUpdates.FrozenBonds = (function() {
    function FrozenBonds(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    FrozenBonds.prototype._read = function() {
      this.contract = new Id015PtlimaptContractId(this._io, this, this._root);
      this.bondId = new Id015PtlimaptBondId(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return FrozenBonds;
  })();

  var Originated = Id015PtlimaptReceiptBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var LostEndorsingRewards = Id015PtlimaptReceiptBalanceUpdates.LostEndorsingRewards = (function() {
    function LostEndorsingRewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LostEndorsingRewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.participation = this._io.readU1();
      this.revelation = this._io.readU1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return LostEndorsingRewards;
  })();

  var Id015PtlimaptOperationMetadataAlphaBalance = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalance = (function() {
    function Id015PtlimaptOperationMetadataAlphaBalance(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptOperationMetadataAlphaBalance.prototype._read = function() {
      this.id015PtlimaptOperationMetadataAlphaBalanceTag = this._io.readU1();
      if (this.id015PtlimaptOperationMetadataAlphaBalanceTag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceTag.CONTRACT) {
        this.contract = new Id015PtlimaptContractId(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationMetadataAlphaBalanceTag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceTag.DEPOSITS) {
        this.deposits = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationMetadataAlphaBalanceTag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceTag.LOST_ENDORSING_REWARDS) {
        this.lostEndorsingRewards = new LostEndorsingRewards(this._io, this, this._root);
      }
      if (this.id015PtlimaptOperationMetadataAlphaBalanceTag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceTag.COMMITMENTS) {
        this.commitments = this._io.readBytes(20);
      }
      if (this.id015PtlimaptOperationMetadataAlphaBalanceTag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceTag.FROZEN_BONDS) {
        this.frozenBonds = new FrozenBonds(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id015PtlimaptOperationMetadataAlphaBalance;
  })();

  var Id015PtlimaptOperationMetadataAlphaBalanceUpdates0 = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id015PtlimaptOperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptOperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId015PtlimaptOperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId015PtlimaptOperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId015PtlimaptOperationMetadataAlphaBalanceUpdates, this._io, "/types/id_015__ptlimapt__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id015PtlimaptOperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId015PtlimaptOperationMetadataAlphaBalanceUpdates);
      var _io__raw_id015PtlimaptOperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id015PtlimaptOperationMetadataAlphaBalanceUpdates);
      this.id015PtlimaptOperationMetadataAlphaBalanceUpdates = new Id015PtlimaptOperationMetadataAlphaBalanceUpdates(_io__raw_id015PtlimaptOperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id015PtlimaptOperationMetadataAlphaBalanceUpdates0;
  })();

  var Id015PtlimaptOperationMetadataAlphaUpdateOrigin = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaUpdateOrigin = (function() {
    function Id015PtlimaptOperationMetadataAlphaUpdateOrigin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptOperationMetadataAlphaUpdateOrigin.prototype._read = function() {
      this.origin = this._io.readU1();
    }

    return Id015PtlimaptOperationMetadataAlphaUpdateOrigin;
  })();

  var Id015PtlimaptOperationMetadataAlphaBalanceUpdates = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceUpdates = (function() {
    function Id015PtlimaptOperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptOperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id015PtlimaptOperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id015PtlimaptOperationMetadataAlphaBalanceUpdatesEntries.push(new Id015PtlimaptOperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id015PtlimaptOperationMetadataAlphaBalanceUpdates;
  })();

  var Id015PtlimaptContractId = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptContractId = (function() {
    function Id015PtlimaptContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptContractId.prototype._read = function() {
      this.id015PtlimaptContractIdTag = this._io.readU1();
      if (this.id015PtlimaptContractIdTag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id015PtlimaptContractIdTag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id015PtlimaptContractId;
  })();

  var BytesDynUint30 = Id015PtlimaptReceiptBalanceUpdates.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Id015PtlimaptOperationMetadataAlphaBalanceUpdate = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceUpdate = (function() {
    function Id015PtlimaptOperationMetadataAlphaBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptOperationMetadataAlphaBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id015PtlimaptOperationMetadataAlphaBalanceUpdate;
  })();

  var Id015PtlimaptBondId = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptBondId = (function() {
    function Id015PtlimaptBondId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptBondId.prototype._read = function() {
      this.id015PtlimaptBondIdTag = this._io.readU1();
      if (this.id015PtlimaptBondIdTag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptBondIdTag.TX_ROLLUP_BOND_ID) {
        this.txRollupBondId = new Id015PtlimaptTxRollupId(this._io, this, this._root);
      }
      if (this.id015PtlimaptBondIdTag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptBondIdTag.SC_ROLLUP_BOND_ID) {
        this.scRollupBondId = new Id015PtlimaptRollupAddress(this._io, this, this._root);
      }
    }

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return Id015PtlimaptBondId;
  })();

  var PublicKeyHash = Id015PtlimaptReceiptBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id015PtlimaptReceiptBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id015PtlimaptReceiptBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id015PtlimaptReceiptBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id015PtlimaptReceiptBalanceUpdates;
})();
return Id015PtlimaptReceiptBalanceUpdates;
}));
