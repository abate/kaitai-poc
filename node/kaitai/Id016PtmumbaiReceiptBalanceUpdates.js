// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id016PtmumbaiReceiptBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 016-PtMumbai.receipt.balance_updates
 */

var Id016PtmumbaiReceiptBalanceUpdates = (function() {
  Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiBondIdTag = Object.freeze({
    TX_ROLLUP_BOND_ID: 0,
    SMART_ROLLUP_BOND_ID: 1,

    0: "TX_ROLLUP_BOND_ID",
    1: "SMART_ROLLUP_BOND_ID",
  });

  Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceTag = Object.freeze({
    CONTRACT: 0,
    BLOCK_FEES: 2,
    DEPOSITS: 4,
    NONCE_REVELATION_REWARDS: 5,
    DOUBLE_SIGNING_EVIDENCE_REWARDS: 6,
    ENDORSING_REWARDS: 7,
    BAKING_REWARDS: 8,
    BAKING_BONUSES: 9,
    STORAGE_FEES: 11,
    DOUBLE_SIGNING_PUNISHMENTS: 12,
    LOST_ENDORSING_REWARDS: 13,
    LIQUIDITY_BAKING_SUBSIDIES: 14,
    BURNED: 15,
    COMMITMENTS: 16,
    BOOTSTRAP: 17,
    INVOICE: 18,
    INITIAL_COMMITMENTS: 19,
    MINTED: 20,
    FROZEN_BONDS: 21,
    TX_ROLLUP_REJECTION_REWARDS: 22,
    TX_ROLLUP_REJECTION_PUNISHMENTS: 23,
    SMART_ROLLUP_REFUTATION_PUNISHMENTS: 24,
    SMART_ROLLUP_REFUTATION_REWARDS: 25,

    0: "CONTRACT",
    2: "BLOCK_FEES",
    4: "DEPOSITS",
    5: "NONCE_REVELATION_REWARDS",
    6: "DOUBLE_SIGNING_EVIDENCE_REWARDS",
    7: "ENDORSING_REWARDS",
    8: "BAKING_REWARDS",
    9: "BAKING_BONUSES",
    11: "STORAGE_FEES",
    12: "DOUBLE_SIGNING_PUNISHMENTS",
    13: "LOST_ENDORSING_REWARDS",
    14: "LIQUIDITY_BAKING_SUBSIDIES",
    15: "BURNED",
    16: "COMMITMENTS",
    17: "BOOTSTRAP",
    18: "INVOICE",
    19: "INITIAL_COMMITMENTS",
    20: "MINTED",
    21: "FROZEN_BONDS",
    22: "TX_ROLLUP_REJECTION_REWARDS",
    23: "TX_ROLLUP_REJECTION_PUNISHMENTS",
    24: "SMART_ROLLUP_REFUTATION_PUNISHMENTS",
    25: "SMART_ROLLUP_REFUTATION_REWARDS",
  });

  Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  Id016PtmumbaiReceiptBalanceUpdates.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id016PtmumbaiReceiptBalanceUpdates.OriginTag = Object.freeze({
    BLOCK_APPLICATION: 0,
    PROTOCOL_MIGRATION: 1,
    SUBSIDY: 2,
    SIMULATION: 3,

    0: "BLOCK_APPLICATION",
    1: "PROTOCOL_MIGRATION",
    2: "SUBSIDY",
    3: "SIMULATION",
  });

  Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  function Id016PtmumbaiReceiptBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiReceiptBalanceUpdates.prototype._read = function() {
    this.id016PtmumbaiOperationMetadataAlphaBalanceUpdates = new Id016PtmumbaiOperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Id016PtmumbaiSmartRollupAddress = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiSmartRollupAddress = (function() {
    function Id016PtmumbaiSmartRollupAddress(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiSmartRollupAddress.prototype._read = function() {
      this.smartRollupHash = this._io.readBytes(20);
    }

    return Id016PtmumbaiSmartRollupAddress;
  })();

  var FrozenBonds = Id016PtmumbaiReceiptBalanceUpdates.FrozenBonds = (function() {
    function FrozenBonds(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    FrozenBonds.prototype._read = function() {
      this.contract = new Id016PtmumbaiContractId(this._io, this, this._root);
      this.bondId = new Id016PtmumbaiBondId(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return FrozenBonds;
  })();

  var Originated = Id016PtmumbaiReceiptBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var LostEndorsingRewards = Id016PtmumbaiReceiptBalanceUpdates.LostEndorsingRewards = (function() {
    function LostEndorsingRewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LostEndorsingRewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.participation = this._io.readU1();
      this.revelation = this._io.readU1();
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return LostEndorsingRewards;
  })();

  var Id016PtmumbaiOperationMetadataAlphaBalanceUpdate = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceUpdate = (function() {
    function Id016PtmumbaiOperationMetadataAlphaBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiOperationMetadataAlphaBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id016PtmumbaiOperationMetadataAlphaBalanceUpdate;
  })();

  var Id016PtmumbaiOperationMetadataAlphaBalance = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalance = (function() {
    function Id016PtmumbaiOperationMetadataAlphaBalance(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiOperationMetadataAlphaBalance.prototype._read = function() {
      this.id016PtmumbaiOperationMetadataAlphaBalanceTag = this._io.readU1();
      if (this.id016PtmumbaiOperationMetadataAlphaBalanceTag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceTag.CONTRACT) {
        this.contract = new Id016PtmumbaiContractId(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationMetadataAlphaBalanceTag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceTag.DEPOSITS) {
        this.deposits = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationMetadataAlphaBalanceTag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceTag.LOST_ENDORSING_REWARDS) {
        this.lostEndorsingRewards = new LostEndorsingRewards(this._io, this, this._root);
      }
      if (this.id016PtmumbaiOperationMetadataAlphaBalanceTag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceTag.COMMITMENTS) {
        this.commitments = this._io.readBytes(20);
      }
      if (this.id016PtmumbaiOperationMetadataAlphaBalanceTag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceTag.FROZEN_BONDS) {
        this.frozenBonds = new FrozenBonds(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id016PtmumbaiOperationMetadataAlphaBalance;
  })();

  var Id016PtmumbaiTxRollupId = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiTxRollupId = (function() {
    function Id016PtmumbaiTxRollupId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiTxRollupId.prototype._read = function() {
      this.rollupHash = this._io.readBytes(20);
    }

    return Id016PtmumbaiTxRollupId;
  })();

  var Id016PtmumbaiBondId = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiBondId = (function() {
    function Id016PtmumbaiBondId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiBondId.prototype._read = function() {
      this.id016PtmumbaiBondIdTag = this._io.readU1();
      if (this.id016PtmumbaiBondIdTag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiBondIdTag.TX_ROLLUP_BOND_ID) {
        this.txRollupBondId = new Id016PtmumbaiTxRollupId(this._io, this, this._root);
      }
      if (this.id016PtmumbaiBondIdTag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiBondIdTag.SMART_ROLLUP_BOND_ID) {
        this.smartRollupBondId = new Id016PtmumbaiSmartRollupAddress(this._io, this, this._root);
      }
    }

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    /**
     * A smart rollup address: A smart rollup is identified by a base58 address starting with sr1
     */

    return Id016PtmumbaiBondId;
  })();

  var Id016PtmumbaiOperationMetadataAlphaUpdateOrigin = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaUpdateOrigin = (function() {
    function Id016PtmumbaiOperationMetadataAlphaUpdateOrigin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiOperationMetadataAlphaUpdateOrigin.prototype._read = function() {
      this.origin = this._io.readU1();
    }

    return Id016PtmumbaiOperationMetadataAlphaUpdateOrigin;
  })();

  var Id016PtmumbaiOperationMetadataAlphaBalanceUpdatesEntries = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id016PtmumbaiOperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiOperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id016PtmumbaiOperationMetadataAlphaBalance = new Id016PtmumbaiOperationMetadataAlphaBalance(this._io, this, this._root);
      this.id016PtmumbaiOperationMetadataAlphaBalanceUpdate = new Id016PtmumbaiOperationMetadataAlphaBalanceUpdate(this._io, this, this._root);
      this.id016PtmumbaiOperationMetadataAlphaUpdateOrigin = new Id016PtmumbaiOperationMetadataAlphaUpdateOrigin(this._io, this, this._root);
    }

    return Id016PtmumbaiOperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var Id016PtmumbaiOperationMetadataAlphaBalanceUpdates0 = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id016PtmumbaiOperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiOperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId016PtmumbaiOperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId016PtmumbaiOperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId016PtmumbaiOperationMetadataAlphaBalanceUpdates, this._io, "/types/id_016__ptmumbai__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id016PtmumbaiOperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId016PtmumbaiOperationMetadataAlphaBalanceUpdates);
      var _io__raw_id016PtmumbaiOperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id016PtmumbaiOperationMetadataAlphaBalanceUpdates);
      this.id016PtmumbaiOperationMetadataAlphaBalanceUpdates = new Id016PtmumbaiOperationMetadataAlphaBalanceUpdates(_io__raw_id016PtmumbaiOperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id016PtmumbaiOperationMetadataAlphaBalanceUpdates0;
  })();

  var Id016PtmumbaiContractId = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiContractId = (function() {
    function Id016PtmumbaiContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiContractId.prototype._read = function() {
      this.id016PtmumbaiContractIdTag = this._io.readU1();
      if (this.id016PtmumbaiContractIdTag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id016PtmumbaiContractIdTag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id016PtmumbaiContractId;
  })();

  var PublicKeyHash = Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Id016PtmumbaiOperationMetadataAlphaBalanceUpdates = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceUpdates = (function() {
    function Id016PtmumbaiOperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiOperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id016PtmumbaiOperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id016PtmumbaiOperationMetadataAlphaBalanceUpdatesEntries.push(new Id016PtmumbaiOperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id016PtmumbaiOperationMetadataAlphaBalanceUpdates;
  })();

  return Id016PtmumbaiReceiptBalanceUpdates;
})();
return Id016PtmumbaiReceiptBalanceUpdates;
}));
