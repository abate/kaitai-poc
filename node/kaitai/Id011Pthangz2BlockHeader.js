// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id011Pthangz2BlockHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 011-PtHangz2.block_header
 */

var Id011Pthangz2BlockHeader = (function() {
  Id011Pthangz2BlockHeader.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id011Pthangz2BlockHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2BlockHeader.prototype._read = function() {
    this.id011Pthangz2BlockHeaderAlphaFullHeader = new Id011Pthangz2BlockHeaderAlphaFullHeader(this._io, this, this._root);
  }

  var Id011Pthangz2BlockHeaderAlphaFullHeader = Id011Pthangz2BlockHeader.Id011Pthangz2BlockHeaderAlphaFullHeader = (function() {
    function Id011Pthangz2BlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2BlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id011Pthangz2BlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id011Pthangz2BlockHeaderAlphaSignedContents = new Id011Pthangz2BlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id011Pthangz2BlockHeaderAlphaFullHeader;
  })();

  var Id011Pthangz2BlockHeaderAlphaSignedContents = Id011Pthangz2BlockHeader.Id011Pthangz2BlockHeaderAlphaSignedContents = (function() {
    function Id011Pthangz2BlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2BlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id011Pthangz2BlockHeaderAlphaUnsignedContents = new Id011Pthangz2BlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id011Pthangz2BlockHeaderAlphaSignedContents;
  })();

  var Id011Pthangz2BlockHeaderAlphaUnsignedContents = Id011Pthangz2BlockHeader.Id011Pthangz2BlockHeaderAlphaUnsignedContents = (function() {
    function Id011Pthangz2BlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2BlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id011Pthangz2BlockHeader.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingEscapeVote = this._io.readU1();
    }

    return Id011Pthangz2BlockHeaderAlphaUnsignedContents;
  })();

  return Id011Pthangz2BlockHeader;
})();
return Id011Pthangz2BlockHeader;
}));
