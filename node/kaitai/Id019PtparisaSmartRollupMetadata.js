// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaSmartRollupMetadata = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.smart_rollup.metadata
 */

var Id019PtparisaSmartRollupMetadata = (function() {
  function Id019PtparisaSmartRollupMetadata(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaSmartRollupMetadata.prototype._read = function() {
    this.address = this._io.readBytes(20);
    this.originationLevel = this._io.readS4be();
  }

  return Id019PtparisaSmartRollupMetadata;
})();
return Id019PtparisaSmartRollupMetadata;
}));
