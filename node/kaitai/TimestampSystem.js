// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.TimestampSystem = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: timestamp.system
 * Description: A timestamp as seen by the underlying, local computer: subsecond-level precision, epoch or rfc3339 based.
 */

var TimestampSystem = (function() {
  function TimestampSystem(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  TimestampSystem.prototype._read = function() {
    this.timestampSystem = this._io.readS8be();
  }

  return TimestampSystem;
})();
return TimestampSystem;
}));
