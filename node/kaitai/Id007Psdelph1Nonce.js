// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id007Psdelph1Nonce = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 007-PsDELPH1.nonce
 */

var Id007Psdelph1Nonce = (function() {
  function Id007Psdelph1Nonce(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1Nonce.prototype._read = function() {
    this.id007Psdelph1Nonce = this._io.readBytes(32);
  }

  return Id007Psdelph1Nonce;
})();
return Id007Psdelph1Nonce;
}));
