// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id008Ptedo2zkDelegateFrozenBalanceByCycles = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 008-PtEdo2Zk.delegate.frozen_balance_by_cycles
 */

var Id008Ptedo2zkDelegateFrozenBalanceByCycles = (function() {
  function Id008Ptedo2zkDelegateFrozenBalanceByCycles(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkDelegateFrozenBalanceByCycles.prototype._read = function() {
    this.lenId008Ptedo2zkDelegateFrozenBalanceByCycles = this._io.readU4be();
    if (!(this.lenId008Ptedo2zkDelegateFrozenBalanceByCycles <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId008Ptedo2zkDelegateFrozenBalanceByCycles, this._io, "/seq/0");
    }
    this._raw_id008Ptedo2zkDelegateFrozenBalanceByCycles = this._io.readBytes(this.lenId008Ptedo2zkDelegateFrozenBalanceByCycles);
    var _io__raw_id008Ptedo2zkDelegateFrozenBalanceByCycles = new KaitaiStream(this._raw_id008Ptedo2zkDelegateFrozenBalanceByCycles);
    this.id008Ptedo2zkDelegateFrozenBalanceByCycles = new Id008Ptedo2zkDelegateFrozenBalanceByCycles(_io__raw_id008Ptedo2zkDelegateFrozenBalanceByCycles, this, this._root);
  }

  var Id008Ptedo2zkMutez = Id008Ptedo2zkDelegateFrozenBalanceByCycles.Id008Ptedo2zkMutez = (function() {
    function Id008Ptedo2zkMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkMutez.prototype._read = function() {
      this.id008Ptedo2zkMutez = new N(this._io, this, this._root);
    }

    return Id008Ptedo2zkMutez;
  })();

  var N = Id008Ptedo2zkDelegateFrozenBalanceByCycles.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id008Ptedo2zkDelegateFrozenBalanceByCyclesEntries = Id008Ptedo2zkDelegateFrozenBalanceByCycles.Id008Ptedo2zkDelegateFrozenBalanceByCyclesEntries = (function() {
    function Id008Ptedo2zkDelegateFrozenBalanceByCyclesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkDelegateFrozenBalanceByCyclesEntries.prototype._read = function() {
      this.cycle = this._io.readS4be();
      this.deposit = new Id008Ptedo2zkMutez(this._io, this, this._root);
      this.fees = new Id008Ptedo2zkMutez(this._io, this, this._root);
      this.rewards = new Id008Ptedo2zkMutez(this._io, this, this._root);
    }

    return Id008Ptedo2zkDelegateFrozenBalanceByCyclesEntries;
  })();

  var Id008Ptedo2zkDelegateFrozenBalanceByCycles = Id008Ptedo2zkDelegateFrozenBalanceByCycles.Id008Ptedo2zkDelegateFrozenBalanceByCycles = (function() {
    function Id008Ptedo2zkDelegateFrozenBalanceByCycles(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkDelegateFrozenBalanceByCycles.prototype._read = function() {
      this.id008Ptedo2zkDelegateFrozenBalanceByCyclesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id008Ptedo2zkDelegateFrozenBalanceByCyclesEntries.push(new Id008Ptedo2zkDelegateFrozenBalanceByCyclesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id008Ptedo2zkDelegateFrozenBalanceByCycles;
  })();

  var NChunk = Id008Ptedo2zkDelegateFrozenBalanceByCycles.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  return Id008Ptedo2zkDelegateFrozenBalanceByCycles;
})();
return Id008Ptedo2zkDelegateFrozenBalanceByCycles;
}));
