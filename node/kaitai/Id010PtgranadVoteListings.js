// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id010PtgranadVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 010-PtGRANAD.vote.listings
 */

var Id010PtgranadVoteListings = (function() {
  Id010PtgranadVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id010PtgranadVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id010PtgranadVoteListings.prototype._read = function() {
    this.lenId010PtgranadVoteListings = this._io.readU4be();
    if (!(this.lenId010PtgranadVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId010PtgranadVoteListings, this._io, "/seq/0");
    }
    this._raw_id010PtgranadVoteListings = this._io.readBytes(this.lenId010PtgranadVoteListings);
    var _io__raw_id010PtgranadVoteListings = new KaitaiStream(this._raw_id010PtgranadVoteListings);
    this.id010PtgranadVoteListings = new Id010PtgranadVoteListings(_io__raw_id010PtgranadVoteListings, this, this._root);
  }

  var Id010PtgranadVoteListings = Id010PtgranadVoteListings.Id010PtgranadVoteListings = (function() {
    function Id010PtgranadVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id010PtgranadVoteListings.prototype._read = function() {
      this.id010PtgranadVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id010PtgranadVoteListingsEntries.push(new Id010PtgranadVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id010PtgranadVoteListings;
  })();

  var Id010PtgranadVoteListingsEntries = Id010PtgranadVoteListings.Id010PtgranadVoteListingsEntries = (function() {
    function Id010PtgranadVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id010PtgranadVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.rolls = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id010PtgranadVoteListingsEntries;
  })();

  var PublicKeyHash = Id010PtgranadVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id010PtgranadVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id010PtgranadVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id010PtgranadVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id010PtgranadVoteListings;
})();
return Id010PtgranadVoteListings;
}));
