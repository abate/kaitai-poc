// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.SignerMessagesRequest = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: signer_messages.request
 */

var SignerMessagesRequest = (function() {
  SignerMessagesRequest.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  SignerMessagesRequest.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  SignerMessagesRequest.SignerMessagesRequestTag = Object.freeze({
    SIGN: 0,
    PUBLIC_KEY: 1,
    AUTHORIZED_KEYS: 2,
    DETERMINISTIC_NONCE: 3,
    DETERMINISTIC_NONCE_HASH: 4,
    SUPPORTS_DETERMINISTIC_NONCES: 5,

    0: "SIGN",
    1: "PUBLIC_KEY",
    2: "AUTHORIZED_KEYS",
    3: "DETERMINISTIC_NONCE",
    4: "DETERMINISTIC_NONCE_HASH",
    5: "SUPPORTS_DETERMINISTIC_NONCES",
  });

  function SignerMessagesRequest(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  SignerMessagesRequest.prototype._read = function() {
    this.signerMessagesRequestTag = this._io.readU1();
    if (this.signerMessagesRequestTag == SignerMessagesRequest.SignerMessagesRequestTag.SIGN) {
      this.sign = new Sign(this._io, this, this._root);
    }
    if (this.signerMessagesRequestTag == SignerMessagesRequest.SignerMessagesRequestTag.PUBLIC_KEY) {
      this.publicKey = new SignerMessagesPublicKeyRequest(this._io, this, this._root);
    }
    if (this.signerMessagesRequestTag == SignerMessagesRequest.SignerMessagesRequestTag.DETERMINISTIC_NONCE) {
      this.deterministicNonce = new DeterministicNonce(this._io, this, this._root);
    }
    if (this.signerMessagesRequestTag == SignerMessagesRequest.SignerMessagesRequestTag.DETERMINISTIC_NONCE_HASH) {
      this.deterministicNonceHash = new DeterministicNonceHash(this._io, this, this._root);
    }
    if (this.signerMessagesRequestTag == SignerMessagesRequest.SignerMessagesRequestTag.SUPPORTS_DETERMINISTIC_NONCES) {
      this.supportsDeterministicNonces = new SignerMessagesSupportsDeterministicNoncesRequest(this._io, this, this._root);
    }
  }

  var SignerMessagesPublicKeyRequest = SignerMessagesRequest.SignerMessagesPublicKeyRequest = (function() {
    function SignerMessagesPublicKeyRequest(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SignerMessagesPublicKeyRequest.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return SignerMessagesPublicKeyRequest;
  })();

  var DeterministicNonce = SignerMessagesRequest.DeterministicNonce = (function() {
    function DeterministicNonce(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DeterministicNonce.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.data = new BytesDynUint30(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == SignerMessagesRequest.Bool.TRUE) {
        this.signature = this._io.readBytesFull();
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return DeterministicNonce;
  })();

  var Sign = SignerMessagesRequest.Sign = (function() {
    function Sign(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sign.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.data = new BytesDynUint30(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == SignerMessagesRequest.Bool.TRUE) {
        this.signature = this._io.readBytesFull();
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Sign;
  })();

  var SignerMessagesSupportsDeterministicNoncesRequest = SignerMessagesRequest.SignerMessagesSupportsDeterministicNoncesRequest = (function() {
    function SignerMessagesSupportsDeterministicNoncesRequest(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SignerMessagesSupportsDeterministicNoncesRequest.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return SignerMessagesSupportsDeterministicNoncesRequest;
  })();

  var BytesDynUint30 = SignerMessagesRequest.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var DeterministicNonceHash = SignerMessagesRequest.DeterministicNonceHash = (function() {
    function DeterministicNonceHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DeterministicNonceHash.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.data = new BytesDynUint30(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == SignerMessagesRequest.Bool.TRUE) {
        this.signature = this._io.readBytesFull();
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return DeterministicNonceHash;
  })();

  var PublicKeyHash = SignerMessagesRequest.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == SignerMessagesRequest.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == SignerMessagesRequest.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == SignerMessagesRequest.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == SignerMessagesRequest.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return SignerMessagesRequest;
})();
return SignerMessagesRequest;
}));
