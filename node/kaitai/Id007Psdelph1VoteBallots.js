// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id007Psdelph1VoteBallots = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 007-PsDELPH1.vote.ballots
 */

var Id007Psdelph1VoteBallots = (function() {
  function Id007Psdelph1VoteBallots(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1VoteBallots.prototype._read = function() {
    this.yay = this._io.readS4be();
    this.nay = this._io.readS4be();
    this.pass = this._io.readS4be();
  }

  return Id007Psdelph1VoteBallots;
})();
return Id007Psdelph1VoteBallots;
}));
