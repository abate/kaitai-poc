// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaSmartRollupCommmitment = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.smart_rollup.commmitment
 */

var AlphaSmartRollupCommmitment = (function() {
  function AlphaSmartRollupCommmitment(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaSmartRollupCommmitment.prototype._read = function() {
    this.compressedState = this._io.readBytes(32);
    this.inboxLevel = this._io.readS4be();
    this.predecessor = this._io.readBytes(32);
    this.numberOfTicks = this._io.readS8be();
  }

  return AlphaSmartRollupCommmitment;
})();
return AlphaSmartRollupCommmitment;
}));
