// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id010PtgranadDelegateFrozenBalance = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 010-PtGRANAD.delegate.frozen_balance
 */

var Id010PtgranadDelegateFrozenBalance = (function() {
  function Id010PtgranadDelegateFrozenBalance(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id010PtgranadDelegateFrozenBalance.prototype._read = function() {
    this.deposits = new Id010PtgranadMutez(this._io, this, this._root);
    this.fees = new Id010PtgranadMutez(this._io, this, this._root);
    this.rewards = new Id010PtgranadMutez(this._io, this, this._root);
  }

  var Id010PtgranadMutez = Id010PtgranadDelegateFrozenBalance.Id010PtgranadMutez = (function() {
    function Id010PtgranadMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id010PtgranadMutez.prototype._read = function() {
      this.id010PtgranadMutez = new N(this._io, this, this._root);
    }

    return Id010PtgranadMutez;
  })();

  var N = Id010PtgranadDelegateFrozenBalance.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var NChunk = Id010PtgranadDelegateFrozenBalance.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  return Id010PtgranadDelegateFrozenBalance;
})();
return Id010PtgranadDelegateFrozenBalance;
}));
