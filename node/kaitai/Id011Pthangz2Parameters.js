// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id011Pthangz2Parameters = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 011-PtHangz2.parameters
 */

var Id011Pthangz2Parameters = (function() {
  Id011Pthangz2Parameters.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id011Pthangz2Parameters.BootstrapAccountsEltTag = Object.freeze({
    PUBLIC_KEY_KNOWN: 0,
    PUBLIC_KEY_UNKNOWN: 1,

    0: "PUBLIC_KEY_KNOWN",
    1: "PUBLIC_KEY_UNKNOWN",
  });

  Id011Pthangz2Parameters.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id011Pthangz2Parameters.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id011Pthangz2Parameters(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2Parameters.prototype._read = function() {
    this.bootstrapAccounts = new BootstrapAccounts0(this._io, this, this._root);
    this.bootstrapContracts = new BootstrapContracts0(this._io, this, this._root);
    this.commitments = new Commitments0(this._io, this, this._root);
    this.securityDepositRampUpCyclesTag = this._io.readU1();
    if (this.securityDepositRampUpCyclesTag == Id011Pthangz2Parameters.Bool.TRUE) {
      this.securityDepositRampUpCycles = new Int31(this._io, this, this._root);
    }
    this.noRewardCyclesTag = this._io.readU1();
    if (this.noRewardCyclesTag == Id011Pthangz2Parameters.Bool.TRUE) {
      this.noRewardCycles = new Int31(this._io, this, this._root);
    }
    this.preservedCycles = this._io.readU1();
    this.blocksPerCycle = this._io.readS4be();
    this.blocksPerCommitment = this._io.readS4be();
    this.blocksPerRollSnapshot = this._io.readS4be();
    this.blocksPerVotingPeriod = this._io.readS4be();
    this.timeBetweenBlocks = new TimeBetweenBlocks0(this._io, this, this._root);
    this.endorsersPerBlock = this._io.readU2be();
    this.hardGasLimitPerOperation = new Z(this._io, this, this._root);
    this.hardGasLimitPerBlock = new Z(this._io, this, this._root);
    this.proofOfWorkThreshold = this._io.readS8be();
    this.tokensPerRoll = new Id011Pthangz2Mutez(this._io, this, this._root);
    this.seedNonceRevelationTip = new Id011Pthangz2Mutez(this._io, this, this._root);
    this.originationSize = new Int31(this._io, this, this._root);
    this.blockSecurityDeposit = new Id011Pthangz2Mutez(this._io, this, this._root);
    this.endorsementSecurityDeposit = new Id011Pthangz2Mutez(this._io, this, this._root);
    this.bakingRewardPerEndorsement = new BakingRewardPerEndorsement0(this._io, this, this._root);
    this.endorsementReward = new EndorsementReward0(this._io, this, this._root);
    this.costPerByte = new Id011Pthangz2Mutez(this._io, this, this._root);
    this.hardStorageLimitPerOperation = new Z(this._io, this, this._root);
    this.quorumMin = this._io.readS4be();
    this.quorumMax = this._io.readS4be();
    this.minProposalQuorum = this._io.readS4be();
    this.initialEndorsers = this._io.readU2be();
    this.delayPerMissingEndorsement = this._io.readS8be();
    this.minimalBlockDelay = this._io.readS8be();
    this.liquidityBakingSubsidy = new Id011Pthangz2Mutez(this._io, this, this._root);
    this.liquidityBakingSunsetLevel = this._io.readS4be();
    this.liquidityBakingEscapeEmaThreshold = this._io.readS4be();
  }

  var TimeBetweenBlocksEntries = Id011Pthangz2Parameters.TimeBetweenBlocksEntries = (function() {
    function TimeBetweenBlocksEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TimeBetweenBlocksEntries.prototype._read = function() {
      this.timeBetweenBlocksElt = this._io.readS8be();
    }

    return TimeBetweenBlocksEntries;
  })();

  var PublicKeyKnown = Id011Pthangz2Parameters.PublicKeyKnown = (function() {
    function PublicKeyKnown(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyKnown.prototype._read = function() {
      this.publicKeyKnownField0 = new PublicKey(this._io, this, this._root);
      this.publicKeyKnownField1 = new Id011Pthangz2Mutez(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key
     * 
     * signature__v0__public_key
     */

    /**
     * id_011__pthangz2__mutez
     */

    return PublicKeyKnown;
  })();

  var Commitments = Id011Pthangz2Parameters.Commitments = (function() {
    function Commitments(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments.prototype._read = function() {
      this.commitmentsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.commitmentsEntries.push(new CommitmentsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Commitments;
  })();

  var N = Id011Pthangz2Parameters.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var TimeBetweenBlocks = Id011Pthangz2Parameters.TimeBetweenBlocks = (function() {
    function TimeBetweenBlocks(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TimeBetweenBlocks.prototype._read = function() {
      this.timeBetweenBlocksEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.timeBetweenBlocksEntries.push(new TimeBetweenBlocksEntries(this._io, this, this._root));
        i++;
      }
    }

    return TimeBetweenBlocks;
  })();

  var EndorsementReward = Id011Pthangz2Parameters.EndorsementReward = (function() {
    function EndorsementReward(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    EndorsementReward.prototype._read = function() {
      this.endorsementRewardEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.endorsementRewardEntries.push(new EndorsementRewardEntries(this._io, this, this._root));
        i++;
      }
    }

    return EndorsementReward;
  })();

  var CommitmentsEntries = Id011Pthangz2Parameters.CommitmentsEntries = (function() {
    function CommitmentsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CommitmentsEntries.prototype._read = function() {
      this.commitmentsEltField0 = this._io.readBytes(20);
      this.commitmentsEltField1 = new Id011Pthangz2Mutez(this._io, this, this._root);
    }

    /**
     * blinded__public__key__hash
     */

    /**
     * id_011__pthangz2__mutez
     */

    return CommitmentsEntries;
  })();

  var Id011Pthangz2Mutez = Id011Pthangz2Parameters.Id011Pthangz2Mutez = (function() {
    function Id011Pthangz2Mutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2Mutez.prototype._read = function() {
      this.id011Pthangz2Mutez = new N(this._io, this, this._root);
    }

    return Id011Pthangz2Mutez;
  })();

  var EndorsementReward0 = Id011Pthangz2Parameters.EndorsementReward0 = (function() {
    function EndorsementReward0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    EndorsementReward0.prototype._read = function() {
      this.lenEndorsementReward = this._io.readU4be();
      if (!(this.lenEndorsementReward <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenEndorsementReward, this._io, "/types/endorsement_reward_0/seq/0");
      }
      this._raw_endorsementReward = this._io.readBytes(this.lenEndorsementReward);
      var _io__raw_endorsementReward = new KaitaiStream(this._raw_endorsementReward);
      this.endorsementReward = new EndorsementReward(_io__raw_endorsementReward, this, this._root);
    }

    return EndorsementReward0;
  })();

  var Commitments0 = Id011Pthangz2Parameters.Commitments0 = (function() {
    function Commitments0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments0.prototype._read = function() {
      this.lenCommitments = this._io.readU4be();
      if (!(this.lenCommitments <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCommitments, this._io, "/types/commitments_0/seq/0");
      }
      this._raw_commitments = this._io.readBytes(this.lenCommitments);
      var _io__raw_commitments = new KaitaiStream(this._raw_commitments);
      this.commitments = new Commitments(_io__raw_commitments, this, this._root);
    }

    return Commitments0;
  })();

  var BootstrapAccounts0 = Id011Pthangz2Parameters.BootstrapAccounts0 = (function() {
    function BootstrapAccounts0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccounts0.prototype._read = function() {
      this.lenBootstrapAccounts = this._io.readU4be();
      if (!(this.lenBootstrapAccounts <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBootstrapAccounts, this._io, "/types/bootstrap_accounts_0/seq/0");
      }
      this._raw_bootstrapAccounts = this._io.readBytes(this.lenBootstrapAccounts);
      var _io__raw_bootstrapAccounts = new KaitaiStream(this._raw_bootstrapAccounts);
      this.bootstrapAccounts = new BootstrapAccounts(_io__raw_bootstrapAccounts, this, this._root);
    }

    return BootstrapAccounts0;
  })();

  var PublicKey = Id011Pthangz2Parameters.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id011Pthangz2Parameters.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id011Pthangz2Parameters.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id011Pthangz2Parameters.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var PublicKeyUnknown = Id011Pthangz2Parameters.PublicKeyUnknown = (function() {
    function PublicKeyUnknown(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyUnknown.prototype._read = function() {
      this.publicKeyUnknownField0 = new PublicKeyHash(this._io, this, this._root);
      this.publicKeyUnknownField1 = new Id011Pthangz2Mutez(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     * 
     * signature__v0__public_key_hash
     */

    /**
     * id_011__pthangz2__mutez
     */

    return PublicKeyUnknown;
  })();

  var BootstrapAccountsEntries = Id011Pthangz2Parameters.BootstrapAccountsEntries = (function() {
    function BootstrapAccountsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccountsEntries.prototype._read = function() {
      this.bootstrapAccountsEltTag = this._io.readU1();
      if (this.bootstrapAccountsEltTag == Id011Pthangz2Parameters.BootstrapAccountsEltTag.PUBLIC_KEY_KNOWN) {
        this.publicKeyKnown = new PublicKeyKnown(this._io, this, this._root);
      }
      if (this.bootstrapAccountsEltTag == Id011Pthangz2Parameters.BootstrapAccountsEltTag.PUBLIC_KEY_UNKNOWN) {
        this.publicKeyUnknown = new PublicKeyUnknown(this._io, this, this._root);
      }
    }

    return BootstrapAccountsEntries;
  })();

  var BootstrapContracts0 = Id011Pthangz2Parameters.BootstrapContracts0 = (function() {
    function BootstrapContracts0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContracts0.prototype._read = function() {
      this.lenBootstrapContracts = this._io.readU4be();
      if (!(this.lenBootstrapContracts <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBootstrapContracts, this._io, "/types/bootstrap_contracts_0/seq/0");
      }
      this._raw_bootstrapContracts = this._io.readBytes(this.lenBootstrapContracts);
      var _io__raw_bootstrapContracts = new KaitaiStream(this._raw_bootstrapContracts);
      this.bootstrapContracts = new BootstrapContracts(_io__raw_bootstrapContracts, this, this._root);
    }

    return BootstrapContracts0;
  })();

  var BytesDynUint30 = Id011Pthangz2Parameters.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var EndorsementRewardEntries = Id011Pthangz2Parameters.EndorsementRewardEntries = (function() {
    function EndorsementRewardEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    EndorsementRewardEntries.prototype._read = function() {
      this.id011Pthangz2Mutez = new Id011Pthangz2Mutez(this._io, this, this._root);
    }

    return EndorsementRewardEntries;
  })();

  var BakingRewardPerEndorsement0 = Id011Pthangz2Parameters.BakingRewardPerEndorsement0 = (function() {
    function BakingRewardPerEndorsement0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BakingRewardPerEndorsement0.prototype._read = function() {
      this.lenBakingRewardPerEndorsement = this._io.readU4be();
      if (!(this.lenBakingRewardPerEndorsement <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBakingRewardPerEndorsement, this._io, "/types/baking_reward_per_endorsement_0/seq/0");
      }
      this._raw_bakingRewardPerEndorsement = this._io.readBytes(this.lenBakingRewardPerEndorsement);
      var _io__raw_bakingRewardPerEndorsement = new KaitaiStream(this._raw_bakingRewardPerEndorsement);
      this.bakingRewardPerEndorsement = new BakingRewardPerEndorsement(_io__raw_bakingRewardPerEndorsement, this, this._root);
    }

    return BakingRewardPerEndorsement0;
  })();

  var Id011Pthangz2ScriptedContracts = Id011Pthangz2Parameters.Id011Pthangz2ScriptedContracts = (function() {
    function Id011Pthangz2ScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2ScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id011Pthangz2ScriptedContracts;
  })();

  var TimeBetweenBlocks0 = Id011Pthangz2Parameters.TimeBetweenBlocks0 = (function() {
    function TimeBetweenBlocks0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TimeBetweenBlocks0.prototype._read = function() {
      this.lenTimeBetweenBlocks = this._io.readU4be();
      if (!(this.lenTimeBetweenBlocks <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenTimeBetweenBlocks, this._io, "/types/time_between_blocks_0/seq/0");
      }
      this._raw_timeBetweenBlocks = this._io.readBytes(this.lenTimeBetweenBlocks);
      var _io__raw_timeBetweenBlocks = new KaitaiStream(this._raw_timeBetweenBlocks);
      this.timeBetweenBlocks = new TimeBetweenBlocks(_io__raw_timeBetweenBlocks, this, this._root);
    }

    return TimeBetweenBlocks0;
  })();

  var BakingRewardPerEndorsement = Id011Pthangz2Parameters.BakingRewardPerEndorsement = (function() {
    function BakingRewardPerEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BakingRewardPerEndorsement.prototype._read = function() {
      this.bakingRewardPerEndorsementEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bakingRewardPerEndorsementEntries.push(new BakingRewardPerEndorsementEntries(this._io, this, this._root));
        i++;
      }
    }

    return BakingRewardPerEndorsement;
  })();

  var BootstrapContractsEntries = Id011Pthangz2Parameters.BootstrapContractsEntries = (function() {
    function BootstrapContractsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContractsEntries.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.amount = new Id011Pthangz2Mutez(this._io, this, this._root);
      this.script = new Id011Pthangz2ScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return BootstrapContractsEntries;
  })();

  var BootstrapContracts = Id011Pthangz2Parameters.BootstrapContracts = (function() {
    function BootstrapContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContracts.prototype._read = function() {
      this.bootstrapContractsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bootstrapContractsEntries.push(new BootstrapContractsEntries(this._io, this, this._root));
        i++;
      }
    }

    return BootstrapContracts;
  })();

  var BootstrapAccounts = Id011Pthangz2Parameters.BootstrapAccounts = (function() {
    function BootstrapAccounts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccounts.prototype._read = function() {
      this.bootstrapAccountsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bootstrapAccountsEntries.push(new BootstrapAccountsEntries(this._io, this, this._root));
        i++;
      }
    }

    return BootstrapAccounts;
  })();

  var Int31 = Id011Pthangz2Parameters.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id011Pthangz2Parameters.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var PublicKeyHash = Id011Pthangz2Parameters.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id011Pthangz2Parameters.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id011Pthangz2Parameters.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id011Pthangz2Parameters.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Z = Id011Pthangz2Parameters.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  var BakingRewardPerEndorsementEntries = Id011Pthangz2Parameters.BakingRewardPerEndorsementEntries = (function() {
    function BakingRewardPerEndorsementEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BakingRewardPerEndorsementEntries.prototype._read = function() {
      this.id011Pthangz2Mutez = new Id011Pthangz2Mutez(this._io, this, this._root);
    }

    return BakingRewardPerEndorsementEntries;
  })();

  return Id011Pthangz2Parameters;
})();
return Id011Pthangz2Parameters;
}));
