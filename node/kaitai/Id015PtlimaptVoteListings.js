// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id015PtlimaptVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 015-PtLimaPt.vote.listings
 */

var Id015PtlimaptVoteListings = (function() {
  Id015PtlimaptVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id015PtlimaptVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id015PtlimaptVoteListings.prototype._read = function() {
    this.lenId015PtlimaptVoteListings = this._io.readU4be();
    if (!(this.lenId015PtlimaptVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId015PtlimaptVoteListings, this._io, "/seq/0");
    }
    this._raw_id015PtlimaptVoteListings = this._io.readBytes(this.lenId015PtlimaptVoteListings);
    var _io__raw_id015PtlimaptVoteListings = new KaitaiStream(this._raw_id015PtlimaptVoteListings);
    this.id015PtlimaptVoteListings = new Id015PtlimaptVoteListings(_io__raw_id015PtlimaptVoteListings, this, this._root);
  }

  var Id015PtlimaptVoteListings = Id015PtlimaptVoteListings.Id015PtlimaptVoteListings = (function() {
    function Id015PtlimaptVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptVoteListings.prototype._read = function() {
      this.id015PtlimaptVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id015PtlimaptVoteListingsEntries.push(new Id015PtlimaptVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id015PtlimaptVoteListings;
  })();

  var Id015PtlimaptVoteListingsEntries = Id015PtlimaptVoteListings.Id015PtlimaptVoteListingsEntries = (function() {
    function Id015PtlimaptVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.votingPower = this._io.readS8be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id015PtlimaptVoteListingsEntries;
  })();

  var PublicKeyHash = Id015PtlimaptVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id015PtlimaptVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id015PtlimaptVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id015PtlimaptVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id015PtlimaptVoteListings;
})();
return Id015PtlimaptVoteListings;
}));
