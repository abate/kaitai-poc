// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.P2pPeerState = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: p2p_peer.state
 * Description: The state a peer connection can be in: accepted (when the connection is being established), running (when the connection is already established), disconnected (otherwise).
 */

var P2pPeerState = (function() {
  P2pPeerState.P2pPeerState = Object.freeze({
    ACCEPTED: 0,
    RUNNING: 1,
    DISCONNECTED: 2,

    0: "ACCEPTED",
    1: "RUNNING",
    2: "DISCONNECTED",
  });

  function P2pPeerState(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  P2pPeerState.prototype._read = function() {
    this.p2pPeerState = this._io.readU1();
  }

  return P2pPeerState;
})();
return P2pPeerState;
}));
