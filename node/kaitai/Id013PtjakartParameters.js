// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id013PtjakartParameters = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 013-PtJakart.parameters
 */

var Id013PtjakartParameters = (function() {
  Id013PtjakartParameters.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id013PtjakartParameters.BootstrapAccountsEltTag = Object.freeze({
    PUBLIC_KEY_KNOWN: 0,
    PUBLIC_KEY_UNKNOWN: 1,

    0: "PUBLIC_KEY_KNOWN",
    1: "PUBLIC_KEY_UNKNOWN",
  });

  Id013PtjakartParameters.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id013PtjakartParameters.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id013PtjakartParameters(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id013PtjakartParameters.prototype._read = function() {
    this.bootstrapAccounts = new BootstrapAccounts0(this._io, this, this._root);
    this.bootstrapContracts = new BootstrapContracts0(this._io, this, this._root);
    this.commitments = new Commitments0(this._io, this, this._root);
    this.securityDepositRampUpCyclesTag = this._io.readU1();
    if (this.securityDepositRampUpCyclesTag == Id013PtjakartParameters.Bool.TRUE) {
      this.securityDepositRampUpCycles = new Int31(this._io, this, this._root);
    }
    this.noRewardCyclesTag = this._io.readU1();
    if (this.noRewardCyclesTag == Id013PtjakartParameters.Bool.TRUE) {
      this.noRewardCycles = new Int31(this._io, this, this._root);
    }
    this.preservedCycles = this._io.readU1();
    this.blocksPerCycle = this._io.readS4be();
    this.blocksPerCommitment = this._io.readS4be();
    this.blocksPerStakeSnapshot = this._io.readS4be();
    this.cyclesPerVotingPeriod = this._io.readS4be();
    this.hardGasLimitPerOperation = new Z(this._io, this, this._root);
    this.hardGasLimitPerBlock = new Z(this._io, this, this._root);
    this.proofOfWorkThreshold = this._io.readS8be();
    this.tokensPerRoll = new Id013PtjakartMutez(this._io, this, this._root);
    this.seedNonceRevelationTip = new Id013PtjakartMutez(this._io, this, this._root);
    this.originationSize = new Int31(this._io, this, this._root);
    this.bakingRewardFixedPortion = new Id013PtjakartMutez(this._io, this, this._root);
    this.bakingRewardBonusPerSlot = new Id013PtjakartMutez(this._io, this, this._root);
    this.endorsingRewardPerSlot = new Id013PtjakartMutez(this._io, this, this._root);
    this.costPerByte = new Id013PtjakartMutez(this._io, this, this._root);
    this.hardStorageLimitPerOperation = new Z(this._io, this, this._root);
    this.quorumMin = this._io.readS4be();
    this.quorumMax = this._io.readS4be();
    this.minProposalQuorum = this._io.readS4be();
    this.liquidityBakingSubsidy = new Id013PtjakartMutez(this._io, this, this._root);
    this.liquidityBakingSunsetLevel = this._io.readS4be();
    this.liquidityBakingToggleEmaThreshold = this._io.readS4be();
    this.maxOperationsTimeToLive = this._io.readS2be();
    this.minimalBlockDelay = this._io.readS8be();
    this.delayIncrementPerRound = this._io.readS8be();
    this.consensusCommitteeSize = new Int31(this._io, this, this._root);
    this.consensusThreshold = new Int31(this._io, this, this._root);
    this.minimalParticipationRatio = new MinimalParticipationRatio(this._io, this, this._root);
    this.maxSlashingPeriod = new Int31(this._io, this, this._root);
    this.frozenDepositsPercentage = new Int31(this._io, this, this._root);
    this.doubleBakingPunishment = new Id013PtjakartMutez(this._io, this, this._root);
    this.ratioOfFrozenDepositsSlashedPerDoubleEndorsement = new RatioOfFrozenDepositsSlashedPerDoubleEndorsement(this._io, this, this._root);
    this.initialSeedTag = this._io.readU1();
    if (this.initialSeedTag == Id013PtjakartParameters.Bool.TRUE) {
      this.initialSeed = this._io.readBytes(32);
    }
    this.cacheScriptSize = new Int31(this._io, this, this._root);
    this.cacheStakeDistributionCycles = this._io.readS1();
    this.cacheSamplerStateCycles = this._io.readS1();
    this.txRollupEnable = this._io.readU1();
    this.txRollupOriginationSize = new Int31(this._io, this, this._root);
    this.txRollupHardSizeLimitPerInbox = new Int31(this._io, this, this._root);
    this.txRollupHardSizeLimitPerMessage = new Int31(this._io, this, this._root);
    this.txRollupMaxWithdrawalsPerBatch = new Int31(this._io, this, this._root);
    this.txRollupCommitmentBond = new Id013PtjakartMutez(this._io, this, this._root);
    this.txRollupFinalityPeriod = new Int31(this._io, this, this._root);
    this.txRollupWithdrawPeriod = new Int31(this._io, this, this._root);
    this.txRollupMaxInboxesCount = new Int31(this._io, this, this._root);
    this.txRollupMaxMessagesPerInbox = new Int31(this._io, this, this._root);
    this.txRollupMaxCommitmentsCount = new Int31(this._io, this, this._root);
    this.txRollupCostPerByteEmaFactor = new Int31(this._io, this, this._root);
    this.txRollupMaxTicketPayloadSize = new Int31(this._io, this, this._root);
    this.txRollupRejectionMaxProofSize = new Int31(this._io, this, this._root);
    this.txRollupSunsetLevel = this._io.readS4be();
    this.scRollupEnable = this._io.readU1();
    this.scRollupOriginationSize = new Int31(this._io, this, this._root);
    this.scRollupChallengeWindowInBlocks = new Int31(this._io, this, this._root);
    this.scRollupMaxAvailableMessages = new Int31(this._io, this, this._root);
  }

  var RatioOfFrozenDepositsSlashedPerDoubleEndorsement = Id013PtjakartParameters.RatioOfFrozenDepositsSlashedPerDoubleEndorsement = (function() {
    function RatioOfFrozenDepositsSlashedPerDoubleEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RatioOfFrozenDepositsSlashedPerDoubleEndorsement.prototype._read = function() {
      this.numerator = this._io.readU2be();
      this.denominator = this._io.readU2be();
    }

    return RatioOfFrozenDepositsSlashedPerDoubleEndorsement;
  })();

  var PublicKeyKnown = Id013PtjakartParameters.PublicKeyKnown = (function() {
    function PublicKeyKnown(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyKnown.prototype._read = function() {
      this.publicKeyKnownField0 = new PublicKey(this._io, this, this._root);
      this.publicKeyKnownField1 = new Id013PtjakartMutez(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key
     * 
     * signature__v0__public_key
     */

    /**
     * id_013__ptjakart__mutez
     */

    return PublicKeyKnown;
  })();

  var Commitments = Id013PtjakartParameters.Commitments = (function() {
    function Commitments(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments.prototype._read = function() {
      this.commitmentsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.commitmentsEntries.push(new CommitmentsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Commitments;
  })();

  var N = Id013PtjakartParameters.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var CommitmentsEntries = Id013PtjakartParameters.CommitmentsEntries = (function() {
    function CommitmentsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CommitmentsEntries.prototype._read = function() {
      this.commitmentsEltField0 = this._io.readBytes(20);
      this.commitmentsEltField1 = new Id013PtjakartMutez(this._io, this, this._root);
    }

    /**
     * blinded__public__key__hash
     */

    /**
     * id_013__ptjakart__mutez
     */

    return CommitmentsEntries;
  })();

  var Commitments0 = Id013PtjakartParameters.Commitments0 = (function() {
    function Commitments0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments0.prototype._read = function() {
      this.lenCommitments = this._io.readU4be();
      if (!(this.lenCommitments <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCommitments, this._io, "/types/commitments_0/seq/0");
      }
      this._raw_commitments = this._io.readBytes(this.lenCommitments);
      var _io__raw_commitments = new KaitaiStream(this._raw_commitments);
      this.commitments = new Commitments(_io__raw_commitments, this, this._root);
    }

    return Commitments0;
  })();

  var MinimalParticipationRatio = Id013PtjakartParameters.MinimalParticipationRatio = (function() {
    function MinimalParticipationRatio(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MinimalParticipationRatio.prototype._read = function() {
      this.numerator = this._io.readU2be();
      this.denominator = this._io.readU2be();
    }

    return MinimalParticipationRatio;
  })();

  var BootstrapAccounts0 = Id013PtjakartParameters.BootstrapAccounts0 = (function() {
    function BootstrapAccounts0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccounts0.prototype._read = function() {
      this.lenBootstrapAccounts = this._io.readU4be();
      if (!(this.lenBootstrapAccounts <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBootstrapAccounts, this._io, "/types/bootstrap_accounts_0/seq/0");
      }
      this._raw_bootstrapAccounts = this._io.readBytes(this.lenBootstrapAccounts);
      var _io__raw_bootstrapAccounts = new KaitaiStream(this._raw_bootstrapAccounts);
      this.bootstrapAccounts = new BootstrapAccounts(_io__raw_bootstrapAccounts, this, this._root);
    }

    return BootstrapAccounts0;
  })();

  var Id013PtjakartScriptedContracts = Id013PtjakartParameters.Id013PtjakartScriptedContracts = (function() {
    function Id013PtjakartScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id013PtjakartScriptedContracts;
  })();

  var PublicKey = Id013PtjakartParameters.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id013PtjakartParameters.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id013PtjakartParameters.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id013PtjakartParameters.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var PublicKeyUnknown = Id013PtjakartParameters.PublicKeyUnknown = (function() {
    function PublicKeyUnknown(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyUnknown.prototype._read = function() {
      this.publicKeyUnknownField0 = new PublicKeyHash(this._io, this, this._root);
      this.publicKeyUnknownField1 = new Id013PtjakartMutez(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     * 
     * signature__v0__public_key_hash
     */

    /**
     * id_013__ptjakart__mutez
     */

    return PublicKeyUnknown;
  })();

  var BootstrapAccountsEntries = Id013PtjakartParameters.BootstrapAccountsEntries = (function() {
    function BootstrapAccountsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccountsEntries.prototype._read = function() {
      this.bootstrapAccountsEltTag = this._io.readU1();
      if (this.bootstrapAccountsEltTag == Id013PtjakartParameters.BootstrapAccountsEltTag.PUBLIC_KEY_KNOWN) {
        this.publicKeyKnown = new PublicKeyKnown(this._io, this, this._root);
      }
      if (this.bootstrapAccountsEltTag == Id013PtjakartParameters.BootstrapAccountsEltTag.PUBLIC_KEY_UNKNOWN) {
        this.publicKeyUnknown = new PublicKeyUnknown(this._io, this, this._root);
      }
    }

    return BootstrapAccountsEntries;
  })();

  var BootstrapContracts0 = Id013PtjakartParameters.BootstrapContracts0 = (function() {
    function BootstrapContracts0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContracts0.prototype._read = function() {
      this.lenBootstrapContracts = this._io.readU4be();
      if (!(this.lenBootstrapContracts <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBootstrapContracts, this._io, "/types/bootstrap_contracts_0/seq/0");
      }
      this._raw_bootstrapContracts = this._io.readBytes(this.lenBootstrapContracts);
      var _io__raw_bootstrapContracts = new KaitaiStream(this._raw_bootstrapContracts);
      this.bootstrapContracts = new BootstrapContracts(_io__raw_bootstrapContracts, this, this._root);
    }

    return BootstrapContracts0;
  })();

  var Id013PtjakartMutez = Id013PtjakartParameters.Id013PtjakartMutez = (function() {
    function Id013PtjakartMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartMutez.prototype._read = function() {
      this.id013PtjakartMutez = new N(this._io, this, this._root);
    }

    return Id013PtjakartMutez;
  })();

  var BytesDynUint30 = Id013PtjakartParameters.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var BootstrapContractsEntries = Id013PtjakartParameters.BootstrapContractsEntries = (function() {
    function BootstrapContractsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContractsEntries.prototype._read = function() {
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id013PtjakartParameters.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.amount = new Id013PtjakartMutez(this._io, this, this._root);
      this.script = new Id013PtjakartScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return BootstrapContractsEntries;
  })();

  var BootstrapContracts = Id013PtjakartParameters.BootstrapContracts = (function() {
    function BootstrapContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContracts.prototype._read = function() {
      this.bootstrapContractsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bootstrapContractsEntries.push(new BootstrapContractsEntries(this._io, this, this._root));
        i++;
      }
    }

    return BootstrapContracts;
  })();

  var BootstrapAccounts = Id013PtjakartParameters.BootstrapAccounts = (function() {
    function BootstrapAccounts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccounts.prototype._read = function() {
      this.bootstrapAccountsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bootstrapAccountsEntries.push(new BootstrapAccountsEntries(this._io, this, this._root));
        i++;
      }
    }

    return BootstrapAccounts;
  })();

  var Int31 = Id013PtjakartParameters.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id013PtjakartParameters.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var PublicKeyHash = Id013PtjakartParameters.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id013PtjakartParameters.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id013PtjakartParameters.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id013PtjakartParameters.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Z = Id013PtjakartParameters.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  return Id013PtjakartParameters;
})();
return Id013PtjakartParameters;
}));
