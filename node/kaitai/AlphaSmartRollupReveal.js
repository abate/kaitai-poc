// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaSmartRollupReveal = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.smart_rollup.reveal
 */

var AlphaSmartRollupReveal = (function() {
  AlphaSmartRollupReveal.AlphaSmartRollupRevealTag = Object.freeze({
    REVEAL_RAW_DATA: 0,
    REVEAL_METADATA: 1,
    REQUEST_DAL_PAGE: 2,
    REVEAL_DAL_PARAMETERS: 3,

    0: "REVEAL_RAW_DATA",
    1: "REVEAL_METADATA",
    2: "REQUEST_DAL_PAGE",
    3: "REVEAL_DAL_PARAMETERS",
  });

  AlphaSmartRollupReveal.InputHashTag = Object.freeze({
    REVEAL_DATA_HASH_V0: 0,

    0: "REVEAL_DATA_HASH_V0",
  });

  function AlphaSmartRollupReveal(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaSmartRollupReveal.prototype._read = function() {
    this.alphaSmartRollupRevealTag = this._io.readU1();
    if (this.alphaSmartRollupRevealTag == AlphaSmartRollupReveal.AlphaSmartRollupRevealTag.REVEAL_RAW_DATA) {
      this.revealRawData = new InputHash(this._io, this, this._root);
    }
    if (this.alphaSmartRollupRevealTag == AlphaSmartRollupReveal.AlphaSmartRollupRevealTag.REQUEST_DAL_PAGE) {
      this.requestDalPage = new PageId(this._io, this, this._root);
    }
  }

  var InputHash = AlphaSmartRollupReveal.InputHash = (function() {
    function InputHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InputHash.prototype._read = function() {
      this.inputHashTag = this._io.readU1();
      if (this.inputHashTag == AlphaSmartRollupReveal.InputHashTag.REVEAL_DATA_HASH_V0) {
        this.revealDataHashV0 = this._io.readBytes(32);
      }
    }

    return InputHash;
  })();

  var PageId = AlphaSmartRollupReveal.PageId = (function() {
    function PageId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PageId.prototype._read = function() {
      this.publishedLevel = this._io.readS4be();
      this.slotIndex = this._io.readU1();
      this.pageIndex = this._io.readS2be();
    }

    return PageId;
  })();

  return AlphaSmartRollupReveal;
})();
return AlphaSmartRollupReveal;
}));
