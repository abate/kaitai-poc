// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaSmartRollupAddress = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.smart_rollup.address
 */

var AlphaSmartRollupAddress = (function() {
  function AlphaSmartRollupAddress(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaSmartRollupAddress.prototype._read = function() {
    this.smartRollupAddress = this._io.readBytes(20);
  }

  return AlphaSmartRollupAddress;
})();
return AlphaSmartRollupAddress;
}));
