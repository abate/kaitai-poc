// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id011Pthangz2BlockHeaderProtocolData = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 011-PtHangz2.block_header.protocol_data
 */

var Id011Pthangz2BlockHeaderProtocolData = (function() {
  Id011Pthangz2BlockHeaderProtocolData.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id011Pthangz2BlockHeaderProtocolData(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2BlockHeaderProtocolData.prototype._read = function() {
    this.id011Pthangz2BlockHeaderAlphaSignedContents = new Id011Pthangz2BlockHeaderAlphaSignedContents(this._io, this, this._root);
  }

  var Id011Pthangz2BlockHeaderAlphaSignedContents = Id011Pthangz2BlockHeaderProtocolData.Id011Pthangz2BlockHeaderAlphaSignedContents = (function() {
    function Id011Pthangz2BlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2BlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id011Pthangz2BlockHeaderAlphaUnsignedContents = new Id011Pthangz2BlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id011Pthangz2BlockHeaderAlphaSignedContents;
  })();

  var Id011Pthangz2BlockHeaderAlphaUnsignedContents = Id011Pthangz2BlockHeaderProtocolData.Id011Pthangz2BlockHeaderAlphaUnsignedContents = (function() {
    function Id011Pthangz2BlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2BlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id011Pthangz2BlockHeaderProtocolData.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingEscapeVote = this._io.readU1();
    }

    return Id011Pthangz2BlockHeaderAlphaUnsignedContents;
  })();

  return Id011Pthangz2BlockHeaderProtocolData;
})();
return Id011Pthangz2BlockHeaderProtocolData;
}));
