// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id011Pthangz2DelegateFrozenBalance = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 011-PtHangz2.delegate.frozen_balance
 */

var Id011Pthangz2DelegateFrozenBalance = (function() {
  function Id011Pthangz2DelegateFrozenBalance(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2DelegateFrozenBalance.prototype._read = function() {
    this.deposits = new Id011Pthangz2Mutez(this._io, this, this._root);
    this.fees = new Id011Pthangz2Mutez(this._io, this, this._root);
    this.rewards = new Id011Pthangz2Mutez(this._io, this, this._root);
  }

  var Id011Pthangz2Mutez = Id011Pthangz2DelegateFrozenBalance.Id011Pthangz2Mutez = (function() {
    function Id011Pthangz2Mutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2Mutez.prototype._read = function() {
      this.id011Pthangz2Mutez = new N(this._io, this, this._root);
    }

    return Id011Pthangz2Mutez;
  })();

  var N = Id011Pthangz2DelegateFrozenBalance.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var NChunk = Id011Pthangz2DelegateFrozenBalance.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  return Id011Pthangz2DelegateFrozenBalance;
})();
return Id011Pthangz2DelegateFrozenBalance;
}));
