// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id011Pthangz2DelegateFrozenBalanceByCycles = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 011-PtHangz2.delegate.frozen_balance_by_cycles
 */

var Id011Pthangz2DelegateFrozenBalanceByCycles = (function() {
  function Id011Pthangz2DelegateFrozenBalanceByCycles(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2DelegateFrozenBalanceByCycles.prototype._read = function() {
    this.lenId011Pthangz2DelegateFrozenBalanceByCycles = this._io.readU4be();
    if (!(this.lenId011Pthangz2DelegateFrozenBalanceByCycles <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId011Pthangz2DelegateFrozenBalanceByCycles, this._io, "/seq/0");
    }
    this._raw_id011Pthangz2DelegateFrozenBalanceByCycles = this._io.readBytes(this.lenId011Pthangz2DelegateFrozenBalanceByCycles);
    var _io__raw_id011Pthangz2DelegateFrozenBalanceByCycles = new KaitaiStream(this._raw_id011Pthangz2DelegateFrozenBalanceByCycles);
    this.id011Pthangz2DelegateFrozenBalanceByCycles = new Id011Pthangz2DelegateFrozenBalanceByCycles(_io__raw_id011Pthangz2DelegateFrozenBalanceByCycles, this, this._root);
  }

  var N = Id011Pthangz2DelegateFrozenBalanceByCycles.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id011Pthangz2DelegateFrozenBalanceByCycles = Id011Pthangz2DelegateFrozenBalanceByCycles.Id011Pthangz2DelegateFrozenBalanceByCycles = (function() {
    function Id011Pthangz2DelegateFrozenBalanceByCycles(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2DelegateFrozenBalanceByCycles.prototype._read = function() {
      this.id011Pthangz2DelegateFrozenBalanceByCyclesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id011Pthangz2DelegateFrozenBalanceByCyclesEntries.push(new Id011Pthangz2DelegateFrozenBalanceByCyclesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id011Pthangz2DelegateFrozenBalanceByCycles;
  })();

  var Id011Pthangz2Mutez = Id011Pthangz2DelegateFrozenBalanceByCycles.Id011Pthangz2Mutez = (function() {
    function Id011Pthangz2Mutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2Mutez.prototype._read = function() {
      this.id011Pthangz2Mutez = new N(this._io, this, this._root);
    }

    return Id011Pthangz2Mutez;
  })();

  var Id011Pthangz2DelegateFrozenBalanceByCyclesEntries = Id011Pthangz2DelegateFrozenBalanceByCycles.Id011Pthangz2DelegateFrozenBalanceByCyclesEntries = (function() {
    function Id011Pthangz2DelegateFrozenBalanceByCyclesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2DelegateFrozenBalanceByCyclesEntries.prototype._read = function() {
      this.cycle = this._io.readS4be();
      this.deposits = new Id011Pthangz2Mutez(this._io, this, this._root);
      this.fees = new Id011Pthangz2Mutez(this._io, this, this._root);
      this.rewards = new Id011Pthangz2Mutez(this._io, this, this._root);
    }

    return Id011Pthangz2DelegateFrozenBalanceByCyclesEntries;
  })();

  var NChunk = Id011Pthangz2DelegateFrozenBalanceByCycles.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  return Id011Pthangz2DelegateFrozenBalanceByCycles;
})();
return Id011Pthangz2DelegateFrozenBalanceByCycles;
}));
