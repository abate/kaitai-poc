// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaSmartRollupWhitelist = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.smart_rollup.whitelist
 */

var Id019PtparisaSmartRollupWhitelist = (function() {
  Id019PtparisaSmartRollupWhitelist.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function Id019PtparisaSmartRollupWhitelist(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaSmartRollupWhitelist.prototype._read = function() {
    this.lenId019PtparisaSmartRollupWhitelist = this._io.readU4be();
    if (!(this.lenId019PtparisaSmartRollupWhitelist <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId019PtparisaSmartRollupWhitelist, this._io, "/seq/0");
    }
    this._raw_id019PtparisaSmartRollupWhitelist = this._io.readBytes(this.lenId019PtparisaSmartRollupWhitelist);
    var _io__raw_id019PtparisaSmartRollupWhitelist = new KaitaiStream(this._raw_id019PtparisaSmartRollupWhitelist);
    this.id019PtparisaSmartRollupWhitelist = new Id019PtparisaSmartRollupWhitelist(_io__raw_id019PtparisaSmartRollupWhitelist, this, this._root);
  }

  var Id019PtparisaSmartRollupWhitelist = Id019PtparisaSmartRollupWhitelist.Id019PtparisaSmartRollupWhitelist = (function() {
    function Id019PtparisaSmartRollupWhitelist(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaSmartRollupWhitelist.prototype._read = function() {
      this.id019PtparisaSmartRollupWhitelistEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id019PtparisaSmartRollupWhitelistEntries.push(new Id019PtparisaSmartRollupWhitelistEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id019PtparisaSmartRollupWhitelist;
  })();

  var Id019PtparisaSmartRollupWhitelistEntries = Id019PtparisaSmartRollupWhitelist.Id019PtparisaSmartRollupWhitelistEntries = (function() {
    function Id019PtparisaSmartRollupWhitelistEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaSmartRollupWhitelistEntries.prototype._read = function() {
      this.signaturePublicKeyHash = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id019PtparisaSmartRollupWhitelistEntries;
  })();

  var PublicKeyHash = Id019PtparisaSmartRollupWhitelist.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id019PtparisaSmartRollupWhitelist.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaSmartRollupWhitelist.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaSmartRollupWhitelist.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaSmartRollupWhitelist.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id019PtparisaSmartRollupWhitelist;
})();
return Id019PtparisaSmartRollupWhitelist;
}));
