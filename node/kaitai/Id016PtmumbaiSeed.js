// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id016PtmumbaiSeed = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 016-PtMumbai.seed
 */

var Id016PtmumbaiSeed = (function() {
  function Id016PtmumbaiSeed(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiSeed.prototype._read = function() {
    this.id016PtmumbaiSeed = this._io.readBytes(32);
  }

  return Id016PtmumbaiSeed;
})();
return Id016PtmumbaiSeed;
}));
