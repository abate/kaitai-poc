// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id017PtnairobConstantsParametric = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 017-PtNairob.constants.parametric
 */

var Id017PtnairobConstantsParametric = (function() {
  Id017PtnairobConstantsParametric.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id017PtnairobConstantsParametric.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function Id017PtnairobConstantsParametric(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id017PtnairobConstantsParametric.prototype._read = function() {
    this.preservedCycles = this._io.readU1();
    this.blocksPerCycle = this._io.readS4be();
    this.blocksPerCommitment = this._io.readS4be();
    this.nonceRevelationThreshold = this._io.readS4be();
    this.blocksPerStakeSnapshot = this._io.readS4be();
    this.cyclesPerVotingPeriod = this._io.readS4be();
    this.hardGasLimitPerOperation = new Z(this._io, this, this._root);
    this.hardGasLimitPerBlock = new Z(this._io, this, this._root);
    this.proofOfWorkThreshold = this._io.readS8be();
    this.minimalStake = new Id017PtnairobMutez(this._io, this, this._root);
    this.vdfDifficulty = this._io.readS8be();
    this.seedNonceRevelationTip = new Id017PtnairobMutez(this._io, this, this._root);
    this.originationSize = new Int31(this._io, this, this._root);
    this.bakingRewardFixedPortion = new Id017PtnairobMutez(this._io, this, this._root);
    this.bakingRewardBonusPerSlot = new Id017PtnairobMutez(this._io, this, this._root);
    this.endorsingRewardPerSlot = new Id017PtnairobMutez(this._io, this, this._root);
    this.costPerByte = new Id017PtnairobMutez(this._io, this, this._root);
    this.hardStorageLimitPerOperation = new Z(this._io, this, this._root);
    this.quorumMin = this._io.readS4be();
    this.quorumMax = this._io.readS4be();
    this.minProposalQuorum = this._io.readS4be();
    this.liquidityBakingSubsidy = new Id017PtnairobMutez(this._io, this, this._root);
    this.liquidityBakingToggleEmaThreshold = this._io.readS4be();
    this.maxOperationsTimeToLive = this._io.readS2be();
    this.minimalBlockDelay = this._io.readS8be();
    this.delayIncrementPerRound = this._io.readS8be();
    this.consensusCommitteeSize = new Int31(this._io, this, this._root);
    this.consensusThreshold = new Int31(this._io, this, this._root);
    this.minimalParticipationRatio = new MinimalParticipationRatio(this._io, this, this._root);
    this.maxSlashingPeriod = new Int31(this._io, this, this._root);
    this.frozenDepositsPercentage = new Int31(this._io, this, this._root);
    this.doubleBakingPunishment = new Id017PtnairobMutez(this._io, this, this._root);
    this.ratioOfFrozenDepositsSlashedPerDoubleEndorsement = new RatioOfFrozenDepositsSlashedPerDoubleEndorsement(this._io, this, this._root);
    this.testnetDictatorTag = this._io.readU1();
    if (this.testnetDictatorTag == Id017PtnairobConstantsParametric.Bool.TRUE) {
      this.testnetDictator = new PublicKeyHash(this._io, this, this._root);
    }
    this.initialSeedTag = this._io.readU1();
    if (this.initialSeedTag == Id017PtnairobConstantsParametric.Bool.TRUE) {
      this.initialSeed = this._io.readBytes(32);
    }
    this.cacheScriptSize = new Int31(this._io, this, this._root);
    this.cacheStakeDistributionCycles = this._io.readS1();
    this.cacheSamplerStateCycles = this._io.readS1();
    this.txRollupEnable = this._io.readU1();
    this.txRollupOriginationSize = new Int31(this._io, this, this._root);
    this.txRollupHardSizeLimitPerInbox = new Int31(this._io, this, this._root);
    this.txRollupHardSizeLimitPerMessage = new Int31(this._io, this, this._root);
    this.txRollupMaxWithdrawalsPerBatch = new Int31(this._io, this, this._root);
    this.txRollupCommitmentBond = new Id017PtnairobMutez(this._io, this, this._root);
    this.txRollupFinalityPeriod = new Int31(this._io, this, this._root);
    this.txRollupWithdrawPeriod = new Int31(this._io, this, this._root);
    this.txRollupMaxInboxesCount = new Int31(this._io, this, this._root);
    this.txRollupMaxMessagesPerInbox = new Int31(this._io, this, this._root);
    this.txRollupMaxCommitmentsCount = new Int31(this._io, this, this._root);
    this.txRollupCostPerByteEmaFactor = new Int31(this._io, this, this._root);
    this.txRollupMaxTicketPayloadSize = new Int31(this._io, this, this._root);
    this.txRollupRejectionMaxProofSize = new Int31(this._io, this, this._root);
    this.txRollupSunsetLevel = this._io.readS4be();
    this.dalParametric = new DalParametric(this._io, this, this._root);
    this.smartRollupEnable = this._io.readU1();
    this.smartRollupArithPvmEnable = this._io.readU1();
    this.smartRollupOriginationSize = new Int31(this._io, this, this._root);
    this.smartRollupChallengeWindowInBlocks = new Int31(this._io, this, this._root);
    this.smartRollupStakeAmount = new Id017PtnairobMutez(this._io, this, this._root);
    this.smartRollupCommitmentPeriodInBlocks = new Int31(this._io, this, this._root);
    this.smartRollupMaxLookaheadInBlocks = this._io.readS4be();
    this.smartRollupMaxActiveOutboxLevels = this._io.readS4be();
    this.smartRollupMaxOutboxMessagesPerLevel = new Int31(this._io, this, this._root);
    this.smartRollupNumberOfSectionsInDissection = this._io.readU1();
    this.smartRollupTimeoutPeriodInBlocks = new Int31(this._io, this, this._root);
    this.smartRollupMaxNumberOfCementedCommitments = new Int31(this._io, this, this._root);
    this.smartRollupMaxNumberOfParallelGames = new Int31(this._io, this, this._root);
    this.zkRollupEnable = this._io.readU1();
    this.zkRollupOriginationSize = new Int31(this._io, this, this._root);
    this.zkRollupMinPendingToProcess = new Int31(this._io, this, this._root);
  }

  var RatioOfFrozenDepositsSlashedPerDoubleEndorsement = Id017PtnairobConstantsParametric.RatioOfFrozenDepositsSlashedPerDoubleEndorsement = (function() {
    function RatioOfFrozenDepositsSlashedPerDoubleEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RatioOfFrozenDepositsSlashedPerDoubleEndorsement.prototype._read = function() {
      this.numerator = this._io.readU2be();
      this.denominator = this._io.readU2be();
    }

    return RatioOfFrozenDepositsSlashedPerDoubleEndorsement;
  })();

  var DalParametric = Id017PtnairobConstantsParametric.DalParametric = (function() {
    function DalParametric(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalParametric.prototype._read = function() {
      this.featureEnable = this._io.readU1();
      this.numberOfSlots = this._io.readS2be();
      this.attestationLag = this._io.readS2be();
      this.attestationThreshold = this._io.readS2be();
      this.blocksPerEpoch = this._io.readS4be();
      this.redundancyFactor = this._io.readU1();
      this.pageSize = this._io.readU2be();
      this.slotSize = new Int31(this._io, this, this._root);
      this.numberOfShards = this._io.readU2be();
    }

    return DalParametric;
  })();

  var N = Id017PtnairobConstantsParametric.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var MinimalParticipationRatio = Id017PtnairobConstantsParametric.MinimalParticipationRatio = (function() {
    function MinimalParticipationRatio(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MinimalParticipationRatio.prototype._read = function() {
      this.numerator = this._io.readU2be();
      this.denominator = this._io.readU2be();
    }

    return MinimalParticipationRatio;
  })();

  var Int31 = Id017PtnairobConstantsParametric.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id017PtnairobConstantsParametric.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Id017PtnairobMutez = Id017PtnairobConstantsParametric.Id017PtnairobMutez = (function() {
    function Id017PtnairobMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobMutez.prototype._read = function() {
      this.id017PtnairobMutez = new N(this._io, this, this._root);
    }

    return Id017PtnairobMutez;
  })();

  var PublicKeyHash = Id017PtnairobConstantsParametric.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id017PtnairobConstantsParametric.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id017PtnairobConstantsParametric.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id017PtnairobConstantsParametric.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id017PtnairobConstantsParametric.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Z = Id017PtnairobConstantsParametric.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  /**
   * A Ed25519, Secp256k1, P256, or BLS public key hash
   */

  return Id017PtnairobConstantsParametric;
})();
return Id017PtnairobConstantsParametric;
}));
