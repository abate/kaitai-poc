// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id008Ptedo2zkConstantsFixed = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 008-PtEdo2Zk.constants.fixed
 */

var Id008Ptedo2zkConstantsFixed = (function() {
  function Id008Ptedo2zkConstantsFixed(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkConstantsFixed.prototype._read = function() {
    this.proofOfWorkNonceSize = this._io.readU1();
    this.nonceLength = this._io.readU1();
    this.maxAnonOpsPerBlock = this._io.readU1();
    this.maxOperationDataLength = new Int31(this._io, this, this._root);
    this.maxProposalsPerDelegate = this._io.readU1();
  }

  var Int31 = Id008Ptedo2zkConstantsFixed.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  return Id008Ptedo2zkConstantsFixed;
})();
return Id008Ptedo2zkConstantsFixed;
}));
