// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id009PsflorenBlockHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 009-PsFLoren.block_header
 */

var Id009PsflorenBlockHeader = (function() {
  Id009PsflorenBlockHeader.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id009PsflorenBlockHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id009PsflorenBlockHeader.prototype._read = function() {
    this.id009PsflorenBlockHeaderAlphaFullHeader = new Id009PsflorenBlockHeaderAlphaFullHeader(this._io, this, this._root);
  }

  var Id009PsflorenBlockHeaderAlphaFullHeader = Id009PsflorenBlockHeader.Id009PsflorenBlockHeaderAlphaFullHeader = (function() {
    function Id009PsflorenBlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenBlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id009PsflorenBlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id009PsflorenBlockHeaderAlphaSignedContents = new Id009PsflorenBlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id009PsflorenBlockHeaderAlphaFullHeader;
  })();

  var Id009PsflorenBlockHeaderAlphaSignedContents = Id009PsflorenBlockHeader.Id009PsflorenBlockHeaderAlphaSignedContents = (function() {
    function Id009PsflorenBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id009PsflorenBlockHeaderAlphaUnsignedContents = new Id009PsflorenBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id009PsflorenBlockHeaderAlphaSignedContents;
  })();

  var Id009PsflorenBlockHeaderAlphaUnsignedContents = Id009PsflorenBlockHeader.Id009PsflorenBlockHeaderAlphaUnsignedContents = (function() {
    function Id009PsflorenBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id009PsflorenBlockHeader.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
    }

    return Id009PsflorenBlockHeaderAlphaUnsignedContents;
  })();

  return Id009PsflorenBlockHeader;
})();
return Id009PsflorenBlockHeader;
}));
