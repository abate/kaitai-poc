// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './SaplingTransactionNullifier', './SaplingTransactionCommitmentValue'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./SaplingTransactionNullifier'), require('./SaplingTransactionCommitmentValue'));
  } else {
    root.SaplingTransactionInput = factory(root.KaitaiStream, root.SaplingTransactionNullifier, root.SaplingTransactionCommitmentValue);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, SaplingTransactionNullifier, SaplingTransactionCommitmentValue) {
/**
 * Encoding id: sapling.transaction.input
 * Description: Input of a transaction
 */

var SaplingTransactionInput = (function() {
  function SaplingTransactionInput(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  SaplingTransactionInput.prototype._read = function() {
    this.cv = new SaplingTransactionCommitmentValue(this._io, this, null);
    this.nf = new SaplingTransactionNullifier(this._io, this, null);
    this.rk = this._io.readBytes(32);
    this.proofI = this._io.readBytes(192);
    this.signature = this._io.readBytes(64);
  }

  return SaplingTransactionInput;
})();
return SaplingTransactionInput;
}));
