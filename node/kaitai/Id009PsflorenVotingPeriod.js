// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id009PsflorenVotingPeriod = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 009-PsFLoren.voting_period
 */

var Id009PsflorenVotingPeriod = (function() {
  Id009PsflorenVotingPeriod.KindTag = Object.freeze({
    PROPOSAL: 0,
    EXPLORATION: 1,
    COOLDOWN: 2,
    PROMOTION: 3,
    ADOPTION: 4,

    0: "PROPOSAL",
    1: "EXPLORATION",
    2: "COOLDOWN",
    3: "PROMOTION",
    4: "ADOPTION",
  });

  function Id009PsflorenVotingPeriod(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id009PsflorenVotingPeriod.prototype._read = function() {
    this.index = this._io.readS4be();
    this.kind = this._io.readU1();
    this.startPosition = this._io.readS4be();
  }

  /**
   * The voting period's index. Starts at 0 with the first block of protocol alpha.
   */

  return Id009PsflorenVotingPeriod;
})();
return Id009PsflorenVotingPeriod;
}));
