// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaSmartRollupGame = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.smart_rollup.game
 */

var Id019PtparisaSmartRollupGame = (function() {
  Id019PtparisaSmartRollupGame.DalSnapshotTag = Object.freeze({
    DAL_SKIP_LIST_LEGACY: 0,
    DAL_SKIP_LIST: 1,

    0: "DAL_SKIP_LIST_LEGACY",
    1: "DAL_SKIP_LIST",
  });

  Id019PtparisaSmartRollupGame.ContentTag = Object.freeze({
    UNATTESTED: 0,
    ATTESTED: 1,

    0: "UNATTESTED",
    1: "ATTESTED",
  });

  Id019PtparisaSmartRollupGame.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id019PtparisaSmartRollupGame.GameStateTag = Object.freeze({
    DISSECTING: 0,
    FINAL_MOVE: 1,

    0: "DISSECTING",
    1: "FINAL_MOVE",
  });

  Id019PtparisaSmartRollupGame.AttestedTag = Object.freeze({
    V0: 0,

    0: "V0",
  });

  Id019PtparisaSmartRollupGame.TurnTag = Object.freeze({
    ALICE: 0,
    BOB: 1,

    0: "ALICE",
    1: "BOB",
  });

  function Id019PtparisaSmartRollupGame(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaSmartRollupGame.prototype._read = function() {
    this.turn = this._io.readU1();
    this.inboxSnapshot = new InboxSnapshot(this._io, this, this._root);
    this.dalSnapshot = new DalSnapshot(this._io, this, this._root);
    this.startLevel = this._io.readS4be();
    this.inboxLevel = this._io.readS4be();
    this.gameState = new GameState(this._io, this, this._root);
  }

  var BackPointers0 = Id019PtparisaSmartRollupGame.BackPointers0 = (function() {
    function BackPointers0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BackPointers0.prototype._read = function() {
      this.lenBackPointers = this._io.readU4be();
      if (!(this.lenBackPointers <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBackPointers, this._io, "/types/back_pointers_0/seq/0");
      }
      this._raw_backPointers = this._io.readBytes(this.lenBackPointers);
      var _io__raw_backPointers = new KaitaiStream(this._raw_backPointers);
      this.backPointers = new BackPointers(_io__raw_backPointers, this, this._root);
    }

    return BackPointers0;
  })();

  var N = Id019PtparisaSmartRollupGame.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Attested = Id019PtparisaSmartRollupGame.Attested = (function() {
    function Attested(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Attested.prototype._read = function() {
      this.attestedTag = this._io.readU1();
      if (this.attestedTag == Id019PtparisaSmartRollupGame.AttestedTag.V0) {
        this.v0 = new V0(this._io, this, this._root);
      }
    }

    return Attested;
  })();

  var BackPointersEntries0 = Id019PtparisaSmartRollupGame.BackPointersEntries0 = (function() {
    function BackPointersEntries0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BackPointersEntries0.prototype._read = function() {
      this.dalSkipListPointer = this._io.readBytes(32);
    }

    return BackPointersEntries0;
  })();

  var InboxSnapshot = Id019PtparisaSmartRollupGame.InboxSnapshot = (function() {
    function InboxSnapshot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InboxSnapshot.prototype._read = function() {
      this.index = new N(this._io, this, this._root);
      this.content = new Content(this._io, this, this._root);
      this.backPointers = new BackPointers0(this._io, this, this._root);
    }

    return InboxSnapshot;
  })();

  var BackPointers2 = Id019PtparisaSmartRollupGame.BackPointers2 = (function() {
    function BackPointers2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BackPointers2.prototype._read = function() {
      this.lenBackPointers = this._io.readU4be();
      if (!(this.lenBackPointers <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBackPointers, this._io, "/types/back_pointers_2/seq/0");
      }
      this._raw_backPointers = this._io.readBytes(this.lenBackPointers);
      var _io__raw_backPointers = new KaitaiStream(this._raw_backPointers);
      this.backPointers = new BackPointers1(_io__raw_backPointers, this, this._root);
    }

    return BackPointers2;
  })();

  var GameState = Id019PtparisaSmartRollupGame.GameState = (function() {
    function GameState(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    GameState.prototype._read = function() {
      this.gameStateTag = this._io.readU1();
      if (this.gameStateTag == Id019PtparisaSmartRollupGame.GameStateTag.DISSECTING) {
        this.dissecting = new Dissecting(this._io, this, this._root);
      }
      if (this.gameStateTag == Id019PtparisaSmartRollupGame.GameStateTag.FINAL_MOVE) {
        this.finalMove = new FinalMove(this._io, this, this._root);
      }
    }

    return GameState;
  })();

  var AgreedStartChunk = Id019PtparisaSmartRollupGame.AgreedStartChunk = (function() {
    function AgreedStartChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AgreedStartChunk.prototype._read = function() {
      this.stateTag = this._io.readU1();
      if (this.stateTag == Id019PtparisaSmartRollupGame.Bool.TRUE) {
        this.state = this._io.readBytes(32);
      }
      this.tick = new N(this._io, this, this._root);
    }

    return AgreedStartChunk;
  })();

  var Dissecting = Id019PtparisaSmartRollupGame.Dissecting = (function() {
    function Dissecting(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Dissecting.prototype._read = function() {
      this.dissection = new Dissection0(this._io, this, this._root);
      this.defaultNumberOfSections = this._io.readU1();
    }

    return Dissecting;
  })();

  var RefutedStopChunk = Id019PtparisaSmartRollupGame.RefutedStopChunk = (function() {
    function RefutedStopChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RefutedStopChunk.prototype._read = function() {
      this.stateTag = this._io.readU1();
      if (this.stateTag == Id019PtparisaSmartRollupGame.Bool.TRUE) {
        this.state = this._io.readBytes(32);
      }
      this.tick = new N(this._io, this, this._root);
    }

    return RefutedStopChunk;
  })();

  var DalSnapshot = Id019PtparisaSmartRollupGame.DalSnapshot = (function() {
    function DalSnapshot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalSnapshot.prototype._read = function() {
      this.dalSnapshotTag = this._io.readU1();
      if (this.dalSnapshotTag == Id019PtparisaSmartRollupGame.DalSnapshotTag.DAL_SKIP_LIST_LEGACY) {
        this.dalSkipListLegacy = this._io.readBytes(57);
      }
      if (this.dalSnapshotTag == Id019PtparisaSmartRollupGame.DalSnapshotTag.DAL_SKIP_LIST) {
        this.dalSkipList = new SkipList(this._io, this, this._root);
      }
    }

    return DalSnapshot;
  })();

  var Dissection0 = Id019PtparisaSmartRollupGame.Dissection0 = (function() {
    function Dissection0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Dissection0.prototype._read = function() {
      this.lenDissection = this._io.readU4be();
      if (!(this.lenDissection <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenDissection, this._io, "/types/dissection_0/seq/0");
      }
      this._raw_dissection = this._io.readBytes(this.lenDissection);
      var _io__raw_dissection = new KaitaiStream(this._raw_dissection);
      this.dissection = new Dissection(_io__raw_dissection, this, this._root);
    }

    return Dissection0;
  })();

  var V0 = Id019PtparisaSmartRollupGame.V0 = (function() {
    function V0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    V0.prototype._read = function() {
      this.level = this._io.readS4be();
      this.index = this._io.readU1();
      this.commitment = this._io.readBytes(48);
    }

    return V0;
  })();

  var Content = Id019PtparisaSmartRollupGame.Content = (function() {
    function Content(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Content.prototype._read = function() {
      this.hash = this._io.readBytes(32);
      this.level = this._io.readS4be();
    }

    return Content;
  })();

  var BackPointers = Id019PtparisaSmartRollupGame.BackPointers = (function() {
    function BackPointers(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BackPointers.prototype._read = function() {
      this.backPointersEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.backPointersEntries.push(new BackPointersEntries(this._io, this, this._root));
        i++;
      }
    }

    return BackPointers;
  })();

  var BackPointers1 = Id019PtparisaSmartRollupGame.BackPointers1 = (function() {
    function BackPointers1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BackPointers1.prototype._read = function() {
      this.backPointersEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.backPointersEntries.push(new BackPointersEntries0(this._io, this, this._root));
        i++;
      }
    }

    return BackPointers1;
  })();

  var Dissection = Id019PtparisaSmartRollupGame.Dissection = (function() {
    function Dissection(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Dissection.prototype._read = function() {
      this.dissectionEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.dissectionEntries.push(new DissectionEntries(this._io, this, this._root));
        i++;
      }
    }

    return Dissection;
  })();

  var Content0 = Id019PtparisaSmartRollupGame.Content0 = (function() {
    function Content0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Content0.prototype._read = function() {
      this.contentTag = this._io.readU1();
      if (this.contentTag == Id019PtparisaSmartRollupGame.ContentTag.UNATTESTED) {
        this.unattested = new Unattested(this._io, this, this._root);
      }
      if (this.contentTag == Id019PtparisaSmartRollupGame.ContentTag.ATTESTED) {
        this.attested = new Attested(this._io, this, this._root);
      }
    }

    return Content0;
  })();

  var Unattested = Id019PtparisaSmartRollupGame.Unattested = (function() {
    function Unattested(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Unattested.prototype._read = function() {
      this.level = this._io.readS4be();
      this.index = this._io.readU1();
    }

    return Unattested;
  })();

  var NChunk = Id019PtparisaSmartRollupGame.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var SkipList = Id019PtparisaSmartRollupGame.SkipList = (function() {
    function SkipList(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SkipList.prototype._read = function() {
      this.index = new N(this._io, this, this._root);
      this.content = new Content0(this._io, this, this._root);
      this.backPointers = new BackPointers2(this._io, this, this._root);
    }

    return SkipList;
  })();

  var BackPointersEntries = Id019PtparisaSmartRollupGame.BackPointersEntries = (function() {
    function BackPointersEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BackPointersEntries.prototype._read = function() {
      this.smartRollupInboxHash = this._io.readBytes(32);
    }

    return BackPointersEntries;
  })();

  var DissectionEntries = Id019PtparisaSmartRollupGame.DissectionEntries = (function() {
    function DissectionEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DissectionEntries.prototype._read = function() {
      this.stateTag = this._io.readU1();
      if (this.stateTag == Id019PtparisaSmartRollupGame.Bool.TRUE) {
        this.state = this._io.readBytes(32);
      }
      this.tick = new N(this._io, this, this._root);
    }

    return DissectionEntries;
  })();

  var FinalMove = Id019PtparisaSmartRollupGame.FinalMove = (function() {
    function FinalMove(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    FinalMove.prototype._read = function() {
      this.agreedStartChunk = new AgreedStartChunk(this._io, this, this._root);
      this.refutedStopChunk = new RefutedStopChunk(this._io, this, this._root);
    }

    return FinalMove;
  })();

  return Id019PtparisaSmartRollupGame;
})();
return Id019PtparisaSmartRollupGame;
}));
