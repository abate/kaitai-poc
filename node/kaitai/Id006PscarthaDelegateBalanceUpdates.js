// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id006PscarthaDelegateBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 006-PsCARTHA.delegate.balance_updates
 */

var Id006PscarthaDelegateBalanceUpdates = (function() {
  Id006PscarthaDelegateBalanceUpdates.Id006PscarthaContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceTag = Object.freeze({
    CONTRACT: 0,
    REWARDS: 1,
    FEES: 2,
    DEPOSITS: 3,

    0: "CONTRACT",
    1: "REWARDS",
    2: "FEES",
    3: "DEPOSITS",
  });

  Id006PscarthaDelegateBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id006PscarthaDelegateBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaDelegateBalanceUpdates.prototype._read = function() {
    this.id006PscarthaOperationMetadataAlphaBalanceUpdates = new Id006PscarthaOperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Id006PscarthaOperationMetadataAlphaBalanceUpdatesEntries = Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id006PscarthaOperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaOperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id006PscarthaOperationMetadataAlphaBalance = new Id006PscarthaOperationMetadataAlphaBalance(this._io, this, this._root);
      this.id006PscarthaOperationMetadataAlphaBalanceUpdate = new Id006PscarthaOperationMetadataAlphaBalanceUpdate(this._io, this, this._root);
    }

    return Id006PscarthaOperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var Originated = Id006PscarthaDelegateBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Id006PscarthaContractId = Id006PscarthaDelegateBalanceUpdates.Id006PscarthaContractId = (function() {
    function Id006PscarthaContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaContractId.prototype._read = function() {
      this.id006PscarthaContractIdTag = this._io.readU1();
      if (this.id006PscarthaContractIdTag == Id006PscarthaDelegateBalanceUpdates.Id006PscarthaContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id006PscarthaContractIdTag == Id006PscarthaDelegateBalanceUpdates.Id006PscarthaContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id006PscarthaContractId;
  })();

  var Id006PscarthaOperationMetadataAlphaBalanceUpdates0 = Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id006PscarthaOperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaOperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId006PscarthaOperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId006PscarthaOperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId006PscarthaOperationMetadataAlphaBalanceUpdates, this._io, "/types/id_006__pscartha__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id006PscarthaOperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId006PscarthaOperationMetadataAlphaBalanceUpdates);
      var _io__raw_id006PscarthaOperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id006PscarthaOperationMetadataAlphaBalanceUpdates);
      this.id006PscarthaOperationMetadataAlphaBalanceUpdates = new Id006PscarthaOperationMetadataAlphaBalanceUpdates(_io__raw_id006PscarthaOperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id006PscarthaOperationMetadataAlphaBalanceUpdates0;
  })();

  var Id006PscarthaOperationMetadataAlphaBalanceUpdate = Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceUpdate = (function() {
    function Id006PscarthaOperationMetadataAlphaBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaOperationMetadataAlphaBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id006PscarthaOperationMetadataAlphaBalanceUpdate;
  })();

  var Rewards = Id006PscarthaDelegateBalanceUpdates.Rewards = (function() {
    function Rewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Rewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Rewards;
  })();

  var Id006PscarthaOperationMetadataAlphaBalance = Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalance = (function() {
    function Id006PscarthaOperationMetadataAlphaBalance(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaOperationMetadataAlphaBalance.prototype._read = function() {
      this.id006PscarthaOperationMetadataAlphaBalanceTag = this._io.readU1();
      if (this.id006PscarthaOperationMetadataAlphaBalanceTag == Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceTag.CONTRACT) {
        this.contract = new Id006PscarthaContractId(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationMetadataAlphaBalanceTag == Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceTag.REWARDS) {
        this.rewards = new Rewards(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationMetadataAlphaBalanceTag == Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceTag.FEES) {
        this.fees = new Fees(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationMetadataAlphaBalanceTag == Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceTag.DEPOSITS) {
        this.deposits = new Deposits(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Id006PscarthaOperationMetadataAlphaBalance;
  })();

  var Deposits = Id006PscarthaDelegateBalanceUpdates.Deposits = (function() {
    function Deposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposits.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Deposits;
  })();

  var Fees = Id006PscarthaDelegateBalanceUpdates.Fees = (function() {
    function Fees(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Fees.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Fees;
  })();

  var Id006PscarthaOperationMetadataAlphaBalanceUpdates = Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceUpdates = (function() {
    function Id006PscarthaOperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaOperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id006PscarthaOperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id006PscarthaOperationMetadataAlphaBalanceUpdatesEntries.push(new Id006PscarthaOperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id006PscarthaOperationMetadataAlphaBalanceUpdates;
  })();

  var PublicKeyHash = Id006PscarthaDelegateBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id006PscarthaDelegateBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id006PscarthaDelegateBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id006PscarthaDelegateBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id006PscarthaDelegateBalanceUpdates;
})();
return Id006PscarthaDelegateBalanceUpdates;
}));
