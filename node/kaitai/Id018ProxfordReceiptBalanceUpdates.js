// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id018ProxfordReceiptBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 018-Proxford.receipt.balance_updates
 */

var Id018ProxfordReceiptBalanceUpdates = (function() {
  Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag = Object.freeze({
    CONTRACT: 0,
    BLOCK_FEES: 2,
    DEPOSITS: 4,
    NONCE_REVELATION_REWARDS: 5,
    ATTESTING_REWARDS: 7,
    BAKING_REWARDS: 8,
    BAKING_BONUSES: 9,
    STORAGE_FEES: 11,
    DOUBLE_SIGNING_PUNISHMENTS: 12,
    LOST_ATTESTING_REWARDS: 13,
    LIQUIDITY_BAKING_SUBSIDIES: 14,
    BURNED: 15,
    COMMITMENTS: 16,
    BOOTSTRAP: 17,
    INVOICE: 18,
    INITIAL_COMMITMENTS: 19,
    MINTED: 20,
    FROZEN_BONDS: 21,
    SMART_ROLLUP_REFUTATION_PUNISHMENTS: 24,
    SMART_ROLLUP_REFUTATION_REWARDS: 25,
    UNSTAKED_DEPOSITS: 26,
    STAKING_DELEGATOR_NUMERATOR: 27,
    STAKING_DELEGATE_DENOMINATOR: 28,

    0: "CONTRACT",
    2: "BLOCK_FEES",
    4: "DEPOSITS",
    5: "NONCE_REVELATION_REWARDS",
    7: "ATTESTING_REWARDS",
    8: "BAKING_REWARDS",
    9: "BAKING_BONUSES",
    11: "STORAGE_FEES",
    12: "DOUBLE_SIGNING_PUNISHMENTS",
    13: "LOST_ATTESTING_REWARDS",
    14: "LIQUIDITY_BAKING_SUBSIDIES",
    15: "BURNED",
    16: "COMMITMENTS",
    17: "BOOTSTRAP",
    18: "INVOICE",
    19: "INITIAL_COMMITMENTS",
    20: "MINTED",
    21: "FROZEN_BONDS",
    24: "SMART_ROLLUP_REFUTATION_PUNISHMENTS",
    25: "SMART_ROLLUP_REFUTATION_REWARDS",
    26: "UNSTAKED_DEPOSITS",
    27: "STAKING_DELEGATOR_NUMERATOR",
    28: "STAKING_DELEGATE_DENOMINATOR",
  });

  Id018ProxfordReceiptBalanceUpdates.Id018ProxfordStakerTag = Object.freeze({
    SINGLE: 0,
    SHARED: 1,

    0: "SINGLE",
    1: "SHARED",
  });

  Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaUpdateOriginTag = Object.freeze({
    BLOCK_APPLICATION: 0,
    PROTOCOL_MIGRATION: 1,
    SUBSIDY: 2,
    SIMULATION: 3,
    DELAYED_OPERATION: 4,

    0: "BLOCK_APPLICATION",
    1: "PROTOCOL_MIGRATION",
    2: "SUBSIDY",
    3: "SIMULATION",
    4: "DELAYED_OPERATION",
  });

  Id018ProxfordReceiptBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  Id018ProxfordReceiptBalanceUpdates.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id018ProxfordReceiptBalanceUpdates.Id018ProxfordBondIdTag = Object.freeze({
    SMART_ROLLUP_BOND_ID: 1,

    1: "SMART_ROLLUP_BOND_ID",
  });

  Id018ProxfordReceiptBalanceUpdates.Id018ProxfordFrozenStakerTag = Object.freeze({
    SINGLE: 0,
    SHARED: 1,
    BAKER: 2,

    0: "SINGLE",
    1: "SHARED",
    2: "BAKER",
  });

  Id018ProxfordReceiptBalanceUpdates.Id018ProxfordContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  function Id018ProxfordReceiptBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id018ProxfordReceiptBalanceUpdates.prototype._read = function() {
    this.id018ProxfordOperationMetadataAlphaBalanceUpdates = new Id018ProxfordOperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Commitments = Id018ProxfordReceiptBalanceUpdates.Commitments = (function() {
    function Commitments(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments.prototype._read = function() {
      this.committer = this._io.readBytes(20);
      this.id018ProxfordOperationMetadataAlphaTezBalanceUpdate = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    return Commitments;
  })();

  var Id018ProxfordOperationMetadataAlphaBalanceUpdates0 = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id018ProxfordOperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordOperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId018ProxfordOperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId018ProxfordOperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId018ProxfordOperationMetadataAlphaBalanceUpdates, this._io, "/types/id_018__proxford__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id018ProxfordOperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId018ProxfordOperationMetadataAlphaBalanceUpdates);
      var _io__raw_id018ProxfordOperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id018ProxfordOperationMetadataAlphaBalanceUpdates);
      this.id018ProxfordOperationMetadataAlphaBalanceUpdates = new Id018ProxfordOperationMetadataAlphaBalanceUpdates(_io__raw_id018ProxfordOperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id018ProxfordOperationMetadataAlphaBalanceUpdates0;
  })();

  var FrozenBonds = Id018ProxfordReceiptBalanceUpdates.FrozenBonds = (function() {
    function FrozenBonds(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    FrozenBonds.prototype._read = function() {
      this.contract = new Id018ProxfordContractId(this._io, this, this._root);
      this.bondId = new Id018ProxfordBondId(this._io, this, this._root);
      this.id018ProxfordOperationMetadataAlphaTezBalanceUpdate = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return FrozenBonds;
  })();

  var Originated = Id018ProxfordReceiptBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Id018ProxfordContractId = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordContractId = (function() {
    function Id018ProxfordContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordContractId.prototype._read = function() {
      this.id018ProxfordContractIdTag = this._io.readU1();
      if (this.id018ProxfordContractIdTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id018ProxfordContractIdTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id018ProxfordContractId;
  })();

  var Id018ProxfordOperationMetadataAlphaBalanceAndUpdate = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdate = (function() {
    function Id018ProxfordOperationMetadataAlphaBalanceAndUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordOperationMetadataAlphaBalanceAndUpdate.prototype._read = function() {
      this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag = this._io.readU1();
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.CONTRACT) {
        this.contract = new Contract(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.BLOCK_FEES) {
        this.blockFees = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.DEPOSITS) {
        this.deposits = new Deposits(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.NONCE_REVELATION_REWARDS) {
        this.nonceRevelationRewards = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.ATTESTING_REWARDS) {
        this.attestingRewards = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.BAKING_REWARDS) {
        this.bakingRewards = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.BAKING_BONUSES) {
        this.bakingBonuses = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.STORAGE_FEES) {
        this.storageFees = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.DOUBLE_SIGNING_PUNISHMENTS) {
        this.doubleSigningPunishments = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.LOST_ATTESTING_REWARDS) {
        this.lostAttestingRewards = new LostAttestingRewards(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.LIQUIDITY_BAKING_SUBSIDIES) {
        this.liquidityBakingSubsidies = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.BURNED) {
        this.burned = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.COMMITMENTS) {
        this.commitments = new Commitments(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.BOOTSTRAP) {
        this.bootstrap = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.INVOICE) {
        this.invoice = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.INITIAL_COMMITMENTS) {
        this.initialCommitments = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.MINTED) {
        this.minted = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.FROZEN_BONDS) {
        this.frozenBonds = new FrozenBonds(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.SMART_ROLLUP_REFUTATION_PUNISHMENTS) {
        this.smartRollupRefutationPunishments = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.SMART_ROLLUP_REFUTATION_REWARDS) {
        this.smartRollupRefutationRewards = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.UNSTAKED_DEPOSITS) {
        this.unstakedDeposits = new UnstakedDeposits(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.STAKING_DELEGATOR_NUMERATOR) {
        this.stakingDelegatorNumerator = new StakingDelegatorNumerator(this._io, this, this._root);
      }
      if (this.id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.STAKING_DELEGATE_DENOMINATOR) {
        this.stakingDelegateDenominator = new StakingDelegateDenominator(this._io, this, this._root);
      }
    }

    return Id018ProxfordOperationMetadataAlphaBalanceAndUpdate;
  })();

  var StakingDelegateDenominator = Id018ProxfordReceiptBalanceUpdates.StakingDelegateDenominator = (function() {
    function StakingDelegateDenominator(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    StakingDelegateDenominator.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.id018ProxfordOperationMetadataAlphaStakingAbstractQuantity = new Id018ProxfordOperationMetadataAlphaStakingAbstractQuantity(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return StakingDelegateDenominator;
  })();

  var Id018ProxfordOperationMetadataAlphaBalanceUpdates = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceUpdates = (function() {
    function Id018ProxfordOperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordOperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id018ProxfordOperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id018ProxfordOperationMetadataAlphaBalanceUpdatesEntries.push(new Id018ProxfordOperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id018ProxfordOperationMetadataAlphaBalanceUpdates;
  })();

  var Single = Id018ProxfordReceiptBalanceUpdates.Single = (function() {
    function Single(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Single.prototype._read = function() {
      this.contract = new Id018ProxfordContractId(this._io, this, this._root);
      this.delegate = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Single;
  })();

  var Deposits = Id018ProxfordReceiptBalanceUpdates.Deposits = (function() {
    function Deposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposits.prototype._read = function() {
      this.staker = new Id018ProxfordFrozenStaker(this._io, this, this._root);
      this.id018ProxfordOperationMetadataAlphaTezBalanceUpdate = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * frozen_staker: Abstract notion of staker used in operation receipts for frozen deposits, either a single staker or all the stakers delegating to some delegate.
     */

    return Deposits;
  })();

  var Id018ProxfordStaker = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordStaker = (function() {
    function Id018ProxfordStaker(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordStaker.prototype._read = function() {
      this.id018ProxfordStakerTag = this._io.readU1();
      if (this.id018ProxfordStakerTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordStakerTag.SINGLE) {
        this.single = new Single(this._io, this, this._root);
      }
      if (this.id018ProxfordStakerTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordStakerTag.SHARED) {
        this.shared = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id018ProxfordStaker;
  })();

  var Id018ProxfordBondId = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordBondId = (function() {
    function Id018ProxfordBondId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordBondId.prototype._read = function() {
      this.id018ProxfordBondIdTag = this._io.readU1();
      if (this.id018ProxfordBondIdTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordBondIdTag.SMART_ROLLUP_BOND_ID) {
        this.smartRollupBondId = this._io.readBytes(20);
      }
    }

    return Id018ProxfordBondId;
  })();

  var Id018ProxfordOperationMetadataAlphaStakingAbstractQuantity = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaStakingAbstractQuantity = (function() {
    function Id018ProxfordOperationMetadataAlphaStakingAbstractQuantity(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordOperationMetadataAlphaStakingAbstractQuantity.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id018ProxfordOperationMetadataAlphaStakingAbstractQuantity;
  })();

  var Id018ProxfordOperationMetadataAlphaTezBalanceUpdate = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate = (function() {
    function Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordOperationMetadataAlphaTezBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id018ProxfordOperationMetadataAlphaTezBalanceUpdate;
  })();

  var Id018ProxfordFrozenStaker = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordFrozenStaker = (function() {
    function Id018ProxfordFrozenStaker(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordFrozenStaker.prototype._read = function() {
      this.id018ProxfordFrozenStakerTag = this._io.readU1();
      if (this.id018ProxfordFrozenStakerTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordFrozenStakerTag.SINGLE) {
        this.single = new Single(this._io, this, this._root);
      }
      if (this.id018ProxfordFrozenStakerTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordFrozenStakerTag.SHARED) {
        this.shared = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id018ProxfordFrozenStakerTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordFrozenStakerTag.BAKER) {
        this.baker = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id018ProxfordFrozenStaker;
  })();

  var LostAttestingRewards = Id018ProxfordReceiptBalanceUpdates.LostAttestingRewards = (function() {
    function LostAttestingRewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LostAttestingRewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.participation = this._io.readU1();
      this.revelation = this._io.readU1();
      this.id018ProxfordOperationMetadataAlphaTezBalanceUpdate = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return LostAttestingRewards;
  })();

  var UnstakedDeposits = Id018ProxfordReceiptBalanceUpdates.UnstakedDeposits = (function() {
    function UnstakedDeposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    UnstakedDeposits.prototype._read = function() {
      this.staker = new Id018ProxfordStaker(this._io, this, this._root);
      this.cycle = this._io.readS4be();
      this.id018ProxfordOperationMetadataAlphaTezBalanceUpdate = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * unstaked_frozen_staker: Abstract notion of staker used in operation receipts for unstaked frozen deposits, either a single staker or all the stakers delegating to some delegate.
     */

    return UnstakedDeposits;
  })();

  var Id018ProxfordOperationMetadataAlphaUpdateOrigin = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaUpdateOrigin = (function() {
    function Id018ProxfordOperationMetadataAlphaUpdateOrigin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordOperationMetadataAlphaUpdateOrigin.prototype._read = function() {
      this.id018ProxfordOperationMetadataAlphaUpdateOriginTag = this._io.readU1();
      if (this.id018ProxfordOperationMetadataAlphaUpdateOriginTag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaUpdateOriginTag.DELAYED_OPERATION) {
        this.delayedOperation = this._io.readBytes(32);
      }
    }

    return Id018ProxfordOperationMetadataAlphaUpdateOrigin;
  })();

  var PublicKeyHash = Id018ProxfordReceiptBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id018ProxfordReceiptBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id018ProxfordReceiptBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id018ProxfordReceiptBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id018ProxfordReceiptBalanceUpdates.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Contract = Id018ProxfordReceiptBalanceUpdates.Contract = (function() {
    function Contract(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Contract.prototype._read = function() {
      this.contract = new Id018ProxfordContractId(this._io, this, this._root);
      this.id018ProxfordOperationMetadataAlphaTezBalanceUpdate = new Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Contract;
  })();

  var StakingDelegatorNumerator = Id018ProxfordReceiptBalanceUpdates.StakingDelegatorNumerator = (function() {
    function StakingDelegatorNumerator(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    StakingDelegatorNumerator.prototype._read = function() {
      this.delegator = new Id018ProxfordContractId(this._io, this, this._root);
      this.id018ProxfordOperationMetadataAlphaStakingAbstractQuantity = new Id018ProxfordOperationMetadataAlphaStakingAbstractQuantity(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return StakingDelegatorNumerator;
  })();

  var Id018ProxfordOperationMetadataAlphaBalanceUpdatesEntries = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id018ProxfordOperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id018ProxfordOperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id018ProxfordOperationMetadataAlphaBalanceAndUpdate = new Id018ProxfordOperationMetadataAlphaBalanceAndUpdate(this._io, this, this._root);
      this.id018ProxfordOperationMetadataAlphaUpdateOrigin = new Id018ProxfordOperationMetadataAlphaUpdateOrigin(this._io, this, this._root);
    }

    return Id018ProxfordOperationMetadataAlphaBalanceUpdatesEntries;
  })();

  return Id018ProxfordReceiptBalanceUpdates;
})();
return Id018ProxfordReceiptBalanceUpdates;
}));
