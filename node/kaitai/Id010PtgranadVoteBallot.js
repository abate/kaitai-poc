// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id010PtgranadVoteBallot = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 010-PtGRANAD.vote.ballot
 */

var Id010PtgranadVoteBallot = (function() {
  function Id010PtgranadVoteBallot(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id010PtgranadVoteBallot.prototype._read = function() {
    this.id010PtgranadVoteBallot = this._io.readS1();
  }

  return Id010PtgranadVoteBallot;
})();
return Id010PtgranadVoteBallot;
}));
