// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaSmartRollupProof = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.smart_rollup.proof
 */

var Id019PtparisaSmartRollupProof = (function() {
  Id019PtparisaSmartRollupProof.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id019PtparisaSmartRollupProof.InputProofTag = Object.freeze({
    INBOX__PROOF: 0,
    REVEAL__PROOF: 1,
    FIRST__INPUT: 2,

    0: "INBOX__PROOF",
    1: "REVEAL__PROOF",
    2: "FIRST__INPUT",
  });

  Id019PtparisaSmartRollupProof.RevealProofTag = Object.freeze({
    RAW__DATA__PROOF: 0,
    METADATA__PROOF: 1,
    DAL__PAGE__PROOF: 2,
    DAL__PARAMETERS__PROOF: 3,

    0: "RAW__DATA__PROOF",
    1: "METADATA__PROOF",
    2: "DAL__PAGE__PROOF",
    3: "DAL__PARAMETERS__PROOF",
  });

  function Id019PtparisaSmartRollupProof(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaSmartRollupProof.prototype._read = function() {
    this.pvmStep = new BytesDynUint30(this._io, this, this._root);
    this.inputProofTag = this._io.readU1();
    if (this.inputProofTag == Id019PtparisaSmartRollupProof.Bool.TRUE) {
      this.inputProof = new InputProof(this._io, this, this._root);
    }
  }

  var DalPageId = Id019PtparisaSmartRollupProof.DalPageId = (function() {
    function DalPageId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalPageId.prototype._read = function() {
      this.publishedLevel = this._io.readS4be();
      this.slotIndex = this._io.readU1();
      this.pageIndex = this._io.readS2be();
    }

    return DalPageId;
  })();

  var DalPageProof = Id019PtparisaSmartRollupProof.DalPageProof = (function() {
    function DalPageProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalPageProof.prototype._read = function() {
      this.dalPageId = new DalPageId(this._io, this, this._root);
      this.dalProof = new BytesDynUint30(this._io, this, this._root);
    }

    return DalPageProof;
  })();

  var InputProof = Id019PtparisaSmartRollupProof.InputProof = (function() {
    function InputProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InputProof.prototype._read = function() {
      this.inputProofTag = this._io.readU1();
      if (this.inputProofTag == Id019PtparisaSmartRollupProof.InputProofTag.INBOX__PROOF) {
        this.inboxProof = new InboxProof(this._io, this, this._root);
      }
      if (this.inputProofTag == Id019PtparisaSmartRollupProof.InputProofTag.REVEAL__PROOF) {
        this.revealProof = new RevealProof(this._io, this, this._root);
      }
    }

    return InputProof;
  })();

  var N = Id019PtparisaSmartRollupProof.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var RevealProof = Id019PtparisaSmartRollupProof.RevealProof = (function() {
    function RevealProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RevealProof.prototype._read = function() {
      this.revealProofTag = this._io.readU1();
      if (this.revealProofTag == Id019PtparisaSmartRollupProof.RevealProofTag.RAW__DATA__PROOF) {
        this.rawDataProof = new RawData0(this._io, this, this._root);
      }
      if (this.revealProofTag == Id019PtparisaSmartRollupProof.RevealProofTag.DAL__PAGE__PROOF) {
        this.dalPageProof = new DalPageProof(this._io, this, this._root);
      }
    }

    return RevealProof;
  })();

  var RawData = Id019PtparisaSmartRollupProof.RawData = (function() {
    function RawData(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RawData.prototype._read = function() {
      this.rawData = this._io.readBytesFull();
    }

    return RawData;
  })();

  var InboxProof = Id019PtparisaSmartRollupProof.InboxProof = (function() {
    function InboxProof(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    InboxProof.prototype._read = function() {
      this.level = this._io.readS4be();
      this.messageCounter = new N(this._io, this, this._root);
      this.serializedProof = new BytesDynUint30(this._io, this, this._root);
    }

    return InboxProof;
  })();

  var BytesDynUint30 = Id019PtparisaSmartRollupProof.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var NChunk = Id019PtparisaSmartRollupProof.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var RawData0 = Id019PtparisaSmartRollupProof.RawData0 = (function() {
    function RawData0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RawData0.prototype._read = function() {
      this.lenRawData = this._io.readU2be();
      if (!(this.lenRawData <= 4096)) {
        throw new KaitaiStream.ValidationGreaterThanError(4096, this.lenRawData, this._io, "/types/raw_data_0/seq/0");
      }
      this._raw_rawData = this._io.readBytes(this.lenRawData);
      var _io__raw_rawData = new KaitaiStream(this._raw_rawData);
      this.rawData = new RawData(_io__raw_rawData, this, this._root);
    }

    return RawData0;
  })();

  return Id019PtparisaSmartRollupProof;
})();
return Id019PtparisaSmartRollupProof;
}));
