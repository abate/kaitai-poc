// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id005Psbabym1Fitness = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 005-PsBabyM1.fitness
 */

var Id005Psbabym1Fitness = (function() {
  function Id005Psbabym1Fitness(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1Fitness.prototype._read = function() {
    this.fitness = new Fitness0(this._io, this, this._root);
  }

  var FitnessElem = Id005Psbabym1Fitness.FitnessElem = (function() {
    function FitnessElem(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    FitnessElem.prototype._read = function() {
      this.fitnessElem = new BytesDynUint30(this._io, this, this._root);
    }

    return FitnessElem;
  })();

  var Fitness0 = Id005Psbabym1Fitness.Fitness0 = (function() {
    function Fitness0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Fitness0.prototype._read = function() {
      this.lenFitness = this._io.readU4be();
      if (!(this.lenFitness <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenFitness, this._io, "/types/fitness_0/seq/0");
      }
      this._raw_fitness = this._io.readBytes(this.lenFitness);
      var _io__raw_fitness = new KaitaiStream(this._raw_fitness);
      this.fitness = new Fitness(_io__raw_fitness, this, this._root);
    }

    return Fitness0;
  })();

  var BytesDynUint30 = Id005Psbabym1Fitness.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Fitness = Id005Psbabym1Fitness.Fitness = (function() {
    function Fitness(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Fitness.prototype._read = function() {
      this.fitnessEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.fitnessEntries.push(new FitnessEntries(this._io, this, this._root));
        i++;
      }
    }

    return Fitness;
  })();

  var FitnessEntries = Id005Psbabym1Fitness.FitnessEntries = (function() {
    function FitnessEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    FitnessEntries.prototype._read = function() {
      this.fitnessElem = new FitnessElem(this._io, this, this._root);
    }

    return FitnessEntries;
  })();

  /**
   * Block fitness: The fitness, or score, of a block, that allow the Tezos to decide which chain is the best. A fitness value is a list of byte sequences. They are compared as follows: shortest lists are smaller; lists of the same length are compared according to the lexicographical order.
   */

  return Id005Psbabym1Fitness;
})();
return Id005Psbabym1Fitness;
}));
