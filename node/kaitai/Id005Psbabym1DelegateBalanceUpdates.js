// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id005Psbabym1DelegateBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 005-PsBabyM1.delegate.balance_updates
 */

var Id005Psbabym1DelegateBalanceUpdates = (function() {
  Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1ContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1OperationMetadataAlphaBalanceTag = Object.freeze({
    CONTRACT: 0,
    REWARDS: 1,
    FEES: 2,
    DEPOSITS: 3,

    0: "CONTRACT",
    1: "REWARDS",
    2: "FEES",
    3: "DEPOSITS",
  });

  Id005Psbabym1DelegateBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id005Psbabym1DelegateBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1DelegateBalanceUpdates.prototype._read = function() {
    this.id005Psbabym1OperationMetadataAlphaBalanceUpdates = new Id005Psbabym1OperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Originated = Id005Psbabym1DelegateBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Id005Psbabym1OperationMetadataAlphaBalanceUpdate = Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1OperationMetadataAlphaBalanceUpdate = (function() {
    function Id005Psbabym1OperationMetadataAlphaBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1OperationMetadataAlphaBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id005Psbabym1OperationMetadataAlphaBalanceUpdate;
  })();

  var Id005Psbabym1OperationMetadataAlphaBalanceUpdates0 = Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1OperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id005Psbabym1OperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1OperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId005Psbabym1OperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId005Psbabym1OperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId005Psbabym1OperationMetadataAlphaBalanceUpdates, this._io, "/types/id_005__psbabym1__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id005Psbabym1OperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId005Psbabym1OperationMetadataAlphaBalanceUpdates);
      var _io__raw_id005Psbabym1OperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id005Psbabym1OperationMetadataAlphaBalanceUpdates);
      this.id005Psbabym1OperationMetadataAlphaBalanceUpdates = new Id005Psbabym1OperationMetadataAlphaBalanceUpdates(_io__raw_id005Psbabym1OperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id005Psbabym1OperationMetadataAlphaBalanceUpdates0;
  })();

  var Rewards = Id005Psbabym1DelegateBalanceUpdates.Rewards = (function() {
    function Rewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Rewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Rewards;
  })();

  var Id005Psbabym1OperationMetadataAlphaBalanceUpdatesEntries = Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1OperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id005Psbabym1OperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1OperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id005Psbabym1OperationMetadataAlphaBalance = new Id005Psbabym1OperationMetadataAlphaBalance(this._io, this, this._root);
      this.id005Psbabym1OperationMetadataAlphaBalanceUpdate = new Id005Psbabym1OperationMetadataAlphaBalanceUpdate(this._io, this, this._root);
    }

    return Id005Psbabym1OperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var Id005Psbabym1OperationMetadataAlphaBalance = Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1OperationMetadataAlphaBalance = (function() {
    function Id005Psbabym1OperationMetadataAlphaBalance(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1OperationMetadataAlphaBalance.prototype._read = function() {
      this.id005Psbabym1OperationMetadataAlphaBalanceTag = this._io.readU1();
      if (this.id005Psbabym1OperationMetadataAlphaBalanceTag == Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1OperationMetadataAlphaBalanceTag.CONTRACT) {
        this.contract = new Id005Psbabym1ContractId(this._io, this, this._root);
      }
      if (this.id005Psbabym1OperationMetadataAlphaBalanceTag == Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1OperationMetadataAlphaBalanceTag.REWARDS) {
        this.rewards = new Rewards(this._io, this, this._root);
      }
      if (this.id005Psbabym1OperationMetadataAlphaBalanceTag == Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1OperationMetadataAlphaBalanceTag.FEES) {
        this.fees = new Fees(this._io, this, this._root);
      }
      if (this.id005Psbabym1OperationMetadataAlphaBalanceTag == Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1OperationMetadataAlphaBalanceTag.DEPOSITS) {
        this.deposits = new Deposits(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Id005Psbabym1OperationMetadataAlphaBalance;
  })();

  var Deposits = Id005Psbabym1DelegateBalanceUpdates.Deposits = (function() {
    function Deposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposits.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Deposits;
  })();

  var Fees = Id005Psbabym1DelegateBalanceUpdates.Fees = (function() {
    function Fees(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Fees.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Fees;
  })();

  var Id005Psbabym1ContractId = Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1ContractId = (function() {
    function Id005Psbabym1ContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1ContractId.prototype._read = function() {
      this.id005Psbabym1ContractIdTag = this._io.readU1();
      if (this.id005Psbabym1ContractIdTag == Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1ContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id005Psbabym1ContractIdTag == Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1ContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id005Psbabym1ContractId;
  })();

  var Id005Psbabym1OperationMetadataAlphaBalanceUpdates = Id005Psbabym1DelegateBalanceUpdates.Id005Psbabym1OperationMetadataAlphaBalanceUpdates = (function() {
    function Id005Psbabym1OperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1OperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id005Psbabym1OperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id005Psbabym1OperationMetadataAlphaBalanceUpdatesEntries.push(new Id005Psbabym1OperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id005Psbabym1OperationMetadataAlphaBalanceUpdates;
  })();

  var PublicKeyHash = Id005Psbabym1DelegateBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id005Psbabym1DelegateBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id005Psbabym1DelegateBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id005Psbabym1DelegateBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id005Psbabym1DelegateBalanceUpdates;
})();
return Id005Psbabym1DelegateBalanceUpdates;
}));
