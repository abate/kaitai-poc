// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id006PscarthaContractBigMapDiff = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 006-PsCARTHA.contract.big_map_diff
 */

var Id006PscarthaContractBigMapDiff = (function() {
  Id006PscarthaContractBigMapDiff.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id006PscarthaContractBigMapDiff.Id006PscarthaContractBigMapDiffEltTag = Object.freeze({
    UPDATE: 0,
    REMOVE: 1,
    COPY: 2,
    ALLOC: 3,

    0: "UPDATE",
    1: "REMOVE",
    2: "COPY",
    3: "ALLOC",
  });

  Id006PscarthaContractBigMapDiff.Id006PscarthaMichelsonV1Primitives = Object.freeze({
    PARAMETER: 0,
    STORAGE: 1,
    CODE: 2,
    FALSE: 3,
    ELT: 4,
    LEFT: 5,
    NONE_0: 6,
    PAIR_0: 7,
    RIGHT_0: 8,
    SOME_0: 9,
    TRUE: 10,
    UNIT_1: 11,
    PACK: 12,
    UNPACK: 13,
    BLAKE2B: 14,
    SHA256: 15,
    SHA512: 16,
    ABS: 17,
    ADD: 18,
    AMOUNT: 19,
    AND: 20,
    BALANCE: 21,
    CAR: 22,
    CDR: 23,
    CHECK_SIGNATURE: 24,
    COMPARE: 25,
    CONCAT: 26,
    CONS: 27,
    CREATE_ACCOUNT: 28,
    CREATE_CONTRACT: 29,
    IMPLICIT_ACCOUNT: 30,
    DIP: 31,
    DROP: 32,
    DUP: 33,
    EDIV: 34,
    EMPTY_MAP: 35,
    EMPTY_SET: 36,
    EQ: 37,
    EXEC: 38,
    FAILWITH: 39,
    GE: 40,
    GET: 41,
    GT: 42,
    HASH_KEY: 43,
    IF: 44,
    IF_CONS: 45,
    IF_LEFT: 46,
    IF_NONE: 47,
    INT_0: 48,
    LAMBDA_0: 49,
    LE: 50,
    LEFT_0: 51,
    LOOP: 52,
    LSL: 53,
    LSR: 54,
    LT: 55,
    MAP_0: 56,
    MEM: 57,
    MUL: 58,
    NEG: 59,
    NEQ: 60,
    NIL: 61,
    NONE: 62,
    NOT: 63,
    NOW: 64,
    OR_0: 65,
    PAIR_1: 66,
    PUSH: 67,
    RIGHT: 68,
    SIZE: 69,
    SOME: 70,
    SOURCE: 71,
    SENDER: 72,
    SELF: 73,
    STEPS_TO_QUOTA: 74,
    SUB: 75,
    SWAP: 76,
    TRANSFER_TOKENS: 77,
    SET_DELEGATE: 78,
    UNIT_0: 79,
    UPDATE: 80,
    XOR: 81,
    ITER: 82,
    LOOP_LEFT: 83,
    ADDRESS_0: 84,
    CONTRACT_0: 85,
    ISNAT: 86,
    CAST: 87,
    RENAME: 88,
    BOOL: 89,
    CONTRACT: 90,
    INT: 91,
    KEY: 92,
    KEY_HASH: 93,
    LAMBDA: 94,
    LIST: 95,
    MAP: 96,
    BIG_MAP: 97,
    NAT: 98,
    OPTION: 99,
    OR: 100,
    PAIR: 101,
    SET: 102,
    SIGNATURE: 103,
    STRING: 104,
    BYTES: 105,
    MUTEZ: 106,
    TIMESTAMP: 107,
    UNIT: 108,
    OPERATION: 109,
    ADDRESS: 110,
    SLICE: 111,
    DIG: 112,
    DUG: 113,
    EMPTY_BIG_MAP: 114,
    APPLY: 115,
    CHAIN_ID: 116,
    CHAIN_ID_0: 117,

    0: "PARAMETER",
    1: "STORAGE",
    2: "CODE",
    3: "FALSE",
    4: "ELT",
    5: "LEFT",
    6: "NONE_0",
    7: "PAIR_0",
    8: "RIGHT_0",
    9: "SOME_0",
    10: "TRUE",
    11: "UNIT_1",
    12: "PACK",
    13: "UNPACK",
    14: "BLAKE2B",
    15: "SHA256",
    16: "SHA512",
    17: "ABS",
    18: "ADD",
    19: "AMOUNT",
    20: "AND",
    21: "BALANCE",
    22: "CAR",
    23: "CDR",
    24: "CHECK_SIGNATURE",
    25: "COMPARE",
    26: "CONCAT",
    27: "CONS",
    28: "CREATE_ACCOUNT",
    29: "CREATE_CONTRACT",
    30: "IMPLICIT_ACCOUNT",
    31: "DIP",
    32: "DROP",
    33: "DUP",
    34: "EDIV",
    35: "EMPTY_MAP",
    36: "EMPTY_SET",
    37: "EQ",
    38: "EXEC",
    39: "FAILWITH",
    40: "GE",
    41: "GET",
    42: "GT",
    43: "HASH_KEY",
    44: "IF",
    45: "IF_CONS",
    46: "IF_LEFT",
    47: "IF_NONE",
    48: "INT_0",
    49: "LAMBDA_0",
    50: "LE",
    51: "LEFT_0",
    52: "LOOP",
    53: "LSL",
    54: "LSR",
    55: "LT",
    56: "MAP_0",
    57: "MEM",
    58: "MUL",
    59: "NEG",
    60: "NEQ",
    61: "NIL",
    62: "NONE",
    63: "NOT",
    64: "NOW",
    65: "OR_0",
    66: "PAIR_1",
    67: "PUSH",
    68: "RIGHT",
    69: "SIZE",
    70: "SOME",
    71: "SOURCE",
    72: "SENDER",
    73: "SELF",
    74: "STEPS_TO_QUOTA",
    75: "SUB",
    76: "SWAP",
    77: "TRANSFER_TOKENS",
    78: "SET_DELEGATE",
    79: "UNIT_0",
    80: "UPDATE",
    81: "XOR",
    82: "ITER",
    83: "LOOP_LEFT",
    84: "ADDRESS_0",
    85: "CONTRACT_0",
    86: "ISNAT",
    87: "CAST",
    88: "RENAME",
    89: "BOOL",
    90: "CONTRACT",
    91: "INT",
    92: "KEY",
    93: "KEY_HASH",
    94: "LAMBDA",
    95: "LIST",
    96: "MAP",
    97: "BIG_MAP",
    98: "NAT",
    99: "OPTION",
    100: "OR",
    101: "PAIR",
    102: "SET",
    103: "SIGNATURE",
    104: "STRING",
    105: "BYTES",
    106: "MUTEZ",
    107: "TIMESTAMP",
    108: "UNIT",
    109: "OPERATION",
    110: "ADDRESS",
    111: "SLICE",
    112: "DIG",
    113: "DUG",
    114: "EMPTY_BIG_MAP",
    115: "APPLY",
    116: "CHAIN_ID",
    117: "CHAIN_ID_0",
  });

  Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1ExpressionTag = Object.freeze({
    INT: 0,
    STRING: 1,
    SEQUENCE: 2,
    PRIM__NO_ARGS__NO_ANNOTS: 3,
    PRIM__NO_ARGS__SOME_ANNOTS: 4,
    PRIM__1_ARG__NO_ANNOTS: 5,
    PRIM__1_ARG__SOME_ANNOTS: 6,
    PRIM__2_ARGS__NO_ANNOTS: 7,
    PRIM__2_ARGS__SOME_ANNOTS: 8,
    PRIM__GENERIC: 9,
    BYTES: 10,

    0: "INT",
    1: "STRING",
    2: "SEQUENCE",
    3: "PRIM__NO_ARGS__NO_ANNOTS",
    4: "PRIM__NO_ARGS__SOME_ANNOTS",
    5: "PRIM__1_ARG__NO_ANNOTS",
    6: "PRIM__1_ARG__SOME_ANNOTS",
    7: "PRIM__2_ARGS__NO_ANNOTS",
    8: "PRIM__2_ARGS__SOME_ANNOTS",
    9: "PRIM__GENERIC",
    10: "BYTES",
  });

  function Id006PscarthaContractBigMapDiff(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaContractBigMapDiff.prototype._read = function() {
    this.lenId006PscarthaContractBigMapDiff = this._io.readU4be();
    if (!(this.lenId006PscarthaContractBigMapDiff <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId006PscarthaContractBigMapDiff, this._io, "/seq/0");
    }
    this._raw_id006PscarthaContractBigMapDiff = this._io.readBytes(this.lenId006PscarthaContractBigMapDiff);
    var _io__raw_id006PscarthaContractBigMapDiff = new KaitaiStream(this._raw_id006PscarthaContractBigMapDiff);
    this.id006PscarthaContractBigMapDiff = new Id006PscarthaContractBigMapDiff(_io__raw_id006PscarthaContractBigMapDiff, this, this._root);
  }

  var Alloc = Id006PscarthaContractBigMapDiff.Alloc = (function() {
    function Alloc(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Alloc.prototype._read = function() {
      this.bigMap = new Z(this._io, this, this._root);
      this.keyType = new Micheline006PscarthaMichelsonV1Expression(this._io, this, this._root);
      this.valueType = new Micheline006PscarthaMichelsonV1Expression(this._io, this, this._root);
    }

    return Alloc;
  })();

  var Prim1ArgSomeAnnots = Id006PscarthaContractBigMapDiff.Prim1ArgSomeAnnots = (function() {
    function Prim1ArgSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim1ArgSomeAnnots.prototype._read = function() {
      this.prim = new Id006PscarthaMichelsonV1Primitives(this._io, this, this._root);
      this.arg = new Micheline006PscarthaMichelsonV1Expression(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return Prim1ArgSomeAnnots;
  })();

  var Id006PscarthaContractBigMapDiffEntries = Id006PscarthaContractBigMapDiff.Id006PscarthaContractBigMapDiffEntries = (function() {
    function Id006PscarthaContractBigMapDiffEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaContractBigMapDiffEntries.prototype._read = function() {
      this.id006PscarthaContractBigMapDiffEltTag = this._io.readU1();
      if (this.id006PscarthaContractBigMapDiffEltTag == Id006PscarthaContractBigMapDiff.Id006PscarthaContractBigMapDiffEltTag.UPDATE) {
        this.update = new Update(this._io, this, this._root);
      }
      if (this.id006PscarthaContractBigMapDiffEltTag == Id006PscarthaContractBigMapDiff.Id006PscarthaContractBigMapDiffEltTag.REMOVE) {
        this.remove = new Z(this._io, this, this._root);
      }
      if (this.id006PscarthaContractBigMapDiffEltTag == Id006PscarthaContractBigMapDiff.Id006PscarthaContractBigMapDiffEltTag.COPY) {
        this.copy = new Copy(this._io, this, this._root);
      }
      if (this.id006PscarthaContractBigMapDiffEltTag == Id006PscarthaContractBigMapDiff.Id006PscarthaContractBigMapDiffEltTag.ALLOC) {
        this.alloc = new Alloc(this._io, this, this._root);
      }
    }

    return Id006PscarthaContractBigMapDiffEntries;
  })();

  var PrimNoArgsSomeAnnots = Id006PscarthaContractBigMapDiff.PrimNoArgsSomeAnnots = (function() {
    function PrimNoArgsSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrimNoArgsSomeAnnots.prototype._read = function() {
      this.prim = new Id006PscarthaMichelsonV1Primitives(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return PrimNoArgsSomeAnnots;
  })();

  var Micheline006PscarthaMichelsonV1Expression = Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1Expression = (function() {
    function Micheline006PscarthaMichelsonV1Expression(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Micheline006PscarthaMichelsonV1Expression.prototype._read = function() {
      this.micheline006PscarthaMichelsonV1ExpressionTag = this._io.readU1();
      if (this.micheline006PscarthaMichelsonV1ExpressionTag == Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1ExpressionTag.INT) {
        this.int = new Z(this._io, this, this._root);
      }
      if (this.micheline006PscarthaMichelsonV1ExpressionTag == Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1ExpressionTag.STRING) {
        this.string = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.micheline006PscarthaMichelsonV1ExpressionTag == Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1ExpressionTag.SEQUENCE) {
        this.sequence = new Sequence0(this._io, this, this._root);
      }
      if (this.micheline006PscarthaMichelsonV1ExpressionTag == Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1ExpressionTag.PRIM__NO_ARGS__NO_ANNOTS) {
        this.primNoArgsNoAnnots = new Id006PscarthaMichelsonV1Primitives(this._io, this, this._root);
      }
      if (this.micheline006PscarthaMichelsonV1ExpressionTag == Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1ExpressionTag.PRIM__NO_ARGS__SOME_ANNOTS) {
        this.primNoArgsSomeAnnots = new PrimNoArgsSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline006PscarthaMichelsonV1ExpressionTag == Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1ExpressionTag.PRIM__1_ARG__NO_ANNOTS) {
        this.prim1ArgNoAnnots = new Prim1ArgNoAnnots(this._io, this, this._root);
      }
      if (this.micheline006PscarthaMichelsonV1ExpressionTag == Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1ExpressionTag.PRIM__1_ARG__SOME_ANNOTS) {
        this.prim1ArgSomeAnnots = new Prim1ArgSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline006PscarthaMichelsonV1ExpressionTag == Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1ExpressionTag.PRIM__2_ARGS__NO_ANNOTS) {
        this.prim2ArgsNoAnnots = new Prim2ArgsNoAnnots(this._io, this, this._root);
      }
      if (this.micheline006PscarthaMichelsonV1ExpressionTag == Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1ExpressionTag.PRIM__2_ARGS__SOME_ANNOTS) {
        this.prim2ArgsSomeAnnots = new Prim2ArgsSomeAnnots(this._io, this, this._root);
      }
      if (this.micheline006PscarthaMichelsonV1ExpressionTag == Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1ExpressionTag.PRIM__GENERIC) {
        this.primGeneric = new PrimGeneric(this._io, this, this._root);
      }
      if (this.micheline006PscarthaMichelsonV1ExpressionTag == Id006PscarthaContractBigMapDiff.Micheline006PscarthaMichelsonV1ExpressionTag.BYTES) {
        this.bytes = new BytesDynUint30(this._io, this, this._root);
      }
    }

    return Micheline006PscarthaMichelsonV1Expression;
  })();

  var PrimGeneric = Id006PscarthaContractBigMapDiff.PrimGeneric = (function() {
    function PrimGeneric(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PrimGeneric.prototype._read = function() {
      this.prim = new Id006PscarthaMichelsonV1Primitives(this._io, this, this._root);
      this.args = new Args0(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return PrimGeneric;
  })();

  var Sequence0 = Id006PscarthaContractBigMapDiff.Sequence0 = (function() {
    function Sequence0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sequence0.prototype._read = function() {
      this.lenSequence = this._io.readU4be();
      if (!(this.lenSequence <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenSequence, this._io, "/types/sequence_0/seq/0");
      }
      this._raw_sequence = this._io.readBytes(this.lenSequence);
      var _io__raw_sequence = new KaitaiStream(this._raw_sequence);
      this.sequence = new Sequence(_io__raw_sequence, this, this._root);
    }

    return Sequence0;
  })();

  var Id006PscarthaMichelsonV1Primitives = Id006PscarthaContractBigMapDiff.Id006PscarthaMichelsonV1Primitives = (function() {
    function Id006PscarthaMichelsonV1Primitives(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaMichelsonV1Primitives.prototype._read = function() {
      this.id006PscarthaMichelsonV1Primitives = this._io.readU1();
    }

    return Id006PscarthaMichelsonV1Primitives;
  })();

  var Sequence = Id006PscarthaContractBigMapDiff.Sequence = (function() {
    function Sequence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Sequence.prototype._read = function() {
      this.sequenceEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.sequenceEntries.push(new SequenceEntries(this._io, this, this._root));
        i++;
      }
    }

    return Sequence;
  })();

  var Id006PscarthaContractBigMapDiff = Id006PscarthaContractBigMapDiff.Id006PscarthaContractBigMapDiff = (function() {
    function Id006PscarthaContractBigMapDiff(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaContractBigMapDiff.prototype._read = function() {
      this.id006PscarthaContractBigMapDiffEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id006PscarthaContractBigMapDiffEntries.push(new Id006PscarthaContractBigMapDiffEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id006PscarthaContractBigMapDiff;
  })();

  var BytesDynUint30 = Id006PscarthaContractBigMapDiff.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Copy = Id006PscarthaContractBigMapDiff.Copy = (function() {
    function Copy(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Copy.prototype._read = function() {
      this.sourceBigMap = new Z(this._io, this, this._root);
      this.destinationBigMap = new Z(this._io, this, this._root);
    }

    return Copy;
  })();

  var Args = Id006PscarthaContractBigMapDiff.Args = (function() {
    function Args(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Args.prototype._read = function() {
      this.argsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.argsEntries.push(new ArgsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Args;
  })();

  var Prim1ArgNoAnnots = Id006PscarthaContractBigMapDiff.Prim1ArgNoAnnots = (function() {
    function Prim1ArgNoAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim1ArgNoAnnots.prototype._read = function() {
      this.prim = new Id006PscarthaMichelsonV1Primitives(this._io, this, this._root);
      this.arg = new Micheline006PscarthaMichelsonV1Expression(this._io, this, this._root);
    }

    return Prim1ArgNoAnnots;
  })();

  var Update = Id006PscarthaContractBigMapDiff.Update = (function() {
    function Update(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Update.prototype._read = function() {
      this.bigMap = new Z(this._io, this, this._root);
      this.keyHash = this._io.readBytes(32);
      this.key = new Micheline006PscarthaMichelsonV1Expression(this._io, this, this._root);
      this.valueTag = this._io.readU1();
      if (this.valueTag == Id006PscarthaContractBigMapDiff.Bool.TRUE) {
        this.value = new Micheline006PscarthaMichelsonV1Expression(this._io, this, this._root);
      }
    }

    return Update;
  })();

  var SequenceEntries = Id006PscarthaContractBigMapDiff.SequenceEntries = (function() {
    function SequenceEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SequenceEntries.prototype._read = function() {
      this.sequenceElt = new Micheline006PscarthaMichelsonV1Expression(this._io, this, this._root);
    }

    return SequenceEntries;
  })();

  var NChunk = Id006PscarthaContractBigMapDiff.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Prim2ArgsNoAnnots = Id006PscarthaContractBigMapDiff.Prim2ArgsNoAnnots = (function() {
    function Prim2ArgsNoAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim2ArgsNoAnnots.prototype._read = function() {
      this.prim = new Id006PscarthaMichelsonV1Primitives(this._io, this, this._root);
      this.arg1 = new Micheline006PscarthaMichelsonV1Expression(this._io, this, this._root);
      this.arg2 = new Micheline006PscarthaMichelsonV1Expression(this._io, this, this._root);
    }

    return Prim2ArgsNoAnnots;
  })();

  var ArgsEntries = Id006PscarthaContractBigMapDiff.ArgsEntries = (function() {
    function ArgsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ArgsEntries.prototype._read = function() {
      this.argsElt = new Micheline006PscarthaMichelsonV1Expression(this._io, this, this._root);
    }

    return ArgsEntries;
  })();

  var Args0 = Id006PscarthaContractBigMapDiff.Args0 = (function() {
    function Args0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Args0.prototype._read = function() {
      this.lenArgs = this._io.readU4be();
      if (!(this.lenArgs <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenArgs, this._io, "/types/args_0/seq/0");
      }
      this._raw_args = this._io.readBytes(this.lenArgs);
      var _io__raw_args = new KaitaiStream(this._raw_args);
      this.args = new Args(_io__raw_args, this, this._root);
    }

    return Args0;
  })();

  var Prim2ArgsSomeAnnots = Id006PscarthaContractBigMapDiff.Prim2ArgsSomeAnnots = (function() {
    function Prim2ArgsSomeAnnots(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Prim2ArgsSomeAnnots.prototype._read = function() {
      this.prim = new Id006PscarthaMichelsonV1Primitives(this._io, this, this._root);
      this.arg1 = new Micheline006PscarthaMichelsonV1Expression(this._io, this, this._root);
      this.arg2 = new Micheline006PscarthaMichelsonV1Expression(this._io, this, this._root);
      this.annots = new BytesDynUint30(this._io, this, this._root);
    }

    return Prim2ArgsSomeAnnots;
  })();

  var Z = Id006PscarthaContractBigMapDiff.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  return Id006PscarthaContractBigMapDiff;
})();
return Id006PscarthaContractBigMapDiff;
}));
