// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id006PscarthaVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 006-PsCARTHA.vote.listings
 */

var Id006PscarthaVoteListings = (function() {
  Id006PscarthaVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id006PscarthaVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaVoteListings.prototype._read = function() {
    this.lenId006PscarthaVoteListings = this._io.readU4be();
    if (!(this.lenId006PscarthaVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId006PscarthaVoteListings, this._io, "/seq/0");
    }
    this._raw_id006PscarthaVoteListings = this._io.readBytes(this.lenId006PscarthaVoteListings);
    var _io__raw_id006PscarthaVoteListings = new KaitaiStream(this._raw_id006PscarthaVoteListings);
    this.id006PscarthaVoteListings = new Id006PscarthaVoteListings(_io__raw_id006PscarthaVoteListings, this, this._root);
  }

  var Id006PscarthaVoteListings = Id006PscarthaVoteListings.Id006PscarthaVoteListings = (function() {
    function Id006PscarthaVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaVoteListings.prototype._read = function() {
      this.id006PscarthaVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id006PscarthaVoteListingsEntries.push(new Id006PscarthaVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id006PscarthaVoteListings;
  })();

  var Id006PscarthaVoteListingsEntries = Id006PscarthaVoteListings.Id006PscarthaVoteListingsEntries = (function() {
    function Id006PscarthaVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.rolls = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id006PscarthaVoteListingsEntries;
  })();

  var PublicKeyHash = Id006PscarthaVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id006PscarthaVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id006PscarthaVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id006PscarthaVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id006PscarthaVoteListings;
})();
return Id006PscarthaVoteListings;
}));
