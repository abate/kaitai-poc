// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id014PtkathmaRawLevel = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 014-PtKathma.raw_level
 */

var Id014PtkathmaRawLevel = (function() {
  function Id014PtkathmaRawLevel(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id014PtkathmaRawLevel.prototype._read = function() {
    this.id014PtkathmaRawLevel = this._io.readS4be();
  }

  return Id014PtkathmaRawLevel;
})();
return Id014PtkathmaRawLevel;
}));
