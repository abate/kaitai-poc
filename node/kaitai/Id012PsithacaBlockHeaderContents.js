// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id012PsithacaBlockHeaderContents = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 012-Psithaca.block_header.contents
 */

var Id012PsithacaBlockHeaderContents = (function() {
  Id012PsithacaBlockHeaderContents.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id012PsithacaBlockHeaderContents(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id012PsithacaBlockHeaderContents.prototype._read = function() {
    this.id012PsithacaBlockHeaderAlphaUnsignedContents = new Id012PsithacaBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id012PsithacaBlockHeaderAlphaUnsignedContents = Id012PsithacaBlockHeaderContents.Id012PsithacaBlockHeaderAlphaUnsignedContents = (function() {
    function Id012PsithacaBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id012PsithacaBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id012PsithacaBlockHeaderContents.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingEscapeVote = this._io.readU1();
    }

    return Id012PsithacaBlockHeaderAlphaUnsignedContents;
  })();

  return Id012PsithacaBlockHeaderContents;
})();
return Id012PsithacaBlockHeaderContents;
}));
