// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './OperationShellHeader', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./OperationShellHeader'), require('./BlockHeaderShell'));
  } else {
    root.Id006PscarthaOperationProtocolData = factory(root.KaitaiStream, root.OperationShellHeader, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, OperationShellHeader, BlockHeaderShell) {
/**
 * Encoding id: 006-PsCARTHA.operation.protocol_data
 */

var Id006PscarthaOperationProtocolData = (function() {
  Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsTag = Object.freeze({
    ENDORSEMENT: 0,
    SEED_NONCE_REVELATION: 1,
    DOUBLE_ENDORSEMENT_EVIDENCE: 2,
    DOUBLE_BAKING_EVIDENCE: 3,
    ACTIVATE_ACCOUNT: 4,
    PROPOSALS: 5,
    BALLOT: 6,
    REVEAL: 107,
    TRANSACTION: 108,
    ORIGINATION: 109,
    DELEGATION: 110,

    0: "ENDORSEMENT",
    1: "SEED_NONCE_REVELATION",
    2: "DOUBLE_ENDORSEMENT_EVIDENCE",
    3: "DOUBLE_BAKING_EVIDENCE",
    4: "ACTIVATE_ACCOUNT",
    5: "PROPOSALS",
    6: "BALLOT",
    107: "REVEAL",
    108: "TRANSACTION",
    109: "ORIGINATION",
    110: "DELEGATION",
  });

  Id006PscarthaOperationProtocolData.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id006PscarthaOperationProtocolData.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id006PscarthaOperationProtocolData.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id006PscarthaOperationProtocolData.Id006PscarthaEntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    255: "NAMED",
  });

  Id006PscarthaOperationProtocolData.Id006PscarthaInlinedEndorsementContentsTag = Object.freeze({
    ENDORSEMENT: 0,

    0: "ENDORSEMENT",
  });

  Id006PscarthaOperationProtocolData.Id006PscarthaContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  function Id006PscarthaOperationProtocolData(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaOperationProtocolData.prototype._read = function() {
    this.id006PscarthaOperationAlphaContentsAndSignature = new Id006PscarthaOperationAlphaContentsAndSignature(this._io, this, this._root);
  }

  var Op20 = Id006PscarthaOperationProtocolData.Op20 = (function() {
    function Op20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op20.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_0/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op2(_io__raw_op2, this, this._root);
    }

    return Op20;
  })();

  var Id006PscarthaBlockHeaderAlphaSignedContents = Id006PscarthaOperationProtocolData.Id006PscarthaBlockHeaderAlphaSignedContents = (function() {
    function Id006PscarthaBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id006PscarthaBlockHeaderAlphaUnsignedContents = new Id006PscarthaBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id006PscarthaBlockHeaderAlphaSignedContents;
  })();

  var ActivateAccount = Id006PscarthaOperationProtocolData.ActivateAccount = (function() {
    function ActivateAccount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ActivateAccount.prototype._read = function() {
      this.pkh = this._io.readBytes(20);
      this.secret = this._io.readBytes(20);
    }

    return ActivateAccount;
  })();

  var DoubleEndorsementEvidence = Id006PscarthaOperationProtocolData.DoubleEndorsementEvidence = (function() {
    function DoubleEndorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleEndorsementEvidence.prototype._read = function() {
      this.op1 = new Op10(this._io, this, this._root);
      this.op2 = new Op20(this._io, this, this._root);
    }

    return DoubleEndorsementEvidence;
  })();

  var Originated = Id006PscarthaOperationProtocolData.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var N = Id006PscarthaOperationProtocolData.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Proposals0 = Id006PscarthaOperationProtocolData.Proposals0 = (function() {
    function Proposals0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals0.prototype._read = function() {
      this.lenProposals = this._io.readU4be();
      if (!(this.lenProposals <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenProposals, this._io, "/types/proposals_0/seq/0");
      }
      this._raw_proposals = this._io.readBytes(this.lenProposals);
      var _io__raw_proposals = new KaitaiStream(this._raw_proposals);
      this.proposals = new Proposals(_io__raw_proposals, this, this._root);
    }

    return Proposals0;
  })();

  var Id006PscarthaContractId = Id006PscarthaOperationProtocolData.Id006PscarthaContractId = (function() {
    function Id006PscarthaContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaContractId.prototype._read = function() {
      this.id006PscarthaContractIdTag = this._io.readU1();
      if (this.id006PscarthaContractIdTag == Id006PscarthaOperationProtocolData.Id006PscarthaContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id006PscarthaContractIdTag == Id006PscarthaOperationProtocolData.Id006PscarthaContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id006PscarthaContractId;
  })();

  var Reveal = Id006PscarthaOperationProtocolData.Reveal = (function() {
    function Reveal(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Reveal.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id006PscarthaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.publicKey = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Reveal;
  })();

  var Id006PscarthaBlockHeaderAlphaUnsignedContents = Id006PscarthaOperationProtocolData.Id006PscarthaBlockHeaderAlphaUnsignedContents = (function() {
    function Id006PscarthaBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id006PscarthaOperationProtocolData.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
    }

    return Id006PscarthaBlockHeaderAlphaUnsignedContents;
  })();

  var SeedNonceRevelation = Id006PscarthaOperationProtocolData.SeedNonceRevelation = (function() {
    function SeedNonceRevelation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SeedNonceRevelation.prototype._read = function() {
      this.level = this._io.readS4be();
      this.nonce = this._io.readBytes(32);
    }

    return SeedNonceRevelation;
  })();

  var Id006PscarthaInlinedEndorsementContents = Id006PscarthaOperationProtocolData.Id006PscarthaInlinedEndorsementContents = (function() {
    function Id006PscarthaInlinedEndorsementContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaInlinedEndorsementContents.prototype._read = function() {
      this.id006PscarthaInlinedEndorsementContentsTag = this._io.readU1();
      if (this.id006PscarthaInlinedEndorsementContentsTag == Id006PscarthaOperationProtocolData.Id006PscarthaInlinedEndorsementContentsTag.ENDORSEMENT) {
        this.endorsement = this._io.readS4be();
      }
    }

    return Id006PscarthaInlinedEndorsementContents;
  })();

  var DoubleBakingEvidence = Id006PscarthaOperationProtocolData.DoubleBakingEvidence = (function() {
    function DoubleBakingEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleBakingEvidence.prototype._read = function() {
      this.bh1 = new Bh10(this._io, this, this._root);
      this.bh2 = new Bh20(this._io, this, this._root);
    }

    return DoubleBakingEvidence;
  })();

  var PublicKey = Id006PscarthaOperationProtocolData.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id006PscarthaOperationProtocolData.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id006PscarthaOperationProtocolData.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id006PscarthaOperationProtocolData.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var Bh2 = Id006PscarthaOperationProtocolData.Bh2 = (function() {
    function Bh2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh2.prototype._read = function() {
      this.id006PscarthaBlockHeaderAlphaFullHeader = new Id006PscarthaBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh2;
  })();

  var Named = Id006PscarthaOperationProtocolData.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Id006PscarthaInlinedEndorsement = Id006PscarthaOperationProtocolData.Id006PscarthaInlinedEndorsement = (function() {
    function Id006PscarthaInlinedEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaInlinedEndorsement.prototype._read = function() {
      this.id006PscarthaInlinedEndorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id006PscarthaInlinedEndorsementContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id006PscarthaOperationProtocolData.Bool.TRUE) {
        this.signature = this._io.readBytes(64);
      }
    }

    return Id006PscarthaInlinedEndorsement;
  })();

  var Bh20 = Id006PscarthaOperationProtocolData.Bh20 = (function() {
    function Bh20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh20.prototype._read = function() {
      this.lenBh2 = this._io.readU4be();
      if (!(this.lenBh2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh2, this._io, "/types/bh2_0/seq/0");
      }
      this._raw_bh2 = this._io.readBytes(this.lenBh2);
      var _io__raw_bh2 = new KaitaiStream(this._raw_bh2);
      this.bh2 = new Bh2(_io__raw_bh2, this, this._root);
    }

    return Bh20;
  })();

  var Delegation = Id006PscarthaOperationProtocolData.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id006PscarthaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id006PscarthaOperationProtocolData.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var Id006PscarthaOperationAlphaContentsAndSignature = Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsAndSignature = (function() {
    function Id006PscarthaOperationAlphaContentsAndSignature(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaOperationAlphaContentsAndSignature.prototype._read = function() {
      this.contents = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.contents.push(new ContentsEntries(this._io, this, this._root));
        i++;
      }
      this.signature = this._io.readBytes(64);
    }

    return Id006PscarthaOperationAlphaContentsAndSignature;
  })();

  var ContentsEntries = Id006PscarthaOperationProtocolData.ContentsEntries = (function() {
    function ContentsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ContentsEntries.prototype._read = function() {
      this.id006PscarthaOperationAlphaContents = new Id006PscarthaOperationAlphaContents(this._io, this, this._root);
    }

    return ContentsEntries;
  })();

  var Bh10 = Id006PscarthaOperationProtocolData.Bh10 = (function() {
    function Bh10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh10.prototype._read = function() {
      this.lenBh1 = this._io.readU4be();
      if (!(this.lenBh1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh1, this._io, "/types/bh1_0/seq/0");
      }
      this._raw_bh1 = this._io.readBytes(this.lenBh1);
      var _io__raw_bh1 = new KaitaiStream(this._raw_bh1);
      this.bh1 = new Bh1(_io__raw_bh1, this, this._root);
    }

    return Bh10;
  })();

  var BytesDynUint30 = Id006PscarthaOperationProtocolData.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Bh1 = Id006PscarthaOperationProtocolData.Bh1 = (function() {
    function Bh1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh1.prototype._read = function() {
      this.id006PscarthaBlockHeaderAlphaFullHeader = new Id006PscarthaBlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh1;
  })();

  var Op10 = Id006PscarthaOperationProtocolData.Op10 = (function() {
    function Op10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op10.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_0/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op1(_io__raw_op1, this, this._root);
    }

    return Op10;
  })();

  var Id006PscarthaEntrypoint = Id006PscarthaOperationProtocolData.Id006PscarthaEntrypoint = (function() {
    function Id006PscarthaEntrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaEntrypoint.prototype._read = function() {
      this.id006PscarthaEntrypointTag = this._io.readU1();
      if (this.id006PscarthaEntrypointTag == Id006PscarthaOperationProtocolData.Id006PscarthaEntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id006PscarthaEntrypoint;
  })();

  var Origination = Id006PscarthaOperationProtocolData.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id006PscarthaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.balance = new Id006PscarthaMutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id006PscarthaOperationProtocolData.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id006PscarthaScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var Id006PscarthaMutez = Id006PscarthaOperationProtocolData.Id006PscarthaMutez = (function() {
    function Id006PscarthaMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaMutez.prototype._read = function() {
      this.id006PscarthaMutez = new N(this._io, this, this._root);
    }

    return Id006PscarthaMutez;
  })();

  var Op2 = Id006PscarthaOperationProtocolData.Op2 = (function() {
    function Op2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op2.prototype._read = function() {
      this.id006PscarthaInlinedEndorsement = new Id006PscarthaInlinedEndorsement(this._io, this, this._root);
    }

    return Op2;
  })();

  var NChunk = Id006PscarthaOperationProtocolData.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Id006PscarthaBlockHeaderAlphaFullHeader = Id006PscarthaOperationProtocolData.Id006PscarthaBlockHeaderAlphaFullHeader = (function() {
    function Id006PscarthaBlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaBlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id006PscarthaBlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id006PscarthaBlockHeaderAlphaSignedContents = new Id006PscarthaBlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id006PscarthaBlockHeaderAlphaFullHeader;
  })();

  var Named0 = Id006PscarthaOperationProtocolData.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id006PscarthaOperationProtocolData.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id006PscarthaMutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.amount = new Id006PscarthaMutez(this._io, this, this._root);
      this.destination = new Id006PscarthaContractId(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id006PscarthaOperationProtocolData.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Transaction;
  })();

  var Id006PscarthaScriptedContracts = Id006PscarthaOperationProtocolData.Id006PscarthaScriptedContracts = (function() {
    function Id006PscarthaScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id006PscarthaScriptedContracts;
  })();

  var Parameters = Id006PscarthaOperationProtocolData.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id006PscarthaEntrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var Proposals1 = Id006PscarthaOperationProtocolData.Proposals1 = (function() {
    function Proposals1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals1.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposals = new Proposals0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Proposals1;
  })();

  var Ballot = Id006PscarthaOperationProtocolData.Ballot = (function() {
    function Ballot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Ballot.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposal = this._io.readBytes(32);
      this.ballot = this._io.readS1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Ballot;
  })();

  var Id006PscarthaOperationAlphaContents = Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContents = (function() {
    function Id006PscarthaOperationAlphaContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaOperationAlphaContents.prototype._read = function() {
      this.id006PscarthaOperationAlphaContentsTag = this._io.readU1();
      if (this.id006PscarthaOperationAlphaContentsTag == Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsTag.ENDORSEMENT) {
        this.endorsement = this._io.readS4be();
      }
      if (this.id006PscarthaOperationAlphaContentsTag == Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsTag.SEED_NONCE_REVELATION) {
        this.seedNonceRevelation = new SeedNonceRevelation(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationAlphaContentsTag == Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsTag.DOUBLE_ENDORSEMENT_EVIDENCE) {
        this.doubleEndorsementEvidence = new DoubleEndorsementEvidence(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationAlphaContentsTag == Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsTag.DOUBLE_BAKING_EVIDENCE) {
        this.doubleBakingEvidence = new DoubleBakingEvidence(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationAlphaContentsTag == Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsTag.ACTIVATE_ACCOUNT) {
        this.activateAccount = new ActivateAccount(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationAlphaContentsTag == Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsTag.PROPOSALS) {
        this.proposals = new Proposals1(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationAlphaContentsTag == Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsTag.BALLOT) {
        this.ballot = new Ballot(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationAlphaContentsTag == Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsTag.REVEAL) {
        this.reveal = new Reveal(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationAlphaContentsTag == Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationAlphaContentsTag == Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id006PscarthaOperationAlphaContentsTag == Id006PscarthaOperationProtocolData.Id006PscarthaOperationAlphaContentsTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
    }

    return Id006PscarthaOperationAlphaContents;
  })();

  var PublicKeyHash = Id006PscarthaOperationProtocolData.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id006PscarthaOperationProtocolData.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id006PscarthaOperationProtocolData.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id006PscarthaOperationProtocolData.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Proposals = Id006PscarthaOperationProtocolData.Proposals = (function() {
    function Proposals(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals.prototype._read = function() {
      this.proposalsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.proposalsEntries.push(new ProposalsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Proposals;
  })();

  var ProposalsEntries = Id006PscarthaOperationProtocolData.ProposalsEntries = (function() {
    function ProposalsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ProposalsEntries.prototype._read = function() {
      this.protocolHash = this._io.readBytes(32);
    }

    return ProposalsEntries;
  })();

  var Op1 = Id006PscarthaOperationProtocolData.Op1 = (function() {
    function Op1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op1.prototype._read = function() {
      this.id006PscarthaInlinedEndorsement = new Id006PscarthaInlinedEndorsement(this._io, this, this._root);
    }

    return Op1;
  })();

  return Id006PscarthaOperationProtocolData;
})();
return Id006PscarthaOperationProtocolData;
}));
