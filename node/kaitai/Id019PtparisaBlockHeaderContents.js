// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaBlockHeaderContents = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.block_header.contents
 */

var Id019PtparisaBlockHeaderContents = (function() {
  Id019PtparisaBlockHeaderContents.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id019PtparisaBlockHeaderContents.Id019PtparisaPerBlockVotesTag = Object.freeze({
    CASE__0: 0,
    CASE__1: 1,
    CASE__2: 2,
    CASE__4: 4,
    CASE__5: 5,
    CASE__6: 6,
    CASE__8: 8,
    CASE__9: 9,
    CASE__10: 10,

    0: "CASE__0",
    1: "CASE__1",
    2: "CASE__2",
    4: "CASE__4",
    5: "CASE__5",
    6: "CASE__6",
    8: "CASE__8",
    9: "CASE__9",
    10: "CASE__10",
  });

  function Id019PtparisaBlockHeaderContents(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaBlockHeaderContents.prototype._read = function() {
    this.id019PtparisaBlockHeaderAlphaUnsignedContents = new Id019PtparisaBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id019PtparisaBlockHeaderAlphaUnsignedContents = Id019PtparisaBlockHeaderContents.Id019PtparisaBlockHeaderAlphaUnsignedContents = (function() {
    function Id019PtparisaBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id019PtparisaBlockHeaderContents.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.perBlockVotes = new Id019PtparisaPerBlockVotes(this._io, this, this._root);
    }

    return Id019PtparisaBlockHeaderAlphaUnsignedContents;
  })();

  var Id019PtparisaPerBlockVotes = Id019PtparisaBlockHeaderContents.Id019PtparisaPerBlockVotes = (function() {
    function Id019PtparisaPerBlockVotes(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaPerBlockVotes.prototype._read = function() {
      this.id019PtparisaPerBlockVotesTag = this._io.readU1();
    }

    return Id019PtparisaPerBlockVotes;
  })();

  return Id019PtparisaBlockHeaderContents;
})();
return Id019PtparisaBlockHeaderContents;
}));
