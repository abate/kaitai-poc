// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id009PsflorenDelegateFrozenBalanceByCycles = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 009-PsFLoren.delegate.frozen_balance_by_cycles
 */

var Id009PsflorenDelegateFrozenBalanceByCycles = (function() {
  function Id009PsflorenDelegateFrozenBalanceByCycles(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id009PsflorenDelegateFrozenBalanceByCycles.prototype._read = function() {
    this.lenId009PsflorenDelegateFrozenBalanceByCycles = this._io.readU4be();
    if (!(this.lenId009PsflorenDelegateFrozenBalanceByCycles <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId009PsflorenDelegateFrozenBalanceByCycles, this._io, "/seq/0");
    }
    this._raw_id009PsflorenDelegateFrozenBalanceByCycles = this._io.readBytes(this.lenId009PsflorenDelegateFrozenBalanceByCycles);
    var _io__raw_id009PsflorenDelegateFrozenBalanceByCycles = new KaitaiStream(this._raw_id009PsflorenDelegateFrozenBalanceByCycles);
    this.id009PsflorenDelegateFrozenBalanceByCycles = new Id009PsflorenDelegateFrozenBalanceByCycles(_io__raw_id009PsflorenDelegateFrozenBalanceByCycles, this, this._root);
  }

  var N = Id009PsflorenDelegateFrozenBalanceByCycles.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id009PsflorenDelegateFrozenBalanceByCycles = Id009PsflorenDelegateFrozenBalanceByCycles.Id009PsflorenDelegateFrozenBalanceByCycles = (function() {
    function Id009PsflorenDelegateFrozenBalanceByCycles(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenDelegateFrozenBalanceByCycles.prototype._read = function() {
      this.id009PsflorenDelegateFrozenBalanceByCyclesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id009PsflorenDelegateFrozenBalanceByCyclesEntries.push(new Id009PsflorenDelegateFrozenBalanceByCyclesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id009PsflorenDelegateFrozenBalanceByCycles;
  })();

  var Id009PsflorenMutez = Id009PsflorenDelegateFrozenBalanceByCycles.Id009PsflorenMutez = (function() {
    function Id009PsflorenMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenMutez.prototype._read = function() {
      this.id009PsflorenMutez = new N(this._io, this, this._root);
    }

    return Id009PsflorenMutez;
  })();

  var NChunk = Id009PsflorenDelegateFrozenBalanceByCycles.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Id009PsflorenDelegateFrozenBalanceByCyclesEntries = Id009PsflorenDelegateFrozenBalanceByCycles.Id009PsflorenDelegateFrozenBalanceByCyclesEntries = (function() {
    function Id009PsflorenDelegateFrozenBalanceByCyclesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id009PsflorenDelegateFrozenBalanceByCyclesEntries.prototype._read = function() {
      this.cycle = this._io.readS4be();
      this.deposit = new Id009PsflorenMutez(this._io, this, this._root);
      this.fees = new Id009PsflorenMutez(this._io, this, this._root);
      this.rewards = new Id009PsflorenMutez(this._io, this, this._root);
    }

    return Id009PsflorenDelegateFrozenBalanceByCyclesEntries;
  })();

  return Id009PsflorenDelegateFrozenBalanceByCycles;
})();
return Id009PsflorenDelegateFrozenBalanceByCycles;
}));
