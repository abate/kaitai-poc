// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id006PscarthaBlockHeaderShellHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 006-PsCARTHA.block_header.shell_header
 */

var Id006PscarthaBlockHeaderShellHeader = (function() {
  function Id006PscarthaBlockHeaderShellHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaBlockHeaderShellHeader.prototype._read = function() {
    this.id006PscarthaBlockHeaderShellHeader = new BlockHeaderShell(this._io, this, null);
  }

  return Id006PscarthaBlockHeaderShellHeader;
})();
return Id006PscarthaBlockHeaderShellHeader;
}));
