// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './SaplingTransactionRcm'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./SaplingTransactionRcm'));
  } else {
    root.SaplingTransactionPlaintext = factory(root.KaitaiStream, root.SaplingTransactionRcm);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, SaplingTransactionRcm) {
/**
 * Encoding id: sapling.transaction.plaintext
 */

var SaplingTransactionPlaintext = (function() {
  function SaplingTransactionPlaintext(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  SaplingTransactionPlaintext.prototype._read = function() {
    this.diversifier = new SaplingWalletDiversifier(this._io, this, this._root);
    this.amount = this._io.readS8be();
    this.rcm = new SaplingTransactionRcm(this._io, this, null);
    this.memo = new BytesDynUint30(this._io, this, this._root);
  }

  var BytesDynUint30 = SaplingTransactionPlaintext.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var SaplingWalletDiversifier = SaplingTransactionPlaintext.SaplingWalletDiversifier = (function() {
    function SaplingWalletDiversifier(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SaplingWalletDiversifier.prototype._read = function() {
      this.saplingWalletDiversifier = this._io.readBytes(11);
    }

    return SaplingWalletDiversifier;
  })();

  return SaplingTransactionPlaintext;
})();
return SaplingTransactionPlaintext;
}));
