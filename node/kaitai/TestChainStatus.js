// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './TimestampProtocol'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./TimestampProtocol'));
  } else {
    root.TestChainStatus = factory(root.KaitaiStream, root.TimestampProtocol);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, TimestampProtocol) {
/**
 * Encoding id: test_chain_status
 * Description: The status of the test chain: not_running (there is no test chain at the moment), forking (the test chain is being setup), running (the test chain is running).
 */

var TestChainStatus = (function() {
  TestChainStatus.TestChainStatusTag = Object.freeze({
    NOT_RUNNING: 0,
    FORKING: 1,
    RUNNING: 2,

    0: "NOT_RUNNING",
    1: "FORKING",
    2: "RUNNING",
  });

  function TestChainStatus(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  TestChainStatus.prototype._read = function() {
    this.testChainStatusTag = this._io.readU1();
    if (this.testChainStatusTag == TestChainStatus.TestChainStatusTag.FORKING) {
      this.forking = new Forking(this._io, this, this._root);
    }
    if (this.testChainStatusTag == TestChainStatus.TestChainStatusTag.RUNNING) {
      this.running = new Running(this._io, this, this._root);
    }
  }

  var Forking = TestChainStatus.Forking = (function() {
    function Forking(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Forking.prototype._read = function() {
      this.protocol = this._io.readBytes(32);
      this.expiration = new TimestampProtocol(this._io, this, null);
    }

    return Forking;
  })();

  var Running = TestChainStatus.Running = (function() {
    function Running(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Running.prototype._read = function() {
      this.chainId = this._io.readBytes(4);
      this.genesis = this._io.readBytes(32);
      this.protocol = this._io.readBytes(32);
      this.expiration = new TimestampProtocol(this._io, this, null);
    }

    return Running;
  })();

  return TestChainStatus;
})();
return TestChainStatus;
}));
