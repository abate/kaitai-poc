// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id014PtkathmaReceiptBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 014-PtKathma.receipt.balance_updates
 */

var Id014PtkathmaReceiptBalanceUpdates = (function() {
  Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaBondIdTag = Object.freeze({
    TX_ROLLUP_BOND_ID: 0,
    SC_ROLLUP_BOND_ID: 1,

    0: "TX_ROLLUP_BOND_ID",
    1: "SC_ROLLUP_BOND_ID",
  });

  Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceTag = Object.freeze({
    CONTRACT: 0,
    BLOCK_FEES: 2,
    DEPOSITS: 4,
    NONCE_REVELATION_REWARDS: 5,
    DOUBLE_SIGNING_EVIDENCE_REWARDS: 6,
    ENDORSING_REWARDS: 7,
    BAKING_REWARDS: 8,
    BAKING_BONUSES: 9,
    STORAGE_FEES: 11,
    DOUBLE_SIGNING_PUNISHMENTS: 12,
    LOST_ENDORSING_REWARDS: 13,
    LIQUIDITY_BAKING_SUBSIDIES: 14,
    BURNED: 15,
    COMMITMENTS: 16,
    BOOTSTRAP: 17,
    INVOICE: 18,
    INITIAL_COMMITMENTS: 19,
    MINTED: 20,
    FROZEN_BONDS: 21,
    TX_ROLLUP_REJECTION_REWARDS: 22,
    TX_ROLLUP_REJECTION_PUNISHMENTS: 23,
    SC_ROLLUP_REFUTATION_PUNISHMENTS: 24,

    0: "CONTRACT",
    2: "BLOCK_FEES",
    4: "DEPOSITS",
    5: "NONCE_REVELATION_REWARDS",
    6: "DOUBLE_SIGNING_EVIDENCE_REWARDS",
    7: "ENDORSING_REWARDS",
    8: "BAKING_REWARDS",
    9: "BAKING_BONUSES",
    11: "STORAGE_FEES",
    12: "DOUBLE_SIGNING_PUNISHMENTS",
    13: "LOST_ENDORSING_REWARDS",
    14: "LIQUIDITY_BAKING_SUBSIDIES",
    15: "BURNED",
    16: "COMMITMENTS",
    17: "BOOTSTRAP",
    18: "INVOICE",
    19: "INITIAL_COMMITMENTS",
    20: "MINTED",
    21: "FROZEN_BONDS",
    22: "TX_ROLLUP_REJECTION_REWARDS",
    23: "TX_ROLLUP_REJECTION_PUNISHMENTS",
    24: "SC_ROLLUP_REFUTATION_PUNISHMENTS",
  });

  Id014PtkathmaReceiptBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id014PtkathmaReceiptBalanceUpdates.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id014PtkathmaReceiptBalanceUpdates.OriginTag = Object.freeze({
    BLOCK_APPLICATION: 0,
    PROTOCOL_MIGRATION: 1,
    SUBSIDY: 2,
    SIMULATION: 3,

    0: "BLOCK_APPLICATION",
    1: "PROTOCOL_MIGRATION",
    2: "SUBSIDY",
    3: "SIMULATION",
  });

  function Id014PtkathmaReceiptBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id014PtkathmaReceiptBalanceUpdates.prototype._read = function() {
    this.id014PtkathmaOperationMetadataAlphaBalanceUpdates = new Id014PtkathmaOperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Id014PtkathmaRollupAddress = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaRollupAddress = (function() {
    function Id014PtkathmaRollupAddress(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaRollupAddress.prototype._read = function() {
      this.id014PtkathmaRollupAddress = new BytesDynUint30(this._io, this, this._root);
    }

    return Id014PtkathmaRollupAddress;
  })();

  var FrozenBonds = Id014PtkathmaReceiptBalanceUpdates.FrozenBonds = (function() {
    function FrozenBonds(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    FrozenBonds.prototype._read = function() {
      this.contract = new Id014PtkathmaContractId(this._io, this, this._root);
      this.bondId = new Id014PtkathmaBondId(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return FrozenBonds;
  })();

  var Originated = Id014PtkathmaReceiptBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var LostEndorsingRewards = Id014PtkathmaReceiptBalanceUpdates.LostEndorsingRewards = (function() {
    function LostEndorsingRewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    LostEndorsingRewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.participation = this._io.readU1();
      this.revelation = this._io.readU1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return LostEndorsingRewards;
  })();

  var Id014PtkathmaBondId = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaBondId = (function() {
    function Id014PtkathmaBondId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaBondId.prototype._read = function() {
      this.id014PtkathmaBondIdTag = this._io.readU1();
      if (this.id014PtkathmaBondIdTag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaBondIdTag.TX_ROLLUP_BOND_ID) {
        this.txRollupBondId = new Id014PtkathmaTxRollupId(this._io, this, this._root);
      }
      if (this.id014PtkathmaBondIdTag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaBondIdTag.SC_ROLLUP_BOND_ID) {
        this.scRollupBondId = new Id014PtkathmaRollupAddress(this._io, this, this._root);
      }
    }

    /**
     * A tx rollup handle: A tx rollup notation as given to an RPC or inside scripts, is a base58 tx rollup hash
     */

    /**
     * A smart contract rollup address: A smart contract rollup is identified by a base58 address starting with scr1
     */

    return Id014PtkathmaBondId;
  })();

  var Id014PtkathmaOperationMetadataAlphaBalanceUpdates = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceUpdates = (function() {
    function Id014PtkathmaOperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaOperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id014PtkathmaOperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id014PtkathmaOperationMetadataAlphaBalanceUpdatesEntries.push(new Id014PtkathmaOperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id014PtkathmaOperationMetadataAlphaBalanceUpdates;
  })();

  var Id014PtkathmaContractId = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaContractId = (function() {
    function Id014PtkathmaContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaContractId.prototype._read = function() {
      this.id014PtkathmaContractIdTag = this._io.readU1();
      if (this.id014PtkathmaContractIdTag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id014PtkathmaContractIdTag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id014PtkathmaContractId;
  })();

  var Id014PtkathmaOperationMetadataAlphaBalance = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalance = (function() {
    function Id014PtkathmaOperationMetadataAlphaBalance(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaOperationMetadataAlphaBalance.prototype._read = function() {
      this.id014PtkathmaOperationMetadataAlphaBalanceTag = this._io.readU1();
      if (this.id014PtkathmaOperationMetadataAlphaBalanceTag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceTag.CONTRACT) {
        this.contract = new Id014PtkathmaContractId(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationMetadataAlphaBalanceTag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceTag.DEPOSITS) {
        this.deposits = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationMetadataAlphaBalanceTag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceTag.LOST_ENDORSING_REWARDS) {
        this.lostEndorsingRewards = new LostEndorsingRewards(this._io, this, this._root);
      }
      if (this.id014PtkathmaOperationMetadataAlphaBalanceTag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceTag.COMMITMENTS) {
        this.commitments = this._io.readBytes(20);
      }
      if (this.id014PtkathmaOperationMetadataAlphaBalanceTag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceTag.FROZEN_BONDS) {
        this.frozenBonds = new FrozenBonds(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id014PtkathmaOperationMetadataAlphaBalance;
  })();

  var Id014PtkathmaOperationMetadataAlphaBalanceUpdates0 = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id014PtkathmaOperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaOperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId014PtkathmaOperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId014PtkathmaOperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId014PtkathmaOperationMetadataAlphaBalanceUpdates, this._io, "/types/id_014__ptkathma__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id014PtkathmaOperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId014PtkathmaOperationMetadataAlphaBalanceUpdates);
      var _io__raw_id014PtkathmaOperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id014PtkathmaOperationMetadataAlphaBalanceUpdates);
      this.id014PtkathmaOperationMetadataAlphaBalanceUpdates = new Id014PtkathmaOperationMetadataAlphaBalanceUpdates(_io__raw_id014PtkathmaOperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id014PtkathmaOperationMetadataAlphaBalanceUpdates0;
  })();

  var Id014PtkathmaOperationMetadataAlphaBalanceUpdatesEntries = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id014PtkathmaOperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaOperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id014PtkathmaOperationMetadataAlphaBalance = new Id014PtkathmaOperationMetadataAlphaBalance(this._io, this, this._root);
      this.id014PtkathmaOperationMetadataAlphaBalanceUpdate = new Id014PtkathmaOperationMetadataAlphaBalanceUpdate(this._io, this, this._root);
      this.id014PtkathmaOperationMetadataAlphaUpdateOrigin = new Id014PtkathmaOperationMetadataAlphaUpdateOrigin(this._io, this, this._root);
    }

    return Id014PtkathmaOperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var BytesDynUint30 = Id014PtkathmaReceiptBalanceUpdates.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Id014PtkathmaOperationMetadataAlphaBalanceUpdate = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceUpdate = (function() {
    function Id014PtkathmaOperationMetadataAlphaBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaOperationMetadataAlphaBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id014PtkathmaOperationMetadataAlphaBalanceUpdate;
  })();

  var Id014PtkathmaTxRollupId = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaTxRollupId = (function() {
    function Id014PtkathmaTxRollupId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaTxRollupId.prototype._read = function() {
      this.rollupHash = this._io.readBytes(20);
    }

    return Id014PtkathmaTxRollupId;
  })();

  var Id014PtkathmaOperationMetadataAlphaUpdateOrigin = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaUpdateOrigin = (function() {
    function Id014PtkathmaOperationMetadataAlphaUpdateOrigin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaOperationMetadataAlphaUpdateOrigin.prototype._read = function() {
      this.origin = this._io.readU1();
    }

    return Id014PtkathmaOperationMetadataAlphaUpdateOrigin;
  })();

  var PublicKeyHash = Id014PtkathmaReceiptBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id014PtkathmaReceiptBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id014PtkathmaReceiptBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id014PtkathmaReceiptBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id014PtkathmaReceiptBalanceUpdates;
})();
return Id014PtkathmaReceiptBalanceUpdates;
}));
