// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id006PscarthaBlockHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 006-PsCARTHA.block_header
 */

var Id006PscarthaBlockHeader = (function() {
  Id006PscarthaBlockHeader.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id006PscarthaBlockHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaBlockHeader.prototype._read = function() {
    this.id006PscarthaBlockHeaderAlphaFullHeader = new Id006PscarthaBlockHeaderAlphaFullHeader(this._io, this, this._root);
  }

  var Id006PscarthaBlockHeaderAlphaFullHeader = Id006PscarthaBlockHeader.Id006PscarthaBlockHeaderAlphaFullHeader = (function() {
    function Id006PscarthaBlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaBlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id006PscarthaBlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id006PscarthaBlockHeaderAlphaSignedContents = new Id006PscarthaBlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id006PscarthaBlockHeaderAlphaFullHeader;
  })();

  var Id006PscarthaBlockHeaderAlphaSignedContents = Id006PscarthaBlockHeader.Id006PscarthaBlockHeaderAlphaSignedContents = (function() {
    function Id006PscarthaBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id006PscarthaBlockHeaderAlphaUnsignedContents = new Id006PscarthaBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id006PscarthaBlockHeaderAlphaSignedContents;
  })();

  var Id006PscarthaBlockHeaderAlphaUnsignedContents = Id006PscarthaBlockHeader.Id006PscarthaBlockHeaderAlphaUnsignedContents = (function() {
    function Id006PscarthaBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id006PscarthaBlockHeader.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
    }

    return Id006PscarthaBlockHeaderAlphaUnsignedContents;
  })();

  return Id006PscarthaBlockHeader;
})();
return Id006PscarthaBlockHeader;
}));
