// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaParameters = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.parameters
 */

var Id019PtparisaParameters = (function() {
  Id019PtparisaParameters.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  Id019PtparisaParameters.PvmKind = Object.freeze({
    ARITH: 0,
    WASM_2_0_0: 1,
    RISCV: 2,

    0: "ARITH",
    1: "WASM_2_0_0",
    2: "RISCV",
  });

  Id019PtparisaParameters.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  Id019PtparisaParameters.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id019PtparisaParameters.BootstrapAccountsEltTag = Object.freeze({
    PUBLIC_KEY_KNOWN: 0,
    PUBLIC_KEY_UNKNOWN: 1,
    PUBLIC_KEY_KNOWN_WITH_DELEGATE: 2,
    PUBLIC_KEY_UNKNOWN_WITH_DELEGATE: 3,
    PUBLIC_KEY_KNOWN_WITH_CONSENSUS_KEY: 4,

    0: "PUBLIC_KEY_KNOWN",
    1: "PUBLIC_KEY_UNKNOWN",
    2: "PUBLIC_KEY_KNOWN_WITH_DELEGATE",
    3: "PUBLIC_KEY_UNKNOWN_WITH_DELEGATE",
    4: "PUBLIC_KEY_KNOWN_WITH_CONSENSUS_KEY",
  });

  function Id019PtparisaParameters(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaParameters.prototype._read = function() {
    this.bootstrapAccounts = new BootstrapAccounts0(this._io, this, this._root);
    this.bootstrapContracts = new BootstrapContracts0(this._io, this, this._root);
    this.bootstrapSmartRollups = new BootstrapSmartRollups0(this._io, this, this._root);
    this.commitments = new Commitments0(this._io, this, this._root);
    this.securityDepositRampUpCyclesTag = this._io.readU1();
    if (this.securityDepositRampUpCyclesTag == Id019PtparisaParameters.Bool.TRUE) {
      this.securityDepositRampUpCycles = new Int31(this._io, this, this._root);
    }
    this.noRewardCyclesTag = this._io.readU1();
    if (this.noRewardCyclesTag == Id019PtparisaParameters.Bool.TRUE) {
      this.noRewardCycles = new Int31(this._io, this, this._root);
    }
    this.consensusRightsDelay = this._io.readU1();
    this.blocksPreservationCycles = this._io.readU1();
    this.delegateParametersActivationDelay = this._io.readU1();
    this.blocksPerCycle = this._io.readS4be();
    this.blocksPerCommitment = this._io.readS4be();
    this.nonceRevelationThreshold = this._io.readS4be();
    this.cyclesPerVotingPeriod = this._io.readS4be();
    this.hardGasLimitPerOperation = new Z(this._io, this, this._root);
    this.hardGasLimitPerBlock = new Z(this._io, this, this._root);
    this.proofOfWorkThreshold = this._io.readS8be();
    this.minimalStake = new Id019PtparisaMutez(this._io, this, this._root);
    this.minimalFrozenStake = new Id019PtparisaMutez(this._io, this, this._root);
    this.vdfDifficulty = this._io.readS8be();
    this.originationSize = new Int31(this._io, this, this._root);
    this.issuanceWeights = new IssuanceWeights(this._io, this, this._root);
    this.costPerByte = new Id019PtparisaMutez(this._io, this, this._root);
    this.hardStorageLimitPerOperation = new Z(this._io, this, this._root);
    this.quorumMin = this._io.readS4be();
    this.quorumMax = this._io.readS4be();
    this.minProposalQuorum = this._io.readS4be();
    this.liquidityBakingSubsidy = new Id019PtparisaMutez(this._io, this, this._root);
    this.liquidityBakingToggleEmaThreshold = this._io.readS4be();
    this.maxOperationsTimeToLive = this._io.readS2be();
    this.minimalBlockDelay = this._io.readS8be();
    this.delayIncrementPerRound = this._io.readS8be();
    this.consensusCommitteeSize = new Int31(this._io, this, this._root);
    this.consensusThreshold = new Int31(this._io, this, this._root);
    this.minimalParticipationRatio = new MinimalParticipationRatio(this._io, this, this._root);
    this.limitOfDelegationOverBaking = this._io.readU1();
    this.percentageOfFrozenDepositsSlashedPerDoubleBaking = this._io.readU2be();
    this.percentageOfFrozenDepositsSlashedPerDoubleAttestation = this._io.readU2be();
    this.maxSlashingPerBlock = this._io.readU2be();
    this.maxSlashingThreshold = new Int31(this._io, this, this._root);
    this.testnetDictatorTag = this._io.readU1();
    if (this.testnetDictatorTag == Id019PtparisaParameters.Bool.TRUE) {
      this.testnetDictator = new PublicKeyHash(this._io, this, this._root);
    }
    this.initialSeedTag = this._io.readU1();
    if (this.initialSeedTag == Id019PtparisaParameters.Bool.TRUE) {
      this.initialSeed = this._io.readBytes(32);
    }
    this.cacheScriptSize = new Int31(this._io, this, this._root);
    this.cacheStakeDistributionCycles = this._io.readS1();
    this.cacheSamplerStateCycles = this._io.readS1();
    this.dalParametric = new DalParametric(this._io, this, this._root);
    this.smartRollupArithPvmEnable = this._io.readU1();
    this.smartRollupOriginationSize = new Int31(this._io, this, this._root);
    this.smartRollupChallengeWindowInBlocks = new Int31(this._io, this, this._root);
    this.smartRollupStakeAmount = new Id019PtparisaMutez(this._io, this, this._root);
    this.smartRollupCommitmentPeriodInBlocks = new Int31(this._io, this, this._root);
    this.smartRollupMaxLookaheadInBlocks = this._io.readS4be();
    this.smartRollupMaxActiveOutboxLevels = this._io.readS4be();
    this.smartRollupMaxOutboxMessagesPerLevel = new Int31(this._io, this, this._root);
    this.smartRollupNumberOfSectionsInDissection = this._io.readU1();
    this.smartRollupTimeoutPeriodInBlocks = new Int31(this._io, this, this._root);
    this.smartRollupMaxNumberOfCementedCommitments = new Int31(this._io, this, this._root);
    this.smartRollupMaxNumberOfParallelGames = new Int31(this._io, this, this._root);
    this.smartRollupRevealActivationLevel = new SmartRollupRevealActivationLevel(this._io, this, this._root);
    this.smartRollupPrivateEnable = this._io.readU1();
    this.smartRollupRiscvPvmEnable = this._io.readU1();
    this.zkRollupEnable = this._io.readU1();
    this.zkRollupOriginationSize = new Int31(this._io, this, this._root);
    this.zkRollupMinPendingToProcess = new Int31(this._io, this, this._root);
    this.zkRollupMaxTicketPayloadSize = new Int31(this._io, this, this._root);
    this.globalLimitOfStakingOverBaking = this._io.readU1();
    this.edgeOfStakingOverDelegation = this._io.readU1();
    this.adaptiveIssuanceLaunchEmaThreshold = this._io.readS4be();
    this.adaptiveRewardsParams = new AdaptiveRewardsParams(this._io, this, this._root);
    this.adaptiveIssuanceActivationVoteEnable = this._io.readU1();
    this.autostakingEnable = this._io.readU1();
    this.adaptiveIssuanceForceActivation = this._io.readU1();
    this.nsEnable = this._io.readU1();
    this.directTicketSpendingEnable = this._io.readU1();
  }

  var BootstrapSmartRollups0 = Id019PtparisaParameters.BootstrapSmartRollups0 = (function() {
    function BootstrapSmartRollups0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapSmartRollups0.prototype._read = function() {
      this.lenBootstrapSmartRollups = this._io.readU4be();
      if (!(this.lenBootstrapSmartRollups <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBootstrapSmartRollups, this._io, "/types/bootstrap_smart_rollups_0/seq/0");
      }
      this._raw_bootstrapSmartRollups = this._io.readBytes(this.lenBootstrapSmartRollups);
      var _io__raw_bootstrapSmartRollups = new KaitaiStream(this._raw_bootstrapSmartRollups);
      this.bootstrapSmartRollups = new BootstrapSmartRollups(_io__raw_bootstrapSmartRollups, this, this._root);
    }

    return BootstrapSmartRollups0;
  })();

  var AdaptiveRewardsParams = Id019PtparisaParameters.AdaptiveRewardsParams = (function() {
    function AdaptiveRewardsParams(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AdaptiveRewardsParams.prototype._read = function() {
      this.issuanceRatioFinalMin = new IssuanceRatioFinalMin(this._io, this, this._root);
      this.issuanceRatioFinalMax = new IssuanceRatioFinalMax(this._io, this, this._root);
      this.issuanceRatioInitialMin = new IssuanceRatioInitialMin(this._io, this, this._root);
      this.issuanceRatioInitialMax = new IssuanceRatioInitialMax(this._io, this, this._root);
      this.initialPeriod = this._io.readU1();
      this.transitionPeriod = this._io.readU1();
      this.maxBonus = this._io.readS8be();
      this.growthRate = new GrowthRate(this._io, this, this._root);
      this.centerDz = new CenterDz(this._io, this, this._root);
      this.radiusDz = new RadiusDz(this._io, this, this._root);
    }

    return AdaptiveRewardsParams;
  })();

  var Whitelist0 = Id019PtparisaParameters.Whitelist0 = (function() {
    function Whitelist0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Whitelist0.prototype._read = function() {
      this.lenWhitelist = this._io.readU4be();
      if (!(this.lenWhitelist <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenWhitelist, this._io, "/types/whitelist_0/seq/0");
      }
      this._raw_whitelist = this._io.readBytes(this.lenWhitelist);
      var _io__raw_whitelist = new KaitaiStream(this._raw_whitelist);
      this.whitelist = new Whitelist(_io__raw_whitelist, this, this._root);
    }

    return Whitelist0;
  })();

  var CenterDz = Id019PtparisaParameters.CenterDz = (function() {
    function CenterDz(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CenterDz.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return CenterDz;
  })();

  var IssuanceWeights = Id019PtparisaParameters.IssuanceWeights = (function() {
    function IssuanceWeights(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IssuanceWeights.prototype._read = function() {
      this.baseTotalIssuedPerMinute = new Id019PtparisaMutez(this._io, this, this._root);
      this.bakingRewardFixedPortionWeight = new Int31(this._io, this, this._root);
      this.bakingRewardBonusWeight = new Int31(this._io, this, this._root);
      this.attestingRewardWeight = new Int31(this._io, this, this._root);
      this.seedNonceRevelationTipWeight = new Int31(this._io, this, this._root);
      this.vdfRevelationTipWeight = new Int31(this._io, this, this._root);
    }

    return IssuanceWeights;
  })();

  var PublicKeyKnown = Id019PtparisaParameters.PublicKeyKnown = (function() {
    function PublicKeyKnown(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyKnown.prototype._read = function() {
      this.publicKeyKnownField0 = new PublicKey(this._io, this, this._root);
      this.publicKeyKnownField1 = new Id019PtparisaMutez(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key
     * 
     * signature__public_key
     */

    /**
     * id_019__ptparisa__mutez
     */

    return PublicKeyKnown;
  })();

  var Commitments = Id019PtparisaParameters.Commitments = (function() {
    function Commitments(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments.prototype._read = function() {
      this.commitmentsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.commitmentsEntries.push(new CommitmentsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Commitments;
  })();

  var DalParametric = Id019PtparisaParameters.DalParametric = (function() {
    function DalParametric(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DalParametric.prototype._read = function() {
      this.featureEnable = this._io.readU1();
      this.incentivesEnable = this._io.readU1();
      this.numberOfSlots = this._io.readU2be();
      this.attestationLag = this._io.readU1();
      this.attestationThreshold = this._io.readU1();
      this.redundancyFactor = this._io.readU1();
      this.pageSize = this._io.readU2be();
      this.slotSize = new Int31(this._io, this, this._root);
      this.numberOfShards = this._io.readU2be();
    }

    return DalParametric;
  })();

  var N = Id019PtparisaParameters.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var CommitmentsEntries = Id019PtparisaParameters.CommitmentsEntries = (function() {
    function CommitmentsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    CommitmentsEntries.prototype._read = function() {
      this.commitmentsEltField0 = this._io.readBytes(20);
      this.commitmentsEltField1 = new Id019PtparisaMutez(this._io, this, this._root);
    }

    /**
     * blinded__public__key__hash
     */

    /**
     * id_019__ptparisa__mutez
     */

    return CommitmentsEntries;
  })();

  var Commitments0 = Id019PtparisaParameters.Commitments0 = (function() {
    function Commitments0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Commitments0.prototype._read = function() {
      this.lenCommitments = this._io.readU4be();
      if (!(this.lenCommitments <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenCommitments, this._io, "/types/commitments_0/seq/0");
      }
      this._raw_commitments = this._io.readBytes(this.lenCommitments);
      var _io__raw_commitments = new KaitaiStream(this._raw_commitments);
      this.commitments = new Commitments(_io__raw_commitments, this, this._root);
    }

    return Commitments0;
  })();

  var MinimalParticipationRatio = Id019PtparisaParameters.MinimalParticipationRatio = (function() {
    function MinimalParticipationRatio(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    MinimalParticipationRatio.prototype._read = function() {
      this.numerator = this._io.readU2be();
      this.denominator = this._io.readU2be();
    }

    return MinimalParticipationRatio;
  })();

  var Id019PtparisaScriptedContracts = Id019PtparisaParameters.Id019PtparisaScriptedContracts = (function() {
    function Id019PtparisaScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id019PtparisaScriptedContracts;
  })();

  var WhitelistEntries = Id019PtparisaParameters.WhitelistEntries = (function() {
    function WhitelistEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    WhitelistEntries.prototype._read = function() {
      this.signaturePublicKeyHash = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return WhitelistEntries;
  })();

  var BootstrapAccounts0 = Id019PtparisaParameters.BootstrapAccounts0 = (function() {
    function BootstrapAccounts0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccounts0.prototype._read = function() {
      this.lenBootstrapAccounts = this._io.readU4be();
      if (!(this.lenBootstrapAccounts <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBootstrapAccounts, this._io, "/types/bootstrap_accounts_0/seq/0");
      }
      this._raw_bootstrapAccounts = this._io.readBytes(this.lenBootstrapAccounts);
      var _io__raw_bootstrapAccounts = new KaitaiStream(this._raw_bootstrapAccounts);
      this.bootstrapAccounts = new BootstrapAccounts(_io__raw_bootstrapAccounts, this, this._root);
    }

    return BootstrapAccounts0;
  })();

  var PublicKey = Id019PtparisaParameters.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id019PtparisaParameters.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id019PtparisaParameters.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id019PtparisaParameters.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id019PtparisaParameters.PublicKeyTag.BLS) {
        this.bls = this._io.readBytes(48);
      }
    }

    return PublicKey;
  })();

  var PublicKeyUnknown = Id019PtparisaParameters.PublicKeyUnknown = (function() {
    function PublicKeyUnknown(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyUnknown.prototype._read = function() {
      this.publicKeyUnknownField0 = new PublicKeyHash(this._io, this, this._root);
      this.publicKeyUnknownField1 = new Id019PtparisaMutez(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     * 
     * signature__public_key_hash
     */

    /**
     * id_019__ptparisa__mutez
     */

    return PublicKeyUnknown;
  })();

  var GrowthRate = Id019PtparisaParameters.GrowthRate = (function() {
    function GrowthRate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    GrowthRate.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return GrowthRate;
  })();

  var BootstrapSmartRollups = Id019PtparisaParameters.BootstrapSmartRollups = (function() {
    function BootstrapSmartRollups(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapSmartRollups.prototype._read = function() {
      this.bootstrapSmartRollupsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bootstrapSmartRollupsEntries.push(new BootstrapSmartRollupsEntries(this._io, this, this._root));
        i++;
      }
    }

    return BootstrapSmartRollups;
  })();

  var PublicKeyUnknownWithDelegate = Id019PtparisaParameters.PublicKeyUnknownWithDelegate = (function() {
    function PublicKeyUnknownWithDelegate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyUnknownWithDelegate.prototype._read = function() {
      this.publicKeyUnknownWithDelegateField0 = new PublicKeyHash(this._io, this, this._root);
      this.publicKeyUnknownWithDelegateField1 = new Id019PtparisaMutez(this._io, this, this._root);
      this.publicKeyUnknownWithDelegateField2 = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     * 
     * signature__public_key_hash
     */

    /**
     * id_019__ptparisa__mutez
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     * 
     * signature__public_key_hash
     */

    return PublicKeyUnknownWithDelegate;
  })();

  var BootstrapAccountsEntries = Id019PtparisaParameters.BootstrapAccountsEntries = (function() {
    function BootstrapAccountsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccountsEntries.prototype._read = function() {
      this.bootstrapAccountsEltTag = this._io.readU1();
      if (this.bootstrapAccountsEltTag == Id019PtparisaParameters.BootstrapAccountsEltTag.PUBLIC_KEY_KNOWN) {
        this.publicKeyKnown = new PublicKeyKnown(this._io, this, this._root);
      }
      if (this.bootstrapAccountsEltTag == Id019PtparisaParameters.BootstrapAccountsEltTag.PUBLIC_KEY_UNKNOWN) {
        this.publicKeyUnknown = new PublicKeyUnknown(this._io, this, this._root);
      }
      if (this.bootstrapAccountsEltTag == Id019PtparisaParameters.BootstrapAccountsEltTag.PUBLIC_KEY_KNOWN_WITH_DELEGATE) {
        this.publicKeyKnownWithDelegate = new PublicKeyKnownWithDelegate(this._io, this, this._root);
      }
      if (this.bootstrapAccountsEltTag == Id019PtparisaParameters.BootstrapAccountsEltTag.PUBLIC_KEY_UNKNOWN_WITH_DELEGATE) {
        this.publicKeyUnknownWithDelegate = new PublicKeyUnknownWithDelegate(this._io, this, this._root);
      }
      if (this.bootstrapAccountsEltTag == Id019PtparisaParameters.BootstrapAccountsEltTag.PUBLIC_KEY_KNOWN_WITH_CONSENSUS_KEY) {
        this.publicKeyKnownWithConsensusKey = new PublicKeyKnownWithConsensusKey(this._io, this, this._root);
      }
    }

    return BootstrapAccountsEntries;
  })();

  var BootstrapContracts0 = Id019PtparisaParameters.BootstrapContracts0 = (function() {
    function BootstrapContracts0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContracts0.prototype._read = function() {
      this.lenBootstrapContracts = this._io.readU4be();
      if (!(this.lenBootstrapContracts <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBootstrapContracts, this._io, "/types/bootstrap_contracts_0/seq/0");
      }
      this._raw_bootstrapContracts = this._io.readBytes(this.lenBootstrapContracts);
      var _io__raw_bootstrapContracts = new KaitaiStream(this._raw_bootstrapContracts);
      this.bootstrapContracts = new BootstrapContracts(_io__raw_bootstrapContracts, this, this._root);
    }

    return BootstrapContracts0;
  })();

  var SmartRollupRevealActivationLevel = Id019PtparisaParameters.SmartRollupRevealActivationLevel = (function() {
    function SmartRollupRevealActivationLevel(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SmartRollupRevealActivationLevel.prototype._read = function() {
      this.rawData = this._io.readS4be();
      this.metadata = this._io.readS4be();
      this.dalPage = this._io.readS4be();
      this.dalParameters = this._io.readS4be();
      this.dalAttestedSlotsValidityLag = new Int31(this._io, this, this._root);
    }

    return SmartRollupRevealActivationLevel;
  })();

  var Id019PtparisaMutez = Id019PtparisaParameters.Id019PtparisaMutez = (function() {
    function Id019PtparisaMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaMutez.prototype._read = function() {
      this.id019PtparisaMutez = new N(this._io, this, this._root);
    }

    return Id019PtparisaMutez;
  })();

  var IssuanceRatioFinalMin = Id019PtparisaParameters.IssuanceRatioFinalMin = (function() {
    function IssuanceRatioFinalMin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IssuanceRatioFinalMin.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return IssuanceRatioFinalMin;
  })();

  var IssuanceRatioInitialMax = Id019PtparisaParameters.IssuanceRatioInitialMax = (function() {
    function IssuanceRatioInitialMax(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IssuanceRatioInitialMax.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return IssuanceRatioInitialMax;
  })();

  var BootstrapSmartRollupsEntries = Id019PtparisaParameters.BootstrapSmartRollupsEntries = (function() {
    function BootstrapSmartRollupsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapSmartRollupsEntries.prototype._read = function() {
      this.address = this._io.readBytes(20);
      this.pvmKind = this._io.readU1();
      this.kernel = new BytesDynUint30(this._io, this, this._root);
      this.parametersTy = new BytesDynUint30(this._io, this, this._root);
      this.whitelistTag = this._io.readU1();
      if (this.whitelistTag == Id019PtparisaParameters.Bool.TRUE) {
        this.whitelist = new Whitelist0(this._io, this, this._root);
      }
    }

    return BootstrapSmartRollupsEntries;
  })();

  var BytesDynUint30 = Id019PtparisaParameters.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var PublicKeyKnownWithDelegate = Id019PtparisaParameters.PublicKeyKnownWithDelegate = (function() {
    function PublicKeyKnownWithDelegate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyKnownWithDelegate.prototype._read = function() {
      this.publicKeyKnownWithDelegateField0 = new PublicKey(this._io, this, this._root);
      this.publicKeyKnownWithDelegateField1 = new Id019PtparisaMutez(this._io, this, this._root);
      this.publicKeyKnownWithDelegateField2 = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key
     * 
     * signature__public_key
     */

    /**
     * id_019__ptparisa__mutez
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     * 
     * signature__public_key_hash
     */

    return PublicKeyKnownWithDelegate;
  })();

  var IssuanceRatioInitialMin = Id019PtparisaParameters.IssuanceRatioInitialMin = (function() {
    function IssuanceRatioInitialMin(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IssuanceRatioInitialMin.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return IssuanceRatioInitialMin;
  })();

  var Whitelist = Id019PtparisaParameters.Whitelist = (function() {
    function Whitelist(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Whitelist.prototype._read = function() {
      this.whitelistEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.whitelistEntries.push(new WhitelistEntries(this._io, this, this._root));
        i++;
      }
    }

    return Whitelist;
  })();

  var BootstrapContractsEntries = Id019PtparisaParameters.BootstrapContractsEntries = (function() {
    function BootstrapContractsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContractsEntries.prototype._read = function() {
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id019PtparisaParameters.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.amount = new Id019PtparisaMutez(this._io, this, this._root);
      this.script = new Id019PtparisaScriptedContracts(this._io, this, this._root);
      this.hashTag = this._io.readU1();
      if (this.hashTag == Id019PtparisaParameters.Bool.TRUE) {
        this.hash = this._io.readBytes(20);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return BootstrapContractsEntries;
  })();

  var BootstrapContracts = Id019PtparisaParameters.BootstrapContracts = (function() {
    function BootstrapContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapContracts.prototype._read = function() {
      this.bootstrapContractsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bootstrapContractsEntries.push(new BootstrapContractsEntries(this._io, this, this._root));
        i++;
      }
    }

    return BootstrapContracts;
  })();

  var BootstrapAccounts = Id019PtparisaParameters.BootstrapAccounts = (function() {
    function BootstrapAccounts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BootstrapAccounts.prototype._read = function() {
      this.bootstrapAccountsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.bootstrapAccountsEntries.push(new BootstrapAccountsEntries(this._io, this, this._root));
        i++;
      }
    }

    return BootstrapAccounts;
  })();

  var RadiusDz = Id019PtparisaParameters.RadiusDz = (function() {
    function RadiusDz(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RadiusDz.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return RadiusDz;
  })();

  var Int31 = Id019PtparisaParameters.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id019PtparisaParameters.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var PublicKeyKnownWithConsensusKey = Id019PtparisaParameters.PublicKeyKnownWithConsensusKey = (function() {
    function PublicKeyKnownWithConsensusKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyKnownWithConsensusKey.prototype._read = function() {
      this.publicKeyKnownWithConsensusKeyField0 = new PublicKey(this._io, this, this._root);
      this.publicKeyKnownWithConsensusKeyField1 = new Id019PtparisaMutez(this._io, this, this._root);
      this.publicKeyKnownWithConsensusKeyField2 = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key
     * 
     * signature__public_key
     */

    /**
     * id_019__ptparisa__mutez
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     * 
     * signature__public_key
     */

    return PublicKeyKnownWithConsensusKey;
  })();

  var PublicKeyHash = Id019PtparisaParameters.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id019PtparisaParameters.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaParameters.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaParameters.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaParameters.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var IssuanceRatioFinalMax = Id019PtparisaParameters.IssuanceRatioFinalMax = (function() {
    function IssuanceRatioFinalMax(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    IssuanceRatioFinalMax.prototype._read = function() {
      this.numerator = new Z(this._io, this, this._root);
      this.denominator = new Z(this._io, this, this._root);
    }

    return IssuanceRatioFinalMax;
  })();

  var Z = Id019PtparisaParameters.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  /**
   * A Ed25519, Secp256k1, P256, or BLS public key hash
   */

  return Id019PtparisaParameters;
})();
return Id019PtparisaParameters;
}));
