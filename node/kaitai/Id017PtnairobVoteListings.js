// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id017PtnairobVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 017-PtNairob.vote.listings
 */

var Id017PtnairobVoteListings = (function() {
  Id017PtnairobVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function Id017PtnairobVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id017PtnairobVoteListings.prototype._read = function() {
    this.lenId017PtnairobVoteListings = this._io.readU4be();
    if (!(this.lenId017PtnairobVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId017PtnairobVoteListings, this._io, "/seq/0");
    }
    this._raw_id017PtnairobVoteListings = this._io.readBytes(this.lenId017PtnairobVoteListings);
    var _io__raw_id017PtnairobVoteListings = new KaitaiStream(this._raw_id017PtnairobVoteListings);
    this.id017PtnairobVoteListings = new Id017PtnairobVoteListings(_io__raw_id017PtnairobVoteListings, this, this._root);
  }

  var Id017PtnairobVoteListings = Id017PtnairobVoteListings.Id017PtnairobVoteListings = (function() {
    function Id017PtnairobVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobVoteListings.prototype._read = function() {
      this.id017PtnairobVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id017PtnairobVoteListingsEntries.push(new Id017PtnairobVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id017PtnairobVoteListings;
  })();

  var Id017PtnairobVoteListingsEntries = Id017PtnairobVoteListings.Id017PtnairobVoteListingsEntries = (function() {
    function Id017PtnairobVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id017PtnairobVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.votingPower = this._io.readS8be();
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id017PtnairobVoteListingsEntries;
  })();

  var PublicKeyHash = Id017PtnairobVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id017PtnairobVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id017PtnairobVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id017PtnairobVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id017PtnairobVoteListings.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id017PtnairobVoteListings;
})();
return Id017PtnairobVoteListings;
}));
