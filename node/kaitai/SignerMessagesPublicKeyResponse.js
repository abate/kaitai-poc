// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.SignerMessagesPublicKeyResponse = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: signer_messages.public_key.response
 */

var SignerMessagesPublicKeyResponse = (function() {
  SignerMessagesPublicKeyResponse.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function SignerMessagesPublicKeyResponse(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  SignerMessagesPublicKeyResponse.prototype._read = function() {
    this.pubkey = new PublicKey(this._io, this, this._root);
  }

  var PublicKey = SignerMessagesPublicKeyResponse.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == SignerMessagesPublicKeyResponse.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == SignerMessagesPublicKeyResponse.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == SignerMessagesPublicKeyResponse.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == SignerMessagesPublicKeyResponse.PublicKeyTag.BLS) {
        this.bls = this._io.readBytes(48);
      }
    }

    return PublicKey;
  })();

  /**
   * A Ed25519, Secp256k1, or P256 public key
   */

  return SignerMessagesPublicKeyResponse;
})();
return SignerMessagesPublicKeyResponse;
}));
