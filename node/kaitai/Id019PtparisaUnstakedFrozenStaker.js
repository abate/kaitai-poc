// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaUnstakedFrozenStaker = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.unstaked_frozen_staker
 */

var Id019PtparisaUnstakedFrozenStaker = (function() {
  Id019PtparisaUnstakedFrozenStaker.Id019PtparisaContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id019PtparisaUnstakedFrozenStaker.Id019PtparisaStakerTag = Object.freeze({
    SINGLE: 0,
    SHARED: 1,

    0: "SINGLE",
    1: "SHARED",
  });

  Id019PtparisaUnstakedFrozenStaker.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function Id019PtparisaUnstakedFrozenStaker(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaUnstakedFrozenStaker.prototype._read = function() {
    this.id019PtparisaStaker = new Id019PtparisaStaker(this._io, this, this._root);
  }

  var Id019PtparisaStaker = Id019PtparisaUnstakedFrozenStaker.Id019PtparisaStaker = (function() {
    function Id019PtparisaStaker(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaStaker.prototype._read = function() {
      this.id019PtparisaStakerTag = this._io.readU1();
      if (this.id019PtparisaStakerTag == Id019PtparisaUnstakedFrozenStaker.Id019PtparisaStakerTag.SINGLE) {
        this.single = new Single(this._io, this, this._root);
      }
      if (this.id019PtparisaStakerTag == Id019PtparisaUnstakedFrozenStaker.Id019PtparisaStakerTag.SHARED) {
        this.shared = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id019PtparisaStaker;
  })();

  var Originated = Id019PtparisaUnstakedFrozenStaker.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Id019PtparisaContractId = Id019PtparisaUnstakedFrozenStaker.Id019PtparisaContractId = (function() {
    function Id019PtparisaContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaContractId.prototype._read = function() {
      this.id019PtparisaContractIdTag = this._io.readU1();
      if (this.id019PtparisaContractIdTag == Id019PtparisaUnstakedFrozenStaker.Id019PtparisaContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id019PtparisaContractIdTag == Id019PtparisaUnstakedFrozenStaker.Id019PtparisaContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id019PtparisaContractId;
  })();

  var Single = Id019PtparisaUnstakedFrozenStaker.Single = (function() {
    function Single(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Single.prototype._read = function() {
      this.contract = new Id019PtparisaContractId(this._io, this, this._root);
      this.delegate = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Single;
  })();

  var PublicKeyHash = Id019PtparisaUnstakedFrozenStaker.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id019PtparisaUnstakedFrozenStaker.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaUnstakedFrozenStaker.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaUnstakedFrozenStaker.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaUnstakedFrozenStaker.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  /**
   * unstaked_frozen_staker: Abstract notion of staker used in operation receipts for unstaked frozen deposits, either a single staker or all the stakers delegating to some delegate.
   */

  return Id019PtparisaUnstakedFrozenStaker;
})();
return Id019PtparisaUnstakedFrozenStaker;
}));
