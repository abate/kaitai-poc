// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id007Psdelph1VoteBallot = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 007-PsDELPH1.vote.ballot
 */

var Id007Psdelph1VoteBallot = (function() {
  function Id007Psdelph1VoteBallot(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1VoteBallot.prototype._read = function() {
    this.id007Psdelph1VoteBallot = this._io.readS1();
  }

  return Id007Psdelph1VoteBallot;
})();
return Id007Psdelph1VoteBallot;
}));
