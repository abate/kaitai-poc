// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaSeed = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.seed
 */

var Id019PtparisaSeed = (function() {
  function Id019PtparisaSeed(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaSeed.prototype._read = function() {
    this.id019PtparisaSeed = this._io.readBytes(32);
  }

  return Id019PtparisaSeed;
})();
return Id019PtparisaSeed;
}));
