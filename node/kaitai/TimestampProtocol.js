// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.TimestampProtocol = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: timestamp.protocol
 * Description: A timestamp as seen by the protocol: second-level precision, epoch based.
 */

var TimestampProtocol = (function() {
  function TimestampProtocol(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  TimestampProtocol.prototype._read = function() {
    this.timestampProtocol = this._io.readS8be();
  }

  return TimestampProtocol;
})();
return TimestampProtocol;
}));
