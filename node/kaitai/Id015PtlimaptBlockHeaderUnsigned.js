// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id015PtlimaptBlockHeaderUnsigned = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 015-PtLimaPt.block_header.unsigned
 */

var Id015PtlimaptBlockHeaderUnsigned = (function() {
  Id015PtlimaptBlockHeaderUnsigned.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id015PtlimaptBlockHeaderUnsigned(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id015PtlimaptBlockHeaderUnsigned.prototype._read = function() {
    this.id015PtlimaptBlockHeaderUnsigned = new BlockHeaderShell(this._io, this, null);
    this.id015PtlimaptBlockHeaderAlphaUnsignedContents = new Id015PtlimaptBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id015PtlimaptBlockHeaderAlphaUnsignedContents = Id015PtlimaptBlockHeaderUnsigned.Id015PtlimaptBlockHeaderAlphaUnsignedContents = (function() {
    function Id015PtlimaptBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id015PtlimaptBlockHeaderUnsigned.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id015PtlimaptLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id015PtlimaptBlockHeaderAlphaUnsignedContents;
  })();

  var Id015PtlimaptLiquidityBakingToggleVote = Id015PtlimaptBlockHeaderUnsigned.Id015PtlimaptLiquidityBakingToggleVote = (function() {
    function Id015PtlimaptLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id015PtlimaptLiquidityBakingToggleVote.prototype._read = function() {
      this.id015PtlimaptLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id015PtlimaptLiquidityBakingToggleVote;
  })();

  return Id015PtlimaptBlockHeaderUnsigned;
})();
return Id015PtlimaptBlockHeaderUnsigned;
}));
