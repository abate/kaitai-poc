// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id016PtmumbaiTez = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 016-PtMumbai.tez
 */

var Id016PtmumbaiTez = (function() {
  function Id016PtmumbaiTez(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiTez.prototype._read = function() {
    this.id016PtmumbaiMutez = new Id016PtmumbaiMutez(this._io, this, this._root);
  }

  var Id016PtmumbaiMutez = Id016PtmumbaiTez.Id016PtmumbaiMutez = (function() {
    function Id016PtmumbaiMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id016PtmumbaiMutez.prototype._read = function() {
      this.id016PtmumbaiMutez = new N(this._io, this, this._root);
    }

    return Id016PtmumbaiMutez;
  })();

  var N = Id016PtmumbaiTez.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var NChunk = Id016PtmumbaiTez.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  return Id016PtmumbaiTez;
})();
return Id016PtmumbaiTez;
}));
