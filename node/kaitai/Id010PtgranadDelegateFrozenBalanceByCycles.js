// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id010PtgranadDelegateFrozenBalanceByCycles = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 010-PtGRANAD.delegate.frozen_balance_by_cycles
 */

var Id010PtgranadDelegateFrozenBalanceByCycles = (function() {
  function Id010PtgranadDelegateFrozenBalanceByCycles(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id010PtgranadDelegateFrozenBalanceByCycles.prototype._read = function() {
    this.lenId010PtgranadDelegateFrozenBalanceByCycles = this._io.readU4be();
    if (!(this.lenId010PtgranadDelegateFrozenBalanceByCycles <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId010PtgranadDelegateFrozenBalanceByCycles, this._io, "/seq/0");
    }
    this._raw_id010PtgranadDelegateFrozenBalanceByCycles = this._io.readBytes(this.lenId010PtgranadDelegateFrozenBalanceByCycles);
    var _io__raw_id010PtgranadDelegateFrozenBalanceByCycles = new KaitaiStream(this._raw_id010PtgranadDelegateFrozenBalanceByCycles);
    this.id010PtgranadDelegateFrozenBalanceByCycles = new Id010PtgranadDelegateFrozenBalanceByCycles(_io__raw_id010PtgranadDelegateFrozenBalanceByCycles, this, this._root);
  }

  var N = Id010PtgranadDelegateFrozenBalanceByCycles.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Id010PtgranadDelegateFrozenBalanceByCyclesEntries = Id010PtgranadDelegateFrozenBalanceByCycles.Id010PtgranadDelegateFrozenBalanceByCyclesEntries = (function() {
    function Id010PtgranadDelegateFrozenBalanceByCyclesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id010PtgranadDelegateFrozenBalanceByCyclesEntries.prototype._read = function() {
      this.cycle = this._io.readS4be();
      this.deposits = new Id010PtgranadMutez(this._io, this, this._root);
      this.fees = new Id010PtgranadMutez(this._io, this, this._root);
      this.rewards = new Id010PtgranadMutez(this._io, this, this._root);
    }

    return Id010PtgranadDelegateFrozenBalanceByCyclesEntries;
  })();

  var Id010PtgranadDelegateFrozenBalanceByCycles = Id010PtgranadDelegateFrozenBalanceByCycles.Id010PtgranadDelegateFrozenBalanceByCycles = (function() {
    function Id010PtgranadDelegateFrozenBalanceByCycles(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id010PtgranadDelegateFrozenBalanceByCycles.prototype._read = function() {
      this.id010PtgranadDelegateFrozenBalanceByCyclesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id010PtgranadDelegateFrozenBalanceByCyclesEntries.push(new Id010PtgranadDelegateFrozenBalanceByCyclesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id010PtgranadDelegateFrozenBalanceByCycles;
  })();

  var NChunk = Id010PtgranadDelegateFrozenBalanceByCycles.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Id010PtgranadMutez = Id010PtgranadDelegateFrozenBalanceByCycles.Id010PtgranadMutez = (function() {
    function Id010PtgranadMutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id010PtgranadMutez.prototype._read = function() {
      this.id010PtgranadMutez = new N(this._io, this, this._root);
    }

    return Id010PtgranadMutez;
  })();

  return Id010PtgranadDelegateFrozenBalanceByCycles;
})();
return Id010PtgranadDelegateFrozenBalanceByCycles;
}));
