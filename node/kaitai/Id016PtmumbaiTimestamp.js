// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './TimestampProtocol'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./TimestampProtocol'));
  } else {
    root.Id016PtmumbaiTimestamp = factory(root.KaitaiStream, root.TimestampProtocol);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, TimestampProtocol) {
/**
 * Encoding id: 016-PtMumbai.timestamp
 */

var Id016PtmumbaiTimestamp = (function() {
  function Id016PtmumbaiTimestamp(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiTimestamp.prototype._read = function() {
    this.id016PtmumbaiTimestamp = new TimestampProtocol(this._io, this, null);
  }

  return Id016PtmumbaiTimestamp;
})();
return Id016PtmumbaiTimestamp;
}));
