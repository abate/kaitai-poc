// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id008Ptedo2zkDelegateBalanceUpdates = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 008-PtEdo2Zk.delegate.balance_updates
 */

var Id008Ptedo2zkDelegateBalanceUpdates = (function() {
  Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceTag = Object.freeze({
    CONTRACT: 0,
    REWARDS: 1,
    FEES: 2,
    DEPOSITS: 3,

    0: "CONTRACT",
    1: "REWARDS",
    2: "FEES",
    3: "DEPOSITS",
  });

  Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id008Ptedo2zkDelegateBalanceUpdates(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkDelegateBalanceUpdates.prototype._read = function() {
    this.id008Ptedo2zkOperationMetadataAlphaBalanceUpdates = new Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates0(this._io, this, this._root);
  }

  var Originated = Id008Ptedo2zkDelegateBalanceUpdates.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var Id008Ptedo2zkOperationMetadataAlphaBalanceUpdatesEntries = Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceUpdatesEntries = (function() {
    function Id008Ptedo2zkOperationMetadataAlphaBalanceUpdatesEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkOperationMetadataAlphaBalanceUpdatesEntries.prototype._read = function() {
      this.id008Ptedo2zkOperationMetadataAlphaBalance = new Id008Ptedo2zkOperationMetadataAlphaBalance(this._io, this, this._root);
      this.id008Ptedo2zkOperationMetadataAlphaBalanceUpdate = new Id008Ptedo2zkOperationMetadataAlphaBalanceUpdate(this._io, this, this._root);
    }

    return Id008Ptedo2zkOperationMetadataAlphaBalanceUpdatesEntries;
  })();

  var Rewards = Id008Ptedo2zkDelegateBalanceUpdates.Rewards = (function() {
    function Rewards(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Rewards.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Rewards;
  })();

  var Deposits = Id008Ptedo2zkDelegateBalanceUpdates.Deposits = (function() {
    function Deposits(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Deposits.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Deposits;
  })();

  var Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates = Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates = (function() {
    function Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates.prototype._read = function() {
      this.id008Ptedo2zkOperationMetadataAlphaBalanceUpdatesEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id008Ptedo2zkOperationMetadataAlphaBalanceUpdatesEntries.push(new Id008Ptedo2zkOperationMetadataAlphaBalanceUpdatesEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates;
  })();

  var Id008Ptedo2zkContractId = Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkContractId = (function() {
    function Id008Ptedo2zkContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkContractId.prototype._read = function() {
      this.id008Ptedo2zkContractIdTag = this._io.readU1();
      if (this.id008Ptedo2zkContractIdTag == Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id008Ptedo2zkContractIdTag == Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id008Ptedo2zkContractId;
  })();

  var Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates0 = Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates0 = (function() {
    function Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates0.prototype._read = function() {
      this.lenId008Ptedo2zkOperationMetadataAlphaBalanceUpdates = this._io.readU4be();
      if (!(this.lenId008Ptedo2zkOperationMetadataAlphaBalanceUpdates <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId008Ptedo2zkOperationMetadataAlphaBalanceUpdates, this._io, "/types/id_008__ptedo2zk__operation_metadata__alpha__balance_updates_0/seq/0");
      }
      this._raw_id008Ptedo2zkOperationMetadataAlphaBalanceUpdates = this._io.readBytes(this.lenId008Ptedo2zkOperationMetadataAlphaBalanceUpdates);
      var _io__raw_id008Ptedo2zkOperationMetadataAlphaBalanceUpdates = new KaitaiStream(this._raw_id008Ptedo2zkOperationMetadataAlphaBalanceUpdates);
      this.id008Ptedo2zkOperationMetadataAlphaBalanceUpdates = new Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates(_io__raw_id008Ptedo2zkOperationMetadataAlphaBalanceUpdates, this, this._root);
    }

    return Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates0;
  })();

  var Fees = Id008Ptedo2zkDelegateBalanceUpdates.Fees = (function() {
    function Fees(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Fees.prototype._read = function() {
      this.delegate = new PublicKeyHash(this._io, this, this._root);
      this.cycle = this._io.readS4be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Fees;
  })();

  var Id008Ptedo2zkOperationMetadataAlphaBalance = Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalance = (function() {
    function Id008Ptedo2zkOperationMetadataAlphaBalance(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkOperationMetadataAlphaBalance.prototype._read = function() {
      this.id008Ptedo2zkOperationMetadataAlphaBalanceTag = this._io.readU1();
      if (this.id008Ptedo2zkOperationMetadataAlphaBalanceTag == Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceTag.CONTRACT) {
        this.contract = new Id008Ptedo2zkContractId(this._io, this, this._root);
      }
      if (this.id008Ptedo2zkOperationMetadataAlphaBalanceTag == Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceTag.REWARDS) {
        this.rewards = new Rewards(this._io, this, this._root);
      }
      if (this.id008Ptedo2zkOperationMetadataAlphaBalanceTag == Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceTag.FEES) {
        this.fees = new Fees(this._io, this, this._root);
      }
      if (this.id008Ptedo2zkOperationMetadataAlphaBalanceTag == Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceTag.DEPOSITS) {
        this.deposits = new Deposits(this._io, this, this._root);
      }
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Id008Ptedo2zkOperationMetadataAlphaBalance;
  })();

  var Id008Ptedo2zkOperationMetadataAlphaBalanceUpdate = Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceUpdate = (function() {
    function Id008Ptedo2zkOperationMetadataAlphaBalanceUpdate(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkOperationMetadataAlphaBalanceUpdate.prototype._read = function() {
      this.change = this._io.readS8be();
    }

    return Id008Ptedo2zkOperationMetadataAlphaBalanceUpdate;
  })();

  var PublicKeyHash = Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id008Ptedo2zkDelegateBalanceUpdates;
})();
return Id008Ptedo2zkDelegateBalanceUpdates;
}));
