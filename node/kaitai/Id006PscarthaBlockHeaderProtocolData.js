// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id006PscarthaBlockHeaderProtocolData = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 006-PsCARTHA.block_header.protocol_data
 */

var Id006PscarthaBlockHeaderProtocolData = (function() {
  Id006PscarthaBlockHeaderProtocolData.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id006PscarthaBlockHeaderProtocolData(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id006PscarthaBlockHeaderProtocolData.prototype._read = function() {
    this.id006PscarthaBlockHeaderAlphaSignedContents = new Id006PscarthaBlockHeaderAlphaSignedContents(this._io, this, this._root);
  }

  var Id006PscarthaBlockHeaderAlphaSignedContents = Id006PscarthaBlockHeaderProtocolData.Id006PscarthaBlockHeaderAlphaSignedContents = (function() {
    function Id006PscarthaBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id006PscarthaBlockHeaderAlphaUnsignedContents = new Id006PscarthaBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id006PscarthaBlockHeaderAlphaSignedContents;
  })();

  var Id006PscarthaBlockHeaderAlphaUnsignedContents = Id006PscarthaBlockHeaderProtocolData.Id006PscarthaBlockHeaderAlphaUnsignedContents = (function() {
    function Id006PscarthaBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id006PscarthaBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id006PscarthaBlockHeaderProtocolData.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
    }

    return Id006PscarthaBlockHeaderAlphaUnsignedContents;
  })();

  return Id006PscarthaBlockHeaderProtocolData;
})();
return Id006PscarthaBlockHeaderProtocolData;
}));
