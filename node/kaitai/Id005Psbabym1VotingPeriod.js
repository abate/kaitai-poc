// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id005Psbabym1VotingPeriod = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 005-PsBabyM1.voting_period
 */

var Id005Psbabym1VotingPeriod = (function() {
  function Id005Psbabym1VotingPeriod(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1VotingPeriod.prototype._read = function() {
    this.id005Psbabym1VotingPeriod = this._io.readS4be();
  }

  return Id005Psbabym1VotingPeriod;
})();
return Id005Psbabym1VotingPeriod;
}));
