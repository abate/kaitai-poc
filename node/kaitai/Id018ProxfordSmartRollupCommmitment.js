// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id018ProxfordSmartRollupCommmitment = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 018-Proxford.smart_rollup.commmitment
 */

var Id018ProxfordSmartRollupCommmitment = (function() {
  function Id018ProxfordSmartRollupCommmitment(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id018ProxfordSmartRollupCommmitment.prototype._read = function() {
    this.compressedState = this._io.readBytes(32);
    this.inboxLevel = this._io.readS4be();
    this.predecessor = this._io.readBytes(32);
    this.numberOfTicks = this._io.readS8be();
  }

  return Id018ProxfordSmartRollupCommmitment;
})();
return Id018ProxfordSmartRollupCommmitment;
}));
