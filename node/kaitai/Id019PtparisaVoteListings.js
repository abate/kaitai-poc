// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id019PtparisaVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 019-PtParisA.vote.listings
 */

var Id019PtparisaVoteListings = (function() {
  Id019PtparisaVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function Id019PtparisaVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id019PtparisaVoteListings.prototype._read = function() {
    this.lenId019PtparisaVoteListings = this._io.readU4be();
    if (!(this.lenId019PtparisaVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId019PtparisaVoteListings, this._io, "/seq/0");
    }
    this._raw_id019PtparisaVoteListings = this._io.readBytes(this.lenId019PtparisaVoteListings);
    var _io__raw_id019PtparisaVoteListings = new KaitaiStream(this._raw_id019PtparisaVoteListings);
    this.id019PtparisaVoteListings = new Id019PtparisaVoteListings(_io__raw_id019PtparisaVoteListings, this, this._root);
  }

  var Id019PtparisaVoteListings = Id019PtparisaVoteListings.Id019PtparisaVoteListings = (function() {
    function Id019PtparisaVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaVoteListings.prototype._read = function() {
      this.id019PtparisaVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id019PtparisaVoteListingsEntries.push(new Id019PtparisaVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id019PtparisaVoteListings;
  })();

  var Id019PtparisaVoteListingsEntries = Id019PtparisaVoteListings.Id019PtparisaVoteListingsEntries = (function() {
    function Id019PtparisaVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id019PtparisaVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.votingPower = this._io.readS8be();
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Id019PtparisaVoteListingsEntries;
  })();

  var PublicKeyHash = Id019PtparisaVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id019PtparisaVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id019PtparisaVoteListings.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id019PtparisaVoteListings;
})();
return Id019PtparisaVoteListings;
}));
