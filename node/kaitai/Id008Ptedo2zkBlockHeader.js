// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id008Ptedo2zkBlockHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 008-PtEdo2Zk.block_header
 */

var Id008Ptedo2zkBlockHeader = (function() {
  Id008Ptedo2zkBlockHeader.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id008Ptedo2zkBlockHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkBlockHeader.prototype._read = function() {
    this.id008Ptedo2zkBlockHeaderAlphaFullHeader = new Id008Ptedo2zkBlockHeaderAlphaFullHeader(this._io, this, this._root);
  }

  var Id008Ptedo2zkBlockHeaderAlphaFullHeader = Id008Ptedo2zkBlockHeader.Id008Ptedo2zkBlockHeaderAlphaFullHeader = (function() {
    function Id008Ptedo2zkBlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkBlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id008Ptedo2zkBlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id008Ptedo2zkBlockHeaderAlphaSignedContents = new Id008Ptedo2zkBlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id008Ptedo2zkBlockHeaderAlphaFullHeader;
  })();

  var Id008Ptedo2zkBlockHeaderAlphaSignedContents = Id008Ptedo2zkBlockHeader.Id008Ptedo2zkBlockHeaderAlphaSignedContents = (function() {
    function Id008Ptedo2zkBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id008Ptedo2zkBlockHeaderAlphaUnsignedContents = new Id008Ptedo2zkBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id008Ptedo2zkBlockHeaderAlphaSignedContents;
  })();

  var Id008Ptedo2zkBlockHeaderAlphaUnsignedContents = Id008Ptedo2zkBlockHeader.Id008Ptedo2zkBlockHeaderAlphaUnsignedContents = (function() {
    function Id008Ptedo2zkBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id008Ptedo2zkBlockHeader.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
    }

    return Id008Ptedo2zkBlockHeaderAlphaUnsignedContents;
  })();

  return Id008Ptedo2zkBlockHeader;
})();
return Id008Ptedo2zkBlockHeader;
}));
