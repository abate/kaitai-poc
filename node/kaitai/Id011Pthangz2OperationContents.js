// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell', './OperationShellHeader'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'), require('./OperationShellHeader'));
  } else {
    root.Id011Pthangz2OperationContents = factory(root.KaitaiStream, root.BlockHeaderShell, root.OperationShellHeader);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell, OperationShellHeader) {
/**
 * Encoding id: 011-PtHangz2.operation.contents
 */

var Id011Pthangz2OperationContents = (function() {
  Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag = Object.freeze({
    ENDORSEMENT: 0,
    SEED_NONCE_REVELATION: 1,
    DOUBLE_ENDORSEMENT_EVIDENCE: 2,
    DOUBLE_BAKING_EVIDENCE: 3,
    ACTIVATE_ACCOUNT: 4,
    PROPOSALS: 5,
    BALLOT: 6,
    ENDORSEMENT_WITH_SLOT: 10,
    FAILING_NOOP: 17,
    REVEAL: 107,
    TRANSACTION: 108,
    ORIGINATION: 109,
    DELEGATION: 110,
    REGISTER_GLOBAL_CONSTANT: 111,

    0: "ENDORSEMENT",
    1: "SEED_NONCE_REVELATION",
    2: "DOUBLE_ENDORSEMENT_EVIDENCE",
    3: "DOUBLE_BAKING_EVIDENCE",
    4: "ACTIVATE_ACCOUNT",
    5: "PROPOSALS",
    6: "BALLOT",
    10: "ENDORSEMENT_WITH_SLOT",
    17: "FAILING_NOOP",
    107: "REVEAL",
    108: "TRANSACTION",
    109: "ORIGINATION",
    110: "DELEGATION",
    111: "REGISTER_GLOBAL_CONSTANT",
  });

  Id011Pthangz2OperationContents.PublicKeyTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id011Pthangz2OperationContents.Id011Pthangz2InlinedEndorsementContentsTag = Object.freeze({
    ENDORSEMENT: 0,

    0: "ENDORSEMENT",
  });

  Id011Pthangz2OperationContents.Id011Pthangz2ContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  Id011Pthangz2OperationContents.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  Id011Pthangz2OperationContents.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  Id011Pthangz2OperationContents.Id011Pthangz2EntrypointTag = Object.freeze({
    DEFAULT: 0,
    ROOT: 1,
    DO: 2,
    SET_DELEGATE: 3,
    REMOVE_DELEGATE: 4,
    NAMED: 255,

    0: "DEFAULT",
    1: "ROOT",
    2: "DO",
    3: "SET_DELEGATE",
    4: "REMOVE_DELEGATE",
    255: "NAMED",
  });

  function Id011Pthangz2OperationContents(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id011Pthangz2OperationContents.prototype._read = function() {
    this.id011Pthangz2OperationAlphaContents = new Id011Pthangz2OperationAlphaContents(this._io, this, this._root);
  }

  var Id011Pthangz2OperationAlphaContents = Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContents = (function() {
    function Id011Pthangz2OperationAlphaContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2OperationAlphaContents.prototype._read = function() {
      this.id011Pthangz2OperationAlphaContentsTag = this._io.readU1();
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.ENDORSEMENT) {
        this.endorsement = this._io.readS4be();
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.SEED_NONCE_REVELATION) {
        this.seedNonceRevelation = new SeedNonceRevelation(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.ENDORSEMENT_WITH_SLOT) {
        this.endorsementWithSlot = new EndorsementWithSlot(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.DOUBLE_ENDORSEMENT_EVIDENCE) {
        this.doubleEndorsementEvidence = new DoubleEndorsementEvidence(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.DOUBLE_BAKING_EVIDENCE) {
        this.doubleBakingEvidence = new DoubleBakingEvidence(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.ACTIVATE_ACCOUNT) {
        this.activateAccount = new ActivateAccount(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.PROPOSALS) {
        this.proposals = new Proposals1(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.BALLOT) {
        this.ballot = new Ballot(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.REVEAL) {
        this.reveal = new Reveal(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.TRANSACTION) {
        this.transaction = new Transaction(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.ORIGINATION) {
        this.origination = new Origination(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.DELEGATION) {
        this.delegation = new Delegation(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.FAILING_NOOP) {
        this.failingNoop = new BytesDynUint30(this._io, this, this._root);
      }
      if (this.id011Pthangz2OperationAlphaContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.REGISTER_GLOBAL_CONSTANT) {
        this.registerGlobalConstant = new RegisterGlobalConstant(this._io, this, this._root);
      }
    }

    return Id011Pthangz2OperationAlphaContents;
  })();

  var Op20 = Id011Pthangz2OperationContents.Op20 = (function() {
    function Op20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op20.prototype._read = function() {
      this.lenOp2 = this._io.readU4be();
      if (!(this.lenOp2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp2, this._io, "/types/op2_0/seq/0");
      }
      this._raw_op2 = this._io.readBytes(this.lenOp2);
      var _io__raw_op2 = new KaitaiStream(this._raw_op2);
      this.op2 = new Op2(_io__raw_op2, this, this._root);
    }

    return Op20;
  })();

  var ActivateAccount = Id011Pthangz2OperationContents.ActivateAccount = (function() {
    function ActivateAccount(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ActivateAccount.prototype._read = function() {
      this.pkh = this._io.readBytes(20);
      this.secret = this._io.readBytes(20);
    }

    return ActivateAccount;
  })();

  var DoubleEndorsementEvidence = Id011Pthangz2OperationContents.DoubleEndorsementEvidence = (function() {
    function DoubleEndorsementEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleEndorsementEvidence.prototype._read = function() {
      this.op1 = new Op10(this._io, this, this._root);
      this.op2 = new Op20(this._io, this, this._root);
      this.slot = this._io.readU2be();
    }

    return DoubleEndorsementEvidence;
  })();

  var Originated = Id011Pthangz2OperationContents.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var N = Id011Pthangz2OperationContents.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var Endorsement = Id011Pthangz2OperationContents.Endorsement = (function() {
    function Endorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Endorsement.prototype._read = function() {
      this.id011Pthangz2InlinedEndorsement = new Id011Pthangz2InlinedEndorsement(this._io, this, this._root);
    }

    return Endorsement;
  })();

  var Proposals0 = Id011Pthangz2OperationContents.Proposals0 = (function() {
    function Proposals0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals0.prototype._read = function() {
      this.lenProposals = this._io.readU4be();
      if (!(this.lenProposals <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenProposals, this._io, "/types/proposals_0/seq/0");
      }
      this._raw_proposals = this._io.readBytes(this.lenProposals);
      var _io__raw_proposals = new KaitaiStream(this._raw_proposals);
      this.proposals = new Proposals(_io__raw_proposals, this, this._root);
    }

    return Proposals0;
  })();

  var Id011Pthangz2InlinedEndorsementContents = Id011Pthangz2OperationContents.Id011Pthangz2InlinedEndorsementContents = (function() {
    function Id011Pthangz2InlinedEndorsementContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2InlinedEndorsementContents.prototype._read = function() {
      this.id011Pthangz2InlinedEndorsementContentsTag = this._io.readU1();
      if (this.id011Pthangz2InlinedEndorsementContentsTag == Id011Pthangz2OperationContents.Id011Pthangz2InlinedEndorsementContentsTag.ENDORSEMENT) {
        this.endorsement = this._io.readS4be();
      }
    }

    return Id011Pthangz2InlinedEndorsementContents;
  })();

  var Id011Pthangz2ContractId = Id011Pthangz2OperationContents.Id011Pthangz2ContractId = (function() {
    function Id011Pthangz2ContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2ContractId.prototype._read = function() {
      this.id011Pthangz2ContractIdTag = this._io.readU1();
      if (this.id011Pthangz2ContractIdTag == Id011Pthangz2OperationContents.Id011Pthangz2ContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.id011Pthangz2ContractIdTag == Id011Pthangz2OperationContents.Id011Pthangz2ContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id011Pthangz2ContractId;
  })();

  var Reveal = Id011Pthangz2OperationContents.Reveal = (function() {
    function Reveal(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Reveal.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id011Pthangz2Mutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.publicKey = new PublicKey(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key
     */

    return Reveal;
  })();

  var Endorsement0 = Id011Pthangz2OperationContents.Endorsement0 = (function() {
    function Endorsement0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Endorsement0.prototype._read = function() {
      this.lenEndorsement = this._io.readU4be();
      if (!(this.lenEndorsement <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenEndorsement, this._io, "/types/endorsement_0/seq/0");
      }
      this._raw_endorsement = this._io.readBytes(this.lenEndorsement);
      var _io__raw_endorsement = new KaitaiStream(this._raw_endorsement);
      this.endorsement = new Endorsement(_io__raw_endorsement, this, this._root);
    }

    return Endorsement0;
  })();

  var Id011Pthangz2BlockHeaderAlphaSignedContents = Id011Pthangz2OperationContents.Id011Pthangz2BlockHeaderAlphaSignedContents = (function() {
    function Id011Pthangz2BlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2BlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id011Pthangz2BlockHeaderAlphaUnsignedContents = new Id011Pthangz2BlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id011Pthangz2BlockHeaderAlphaSignedContents;
  })();

  var Id011Pthangz2Mutez = Id011Pthangz2OperationContents.Id011Pthangz2Mutez = (function() {
    function Id011Pthangz2Mutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2Mutez.prototype._read = function() {
      this.id011Pthangz2Mutez = new N(this._io, this, this._root);
    }

    return Id011Pthangz2Mutez;
  })();

  var SeedNonceRevelation = Id011Pthangz2OperationContents.SeedNonceRevelation = (function() {
    function SeedNonceRevelation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    SeedNonceRevelation.prototype._read = function() {
      this.level = this._io.readS4be();
      this.nonce = this._io.readBytes(32);
    }

    return SeedNonceRevelation;
  })();

  var Id011Pthangz2Entrypoint = Id011Pthangz2OperationContents.Id011Pthangz2Entrypoint = (function() {
    function Id011Pthangz2Entrypoint(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2Entrypoint.prototype._read = function() {
      this.id011Pthangz2EntrypointTag = this._io.readU1();
      if (this.id011Pthangz2EntrypointTag == Id011Pthangz2OperationContents.Id011Pthangz2EntrypointTag.NAMED) {
        this.named = new Named0(this._io, this, this._root);
      }
    }

    return Id011Pthangz2Entrypoint;
  })();

  var DoubleBakingEvidence = Id011Pthangz2OperationContents.DoubleBakingEvidence = (function() {
    function DoubleBakingEvidence(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    DoubleBakingEvidence.prototype._read = function() {
      this.bh1 = new Bh10(this._io, this, this._root);
      this.bh2 = new Bh20(this._io, this, this._root);
    }

    return DoubleBakingEvidence;
  })();

  var PublicKey = Id011Pthangz2OperationContents.PublicKey = (function() {
    function PublicKey(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKey.prototype._read = function() {
      this.publicKeyTag = this._io.readU1();
      if (this.publicKeyTag == Id011Pthangz2OperationContents.PublicKeyTag.ED25519) {
        this.ed25519 = this._io.readBytes(32);
      }
      if (this.publicKeyTag == Id011Pthangz2OperationContents.PublicKeyTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(33);
      }
      if (this.publicKeyTag == Id011Pthangz2OperationContents.PublicKeyTag.P256) {
        this.p256 = this._io.readBytes(33);
      }
    }

    return PublicKey;
  })();

  var Bh2 = Id011Pthangz2OperationContents.Bh2 = (function() {
    function Bh2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh2.prototype._read = function() {
      this.id011Pthangz2BlockHeaderAlphaFullHeader = new Id011Pthangz2BlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh2;
  })();

  var Named = Id011Pthangz2OperationContents.Named = (function() {
    function Named(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named.prototype._read = function() {
      this.named = this._io.readBytesFull();
    }

    return Named;
  })();

  var Bh20 = Id011Pthangz2OperationContents.Bh20 = (function() {
    function Bh20(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh20.prototype._read = function() {
      this.lenBh2 = this._io.readU4be();
      if (!(this.lenBh2 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh2, this._io, "/types/bh2_0/seq/0");
      }
      this._raw_bh2 = this._io.readBytes(this.lenBh2);
      var _io__raw_bh2 = new KaitaiStream(this._raw_bh2);
      this.bh2 = new Bh2(_io__raw_bh2, this, this._root);
    }

    return Bh20;
  })();

  var Delegation = Id011Pthangz2OperationContents.Delegation = (function() {
    function Delegation(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Delegation.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id011Pthangz2Mutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id011Pthangz2OperationContents.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Delegation;
  })();

  var RegisterGlobalConstant = Id011Pthangz2OperationContents.RegisterGlobalConstant = (function() {
    function RegisterGlobalConstant(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    RegisterGlobalConstant.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id011Pthangz2Mutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return RegisterGlobalConstant;
  })();

  var Bh10 = Id011Pthangz2OperationContents.Bh10 = (function() {
    function Bh10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh10.prototype._read = function() {
      this.lenBh1 = this._io.readU4be();
      if (!(this.lenBh1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBh1, this._io, "/types/bh1_0/seq/0");
      }
      this._raw_bh1 = this._io.readBytes(this.lenBh1);
      var _io__raw_bh1 = new KaitaiStream(this._raw_bh1);
      this.bh1 = new Bh1(_io__raw_bh1, this, this._root);
    }

    return Bh10;
  })();

  var BytesDynUint30 = Id011Pthangz2OperationContents.BytesDynUint30 = (function() {
    function BytesDynUint30(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    BytesDynUint30.prototype._read = function() {
      this.lenBytesDynUint30 = this._io.readU4be();
      if (!(this.lenBytesDynUint30 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenBytesDynUint30, this._io, "/types/bytes_dyn_uint30/seq/0");
      }
      this.bytesDynUint30 = this._io.readBytes(this.lenBytesDynUint30);
    }

    return BytesDynUint30;
  })();

  var Bh1 = Id011Pthangz2OperationContents.Bh1 = (function() {
    function Bh1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Bh1.prototype._read = function() {
      this.id011Pthangz2BlockHeaderAlphaFullHeader = new Id011Pthangz2BlockHeaderAlphaFullHeader(this._io, this, this._root);
    }

    return Bh1;
  })();

  var Op10 = Id011Pthangz2OperationContents.Op10 = (function() {
    function Op10(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op10.prototype._read = function() {
      this.lenOp1 = this._io.readU4be();
      if (!(this.lenOp1 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenOp1, this._io, "/types/op1_0/seq/0");
      }
      this._raw_op1 = this._io.readBytes(this.lenOp1);
      var _io__raw_op1 = new KaitaiStream(this._raw_op1);
      this.op1 = new Op1(_io__raw_op1, this, this._root);
    }

    return Op10;
  })();

  var EndorsementWithSlot = Id011Pthangz2OperationContents.EndorsementWithSlot = (function() {
    function EndorsementWithSlot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    EndorsementWithSlot.prototype._read = function() {
      this.endorsement = new Endorsement0(this._io, this, this._root);
      this.slot = this._io.readU2be();
    }

    return EndorsementWithSlot;
  })();

  var Id011Pthangz2ScriptedContracts = Id011Pthangz2OperationContents.Id011Pthangz2ScriptedContracts = (function() {
    function Id011Pthangz2ScriptedContracts(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2ScriptedContracts.prototype._read = function() {
      this.code = new BytesDynUint30(this._io, this, this._root);
      this.storage = new BytesDynUint30(this._io, this, this._root);
    }

    return Id011Pthangz2ScriptedContracts;
  })();

  var Id011Pthangz2BlockHeaderAlphaFullHeader = Id011Pthangz2OperationContents.Id011Pthangz2BlockHeaderAlphaFullHeader = (function() {
    function Id011Pthangz2BlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2BlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id011Pthangz2BlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id011Pthangz2BlockHeaderAlphaSignedContents = new Id011Pthangz2BlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id011Pthangz2BlockHeaderAlphaFullHeader;
  })();

  var Origination = Id011Pthangz2OperationContents.Origination = (function() {
    function Origination(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Origination.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id011Pthangz2Mutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.balance = new Id011Pthangz2Mutez(this._io, this, this._root);
      this.delegateTag = this._io.readU1();
      if (this.delegateTag == Id011Pthangz2OperationContents.Bool.TRUE) {
        this.delegate = new PublicKeyHash(this._io, this, this._root);
      }
      this.script = new Id011Pthangz2ScriptedContracts(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Origination;
  })();

  var Op2 = Id011Pthangz2OperationContents.Op2 = (function() {
    function Op2(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op2.prototype._read = function() {
      this.id011Pthangz2InlinedEndorsement = new Id011Pthangz2InlinedEndorsement(this._io, this, this._root);
    }

    return Op2;
  })();

  var NChunk = Id011Pthangz2OperationContents.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Named0 = Id011Pthangz2OperationContents.Named0 = (function() {
    function Named0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Named0.prototype._read = function() {
      this.lenNamed = this._io.readU1();
      if (!(this.lenNamed <= 31)) {
        throw new KaitaiStream.ValidationGreaterThanError(31, this.lenNamed, this._io, "/types/named_0/seq/0");
      }
      this._raw_named = this._io.readBytes(this.lenNamed);
      var _io__raw_named = new KaitaiStream(this._raw_named);
      this.named = new Named(_io__raw_named, this, this._root);
    }

    return Named0;
  })();

  var Transaction = Id011Pthangz2OperationContents.Transaction = (function() {
    function Transaction(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Transaction.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.fee = new Id011Pthangz2Mutez(this._io, this, this._root);
      this.counter = new N(this._io, this, this._root);
      this.gasLimit = new N(this._io, this, this._root);
      this.storageLimit = new N(this._io, this, this._root);
      this.amount = new Id011Pthangz2Mutez(this._io, this, this._root);
      this.destination = new Id011Pthangz2ContractId(this._io, this, this._root);
      this.parametersTag = this._io.readU1();
      if (this.parametersTag == Id011Pthangz2OperationContents.Bool.TRUE) {
        this.parameters = new Parameters(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    return Transaction;
  })();

  var Id011Pthangz2InlinedEndorsement = Id011Pthangz2OperationContents.Id011Pthangz2InlinedEndorsement = (function() {
    function Id011Pthangz2InlinedEndorsement(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2InlinedEndorsement.prototype._read = function() {
      this.id011Pthangz2InlinedEndorsement = new OperationShellHeader(this._io, this, null);
      this.operations = new Id011Pthangz2InlinedEndorsementContents(this._io, this, this._root);
      this.signatureTag = this._io.readU1();
      if (this.signatureTag == Id011Pthangz2OperationContents.Bool.TRUE) {
        this.signature = this._io.readBytes(64);
      }
    }

    return Id011Pthangz2InlinedEndorsement;
  })();

  var Parameters = Id011Pthangz2OperationContents.Parameters = (function() {
    function Parameters(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Parameters.prototype._read = function() {
      this.entrypoint = new Id011Pthangz2Entrypoint(this._io, this, this._root);
      this.value = new BytesDynUint30(this._io, this, this._root);
    }

    /**
     * entrypoint: Named entrypoint to a Michelson smart contract
     */

    return Parameters;
  })();

  var Id011Pthangz2BlockHeaderAlphaUnsignedContents = Id011Pthangz2OperationContents.Id011Pthangz2BlockHeaderAlphaUnsignedContents = (function() {
    function Id011Pthangz2BlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id011Pthangz2BlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id011Pthangz2OperationContents.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingEscapeVote = this._io.readU1();
    }

    return Id011Pthangz2BlockHeaderAlphaUnsignedContents;
  })();

  var Proposals1 = Id011Pthangz2OperationContents.Proposals1 = (function() {
    function Proposals1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals1.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposals = new Proposals0(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Proposals1;
  })();

  var Ballot = Id011Pthangz2OperationContents.Ballot = (function() {
    function Ballot(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Ballot.prototype._read = function() {
      this.source = new PublicKeyHash(this._io, this, this._root);
      this.period = this._io.readS4be();
      this.proposal = this._io.readBytes(32);
      this.ballot = this._io.readS1();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Ballot;
  })();

  var PublicKeyHash = Id011Pthangz2OperationContents.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id011Pthangz2OperationContents.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id011Pthangz2OperationContents.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id011Pthangz2OperationContents.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  var Proposals = Id011Pthangz2OperationContents.Proposals = (function() {
    function Proposals(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Proposals.prototype._read = function() {
      this.proposalsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.proposalsEntries.push(new ProposalsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Proposals;
  })();

  var ProposalsEntries = Id011Pthangz2OperationContents.ProposalsEntries = (function() {
    function ProposalsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    ProposalsEntries.prototype._read = function() {
      this.protocolHash = this._io.readBytes(32);
    }

    return ProposalsEntries;
  })();

  var Op1 = Id011Pthangz2OperationContents.Op1 = (function() {
    function Op1(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Op1.prototype._read = function() {
      this.id011Pthangz2InlinedEndorsement = new Id011Pthangz2InlinedEndorsement(this._io, this, this._root);
    }

    return Op1;
  })();

  return Id011Pthangz2OperationContents;
})();
return Id011Pthangz2OperationContents;
}));
