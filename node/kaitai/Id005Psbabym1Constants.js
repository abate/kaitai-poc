// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id005Psbabym1Constants = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 005-PsBabyM1.constants
 */

var Id005Psbabym1Constants = (function() {
  function Id005Psbabym1Constants(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id005Psbabym1Constants.prototype._read = function() {
    this.proofOfWorkNonceSize = this._io.readU1();
    this.nonceLength = this._io.readU1();
    this.maxRevelationsPerBlock = this._io.readU1();
    this.maxOperationDataLength = new Int31(this._io, this, this._root);
    this.maxProposalsPerDelegate = this._io.readU1();
    this.preservedCycles = this._io.readU1();
    this.blocksPerCycle = this._io.readS4be();
    this.blocksPerCommitment = this._io.readS4be();
    this.blocksPerRollSnapshot = this._io.readS4be();
    this.blocksPerVotingPeriod = this._io.readS4be();
    this.timeBetweenBlocks = new TimeBetweenBlocks0(this._io, this, this._root);
    this.endorsersPerBlock = this._io.readU2be();
    this.hardGasLimitPerOperation = new Z(this._io, this, this._root);
    this.hardGasLimitPerBlock = new Z(this._io, this, this._root);
    this.proofOfWorkThreshold = this._io.readS8be();
    this.tokensPerRoll = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.michelsonMaximumTypeSize = this._io.readU2be();
    this.seedNonceRevelationTip = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.originationSize = new Int31(this._io, this, this._root);
    this.blockSecurityDeposit = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.endorsementSecurityDeposit = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.blockReward = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.endorsementReward = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.costPerByte = new Id005Psbabym1Mutez(this._io, this, this._root);
    this.hardStorageLimitPerOperation = new Z(this._io, this, this._root);
    this.testChainDuration = this._io.readS8be();
    this.quorumMin = this._io.readS4be();
    this.quorumMax = this._io.readS4be();
    this.minProposalQuorum = this._io.readS4be();
    this.initialEndorsers = this._io.readU2be();
    this.delayPerMissingEndorsement = this._io.readS8be();
  }

  var TimeBetweenBlocksEntries = Id005Psbabym1Constants.TimeBetweenBlocksEntries = (function() {
    function TimeBetweenBlocksEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TimeBetweenBlocksEntries.prototype._read = function() {
      this.timeBetweenBlocksElt = this._io.readS8be();
    }

    return TimeBetweenBlocksEntries;
  })();

  var N = Id005Psbabym1Constants.N = (function() {
    function N(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    N.prototype._read = function() {
      this.n = [];
      var i = 0;
      do {
        var _ = new NChunk(this._io, this, this._root);
        this.n.push(_);
        i++;
      } while (!(!(_.hasMore)));
    }

    return N;
  })();

  var TimeBetweenBlocks = Id005Psbabym1Constants.TimeBetweenBlocks = (function() {
    function TimeBetweenBlocks(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TimeBetweenBlocks.prototype._read = function() {
      this.timeBetweenBlocksEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.timeBetweenBlocksEntries.push(new TimeBetweenBlocksEntries(this._io, this, this._root));
        i++;
      }
    }

    return TimeBetweenBlocks;
  })();

  var Id005Psbabym1Mutez = Id005Psbabym1Constants.Id005Psbabym1Mutez = (function() {
    function Id005Psbabym1Mutez(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id005Psbabym1Mutez.prototype._read = function() {
      this.id005Psbabym1Mutez = new N(this._io, this, this._root);
    }

    return Id005Psbabym1Mutez;
  })();

  var TimeBetweenBlocks0 = Id005Psbabym1Constants.TimeBetweenBlocks0 = (function() {
    function TimeBetweenBlocks0(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    TimeBetweenBlocks0.prototype._read = function() {
      this.lenTimeBetweenBlocks = this._io.readU4be();
      if (!(this.lenTimeBetweenBlocks <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenTimeBetweenBlocks, this._io, "/types/time_between_blocks_0/seq/0");
      }
      this._raw_timeBetweenBlocks = this._io.readBytes(this.lenTimeBetweenBlocks);
      var _io__raw_timeBetweenBlocks = new KaitaiStream(this._raw_timeBetweenBlocks);
      this.timeBetweenBlocks = new TimeBetweenBlocks(_io__raw_timeBetweenBlocks, this, this._root);
    }

    return TimeBetweenBlocks0;
  })();

  var Int31 = Id005Psbabym1Constants.Int31 = (function() {
    function Int31(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Int31.prototype._read = function() {
      this.int31 = this._io.readS4be();
      if (!(this.int31 >= -1073741824)) {
        throw new KaitaiStream.ValidationLessThanError(-1073741824, this.int31, this._io, "/types/int31/seq/0");
      }
      if (!(this.int31 <= 1073741823)) {
        throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.int31, this._io, "/types/int31/seq/0");
      }
    }

    return Int31;
  })();

  var NChunk = Id005Psbabym1Constants.NChunk = (function() {
    function NChunk(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    NChunk.prototype._read = function() {
      this.hasMore = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(7);
    }

    return NChunk;
  })();

  var Z = Id005Psbabym1Constants.Z = (function() {
    function Z(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Z.prototype._read = function() {
      this.hasTail = this._io.readBitsIntBe(1) != 0;
      this.sign = this._io.readBitsIntBe(1) != 0;
      this.payload = this._io.readBitsIntBe(6);
      this._io.alignToByte();
      if (this.hasTail) {
        this.tail = [];
        var i = 0;
        do {
          var _ = new NChunk(this._io, this, this._root);
          this.tail.push(_);
          i++;
        } while (!(!(_.hasMore)));
      }
    }

    return Z;
  })();

  return Id005Psbabym1Constants;
})();
return Id005Psbabym1Constants;
}));
