// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './BlockHeaderShell'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./BlockHeaderShell'));
  } else {
    root.Id014PtkathmaBlockHeader = factory(root.KaitaiStream, root.BlockHeaderShell);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, BlockHeaderShell) {
/**
 * Encoding id: 014-PtKathma.block_header
 */

var Id014PtkathmaBlockHeader = (function() {
  Id014PtkathmaBlockHeader.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id014PtkathmaBlockHeader(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id014PtkathmaBlockHeader.prototype._read = function() {
    this.id014PtkathmaBlockHeaderAlphaFullHeader = new Id014PtkathmaBlockHeaderAlphaFullHeader(this._io, this, this._root);
  }

  var Id014PtkathmaBlockHeaderAlphaFullHeader = Id014PtkathmaBlockHeader.Id014PtkathmaBlockHeaderAlphaFullHeader = (function() {
    function Id014PtkathmaBlockHeaderAlphaFullHeader(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaBlockHeaderAlphaFullHeader.prototype._read = function() {
      this.id014PtkathmaBlockHeaderAlphaFullHeader = new BlockHeaderShell(this._io, this, null);
      this.id014PtkathmaBlockHeaderAlphaSignedContents = new Id014PtkathmaBlockHeaderAlphaSignedContents(this._io, this, this._root);
    }

    return Id014PtkathmaBlockHeaderAlphaFullHeader;
  })();

  var Id014PtkathmaBlockHeaderAlphaSignedContents = Id014PtkathmaBlockHeader.Id014PtkathmaBlockHeaderAlphaSignedContents = (function() {
    function Id014PtkathmaBlockHeaderAlphaSignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaBlockHeaderAlphaSignedContents.prototype._read = function() {
      this.id014PtkathmaBlockHeaderAlphaUnsignedContents = new Id014PtkathmaBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
      this.signature = this._io.readBytes(64);
    }

    return Id014PtkathmaBlockHeaderAlphaSignedContents;
  })();

  var Id014PtkathmaBlockHeaderAlphaUnsignedContents = Id014PtkathmaBlockHeader.Id014PtkathmaBlockHeaderAlphaUnsignedContents = (function() {
    function Id014PtkathmaBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.payloadHash = this._io.readBytes(32);
      this.payloadRound = this._io.readS4be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id014PtkathmaBlockHeader.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingToggleVote = new Id014PtkathmaLiquidityBakingToggleVote(this._io, this, this._root);
    }

    return Id014PtkathmaBlockHeaderAlphaUnsignedContents;
  })();

  var Id014PtkathmaLiquidityBakingToggleVote = Id014PtkathmaBlockHeader.Id014PtkathmaLiquidityBakingToggleVote = (function() {
    function Id014PtkathmaLiquidityBakingToggleVote(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id014PtkathmaLiquidityBakingToggleVote.prototype._read = function() {
      this.id014PtkathmaLiquidityBakingToggleVote = this._io.readS1();
    }

    return Id014PtkathmaLiquidityBakingToggleVote;
  })();

  return Id014PtkathmaBlockHeader;
})();
return Id014PtkathmaBlockHeader;
}));
