// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id010PtgranadBlockHeaderContents = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 010-PtGRANAD.block_header.contents
 */

var Id010PtgranadBlockHeaderContents = (function() {
  Id010PtgranadBlockHeaderContents.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id010PtgranadBlockHeaderContents(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id010PtgranadBlockHeaderContents.prototype._read = function() {
    this.id010PtgranadBlockHeaderAlphaUnsignedContents = new Id010PtgranadBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id010PtgranadBlockHeaderAlphaUnsignedContents = Id010PtgranadBlockHeaderContents.Id010PtgranadBlockHeaderAlphaUnsignedContents = (function() {
    function Id010PtgranadBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id010PtgranadBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id010PtgranadBlockHeaderContents.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
      this.liquidityBakingEscapeVote = this._io.readU1();
    }

    return Id010PtgranadBlockHeaderAlphaUnsignedContents;
  })();

  return Id010PtgranadBlockHeaderContents;
})();
return Id010PtgranadBlockHeaderContents;
}));
