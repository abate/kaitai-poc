// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaFrozenStaker = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.frozen_staker
 */

var AlphaFrozenStaker = (function() {
  AlphaFrozenStaker.AlphaContractIdTag = Object.freeze({
    IMPLICIT: 0,
    ORIGINATED: 1,

    0: "IMPLICIT",
    1: "ORIGINATED",
  });

  AlphaFrozenStaker.AlphaFrozenStakerTag = Object.freeze({
    SINGLE: 0,
    SHARED: 1,
    BAKER: 2,
    BAKER_EDGE: 3,

    0: "SINGLE",
    1: "SHARED",
    2: "BAKER",
    3: "BAKER_EDGE",
  });

  AlphaFrozenStaker.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function AlphaFrozenStaker(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaFrozenStaker.prototype._read = function() {
    this.alphaFrozenStaker = new AlphaFrozenStaker(this._io, this, this._root);
  }

  var AlphaContractId = AlphaFrozenStaker.AlphaContractId = (function() {
    function AlphaContractId(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaContractId.prototype._read = function() {
      this.alphaContractIdTag = this._io.readU1();
      if (this.alphaContractIdTag == AlphaFrozenStaker.AlphaContractIdTag.IMPLICIT) {
        this.implicit = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.alphaContractIdTag == AlphaFrozenStaker.AlphaContractIdTag.ORIGINATED) {
        this.originated = new Originated(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return AlphaContractId;
  })();

  var Originated = AlphaFrozenStaker.Originated = (function() {
    function Originated(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Originated.prototype._read = function() {
      this.contractHash = this._io.readBytes(20);
      this.originatedPadding = this._io.readBytes(1);
    }

    /**
     * This field is for padding, ignore
     */

    return Originated;
  })();

  var AlphaFrozenStaker = AlphaFrozenStaker.AlphaFrozenStaker = (function() {
    function AlphaFrozenStaker(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaFrozenStaker.prototype._read = function() {
      this.alphaFrozenStakerTag = this._io.readU1();
      if (this.alphaFrozenStakerTag == AlphaFrozenStaker.AlphaFrozenStakerTag.SINGLE) {
        this.single = new Single(this._io, this, this._root);
      }
      if (this.alphaFrozenStakerTag == AlphaFrozenStaker.AlphaFrozenStakerTag.SHARED) {
        this.shared = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.alphaFrozenStakerTag == AlphaFrozenStaker.AlphaFrozenStakerTag.BAKER) {
        this.baker = new PublicKeyHash(this._io, this, this._root);
      }
      if (this.alphaFrozenStakerTag == AlphaFrozenStaker.AlphaFrozenStakerTag.BAKER_EDGE) {
        this.bakerEdge = new PublicKeyHash(this._io, this, this._root);
      }
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return AlphaFrozenStaker;
  })();

  var Single = AlphaFrozenStaker.Single = (function() {
    function Single(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Single.prototype._read = function() {
      this.contract = new AlphaContractId(this._io, this, this._root);
      this.delegate = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A contract handle: A contract notation as given to an RPC or inside scripts. Can be a base58 implicit contract hash or a base58 originated contract hash.
     */

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return Single;
  })();

  var PublicKeyHash = AlphaFrozenStaker.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == AlphaFrozenStaker.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaFrozenStaker.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaFrozenStaker.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaFrozenStaker.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  /**
   * frozen_staker: Abstract notion of staker used in operation receipts for frozen deposits, either a single staker or all the stakers delegating to some delegate.
   */

  return AlphaFrozenStaker;
})();
return AlphaFrozenStaker;
}));
