// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id008Ptedo2zkBlockHeaderContents = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 008-PtEdo2Zk.block_header.contents
 */

var Id008Ptedo2zkBlockHeaderContents = (function() {
  Id008Ptedo2zkBlockHeaderContents.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id008Ptedo2zkBlockHeaderContents(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkBlockHeaderContents.prototype._read = function() {
    this.id008Ptedo2zkBlockHeaderAlphaUnsignedContents = new Id008Ptedo2zkBlockHeaderAlphaUnsignedContents(this._io, this, this._root);
  }

  var Id008Ptedo2zkBlockHeaderAlphaUnsignedContents = Id008Ptedo2zkBlockHeaderContents.Id008Ptedo2zkBlockHeaderAlphaUnsignedContents = (function() {
    function Id008Ptedo2zkBlockHeaderAlphaUnsignedContents(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id008Ptedo2zkBlockHeaderAlphaUnsignedContents.prototype._read = function() {
      this.priority = this._io.readU2be();
      this.proofOfWorkNonce = this._io.readBytes(8);
      this.seedNonceHashTag = this._io.readU1();
      if (this.seedNonceHashTag == Id008Ptedo2zkBlockHeaderContents.Bool.TRUE) {
        this.seedNonceHash = this._io.readBytes(32);
      }
    }

    return Id008Ptedo2zkBlockHeaderAlphaUnsignedContents;
  })();

  return Id008Ptedo2zkBlockHeaderContents;
})();
return Id008Ptedo2zkBlockHeaderContents;
}));
