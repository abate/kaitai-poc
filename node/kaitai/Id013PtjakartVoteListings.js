// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id013PtjakartVoteListings = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 013-PtJakart.vote.listings
 */

var Id013PtjakartVoteListings = (function() {
  Id013PtjakartVoteListings.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
  });

  function Id013PtjakartVoteListings(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id013PtjakartVoteListings.prototype._read = function() {
    this.lenId013PtjakartVoteListings = this._io.readU4be();
    if (!(this.lenId013PtjakartVoteListings <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenId013PtjakartVoteListings, this._io, "/seq/0");
    }
    this._raw_id013PtjakartVoteListings = this._io.readBytes(this.lenId013PtjakartVoteListings);
    var _io__raw_id013PtjakartVoteListings = new KaitaiStream(this._raw_id013PtjakartVoteListings);
    this.id013PtjakartVoteListings = new Id013PtjakartVoteListings(_io__raw_id013PtjakartVoteListings, this, this._root);
  }

  var Id013PtjakartVoteListings = Id013PtjakartVoteListings.Id013PtjakartVoteListings = (function() {
    function Id013PtjakartVoteListings(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartVoteListings.prototype._read = function() {
      this.id013PtjakartVoteListingsEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.id013PtjakartVoteListingsEntries.push(new Id013PtjakartVoteListingsEntries(this._io, this, this._root));
        i++;
      }
    }

    return Id013PtjakartVoteListings;
  })();

  var Id013PtjakartVoteListingsEntries = Id013PtjakartVoteListings.Id013PtjakartVoteListingsEntries = (function() {
    function Id013PtjakartVoteListingsEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    Id013PtjakartVoteListingsEntries.prototype._read = function() {
      this.pkh = new PublicKeyHash(this._io, this, this._root);
      this.votingPower = this._io.readS8be();
    }

    /**
     * A Ed25519, Secp256k1, or P256 public key hash
     */

    return Id013PtjakartVoteListingsEntries;
  })();

  var PublicKeyHash = Id013PtjakartVoteListings.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == Id013PtjakartVoteListings.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id013PtjakartVoteListings.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == Id013PtjakartVoteListings.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return Id013PtjakartVoteListings;
})();
return Id013PtjakartVoteListings;
}));
