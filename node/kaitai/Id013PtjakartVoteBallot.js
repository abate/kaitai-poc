// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id013PtjakartVoteBallot = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 013-PtJakart.vote.ballot
 */

var Id013PtjakartVoteBallot = (function() {
  function Id013PtjakartVoteBallot(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id013PtjakartVoteBallot.prototype._read = function() {
    this.id013PtjakartVoteBallot = this._io.readS1();
  }

  return Id013PtjakartVoteBallot;
})();
return Id013PtjakartVoteBallot;
}));
