// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id016PtmumbaiLevel = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 016-PtMumbai.level
 */

var Id016PtmumbaiLevel = (function() {
  Id016PtmumbaiLevel.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function Id016PtmumbaiLevel(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id016PtmumbaiLevel.prototype._read = function() {
    this.level = this._io.readS4be();
    this.levelPosition = this._io.readS4be();
    this.cycle = this._io.readS4be();
    this.cyclePosition = this._io.readS4be();
    this.expectedCommitment = this._io.readU1();
  }

  /**
   * The level of the block relative to genesis. This is also the Shell's notion of level.
   */

  /**
   * The level of the block relative to the successor of the genesis block. More precisely, it is the position of the block relative to the block that starts the "Alpha family" of protocols, which includes all protocols except Genesis (that is, from 001 onwards).
   */

  /**
   * The current cycle's number. Note that cycles are a protocol-specific notion. As a result, the cycle number starts at 0 with the first block of the Alpha family of protocols.
   */

  /**
   * The current level of the block relative to the first block of the current cycle.
   */

  /**
   * Tells whether the baker of this block has to commit a seed nonce hash.
   */

  return Id016PtmumbaiLevel;
})();
return Id016PtmumbaiLevel;
}));
