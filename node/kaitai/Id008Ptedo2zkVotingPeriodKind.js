// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id008Ptedo2zkVotingPeriodKind = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 008-PtEdo2Zk.voting_period.kind
 */

var Id008Ptedo2zkVotingPeriodKind = (function() {
  Id008Ptedo2zkVotingPeriodKind.Id008Ptedo2zkVotingPeriodKindTag = Object.freeze({
    PROPOSAL: 0,
    TESTING_VOTE: 1,
    TESTING: 2,
    PROMOTION_VOTE: 3,
    ADOPTION: 4,

    0: "PROPOSAL",
    1: "TESTING_VOTE",
    2: "TESTING",
    3: "PROMOTION_VOTE",
    4: "ADOPTION",
  });

  function Id008Ptedo2zkVotingPeriodKind(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id008Ptedo2zkVotingPeriodKind.prototype._read = function() {
    this.id008Ptedo2zkVotingPeriodKindTag = this._io.readU1();
  }

  return Id008Ptedo2zkVotingPeriodKind;
})();
return Id008Ptedo2zkVotingPeriodKind;
}));
