// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './TimestampProtocol'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./TimestampProtocol'));
  } else {
    root.Id007Psdelph1Timestamp = factory(root.KaitaiStream, root.TimestampProtocol);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, TimestampProtocol) {
/**
 * Encoding id: 007-PsDELPH1.timestamp
 */

var Id007Psdelph1Timestamp = (function() {
  function Id007Psdelph1Timestamp(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1Timestamp.prototype._read = function() {
    this.id007Psdelph1Timestamp = new TimestampProtocol(this._io, this, null);
  }

  return Id007Psdelph1Timestamp;
})();
return Id007Psdelph1Timestamp;
}));
