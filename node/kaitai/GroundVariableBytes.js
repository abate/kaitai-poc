// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.GroundVariableBytes = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: ground.variable.bytes
 */

var GroundVariableBytes = (function() {
  function GroundVariableBytes(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  GroundVariableBytes.prototype._read = function() {
    this.groundVariableBytes = this._io.readBytesFull();
  }

  return GroundVariableBytes;
})();
return GroundVariableBytes;
}));
