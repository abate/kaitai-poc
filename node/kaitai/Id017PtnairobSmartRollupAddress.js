// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.Id017PtnairobSmartRollupAddress = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: 017-PtNairob.smart_rollup.address
 */

var Id017PtnairobSmartRollupAddress = (function() {
  function Id017PtnairobSmartRollupAddress(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id017PtnairobSmartRollupAddress.prototype._read = function() {
    this.smartRollupHash = this._io.readBytes(20);
  }

  return Id017PtnairobSmartRollupAddress;
})();
return Id017PtnairobSmartRollupAddress;
}));
