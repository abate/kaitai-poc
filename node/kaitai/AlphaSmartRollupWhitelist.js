// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'));
  } else {
    root.AlphaSmartRollupWhitelist = factory(root.KaitaiStream);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream) {
/**
 * Encoding id: alpha.smart_rollup.whitelist
 */

var AlphaSmartRollupWhitelist = (function() {
  AlphaSmartRollupWhitelist.PublicKeyHashTag = Object.freeze({
    ED25519: 0,
    SECP256K1: 1,
    P256: 2,
    BLS: 3,

    0: "ED25519",
    1: "SECP256K1",
    2: "P256",
    3: "BLS",
  });

  function AlphaSmartRollupWhitelist(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  AlphaSmartRollupWhitelist.prototype._read = function() {
    this.lenAlphaSmartRollupWhitelist = this._io.readU4be();
    if (!(this.lenAlphaSmartRollupWhitelist <= 1073741823)) {
      throw new KaitaiStream.ValidationGreaterThanError(1073741823, this.lenAlphaSmartRollupWhitelist, this._io, "/seq/0");
    }
    this._raw_alphaSmartRollupWhitelist = this._io.readBytes(this.lenAlphaSmartRollupWhitelist);
    var _io__raw_alphaSmartRollupWhitelist = new KaitaiStream(this._raw_alphaSmartRollupWhitelist);
    this.alphaSmartRollupWhitelist = new AlphaSmartRollupWhitelist(_io__raw_alphaSmartRollupWhitelist, this, this._root);
  }

  var AlphaSmartRollupWhitelist = AlphaSmartRollupWhitelist.AlphaSmartRollupWhitelist = (function() {
    function AlphaSmartRollupWhitelist(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaSmartRollupWhitelist.prototype._read = function() {
      this.alphaSmartRollupWhitelistEntries = [];
      var i = 0;
      while (!this._io.isEof()) {
        this.alphaSmartRollupWhitelistEntries.push(new AlphaSmartRollupWhitelistEntries(this._io, this, this._root));
        i++;
      }
    }

    return AlphaSmartRollupWhitelist;
  })();

  var AlphaSmartRollupWhitelistEntries = AlphaSmartRollupWhitelist.AlphaSmartRollupWhitelistEntries = (function() {
    function AlphaSmartRollupWhitelistEntries(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    AlphaSmartRollupWhitelistEntries.prototype._read = function() {
      this.signaturePublicKeyHash = new PublicKeyHash(this._io, this, this._root);
    }

    /**
     * A Ed25519, Secp256k1, P256, or BLS public key hash
     */

    return AlphaSmartRollupWhitelistEntries;
  })();

  var PublicKeyHash = AlphaSmartRollupWhitelist.PublicKeyHash = (function() {
    function PublicKeyHash(_io, _parent, _root) {
      this._io = _io;
      this._parent = _parent;
      this._root = _root || this;

      this._read();
    }
    PublicKeyHash.prototype._read = function() {
      this.publicKeyHashTag = this._io.readU1();
      if (this.publicKeyHashTag == AlphaSmartRollupWhitelist.PublicKeyHashTag.ED25519) {
        this.ed25519 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaSmartRollupWhitelist.PublicKeyHashTag.SECP256K1) {
        this.secp256k1 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaSmartRollupWhitelist.PublicKeyHashTag.P256) {
        this.p256 = this._io.readBytes(20);
      }
      if (this.publicKeyHashTag == AlphaSmartRollupWhitelist.PublicKeyHashTag.BLS) {
        this.bls = this._io.readBytes(20);
      }
    }

    return PublicKeyHash;
  })();

  return AlphaSmartRollupWhitelist;
})();
return AlphaSmartRollupWhitelist;
}));
