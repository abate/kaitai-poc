// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './Operation'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./Operation'));
  } else {
    root.Id007Psdelph1OperationRaw = factory(root.KaitaiStream, root.Operation);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, Operation) {
/**
 * Encoding id: 007-PsDELPH1.operation.raw
 */

var Id007Psdelph1OperationRaw = (function() {
  function Id007Psdelph1OperationRaw(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  Id007Psdelph1OperationRaw.prototype._read = function() {
    this.id007Psdelph1OperationRaw = new Operation(this._io, this, null);
  }

  return Id007Psdelph1OperationRaw;
})();
return Id007Psdelph1OperationRaw;
}));
