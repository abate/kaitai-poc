// This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

(function (root, factory) {
  if (typeof define === 'function' && define.amd) {
    define(['kaitai-struct/KaitaiStream', './P2pAddress'], factory);
  } else if (typeof module === 'object' && module.exports) {
    module.exports = factory(require('kaitai-struct/KaitaiStream'), require('./P2pAddress'));
  } else {
    root.P2pConnectionId = factory(root.KaitaiStream, root.P2pAddress);
  }
}(typeof self !== 'undefined' ? self : this, function (KaitaiStream, P2pAddress) {
/**
 * Encoding id: p2p_connection.id
 * Description: The identifier for a p2p connection. It includes an address and a port number.
 */

var P2pConnectionId = (function() {
  P2pConnectionId.Bool = Object.freeze({
    FALSE: 0,
    TRUE: 255,

    0: "FALSE",
    255: "TRUE",
  });

  function P2pConnectionId(_io, _parent, _root) {
    this._io = _io;
    this._parent = _parent;
    this._root = _root || this;

    this._read();
  }
  P2pConnectionId.prototype._read = function() {
    this.addr = new P2pAddress(this._io, this, null);
    this.portTag = this._io.readU1();
    if (this.portTag == P2pConnectionId.Bool.TRUE) {
      this.port = this._io.readU2be();
    }
  }

  return P2pConnectionId;
})();
return P2pConnectionId;
}));
