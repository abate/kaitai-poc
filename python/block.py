#!/usr/bin/env python3

import requests
import base58
import json
from kaitaistruct import KaitaiStream, BytesIO
from kaitai.block_header__shell import BlockHeaderShell
from datetime import datetime

BASE_URL = "https://rpc.ghostnet.teztnets.com"

# generic RPC function
def request_data(url, headers):
    try:
        response = requests.get(url, headers=headers)
        response.raise_for_status()  # Check for HTTP errors
        return response.content
    except requests.exceptions.RequestException as err:
        # Handle any request errors
        print(f"Error: {err}")
        return None

# call RPC using binary encoding
def get_block_header_binary(block_hash):
    headers = {"Accept": "application/octet-stream"}
    url = f"{BASE_URL}/chains/main/blocks/{block_hash}/header/shell"
    # we remove the first 4 bytes that are the len of the
    # answer. See octet_stream function in octez
    return bytearray(request_data(url,headers))[4:]

# call RPC using json encoding
def get_block_header_json(block_hash):
    headers = {"Accept": "application/json"}
    url = f"{BASE_URL}/chains/main/blocks/{block_hash}/header/shell"
    return request_data(url,headers)

# get the block data in binary form and decode it using kaitai
def get_block(block_hash):

    header_binary = get_block_header_binary(block_hash)
    data = BlockHeaderShell(KaitaiStream(BytesIO(header_binary)))

    date = datetime.fromtimestamp(data.timestamp.timestamp__protocol)
    value = {
        "level": data.level,
        "proto": data.proto,
        "timestamp": date,
        "predecessor_hex": data.predecessor.hex(),
        "predecessor": base58.b58encode_check((b"\x01\x34" + data.predecessor))
    }
    return json.dumps(value,  indent=4, sort_keys=True, default=str)

block_hash = "BLYDg1hJPTMBNzcp72WTUVKZLLPnB88WLzKsyUdLAWM2Xn1qxbD"
print(get_block(block_hash))

# get the block data directly in json form

jdata=get_block_header_json(block_hash)
data = json.loads(jdata.decode().replace("'", '"'))
print(json.dumps(data, sort_keys=True, indent=2))
