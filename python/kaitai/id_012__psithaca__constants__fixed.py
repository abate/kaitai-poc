# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id012PsithacaConstantsFixed(KaitaiStruct):
    """Encoding id: 012-Psithaca.constants.fixed."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.proof_of_work_nonce_size = self._io.read_u1()
        self.nonce_length = self._io.read_u1()
        self.max_anon_ops_per_block = self._io.read_u1()
        self.max_operation_data_length = Id012PsithacaConstantsFixed.Int31(self._io, self, self._root)
        self.max_proposals_per_delegate = self._io.read_u1()
        self.max_micheline_node_count = Id012PsithacaConstantsFixed.Int31(self._io, self, self._root)
        self.max_micheline_bytes_limit = Id012PsithacaConstantsFixed.Int31(self._io, self, self._root)
        self.max_allowed_global_constants_depth = Id012PsithacaConstantsFixed.Int31(self._io, self, self._root)
        self.cache_layout = Id012PsithacaConstantsFixed.CacheLayout0(self._io, self, self._root)
        self.michelson_maximum_type_size = self._io.read_u2be()

    class CacheLayout(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.cache_layout_entries = []
            i = 0
            while not self._io.is_eof():
                self.cache_layout_entries.append(Id012PsithacaConstantsFixed.CacheLayoutEntries(self._io, self, self._root))
                i += 1



    class CacheLayout0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_cache_layout = self._io.read_u4be()
            if not self.len_cache_layout <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_cache_layout, self._io, u"/types/cache_layout_0/seq/0")
            self._raw_cache_layout = self._io.read_bytes(self.len_cache_layout)
            _io__raw_cache_layout = KaitaiStream(BytesIO(self._raw_cache_layout))
            self.cache_layout = Id012PsithacaConstantsFixed.CacheLayout(_io__raw_cache_layout, self, self._root)


    class CacheLayoutEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.cache_layout_elt = self._io.read_s8be()


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")



