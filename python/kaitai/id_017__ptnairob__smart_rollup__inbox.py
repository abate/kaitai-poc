# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id017PtnairobSmartRollupInbox(KaitaiStruct):
    """Encoding id: 017-PtNairob.smart_rollup.inbox."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.level = self._io.read_s4be()
        self.old_levels_messages = Id017PtnairobSmartRollupInbox.OldLevelsMessages(self._io, self, self._root)

    class BackPointers0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_back_pointers = self._io.read_u4be()
            if not self.len_back_pointers <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_back_pointers, self._io, u"/types/back_pointers_0/seq/0")
            self._raw_back_pointers = self._io.read_bytes(self.len_back_pointers)
            _io__raw_back_pointers = KaitaiStream(BytesIO(self._raw_back_pointers))
            self.back_pointers = Id017PtnairobSmartRollupInbox.BackPointers(_io__raw_back_pointers, self, self._root)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id017PtnairobSmartRollupInbox.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class OldLevelsMessages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.index = Id017PtnairobSmartRollupInbox.N(self._io, self, self._root)
            self.content = Id017PtnairobSmartRollupInbox.Content(self._io, self, self._root)
            self.back_pointers = Id017PtnairobSmartRollupInbox.BackPointers0(self._io, self, self._root)


    class Content(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.hash = self._io.read_bytes(32)
            self.level = self._io.read_s4be()


    class BackPointers(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.back_pointers_entries = []
            i = 0
            while not self._io.is_eof():
                self.back_pointers_entries.append(Id017PtnairobSmartRollupInbox.BackPointersEntries(self._io, self, self._root))
                i += 1



    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class BackPointersEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.smart_rollup_inbox_hash = self._io.read_bytes(32)



