# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Protocol(KaitaiStruct):
    """Encoding id: protocol
    Description: The environment a protocol relies on and the components a protocol is made of."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.expected_env_version = Protocol.ProtocolEnvironmentVersion(self._io, self, self._root)
        self.components = Protocol.Components0(self._io, self, self._root)

    class Components0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_components = self._io.read_u4be()
            if not self.len_components <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_components, self._io, u"/types/components_0/seq/0")
            self._raw_components = self._io.read_bytes(self.len_components)
            _io__raw_components = KaitaiStream(BytesIO(self._raw_components))
            self.components = Protocol.Components(_io__raw_components, self, self._root)


    class ComponentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.name = Protocol.BytesDynUint30(self._io, self, self._root)
            self.interface_tag = KaitaiStream.resolve_enum(Protocol.Bool, self._io.read_u1())
            if self.interface_tag == Protocol.Bool.true:
                self.interface = Protocol.BytesDynUint30(self._io, self, self._root)

            self.implementation = Protocol.BytesDynUint30(self._io, self, self._root)


    class Components(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.components_entries = []
            i = 0
            while not self._io.is_eof():
                self.components_entries.append(Protocol.ComponentsEntries(self._io, self, self._root))
                i += 1



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class ProtocolEnvironmentVersion(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol__environment_version = self._io.read_u2be()



