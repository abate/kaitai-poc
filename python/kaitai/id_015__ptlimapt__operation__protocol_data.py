# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import operation__shell_header
from kaitai import block_header__shell
class Id015PtlimaptOperationProtocolData(KaitaiStruct):
    """Encoding id: 015-PtLimaPt.operation.protocol_data."""

    class RevealProofTag(Enum):
        raw__data__proof = 0

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class PvmKind(Enum):
        arith_pvm_kind = 0
        wasm_2_0_0_pvm_kind = 1

    class Micheline015PtlimaptMichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10

    class Id015PtlimaptOperationAlphaContentsTag(Enum):
        seed_nonce_revelation = 1
        double_endorsement_evidence = 2
        double_baking_evidence = 3
        activate_account = 4
        proposals = 5
        ballot = 6
        double_preendorsement_evidence = 7
        vdf_revelation = 8
        drain_delegate = 9
        failing_noop = 17
        preendorsement = 20
        endorsement = 21
        dal_slot_availability = 22
        reveal = 107
        transaction = 108
        origination = 109
        delegation = 110
        register_global_constant = 111
        set_deposits_limit = 112
        increase_paid_storage = 113
        update_consensus_key = 114
        tx_rollup_origination = 150
        tx_rollup_submit_batch = 151
        tx_rollup_commit = 152
        tx_rollup_return_bond = 153
        tx_rollup_finalize_commitment = 154
        tx_rollup_remove_commitment = 155
        tx_rollup_rejection = 156
        tx_rollup_dispatch_tickets = 157
        transfer_ticket = 158
        sc_rollup_originate = 200
        sc_rollup_add_messages = 201
        sc_rollup_cement = 202
        sc_rollup_publish = 203
        sc_rollup_refute = 204
        sc_rollup_timeout = 205
        sc_rollup_execute_outbox_message = 206
        sc_rollup_recover_bond = 207
        sc_rollup_dal_slot_subscribe = 208
        dal_publish_slot_header = 230
        zk_rollup_origination = 250
        zk_rollup_publish = 251

    class PvmStepTag(Enum):
        arithmetic__pvm__with__proof = 0
        wasm__2__0__0__pvm__with__proof = 1
        unencodable = 255

    class Id015PtlimaptInlinedEndorsementMempoolContentsTag(Enum):
        endorsement = 21

    class ProofTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3

    class TreeEncodingTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__7 = 7
        case__8 = 8
        case__9 = 9
        case__10 = 10
        case__11 = 11
        case__12 = 12
        case__13 = 13
        case__14 = 14
        case__15 = 15
        case__16 = 16
        case__17 = 17
        case__18 = 18
        case__19 = 19
        case__20 = 20
        case__21 = 21
        case__22 = 22
        case__23 = 23
        case__24 = 24
        case__25 = 25
        case__26 = 26
        case__27 = 27
        case__28 = 28
        case__29 = 29
        case__30 = 30
        case__31 = 31
        case__32 = 32
        case__33 = 33
        case__34 = 34
        case__35 = 35
        case__36 = 36
        case__37 = 37
        case__38 = 38
        case__39 = 39
        case__40 = 40
        case__41 = 41
        case__42 = 42
        case__43 = 43
        case__44 = 44
        case__45 = 45
        case__46 = 46
        case__47 = 47
        case__48 = 48
        case__49 = 49
        case__50 = 50
        case__51 = 51
        case__52 = 52
        case__53 = 53
        case__54 = 54
        case__55 = 55
        case__56 = 56
        case__57 = 57
        case__58 = 58
        case__59 = 59
        case__60 = 60
        case__61 = 61
        case__62 = 62
        case__63 = 63
        case__64 = 64
        case__65 = 65
        case__66 = 66
        case__67 = 67
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__132 = 132
        case__133 = 133
        case__134 = 134
        case__135 = 135
        case__136 = 136
        case__137 = 137
        case__138 = 138
        case__139 = 139
        case__140 = 140
        case__141 = 141
        case__142 = 142
        case__143 = 143
        case__144 = 144
        case__145 = 145
        case__146 = 146
        case__147 = 147
        case__148 = 148
        case__149 = 149
        case__150 = 150
        case__151 = 151
        case__152 = 152
        case__153 = 153
        case__154 = 154
        case__155 = 155
        case__156 = 156
        case__157 = 157
        case__158 = 158
        case__159 = 159
        case__160 = 160
        case__161 = 161
        case__162 = 162
        case__163 = 163
        case__164 = 164
        case__165 = 165
        case__166 = 166
        case__167 = 167
        case__168 = 168
        case__169 = 169
        case__170 = 170
        case__171 = 171
        case__172 = 172
        case__173 = 173
        case__174 = 174
        case__175 = 175
        case__176 = 176
        case__177 = 177
        case__178 = 178
        case__179 = 179
        case__180 = 180
        case__181 = 181
        case__182 = 182
        case__183 = 183
        case__184 = 184
        case__185 = 185
        case__186 = 186
        case__187 = 187
        case__188 = 188
        case__189 = 189
        case__190 = 190
        case__191 = 191
        case__192 = 192
        case__193 = 193
        case__195 = 195
        case__200 = 200
        case__208 = 208
        case__216 = 216
        case__217 = 217
        case__218 = 218
        case__219 = 219
        case__224 = 224

    class OpEltField1Tag(Enum):
        none = 0
        some = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id015PtlimaptEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        deposit = 5
        named = 255

    class Id015PtlimaptInlinedPreendorsementContentsTag(Enum):
        preendorsement = 20

    class PredecessorTag(Enum):
        none = 0
        some = 1

    class Id015PtlimaptContractIdTag(Enum):
        implicit = 0
        originated = 1

    class AmountTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3

    class Id015PtlimaptMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left = 5
        none_0 = 6
        pair_1 = 7
        right = 8
        some_0 = 9
        true = 10
        unit_0 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left_0 = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right_0 = 68
        size = 69
        some = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_1 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction_deprecated = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_deprecated = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140
        chest = 141
        chest_key = 142
        open_chest = 143
        view_0 = 144
        view = 145
        constant = 146
        sub_mutez = 147
        tx_rollup_l2_address = 148
        min_block_time = 149
        sapling_transaction = 150
        emit = 151
        lambda_rec = 152
        lambda_rec_0 = 153
        ticket_0 = 154

    class InodeTreeTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__7 = 7
        case__8 = 8
        case__9 = 9
        case__10 = 10
        case__11 = 11
        case__12 = 12
        case__13 = 13
        case__14 = 14
        case__15 = 15
        case__16 = 16
        case__17 = 17
        case__18 = 18
        case__19 = 19
        case__20 = 20
        case__21 = 21
        case__22 = 22
        case__23 = 23
        case__24 = 24
        case__25 = 25
        case__26 = 26
        case__27 = 27
        case__28 = 28
        case__29 = 29
        case__30 = 30
        case__31 = 31
        case__32 = 32
        case__33 = 33
        case__34 = 34
        case__35 = 35
        case__36 = 36
        case__37 = 37
        case__38 = 38
        case__39 = 39
        case__40 = 40
        case__41 = 41
        case__42 = 42
        case__43 = 43
        case__44 = 44
        case__45 = 45
        case__46 = 46
        case__47 = 47
        case__48 = 48
        case__49 = 49
        case__50 = 50
        case__51 = 51
        case__52 = 52
        case__53 = 53
        case__54 = 54
        case__55 = 55
        case__56 = 56
        case__57 = 57
        case__58 = 58
        case__59 = 59
        case__60 = 60
        case__61 = 61
        case__62 = 62
        case__63 = 63
        case__64 = 64
        case__65 = 65
        case__66 = 66
        case__67 = 67
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__132 = 132
        case__133 = 133
        case__134 = 134
        case__135 = 135
        case__136 = 136
        case__137 = 137
        case__138 = 138
        case__139 = 139
        case__140 = 140
        case__141 = 141
        case__142 = 142
        case__143 = 143
        case__144 = 144
        case__145 = 145
        case__146 = 146
        case__147 = 147
        case__148 = 148
        case__149 = 149
        case__150 = 150
        case__151 = 151
        case__152 = 152
        case__153 = 153
        case__154 = 154
        case__155 = 155
        case__156 = 156
        case__157 = 157
        case__158 = 158
        case__159 = 159
        case__160 = 160
        case__161 = 161
        case__162 = 162
        case__163 = 163
        case__164 = 164
        case__165 = 165
        case__166 = 166
        case__167 = 167
        case__168 = 168
        case__169 = 169
        case__170 = 170
        case__171 = 171
        case__172 = 172
        case__173 = 173
        case__174 = 174
        case__175 = 175
        case__176 = 176
        case__177 = 177
        case__178 = 178
        case__179 = 179
        case__180 = 180
        case__181 = 181
        case__182 = 182
        case__183 = 183
        case__184 = 184
        case__185 = 185
        case__186 = 186
        case__187 = 187
        case__188 = 188
        case__189 = 189
        case__190 = 190
        case__191 = 191
        case__192 = 192
        case__208 = 208
        case__209 = 209
        case__210 = 210
        case__211 = 211
        case__224 = 224

    class StepTag(Enum):
        dissection = 0
        proof = 1

    class MessageTag(Enum):
        batch = 0
        deposit = 1

    class Id015PtlimaptContractIdOriginatedTag(Enum):
        originated = 1

    class InputProofTag(Enum):
        inbox__proof = 0
        reveal__proof = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_015__ptlimapt__operation__alpha__contents_and_signature = Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsAndSignature(self._io, self, self._root)

    class Case131EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131_elt_field0 = self._io.read_u1()
            if not self.len_case__131_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__131_elt_field0, self._io, u"/types/case__131_elt_field0_0/seq/0")
            self._raw_case__131_elt_field0 = self._io.read_bytes(self.len_case__131_elt_field0)
            _io__raw_case__131_elt_field0 = KaitaiStream(BytesIO(self._raw_case__131_elt_field0))
            self.case__131_elt_field0 = Id015PtlimaptOperationProtocolData.Case131EltField0(_io__raw_case__131_elt_field0, self, self._root)


    class Case24(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__24_field0 = self._io.read_u1()
            self.case__24_field1 = []
            for i in range(6):
                self.case__24_field1.append(Id015PtlimaptOperationProtocolData.Case24Field1Entries(self._io, self, self._root))



    class Case189Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__189_elt_field0 = Id015PtlimaptOperationProtocolData.Case189EltField00(self._io, self, self._root)
            self.case__189_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case211Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__211_field1 = self._io.read_bytes_full()


    class Case23Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__23_field1_elt_field0 = self._io.read_u1()
            self.case__23_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case138EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__138_elt_field0 = self._io.read_bytes_full()


    class Id015PtlimaptInlinedPreendorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__preendorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id015PtlimaptOperationProtocolData.Id015PtlimaptInlinedPreendorsementContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Bool, self._io.read_u1())
            if self.signature_tag == Id015PtlimaptOperationProtocolData.Bool.true:
                self.signature = self._io.read_bytes(64)



    class Op20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_0/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id015PtlimaptOperationProtocolData.Op2(_io__raw_op2, self, self._root)


    class Case177Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__177_elt_field0 = Id015PtlimaptOperationProtocolData.Case177EltField00(self._io, self, self._root)
            self.case__177_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__1_field0 = self._io.read_s2be()
            self.case__1_field1 = self._io.read_bytes(32)
            self.case__1_field2 = self._io.read_bytes(32)
            self.case__1_field3 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case42Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__42_field1_elt_field0 = self._io.read_u1()
            self.case__42_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case163Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__163_elt_field0 = Id015PtlimaptOperationProtocolData.Case163EltField00(self._io, self, self._root)
            self.case__163_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case192(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__192 = self._io.read_bytes_full()


    class Case183Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__183_elt_field0 = Id015PtlimaptOperationProtocolData.Case183EltField00(self._io, self, self._root)
            self.case__183_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case55Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__55_field1_elt_field0 = self._io.read_u1()
            self.case__55_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Messages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.messages_entries = []
            i = 0
            while not self._io.is_eof():
                self.messages_entries.append(Id015PtlimaptOperationProtocolData.MessagesEntries(self._io, self, self._root))
                i += 1



    class Case65Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__65_field1_elt = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case211(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__211_field0 = self._io.read_s8be()
            self.case__211_field1 = Id015PtlimaptOperationProtocolData.Case211Field10(self._io, self, self._root)
            self.case__211_field2 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case43(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__43_field0 = self._io.read_s8be()
            self.case__43_field1 = []
            for i in range(10):
                self.case__43_field1.append(Id015PtlimaptOperationProtocolData.Case43Field1Entries(self._io, self, self._root))



    class Case54(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__54_field0 = self._io.read_s4be()
            self.case__54_field1 = []
            for i in range(13):
                self.case__54_field1.append(Id015PtlimaptOperationProtocolData.Case54Field1Entries(self._io, self, self._root))



    class Case138EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__138_elt_field0 = self._io.read_u1()
            if not self.len_case__138_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__138_elt_field0, self._io, u"/types/case__138_elt_field0_0/seq/0")
            self._raw_case__138_elt_field0 = self._io.read_bytes(self.len_case__138_elt_field0)
            _io__raw_case__138_elt_field0 = KaitaiStream(BytesIO(self._raw_case__138_elt_field0))
            self.case__138_elt_field0 = Id015PtlimaptOperationProtocolData.Case138EltField0(_io__raw_case__138_elt_field0, self, self._root)


    class Case134EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__134_elt_field0 = self._io.read_u1()
            if not self.len_case__134_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__134_elt_field0, self._io, u"/types/case__134_elt_field0_0/seq/0")
            self._raw_case__134_elt_field0 = self._io.read_bytes(self.len_case__134_elt_field0)
            _io__raw_case__134_elt_field0 = KaitaiStream(BytesIO(self._raw_case__134_elt_field0))
            self.case__134_elt_field0 = Id015PtlimaptOperationProtocolData.Case134EltField0(_io__raw_case__134_elt_field0, self, self._root)


    class Case32(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__32_field0 = self._io.read_u1()
            self.case__32_field1 = []
            for i in range(8):
                self.case__32_field1.append(Id015PtlimaptOperationProtocolData.Case32Field1Entries(self._io, self, self._root))



    class Case59Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__59_field1_elt_field0 = self._io.read_u1()
            self.case__59_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case65(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__65_field0 = self._io.read_u2be()
            self.case__65_field1 = []
            for i in range(32):
                self.case__65_field1.append(Id015PtlimaptOperationProtocolData.Case65Field1Entries(self._io, self, self._root))



    class Case181Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__181_elt_field0 = Id015PtlimaptOperationProtocolData.Case181EltField00(self._io, self, self._root)
            self.case__181_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Stakers(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alice = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.bob = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)


    class MessageResultPathEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_list_hash = self._io.read_bytes(32)


    class Case66Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__66_field1_elt = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class InodeTree(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inode_tree_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.InodeTreeTag, self._io.read_u1())
            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__4:
                self.case__4 = Id015PtlimaptOperationProtocolData.Case4(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__8:
                self.case__8 = Id015PtlimaptOperationProtocolData.Case8(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__12:
                self.case__12 = Id015PtlimaptOperationProtocolData.Case12(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__16:
                self.case__16 = Id015PtlimaptOperationProtocolData.Case16(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__20:
                self.case__20 = Id015PtlimaptOperationProtocolData.Case20(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__24:
                self.case__24 = Id015PtlimaptOperationProtocolData.Case24(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__28:
                self.case__28 = Id015PtlimaptOperationProtocolData.Case28(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__32:
                self.case__32 = Id015PtlimaptOperationProtocolData.Case32(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__36:
                self.case__36 = Id015PtlimaptOperationProtocolData.Case36(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__40:
                self.case__40 = Id015PtlimaptOperationProtocolData.Case40(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__44:
                self.case__44 = Id015PtlimaptOperationProtocolData.Case44(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__48:
                self.case__48 = Id015PtlimaptOperationProtocolData.Case48(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__52:
                self.case__52 = Id015PtlimaptOperationProtocolData.Case52(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__56:
                self.case__56 = Id015PtlimaptOperationProtocolData.Case56(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__60:
                self.case__60 = Id015PtlimaptOperationProtocolData.Case60(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__64:
                self.case__64 = Id015PtlimaptOperationProtocolData.Case64(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__5:
                self.case__5 = Id015PtlimaptOperationProtocolData.Case5(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__9:
                self.case__9 = Id015PtlimaptOperationProtocolData.Case9(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__13:
                self.case__13 = Id015PtlimaptOperationProtocolData.Case13(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__17:
                self.case__17 = Id015PtlimaptOperationProtocolData.Case17(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__21:
                self.case__21 = Id015PtlimaptOperationProtocolData.Case21(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__25:
                self.case__25 = Id015PtlimaptOperationProtocolData.Case25(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__29:
                self.case__29 = Id015PtlimaptOperationProtocolData.Case29(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__33:
                self.case__33 = Id015PtlimaptOperationProtocolData.Case33(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__37:
                self.case__37 = Id015PtlimaptOperationProtocolData.Case37(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__41:
                self.case__41 = Id015PtlimaptOperationProtocolData.Case41(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__45:
                self.case__45 = Id015PtlimaptOperationProtocolData.Case45(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__49:
                self.case__49 = Id015PtlimaptOperationProtocolData.Case49(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__53:
                self.case__53 = Id015PtlimaptOperationProtocolData.Case53(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__57:
                self.case__57 = Id015PtlimaptOperationProtocolData.Case57(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__61:
                self.case__61 = Id015PtlimaptOperationProtocolData.Case61(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__65:
                self.case__65 = Id015PtlimaptOperationProtocolData.Case65(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__6:
                self.case__6 = Id015PtlimaptOperationProtocolData.Case6(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__10:
                self.case__10 = Id015PtlimaptOperationProtocolData.Case10(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__14:
                self.case__14 = Id015PtlimaptOperationProtocolData.Case14(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__18:
                self.case__18 = Id015PtlimaptOperationProtocolData.Case18(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__22:
                self.case__22 = Id015PtlimaptOperationProtocolData.Case22(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__26:
                self.case__26 = Id015PtlimaptOperationProtocolData.Case26(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__30:
                self.case__30 = Id015PtlimaptOperationProtocolData.Case30(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__34:
                self.case__34 = Id015PtlimaptOperationProtocolData.Case34(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__38:
                self.case__38 = Id015PtlimaptOperationProtocolData.Case38(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__42:
                self.case__42 = Id015PtlimaptOperationProtocolData.Case42(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__46:
                self.case__46 = Id015PtlimaptOperationProtocolData.Case46(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__50:
                self.case__50 = Id015PtlimaptOperationProtocolData.Case50(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__54:
                self.case__54 = Id015PtlimaptOperationProtocolData.Case54(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__58:
                self.case__58 = Id015PtlimaptOperationProtocolData.Case58(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__62:
                self.case__62 = Id015PtlimaptOperationProtocolData.Case62(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__66:
                self.case__66 = Id015PtlimaptOperationProtocolData.Case66(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__3:
                self.case__3 = self._io.read_s8be()

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__7:
                self.case__7 = Id015PtlimaptOperationProtocolData.Case7(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__11:
                self.case__11 = Id015PtlimaptOperationProtocolData.Case11(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__15:
                self.case__15 = Id015PtlimaptOperationProtocolData.Case15(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__19:
                self.case__19 = Id015PtlimaptOperationProtocolData.Case19(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__23:
                self.case__23 = Id015PtlimaptOperationProtocolData.Case23(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__27:
                self.case__27 = Id015PtlimaptOperationProtocolData.Case27(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__31:
                self.case__31 = Id015PtlimaptOperationProtocolData.Case31(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__35:
                self.case__35 = Id015PtlimaptOperationProtocolData.Case35(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__39:
                self.case__39 = Id015PtlimaptOperationProtocolData.Case39(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__43:
                self.case__43 = Id015PtlimaptOperationProtocolData.Case43(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__47:
                self.case__47 = Id015PtlimaptOperationProtocolData.Case47(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__51:
                self.case__51 = Id015PtlimaptOperationProtocolData.Case51(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__55:
                self.case__55 = Id015PtlimaptOperationProtocolData.Case55(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__59:
                self.case__59 = Id015PtlimaptOperationProtocolData.Case59(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__63:
                self.case__63 = Id015PtlimaptOperationProtocolData.Case63(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__67:
                self.case__67 = Id015PtlimaptOperationProtocolData.Case67(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__129:
                self.case__129 = Id015PtlimaptOperationProtocolData.Case129Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__130:
                self.case__130 = Id015PtlimaptOperationProtocolData.Case130Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__131:
                self.case__131 = Id015PtlimaptOperationProtocolData.Case131Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__132:
                self.case__132 = Id015PtlimaptOperationProtocolData.Case132Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__133:
                self.case__133 = Id015PtlimaptOperationProtocolData.Case133Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__134:
                self.case__134 = Id015PtlimaptOperationProtocolData.Case134Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__135:
                self.case__135 = Id015PtlimaptOperationProtocolData.Case135Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__136:
                self.case__136 = Id015PtlimaptOperationProtocolData.Case136Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__137:
                self.case__137 = Id015PtlimaptOperationProtocolData.Case137Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__138:
                self.case__138 = Id015PtlimaptOperationProtocolData.Case138Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__139:
                self.case__139 = Id015PtlimaptOperationProtocolData.Case139Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__140:
                self.case__140 = Id015PtlimaptOperationProtocolData.Case140Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__141:
                self.case__141 = Id015PtlimaptOperationProtocolData.Case141Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__142:
                self.case__142 = Id015PtlimaptOperationProtocolData.Case142Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__143:
                self.case__143 = Id015PtlimaptOperationProtocolData.Case143Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__144:
                self.case__144 = Id015PtlimaptOperationProtocolData.Case144Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__145:
                self.case__145 = Id015PtlimaptOperationProtocolData.Case145Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__146:
                self.case__146 = Id015PtlimaptOperationProtocolData.Case146Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__147:
                self.case__147 = Id015PtlimaptOperationProtocolData.Case147Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__148:
                self.case__148 = Id015PtlimaptOperationProtocolData.Case148Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__149:
                self.case__149 = Id015PtlimaptOperationProtocolData.Case149Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__150:
                self.case__150 = Id015PtlimaptOperationProtocolData.Case150Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__151:
                self.case__151 = Id015PtlimaptOperationProtocolData.Case151Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__152:
                self.case__152 = Id015PtlimaptOperationProtocolData.Case152Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__153:
                self.case__153 = Id015PtlimaptOperationProtocolData.Case153Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__154:
                self.case__154 = Id015PtlimaptOperationProtocolData.Case154Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__155:
                self.case__155 = Id015PtlimaptOperationProtocolData.Case155Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__156:
                self.case__156 = Id015PtlimaptOperationProtocolData.Case156Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__157:
                self.case__157 = Id015PtlimaptOperationProtocolData.Case157Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__158:
                self.case__158 = Id015PtlimaptOperationProtocolData.Case158Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__159:
                self.case__159 = Id015PtlimaptOperationProtocolData.Case159Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__160:
                self.case__160 = Id015PtlimaptOperationProtocolData.Case160Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__161:
                self.case__161 = Id015PtlimaptOperationProtocolData.Case161Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__162:
                self.case__162 = Id015PtlimaptOperationProtocolData.Case162Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__163:
                self.case__163 = Id015PtlimaptOperationProtocolData.Case163Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__164:
                self.case__164 = Id015PtlimaptOperationProtocolData.Case164Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__165:
                self.case__165 = Id015PtlimaptOperationProtocolData.Case165Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__166:
                self.case__166 = Id015PtlimaptOperationProtocolData.Case166Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__167:
                self.case__167 = Id015PtlimaptOperationProtocolData.Case167Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__168:
                self.case__168 = Id015PtlimaptOperationProtocolData.Case168Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__169:
                self.case__169 = Id015PtlimaptOperationProtocolData.Case169Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__170:
                self.case__170 = Id015PtlimaptOperationProtocolData.Case170Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__171:
                self.case__171 = Id015PtlimaptOperationProtocolData.Case171Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__172:
                self.case__172 = Id015PtlimaptOperationProtocolData.Case172Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__173:
                self.case__173 = Id015PtlimaptOperationProtocolData.Case173Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__174:
                self.case__174 = Id015PtlimaptOperationProtocolData.Case174Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__175:
                self.case__175 = Id015PtlimaptOperationProtocolData.Case175Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__176:
                self.case__176 = Id015PtlimaptOperationProtocolData.Case176Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__177:
                self.case__177 = Id015PtlimaptOperationProtocolData.Case177Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__178:
                self.case__178 = Id015PtlimaptOperationProtocolData.Case178Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__179:
                self.case__179 = Id015PtlimaptOperationProtocolData.Case179Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__180:
                self.case__180 = Id015PtlimaptOperationProtocolData.Case180Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__181:
                self.case__181 = Id015PtlimaptOperationProtocolData.Case181Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__182:
                self.case__182 = Id015PtlimaptOperationProtocolData.Case182Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__183:
                self.case__183 = Id015PtlimaptOperationProtocolData.Case183Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__184:
                self.case__184 = Id015PtlimaptOperationProtocolData.Case184Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__185:
                self.case__185 = Id015PtlimaptOperationProtocolData.Case185Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__186:
                self.case__186 = Id015PtlimaptOperationProtocolData.Case186Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__187:
                self.case__187 = Id015PtlimaptOperationProtocolData.Case187Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__188:
                self.case__188 = Id015PtlimaptOperationProtocolData.Case188Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__189:
                self.case__189 = Id015PtlimaptOperationProtocolData.Case189Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__190:
                self.case__190 = Id015PtlimaptOperationProtocolData.Case190Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__191:
                self.case__191 = Id015PtlimaptOperationProtocolData.Case1910(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__192:
                self.case__192 = self._io.read_bytes(32)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__208:
                self.case__208 = Id015PtlimaptOperationProtocolData.Case208(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__209:
                self.case__209 = Id015PtlimaptOperationProtocolData.Case209(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__210:
                self.case__210 = Id015PtlimaptOperationProtocolData.Case210(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationProtocolData.InodeTreeTag.case__211:
                self.case__211 = Id015PtlimaptOperationProtocolData.Case211(self._io, self, self._root)



    class Case171EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__171_elt_field0 = self._io.read_bytes_full()


    class Id015PtlimaptScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.storage = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Case35(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__35_field0 = self._io.read_s8be()
            self.case__35_field1 = []
            for i in range(8):
                self.case__35_field1.append(Id015PtlimaptOperationProtocolData.Case35Field1Entries(self._io, self, self._root))



    class IncreasePaidStorage(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.amount = Id015PtlimaptOperationProtocolData.Z(self._io, self, self._root)
            self.destination = Id015PtlimaptOperationProtocolData.Id015PtlimaptContractIdOriginated(self._io, self, self._root)


    class Case180EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__180_elt_field0 = self._io.read_u1()
            if not self.len_case__180_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__180_elt_field0, self._io, u"/types/case__180_elt_field0_0/seq/0")
            self._raw_case__180_elt_field0 = self._io.read_bytes(self.len_case__180_elt_field0)
            _io__raw_case__180_elt_field0 = KaitaiStream(BytesIO(self._raw_case__180_elt_field0))
            self.case__180_elt_field0 = Id015PtlimaptOperationProtocolData.Case180EltField0(_io__raw_case__180_elt_field0, self, self._root)


    class Case149EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__149_elt_field0 = self._io.read_bytes_full()


    class Case51Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__51_field1_elt_field0 = self._io.read_u1()
            self.case__51_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case17(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__17_field0 = self._io.read_u2be()
            self.case__17_field1 = []
            for i in range(4):
                self.case__17_field1.append(Id015PtlimaptOperationProtocolData.Case17Field1Entries(self._io, self, self._root))



    class ScRollupPublish(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.commitment = Id015PtlimaptOperationProtocolData.Commitment0(self._io, self, self._root)


    class InputProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.input_proof_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.InputProofTag, self._io.read_u1())
            if self.input_proof_tag == Id015PtlimaptOperationProtocolData.InputProofTag.inbox__proof:
                self.inbox__proof = Id015PtlimaptOperationProtocolData.InboxProof(self._io, self, self._root)

            if self.input_proof_tag == Id015PtlimaptOperationProtocolData.InputProofTag.reveal__proof:
                self.reveal__proof = Id015PtlimaptOperationProtocolData.RevealProof(self._io, self, self._root)



    class Case28(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__28_field0 = self._io.read_u1()
            self.case__28_field1 = []
            for i in range(7):
                self.case__28_field1.append(Id015PtlimaptOperationProtocolData.Case28Field1Entries(self._io, self, self._root))



    class Id015PtlimaptBlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_015__ptlimapt__block_header__alpha__signed_contents = Id015PtlimaptOperationProtocolData.Id015PtlimaptBlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Case53Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__53_field1_elt_field0 = self._io.read_u1()
            self.case__53_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case8(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__8_field0 = self._io.read_u1()
            self.case__8_field1 = []
            for i in range(2):
                self.case__8_field1.append(Id015PtlimaptOperationProtocolData.Case8Field1Entries(self._io, self, self._root))



    class Case10Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__10_field1_elt_field0 = self._io.read_u1()
            self.case__10_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Id015PtlimaptLiquidityBakingToggleVote(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__liquidity_baking_toggle_vote = self._io.read_s1()


    class Id015PtlimaptTxRollupId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup_hash = self._io.read_bytes(20)


    class Case189EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__189_elt_field0 = self._io.read_bytes_full()


    class Case150EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__150_elt_field0 = self._io.read_bytes_full()


    class Case10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__10_field0 = self._io.read_s4be()
            self.case__10_field1 = []
            for i in range(2):
                self.case__10_field1.append(Id015PtlimaptOperationProtocolData.Case10Field1Entries(self._io, self, self._root))



    class Case180EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__180_elt_field0 = self._io.read_bytes_full()


    class Case60Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__60_field1 = self._io.read_u4be()
            if not self.len_case__60_field1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__60_field1, self._io, u"/types/case__60_field1_0/seq/0")
            self._raw_case__60_field1 = self._io.read_bytes(self.len_case__60_field1)
            _io__raw_case__60_field1 = KaitaiStream(BytesIO(self._raw_case__60_field1))
            self.case__60_field1 = Id015PtlimaptOperationProtocolData.Case60Field1(_io__raw_case__60_field1, self, self._root)


    class Case61Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__61_field1_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__61_field1_entries.append(Id015PtlimaptOperationProtocolData.Case61Field1Entries(self._io, self, self._root))
                i += 1



    class ActivateAccount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = self._io.read_bytes(20)
            self.secret = self._io.read_bytes(20)


    class Case61(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__61_field0 = self._io.read_u2be()
            self.case__61_field1 = Id015PtlimaptOperationProtocolData.Case61Field10(self._io, self, self._root)


    class Case152EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__152_elt_field0 = self._io.read_bytes_full()


    class Case57Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__57_field1_elt_field0 = self._io.read_u1()
            self.case__57_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case147Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__147_elt_field0 = Id015PtlimaptOperationProtocolData.Case147EltField00(self._io, self, self._root)
            self.case__147_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case62Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__62_field1_elt_field0 = self._io.read_u1()
            self.case__62_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class DoubleEndorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id015PtlimaptOperationProtocolData.Op10(self._io, self, self._root)
            self.op2 = Id015PtlimaptOperationProtocolData.Op20(self._io, self, self._root)


    class Step(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.step_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.StepTag, self._io.read_u1())
            if self.step_tag == Id015PtlimaptOperationProtocolData.StepTag.dissection:
                self.dissection = Id015PtlimaptOperationProtocolData.Dissection0(self._io, self, self._root)

            if self.step_tag == Id015PtlimaptOperationProtocolData.StepTag.proof:
                self.proof = Id015PtlimaptOperationProtocolData.Proof1(self._io, self, self._root)



    class Id015PtlimaptRollupAddress(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__rollup_address = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Case39(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__39_field0 = self._io.read_s8be()
            self.case__39_field1 = []
            for i in range(9):
                self.case__39_field1.append(Id015PtlimaptOperationProtocolData.Case39Field1Entries(self._io, self, self._root))



    class Case9(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__9_field0 = self._io.read_u2be()
            self.case__9_field1 = []
            for i in range(2):
                self.case__9_field1.append(Id015PtlimaptOperationProtocolData.Case9Field1Entries(self._io, self, self._root))



    class Case18Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__18_field1_elt_field0 = self._io.read_u1()
            self.case__18_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case13(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__13_field0 = self._io.read_u2be()
            self.case__13_field1 = []
            for i in range(3):
                self.case__13_field1.append(Id015PtlimaptOperationProtocolData.Case13Field1Entries(self._io, self, self._root))



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Case170EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__170_elt_field0 = self._io.read_u1()
            if not self.len_case__170_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__170_elt_field0, self._io, u"/types/case__170_elt_field0_0/seq/0")
            self._raw_case__170_elt_field0 = self._io.read_bytes(self.len_case__170_elt_field0)
            _io__raw_case__170_elt_field0 = KaitaiStream(BytesIO(self._raw_case__170_elt_field0))
            self.case__170_elt_field0 = Id015PtlimaptOperationProtocolData.Case170EltField0(_io__raw_case__170_elt_field0, self, self._root)


    class Case29Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__29_field1_elt_field0 = self._io.read_u1()
            self.case__29_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case145EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__145_elt_field0 = self._io.read_u1()
            if not self.len_case__145_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__145_elt_field0, self._io, u"/types/case__145_elt_field0_0/seq/0")
            self._raw_case__145_elt_field0 = self._io.read_bytes(self.len_case__145_elt_field0)
            _io__raw_case__145_elt_field0 = KaitaiStream(BytesIO(self._raw_case__145_elt_field0))
            self.case__145_elt_field0 = Id015PtlimaptOperationProtocolData.Case145EltField0(_io__raw_case__145_elt_field0, self, self._root)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id015PtlimaptOperationProtocolData.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id015PtlimaptInlinedEndorsementMempoolContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__endorsement_mempool__contents_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Id015PtlimaptInlinedEndorsementMempoolContentsTag, self._io.read_u1())
            if self.id_015__ptlimapt__inlined__endorsement_mempool__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptInlinedEndorsementMempoolContentsTag.endorsement:
                self.endorsement = Id015PtlimaptOperationProtocolData.Endorsement(self._io, self, self._root)



    class Case186Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__186_elt_field0 = Id015PtlimaptOperationProtocolData.Case186EltField00(self._io, self, self._root)
            self.case__186_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case4(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__4_field0 = self._io.read_u1()
            self.case__4_field1 = []
            for i in range(1):
                self.case__4_field1.append(Id015PtlimaptOperationProtocolData.Case4Field1Entries(self._io, self, self._root))



    class Endorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class Case187EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__187_elt_field0 = self._io.read_bytes_full()


    class Case60Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__60_field1_elt_field0 = self._io.read_u1()
            self.case__60_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case130Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__130_elt_field0 = Id015PtlimaptOperationProtocolData.Case130EltField00(self._io, self, self._root)
            self.case__130_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case136Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__136_elt_field0 = Id015PtlimaptOperationProtocolData.Case136EltField00(self._io, self, self._root)
            self.case__136_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case62Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__62_field1_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__62_field1_entries.append(Id015PtlimaptOperationProtocolData.Case62Field1Entries(self._io, self, self._root))
                i += 1



    class Case63Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__63_field1_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__63_field1_entries.append(Id015PtlimaptOperationProtocolData.Case63Field1Entries(self._io, self, self._root))
                i += 1



    class Case163EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__163_elt_field0 = self._io.read_u1()
            if not self.len_case__163_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__163_elt_field0, self._io, u"/types/case__163_elt_field0_0/seq/0")
            self._raw_case__163_elt_field0 = self._io.read_bytes(self.len_case__163_elt_field0)
            _io__raw_case__163_elt_field0 = KaitaiStream(BytesIO(self._raw_case__163_elt_field0))
            self.case__163_elt_field0 = Id015PtlimaptOperationProtocolData.Case163EltField0(_io__raw_case__163_elt_field0, self, self._root)


    class Proposals0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_proposals = self._io.read_u4be()
            if not self.len_proposals <= 640:
                raise kaitaistruct.ValidationGreaterThanError(640, self.len_proposals, self._io, u"/types/proposals_0/seq/0")
            self._raw_proposals = self._io.read_bytes(self.len_proposals)
            _io__raw_proposals = KaitaiStream(BytesIO(self._raw_proposals))
            self.proposals = Id015PtlimaptOperationProtocolData.Proposals(_io__raw_proposals, self, self._root)


    class MessagesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_hash = self._io.read_bytes(32)


    class Case157Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__157_elt_field0 = Id015PtlimaptOperationProtocolData.Case157EltField00(self._io, self, self._root)
            self.case__157_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case1910(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__191 = self._io.read_u4be()
            if not self.len_case__191 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__191, self._io, u"/types/case__191_0/seq/0")
            self._raw_case__191 = self._io.read_bytes(self.len_case__191)
            _io__raw_case__191 = KaitaiStream(BytesIO(self._raw_case__191))
            self.case__191 = Id015PtlimaptOperationProtocolData.Case191(_io__raw_case__191, self, self._root)


    class TransferTicket(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.ticket_contents = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.ticket_ty = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.ticket_ticketer = Id015PtlimaptOperationProtocolData.Id015PtlimaptContractId(self._io, self, self._root)
            self.ticket_amount = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.destination = Id015PtlimaptOperationProtocolData.Id015PtlimaptContractId(self._io, self, self._root)
            self.entrypoint = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Case208Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__208_field1 = self._io.read_bytes_full()


    class Case129EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__129_elt_field0 = self._io.read_bytes_full()


    class Case218(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__218_field0 = self._io.read_s4be()
            self.case__218_field1 = Id015PtlimaptOperationProtocolData.Case218Field10(self._io, self, self._root)
            self.case__218_field2 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Payload(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_entries = []
            i = 0
            while not self._io.is_eof():
                self.payload_entries.append(Id015PtlimaptOperationProtocolData.PayloadEntries(self._io, self, self._root))
                i += 1



    class Case13Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__13_field1_elt_field0 = self._io.read_u1()
            self.case__13_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case140EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__140_elt_field0 = self._io.read_bytes_full()


    class Case48Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__48_field1_elt_field0 = self._io.read_u1()
            self.case__48_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case60Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__60_field1_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__60_field1_entries.append(Id015PtlimaptOperationProtocolData.Case60Field1Entries(self._io, self, self._root))
                i += 1



    class Case145Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__145_elt_field0 = Id015PtlimaptOperationProtocolData.Case145EltField00(self._io, self, self._root)
            self.case__145_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case155EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__155_elt_field0 = self._io.read_u1()
            if not self.len_case__155_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__155_elt_field0, self._io, u"/types/case__155_elt_field0_0/seq/0")
            self._raw_case__155_elt_field0 = self._io.read_bytes(self.len_case__155_elt_field0)
            _io__raw_case__155_elt_field0 = KaitaiStream(BytesIO(self._raw_case__155_elt_field0))
            self.case__155_elt_field0 = Id015PtlimaptOperationProtocolData.Case155EltField0(_io__raw_case__155_elt_field0, self, self._root)


    class CircuitsInfo(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.circuits_info_entries = []
            i = 0
            while not self._io.is_eof():
                self.circuits_info_entries.append(Id015PtlimaptOperationProtocolData.CircuitsInfoEntries(self._io, self, self._root))
                i += 1



    class Case57(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__57_field0 = self._io.read_u2be()
            self.case__57_field1 = []
            for i in range(14):
                self.case__57_field1.append(Id015PtlimaptOperationProtocolData.Case57Field1Entries(self._io, self, self._root))



    class Op12(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_2/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id015PtlimaptOperationProtocolData.Op11(_io__raw_op1, self, self._root)


    class Case62Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__62_field1 = self._io.read_u4be()
            if not self.len_case__62_field1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__62_field1, self._io, u"/types/case__62_field1_0/seq/0")
            self._raw_case__62_field1 = self._io.read_bytes(self.len_case__62_field1)
            _io__raw_case__62_field1 = KaitaiStream(BytesIO(self._raw_case__62_field1))
            self.case__62_field1 = Id015PtlimaptOperationProtocolData.Case62Field1(_io__raw_case__62_field1, self, self._root)


    class Case40(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__40_field0 = self._io.read_u1()
            self.case__40_field1 = []
            for i in range(10):
                self.case__40_field1.append(Id015PtlimaptOperationProtocolData.Case40Field1Entries(self._io, self, self._root))



    class ScRollupExecuteOutboxMessage(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.cemented_commitment = self._io.read_bytes(32)
            self.output_proof = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Case132EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__132_elt_field0 = self._io.read_u1()
            if not self.len_case__132_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__132_elt_field0, self._io, u"/types/case__132_elt_field0_0/seq/0")
            self._raw_case__132_elt_field0 = self._io.read_bytes(self.len_case__132_elt_field0)
            _io__raw_case__132_elt_field0 = KaitaiStream(BytesIO(self._raw_case__132_elt_field0))
            self.case__132_elt_field0 = Id015PtlimaptOperationProtocolData.Case132EltField0(_io__raw_case__132_elt_field0, self, self._root)


    class Case9Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__9_field1_elt_field0 = self._io.read_u1()
            self.case__9_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Id015PtlimaptEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__entrypoint_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Id015PtlimaptEntrypointTag, self._io.read_u1())
            if self.id_015__ptlimapt__entrypoint_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptEntrypointTag.named:
                self.named = Id015PtlimaptOperationProtocolData.Named0(self._io, self, self._root)



    class TxRollupRemoveCommitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptTxRollupId(self._io, self, self._root)


    class Case0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field0 = self._io.read_s2be()
            self.case__0_field1 = self._io.read_bytes(32)
            self.case__0_field2 = self._io.read_bytes(32)
            self.case__0_field3 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case210Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__210_field1 = self._io.read_bytes_full()


    class Case164Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__164_elt_field0 = Id015PtlimaptOperationProtocolData.Case164EltField00(self._io, self, self._root)
            self.case__164_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case165EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__165_elt_field0 = self._io.read_bytes_full()


    class Case191EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__191_elt_field0 = self._io.read_u1()
            if not self.len_case__191_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__191_elt_field0, self._io, u"/types/case__191_elt_field0_0/seq/0")
            self._raw_case__191_elt_field0 = self._io.read_bytes(self.len_case__191_elt_field0)
            _io__raw_case__191_elt_field0 = KaitaiStream(BytesIO(self._raw_case__191_elt_field0))
            self.case__191_elt_field0 = Id015PtlimaptOperationProtocolData.Case191EltField0(_io__raw_case__191_elt_field0, self, self._root)


    class Case51(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__51_field0 = self._io.read_s8be()
            self.case__51_field1 = []
            for i in range(12):
                self.case__51_field1.append(Id015PtlimaptOperationProtocolData.Case51Field1Entries(self._io, self, self._root))



    class Case178EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__178_elt_field0 = self._io.read_u1()
            if not self.len_case__178_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__178_elt_field0, self._io, u"/types/case__178_elt_field0_0/seq/0")
            self._raw_case__178_elt_field0 = self._io.read_bytes(self.len_case__178_elt_field0)
            _io__raw_case__178_elt_field0 = KaitaiStream(BytesIO(self._raw_case__178_elt_field0))
            self.case__178_elt_field0 = Id015PtlimaptOperationProtocolData.Case178EltField0(_io__raw_case__178_elt_field0, self, self._root)


    class Case146EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__146_elt_field0 = self._io.read_bytes_full()


    class Case22Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__22_field1_elt_field0 = self._io.read_u1()
            self.case__22_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case21(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__21_field0 = self._io.read_u2be()
            self.case__21_field1 = []
            for i in range(5):
                self.case__21_field1.append(Id015PtlimaptOperationProtocolData.Case21Field1Entries(self._io, self, self._root))



    class Case46(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__46_field0 = self._io.read_s4be()
            self.case__46_field1 = []
            for i in range(11):
                self.case__46_field1.append(Id015PtlimaptOperationProtocolData.Case46Field1Entries(self._io, self, self._root))



    class Case159Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__159_elt_field0 = Id015PtlimaptOperationProtocolData.Case159EltField00(self._io, self, self._root)
            self.case__159_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case134Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__134_elt_field0 = Id015PtlimaptOperationProtocolData.Case134EltField00(self._io, self, self._root)
            self.case__134_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case155Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__155_elt_field0 = Id015PtlimaptOperationProtocolData.Case155EltField00(self._io, self, self._root)
            self.case__155_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case148EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__148_elt_field0 = self._io.read_u1()
            if not self.len_case__148_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__148_elt_field0, self._io, u"/types/case__148_elt_field0_0/seq/0")
            self._raw_case__148_elt_field0 = self._io.read_bytes(self.len_case__148_elt_field0)
            _io__raw_case__148_elt_field0 = KaitaiStream(BytesIO(self._raw_case__148_elt_field0))
            self.case__148_elt_field0 = Id015PtlimaptOperationProtocolData.Case148EltField0(_io__raw_case__148_elt_field0, self, self._root)


    class Case54Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__54_field1_elt_field0 = self._io.read_u1()
            self.case__54_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptOperationProtocolData.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Case172Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__172_elt_field0 = Id015PtlimaptOperationProtocolData.Case172EltField00(self._io, self, self._root)
            self.case__172_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case135Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__135_elt_field0 = Id015PtlimaptOperationProtocolData.Case135EltField00(self._io, self, self._root)
            self.case__135_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Reveal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.public_key = Id015PtlimaptOperationProtocolData.PublicKey(self._io, self, self._root)


    class Case148EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__148_elt_field0 = self._io.read_bytes_full()


    class Case168EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__168_elt_field0 = self._io.read_bytes_full()


    class Case210(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__210_field0 = self._io.read_s4be()
            self.case__210_field1 = Id015PtlimaptOperationProtocolData.Case210Field10(self._io, self, self._root)
            self.case__210_field2 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case34(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__34_field0 = self._io.read_s4be()
            self.case__34_field1 = []
            for i in range(8):
                self.case__34_field1.append(Id015PtlimaptOperationProtocolData.Case34Field1Entries(self._io, self, self._root))



    class RevealProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.reveal_proof_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.RevealProofTag, self._io.read_u1())
            if self.reveal_proof_tag == Id015PtlimaptOperationProtocolData.RevealProofTag.raw__data__proof:
                self.raw__data__proof = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)



    class Id015PtlimaptInlinedPreendorsementContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__preendorsement__contents_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Id015PtlimaptInlinedPreendorsementContentsTag, self._io.read_u1())
            if self.id_015__ptlimapt__inlined__preendorsement__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptInlinedPreendorsementContentsTag.preendorsement:
                self.preendorsement = Id015PtlimaptOperationProtocolData.Preendorsement(self._io, self, self._root)



    class Case31Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__31_field1_elt_field0 = self._io.read_u1()
            self.case__31_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case23(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__23_field0 = self._io.read_s8be()
            self.case__23_field1 = []
            for i in range(5):
                self.case__23_field1.append(Id015PtlimaptOperationProtocolData.Case23Field1Entries(self._io, self, self._root))



    class Case44(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__44_field0 = self._io.read_u1()
            self.case__44_field1 = []
            for i in range(11):
                self.case__44_field1.append(Id015PtlimaptOperationProtocolData.Case44Field1Entries(self._io, self, self._root))



    class Case6Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__6_field1_elt_field0 = self._io.read_u1()
            self.case__6_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class CircuitsInfo0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_circuits_info = self._io.read_u4be()
            if not self.len_circuits_info <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_circuits_info, self._io, u"/types/circuits_info_0/seq/0")
            self._raw_circuits_info = self._io.read_bytes(self.len_circuits_info)
            _io__raw_circuits_info = KaitaiStream(BytesIO(self._raw_circuits_info))
            self.circuits_info = Id015PtlimaptOperationProtocolData.CircuitsInfo(_io__raw_circuits_info, self, self._root)


    class Case188Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__188_elt_field0 = Id015PtlimaptOperationProtocolData.Case188EltField00(self._io, self, self._root)
            self.case__188_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Price(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id = self._io.read_bytes(32)
            self.amount = Id015PtlimaptOperationProtocolData.Z(self._io, self, self._root)


    class Case33(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__33_field0 = self._io.read_u2be()
            self.case__33_field1 = []
            for i in range(8):
                self.case__33_field1.append(Id015PtlimaptOperationProtocolData.Case33Field1Entries(self._io, self, self._root))



    class Case66(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__66_field0 = self._io.read_s4be()
            self.case__66_field1 = []
            for i in range(32):
                self.case__66_field1.append(Id015PtlimaptOperationProtocolData.Case66Field1Entries(self._io, self, self._root))



    class Case150Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__150_elt_field0 = Id015PtlimaptOperationProtocolData.Case150EltField00(self._io, self, self._root)
            self.case__150_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case185EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__185_elt_field0 = self._io.read_u1()
            if not self.len_case__185_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__185_elt_field0, self._io, u"/types/case__185_elt_field0_0/seq/0")
            self._raw_case__185_elt_field0 = self._io.read_bytes(self.len_case__185_elt_field0)
            _io__raw_case__185_elt_field0 = KaitaiStream(BytesIO(self._raw_case__185_elt_field0))
            self.case__185_elt_field0 = Id015PtlimaptOperationProtocolData.Case185EltField0(_io__raw_case__185_elt_field0, self, self._root)


    class Id015PtlimaptBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__block_header__alpha__unsigned_contents = Id015PtlimaptOperationProtocolData.Id015PtlimaptBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes(64)


    class Case142Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__142_elt_field0 = Id015PtlimaptOperationProtocolData.Case142EltField00(self._io, self, self._root)
            self.case__142_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case22(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__22_field0 = self._io.read_s4be()
            self.case__22_field1 = []
            for i in range(5):
                self.case__22_field1.append(Id015PtlimaptOperationProtocolData.Case22Field1Entries(self._io, self, self._root))



    class Case175EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__175_elt_field0 = self._io.read_u1()
            if not self.len_case__175_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__175_elt_field0, self._io, u"/types/case__175_elt_field0_0/seq/0")
            self._raw_case__175_elt_field0 = self._io.read_bytes(self.len_case__175_elt_field0)
            _io__raw_case__175_elt_field0 = KaitaiStream(BytesIO(self._raw_case__175_elt_field0))
            self.case__175_elt_field0 = Id015PtlimaptOperationProtocolData.Case175EltField0(_io__raw_case__175_elt_field0, self, self._root)


    class MessageEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_elt = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Case55(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__55_field0 = self._io.read_s8be()
            self.case__55_field1 = []
            for i in range(13):
                self.case__55_field1.append(Id015PtlimaptOperationProtocolData.Case55Field1Entries(self._io, self, self._root))



    class Case40Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__40_field1_elt_field0 = self._io.read_u1()
            self.case__40_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class OpEltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_code = Id015PtlimaptOperationProtocolData.Int31(self._io, self, self._root)
            self.price = Id015PtlimaptOperationProtocolData.Price(self._io, self, self._root)
            self.l1_dst = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.rollup_id = self._io.read_bytes(20)
            self.payload = Id015PtlimaptOperationProtocolData.Payload0(self._io, self, self._root)


    class Case191Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__191_elt_field0 = Id015PtlimaptOperationProtocolData.Case191EltField00(self._io, self, self._root)
            self.case__191_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case154EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__154_elt_field0 = self._io.read_bytes_full()


    class Case216Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__216_field1 = self._io.read_bytes_full()


    class Case144Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__144_elt_field0 = Id015PtlimaptOperationProtocolData.Case144EltField00(self._io, self, self._root)
            self.case__144_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case187Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__187_elt_field0 = Id015PtlimaptOperationProtocolData.Case187EltField00(self._io, self, self._root)
            self.case__187_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case132EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__132_elt_field0 = self._io.read_bytes_full()


    class Case217Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__217_field1 = self._io.read_bytes_full()


    class Case61Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__61_field1 = self._io.read_u4be()
            if not self.len_case__61_field1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__61_field1, self._io, u"/types/case__61_field1_0/seq/0")
            self._raw_case__61_field1 = self._io.read_bytes(self.len_case__61_field1)
            _io__raw_case__61_field1 = KaitaiStream(BytesIO(self._raw_case__61_field1))
            self.case__61_field1 = Id015PtlimaptOperationProtocolData.Case61Field1(_io__raw_case__61_field1, self, self._root)


    class Case52(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__52_field0 = self._io.read_u1()
            self.case__52_field1 = []
            for i in range(13):
                self.case__52_field1.append(Id015PtlimaptOperationProtocolData.Case52Field1Entries(self._io, self, self._root))



    class Case5(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__5_field0 = self._io.read_u2be()
            self.case__5_field1 = []
            for i in range(1):
                self.case__5_field1.append(Id015PtlimaptOperationProtocolData.Case5Field1Entries(self._io, self, self._root))



    class Case7Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__7_field1_elt_field0 = self._io.read_u1()
            self.case__7_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case162Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__162_elt_field0 = Id015PtlimaptOperationProtocolData.Case162EltField00(self._io, self, self._root)
            self.case__162_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case193(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__193 = self._io.read_bytes_full()


    class Case38(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__38_field0 = self._io.read_s4be()
            self.case__38_field1 = []
            for i in range(9):
                self.case__38_field1.append(Id015PtlimaptOperationProtocolData.Case38Field1Entries(self._io, self, self._root))



    class Id015PtlimaptOperationAlphaContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__operation__alpha__contents_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag, self._io.read_u1())
            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.endorsement:
                self.endorsement = Id015PtlimaptOperationProtocolData.Endorsement(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.preendorsement:
                self.preendorsement = Id015PtlimaptOperationProtocolData.Preendorsement(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.dal_slot_availability:
                self.dal_slot_availability = Id015PtlimaptOperationProtocolData.DalSlotAvailability(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.seed_nonce_revelation:
                self.seed_nonce_revelation = Id015PtlimaptOperationProtocolData.SeedNonceRevelation(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.vdf_revelation:
                self.vdf_revelation = Id015PtlimaptOperationProtocolData.Solution(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.double_endorsement_evidence:
                self.double_endorsement_evidence = Id015PtlimaptOperationProtocolData.DoubleEndorsementEvidence(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.double_preendorsement_evidence:
                self.double_preendorsement_evidence = Id015PtlimaptOperationProtocolData.DoublePreendorsementEvidence(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.double_baking_evidence:
                self.double_baking_evidence = Id015PtlimaptOperationProtocolData.DoubleBakingEvidence(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.activate_account:
                self.activate_account = Id015PtlimaptOperationProtocolData.ActivateAccount(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.proposals:
                self.proposals = Id015PtlimaptOperationProtocolData.Proposals1(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.ballot:
                self.ballot = Id015PtlimaptOperationProtocolData.Ballot(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.reveal:
                self.reveal = Id015PtlimaptOperationProtocolData.Reveal(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.transaction:
                self.transaction = Id015PtlimaptOperationProtocolData.Transaction(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.origination:
                self.origination = Id015PtlimaptOperationProtocolData.Origination(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.delegation:
                self.delegation = Id015PtlimaptOperationProtocolData.Delegation(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.set_deposits_limit:
                self.set_deposits_limit = Id015PtlimaptOperationProtocolData.SetDepositsLimit(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.increase_paid_storage:
                self.increase_paid_storage = Id015PtlimaptOperationProtocolData.IncreasePaidStorage(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.update_consensus_key:
                self.update_consensus_key = Id015PtlimaptOperationProtocolData.UpdateConsensusKey(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.drain_delegate:
                self.drain_delegate = Id015PtlimaptOperationProtocolData.DrainDelegate(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.failing_noop:
                self.failing_noop = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.register_global_constant:
                self.register_global_constant = Id015PtlimaptOperationProtocolData.RegisterGlobalConstant(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_origination:
                self.tx_rollup_origination = Id015PtlimaptOperationProtocolData.TxRollupOrigination(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_submit_batch:
                self.tx_rollup_submit_batch = Id015PtlimaptOperationProtocolData.TxRollupSubmitBatch(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_commit:
                self.tx_rollup_commit = Id015PtlimaptOperationProtocolData.TxRollupCommit(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_return_bond:
                self.tx_rollup_return_bond = Id015PtlimaptOperationProtocolData.TxRollupReturnBond(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_finalize_commitment:
                self.tx_rollup_finalize_commitment = Id015PtlimaptOperationProtocolData.TxRollupFinalizeCommitment(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_remove_commitment:
                self.tx_rollup_remove_commitment = Id015PtlimaptOperationProtocolData.TxRollupRemoveCommitment(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_rejection:
                self.tx_rollup_rejection = Id015PtlimaptOperationProtocolData.TxRollupRejection(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_dispatch_tickets:
                self.tx_rollup_dispatch_tickets = Id015PtlimaptOperationProtocolData.TxRollupDispatchTickets(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.transfer_ticket:
                self.transfer_ticket = Id015PtlimaptOperationProtocolData.TransferTicket(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.dal_publish_slot_header:
                self.dal_publish_slot_header = Id015PtlimaptOperationProtocolData.DalPublishSlotHeader(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_originate:
                self.sc_rollup_originate = Id015PtlimaptOperationProtocolData.ScRollupOriginate(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_add_messages:
                self.sc_rollup_add_messages = Id015PtlimaptOperationProtocolData.ScRollupAddMessages(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_cement:
                self.sc_rollup_cement = Id015PtlimaptOperationProtocolData.ScRollupCement(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_publish:
                self.sc_rollup_publish = Id015PtlimaptOperationProtocolData.ScRollupPublish(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_refute:
                self.sc_rollup_refute = Id015PtlimaptOperationProtocolData.ScRollupRefute(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_timeout:
                self.sc_rollup_timeout = Id015PtlimaptOperationProtocolData.ScRollupTimeout(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_execute_outbox_message:
                self.sc_rollup_execute_outbox_message = Id015PtlimaptOperationProtocolData.ScRollupExecuteOutboxMessage(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_recover_bond:
                self.sc_rollup_recover_bond = Id015PtlimaptOperationProtocolData.ScRollupRecoverBond(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_dal_slot_subscribe:
                self.sc_rollup_dal_slot_subscribe = Id015PtlimaptOperationProtocolData.ScRollupDalSlotSubscribe(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.zk_rollup_origination:
                self.zk_rollup_origination = Id015PtlimaptOperationProtocolData.ZkRollupOrigination(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContentsTag.zk_rollup_publish:
                self.zk_rollup_publish = Id015PtlimaptOperationProtocolData.ZkRollupPublish(self._io, self, self._root)



    class Case58Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__58_field1_elt_field0 = self._io.read_u1()
            self.case__58_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Refutation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.choice = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.step = Id015PtlimaptOperationProtocolData.Step(self._io, self, self._root)


    class ScRollupDalSlotSubscribe(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.slot_index = self._io.read_u1()


    class Case166Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__166_elt_field0 = Id015PtlimaptOperationProtocolData.Case166EltField00(self._io, self, self._root)
            self.case__166_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case188EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__188_elt_field0 = self._io.read_u1()
            if not self.len_case__188_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__188_elt_field0, self._io, u"/types/case__188_elt_field0_0/seq/0")
            self._raw_case__188_elt_field0 = self._io.read_bytes(self.len_case__188_elt_field0)
            _io__raw_case__188_elt_field0 = KaitaiStream(BytesIO(self._raw_case__188_elt_field0))
            self.case__188_elt_field0 = Id015PtlimaptOperationProtocolData.Case188EltField0(_io__raw_case__188_elt_field0, self, self._root)


    class ScRollupOriginate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.pvm_kind = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.PvmKind, self._io.read_u1())
            self.boot_sector = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.origination_proof = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.parameters_ty = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Case62(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__62_field0 = self._io.read_s4be()
            self.case__62_field1 = Id015PtlimaptOperationProtocolData.Case62Field10(self._io, self, self._root)


    class Case40(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__4_field0 = self._io.read_u1()
            self.case__4_field1 = []
            for i in range(1):
                self.case__4_field1.append(Id015PtlimaptOperationProtocolData.Case4Field1Entries0(self._io, self, self._root))



    class SeedNonceRevelation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.nonce = self._io.read_bytes(32)


    class Case27(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__27_field0 = self._io.read_s8be()
            self.case__27_field1 = []
            for i in range(6):
                self.case__27_field1.append(Id015PtlimaptOperationProtocolData.Case27Field1Entries(self._io, self, self._root))



    class Case136EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__136_elt_field0 = self._io.read_u1()
            if not self.len_case__136_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__136_elt_field0, self._io, u"/types/case__136_elt_field0_0/seq/0")
            self._raw_case__136_elt_field0 = self._io.read_bytes(self.len_case__136_elt_field0)
            _io__raw_case__136_elt_field0 = KaitaiStream(BytesIO(self._raw_case__136_elt_field0))
            self.case__136_elt_field0 = Id015PtlimaptOperationProtocolData.Case136EltField0(_io__raw_case__136_elt_field0, self, self._root)


    class Case208Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__208_field1 = self._io.read_u1()
            if not self.len_case__208_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__208_field1, self._io, u"/types/case__208_field1_0/seq/0")
            self._raw_case__208_field1 = self._io.read_bytes(self.len_case__208_field1)
            _io__raw_case__208_field1 = KaitaiStream(BytesIO(self._raw_case__208_field1))
            self.case__208_field1 = Id015PtlimaptOperationProtocolData.Case208Field1(_io__raw_case__208_field1, self, self._root)


    class Preendorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class Case142EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__142_elt_field0 = self._io.read_u1()
            if not self.len_case__142_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__142_elt_field0, self._io, u"/types/case__142_elt_field0_0/seq/0")
            self._raw_case__142_elt_field0 = self._io.read_bytes(self.len_case__142_elt_field0)
            _io__raw_case__142_elt_field0 = KaitaiStream(BytesIO(self._raw_case__142_elt_field0))
            self.case__142_elt_field0 = Id015PtlimaptOperationProtocolData.Case142EltField0(_io__raw_case__142_elt_field0, self, self._root)


    class Case179EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__179_elt_field0 = self._io.read_u1()
            if not self.len_case__179_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__179_elt_field0, self._io, u"/types/case__179_elt_field0_0/seq/0")
            self._raw_case__179_elt_field0 = self._io.read_bytes(self.len_case__179_elt_field0)
            _io__raw_case__179_elt_field0 = KaitaiStream(BytesIO(self._raw_case__179_elt_field0))
            self.case__179_elt_field0 = Id015PtlimaptOperationProtocolData.Case179EltField0(_io__raw_case__179_elt_field0, self, self._root)


    class Case143EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__143_elt_field0 = self._io.read_bytes_full()


    class Case135EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__135_elt_field0 = self._io.read_u1()
            if not self.len_case__135_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__135_elt_field0, self._io, u"/types/case__135_elt_field0_0/seq/0")
            self._raw_case__135_elt_field0 = self._io.read_bytes(self.len_case__135_elt_field0)
            _io__raw_case__135_elt_field0 = KaitaiStream(BytesIO(self._raw_case__135_elt_field0))
            self.case__135_elt_field0 = Id015PtlimaptOperationProtocolData.Case135EltField0(_io__raw_case__135_elt_field0, self, self._root)


    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptOperationProtocolData.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.annots = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Case49(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__49_field0 = self._io.read_u2be()
            self.case__49_field1 = []
            for i in range(12):
                self.case__49_field1.append(Id015PtlimaptOperationProtocolData.Case49Field1Entries(self._io, self, self._root))



    class Case11(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__11_field0 = self._io.read_s8be()
            self.case__11_field1 = []
            for i in range(2):
                self.case__11_field1.append(Id015PtlimaptOperationProtocolData.Case11Field1Entries(self._io, self, self._root))



    class Case153EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__153_elt_field0 = self._io.read_u1()
            if not self.len_case__153_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__153_elt_field0, self._io, u"/types/case__153_elt_field0_0/seq/0")
            self._raw_case__153_elt_field0 = self._io.read_bytes(self.len_case__153_elt_field0)
            _io__raw_case__153_elt_field0 = KaitaiStream(BytesIO(self._raw_case__153_elt_field0))
            self.case__153_elt_field0 = Id015PtlimaptOperationProtocolData.Case153EltField0(_io__raw_case__153_elt_field0, self, self._root)


    class Case178Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__178_elt_field0 = Id015PtlimaptOperationProtocolData.Case178EltField00(self._io, self, self._root)
            self.case__178_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case184EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__184_elt_field0 = self._io.read_bytes_full()


    class Case16(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__16_field0 = self._io.read_u1()
            self.case__16_field1 = []
            for i in range(4):
                self.case__16_field1.append(Id015PtlimaptOperationProtocolData.Case16Field1Entries(self._io, self, self._root))



    class Case162EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__162_elt_field0 = self._io.read_u1()
            if not self.len_case__162_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__162_elt_field0, self._io, u"/types/case__162_elt_field0_0/seq/0")
            self._raw_case__162_elt_field0 = self._io.read_bytes(self.len_case__162_elt_field0)
            _io__raw_case__162_elt_field0 = KaitaiStream(BytesIO(self._raw_case__162_elt_field0))
            self.case__162_elt_field0 = Id015PtlimaptOperationProtocolData.Case162EltField0(_io__raw_case__162_elt_field0, self, self._root)


    class Case5Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__5_field1_elt_field0 = self._io.read_u1()
            self.case__5_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case190EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__190_elt_field0 = self._io.read_bytes_full()


    class Case163EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__163_elt_field0 = self._io.read_bytes_full()


    class Case187EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__187_elt_field0 = self._io.read_u1()
            if not self.len_case__187_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__187_elt_field0, self._io, u"/types/case__187_elt_field0_0/seq/0")
            self._raw_case__187_elt_field0 = self._io.read_bytes(self.len_case__187_elt_field0)
            _io__raw_case__187_elt_field0 = KaitaiStream(BytesIO(self._raw_case__187_elt_field0))
            self.case__187_elt_field0 = Id015PtlimaptOperationProtocolData.Case187EltField0(_io__raw_case__187_elt_field0, self, self._root)


    class Case167EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__167_elt_field0 = self._io.read_bytes_full()


    class Case161Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__161_elt_field0 = Id015PtlimaptOperationProtocolData.Case161EltField00(self._io, self, self._root)
            self.case__161_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case39Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__39_field1_elt_field0 = self._io.read_u1()
            self.case__39_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case219(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__219_field0 = self._io.read_s8be()
            self.case__219_field1 = Id015PtlimaptOperationProtocolData.Case219Field10(self._io, self, self._root)
            self.case__219_field2 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case50Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__50_field1_elt_field0 = self._io.read_u1()
            self.case__50_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case157EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__157_elt_field0 = self._io.read_bytes_full()


    class Case11Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__11_field1_elt_field0 = self._io.read_u1()
            self.case__11_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Id015PtlimaptContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__contract_id_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Id015PtlimaptContractIdTag, self._io.read_u1())
            if self.id_015__ptlimapt__contract_id_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptContractIdTag.implicit:
                self.implicit = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)

            if self.id_015__ptlimapt__contract_id_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptContractIdTag.originated:
                self.originated = Id015PtlimaptOperationProtocolData.Originated(self._io, self, self._root)



    class Case182EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__182_elt_field0 = self._io.read_u1()
            if not self.len_case__182_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__182_elt_field0, self._io, u"/types/case__182_elt_field0_0/seq/0")
            self._raw_case__182_elt_field0 = self._io.read_bytes(self.len_case__182_elt_field0)
            _io__raw_case__182_elt_field0 = KaitaiStream(BytesIO(self._raw_case__182_elt_field0))
            self.case__182_elt_field0 = Id015PtlimaptOperationProtocolData.Case182EltField0(_io__raw_case__182_elt_field0, self, self._root)


    class Case137EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__137_elt_field0 = self._io.read_bytes_full()


    class Case219Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__219_field1 = self._io.read_u1()
            if not self.len_case__219_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__219_field1, self._io, u"/types/case__219_field1_0/seq/0")
            self._raw_case__219_field1 = self._io.read_bytes(self.len_case__219_field1)
            _io__raw_case__219_field1 = KaitaiStream(BytesIO(self._raw_case__219_field1))
            self.case__219_field1 = Id015PtlimaptOperationProtocolData.Case219Field1(_io__raw_case__219_field1, self, self._root)


    class TxRollupCommit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptTxRollupId(self._io, self, self._root)
            self.commitment = Id015PtlimaptOperationProtocolData.Commitment(self._io, self, self._root)


    class DoubleBakingEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bh1 = Id015PtlimaptOperationProtocolData.Bh10(self._io, self, self._root)
            self.bh2 = Id015PtlimaptOperationProtocolData.Bh20(self._io, self, self._root)


    class Case1911(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__191 = self._io.read_u4be()
            if not self.len_case__191 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__191, self._io, u"/types/case__191_1/seq/0")
            self._raw_case__191 = self._io.read_bytes(self.len_case__191)
            _io__raw_case__191 = KaitaiStream(BytesIO(self._raw_case__191))
            self.case__191 = Id015PtlimaptOperationProtocolData.Case191(_io__raw_case__191, self, self._root)


    class Case45(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__45_field0 = self._io.read_u2be()
            self.case__45_field1 = []
            for i in range(11):
                self.case__45_field1.append(Id015PtlimaptOperationProtocolData.Case45Field1Entries(self._io, self, self._root))



    class Case217Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__217_field1 = self._io.read_u1()
            if not self.len_case__217_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__217_field1, self._io, u"/types/case__217_field1_0/seq/0")
            self._raw_case__217_field1 = self._io.read_bytes(self.len_case__217_field1)
            _io__raw_case__217_field1 = KaitaiStream(BytesIO(self._raw_case__217_field1))
            self.case__217_field1 = Id015PtlimaptOperationProtocolData.Case217Field1(_io__raw_case__217_field1, self, self._root)


    class Case129EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__129_elt_field0 = self._io.read_u1()
            if not self.len_case__129_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__129_elt_field0, self._io, u"/types/case__129_elt_field0_0/seq/0")
            self._raw_case__129_elt_field0 = self._io.read_bytes(self.len_case__129_elt_field0)
            _io__raw_case__129_elt_field0 = KaitaiStream(BytesIO(self._raw_case__129_elt_field0))
            self.case__129_elt_field0 = Id015PtlimaptOperationProtocolData.Case129EltField0(_io__raw_case__129_elt_field0, self, self._root)


    class Case152EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__152_elt_field0 = self._io.read_u1()
            if not self.len_case__152_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__152_elt_field0, self._io, u"/types/case__152_elt_field0_0/seq/0")
            self._raw_case__152_elt_field0 = self._io.read_bytes(self.len_case__152_elt_field0)
            _io__raw_case__152_elt_field0 = KaitaiStream(BytesIO(self._raw_case__152_elt_field0))
            self.case__152_elt_field0 = Id015PtlimaptOperationProtocolData.Case152EltField0(_io__raw_case__152_elt_field0, self, self._root)


    class Case27Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__27_field1_elt_field0 = self._io.read_u1()
            self.case__27_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case160Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__160_elt_field0 = Id015PtlimaptOperationProtocolData.Case160EltField00(self._io, self, self._root)
            self.case__160_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id015PtlimaptOperationProtocolData.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id015PtlimaptOperationProtocolData.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id015PtlimaptOperationProtocolData.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Case165Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__165_elt_field0 = Id015PtlimaptOperationProtocolData.Case165EltField00(self._io, self, self._root)
            self.case__165_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case12(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__12_field0 = self._io.read_u1()
            self.case__12_field1 = []
            for i in range(3):
                self.case__12_field1.append(Id015PtlimaptOperationProtocolData.Case12Field1Entries(self._io, self, self._root))



    class TxRollupDispatchTickets(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.tx_rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptTxRollupId(self._io, self, self._root)
            self.level = self._io.read_s4be()
            self.context_hash = self._io.read_bytes(32)
            self.message_index = Id015PtlimaptOperationProtocolData.Int31(self._io, self, self._root)
            self.message_result_path = Id015PtlimaptOperationProtocolData.MessageResultPath0(self._io, self, self._root)
            self.tickets_info = Id015PtlimaptOperationProtocolData.TicketsInfo0(self._io, self, self._root)


    class Case210Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__210_field1 = self._io.read_u1()
            if not self.len_case__210_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__210_field1, self._io, u"/types/case__210_field1_0/seq/0")
            self._raw_case__210_field1 = self._io.read_bytes(self.len_case__210_field1)
            _io__raw_case__210_field1 = KaitaiStream(BytesIO(self._raw_case__210_field1))
            self.case__210_field1 = Id015PtlimaptOperationProtocolData.Case210Field1(_io__raw_case__210_field1, self, self._root)


    class Bh2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__block_header__alpha__full_header = Id015PtlimaptOperationProtocolData.Id015PtlimaptBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Case139EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__139_elt_field0 = self._io.read_u1()
            if not self.len_case__139_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__139_elt_field0, self._io, u"/types/case__139_elt_field0_0/seq/0")
            self._raw_case__139_elt_field0 = self._io.read_bytes(self.len_case__139_elt_field0)
            _io__raw_case__139_elt_field0 = KaitaiStream(BytesIO(self._raw_case__139_elt_field0))
            self.case__139_elt_field0 = Id015PtlimaptOperationProtocolData.Case139EltField0(_io__raw_case__139_elt_field0, self, self._root)


    class PreviousMessageResultPathEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_list_hash = self._io.read_bytes(32)


    class Case36Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__36_field1_elt_field0 = self._io.read_u1()
            self.case__36_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case141EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__141_elt_field0 = self._io.read_u1()
            if not self.len_case__141_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__141_elt_field0, self._io, u"/types/case__141_elt_field0_0/seq/0")
            self._raw_case__141_elt_field0 = self._io.read_bytes(self.len_case__141_elt_field0)
            _io__raw_case__141_elt_field0 = KaitaiStream(BytesIO(self._raw_case__141_elt_field0))
            self.case__141_elt_field0 = Id015PtlimaptOperationProtocolData.Case141EltField0(_io__raw_case__141_elt_field0, self, self._root)


    class Case157EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__157_elt_field0 = self._io.read_u1()
            if not self.len_case__157_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__157_elt_field0, self._io, u"/types/case__157_elt_field0_0/seq/0")
            self._raw_case__157_elt_field0 = self._io.read_bytes(self.len_case__157_elt_field0)
            _io__raw_case__157_elt_field0 = KaitaiStream(BytesIO(self._raw_case__157_elt_field0))
            self.case__157_elt_field0 = Id015PtlimaptOperationProtocolData.Case157EltField0(_io__raw_case__157_elt_field0, self, self._root)


    class Case147EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__147_elt_field0 = self._io.read_u1()
            if not self.len_case__147_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__147_elt_field0, self._io, u"/types/case__147_elt_field0_0/seq/0")
            self._raw_case__147_elt_field0 = self._io.read_bytes(self.len_case__147_elt_field0)
            _io__raw_case__147_elt_field0 = KaitaiStream(BytesIO(self._raw_case__147_elt_field0))
            self.case__147_elt_field0 = Id015PtlimaptOperationProtocolData.Case147EltField0(_io__raw_case__147_elt_field0, self, self._root)


    class Id015PtlimaptMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__mutez = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)


    class TreeEncoding(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.tree_encoding_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.TreeEncodingTag, self._io.read_u1())
            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__4:
                self.case__4 = Id015PtlimaptOperationProtocolData.Case40(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__8:
                self.case__8 = Id015PtlimaptOperationProtocolData.Case8(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__12:
                self.case__12 = Id015PtlimaptOperationProtocolData.Case12(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__16:
                self.case__16 = Id015PtlimaptOperationProtocolData.Case16(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__20:
                self.case__20 = Id015PtlimaptOperationProtocolData.Case20(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__24:
                self.case__24 = Id015PtlimaptOperationProtocolData.Case24(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__28:
                self.case__28 = Id015PtlimaptOperationProtocolData.Case28(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__32:
                self.case__32 = Id015PtlimaptOperationProtocolData.Case32(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__36:
                self.case__36 = Id015PtlimaptOperationProtocolData.Case36(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__40:
                self.case__40 = Id015PtlimaptOperationProtocolData.Case40(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__44:
                self.case__44 = Id015PtlimaptOperationProtocolData.Case44(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__48:
                self.case__48 = Id015PtlimaptOperationProtocolData.Case48(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__52:
                self.case__52 = Id015PtlimaptOperationProtocolData.Case52(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__56:
                self.case__56 = Id015PtlimaptOperationProtocolData.Case56(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__60:
                self.case__60 = Id015PtlimaptOperationProtocolData.Case60(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__64:
                self.case__64 = Id015PtlimaptOperationProtocolData.Case64(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__5:
                self.case__5 = Id015PtlimaptOperationProtocolData.Case5(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__9:
                self.case__9 = Id015PtlimaptOperationProtocolData.Case9(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__13:
                self.case__13 = Id015PtlimaptOperationProtocolData.Case13(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__17:
                self.case__17 = Id015PtlimaptOperationProtocolData.Case17(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__21:
                self.case__21 = Id015PtlimaptOperationProtocolData.Case21(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__25:
                self.case__25 = Id015PtlimaptOperationProtocolData.Case25(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__29:
                self.case__29 = Id015PtlimaptOperationProtocolData.Case29(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__33:
                self.case__33 = Id015PtlimaptOperationProtocolData.Case33(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__37:
                self.case__37 = Id015PtlimaptOperationProtocolData.Case37(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__41:
                self.case__41 = Id015PtlimaptOperationProtocolData.Case41(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__45:
                self.case__45 = Id015PtlimaptOperationProtocolData.Case45(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__49:
                self.case__49 = Id015PtlimaptOperationProtocolData.Case49(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__53:
                self.case__53 = Id015PtlimaptOperationProtocolData.Case53(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__57:
                self.case__57 = Id015PtlimaptOperationProtocolData.Case57(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__61:
                self.case__61 = Id015PtlimaptOperationProtocolData.Case61(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__65:
                self.case__65 = Id015PtlimaptOperationProtocolData.Case65(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__6:
                self.case__6 = Id015PtlimaptOperationProtocolData.Case6(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__10:
                self.case__10 = Id015PtlimaptOperationProtocolData.Case10(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__14:
                self.case__14 = Id015PtlimaptOperationProtocolData.Case14(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__18:
                self.case__18 = Id015PtlimaptOperationProtocolData.Case18(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__22:
                self.case__22 = Id015PtlimaptOperationProtocolData.Case22(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__26:
                self.case__26 = Id015PtlimaptOperationProtocolData.Case26(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__30:
                self.case__30 = Id015PtlimaptOperationProtocolData.Case30(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__34:
                self.case__34 = Id015PtlimaptOperationProtocolData.Case34(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__38:
                self.case__38 = Id015PtlimaptOperationProtocolData.Case38(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__42:
                self.case__42 = Id015PtlimaptOperationProtocolData.Case42(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__46:
                self.case__46 = Id015PtlimaptOperationProtocolData.Case46(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__50:
                self.case__50 = Id015PtlimaptOperationProtocolData.Case50(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__54:
                self.case__54 = Id015PtlimaptOperationProtocolData.Case54(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__58:
                self.case__58 = Id015PtlimaptOperationProtocolData.Case58(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__62:
                self.case__62 = Id015PtlimaptOperationProtocolData.Case62(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__66:
                self.case__66 = Id015PtlimaptOperationProtocolData.Case66(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__3:
                self.case__3 = self._io.read_s8be()

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__7:
                self.case__7 = Id015PtlimaptOperationProtocolData.Case7(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__11:
                self.case__11 = Id015PtlimaptOperationProtocolData.Case11(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__15:
                self.case__15 = Id015PtlimaptOperationProtocolData.Case15(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__19:
                self.case__19 = Id015PtlimaptOperationProtocolData.Case19(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__23:
                self.case__23 = Id015PtlimaptOperationProtocolData.Case23(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__27:
                self.case__27 = Id015PtlimaptOperationProtocolData.Case27(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__31:
                self.case__31 = Id015PtlimaptOperationProtocolData.Case31(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__35:
                self.case__35 = Id015PtlimaptOperationProtocolData.Case35(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__39:
                self.case__39 = Id015PtlimaptOperationProtocolData.Case39(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__43:
                self.case__43 = Id015PtlimaptOperationProtocolData.Case43(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__47:
                self.case__47 = Id015PtlimaptOperationProtocolData.Case47(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__51:
                self.case__51 = Id015PtlimaptOperationProtocolData.Case51(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__55:
                self.case__55 = Id015PtlimaptOperationProtocolData.Case55(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__59:
                self.case__59 = Id015PtlimaptOperationProtocolData.Case59(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__63:
                self.case__63 = Id015PtlimaptOperationProtocolData.Case63(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__67:
                self.case__67 = Id015PtlimaptOperationProtocolData.Case67(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__129:
                self.case__129 = Id015PtlimaptOperationProtocolData.Case129Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__130:
                self.case__130 = Id015PtlimaptOperationProtocolData.Case130Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__131:
                self.case__131 = Id015PtlimaptOperationProtocolData.Case131Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__132:
                self.case__132 = Id015PtlimaptOperationProtocolData.Case132Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__133:
                self.case__133 = Id015PtlimaptOperationProtocolData.Case133Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__134:
                self.case__134 = Id015PtlimaptOperationProtocolData.Case134Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__135:
                self.case__135 = Id015PtlimaptOperationProtocolData.Case135Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__136:
                self.case__136 = Id015PtlimaptOperationProtocolData.Case136Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__137:
                self.case__137 = Id015PtlimaptOperationProtocolData.Case137Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__138:
                self.case__138 = Id015PtlimaptOperationProtocolData.Case138Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__139:
                self.case__139 = Id015PtlimaptOperationProtocolData.Case139Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__140:
                self.case__140 = Id015PtlimaptOperationProtocolData.Case140Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__141:
                self.case__141 = Id015PtlimaptOperationProtocolData.Case141Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__142:
                self.case__142 = Id015PtlimaptOperationProtocolData.Case142Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__143:
                self.case__143 = Id015PtlimaptOperationProtocolData.Case143Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__144:
                self.case__144 = Id015PtlimaptOperationProtocolData.Case144Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__145:
                self.case__145 = Id015PtlimaptOperationProtocolData.Case145Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__146:
                self.case__146 = Id015PtlimaptOperationProtocolData.Case146Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__147:
                self.case__147 = Id015PtlimaptOperationProtocolData.Case147Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__148:
                self.case__148 = Id015PtlimaptOperationProtocolData.Case148Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__149:
                self.case__149 = Id015PtlimaptOperationProtocolData.Case149Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__150:
                self.case__150 = Id015PtlimaptOperationProtocolData.Case150Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__151:
                self.case__151 = Id015PtlimaptOperationProtocolData.Case151Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__152:
                self.case__152 = Id015PtlimaptOperationProtocolData.Case152Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__153:
                self.case__153 = Id015PtlimaptOperationProtocolData.Case153Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__154:
                self.case__154 = Id015PtlimaptOperationProtocolData.Case154Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__155:
                self.case__155 = Id015PtlimaptOperationProtocolData.Case155Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__156:
                self.case__156 = Id015PtlimaptOperationProtocolData.Case156Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__157:
                self.case__157 = Id015PtlimaptOperationProtocolData.Case157Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__158:
                self.case__158 = Id015PtlimaptOperationProtocolData.Case158Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__159:
                self.case__159 = Id015PtlimaptOperationProtocolData.Case159Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__160:
                self.case__160 = Id015PtlimaptOperationProtocolData.Case160Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__161:
                self.case__161 = Id015PtlimaptOperationProtocolData.Case161Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__162:
                self.case__162 = Id015PtlimaptOperationProtocolData.Case162Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__163:
                self.case__163 = Id015PtlimaptOperationProtocolData.Case163Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__164:
                self.case__164 = Id015PtlimaptOperationProtocolData.Case164Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__165:
                self.case__165 = Id015PtlimaptOperationProtocolData.Case165Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__166:
                self.case__166 = Id015PtlimaptOperationProtocolData.Case166Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__167:
                self.case__167 = Id015PtlimaptOperationProtocolData.Case167Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__168:
                self.case__168 = Id015PtlimaptOperationProtocolData.Case168Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__169:
                self.case__169 = Id015PtlimaptOperationProtocolData.Case169Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__170:
                self.case__170 = Id015PtlimaptOperationProtocolData.Case170Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__171:
                self.case__171 = Id015PtlimaptOperationProtocolData.Case171Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__172:
                self.case__172 = Id015PtlimaptOperationProtocolData.Case172Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__173:
                self.case__173 = Id015PtlimaptOperationProtocolData.Case173Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__174:
                self.case__174 = Id015PtlimaptOperationProtocolData.Case174Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__175:
                self.case__175 = Id015PtlimaptOperationProtocolData.Case175Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__176:
                self.case__176 = Id015PtlimaptOperationProtocolData.Case176Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__177:
                self.case__177 = Id015PtlimaptOperationProtocolData.Case177Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__178:
                self.case__178 = Id015PtlimaptOperationProtocolData.Case178Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__179:
                self.case__179 = Id015PtlimaptOperationProtocolData.Case179Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__180:
                self.case__180 = Id015PtlimaptOperationProtocolData.Case180Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__181:
                self.case__181 = Id015PtlimaptOperationProtocolData.Case181Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__182:
                self.case__182 = Id015PtlimaptOperationProtocolData.Case182Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__183:
                self.case__183 = Id015PtlimaptOperationProtocolData.Case183Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__184:
                self.case__184 = Id015PtlimaptOperationProtocolData.Case184Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__185:
                self.case__185 = Id015PtlimaptOperationProtocolData.Case185Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__186:
                self.case__186 = Id015PtlimaptOperationProtocolData.Case186Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__187:
                self.case__187 = Id015PtlimaptOperationProtocolData.Case187Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__188:
                self.case__188 = Id015PtlimaptOperationProtocolData.Case188Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__189:
                self.case__189 = Id015PtlimaptOperationProtocolData.Case189Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__190:
                self.case__190 = Id015PtlimaptOperationProtocolData.Case190Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__191:
                self.case__191 = Id015PtlimaptOperationProtocolData.Case1911(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__192:
                self.case__192 = Id015PtlimaptOperationProtocolData.Case1920(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__193:
                self.case__193 = Id015PtlimaptOperationProtocolData.Case1930(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__195:
                self.case__195 = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__200:
                self.case__200 = self._io.read_bytes(32)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__208:
                self.case__208 = self._io.read_bytes(32)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__216:
                self.case__216 = Id015PtlimaptOperationProtocolData.Case216(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__217:
                self.case__217 = Id015PtlimaptOperationProtocolData.Case217(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__218:
                self.case__218 = Id015PtlimaptOperationProtocolData.Case218(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationProtocolData.TreeEncodingTag.case__219:
                self.case__219 = Id015PtlimaptOperationProtocolData.Case219(self._io, self, self._root)



    class PvmStep(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pvm_step_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.PvmStepTag, self._io.read_u1())
            if self.pvm_step_tag == Id015PtlimaptOperationProtocolData.PvmStepTag.arithmetic__pvm__with__proof:
                self.arithmetic__pvm__with__proof = Id015PtlimaptOperationProtocolData.Proof(self._io, self, self._root)

            if self.pvm_step_tag == Id015PtlimaptOperationProtocolData.PvmStepTag.wasm__2__0__0__pvm__with__proof:
                self.wasm__2__0__0__pvm__with__proof = Id015PtlimaptOperationProtocolData.Proof0(self._io, self, self._root)



    class Case208(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__208_field0 = self._io.read_u1()
            self.case__208_field1 = Id015PtlimaptOperationProtocolData.Case208Field10(self._io, self, self._root)
            self.case__208_field2 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case49Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__49_field1_elt_field0 = self._io.read_u1()
            self.case__49_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class ScRollupCement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.commitment = self._io.read_bytes(32)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Case30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__30_field0 = self._io.read_s4be()
            self.case__30_field1 = []
            for i in range(7):
                self.case__30_field1.append(Id015PtlimaptOperationProtocolData.Case30Field1Entries(self._io, self, self._root))



    class Case67(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__67_field0 = self._io.read_s8be()
            self.case__67_field1 = []
            for i in range(32):
                self.case__67_field1.append(Id015PtlimaptOperationProtocolData.Case67Field1Entries(self._io, self, self._root))



    class Case143Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__143_elt_field0 = Id015PtlimaptOperationProtocolData.Case143EltField00(self._io, self, self._root)
            self.case__143_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Amount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.AmountTag, self._io.read_u1())
            if self.amount_tag == Id015PtlimaptOperationProtocolData.AmountTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.amount_tag == Id015PtlimaptOperationProtocolData.AmountTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.amount_tag == Id015PtlimaptOperationProtocolData.AmountTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.amount_tag == Id015PtlimaptOperationProtocolData.AmountTag.case__3:
                self.case__3 = self._io.read_s8be()



    class Id015PtlimaptOperationAlphaContentsAndSignature(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents = []
            i = 0
            while not self._io.is_eof():
                self.contents.append(Id015PtlimaptOperationProtocolData.ContentsEntries(self._io, self, self._root))
                i += 1

            self.signature = self._io.read_bytes(64)


    class Case170Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__170_elt_field0 = Id015PtlimaptOperationProtocolData.Case170EltField00(self._io, self, self._root)
            self.case__170_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptOperationProtocolData.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.args = Id015PtlimaptOperationProtocolData.Args0(self._io, self, self._root)
            self.annots = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Case41Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__41_field1_elt_field0 = self._io.read_u1()
            self.case__41_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case185Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__185_elt_field0 = Id015PtlimaptOperationProtocolData.Case185EltField00(self._io, self, self._root)
            self.case__185_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case158EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__158_elt_field0 = self._io.read_u1()
            if not self.len_case__158_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__158_elt_field0, self._io, u"/types/case__158_elt_field0_0/seq/0")
            self._raw_case__158_elt_field0 = self._io.read_bytes(self.len_case__158_elt_field0)
            _io__raw_case__158_elt_field0 = KaitaiStream(BytesIO(self._raw_case__158_elt_field0))
            self.case__158_elt_field0 = Id015PtlimaptOperationProtocolData.Case158EltField0(_io__raw_case__158_elt_field0, self, self._root)


    class Case189EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__189_elt_field0 = self._io.read_u1()
            if not self.len_case__189_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__189_elt_field0, self._io, u"/types/case__189_elt_field0_0/seq/0")
            self._raw_case__189_elt_field0 = self._io.read_bytes(self.len_case__189_elt_field0)
            _io__raw_case__189_elt_field0 = KaitaiStream(BytesIO(self._raw_case__189_elt_field0))
            self.case__189_elt_field0 = Id015PtlimaptOperationProtocolData.Case189EltField0(_io__raw_case__189_elt_field0, self, self._root)


    class Case175EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__175_elt_field0 = self._io.read_bytes_full()


    class Bh20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh2 = self._io.read_u4be()
            if not self.len_bh2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh2, self._io, u"/types/bh2_0/seq/0")
            self._raw_bh2 = self._io.read_bytes(self.len_bh2)
            _io__raw_bh2 = KaitaiStream(BytesIO(self._raw_bh2))
            self.bh2 = Id015PtlimaptOperationProtocolData.Bh2(_io__raw_bh2, self, self._root)


    class Case151Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__151_elt_field0 = Id015PtlimaptOperationProtocolData.Case151EltField00(self._io, self, self._root)
            self.case__151_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case173Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__173_elt_field0 = Id015PtlimaptOperationProtocolData.Case173EltField00(self._io, self, self._root)
            self.case__173_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case190EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__190_elt_field0 = self._io.read_u1()
            if not self.len_case__190_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__190_elt_field0, self._io, u"/types/case__190_elt_field0_0/seq/0")
            self._raw_case__190_elt_field0 = self._io.read_bytes(self.len_case__190_elt_field0)
            _io__raw_case__190_elt_field0 = KaitaiStream(BytesIO(self._raw_case__190_elt_field0))
            self.case__190_elt_field0 = Id015PtlimaptOperationProtocolData.Case190EltField0(_io__raw_case__190_elt_field0, self, self._root)


    class Case28Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__28_field1_elt_field0 = self._io.read_u1()
            self.case__28_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Commitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.messages = Id015PtlimaptOperationProtocolData.Messages0(self._io, self, self._root)
            self.predecessor = Id015PtlimaptOperationProtocolData.Predecessor(self._io, self, self._root)
            self.inbox_merkle_root = self._io.read_bytes(32)


    class Case2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__2_field0 = self._io.read_s2be()
            self.case__2_field1 = self._io.read_bytes(32)
            self.case__2_field2 = self._io.read_bytes(32)
            self.case__2_field3 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case162EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__162_elt_field0 = self._io.read_bytes_full()


    class TicketsInfoEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.ty = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.ticketer = Id015PtlimaptOperationProtocolData.Id015PtlimaptContractId(self._io, self, self._root)
            self.amount = Id015PtlimaptOperationProtocolData.Amount(self._io, self, self._root)
            self.claimer = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)


    class Case184EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__184_elt_field0 = self._io.read_u1()
            if not self.len_case__184_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__184_elt_field0, self._io, u"/types/case__184_elt_field0_0/seq/0")
            self._raw_case__184_elt_field0 = self._io.read_bytes(self.len_case__184_elt_field0)
            _io__raw_case__184_elt_field0 = KaitaiStream(BytesIO(self._raw_case__184_elt_field0))
            self.case__184_elt_field0 = Id015PtlimaptOperationProtocolData.Case184EltField0(_io__raw_case__184_elt_field0, self, self._root)


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Bool, self._io.read_u1())
            if self.delegate_tag == Id015PtlimaptOperationProtocolData.Bool.true:
                self.delegate = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)



    class Case56(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__56_field0 = self._io.read_u1()
            self.case__56_field1 = []
            for i in range(14):
                self.case__56_field1.append(Id015PtlimaptOperationProtocolData.Case56Field1Entries(self._io, self, self._root))



    class Case167Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__167_elt_field0 = Id015PtlimaptOperationProtocolData.Case167EltField00(self._io, self, self._root)
            self.case__167_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class RegisterGlobalConstant(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.value = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Case53(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__53_field0 = self._io.read_u2be()
            self.case__53_field1 = []
            for i in range(13):
                self.case__53_field1.append(Id015PtlimaptOperationProtocolData.Case53Field1Entries(self._io, self, self._root))



    class Case161EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__161_elt_field0 = self._io.read_bytes_full()


    class Solution(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.solution_field0 = self._io.read_bytes(100)
            self.solution_field1 = self._io.read_bytes(100)


    class Case67Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__67_field1_elt = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case32Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__32_field1_elt_field0 = self._io.read_u1()
            self.case__32_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case166EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__166_elt_field0 = self._io.read_bytes_full()


    class Case64Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__64_field1_elt = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case129Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__129_elt_field0 = Id015PtlimaptOperationProtocolData.Case129EltField00(self._io, self, self._root)
            self.case__129_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case19Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__19_field1_elt_field0 = self._io.read_u1()
            self.case__19_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case176Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__176_elt_field0 = Id015PtlimaptOperationProtocolData.Case176EltField00(self._io, self, self._root)
            self.case__176_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case186EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__186_elt_field0 = self._io.read_u1()
            if not self.len_case__186_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__186_elt_field0, self._io, u"/types/case__186_elt_field0_0/seq/0")
            self._raw_case__186_elt_field0 = self._io.read_bytes(self.len_case__186_elt_field0)
            _io__raw_case__186_elt_field0 = KaitaiStream(BytesIO(self._raw_case__186_elt_field0))
            self.case__186_elt_field0 = Id015PtlimaptOperationProtocolData.Case186EltField0(_io__raw_case__186_elt_field0, self, self._root)


    class ScRollupRecoverBond(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)


    class Case37Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__37_field1_elt_field0 = self._io.read_u1()
            self.case__37_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case176EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__176_elt_field0 = self._io.read_bytes_full()


    class PreviousMessageResult(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.context_hash = self._io.read_bytes(32)
            self.withdraw_list_hash = self._io.read_bytes(32)


    class Case20Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__20_field1_elt_field0 = self._io.read_u1()
            self.case__20_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class PayloadEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_elt = self._io.read_bytes(32)


    class Case217(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__217_field0 = self._io.read_u2be()
            self.case__217_field1 = Id015PtlimaptOperationProtocolData.Case217Field10(self._io, self, self._root)
            self.case__217_field2 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case219Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__219_field1 = self._io.read_bytes_full()


    class Case130EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__130_elt_field0 = self._io.read_bytes_full()


    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = Id015PtlimaptOperationProtocolData.Sequence(_io__raw_sequence, self, self._root)


    class Case179Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__179_elt_field0 = Id015PtlimaptOperationProtocolData.Case179EltField00(self._io, self, self._root)
            self.case__179_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case170EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__170_elt_field0 = self._io.read_bytes_full()


    class Case173EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__173_elt_field0 = self._io.read_u1()
            if not self.len_case__173_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__173_elt_field0, self._io, u"/types/case__173_elt_field0_0/seq/0")
            self._raw_case__173_elt_field0 = self._io.read_bytes(self.len_case__173_elt_field0)
            _io__raw_case__173_elt_field0 = KaitaiStream(BytesIO(self._raw_case__173_elt_field0))
            self.case__173_elt_field0 = Id015PtlimaptOperationProtocolData.Case173EltField0(_io__raw_case__173_elt_field0, self, self._root)


    class Case6(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__6_field0 = self._io.read_s4be()
            self.case__6_field1 = []
            for i in range(1):
                self.case__6_field1.append(Id015PtlimaptOperationProtocolData.Case6Field1Entries(self._io, self, self._root))



    class Case139Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__139_elt_field0 = Id015PtlimaptOperationProtocolData.Case139EltField00(self._io, self, self._root)
            self.case__139_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class ContentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__operation__alpha__contents = Id015PtlimaptOperationProtocolData.Id015PtlimaptOperationAlphaContents(self._io, self, self._root)


    class Case158EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__158_elt_field0 = self._io.read_bytes_full()


    class Case4Field1Entries0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__4_field1_elt_field0 = self._io.read_u1()
            self.case__4_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class PreviousMessageResultPath(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.previous_message_result_path_entries = []
            i = 0
            while not self._io.is_eof():
                self.previous_message_result_path_entries.append(Id015PtlimaptOperationProtocolData.PreviousMessageResultPathEntries(self._io, self, self._root))
                i += 1



    class Case168Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__168_elt_field0 = Id015PtlimaptOperationProtocolData.Case168EltField00(self._io, self, self._root)
            self.case__168_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case186EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__186_elt_field0 = self._io.read_bytes_full()


    class ScRollupAddMessages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.message = Id015PtlimaptOperationProtocolData.Message1(self._io, self, self._root)


    class Case172EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__172_elt_field0 = self._io.read_u1()
            if not self.len_case__172_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__172_elt_field0, self._io, u"/types/case__172_elt_field0_0/seq/0")
            self._raw_case__172_elt_field0 = self._io.read_bytes(self.len_case__172_elt_field0)
            _io__raw_case__172_elt_field0 = KaitaiStream(BytesIO(self._raw_case__172_elt_field0))
            self.case__172_elt_field0 = Id015PtlimaptOperationProtocolData.Case172EltField0(_io__raw_case__172_elt_field0, self, self._root)


    class Case174EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__174_elt_field0 = self._io.read_bytes_full()


    class Case38Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__38_field1_elt_field0 = self._io.read_u1()
            self.case__38_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case150EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__150_elt_field0 = self._io.read_u1()
            if not self.len_case__150_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__150_elt_field0, self._io, u"/types/case__150_elt_field0_0/seq/0")
            self._raw_case__150_elt_field0 = self._io.read_bytes(self.len_case__150_elt_field0)
            _io__raw_case__150_elt_field0 = KaitaiStream(BytesIO(self._raw_case__150_elt_field0))
            self.case__150_elt_field0 = Id015PtlimaptOperationProtocolData.Case150EltField0(_io__raw_case__150_elt_field0, self, self._root)


    class Case174Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__174_elt_field0 = Id015PtlimaptOperationProtocolData.Case174EltField00(self._io, self, self._root)
            self.case__174_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(Id015PtlimaptOperationProtocolData.SequenceEntries(self._io, self, self._root))
                i += 1



    class Case48(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__48_field0 = self._io.read_u1()
            self.case__48_field1 = []
            for i in range(12):
                self.case__48_field1.append(Id015PtlimaptOperationProtocolData.Case48Field1Entries(self._io, self, self._root))



    class PreviousMessageResultPath0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_previous_message_result_path = self._io.read_u4be()
            if not self.len_previous_message_result_path <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_previous_message_result_path, self._io, u"/types/previous_message_result_path_0/seq/0")
            self._raw_previous_message_result_path = self._io.read_bytes(self.len_previous_message_result_path)
            _io__raw_previous_message_result_path = KaitaiStream(BytesIO(self._raw_previous_message_result_path))
            self.previous_message_result_path = Id015PtlimaptOperationProtocolData.PreviousMessageResultPath(_io__raw_previous_message_result_path, self, self._root)


    class Case133EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__133_elt_field0 = self._io.read_bytes_full()


    class Case149EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__149_elt_field0 = self._io.read_u1()
            if not self.len_case__149_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__149_elt_field0, self._io, u"/types/case__149_elt_field0_0/seq/0")
            self._raw_case__149_elt_field0 = self._io.read_bytes(self.len_case__149_elt_field0)
            _io__raw_case__149_elt_field0 = KaitaiStream(BytesIO(self._raw_case__149_elt_field0))
            self.case__149_elt_field0 = Id015PtlimaptOperationProtocolData.Case149EltField0(_io__raw_case__149_elt_field0, self, self._root)


    class Bh10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh1 = self._io.read_u4be()
            if not self.len_bh1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh1, self._io, u"/types/bh1_0/seq/0")
            self._raw_bh1 = self._io.read_bytes(self.len_bh1)
            _io__raw_bh1 = KaitaiStream(BytesIO(self._raw_bh1))
            self.bh1 = Id015PtlimaptOperationProtocolData.Bh1(_io__raw_bh1, self, self._root)


    class Case144EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__144_elt_field0 = self._io.read_bytes_full()


    class Case133Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__133_elt_field0 = Id015PtlimaptOperationProtocolData.Case133EltField00(self._io, self, self._root)
            self.case__133_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case140EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__140_elt_field0 = self._io.read_u1()
            if not self.len_case__140_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__140_elt_field0, self._io, u"/types/case__140_elt_field0_0/seq/0")
            self._raw_case__140_elt_field0 = self._io.read_bytes(self.len_case__140_elt_field0)
            _io__raw_case__140_elt_field0 = KaitaiStream(BytesIO(self._raw_case__140_elt_field0))
            self.case__140_elt_field0 = Id015PtlimaptOperationProtocolData.Case140EltField0(_io__raw_case__140_elt_field0, self, self._root)


    class Case183EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__183_elt_field0 = self._io.read_bytes_full()


    class Case131EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_elt_field0 = self._io.read_bytes_full()


    class Case3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__3_field0 = self._io.read_s2be()
            self.case__3_field1 = self._io.read_bytes(32)
            self.case__3_field2 = self._io.read_bytes(32)
            self.case__3_field3 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Op22(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_2/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id015PtlimaptOperationProtocolData.Op21(_io__raw_op2, self, self._root)


    class Case136EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__136_elt_field0 = self._io.read_bytes_full()


    class Case45Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__45_field1_elt_field0 = self._io.read_u1()
            self.case__45_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case59(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__59_field0 = self._io.read_s8be()
            self.case__59_field1 = []
            for i in range(14):
                self.case__59_field1.append(Id015PtlimaptOperationProtocolData.Case59Field1Entries(self._io, self, self._root))



    class ScRollupTimeout(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.stakers = Id015PtlimaptOperationProtocolData.Stakers(self._io, self, self._root)


    class CircuitsInfoEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.circuits_info_elt_field0 = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.circuits_info_elt_field1 = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Bool, self._io.read_u1())


    class Case190Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__190_elt_field0 = Id015PtlimaptOperationProtocolData.Case190EltField00(self._io, self, self._root)
            self.case__190_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case29(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__29_field0 = self._io.read_u2be()
            self.case__29_field1 = []
            for i in range(7):
                self.case__29_field1.append(Id015PtlimaptOperationProtocolData.Case29Field1Entries(self._io, self, self._root))



    class Message0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_entries.append(Id015PtlimaptOperationProtocolData.MessageEntries(self._io, self, self._root))
                i += 1



    class Case26Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__26_field1_elt_field0 = self._io.read_u1()
            self.case__26_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class InboxProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.message_counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.serialized_proof = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Id015PtlimaptInlinedEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__endorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id015PtlimaptOperationProtocolData.Id015PtlimaptInlinedEndorsementMempoolContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Bool, self._io.read_u1())
            if self.signature_tag == Id015PtlimaptOperationProtocolData.Bool.true:
                self.signature = self._io.read_bytes(64)



    class Case16Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__16_field1_elt_field0 = self._io.read_u1()
            self.case__16_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case211Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__211_field1 = self._io.read_u1()
            if not self.len_case__211_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__211_field1, self._io, u"/types/case__211_field1_0/seq/0")
            self._raw_case__211_field1 = self._io.read_bytes(self.len_case__211_field1)
            _io__raw_case__211_field1 = KaitaiStream(BytesIO(self._raw_case__211_field1))
            self.case__211_field1 = Id015PtlimaptOperationProtocolData.Case211Field1(_io__raw_case__211_field1, self, self._root)


    class Case1930(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__193 = self._io.read_u2be()
            if not self.len_case__193 <= 65535:
                raise kaitaistruct.ValidationGreaterThanError(65535, self.len_case__193, self._io, u"/types/case__193_0/seq/0")
            self._raw_case__193 = self._io.read_bytes(self.len_case__193)
            _io__raw_case__193 = KaitaiStream(BytesIO(self._raw_case__193))
            self.case__193 = Id015PtlimaptOperationProtocolData.Case193(_io__raw_case__193, self, self._root)


    class OpEltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_elt_field1_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.OpEltField1Tag, self._io.read_u1())
            if self.op_elt_field1_tag == Id015PtlimaptOperationProtocolData.OpEltField1Tag.some:
                self.some = Id015PtlimaptOperationProtocolData.Some(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Case131Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_elt_field0 = Id015PtlimaptOperationProtocolData.Case131EltField00(self._io, self, self._root)
            self.case__131_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case138Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__138_elt_field0 = Id015PtlimaptOperationProtocolData.Case138EltField00(self._io, self, self._root)
            self.case__138_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Op0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op = self._io.read_u4be()
            if not self.len_op <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op, self._io, u"/types/op_0/seq/0")
            self._raw_op = self._io.read_bytes(self.len_op)
            _io__raw_op = KaitaiStream(BytesIO(self._raw_op))
            self.op = Id015PtlimaptOperationProtocolData.Op(_io__raw_op, self, self._root)


    class Proof1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pvm_step = Id015PtlimaptOperationProtocolData.PvmStep(self._io, self, self._root)
            self.input_proof_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Bool, self._io.read_u1())
            if self.input_proof_tag == Id015PtlimaptOperationProtocolData.Bool.true:
                self.input_proof = Id015PtlimaptOperationProtocolData.InputProof(self._io, self, self._root)



    class Bh1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__block_header__alpha__full_header = Id015PtlimaptOperationProtocolData.Id015PtlimaptBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Case63(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__63_field0 = self._io.read_s8be()
            self.case__63_field1 = Id015PtlimaptOperationProtocolData.Case63Field10(self._io, self, self._root)


    class Case34Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__34_field1_elt_field0 = self._io.read_u1()
            self.case__34_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Payload0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_payload = self._io.read_u4be()
            if not self.len_payload <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_payload, self._io, u"/types/payload_0/seq/0")
            self._raw_payload = self._io.read_bytes(self.len_payload)
            _io__raw_payload = KaitaiStream(BytesIO(self._raw_payload))
            self.payload = Id015PtlimaptOperationProtocolData.Payload(_io__raw_payload, self, self._root)


    class Case161EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__161_elt_field0 = self._io.read_u1()
            if not self.len_case__161_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__161_elt_field0, self._io, u"/types/case__161_elt_field0_0/seq/0")
            self._raw_case__161_elt_field0 = self._io.read_bytes(self.len_case__161_elt_field0)
            _io__raw_case__161_elt_field0 = KaitaiStream(BytesIO(self._raw_case__161_elt_field0))
            self.case__161_elt_field0 = Id015PtlimaptOperationProtocolData.Case161EltField0(_io__raw_case__161_elt_field0, self, self._root)


    class Case156EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__156_elt_field0 = self._io.read_u1()
            if not self.len_case__156_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__156_elt_field0, self._io, u"/types/case__156_elt_field0_0/seq/0")
            self._raw_case__156_elt_field0 = self._io.read_bytes(self.len_case__156_elt_field0)
            _io__raw_case__156_elt_field0 = KaitaiStream(BytesIO(self._raw_case__156_elt_field0))
            self.case__156_elt_field0 = Id015PtlimaptOperationProtocolData.Case156EltField0(_io__raw_case__156_elt_field0, self, self._root)


    class Dissection0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_dissection = self._io.read_u4be()
            if not self.len_dissection <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_dissection, self._io, u"/types/dissection_0/seq/0")
            self._raw_dissection = self._io.read_bytes(self.len_dissection)
            _io__raw_dissection = KaitaiStream(BytesIO(self._raw_dissection))
            self.dissection = Id015PtlimaptOperationProtocolData.Dissection(_io__raw_dissection, self, self._root)


    class Case177EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__177_elt_field0 = self._io.read_bytes_full()


    class Case142EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__142_elt_field0 = self._io.read_bytes_full()


    class Case25Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__25_field1_elt_field0 = self._io.read_u1()
            self.case__25_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class TxRollupRejection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptTxRollupId(self._io, self, self._root)
            self.level = self._io.read_s4be()
            self.message = Id015PtlimaptOperationProtocolData.Message(self._io, self, self._root)
            self.message_position = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.message_path = Id015PtlimaptOperationProtocolData.MessagePath0(self._io, self, self._root)
            self.message_result_hash = self._io.read_bytes(32)
            self.message_result_path = Id015PtlimaptOperationProtocolData.MessageResultPath0(self._io, self, self._root)
            self.previous_message_result = Id015PtlimaptOperationProtocolData.PreviousMessageResult(self._io, self, self._root)
            self.previous_message_result_path = Id015PtlimaptOperationProtocolData.PreviousMessageResultPath0(self._io, self, self._root)
            self.proof = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class SetDepositsLimit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.limit_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Bool, self._io.read_u1())
            if self.limit_tag == Id015PtlimaptOperationProtocolData.Bool.true:
                self.limit = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)



    class Case184Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__184_elt_field0 = Id015PtlimaptOperationProtocolData.Case184EltField00(self._io, self, self._root)
            self.case__184_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case185EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__185_elt_field0 = self._io.read_bytes_full()


    class Case159EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__159_elt_field0 = self._io.read_u1()
            if not self.len_case__159_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__159_elt_field0, self._io, u"/types/case__159_elt_field0_0/seq/0")
            self._raw_case__159_elt_field0 = self._io.read_bytes(self.len_case__159_elt_field0)
            _io__raw_case__159_elt_field0 = KaitaiStream(BytesIO(self._raw_case__159_elt_field0))
            self.case__159_elt_field0 = Id015PtlimaptOperationProtocolData.Case159EltField0(_io__raw_case__159_elt_field0, self, self._root)


    class Case169Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__169_elt_field0 = Id015PtlimaptOperationProtocolData.Case169EltField00(self._io, self, self._root)
            self.case__169_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case137Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__137_elt_field0 = Id015PtlimaptOperationProtocolData.Case137EltField00(self._io, self, self._root)
            self.case__137_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case15(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__15_field0 = self._io.read_s8be()
            self.case__15_field1 = []
            for i in range(3):
                self.case__15_field1.append(Id015PtlimaptOperationProtocolData.Case15Field1Entries(self._io, self, self._root))



    class Case133EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__133_elt_field0 = self._io.read_u1()
            if not self.len_case__133_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__133_elt_field0, self._io, u"/types/case__133_elt_field0_0/seq/0")
            self._raw_case__133_elt_field0 = self._io.read_bytes(self.len_case__133_elt_field0)
            _io__raw_case__133_elt_field0 = KaitaiStream(BytesIO(self._raw_case__133_elt_field0))
            self.case__133_elt_field0 = Id015PtlimaptOperationProtocolData.Case133EltField0(_io__raw_case__133_elt_field0, self, self._root)


    class Message1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message = self._io.read_u4be()
            if not self.len_message <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message, self._io, u"/types/message_1/seq/0")
            self._raw_message = self._io.read_bytes(self.len_message)
            _io__raw_message = KaitaiStream(BytesIO(self._raw_message))
            self.message = Id015PtlimaptOperationProtocolData.Message0(_io__raw_message, self, self._root)


    class Case176EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__176_elt_field0 = self._io.read_u1()
            if not self.len_case__176_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__176_elt_field0, self._io, u"/types/case__176_elt_field0_0/seq/0")
            self._raw_case__176_elt_field0 = self._io.read_bytes(self.len_case__176_elt_field0)
            _io__raw_case__176_elt_field0 = KaitaiStream(BytesIO(self._raw_case__176_elt_field0))
            self.case__176_elt_field0 = Id015PtlimaptOperationProtocolData.Case176EltField0(_io__raw_case__176_elt_field0, self, self._root)


    class Case152Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__152_elt_field0 = Id015PtlimaptOperationProtocolData.Case152EltField00(self._io, self, self._root)
            self.case__152_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class ScRollupRefute(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.opponent = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.refutation_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Bool, self._io.read_u1())
            if self.refutation_tag == Id015PtlimaptOperationProtocolData.Bool.true:
                self.refutation = Id015PtlimaptOperationProtocolData.Refutation(self._io, self, self._root)



    class Op10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_0/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id015PtlimaptOperationProtocolData.Op1(_io__raw_op1, self, self._root)


    class Case63Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__63_field1_elt_field0 = self._io.read_u1()
            self.case__63_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class InitState0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_init_state = self._io.read_u4be()
            if not self.len_init_state <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_init_state, self._io, u"/types/init_state_0/seq/0")
            self._raw_init_state = self._io.read_bytes(self.len_init_state)
            _io__raw_init_state = KaitaiStream(BytesIO(self._raw_init_state))
            self.init_state = Id015PtlimaptOperationProtocolData.InitState(_io__raw_init_state, self, self._root)


    class Case156Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__156_elt_field0 = Id015PtlimaptOperationProtocolData.Case156EltField00(self._io, self, self._root)
            self.case__156_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case209Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__209_field1 = self._io.read_u1()
            if not self.len_case__209_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__209_field1, self._io, u"/types/case__209_field1_0/seq/0")
            self._raw_case__209_field1 = self._io.read_bytes(self.len_case__209_field1)
            _io__raw_case__209_field1 = KaitaiStream(BytesIO(self._raw_case__209_field1))
            self.case__209_field1 = Id015PtlimaptOperationProtocolData.Case209Field1(_io__raw_case__209_field1, self, self._root)


    class Case147EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__147_elt_field0 = self._io.read_bytes_full()


    class Op21(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__preendorsement = Id015PtlimaptOperationProtocolData.Id015PtlimaptInlinedPreendorsement(self._io, self, self._root)


    class Case20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__20_field0 = self._io.read_u1()
            self.case__20_field1 = []
            for i in range(5):
                self.case__20_field1.append(Id015PtlimaptOperationProtocolData.Case20Field1Entries(self._io, self, self._root))



    class Case155EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__155_elt_field0 = self._io.read_bytes_full()


    class Proof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proof_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.ProofTag, self._io.read_u1())
            if self.proof_tag == Id015PtlimaptOperationProtocolData.ProofTag.case__0:
                self.case__0 = Id015PtlimaptOperationProtocolData.Case0(self._io, self, self._root)

            if self.proof_tag == Id015PtlimaptOperationProtocolData.ProofTag.case__2:
                self.case__2 = Id015PtlimaptOperationProtocolData.Case2(self._io, self, self._root)

            if self.proof_tag == Id015PtlimaptOperationProtocolData.ProofTag.case__1:
                self.case__1 = Id015PtlimaptOperationProtocolData.Case1(self._io, self, self._root)

            if self.proof_tag == Id015PtlimaptOperationProtocolData.ProofTag.case__3:
                self.case__3 = Id015PtlimaptOperationProtocolData.Case3(self._io, self, self._root)



    class Case180Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__180_elt_field0 = Id015PtlimaptOperationProtocolData.Case180EltField00(self._io, self, self._root)
            self.case__180_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case151EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__151_elt_field0 = self._io.read_bytes_full()


    class Case191EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__191_elt_field0 = self._io.read_bytes_full()


    class Commitment0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.compressed_state = self._io.read_bytes(32)
            self.inbox_level = self._io.read_s4be()
            self.predecessor = self._io.read_bytes(32)
            self.number_of_ticks = self._io.read_s8be()


    class Case143EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__143_elt_field0 = self._io.read_u1()
            if not self.len_case__143_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__143_elt_field0, self._io, u"/types/case__143_elt_field0_0/seq/0")
            self._raw_case__143_elt_field0 = self._io.read_bytes(self.len_case__143_elt_field0)
            _io__raw_case__143_elt_field0 = KaitaiStream(BytesIO(self._raw_case__143_elt_field0))
            self.case__143_elt_field0 = Id015PtlimaptOperationProtocolData.Case143EltField0(_io__raw_case__143_elt_field0, self, self._root)


    class Case37(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__37_field0 = self._io.read_u2be()
            self.case__37_field1 = []
            for i in range(9):
                self.case__37_field1.append(Id015PtlimaptOperationProtocolData.Case37Field1Entries(self._io, self, self._root))



    class Case35Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__35_field1_elt_field0 = self._io.read_u1()
            self.case__35_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.balance = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Bool, self._io.read_u1())
            if self.delegate_tag == Id015PtlimaptOperationProtocolData.Bool.true:
                self.delegate = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)

            self.script = Id015PtlimaptOperationProtocolData.Id015PtlimaptScriptedContracts(self._io, self, self._root)


    class Case149Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__149_elt_field0 = Id015PtlimaptOperationProtocolData.Case149EltField00(self._io, self, self._root)
            self.case__149_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class DrainDelegate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.consensus_key = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.delegate = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.destination = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)


    class Case46Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__46_field1_elt_field0 = self._io.read_u1()
            self.case__46_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case164EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__164_elt_field0 = self._io.read_bytes_full()


    class Case26(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__26_field0 = self._io.read_s4be()
            self.case__26_field1 = []
            for i in range(6):
                self.case__26_field1.append(Id015PtlimaptOperationProtocolData.Case26Field1Entries(self._io, self, self._root))



    class Case31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__31_field0 = self._io.read_s8be()
            self.case__31_field1 = []
            for i in range(7):
                self.case__31_field1.append(Id015PtlimaptOperationProtocolData.Case31Field1Entries(self._io, self, self._root))



    class Case19(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__19_field0 = self._io.read_s8be()
            self.case__19_field1 = []
            for i in range(4):
                self.case__19_field1.append(Id015PtlimaptOperationProtocolData.Case19Field1Entries(self._io, self, self._root))



    class Id015PtlimaptContractIdOriginated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__contract_id__originated_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Id015PtlimaptContractIdOriginatedTag, self._io.read_u1())
            if self.id_015__ptlimapt__contract_id__originated_tag == Id015PtlimaptOperationProtocolData.Id015PtlimaptContractIdOriginatedTag.originated:
                self.originated = Id015PtlimaptOperationProtocolData.Originated(self._io, self, self._root)



    class Micheline015PtlimaptMichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__015__ptlimapt__michelson_v1__expression_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1ExpressionTag.int:
                self.int = Id015PtlimaptOperationProtocolData.Z(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1ExpressionTag.string:
                self.string = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1ExpressionTag.sequence:
                self.sequence = Id015PtlimaptOperationProtocolData.Sequence0(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = Id015PtlimaptOperationProtocolData.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = Id015PtlimaptOperationProtocolData.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = Id015PtlimaptOperationProtocolData.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = Id015PtlimaptOperationProtocolData.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = Id015PtlimaptOperationProtocolData.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = Id015PtlimaptOperationProtocolData.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = Id015PtlimaptOperationProtocolData.PrimGeneric(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1ExpressionTag.bytes:
                self.bytes = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)



    class Case43Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__43_field1_elt_field0 = self._io.read_u1()
            self.case__43_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case160EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__160_elt_field0 = self._io.read_u1()
            if not self.len_case__160_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__160_elt_field0, self._io, u"/types/case__160_elt_field0_0/seq/0")
            self._raw_case__160_elt_field0 = self._io.read_bytes(self.len_case__160_elt_field0)
            _io__raw_case__160_elt_field0 = KaitaiStream(BytesIO(self._raw_case__160_elt_field0))
            self.case__160_elt_field0 = Id015PtlimaptOperationProtocolData.Case160EltField0(_io__raw_case__160_elt_field0, self, self._root)


    class Case41(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__41_field0 = self._io.read_u2be()
            self.case__41_field1 = []
            for i in range(10):
                self.case__41_field1.append(Id015PtlimaptOperationProtocolData.Case41Field1Entries(self._io, self, self._root))



    class Case209(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__209_field0 = self._io.read_u2be()
            self.case__209_field1 = Id015PtlimaptOperationProtocolData.Case209Field10(self._io, self, self._root)
            self.case__209_field2 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field0 = self._io.read_s2be()
            self.case__0_field1 = self._io.read_bytes(32)
            self.case__0_field2 = self._io.read_bytes(32)
            self.case__0_field3 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case173EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__173_elt_field0 = self._io.read_bytes_full()


    class Case12Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__12_field1_elt_field0 = self._io.read_u1()
            self.case__12_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case14Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__14_field1_elt_field0 = self._io.read_u1()
            self.case__14_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Message(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.MessageTag, self._io.read_u1())
            if self.message_tag == Id015PtlimaptOperationProtocolData.MessageTag.batch:
                self.batch = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)

            if self.message_tag == Id015PtlimaptOperationProtocolData.MessageTag.deposit:
                self.deposit = Id015PtlimaptOperationProtocolData.Deposit(self._io, self, self._root)



    class Case30Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__30_field1_elt_field0 = self._io.read_u1()
            self.case__30_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case141Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__141_elt_field0 = Id015PtlimaptOperationProtocolData.Case141EltField00(self._io, self, self._root)
            self.case__141_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case178EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__178_elt_field0 = self._io.read_bytes_full()


    class InitState(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.init_state_entries = []
            i = 0
            while not self._io.is_eof():
                self.init_state_entries.append(Id015PtlimaptOperationProtocolData.InitStateEntries(self._io, self, self._root))
                i += 1



    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(Id015PtlimaptOperationProtocolData.ArgsEntries(self._io, self, self._root))
                i += 1



    class Case172EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__172_elt_field0 = self._io.read_bytes_full()


    class Case139EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__139_elt_field0 = self._io.read_bytes_full()


    class Case209Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__209_field1 = self._io.read_bytes_full()


    class ZkRollupOrigination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.public_parameters = Id015PtlimaptOperationProtocolData.PublicParameters(self._io, self, self._root)
            self.circuits_info = Id015PtlimaptOperationProtocolData.CircuitsInfo0(self._io, self, self._root)
            self.init_state = Id015PtlimaptOperationProtocolData.InitState0(self._io, self, self._root)
            self.nb_ops = Id015PtlimaptOperationProtocolData.Int31(self._io, self, self._root)


    class Case159EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__159_elt_field0 = self._io.read_bytes_full()


    class Case216(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__216_field0 = self._io.read_u1()
            self.case__216_field1 = Id015PtlimaptOperationProtocolData.Case216Field10(self._io, self, self._root)
            self.case__216_field2 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Op2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__endorsement = Id015PtlimaptOperationProtocolData.Id015PtlimaptInlinedEndorsement(self._io, self, self._root)


    class Case56Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__56_field1_elt_field0 = self._io.read_u1()
            self.case__56_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case216Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__216_field1 = self._io.read_u1()
            if not self.len_case__216_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__216_field1, self._io, u"/types/case__216_field1_0/seq/0")
            self._raw_case__216_field1 = self._io.read_bytes(self.len_case__216_field1)
            _io__raw_case__216_field1 = KaitaiStream(BytesIO(self._raw_case__216_field1))
            self.case__216_field1 = Id015PtlimaptOperationProtocolData.Case216Field1(_io__raw_case__216_field1, self, self._root)


    class Case191(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__191_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__191_entries.append(Id015PtlimaptOperationProtocolData.Case191Entries(self._io, self, self._root))
                i += 1



    class Case153Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__153_elt_field0 = Id015PtlimaptOperationProtocolData.Case153EltField00(self._io, self, self._root)
            self.case__153_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case63Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__63_field1 = self._io.read_u4be()
            if not self.len_case__63_field1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__63_field1, self._io, u"/types/case__63_field1_0/seq/0")
            self._raw_case__63_field1 = self._io.read_bytes(self.len_case__63_field1)
            _io__raw_case__63_field1 = KaitaiStream(BytesIO(self._raw_case__63_field1))
            self.case__63_field1 = Id015PtlimaptOperationProtocolData.Case63Field1(_io__raw_case__63_field1, self, self._root)


    class Case7(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__7_field0 = self._io.read_s8be()
            self.case__7_field1 = []
            for i in range(1):
                self.case__7_field1.append(Id015PtlimaptOperationProtocolData.Case7Field1Entries(self._io, self, self._root))



    class Case218Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__218_field1 = self._io.read_bytes_full()


    class Case15Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__15_field1_elt_field0 = self._io.read_u1()
            self.case__15_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case156EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__156_elt_field0 = self._io.read_bytes_full()


    class Case174EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__174_elt_field0 = self._io.read_u1()
            if not self.len_case__174_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__174_elt_field0, self._io, u"/types/case__174_elt_field0_0/seq/0")
            self._raw_case__174_elt_field0 = self._io.read_bytes(self.len_case__174_elt_field0)
            _io__raw_case__174_elt_field0 = KaitaiStream(BytesIO(self._raw_case__174_elt_field0))
            self.case__174_elt_field0 = Id015PtlimaptOperationProtocolData.Case174EltField0(_io__raw_case__174_elt_field0, self, self._root)


    class Case160EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__160_elt_field0 = self._io.read_bytes_full()


    class TxRollupReturnBond(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptTxRollupId(self._io, self, self._root)


    class Case169EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__169_elt_field0 = self._io.read_u1()
            if not self.len_case__169_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__169_elt_field0, self._io, u"/types/case__169_elt_field0_0/seq/0")
            self._raw_case__169_elt_field0 = self._io.read_bytes(self.len_case__169_elt_field0)
            _io__raw_case__169_elt_field0 = KaitaiStream(BytesIO(self._raw_case__169_elt_field0))
            self.case__169_elt_field0 = Id015PtlimaptOperationProtocolData.Case169EltField0(_io__raw_case__169_elt_field0, self, self._root)


    class Dissection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dissection_entries = []
            i = 0
            while not self._io.is_eof():
                self.dissection_entries.append(Id015PtlimaptOperationProtocolData.DissectionEntries(self._io, self, self._root))
                i += 1



    class TicketsInfo0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_tickets_info = self._io.read_u4be()
            if not self.len_tickets_info <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_tickets_info, self._io, u"/types/tickets_info_0/seq/0")
            self._raw_tickets_info = self._io.read_bytes(self.len_tickets_info)
            _io__raw_tickets_info = KaitaiStream(BytesIO(self._raw_tickets_info))
            self.tickets_info = Id015PtlimaptOperationProtocolData.TicketsInfo(_io__raw_tickets_info, self, self._root)


    class TxRollupFinalizeCommitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptTxRollupId(self._io, self, self._root)


    class Case17Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__17_field1_elt_field0 = self._io.read_u1()
            self.case__17_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class UpdateConsensusKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.pk = Id015PtlimaptOperationProtocolData.PublicKey(self._io, self, self._root)


    class Case145EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__145_elt_field0 = self._io.read_bytes_full()


    class TxRollupOrigination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)


    class Case47(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__47_field0 = self._io.read_s8be()
            self.case__47_field1 = []
            for i in range(11):
                self.case__47_field1.append(Id015PtlimaptOperationProtocolData.Case47Field1Entries(self._io, self, self._root))



    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptOperationProtocolData.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class Case21Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__21_field1_elt_field0 = self._io.read_u1()
            self.case__21_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case60(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__60_field0 = self._io.read_u1()
            self.case__60_field1 = Id015PtlimaptOperationProtocolData.Case60Field10(self._io, self, self._root)


    class Case52Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__52_field1_elt_field0 = self._io.read_u1()
            self.case__52_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class MessageResultPath(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_path_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_result_path_entries.append(Id015PtlimaptOperationProtocolData.MessageResultPathEntries(self._io, self, self._root))
                i += 1



    class Id015PtlimaptMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__michelson__v1__primitives = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Id015PtlimaptMichelsonV1Primitives, self._io.read_u1())


    class Case175Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__175_elt_field0 = Id015PtlimaptOperationProtocolData.Case175EltField00(self._io, self, self._root)
            self.case__175_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case177EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__177_elt_field0 = self._io.read_u1()
            if not self.len_case__177_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__177_elt_field0, self._io, u"/types/case__177_elt_field0_0/seq/0")
            self._raw_case__177_elt_field0 = self._io.read_bytes(self.len_case__177_elt_field0)
            _io__raw_case__177_elt_field0 = KaitaiStream(BytesIO(self._raw_case__177_elt_field0))
            self.case__177_elt_field0 = Id015PtlimaptOperationProtocolData.Case177EltField0(_io__raw_case__177_elt_field0, self, self._root)


    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class Case181EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__181_elt_field0 = self._io.read_u1()
            if not self.len_case__181_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__181_elt_field0, self._io, u"/types/case__181_elt_field0_0/seq/0")
            self._raw_case__181_elt_field0 = self._io.read_bytes(self.len_case__181_elt_field0)
            _io__raw_case__181_elt_field0 = KaitaiStream(BytesIO(self._raw_case__181_elt_field0))
            self.case__181_elt_field0 = Id015PtlimaptOperationProtocolData.Case181EltField0(_io__raw_case__181_elt_field0, self, self._root)


    class Case8Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__8_field1_elt_field0 = self._io.read_u1()
            self.case__8_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptOperationProtocolData.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class MessageResultPath0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message_result_path = self._io.read_u4be()
            if not self.len_message_result_path <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message_result_path, self._io, u"/types/message_result_path_0/seq/0")
            self._raw_message_result_path = self._io.read_bytes(self.len_message_result_path)
            _io__raw_message_result_path = KaitaiStream(BytesIO(self._raw_message_result_path))
            self.message_result_path = Id015PtlimaptOperationProtocolData.MessageResultPath(_io__raw_message_result_path, self, self._root)


    class Case58(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__58_field0 = self._io.read_s4be()
            self.case__58_field1 = []
            for i in range(14):
                self.case__58_field1.append(Id015PtlimaptOperationProtocolData.Case58Field1Entries(self._io, self, self._root))



    class Case181EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__181_elt_field0 = self._io.read_bytes_full()


    class MessagePathEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inbox_list_hash = self._io.read_bytes(32)


    class PublicParameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_parameters_field0 = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.public_parameters_field1 = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Case24Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__24_field1_elt_field0 = self._io.read_u1()
            self.case__24_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case154EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__154_elt_field0 = self._io.read_u1()
            if not self.len_case__154_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__154_elt_field0, self._io, u"/types/case__154_elt_field0_0/seq/0")
            self._raw_case__154_elt_field0 = self._io.read_bytes(self.len_case__154_elt_field0)
            _io__raw_case__154_elt_field0 = KaitaiStream(BytesIO(self._raw_case__154_elt_field0))
            self.case__154_elt_field0 = Id015PtlimaptOperationProtocolData.Case154EltField0(_io__raw_case__154_elt_field0, self, self._root)


    class InitStateEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.init_state_elt = self._io.read_bytes(32)


    class Case179EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__179_elt_field0 = self._io.read_bytes_full()


    class DalSlotAvailability(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.endorser = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.endorsement = Id015PtlimaptOperationProtocolData.Z(self._io, self, self._root)


    class Slot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.index = self._io.read_u1()
            self.header = self._io.read_bytes(48)


    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class Case135EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__135_elt_field0 = self._io.read_bytes_full()


    class Case153EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__153_elt_field0 = self._io.read_bytes_full()


    class Deposit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sender = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.destination = self._io.read_bytes(20)
            self.ticket_hash = self._io.read_bytes(32)
            self.amount = Id015PtlimaptOperationProtocolData.Amount(self._io, self, self._root)


    class Case44Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__44_field1_elt_field0 = self._io.read_u1()
            self.case__44_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case132Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__132_elt_field0 = Id015PtlimaptOperationProtocolData.Case132EltField00(self._io, self, self._root)
            self.case__132_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class MessagePath(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_path_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_path_entries.append(Id015PtlimaptOperationProtocolData.MessagePathEntries(self._io, self, self._root))
                i += 1



    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = Id015PtlimaptOperationProtocolData.Args(_io__raw_args, self, self._root)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id015PtlimaptOperationProtocolData.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.amount = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.destination = Id015PtlimaptOperationProtocolData.Id015PtlimaptContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Bool, self._io.read_u1())
            if self.parameters_tag == Id015PtlimaptOperationProtocolData.Bool.true:
                self.parameters = Id015PtlimaptOperationProtocolData.Parameters(self._io, self, self._root)



    class Case144EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__144_elt_field0 = self._io.read_u1()
            if not self.len_case__144_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__144_elt_field0, self._io, u"/types/case__144_elt_field0_0/seq/0")
            self._raw_case__144_elt_field0 = self._io.read_bytes(self.len_case__144_elt_field0)
            _io__raw_case__144_elt_field0 = KaitaiStream(BytesIO(self._raw_case__144_elt_field0))
            self.case__144_elt_field0 = Id015PtlimaptOperationProtocolData.Case144EltField0(_io__raw_case__144_elt_field0, self, self._root)


    class Case47Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__47_field1_elt_field0 = self._io.read_u1()
            self.case__47_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case14(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__14_field0 = self._io.read_s4be()
            self.case__14_field1 = []
            for i in range(3):
                self.case__14_field1.append(Id015PtlimaptOperationProtocolData.Case14Field1Entries(self._io, self, self._root))



    class Case218Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__218_field1 = self._io.read_u1()
            if not self.len_case__218_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__218_field1, self._io, u"/types/case__218_field1_0/seq/0")
            self._raw_case__218_field1 = self._io.read_bytes(self.len_case__218_field1)
            _io__raw_case__218_field1 = KaitaiStream(BytesIO(self._raw_case__218_field1))
            self.case__218_field1 = Id015PtlimaptOperationProtocolData.Case218Field1(_io__raw_case__218_field1, self, self._root)


    class Case169EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__169_elt_field0 = self._io.read_bytes_full()


    class Case168EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__168_elt_field0 = self._io.read_u1()
            if not self.len_case__168_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__168_elt_field0, self._io, u"/types/case__168_elt_field0_0/seq/0")
            self._raw_case__168_elt_field0 = self._io.read_bytes(self.len_case__168_elt_field0)
            _io__raw_case__168_elt_field0 = KaitaiStream(BytesIO(self._raw_case__168_elt_field0))
            self.case__168_elt_field0 = Id015PtlimaptOperationProtocolData.Case168EltField0(_io__raw_case__168_elt_field0, self, self._root)


    class Case140Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__140_elt_field0 = Id015PtlimaptOperationProtocolData.Case140EltField00(self._io, self, self._root)
            self.case__140_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case182Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__182_elt_field0 = Id015PtlimaptOperationProtocolData.Case182EltField00(self._io, self, self._root)
            self.case__182_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id015PtlimaptOperationProtocolData.Id015PtlimaptEntrypoint(self._io, self, self._root)
            self.value = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Some(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents = Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.ty = Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.ticketer = Id015PtlimaptOperationProtocolData.Id015PtlimaptContractId(self._io, self, self._root)


    class Case25(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__25_field0 = self._io.read_u2be()
            self.case__25_field1 = []
            for i in range(6):
                self.case__25_field1.append(Id015PtlimaptOperationProtocolData.Case25Field1Entries(self._io, self, self._root))



    class Case4Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__4_field1_elt_field0 = self._io.read_u1()
            self.case__4_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Messages0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_messages = self._io.read_u4be()
            if not self.len_messages <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_messages, self._io, u"/types/messages_0/seq/0")
            self._raw_messages = self._io.read_bytes(self.len_messages)
            _io__raw_messages = KaitaiStream(BytesIO(self._raw_messages))
            self.messages = Id015PtlimaptOperationProtocolData.Messages(_io__raw_messages, self, self._root)


    class Case171Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__171_elt_field0 = Id015PtlimaptOperationProtocolData.Case171EltField00(self._io, self, self._root)
            self.case__171_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptOperationProtocolData.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id015PtlimaptOperationProtocolData.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Case1920(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__192 = self._io.read_u1()
            if not self.len_case__192 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__192, self._io, u"/types/case__192_0/seq/0")
            self._raw_case__192 = self._io.read_bytes(self.len_case__192)
            _io__raw_case__192 = KaitaiStream(BytesIO(self._raw_case__192))
            self.case__192 = Id015PtlimaptOperationProtocolData.Case192(_io__raw_case__192, self, self._root)


    class Proof0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proof_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.ProofTag, self._io.read_u1())
            if self.proof_tag == Id015PtlimaptOperationProtocolData.ProofTag.case__0:
                self.case__0 = Id015PtlimaptOperationProtocolData.Case00(self._io, self, self._root)

            if self.proof_tag == Id015PtlimaptOperationProtocolData.ProofTag.case__2:
                self.case__2 = Id015PtlimaptOperationProtocolData.Case2(self._io, self, self._root)

            if self.proof_tag == Id015PtlimaptOperationProtocolData.ProofTag.case__1:
                self.case__1 = Id015PtlimaptOperationProtocolData.Case1(self._io, self, self._root)

            if self.proof_tag == Id015PtlimaptOperationProtocolData.ProofTag.case__3:
                self.case__3 = Id015PtlimaptOperationProtocolData.Case3(self._io, self, self._root)



    class Case146Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__146_elt_field0 = Id015PtlimaptOperationProtocolData.Case146EltField00(self._io, self, self._root)
            self.case__146_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case167EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__167_elt_field0 = self._io.read_u1()
            if not self.len_case__167_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__167_elt_field0, self._io, u"/types/case__167_elt_field0_0/seq/0")
            self._raw_case__167_elt_field0 = self._io.read_bytes(self.len_case__167_elt_field0)
            _io__raw_case__167_elt_field0 = KaitaiStream(BytesIO(self._raw_case__167_elt_field0))
            self.case__167_elt_field0 = Id015PtlimaptOperationProtocolData.Case167EltField0(_io__raw_case__167_elt_field0, self, self._root)


    class Case164EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__164_elt_field0 = self._io.read_u1()
            if not self.len_case__164_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__164_elt_field0, self._io, u"/types/case__164_elt_field0_0/seq/0")
            self._raw_case__164_elt_field0 = self._io.read_bytes(self.len_case__164_elt_field0)
            _io__raw_case__164_elt_field0 = KaitaiStream(BytesIO(self._raw_case__164_elt_field0))
            self.case__164_elt_field0 = Id015PtlimaptOperationProtocolData.Case164EltField0(_io__raw_case__164_elt_field0, self, self._root)


    class Proposals1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposals = Id015PtlimaptOperationProtocolData.Proposals0(self._io, self, self._root)


    class TxRollupSubmitBatch(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationProtocolData.Id015PtlimaptTxRollupId(self._io, self, self._root)
            self.content = Id015PtlimaptOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.burn_limit_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Bool, self._io.read_u1())
            if self.burn_limit_tag == Id015PtlimaptOperationProtocolData.Bool.true:
                self.burn_limit = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)



    class Case64(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__64_field0 = self._io.read_u1()
            self.case__64_field1 = []
            for i in range(32):
                self.case__64_field1.append(Id015PtlimaptOperationProtocolData.Case64Field1Entries(self._io, self, self._root))



    class Case188EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__188_elt_field0 = self._io.read_bytes_full()


    class Case33Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__33_field1_elt_field0 = self._io.read_u1()
            self.case__33_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Case183EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__183_elt_field0 = self._io.read_u1()
            if not self.len_case__183_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__183_elt_field0, self._io, u"/types/case__183_elt_field0_0/seq/0")
            self._raw_case__183_elt_field0 = self._io.read_bytes(self.len_case__183_elt_field0)
            _io__raw_case__183_elt_field0 = KaitaiStream(BytesIO(self._raw_case__183_elt_field0))
            self.case__183_elt_field0 = Id015PtlimaptOperationProtocolData.Case183EltField0(_io__raw_case__183_elt_field0, self, self._root)


    class Case151EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__151_elt_field0 = self._io.read_u1()
            if not self.len_case__151_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__151_elt_field0, self._io, u"/types/case__151_elt_field0_0/seq/0")
            self._raw_case__151_elt_field0 = self._io.read_bytes(self.len_case__151_elt_field0)
            _io__raw_case__151_elt_field0 = KaitaiStream(BytesIO(self._raw_case__151_elt_field0))
            self.case__151_elt_field0 = Id015PtlimaptOperationProtocolData.Case151EltField0(_io__raw_case__151_elt_field0, self, self._root)


    class DissectionEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.state_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Bool, self._io.read_u1())
            if self.state_tag == Id015PtlimaptOperationProtocolData.Bool.true:
                self.state = self._io.read_bytes(32)

            self.tick = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)


    class Case130EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__130_elt_field0 = self._io.read_u1()
            if not self.len_case__130_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__130_elt_field0, self._io, u"/types/case__130_elt_field0_0/seq/0")
            self._raw_case__130_elt_field0 = self._io.read_bytes(self.len_case__130_elt_field0)
            _io__raw_case__130_elt_field0 = KaitaiStream(BytesIO(self._raw_case__130_elt_field0))
            self.case__130_elt_field0 = Id015PtlimaptOperationProtocolData.Case130EltField0(_io__raw_case__130_elt_field0, self, self._root)


    class TicketsInfo(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.tickets_info_entries = []
            i = 0
            while not self._io.is_eof():
                self.tickets_info_entries.append(Id015PtlimaptOperationProtocolData.TicketsInfoEntries(self._io, self, self._root))
                i += 1



    class Case141EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__141_elt_field0 = self._io.read_bytes_full()


    class Case166EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__166_elt_field0 = self._io.read_u1()
            if not self.len_case__166_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__166_elt_field0, self._io, u"/types/case__166_elt_field0_0/seq/0")
            self._raw_case__166_elt_field0 = self._io.read_bytes(self.len_case__166_elt_field0)
            _io__raw_case__166_elt_field0 = KaitaiStream(BytesIO(self._raw_case__166_elt_field0))
            self.case__166_elt_field0 = Id015PtlimaptOperationProtocolData.Case166EltField0(_io__raw_case__166_elt_field0, self, self._root)


    class Case182EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__182_elt_field0 = self._io.read_bytes_full()


    class Case42(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__42_field0 = self._io.read_s4be()
            self.case__42_field1 = []
            for i in range(10):
                self.case__42_field1.append(Id015PtlimaptOperationProtocolData.Case42Field1Entries(self._io, self, self._root))



    class Case148Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__148_elt_field0 = Id015PtlimaptOperationProtocolData.Case148EltField00(self._io, self, self._root)
            self.case__148_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Ballot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposal = self._io.read_bytes(32)
            self.ballot = self._io.read_s1()


    class Case165EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__165_elt_field0 = self._io.read_u1()
            if not self.len_case__165_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__165_elt_field0, self._io, u"/types/case__165_elt_field0_0/seq/0")
            self._raw_case__165_elt_field0 = self._io.read_bytes(self.len_case__165_elt_field0)
            _io__raw_case__165_elt_field0 = KaitaiStream(BytesIO(self._raw_case__165_elt_field0))
            self.case__165_elt_field0 = Id015PtlimaptOperationProtocolData.Case165EltField0(_io__raw_case__165_elt_field0, self, self._root)


    class Predecessor(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.predecessor_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.PredecessorTag, self._io.read_u1())
            if self.predecessor_tag == Id015PtlimaptOperationProtocolData.PredecessorTag.some:
                self.some = self._io.read_bytes(32)



    class DalPublishSlotHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.slot = Id015PtlimaptOperationProtocolData.Slot(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id015PtlimaptOperationProtocolData.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id015PtlimaptOperationProtocolData.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id015PtlimaptOperationProtocolData.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class MessagePath0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message_path = self._io.read_u4be()
            if not self.len_message_path <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message_path, self._io, u"/types/message_path_0/seq/0")
            self._raw_message_path = self._io.read_bytes(self.len_message_path)
            _io__raw_message_path = KaitaiStream(BytesIO(self._raw_message_path))
            self.message_path = Id015PtlimaptOperationProtocolData.MessagePath(_io__raw_message_path, self, self._root)


    class Case154Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__154_elt_field0 = Id015PtlimaptOperationProtocolData.Case154EltField00(self._io, self, self._root)
            self.case__154_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Case18(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__18_field0 = self._io.read_s4be()
            self.case__18_field1 = []
            for i in range(4):
                self.case__18_field1.append(Id015PtlimaptOperationProtocolData.Case18Field1Entries(self._io, self, self._root))



    class Case50(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__50_field0 = self._io.read_s4be()
            self.case__50_field1 = []
            for i in range(12):
                self.case__50_field1.append(Id015PtlimaptOperationProtocolData.Case50Field1Entries(self._io, self, self._root))



    class Op11(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__preendorsement = Id015PtlimaptOperationProtocolData.Id015PtlimaptInlinedPreendorsement(self._io, self, self._root)


    class Case171EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__171_elt_field0 = self._io.read_u1()
            if not self.len_case__171_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__171_elt_field0, self._io, u"/types/case__171_elt_field0_0/seq/0")
            self._raw_case__171_elt_field0 = self._io.read_bytes(self.len_case__171_elt_field0)
            _io__raw_case__171_elt_field0 = KaitaiStream(BytesIO(self._raw_case__171_elt_field0))
            self.case__171_elt_field0 = Id015PtlimaptOperationProtocolData.Case171EltField0(_io__raw_case__171_elt_field0, self, self._root)


    class Proposals(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proposals_entries = []
            i = 0
            while not self._io.is_eof():
                self.proposals_entries.append(Id015PtlimaptOperationProtocolData.ProposalsEntries(self._io, self, self._root))
                i += 1



    class Case146EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__146_elt_field0 = self._io.read_u1()
            if not self.len_case__146_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__146_elt_field0, self._io, u"/types/case__146_elt_field0_0/seq/0")
            self._raw_case__146_elt_field0 = self._io.read_bytes(self.len_case__146_elt_field0)
            _io__raw_case__146_elt_field0 = KaitaiStream(BytesIO(self._raw_case__146_elt_field0))
            self.case__146_elt_field0 = Id015PtlimaptOperationProtocolData.Case146EltField0(_io__raw_case__146_elt_field0, self, self._root)


    class Case61Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__61_field1_elt_field0 = self._io.read_u1()
            self.case__61_field1_elt_field1 = Id015PtlimaptOperationProtocolData.InodeTree(self._io, self, self._root)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id015PtlimaptOperationProtocolData.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1



    class Case36(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__36_field0 = self._io.read_u1()
            self.case__36_field1 = []
            for i in range(9):
                self.case__36_field1.append(Id015PtlimaptOperationProtocolData.Case36Field1Entries(self._io, self, self._root))



    class OpEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_elt_field0 = Id015PtlimaptOperationProtocolData.OpEltField0(self._io, self, self._root)
            self.op_elt_field1 = Id015PtlimaptOperationProtocolData.OpEltField1(self._io, self, self._root)


    class Case158Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__158_elt_field0 = Id015PtlimaptOperationProtocolData.Case158EltField00(self._io, self, self._root)
            self.case__158_elt_field1 = Id015PtlimaptOperationProtocolData.TreeEncoding(self._io, self, self._root)


    class Id015PtlimaptBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_hash = self._io.read_bytes(32)
            self.payload_round = self._io.read_s4be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationProtocolData.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id015PtlimaptOperationProtocolData.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.liquidity_baking_toggle_vote = Id015PtlimaptOperationProtocolData.Id015PtlimaptLiquidityBakingToggleVote(self._io, self, self._root)


    class ProposalsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol_hash = self._io.read_bytes(32)


    class Op1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__endorsement = Id015PtlimaptOperationProtocolData.Id015PtlimaptInlinedEndorsement(self._io, self, self._root)


    class ZkRollupPublish(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationProtocolData.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationProtocolData.N(self._io, self, self._root)
            self.zk_rollup = self._io.read_bytes(20)
            self.op = Id015PtlimaptOperationProtocolData.Op0(self._io, self, self._root)


    class Case134EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__134_elt_field0 = self._io.read_bytes_full()


    class Op(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_entries = []
            i = 0
            while not self._io.is_eof():
                self.op_entries.append(Id015PtlimaptOperationProtocolData.OpEntries(self._io, self, self._root))
                i += 1



    class DoublePreendorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id015PtlimaptOperationProtocolData.Op12(self._io, self, self._root)
            self.op2 = Id015PtlimaptOperationProtocolData.Op22(self._io, self, self._root)


    class Case137EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__137_elt_field0 = self._io.read_u1()
            if not self.len_case__137_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__137_elt_field0, self._io, u"/types/case__137_elt_field0_0/seq/0")
            self._raw_case__137_elt_field0 = self._io.read_bytes(self.len_case__137_elt_field0)
            _io__raw_case__137_elt_field0 = KaitaiStream(BytesIO(self._raw_case__137_elt_field0))
            self.case__137_elt_field0 = Id015PtlimaptOperationProtocolData.Case137EltField0(_io__raw_case__137_elt_field0, self, self._root)



