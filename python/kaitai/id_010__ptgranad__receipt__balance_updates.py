# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id010PtgranadReceiptBalanceUpdates(KaitaiStruct):
    """Encoding id: 010-PtGRANAD.receipt.balance_updates."""

    class Id010PtgranadContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id010PtgranadOperationMetadataAlphaBalanceTag(Enum):
        contract = 0
        rewards = 1
        fees = 2
        deposits = 3

    class OriginTag(Enum):
        block_application = 0
        protocol_migration = 1
        subsidy = 2

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_010__ptgranad__operation_metadata__alpha__balance_updates = Id010PtgranadReceiptBalanceUpdates.Id010PtgranadOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Id010PtgranadOperationMetadataAlphaBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id010PtgranadOperationMetadataAlphaUpdateOrigin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.origin = KaitaiStream.resolve_enum(Id010PtgranadReceiptBalanceUpdates.OriginTag, self._io.read_u1())


    class Id010PtgranadOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__operation_metadata__alpha__balance = Id010PtgranadReceiptBalanceUpdates.Id010PtgranadOperationMetadataAlphaBalance(self._io, self, self._root)
            self.id_010__ptgranad__operation_metadata__alpha__balance_update = Id010PtgranadReceiptBalanceUpdates.Id010PtgranadOperationMetadataAlphaBalanceUpdate(self._io, self, self._root)
            self.id_010__ptgranad__operation_metadata__alpha__update_origin = Id010PtgranadReceiptBalanceUpdates.Id010PtgranadOperationMetadataAlphaUpdateOrigin(self._io, self, self._root)


    class Rewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id010PtgranadReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id010PtgranadContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__contract_id_tag = KaitaiStream.resolve_enum(Id010PtgranadReceiptBalanceUpdates.Id010PtgranadContractIdTag, self._io.read_u1())
            if self.id_010__ptgranad__contract_id_tag == Id010PtgranadReceiptBalanceUpdates.Id010PtgranadContractIdTag.implicit:
                self.implicit = Id010PtgranadReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_010__ptgranad__contract_id_tag == Id010PtgranadReceiptBalanceUpdates.Id010PtgranadContractIdTag.originated:
                self.originated = Id010PtgranadReceiptBalanceUpdates.Originated(self._io, self, self._root)



    class Id010PtgranadOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_010__ptgranad__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_010__ptgranad__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_010__ptgranad__operation_metadata__alpha__balance_updates, self._io, u"/types/id_010__ptgranad__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_010__ptgranad__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_010__ptgranad__operation_metadata__alpha__balance_updates)
            _io__raw_id_010__ptgranad__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_010__ptgranad__operation_metadata__alpha__balance_updates))
            self.id_010__ptgranad__operation_metadata__alpha__balance_updates = Id010PtgranadReceiptBalanceUpdates.Id010PtgranadOperationMetadataAlphaBalanceUpdates(_io__raw_id_010__ptgranad__operation_metadata__alpha__balance_updates, self, self._root)


    class Deposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id010PtgranadReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Fees(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id010PtgranadReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id010PtgranadOperationMetadataAlphaBalance(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__operation_metadata__alpha__balance_tag = KaitaiStream.resolve_enum(Id010PtgranadReceiptBalanceUpdates.Id010PtgranadOperationMetadataAlphaBalanceTag, self._io.read_u1())
            if self.id_010__ptgranad__operation_metadata__alpha__balance_tag == Id010PtgranadReceiptBalanceUpdates.Id010PtgranadOperationMetadataAlphaBalanceTag.contract:
                self.contract = Id010PtgranadReceiptBalanceUpdates.Id010PtgranadContractId(self._io, self, self._root)

            if self.id_010__ptgranad__operation_metadata__alpha__balance_tag == Id010PtgranadReceiptBalanceUpdates.Id010PtgranadOperationMetadataAlphaBalanceTag.rewards:
                self.rewards = Id010PtgranadReceiptBalanceUpdates.Rewards(self._io, self, self._root)

            if self.id_010__ptgranad__operation_metadata__alpha__balance_tag == Id010PtgranadReceiptBalanceUpdates.Id010PtgranadOperationMetadataAlphaBalanceTag.fees:
                self.fees = Id010PtgranadReceiptBalanceUpdates.Fees(self._io, self, self._root)

            if self.id_010__ptgranad__operation_metadata__alpha__balance_tag == Id010PtgranadReceiptBalanceUpdates.Id010PtgranadOperationMetadataAlphaBalanceTag.deposits:
                self.deposits = Id010PtgranadReceiptBalanceUpdates.Deposits(self._io, self, self._root)



    class Id010PtgranadOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_010__ptgranad__operation_metadata__alpha__balance_updates_entries.append(Id010PtgranadReceiptBalanceUpdates.Id010PtgranadOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id010PtgranadReceiptBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id010PtgranadReceiptBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id010PtgranadReceiptBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id010PtgranadReceiptBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




