# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import distributed_db_version__name
from kaitai import p2p_version
from kaitai import distributed_db_version
class NetworkVersion(KaitaiStruct):
    """Encoding id: network_version
    Description: A version number for the network protocol (includes distributed DB version and p2p version)."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.chain_name = distributed_db_version__name.DistributedDbVersionName(self._io)
        self.distributed_db_version = distributed_db_version.DistributedDbVersion(self._io)
        self.p2p_version = p2p_version.P2pVersion(self._io)


