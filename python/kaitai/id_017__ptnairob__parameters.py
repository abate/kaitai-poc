# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id017PtnairobParameters(KaitaiStruct):
    """Encoding id: 017-PtNairob.parameters."""

    class Bool(Enum):
        false = 0
        true = 255

    class BootstrapAccountsEltTag(Enum):
        public_key_known = 0
        public_key_unknown = 1
        public_key_known_with_delegate = 2
        public_key_unknown_with_delegate = 3
        public_key_known_with_consensus_key = 4

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.bootstrap_accounts = Id017PtnairobParameters.BootstrapAccounts0(self._io, self, self._root)
        self.bootstrap_contracts = Id017PtnairobParameters.BootstrapContracts0(self._io, self, self._root)
        self.commitments = Id017PtnairobParameters.Commitments0(self._io, self, self._root)
        self.security_deposit_ramp_up_cycles_tag = KaitaiStream.resolve_enum(Id017PtnairobParameters.Bool, self._io.read_u1())
        if self.security_deposit_ramp_up_cycles_tag == Id017PtnairobParameters.Bool.true:
            self.security_deposit_ramp_up_cycles = Id017PtnairobParameters.Int31(self._io, self, self._root)

        self.no_reward_cycles_tag = KaitaiStream.resolve_enum(Id017PtnairobParameters.Bool, self._io.read_u1())
        if self.no_reward_cycles_tag == Id017PtnairobParameters.Bool.true:
            self.no_reward_cycles = Id017PtnairobParameters.Int31(self._io, self, self._root)

        self.preserved_cycles = self._io.read_u1()
        self.blocks_per_cycle = self._io.read_s4be()
        self.blocks_per_commitment = self._io.read_s4be()
        self.nonce_revelation_threshold = self._io.read_s4be()
        self.blocks_per_stake_snapshot = self._io.read_s4be()
        self.cycles_per_voting_period = self._io.read_s4be()
        self.hard_gas_limit_per_operation = Id017PtnairobParameters.Z(self._io, self, self._root)
        self.hard_gas_limit_per_block = Id017PtnairobParameters.Z(self._io, self, self._root)
        self.proof_of_work_threshold = self._io.read_s8be()
        self.minimal_stake = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
        self.vdf_difficulty = self._io.read_s8be()
        self.seed_nonce_revelation_tip = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
        self.origination_size = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.baking_reward_fixed_portion = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
        self.baking_reward_bonus_per_slot = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
        self.endorsing_reward_per_slot = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
        self.cost_per_byte = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
        self.hard_storage_limit_per_operation = Id017PtnairobParameters.Z(self._io, self, self._root)
        self.quorum_min = self._io.read_s4be()
        self.quorum_max = self._io.read_s4be()
        self.min_proposal_quorum = self._io.read_s4be()
        self.liquidity_baking_subsidy = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
        self.liquidity_baking_toggle_ema_threshold = self._io.read_s4be()
        self.max_operations_time_to_live = self._io.read_s2be()
        self.minimal_block_delay = self._io.read_s8be()
        self.delay_increment_per_round = self._io.read_s8be()
        self.consensus_committee_size = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.consensus_threshold = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.minimal_participation_ratio = Id017PtnairobParameters.MinimalParticipationRatio(self._io, self, self._root)
        self.max_slashing_period = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.frozen_deposits_percentage = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.double_baking_punishment = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
        self.ratio_of_frozen_deposits_slashed_per_double_endorsement = Id017PtnairobParameters.RatioOfFrozenDepositsSlashedPerDoubleEndorsement(self._io, self, self._root)
        self.testnet_dictator_tag = KaitaiStream.resolve_enum(Id017PtnairobParameters.Bool, self._io.read_u1())
        if self.testnet_dictator_tag == Id017PtnairobParameters.Bool.true:
            self.testnet_dictator = Id017PtnairobParameters.PublicKeyHash(self._io, self, self._root)

        self.initial_seed_tag = KaitaiStream.resolve_enum(Id017PtnairobParameters.Bool, self._io.read_u1())
        if self.initial_seed_tag == Id017PtnairobParameters.Bool.true:
            self.initial_seed = self._io.read_bytes(32)

        self.cache_script_size = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.cache_stake_distribution_cycles = self._io.read_s1()
        self.cache_sampler_state_cycles = self._io.read_s1()
        self.tx_rollup_enable = KaitaiStream.resolve_enum(Id017PtnairobParameters.Bool, self._io.read_u1())
        self.tx_rollup_origination_size = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.tx_rollup_hard_size_limit_per_inbox = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.tx_rollup_hard_size_limit_per_message = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_withdrawals_per_batch = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.tx_rollup_commitment_bond = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
        self.tx_rollup_finality_period = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.tx_rollup_withdraw_period = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_inboxes_count = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_messages_per_inbox = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_commitments_count = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.tx_rollup_cost_per_byte_ema_factor = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_ticket_payload_size = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.tx_rollup_rejection_max_proof_size = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.tx_rollup_sunset_level = self._io.read_s4be()
        self.dal_parametric = Id017PtnairobParameters.DalParametric(self._io, self, self._root)
        self.smart_rollup_enable = KaitaiStream.resolve_enum(Id017PtnairobParameters.Bool, self._io.read_u1())
        self.smart_rollup_arith_pvm_enable = KaitaiStream.resolve_enum(Id017PtnairobParameters.Bool, self._io.read_u1())
        self.smart_rollup_origination_size = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.smart_rollup_challenge_window_in_blocks = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.smart_rollup_stake_amount = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
        self.smart_rollup_commitment_period_in_blocks = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.smart_rollup_max_lookahead_in_blocks = self._io.read_s4be()
        self.smart_rollup_max_active_outbox_levels = self._io.read_s4be()
        self.smart_rollup_max_outbox_messages_per_level = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.smart_rollup_number_of_sections_in_dissection = self._io.read_u1()
        self.smart_rollup_timeout_period_in_blocks = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.smart_rollup_max_number_of_cemented_commitments = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.smart_rollup_max_number_of_parallel_games = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.zk_rollup_enable = KaitaiStream.resolve_enum(Id017PtnairobParameters.Bool, self._io.read_u1())
        self.zk_rollup_origination_size = Id017PtnairobParameters.Int31(self._io, self, self._root)
        self.zk_rollup_min_pending_to_process = Id017PtnairobParameters.Int31(self._io, self, self._root)

    class RatioOfFrozenDepositsSlashedPerDoubleEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class PublicKeyKnown(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_known_field0 = Id017PtnairobParameters.PublicKey(self._io, self, self._root)
            self.public_key_known_field1 = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)


    class Commitments(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_entries = []
            i = 0
            while not self._io.is_eof():
                self.commitments_entries.append(Id017PtnairobParameters.CommitmentsEntries(self._io, self, self._root))
                i += 1



    class DalParametric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.feature_enable = KaitaiStream.resolve_enum(Id017PtnairobParameters.Bool, self._io.read_u1())
            self.number_of_slots = self._io.read_s2be()
            self.attestation_lag = self._io.read_s2be()
            self.attestation_threshold = self._io.read_s2be()
            self.blocks_per_epoch = self._io.read_s4be()
            self.redundancy_factor = self._io.read_u1()
            self.page_size = self._io.read_u2be()
            self.slot_size = Id017PtnairobParameters.Int31(self._io, self, self._root)
            self.number_of_shards = self._io.read_u2be()


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id017PtnairobParameters.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class CommitmentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_elt_field0 = self._io.read_bytes(20)
            self.commitments_elt_field1 = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)


    class Commitments0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_commitments = self._io.read_u4be()
            if not self.len_commitments <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_commitments, self._io, u"/types/commitments_0/seq/0")
            self._raw_commitments = self._io.read_bytes(self.len_commitments)
            _io__raw_commitments = KaitaiStream(BytesIO(self._raw_commitments))
            self.commitments = Id017PtnairobParameters.Commitments(_io__raw_commitments, self, self._root)


    class MinimalParticipationRatio(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class BootstrapAccounts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_accounts = self._io.read_u4be()
            if not self.len_bootstrap_accounts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_accounts, self._io, u"/types/bootstrap_accounts_0/seq/0")
            self._raw_bootstrap_accounts = self._io.read_bytes(self.len_bootstrap_accounts)
            _io__raw_bootstrap_accounts = KaitaiStream(BytesIO(self._raw_bootstrap_accounts))
            self.bootstrap_accounts = Id017PtnairobParameters.BootstrapAccounts(_io__raw_bootstrap_accounts, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id017PtnairobParameters.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id017PtnairobParameters.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id017PtnairobParameters.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id017PtnairobParameters.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)

            if self.public_key_tag == Id017PtnairobParameters.PublicKeyTag.bls:
                self.bls = self._io.read_bytes(48)



    class PublicKeyUnknown(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_unknown_field0 = Id017PtnairobParameters.PublicKeyHash(self._io, self, self._root)
            self.public_key_unknown_field1 = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)


    class PublicKeyUnknownWithDelegate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_unknown_with_delegate_field0 = Id017PtnairobParameters.PublicKeyHash(self._io, self, self._root)
            self.public_key_unknown_with_delegate_field1 = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
            self.public_key_unknown_with_delegate_field2 = Id017PtnairobParameters.PublicKeyHash(self._io, self, self._root)


    class BootstrapAccountsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_accounts_elt_tag = KaitaiStream.resolve_enum(Id017PtnairobParameters.BootstrapAccountsEltTag, self._io.read_u1())
            if self.bootstrap_accounts_elt_tag == Id017PtnairobParameters.BootstrapAccountsEltTag.public_key_known:
                self.public_key_known = Id017PtnairobParameters.PublicKeyKnown(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == Id017PtnairobParameters.BootstrapAccountsEltTag.public_key_unknown:
                self.public_key_unknown = Id017PtnairobParameters.PublicKeyUnknown(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == Id017PtnairobParameters.BootstrapAccountsEltTag.public_key_known_with_delegate:
                self.public_key_known_with_delegate = Id017PtnairobParameters.PublicKeyKnownWithDelegate(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == Id017PtnairobParameters.BootstrapAccountsEltTag.public_key_unknown_with_delegate:
                self.public_key_unknown_with_delegate = Id017PtnairobParameters.PublicKeyUnknownWithDelegate(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == Id017PtnairobParameters.BootstrapAccountsEltTag.public_key_known_with_consensus_key:
                self.public_key_known_with_consensus_key = Id017PtnairobParameters.PublicKeyKnownWithConsensusKey(self._io, self, self._root)



    class BootstrapContracts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_contracts = self._io.read_u4be()
            if not self.len_bootstrap_contracts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_contracts, self._io, u"/types/bootstrap_contracts_0/seq/0")
            self._raw_bootstrap_contracts = self._io.read_bytes(self.len_bootstrap_contracts)
            _io__raw_bootstrap_contracts = KaitaiStream(BytesIO(self._raw_bootstrap_contracts))
            self.bootstrap_contracts = Id017PtnairobParameters.BootstrapContracts(_io__raw_bootstrap_contracts, self, self._root)


    class Id017PtnairobScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id017PtnairobParameters.BytesDynUint30(self._io, self, self._root)
            self.storage = Id017PtnairobParameters.BytesDynUint30(self._io, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class PublicKeyKnownWithDelegate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_known_with_delegate_field0 = Id017PtnairobParameters.PublicKey(self._io, self, self._root)
            self.public_key_known_with_delegate_field1 = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
            self.public_key_known_with_delegate_field2 = Id017PtnairobParameters.PublicKeyHash(self._io, self, self._root)


    class BootstrapContractsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id017PtnairobParameters.Bool, self._io.read_u1())
            if self.delegate_tag == Id017PtnairobParameters.Bool.true:
                self.delegate = Id017PtnairobParameters.PublicKeyHash(self._io, self, self._root)

            self.amount = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
            self.script = Id017PtnairobParameters.Id017PtnairobScriptedContracts(self._io, self, self._root)


    class BootstrapContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_contracts_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_contracts_entries.append(Id017PtnairobParameters.BootstrapContractsEntries(self._io, self, self._root))
                i += 1



    class BootstrapAccounts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_accounts_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_accounts_entries.append(Id017PtnairobParameters.BootstrapAccountsEntries(self._io, self, self._root))
                i += 1



    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Id017PtnairobMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__mutez = Id017PtnairobParameters.N(self._io, self, self._root)


    class PublicKeyKnownWithConsensusKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_known_with_consensus_key_field0 = Id017PtnairobParameters.PublicKey(self._io, self, self._root)
            self.public_key_known_with_consensus_key_field1 = Id017PtnairobParameters.Id017PtnairobMutez(self._io, self, self._root)
            self.public_key_known_with_consensus_key_field2 = Id017PtnairobParameters.PublicKey(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id017PtnairobParameters.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id017PtnairobParameters.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id017PtnairobParameters.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id017PtnairobParameters.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id017PtnairobParameters.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id017PtnairobParameters.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




