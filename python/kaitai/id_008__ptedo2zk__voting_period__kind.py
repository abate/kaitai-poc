# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id008Ptedo2zkVotingPeriodKind(KaitaiStruct):
    """Encoding id: 008-PtEdo2Zk.voting_period.kind."""

    class Id008Ptedo2zkVotingPeriodKindTag(Enum):
        proposal = 0
        testing_vote = 1
        testing = 2
        promotion_vote = 3
        adoption = 4
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_008__ptedo2zk__voting_period__kind_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkVotingPeriodKind.Id008Ptedo2zkVotingPeriodKindTag, self._io.read_u1())


