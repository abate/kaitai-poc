# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import p2p_address
class P2pConnectionId(KaitaiStruct):
    """Encoding id: p2p_connection.id
    Description: The identifier for a p2p connection. It includes an address and a port number."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.addr = p2p_address.P2pAddress(self._io)
        self.port_tag = KaitaiStream.resolve_enum(P2pConnectionId.Bool, self._io.read_u1())
        if self.port_tag == P2pConnectionId.Bool.true:
            self.port = self._io.read_u2be()



