# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id019PtparisaReceiptBalanceUpdates(KaitaiStruct):
    """Encoding id: 019-PtParisA.receipt.balance_updates."""

    class Id019PtparisaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag(Enum):
        contract = 0
        block_fees = 2
        deposits = 4
        nonce_revelation_rewards = 5
        attesting_rewards = 7
        baking_rewards = 8
        baking_bonuses = 9
        storage_fees = 11
        double_signing_punishments = 12
        lost_attesting_rewards = 13
        liquidity_baking_subsidies = 14
        burned = 15
        commitments = 16
        bootstrap = 17
        invoice = 18
        initial_commitments = 19
        minted = 20
        frozen_bonds = 21
        smart_rollup_refutation_punishments = 24
        smart_rollup_refutation_rewards = 25
        unstaked_deposits = 26
        staking_delegator_numerator = 27
        staking_delegate_denominator = 28

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class Id019PtparisaStakerTag(Enum):
        single = 0
        shared = 1

    class Bool(Enum):
        false = 0
        true = 255

    class Id019PtparisaFrozenStakerTag(Enum):
        single = 0
        shared = 1
        baker = 2

    class Id019PtparisaBondIdTag(Enum):
        smart_rollup_bond_id = 1

    class Id019PtparisaOperationMetadataAlphaUpdateOriginTag(Enum):
        block_application = 0
        protocol_migration = 1
        subsidy = 2
        simulation = 3
        delayed_operation = 4
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_019__ptparisa__operation_metadata__alpha__balance_updates = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Id019PtparisaOperationMetadataAlphaBalanceAndUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag = KaitaiStream.resolve_enum(Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag, self._io.read_u1())
            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.contract:
                self.contract = Id019PtparisaReceiptBalanceUpdates.Contract(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.block_fees:
                self.block_fees = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.deposits:
                self.deposits = Id019PtparisaReceiptBalanceUpdates.Deposits(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.nonce_revelation_rewards:
                self.nonce_revelation_rewards = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.attesting_rewards:
                self.attesting_rewards = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.baking_rewards:
                self.baking_rewards = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.baking_bonuses:
                self.baking_bonuses = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.storage_fees:
                self.storage_fees = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.double_signing_punishments:
                self.double_signing_punishments = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.lost_attesting_rewards:
                self.lost_attesting_rewards = Id019PtparisaReceiptBalanceUpdates.LostAttestingRewards(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.liquidity_baking_subsidies:
                self.liquidity_baking_subsidies = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.burned:
                self.burned = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.commitments:
                self.commitments = Id019PtparisaReceiptBalanceUpdates.Commitments(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.bootstrap:
                self.bootstrap = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.invoice:
                self.invoice = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.initial_commitments:
                self.initial_commitments = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.minted:
                self.minted = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.frozen_bonds:
                self.frozen_bonds = Id019PtparisaReceiptBalanceUpdates.FrozenBonds(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.smart_rollup_refutation_punishments:
                self.smart_rollup_refutation_punishments = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.smart_rollup_refutation_rewards:
                self.smart_rollup_refutation_rewards = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.unstaked_deposits:
                self.unstaked_deposits = Id019PtparisaReceiptBalanceUpdates.UnstakedDeposits(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.staking_delegator_numerator:
                self.staking_delegator_numerator = Id019PtparisaReceiptBalanceUpdates.StakingDelegatorNumerator(self._io, self, self._root)

            if self.id_019__ptparisa__operation_metadata__alpha__balance_and_update_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdateTag.staking_delegate_denominator:
                self.staking_delegate_denominator = Id019PtparisaReceiptBalanceUpdates.StakingDelegateDenominator(self._io, self, self._root)



    class Id019PtparisaStaker(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__staker_tag = KaitaiStream.resolve_enum(Id019PtparisaReceiptBalanceUpdates.Id019PtparisaStakerTag, self._io.read_u1())
            if self.id_019__ptparisa__staker_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaStakerTag.single:
                self.single = Id019PtparisaReceiptBalanceUpdates.Single(self._io, self, self._root)

            if self.id_019__ptparisa__staker_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaStakerTag.shared:
                self.shared = Id019PtparisaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)



    class Id019PtparisaBondId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__bond_id_tag = KaitaiStream.resolve_enum(Id019PtparisaReceiptBalanceUpdates.Id019PtparisaBondIdTag, self._io.read_u1())
            if self.id_019__ptparisa__bond_id_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaBondIdTag.smart_rollup_bond_id:
                self.smart_rollup_bond_id = self._io.read_bytes(20)



    class Commitments(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.committer = self._io.read_bytes(20)
            self.id_019__ptparisa__operation_metadata__alpha__tez_balance_update = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class FrozenBonds(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaContractId(self._io, self, self._root)
            self.bond_id = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaBondId(self._io, self, self._root)
            self.id_019__ptparisa__operation_metadata__alpha__tez_balance_update = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id019PtparisaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__contract_id_tag = KaitaiStream.resolve_enum(Id019PtparisaReceiptBalanceUpdates.Id019PtparisaContractIdTag, self._io.read_u1())
            if self.id_019__ptparisa__contract_id_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaContractIdTag.implicit:
                self.implicit = Id019PtparisaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_019__ptparisa__contract_id_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaContractIdTag.originated:
                self.originated = Id019PtparisaReceiptBalanceUpdates.Originated(self._io, self, self._root)



    class Id019PtparisaOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_019__ptparisa__operation_metadata__alpha__balance_updates_entries.append(Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class StakingDelegateDenominator(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id019PtparisaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.id_019__ptparisa__operation_metadata__alpha__staking_abstract_quantity = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaStakingAbstractQuantity(self._io, self, self._root)


    class Id019PtparisaOperationMetadataAlphaUpdateOrigin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__operation_metadata__alpha__update_origin_tag = KaitaiStream.resolve_enum(Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaUpdateOriginTag, self._io.read_u1())
            if self.id_019__ptparisa__operation_metadata__alpha__update_origin_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaUpdateOriginTag.delayed_operation:
                self.delayed_operation = self._io.read_bytes(32)



    class Single(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaContractId(self._io, self, self._root)
            self.delegate = Id019PtparisaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)


    class Deposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.staker = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaFrozenStaker(self._io, self, self._root)
            self.id_019__ptparisa__operation_metadata__alpha__tez_balance_update = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class Id019PtparisaFrozenStaker(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__frozen_staker_tag = KaitaiStream.resolve_enum(Id019PtparisaReceiptBalanceUpdates.Id019PtparisaFrozenStakerTag, self._io.read_u1())
            if self.id_019__ptparisa__frozen_staker_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaFrozenStakerTag.single:
                self.single = Id019PtparisaReceiptBalanceUpdates.Single(self._io, self, self._root)

            if self.id_019__ptparisa__frozen_staker_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaFrozenStakerTag.shared:
                self.shared = Id019PtparisaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_019__ptparisa__frozen_staker_tag == Id019PtparisaReceiptBalanceUpdates.Id019PtparisaFrozenStakerTag.baker:
                self.baker = Id019PtparisaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)



    class Id019PtparisaOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_019__ptparisa__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_019__ptparisa__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_019__ptparisa__operation_metadata__alpha__balance_updates, self._io, u"/types/id_019__ptparisa__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_019__ptparisa__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_019__ptparisa__operation_metadata__alpha__balance_updates)
            _io__raw_id_019__ptparisa__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_019__ptparisa__operation_metadata__alpha__balance_updates))
            self.id_019__ptparisa__operation_metadata__alpha__balance_updates = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceUpdates(_io__raw_id_019__ptparisa__operation_metadata__alpha__balance_updates, self, self._root)


    class Id019PtparisaOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__operation_metadata__alpha__balance_and_update = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaBalanceAndUpdate(self._io, self, self._root)
            self.id_019__ptparisa__operation_metadata__alpha__update_origin = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaUpdateOrigin(self._io, self, self._root)


    class LostAttestingRewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id019PtparisaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.participation = KaitaiStream.resolve_enum(Id019PtparisaReceiptBalanceUpdates.Bool, self._io.read_u1())
            self.revelation = KaitaiStream.resolve_enum(Id019PtparisaReceiptBalanceUpdates.Bool, self._io.read_u1())
            self.id_019__ptparisa__operation_metadata__alpha__tez_balance_update = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class UnstakedDeposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.staker = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaStaker(self._io, self, self._root)
            self.cycle = self._io.read_s4be()
            self.id_019__ptparisa__operation_metadata__alpha__tez_balance_update = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class Id019PtparisaOperationMetadataAlphaStakingAbstractQuantity(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id019PtparisaReceiptBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id019PtparisaReceiptBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id019PtparisaReceiptBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id019PtparisaReceiptBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id019PtparisaReceiptBalanceUpdates.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class Contract(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaContractId(self._io, self, self._root)
            self.id_019__ptparisa__operation_metadata__alpha__tez_balance_update = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class StakingDelegatorNumerator(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegator = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaContractId(self._io, self, self._root)
            self.id_019__ptparisa__operation_metadata__alpha__staking_abstract_quantity = Id019PtparisaReceiptBalanceUpdates.Id019PtparisaOperationMetadataAlphaStakingAbstractQuantity(self._io, self, self._root)



