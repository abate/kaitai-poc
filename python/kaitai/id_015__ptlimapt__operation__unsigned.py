# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import operation__shell_header
from kaitai import block_header__shell
class Id015PtlimaptOperationUnsigned(KaitaiStruct):
    """Encoding id: 015-PtLimaPt.operation.unsigned."""

    class RevealProofTag(Enum):
        raw__data__proof = 0

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class PvmKind(Enum):
        arith_pvm_kind = 0
        wasm_2_0_0_pvm_kind = 1

    class Micheline015PtlimaptMichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10

    class Id015PtlimaptOperationAlphaContentsTag(Enum):
        seed_nonce_revelation = 1
        double_endorsement_evidence = 2
        double_baking_evidence = 3
        activate_account = 4
        proposals = 5
        ballot = 6
        double_preendorsement_evidence = 7
        vdf_revelation = 8
        drain_delegate = 9
        failing_noop = 17
        preendorsement = 20
        endorsement = 21
        dal_slot_availability = 22
        reveal = 107
        transaction = 108
        origination = 109
        delegation = 110
        register_global_constant = 111
        set_deposits_limit = 112
        increase_paid_storage = 113
        update_consensus_key = 114
        tx_rollup_origination = 150
        tx_rollup_submit_batch = 151
        tx_rollup_commit = 152
        tx_rollup_return_bond = 153
        tx_rollup_finalize_commitment = 154
        tx_rollup_remove_commitment = 155
        tx_rollup_rejection = 156
        tx_rollup_dispatch_tickets = 157
        transfer_ticket = 158
        sc_rollup_originate = 200
        sc_rollup_add_messages = 201
        sc_rollup_cement = 202
        sc_rollup_publish = 203
        sc_rollup_refute = 204
        sc_rollup_timeout = 205
        sc_rollup_execute_outbox_message = 206
        sc_rollup_recover_bond = 207
        sc_rollup_dal_slot_subscribe = 208
        dal_publish_slot_header = 230
        zk_rollup_origination = 250
        zk_rollup_publish = 251

    class PvmStepTag(Enum):
        arithmetic__pvm__with__proof = 0
        wasm__2__0__0__pvm__with__proof = 1
        unencodable = 255

    class Id015PtlimaptInlinedEndorsementMempoolContentsTag(Enum):
        endorsement = 21

    class ProofTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3

    class TreeEncodingTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__7 = 7
        case__8 = 8
        case__9 = 9
        case__10 = 10
        case__11 = 11
        case__12 = 12
        case__13 = 13
        case__14 = 14
        case__15 = 15
        case__16 = 16
        case__17 = 17
        case__18 = 18
        case__19 = 19
        case__20 = 20
        case__21 = 21
        case__22 = 22
        case__23 = 23
        case__24 = 24
        case__25 = 25
        case__26 = 26
        case__27 = 27
        case__28 = 28
        case__29 = 29
        case__30 = 30
        case__31 = 31
        case__32 = 32
        case__33 = 33
        case__34 = 34
        case__35 = 35
        case__36 = 36
        case__37 = 37
        case__38 = 38
        case__39 = 39
        case__40 = 40
        case__41 = 41
        case__42 = 42
        case__43 = 43
        case__44 = 44
        case__45 = 45
        case__46 = 46
        case__47 = 47
        case__48 = 48
        case__49 = 49
        case__50 = 50
        case__51 = 51
        case__52 = 52
        case__53 = 53
        case__54 = 54
        case__55 = 55
        case__56 = 56
        case__57 = 57
        case__58 = 58
        case__59 = 59
        case__60 = 60
        case__61 = 61
        case__62 = 62
        case__63 = 63
        case__64 = 64
        case__65 = 65
        case__66 = 66
        case__67 = 67
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__132 = 132
        case__133 = 133
        case__134 = 134
        case__135 = 135
        case__136 = 136
        case__137 = 137
        case__138 = 138
        case__139 = 139
        case__140 = 140
        case__141 = 141
        case__142 = 142
        case__143 = 143
        case__144 = 144
        case__145 = 145
        case__146 = 146
        case__147 = 147
        case__148 = 148
        case__149 = 149
        case__150 = 150
        case__151 = 151
        case__152 = 152
        case__153 = 153
        case__154 = 154
        case__155 = 155
        case__156 = 156
        case__157 = 157
        case__158 = 158
        case__159 = 159
        case__160 = 160
        case__161 = 161
        case__162 = 162
        case__163 = 163
        case__164 = 164
        case__165 = 165
        case__166 = 166
        case__167 = 167
        case__168 = 168
        case__169 = 169
        case__170 = 170
        case__171 = 171
        case__172 = 172
        case__173 = 173
        case__174 = 174
        case__175 = 175
        case__176 = 176
        case__177 = 177
        case__178 = 178
        case__179 = 179
        case__180 = 180
        case__181 = 181
        case__182 = 182
        case__183 = 183
        case__184 = 184
        case__185 = 185
        case__186 = 186
        case__187 = 187
        case__188 = 188
        case__189 = 189
        case__190 = 190
        case__191 = 191
        case__192 = 192
        case__193 = 193
        case__195 = 195
        case__200 = 200
        case__208 = 208
        case__216 = 216
        case__217 = 217
        case__218 = 218
        case__219 = 219
        case__224 = 224

    class OpEltField1Tag(Enum):
        none = 0
        some = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id015PtlimaptEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        deposit = 5
        named = 255

    class Id015PtlimaptInlinedPreendorsementContentsTag(Enum):
        preendorsement = 20

    class PredecessorTag(Enum):
        none = 0
        some = 1

    class Id015PtlimaptContractIdTag(Enum):
        implicit = 0
        originated = 1

    class AmountTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3

    class Id015PtlimaptMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left = 5
        none_0 = 6
        pair_1 = 7
        right = 8
        some_0 = 9
        true = 10
        unit_0 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left_0 = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right_0 = 68
        size = 69
        some = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_1 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction_deprecated = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_deprecated = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140
        chest = 141
        chest_key = 142
        open_chest = 143
        view_0 = 144
        view = 145
        constant = 146
        sub_mutez = 147
        tx_rollup_l2_address = 148
        min_block_time = 149
        sapling_transaction = 150
        emit = 151
        lambda_rec = 152
        lambda_rec_0 = 153
        ticket_0 = 154

    class InodeTreeTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__7 = 7
        case__8 = 8
        case__9 = 9
        case__10 = 10
        case__11 = 11
        case__12 = 12
        case__13 = 13
        case__14 = 14
        case__15 = 15
        case__16 = 16
        case__17 = 17
        case__18 = 18
        case__19 = 19
        case__20 = 20
        case__21 = 21
        case__22 = 22
        case__23 = 23
        case__24 = 24
        case__25 = 25
        case__26 = 26
        case__27 = 27
        case__28 = 28
        case__29 = 29
        case__30 = 30
        case__31 = 31
        case__32 = 32
        case__33 = 33
        case__34 = 34
        case__35 = 35
        case__36 = 36
        case__37 = 37
        case__38 = 38
        case__39 = 39
        case__40 = 40
        case__41 = 41
        case__42 = 42
        case__43 = 43
        case__44 = 44
        case__45 = 45
        case__46 = 46
        case__47 = 47
        case__48 = 48
        case__49 = 49
        case__50 = 50
        case__51 = 51
        case__52 = 52
        case__53 = 53
        case__54 = 54
        case__55 = 55
        case__56 = 56
        case__57 = 57
        case__58 = 58
        case__59 = 59
        case__60 = 60
        case__61 = 61
        case__62 = 62
        case__63 = 63
        case__64 = 64
        case__65 = 65
        case__66 = 66
        case__67 = 67
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__132 = 132
        case__133 = 133
        case__134 = 134
        case__135 = 135
        case__136 = 136
        case__137 = 137
        case__138 = 138
        case__139 = 139
        case__140 = 140
        case__141 = 141
        case__142 = 142
        case__143 = 143
        case__144 = 144
        case__145 = 145
        case__146 = 146
        case__147 = 147
        case__148 = 148
        case__149 = 149
        case__150 = 150
        case__151 = 151
        case__152 = 152
        case__153 = 153
        case__154 = 154
        case__155 = 155
        case__156 = 156
        case__157 = 157
        case__158 = 158
        case__159 = 159
        case__160 = 160
        case__161 = 161
        case__162 = 162
        case__163 = 163
        case__164 = 164
        case__165 = 165
        case__166 = 166
        case__167 = 167
        case__168 = 168
        case__169 = 169
        case__170 = 170
        case__171 = 171
        case__172 = 172
        case__173 = 173
        case__174 = 174
        case__175 = 175
        case__176 = 176
        case__177 = 177
        case__178 = 178
        case__179 = 179
        case__180 = 180
        case__181 = 181
        case__182 = 182
        case__183 = 183
        case__184 = 184
        case__185 = 185
        case__186 = 186
        case__187 = 187
        case__188 = 188
        case__189 = 189
        case__190 = 190
        case__191 = 191
        case__192 = 192
        case__208 = 208
        case__209 = 209
        case__210 = 210
        case__211 = 211
        case__224 = 224

    class StepTag(Enum):
        dissection = 0
        proof = 1

    class MessageTag(Enum):
        batch = 0
        deposit = 1

    class Id015PtlimaptContractIdOriginatedTag(Enum):
        originated = 1

    class InputProofTag(Enum):
        inbox__proof = 0
        reveal__proof = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_015__ptlimapt__operation__alpha__unsigned_operation = Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaUnsignedOperation(self._io, self, self._root)

    class Case131EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131_elt_field0 = self._io.read_u1()
            if not self.len_case__131_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__131_elt_field0, self._io, u"/types/case__131_elt_field0_0/seq/0")
            self._raw_case__131_elt_field0 = self._io.read_bytes(self.len_case__131_elt_field0)
            _io__raw_case__131_elt_field0 = KaitaiStream(BytesIO(self._raw_case__131_elt_field0))
            self.case__131_elt_field0 = Id015PtlimaptOperationUnsigned.Case131EltField0(_io__raw_case__131_elt_field0, self, self._root)


    class Case24(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__24_field0 = self._io.read_u1()
            self.case__24_field1 = []
            for i in range(6):
                self.case__24_field1.append(Id015PtlimaptOperationUnsigned.Case24Field1Entries(self._io, self, self._root))



    class Case189Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__189_elt_field0 = Id015PtlimaptOperationUnsigned.Case189EltField00(self._io, self, self._root)
            self.case__189_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case211Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__211_field1 = self._io.read_bytes_full()


    class Case23Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__23_field1_elt_field0 = self._io.read_u1()
            self.case__23_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case138EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__138_elt_field0 = self._io.read_bytes_full()


    class Id015PtlimaptInlinedPreendorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__preendorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id015PtlimaptOperationUnsigned.Id015PtlimaptInlinedPreendorsementContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Bool, self._io.read_u1())
            if self.signature_tag == Id015PtlimaptOperationUnsigned.Bool.true:
                self.signature = self._io.read_bytes(64)



    class Op20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_0/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id015PtlimaptOperationUnsigned.Op2(_io__raw_op2, self, self._root)


    class Case177Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__177_elt_field0 = Id015PtlimaptOperationUnsigned.Case177EltField00(self._io, self, self._root)
            self.case__177_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__1_field0 = self._io.read_s2be()
            self.case__1_field1 = self._io.read_bytes(32)
            self.case__1_field2 = self._io.read_bytes(32)
            self.case__1_field3 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case42Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__42_field1_elt_field0 = self._io.read_u1()
            self.case__42_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case163Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__163_elt_field0 = Id015PtlimaptOperationUnsigned.Case163EltField00(self._io, self, self._root)
            self.case__163_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case192(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__192 = self._io.read_bytes_full()


    class Case183Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__183_elt_field0 = Id015PtlimaptOperationUnsigned.Case183EltField00(self._io, self, self._root)
            self.case__183_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case55Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__55_field1_elt_field0 = self._io.read_u1()
            self.case__55_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Messages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.messages_entries = []
            i = 0
            while not self._io.is_eof():
                self.messages_entries.append(Id015PtlimaptOperationUnsigned.MessagesEntries(self._io, self, self._root))
                i += 1



    class Case65Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__65_field1_elt = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case211(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__211_field0 = self._io.read_s8be()
            self.case__211_field1 = Id015PtlimaptOperationUnsigned.Case211Field10(self._io, self, self._root)
            self.case__211_field2 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case43(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__43_field0 = self._io.read_s8be()
            self.case__43_field1 = []
            for i in range(10):
                self.case__43_field1.append(Id015PtlimaptOperationUnsigned.Case43Field1Entries(self._io, self, self._root))



    class Case54(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__54_field0 = self._io.read_s4be()
            self.case__54_field1 = []
            for i in range(13):
                self.case__54_field1.append(Id015PtlimaptOperationUnsigned.Case54Field1Entries(self._io, self, self._root))



    class Case138EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__138_elt_field0 = self._io.read_u1()
            if not self.len_case__138_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__138_elt_field0, self._io, u"/types/case__138_elt_field0_0/seq/0")
            self._raw_case__138_elt_field0 = self._io.read_bytes(self.len_case__138_elt_field0)
            _io__raw_case__138_elt_field0 = KaitaiStream(BytesIO(self._raw_case__138_elt_field0))
            self.case__138_elt_field0 = Id015PtlimaptOperationUnsigned.Case138EltField0(_io__raw_case__138_elt_field0, self, self._root)


    class Case134EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__134_elt_field0 = self._io.read_u1()
            if not self.len_case__134_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__134_elt_field0, self._io, u"/types/case__134_elt_field0_0/seq/0")
            self._raw_case__134_elt_field0 = self._io.read_bytes(self.len_case__134_elt_field0)
            _io__raw_case__134_elt_field0 = KaitaiStream(BytesIO(self._raw_case__134_elt_field0))
            self.case__134_elt_field0 = Id015PtlimaptOperationUnsigned.Case134EltField0(_io__raw_case__134_elt_field0, self, self._root)


    class Case32(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__32_field0 = self._io.read_u1()
            self.case__32_field1 = []
            for i in range(8):
                self.case__32_field1.append(Id015PtlimaptOperationUnsigned.Case32Field1Entries(self._io, self, self._root))



    class Case59Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__59_field1_elt_field0 = self._io.read_u1()
            self.case__59_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case65(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__65_field0 = self._io.read_u2be()
            self.case__65_field1 = []
            for i in range(32):
                self.case__65_field1.append(Id015PtlimaptOperationUnsigned.Case65Field1Entries(self._io, self, self._root))



    class Case181Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__181_elt_field0 = Id015PtlimaptOperationUnsigned.Case181EltField00(self._io, self, self._root)
            self.case__181_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Stakers(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alice = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.bob = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)


    class MessageResultPathEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_list_hash = self._io.read_bytes(32)


    class Case66Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__66_field1_elt = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class InodeTree(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inode_tree_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.InodeTreeTag, self._io.read_u1())
            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__4:
                self.case__4 = Id015PtlimaptOperationUnsigned.Case4(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__8:
                self.case__8 = Id015PtlimaptOperationUnsigned.Case8(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__12:
                self.case__12 = Id015PtlimaptOperationUnsigned.Case12(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__16:
                self.case__16 = Id015PtlimaptOperationUnsigned.Case16(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__20:
                self.case__20 = Id015PtlimaptOperationUnsigned.Case20(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__24:
                self.case__24 = Id015PtlimaptOperationUnsigned.Case24(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__28:
                self.case__28 = Id015PtlimaptOperationUnsigned.Case28(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__32:
                self.case__32 = Id015PtlimaptOperationUnsigned.Case32(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__36:
                self.case__36 = Id015PtlimaptOperationUnsigned.Case36(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__40:
                self.case__40 = Id015PtlimaptOperationUnsigned.Case40(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__44:
                self.case__44 = Id015PtlimaptOperationUnsigned.Case44(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__48:
                self.case__48 = Id015PtlimaptOperationUnsigned.Case48(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__52:
                self.case__52 = Id015PtlimaptOperationUnsigned.Case52(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__56:
                self.case__56 = Id015PtlimaptOperationUnsigned.Case56(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__60:
                self.case__60 = Id015PtlimaptOperationUnsigned.Case60(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__64:
                self.case__64 = Id015PtlimaptOperationUnsigned.Case64(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__5:
                self.case__5 = Id015PtlimaptOperationUnsigned.Case5(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__9:
                self.case__9 = Id015PtlimaptOperationUnsigned.Case9(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__13:
                self.case__13 = Id015PtlimaptOperationUnsigned.Case13(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__17:
                self.case__17 = Id015PtlimaptOperationUnsigned.Case17(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__21:
                self.case__21 = Id015PtlimaptOperationUnsigned.Case21(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__25:
                self.case__25 = Id015PtlimaptOperationUnsigned.Case25(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__29:
                self.case__29 = Id015PtlimaptOperationUnsigned.Case29(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__33:
                self.case__33 = Id015PtlimaptOperationUnsigned.Case33(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__37:
                self.case__37 = Id015PtlimaptOperationUnsigned.Case37(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__41:
                self.case__41 = Id015PtlimaptOperationUnsigned.Case41(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__45:
                self.case__45 = Id015PtlimaptOperationUnsigned.Case45(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__49:
                self.case__49 = Id015PtlimaptOperationUnsigned.Case49(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__53:
                self.case__53 = Id015PtlimaptOperationUnsigned.Case53(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__57:
                self.case__57 = Id015PtlimaptOperationUnsigned.Case57(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__61:
                self.case__61 = Id015PtlimaptOperationUnsigned.Case61(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__65:
                self.case__65 = Id015PtlimaptOperationUnsigned.Case65(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__6:
                self.case__6 = Id015PtlimaptOperationUnsigned.Case6(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__10:
                self.case__10 = Id015PtlimaptOperationUnsigned.Case10(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__14:
                self.case__14 = Id015PtlimaptOperationUnsigned.Case14(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__18:
                self.case__18 = Id015PtlimaptOperationUnsigned.Case18(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__22:
                self.case__22 = Id015PtlimaptOperationUnsigned.Case22(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__26:
                self.case__26 = Id015PtlimaptOperationUnsigned.Case26(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__30:
                self.case__30 = Id015PtlimaptOperationUnsigned.Case30(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__34:
                self.case__34 = Id015PtlimaptOperationUnsigned.Case34(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__38:
                self.case__38 = Id015PtlimaptOperationUnsigned.Case38(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__42:
                self.case__42 = Id015PtlimaptOperationUnsigned.Case42(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__46:
                self.case__46 = Id015PtlimaptOperationUnsigned.Case46(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__50:
                self.case__50 = Id015PtlimaptOperationUnsigned.Case50(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__54:
                self.case__54 = Id015PtlimaptOperationUnsigned.Case54(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__58:
                self.case__58 = Id015PtlimaptOperationUnsigned.Case58(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__62:
                self.case__62 = Id015PtlimaptOperationUnsigned.Case62(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__66:
                self.case__66 = Id015PtlimaptOperationUnsigned.Case66(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__3:
                self.case__3 = self._io.read_s8be()

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__7:
                self.case__7 = Id015PtlimaptOperationUnsigned.Case7(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__11:
                self.case__11 = Id015PtlimaptOperationUnsigned.Case11(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__15:
                self.case__15 = Id015PtlimaptOperationUnsigned.Case15(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__19:
                self.case__19 = Id015PtlimaptOperationUnsigned.Case19(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__23:
                self.case__23 = Id015PtlimaptOperationUnsigned.Case23(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__27:
                self.case__27 = Id015PtlimaptOperationUnsigned.Case27(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__31:
                self.case__31 = Id015PtlimaptOperationUnsigned.Case31(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__35:
                self.case__35 = Id015PtlimaptOperationUnsigned.Case35(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__39:
                self.case__39 = Id015PtlimaptOperationUnsigned.Case39(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__43:
                self.case__43 = Id015PtlimaptOperationUnsigned.Case43(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__47:
                self.case__47 = Id015PtlimaptOperationUnsigned.Case47(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__51:
                self.case__51 = Id015PtlimaptOperationUnsigned.Case51(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__55:
                self.case__55 = Id015PtlimaptOperationUnsigned.Case55(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__59:
                self.case__59 = Id015PtlimaptOperationUnsigned.Case59(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__63:
                self.case__63 = Id015PtlimaptOperationUnsigned.Case63(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__67:
                self.case__67 = Id015PtlimaptOperationUnsigned.Case67(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__129:
                self.case__129 = Id015PtlimaptOperationUnsigned.Case129Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__130:
                self.case__130 = Id015PtlimaptOperationUnsigned.Case130Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__131:
                self.case__131 = Id015PtlimaptOperationUnsigned.Case131Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__132:
                self.case__132 = Id015PtlimaptOperationUnsigned.Case132Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__133:
                self.case__133 = Id015PtlimaptOperationUnsigned.Case133Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__134:
                self.case__134 = Id015PtlimaptOperationUnsigned.Case134Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__135:
                self.case__135 = Id015PtlimaptOperationUnsigned.Case135Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__136:
                self.case__136 = Id015PtlimaptOperationUnsigned.Case136Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__137:
                self.case__137 = Id015PtlimaptOperationUnsigned.Case137Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__138:
                self.case__138 = Id015PtlimaptOperationUnsigned.Case138Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__139:
                self.case__139 = Id015PtlimaptOperationUnsigned.Case139Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__140:
                self.case__140 = Id015PtlimaptOperationUnsigned.Case140Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__141:
                self.case__141 = Id015PtlimaptOperationUnsigned.Case141Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__142:
                self.case__142 = Id015PtlimaptOperationUnsigned.Case142Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__143:
                self.case__143 = Id015PtlimaptOperationUnsigned.Case143Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__144:
                self.case__144 = Id015PtlimaptOperationUnsigned.Case144Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__145:
                self.case__145 = Id015PtlimaptOperationUnsigned.Case145Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__146:
                self.case__146 = Id015PtlimaptOperationUnsigned.Case146Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__147:
                self.case__147 = Id015PtlimaptOperationUnsigned.Case147Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__148:
                self.case__148 = Id015PtlimaptOperationUnsigned.Case148Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__149:
                self.case__149 = Id015PtlimaptOperationUnsigned.Case149Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__150:
                self.case__150 = Id015PtlimaptOperationUnsigned.Case150Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__151:
                self.case__151 = Id015PtlimaptOperationUnsigned.Case151Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__152:
                self.case__152 = Id015PtlimaptOperationUnsigned.Case152Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__153:
                self.case__153 = Id015PtlimaptOperationUnsigned.Case153Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__154:
                self.case__154 = Id015PtlimaptOperationUnsigned.Case154Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__155:
                self.case__155 = Id015PtlimaptOperationUnsigned.Case155Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__156:
                self.case__156 = Id015PtlimaptOperationUnsigned.Case156Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__157:
                self.case__157 = Id015PtlimaptOperationUnsigned.Case157Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__158:
                self.case__158 = Id015PtlimaptOperationUnsigned.Case158Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__159:
                self.case__159 = Id015PtlimaptOperationUnsigned.Case159Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__160:
                self.case__160 = Id015PtlimaptOperationUnsigned.Case160Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__161:
                self.case__161 = Id015PtlimaptOperationUnsigned.Case161Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__162:
                self.case__162 = Id015PtlimaptOperationUnsigned.Case162Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__163:
                self.case__163 = Id015PtlimaptOperationUnsigned.Case163Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__164:
                self.case__164 = Id015PtlimaptOperationUnsigned.Case164Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__165:
                self.case__165 = Id015PtlimaptOperationUnsigned.Case165Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__166:
                self.case__166 = Id015PtlimaptOperationUnsigned.Case166Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__167:
                self.case__167 = Id015PtlimaptOperationUnsigned.Case167Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__168:
                self.case__168 = Id015PtlimaptOperationUnsigned.Case168Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__169:
                self.case__169 = Id015PtlimaptOperationUnsigned.Case169Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__170:
                self.case__170 = Id015PtlimaptOperationUnsigned.Case170Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__171:
                self.case__171 = Id015PtlimaptOperationUnsigned.Case171Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__172:
                self.case__172 = Id015PtlimaptOperationUnsigned.Case172Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__173:
                self.case__173 = Id015PtlimaptOperationUnsigned.Case173Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__174:
                self.case__174 = Id015PtlimaptOperationUnsigned.Case174Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__175:
                self.case__175 = Id015PtlimaptOperationUnsigned.Case175Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__176:
                self.case__176 = Id015PtlimaptOperationUnsigned.Case176Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__177:
                self.case__177 = Id015PtlimaptOperationUnsigned.Case177Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__178:
                self.case__178 = Id015PtlimaptOperationUnsigned.Case178Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__179:
                self.case__179 = Id015PtlimaptOperationUnsigned.Case179Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__180:
                self.case__180 = Id015PtlimaptOperationUnsigned.Case180Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__181:
                self.case__181 = Id015PtlimaptOperationUnsigned.Case181Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__182:
                self.case__182 = Id015PtlimaptOperationUnsigned.Case182Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__183:
                self.case__183 = Id015PtlimaptOperationUnsigned.Case183Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__184:
                self.case__184 = Id015PtlimaptOperationUnsigned.Case184Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__185:
                self.case__185 = Id015PtlimaptOperationUnsigned.Case185Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__186:
                self.case__186 = Id015PtlimaptOperationUnsigned.Case186Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__187:
                self.case__187 = Id015PtlimaptOperationUnsigned.Case187Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__188:
                self.case__188 = Id015PtlimaptOperationUnsigned.Case188Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__189:
                self.case__189 = Id015PtlimaptOperationUnsigned.Case189Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__190:
                self.case__190 = Id015PtlimaptOperationUnsigned.Case190Entries(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__191:
                self.case__191 = Id015PtlimaptOperationUnsigned.Case1910(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__192:
                self.case__192 = self._io.read_bytes(32)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__208:
                self.case__208 = Id015PtlimaptOperationUnsigned.Case208(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__209:
                self.case__209 = Id015PtlimaptOperationUnsigned.Case209(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__210:
                self.case__210 = Id015PtlimaptOperationUnsigned.Case210(self._io, self, self._root)

            if self.inode_tree_tag == Id015PtlimaptOperationUnsigned.InodeTreeTag.case__211:
                self.case__211 = Id015PtlimaptOperationUnsigned.Case211(self._io, self, self._root)



    class Case171EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__171_elt_field0 = self._io.read_bytes_full()


    class Id015PtlimaptScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.storage = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case35(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__35_field0 = self._io.read_s8be()
            self.case__35_field1 = []
            for i in range(8):
                self.case__35_field1.append(Id015PtlimaptOperationUnsigned.Case35Field1Entries(self._io, self, self._root))



    class IncreasePaidStorage(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.amount = Id015PtlimaptOperationUnsigned.Z(self._io, self, self._root)
            self.destination = Id015PtlimaptOperationUnsigned.Id015PtlimaptContractIdOriginated(self._io, self, self._root)


    class Case180EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__180_elt_field0 = self._io.read_u1()
            if not self.len_case__180_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__180_elt_field0, self._io, u"/types/case__180_elt_field0_0/seq/0")
            self._raw_case__180_elt_field0 = self._io.read_bytes(self.len_case__180_elt_field0)
            _io__raw_case__180_elt_field0 = KaitaiStream(BytesIO(self._raw_case__180_elt_field0))
            self.case__180_elt_field0 = Id015PtlimaptOperationUnsigned.Case180EltField0(_io__raw_case__180_elt_field0, self, self._root)


    class Case149EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__149_elt_field0 = self._io.read_bytes_full()


    class Case51Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__51_field1_elt_field0 = self._io.read_u1()
            self.case__51_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case17(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__17_field0 = self._io.read_u2be()
            self.case__17_field1 = []
            for i in range(4):
                self.case__17_field1.append(Id015PtlimaptOperationUnsigned.Case17Field1Entries(self._io, self, self._root))



    class ScRollupPublish(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.commitment = Id015PtlimaptOperationUnsigned.Commitment0(self._io, self, self._root)


    class InputProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.input_proof_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.InputProofTag, self._io.read_u1())
            if self.input_proof_tag == Id015PtlimaptOperationUnsigned.InputProofTag.inbox__proof:
                self.inbox__proof = Id015PtlimaptOperationUnsigned.InboxProof(self._io, self, self._root)

            if self.input_proof_tag == Id015PtlimaptOperationUnsigned.InputProofTag.reveal__proof:
                self.reveal__proof = Id015PtlimaptOperationUnsigned.RevealProof(self._io, self, self._root)



    class Case28(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__28_field0 = self._io.read_u1()
            self.case__28_field1 = []
            for i in range(7):
                self.case__28_field1.append(Id015PtlimaptOperationUnsigned.Case28Field1Entries(self._io, self, self._root))



    class Id015PtlimaptBlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_015__ptlimapt__block_header__alpha__signed_contents = Id015PtlimaptOperationUnsigned.Id015PtlimaptBlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Case53Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__53_field1_elt_field0 = self._io.read_u1()
            self.case__53_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case8(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__8_field0 = self._io.read_u1()
            self.case__8_field1 = []
            for i in range(2):
                self.case__8_field1.append(Id015PtlimaptOperationUnsigned.Case8Field1Entries(self._io, self, self._root))



    class Case10Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__10_field1_elt_field0 = self._io.read_u1()
            self.case__10_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Id015PtlimaptLiquidityBakingToggleVote(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__liquidity_baking_toggle_vote = self._io.read_s1()


    class Id015PtlimaptTxRollupId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup_hash = self._io.read_bytes(20)


    class Case189EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__189_elt_field0 = self._io.read_bytes_full()


    class Case150EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__150_elt_field0 = self._io.read_bytes_full()


    class Case10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__10_field0 = self._io.read_s4be()
            self.case__10_field1 = []
            for i in range(2):
                self.case__10_field1.append(Id015PtlimaptOperationUnsigned.Case10Field1Entries(self._io, self, self._root))



    class Case180EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__180_elt_field0 = self._io.read_bytes_full()


    class Case60Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__60_field1 = self._io.read_u4be()
            if not self.len_case__60_field1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__60_field1, self._io, u"/types/case__60_field1_0/seq/0")
            self._raw_case__60_field1 = self._io.read_bytes(self.len_case__60_field1)
            _io__raw_case__60_field1 = KaitaiStream(BytesIO(self._raw_case__60_field1))
            self.case__60_field1 = Id015PtlimaptOperationUnsigned.Case60Field1(_io__raw_case__60_field1, self, self._root)


    class Case61Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__61_field1_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__61_field1_entries.append(Id015PtlimaptOperationUnsigned.Case61Field1Entries(self._io, self, self._root))
                i += 1



    class ActivateAccount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = self._io.read_bytes(20)
            self.secret = self._io.read_bytes(20)


    class Case61(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__61_field0 = self._io.read_u2be()
            self.case__61_field1 = Id015PtlimaptOperationUnsigned.Case61Field10(self._io, self, self._root)


    class Case152EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__152_elt_field0 = self._io.read_bytes_full()


    class Case57Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__57_field1_elt_field0 = self._io.read_u1()
            self.case__57_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case147Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__147_elt_field0 = Id015PtlimaptOperationUnsigned.Case147EltField00(self._io, self, self._root)
            self.case__147_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case62Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__62_field1_elt_field0 = self._io.read_u1()
            self.case__62_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class DoubleEndorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id015PtlimaptOperationUnsigned.Op10(self._io, self, self._root)
            self.op2 = Id015PtlimaptOperationUnsigned.Op20(self._io, self, self._root)


    class Step(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.step_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.StepTag, self._io.read_u1())
            if self.step_tag == Id015PtlimaptOperationUnsigned.StepTag.dissection:
                self.dissection = Id015PtlimaptOperationUnsigned.Dissection0(self._io, self, self._root)

            if self.step_tag == Id015PtlimaptOperationUnsigned.StepTag.proof:
                self.proof = Id015PtlimaptOperationUnsigned.Proof1(self._io, self, self._root)



    class Id015PtlimaptRollupAddress(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__rollup_address = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case39(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__39_field0 = self._io.read_s8be()
            self.case__39_field1 = []
            for i in range(9):
                self.case__39_field1.append(Id015PtlimaptOperationUnsigned.Case39Field1Entries(self._io, self, self._root))



    class Case9(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__9_field0 = self._io.read_u2be()
            self.case__9_field1 = []
            for i in range(2):
                self.case__9_field1.append(Id015PtlimaptOperationUnsigned.Case9Field1Entries(self._io, self, self._root))



    class Case18Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__18_field1_elt_field0 = self._io.read_u1()
            self.case__18_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case13(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__13_field0 = self._io.read_u2be()
            self.case__13_field1 = []
            for i in range(3):
                self.case__13_field1.append(Id015PtlimaptOperationUnsigned.Case13Field1Entries(self._io, self, self._root))



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Case170EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__170_elt_field0 = self._io.read_u1()
            if not self.len_case__170_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__170_elt_field0, self._io, u"/types/case__170_elt_field0_0/seq/0")
            self._raw_case__170_elt_field0 = self._io.read_bytes(self.len_case__170_elt_field0)
            _io__raw_case__170_elt_field0 = KaitaiStream(BytesIO(self._raw_case__170_elt_field0))
            self.case__170_elt_field0 = Id015PtlimaptOperationUnsigned.Case170EltField0(_io__raw_case__170_elt_field0, self, self._root)


    class Case29Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__29_field1_elt_field0 = self._io.read_u1()
            self.case__29_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case145EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__145_elt_field0 = self._io.read_u1()
            if not self.len_case__145_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__145_elt_field0, self._io, u"/types/case__145_elt_field0_0/seq/0")
            self._raw_case__145_elt_field0 = self._io.read_bytes(self.len_case__145_elt_field0)
            _io__raw_case__145_elt_field0 = KaitaiStream(BytesIO(self._raw_case__145_elt_field0))
            self.case__145_elt_field0 = Id015PtlimaptOperationUnsigned.Case145EltField0(_io__raw_case__145_elt_field0, self, self._root)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id015PtlimaptOperationUnsigned.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id015PtlimaptInlinedEndorsementMempoolContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__endorsement_mempool__contents_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Id015PtlimaptInlinedEndorsementMempoolContentsTag, self._io.read_u1())
            if self.id_015__ptlimapt__inlined__endorsement_mempool__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptInlinedEndorsementMempoolContentsTag.endorsement:
                self.endorsement = Id015PtlimaptOperationUnsigned.Endorsement(self._io, self, self._root)



    class Case186Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__186_elt_field0 = Id015PtlimaptOperationUnsigned.Case186EltField00(self._io, self, self._root)
            self.case__186_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case4(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__4_field0 = self._io.read_u1()
            self.case__4_field1 = []
            for i in range(1):
                self.case__4_field1.append(Id015PtlimaptOperationUnsigned.Case4Field1Entries(self._io, self, self._root))



    class Endorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class Case187EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__187_elt_field0 = self._io.read_bytes_full()


    class Case60Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__60_field1_elt_field0 = self._io.read_u1()
            self.case__60_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case130Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__130_elt_field0 = Id015PtlimaptOperationUnsigned.Case130EltField00(self._io, self, self._root)
            self.case__130_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case136Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__136_elt_field0 = Id015PtlimaptOperationUnsigned.Case136EltField00(self._io, self, self._root)
            self.case__136_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case62Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__62_field1_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__62_field1_entries.append(Id015PtlimaptOperationUnsigned.Case62Field1Entries(self._io, self, self._root))
                i += 1



    class Case63Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__63_field1_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__63_field1_entries.append(Id015PtlimaptOperationUnsigned.Case63Field1Entries(self._io, self, self._root))
                i += 1



    class Case163EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__163_elt_field0 = self._io.read_u1()
            if not self.len_case__163_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__163_elt_field0, self._io, u"/types/case__163_elt_field0_0/seq/0")
            self._raw_case__163_elt_field0 = self._io.read_bytes(self.len_case__163_elt_field0)
            _io__raw_case__163_elt_field0 = KaitaiStream(BytesIO(self._raw_case__163_elt_field0))
            self.case__163_elt_field0 = Id015PtlimaptOperationUnsigned.Case163EltField0(_io__raw_case__163_elt_field0, self, self._root)


    class Proposals0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_proposals = self._io.read_u4be()
            if not self.len_proposals <= 640:
                raise kaitaistruct.ValidationGreaterThanError(640, self.len_proposals, self._io, u"/types/proposals_0/seq/0")
            self._raw_proposals = self._io.read_bytes(self.len_proposals)
            _io__raw_proposals = KaitaiStream(BytesIO(self._raw_proposals))
            self.proposals = Id015PtlimaptOperationUnsigned.Proposals(_io__raw_proposals, self, self._root)


    class MessagesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_hash = self._io.read_bytes(32)


    class Case157Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__157_elt_field0 = Id015PtlimaptOperationUnsigned.Case157EltField00(self._io, self, self._root)
            self.case__157_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case1910(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__191 = self._io.read_u4be()
            if not self.len_case__191 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__191, self._io, u"/types/case__191_0/seq/0")
            self._raw_case__191 = self._io.read_bytes(self.len_case__191)
            _io__raw_case__191 = KaitaiStream(BytesIO(self._raw_case__191))
            self.case__191 = Id015PtlimaptOperationUnsigned.Case191(_io__raw_case__191, self, self._root)


    class TransferTicket(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.ticket_contents = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.ticket_ty = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.ticket_ticketer = Id015PtlimaptOperationUnsigned.Id015PtlimaptContractId(self._io, self, self._root)
            self.ticket_amount = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.destination = Id015PtlimaptOperationUnsigned.Id015PtlimaptContractId(self._io, self, self._root)
            self.entrypoint = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case208Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__208_field1 = self._io.read_bytes_full()


    class Case129EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__129_elt_field0 = self._io.read_bytes_full()


    class Case218(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__218_field0 = self._io.read_s4be()
            self.case__218_field1 = Id015PtlimaptOperationUnsigned.Case218Field10(self._io, self, self._root)
            self.case__218_field2 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Payload(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_entries = []
            i = 0
            while not self._io.is_eof():
                self.payload_entries.append(Id015PtlimaptOperationUnsigned.PayloadEntries(self._io, self, self._root))
                i += 1



    class Case13Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__13_field1_elt_field0 = self._io.read_u1()
            self.case__13_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case140EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__140_elt_field0 = self._io.read_bytes_full()


    class Case48Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__48_field1_elt_field0 = self._io.read_u1()
            self.case__48_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case60Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__60_field1_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__60_field1_entries.append(Id015PtlimaptOperationUnsigned.Case60Field1Entries(self._io, self, self._root))
                i += 1



    class Case145Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__145_elt_field0 = Id015PtlimaptOperationUnsigned.Case145EltField00(self._io, self, self._root)
            self.case__145_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case155EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__155_elt_field0 = self._io.read_u1()
            if not self.len_case__155_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__155_elt_field0, self._io, u"/types/case__155_elt_field0_0/seq/0")
            self._raw_case__155_elt_field0 = self._io.read_bytes(self.len_case__155_elt_field0)
            _io__raw_case__155_elt_field0 = KaitaiStream(BytesIO(self._raw_case__155_elt_field0))
            self.case__155_elt_field0 = Id015PtlimaptOperationUnsigned.Case155EltField0(_io__raw_case__155_elt_field0, self, self._root)


    class CircuitsInfo(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.circuits_info_entries = []
            i = 0
            while not self._io.is_eof():
                self.circuits_info_entries.append(Id015PtlimaptOperationUnsigned.CircuitsInfoEntries(self._io, self, self._root))
                i += 1



    class Case57(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__57_field0 = self._io.read_u2be()
            self.case__57_field1 = []
            for i in range(14):
                self.case__57_field1.append(Id015PtlimaptOperationUnsigned.Case57Field1Entries(self._io, self, self._root))



    class Op12(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_2/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id015PtlimaptOperationUnsigned.Op11(_io__raw_op1, self, self._root)


    class Case62Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__62_field1 = self._io.read_u4be()
            if not self.len_case__62_field1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__62_field1, self._io, u"/types/case__62_field1_0/seq/0")
            self._raw_case__62_field1 = self._io.read_bytes(self.len_case__62_field1)
            _io__raw_case__62_field1 = KaitaiStream(BytesIO(self._raw_case__62_field1))
            self.case__62_field1 = Id015PtlimaptOperationUnsigned.Case62Field1(_io__raw_case__62_field1, self, self._root)


    class Case40(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__40_field0 = self._io.read_u1()
            self.case__40_field1 = []
            for i in range(10):
                self.case__40_field1.append(Id015PtlimaptOperationUnsigned.Case40Field1Entries(self._io, self, self._root))



    class ScRollupExecuteOutboxMessage(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.cemented_commitment = self._io.read_bytes(32)
            self.output_proof = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case132EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__132_elt_field0 = self._io.read_u1()
            if not self.len_case__132_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__132_elt_field0, self._io, u"/types/case__132_elt_field0_0/seq/0")
            self._raw_case__132_elt_field0 = self._io.read_bytes(self.len_case__132_elt_field0)
            _io__raw_case__132_elt_field0 = KaitaiStream(BytesIO(self._raw_case__132_elt_field0))
            self.case__132_elt_field0 = Id015PtlimaptOperationUnsigned.Case132EltField0(_io__raw_case__132_elt_field0, self, self._root)


    class Case9Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__9_field1_elt_field0 = self._io.read_u1()
            self.case__9_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Id015PtlimaptEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__entrypoint_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Id015PtlimaptEntrypointTag, self._io.read_u1())
            if self.id_015__ptlimapt__entrypoint_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptEntrypointTag.named:
                self.named = Id015PtlimaptOperationUnsigned.Named0(self._io, self, self._root)



    class TxRollupRemoveCommitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptTxRollupId(self._io, self, self._root)


    class Case0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field0 = self._io.read_s2be()
            self.case__0_field1 = self._io.read_bytes(32)
            self.case__0_field2 = self._io.read_bytes(32)
            self.case__0_field3 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case210Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__210_field1 = self._io.read_bytes_full()


    class Case164Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__164_elt_field0 = Id015PtlimaptOperationUnsigned.Case164EltField00(self._io, self, self._root)
            self.case__164_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case165EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__165_elt_field0 = self._io.read_bytes_full()


    class Case191EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__191_elt_field0 = self._io.read_u1()
            if not self.len_case__191_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__191_elt_field0, self._io, u"/types/case__191_elt_field0_0/seq/0")
            self._raw_case__191_elt_field0 = self._io.read_bytes(self.len_case__191_elt_field0)
            _io__raw_case__191_elt_field0 = KaitaiStream(BytesIO(self._raw_case__191_elt_field0))
            self.case__191_elt_field0 = Id015PtlimaptOperationUnsigned.Case191EltField0(_io__raw_case__191_elt_field0, self, self._root)


    class Case51(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__51_field0 = self._io.read_s8be()
            self.case__51_field1 = []
            for i in range(12):
                self.case__51_field1.append(Id015PtlimaptOperationUnsigned.Case51Field1Entries(self._io, self, self._root))



    class Case178EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__178_elt_field0 = self._io.read_u1()
            if not self.len_case__178_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__178_elt_field0, self._io, u"/types/case__178_elt_field0_0/seq/0")
            self._raw_case__178_elt_field0 = self._io.read_bytes(self.len_case__178_elt_field0)
            _io__raw_case__178_elt_field0 = KaitaiStream(BytesIO(self._raw_case__178_elt_field0))
            self.case__178_elt_field0 = Id015PtlimaptOperationUnsigned.Case178EltField0(_io__raw_case__178_elt_field0, self, self._root)


    class Case146EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__146_elt_field0 = self._io.read_bytes_full()


    class Case22Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__22_field1_elt_field0 = self._io.read_u1()
            self.case__22_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case21(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__21_field0 = self._io.read_u2be()
            self.case__21_field1 = []
            for i in range(5):
                self.case__21_field1.append(Id015PtlimaptOperationUnsigned.Case21Field1Entries(self._io, self, self._root))



    class Case46(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__46_field0 = self._io.read_s4be()
            self.case__46_field1 = []
            for i in range(11):
                self.case__46_field1.append(Id015PtlimaptOperationUnsigned.Case46Field1Entries(self._io, self, self._root))



    class Case159Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__159_elt_field0 = Id015PtlimaptOperationUnsigned.Case159EltField00(self._io, self, self._root)
            self.case__159_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case134Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__134_elt_field0 = Id015PtlimaptOperationUnsigned.Case134EltField00(self._io, self, self._root)
            self.case__134_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case155Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__155_elt_field0 = Id015PtlimaptOperationUnsigned.Case155EltField00(self._io, self, self._root)
            self.case__155_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case148EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__148_elt_field0 = self._io.read_u1()
            if not self.len_case__148_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__148_elt_field0, self._io, u"/types/case__148_elt_field0_0/seq/0")
            self._raw_case__148_elt_field0 = self._io.read_bytes(self.len_case__148_elt_field0)
            _io__raw_case__148_elt_field0 = KaitaiStream(BytesIO(self._raw_case__148_elt_field0))
            self.case__148_elt_field0 = Id015PtlimaptOperationUnsigned.Case148EltField0(_io__raw_case__148_elt_field0, self, self._root)


    class Case54Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__54_field1_elt_field0 = self._io.read_u1()
            self.case__54_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptOperationUnsigned.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case172Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__172_elt_field0 = Id015PtlimaptOperationUnsigned.Case172EltField00(self._io, self, self._root)
            self.case__172_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case135Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__135_elt_field0 = Id015PtlimaptOperationUnsigned.Case135EltField00(self._io, self, self._root)
            self.case__135_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Reveal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.public_key = Id015PtlimaptOperationUnsigned.PublicKey(self._io, self, self._root)


    class Case148EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__148_elt_field0 = self._io.read_bytes_full()


    class Case168EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__168_elt_field0 = self._io.read_bytes_full()


    class Case210(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__210_field0 = self._io.read_s4be()
            self.case__210_field1 = Id015PtlimaptOperationUnsigned.Case210Field10(self._io, self, self._root)
            self.case__210_field2 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case34(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__34_field0 = self._io.read_s4be()
            self.case__34_field1 = []
            for i in range(8):
                self.case__34_field1.append(Id015PtlimaptOperationUnsigned.Case34Field1Entries(self._io, self, self._root))



    class RevealProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.reveal_proof_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.RevealProofTag, self._io.read_u1())
            if self.reveal_proof_tag == Id015PtlimaptOperationUnsigned.RevealProofTag.raw__data__proof:
                self.raw__data__proof = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)



    class Id015PtlimaptInlinedPreendorsementContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__preendorsement__contents_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Id015PtlimaptInlinedPreendorsementContentsTag, self._io.read_u1())
            if self.id_015__ptlimapt__inlined__preendorsement__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptInlinedPreendorsementContentsTag.preendorsement:
                self.preendorsement = Id015PtlimaptOperationUnsigned.Preendorsement(self._io, self, self._root)



    class Case31Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__31_field1_elt_field0 = self._io.read_u1()
            self.case__31_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case23(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__23_field0 = self._io.read_s8be()
            self.case__23_field1 = []
            for i in range(5):
                self.case__23_field1.append(Id015PtlimaptOperationUnsigned.Case23Field1Entries(self._io, self, self._root))



    class Case44(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__44_field0 = self._io.read_u1()
            self.case__44_field1 = []
            for i in range(11):
                self.case__44_field1.append(Id015PtlimaptOperationUnsigned.Case44Field1Entries(self._io, self, self._root))



    class Case6Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__6_field1_elt_field0 = self._io.read_u1()
            self.case__6_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class CircuitsInfo0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_circuits_info = self._io.read_u4be()
            if not self.len_circuits_info <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_circuits_info, self._io, u"/types/circuits_info_0/seq/0")
            self._raw_circuits_info = self._io.read_bytes(self.len_circuits_info)
            _io__raw_circuits_info = KaitaiStream(BytesIO(self._raw_circuits_info))
            self.circuits_info = Id015PtlimaptOperationUnsigned.CircuitsInfo(_io__raw_circuits_info, self, self._root)


    class Case188Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__188_elt_field0 = Id015PtlimaptOperationUnsigned.Case188EltField00(self._io, self, self._root)
            self.case__188_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Price(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id = self._io.read_bytes(32)
            self.amount = Id015PtlimaptOperationUnsigned.Z(self._io, self, self._root)


    class Case33(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__33_field0 = self._io.read_u2be()
            self.case__33_field1 = []
            for i in range(8):
                self.case__33_field1.append(Id015PtlimaptOperationUnsigned.Case33Field1Entries(self._io, self, self._root))



    class Case66(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__66_field0 = self._io.read_s4be()
            self.case__66_field1 = []
            for i in range(32):
                self.case__66_field1.append(Id015PtlimaptOperationUnsigned.Case66Field1Entries(self._io, self, self._root))



    class Case150Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__150_elt_field0 = Id015PtlimaptOperationUnsigned.Case150EltField00(self._io, self, self._root)
            self.case__150_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case185EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__185_elt_field0 = self._io.read_u1()
            if not self.len_case__185_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__185_elt_field0, self._io, u"/types/case__185_elt_field0_0/seq/0")
            self._raw_case__185_elt_field0 = self._io.read_bytes(self.len_case__185_elt_field0)
            _io__raw_case__185_elt_field0 = KaitaiStream(BytesIO(self._raw_case__185_elt_field0))
            self.case__185_elt_field0 = Id015PtlimaptOperationUnsigned.Case185EltField0(_io__raw_case__185_elt_field0, self, self._root)


    class Id015PtlimaptBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__block_header__alpha__unsigned_contents = Id015PtlimaptOperationUnsigned.Id015PtlimaptBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes(64)


    class Case142Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__142_elt_field0 = Id015PtlimaptOperationUnsigned.Case142EltField00(self._io, self, self._root)
            self.case__142_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case22(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__22_field0 = self._io.read_s4be()
            self.case__22_field1 = []
            for i in range(5):
                self.case__22_field1.append(Id015PtlimaptOperationUnsigned.Case22Field1Entries(self._io, self, self._root))



    class Case175EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__175_elt_field0 = self._io.read_u1()
            if not self.len_case__175_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__175_elt_field0, self._io, u"/types/case__175_elt_field0_0/seq/0")
            self._raw_case__175_elt_field0 = self._io.read_bytes(self.len_case__175_elt_field0)
            _io__raw_case__175_elt_field0 = KaitaiStream(BytesIO(self._raw_case__175_elt_field0))
            self.case__175_elt_field0 = Id015PtlimaptOperationUnsigned.Case175EltField0(_io__raw_case__175_elt_field0, self, self._root)


    class MessageEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_elt = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case55(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__55_field0 = self._io.read_s8be()
            self.case__55_field1 = []
            for i in range(13):
                self.case__55_field1.append(Id015PtlimaptOperationUnsigned.Case55Field1Entries(self._io, self, self._root))



    class Case40Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__40_field1_elt_field0 = self._io.read_u1()
            self.case__40_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class OpEltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_code = Id015PtlimaptOperationUnsigned.Int31(self._io, self, self._root)
            self.price = Id015PtlimaptOperationUnsigned.Price(self._io, self, self._root)
            self.l1_dst = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.rollup_id = self._io.read_bytes(20)
            self.payload = Id015PtlimaptOperationUnsigned.Payload0(self._io, self, self._root)


    class Case191Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__191_elt_field0 = Id015PtlimaptOperationUnsigned.Case191EltField00(self._io, self, self._root)
            self.case__191_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case154EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__154_elt_field0 = self._io.read_bytes_full()


    class Case216Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__216_field1 = self._io.read_bytes_full()


    class Case144Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__144_elt_field0 = Id015PtlimaptOperationUnsigned.Case144EltField00(self._io, self, self._root)
            self.case__144_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case187Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__187_elt_field0 = Id015PtlimaptOperationUnsigned.Case187EltField00(self._io, self, self._root)
            self.case__187_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case132EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__132_elt_field0 = self._io.read_bytes_full()


    class Case217Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__217_field1 = self._io.read_bytes_full()


    class Case61Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__61_field1 = self._io.read_u4be()
            if not self.len_case__61_field1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__61_field1, self._io, u"/types/case__61_field1_0/seq/0")
            self._raw_case__61_field1 = self._io.read_bytes(self.len_case__61_field1)
            _io__raw_case__61_field1 = KaitaiStream(BytesIO(self._raw_case__61_field1))
            self.case__61_field1 = Id015PtlimaptOperationUnsigned.Case61Field1(_io__raw_case__61_field1, self, self._root)


    class Case52(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__52_field0 = self._io.read_u1()
            self.case__52_field1 = []
            for i in range(13):
                self.case__52_field1.append(Id015PtlimaptOperationUnsigned.Case52Field1Entries(self._io, self, self._root))



    class Case5(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__5_field0 = self._io.read_u2be()
            self.case__5_field1 = []
            for i in range(1):
                self.case__5_field1.append(Id015PtlimaptOperationUnsigned.Case5Field1Entries(self._io, self, self._root))



    class Case7Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__7_field1_elt_field0 = self._io.read_u1()
            self.case__7_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case162Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__162_elt_field0 = Id015PtlimaptOperationUnsigned.Case162EltField00(self._io, self, self._root)
            self.case__162_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case193(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__193 = self._io.read_bytes_full()


    class Case38(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__38_field0 = self._io.read_s4be()
            self.case__38_field1 = []
            for i in range(9):
                self.case__38_field1.append(Id015PtlimaptOperationUnsigned.Case38Field1Entries(self._io, self, self._root))



    class Id015PtlimaptOperationAlphaContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__operation__alpha__contents_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag, self._io.read_u1())
            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.endorsement:
                self.endorsement = Id015PtlimaptOperationUnsigned.Endorsement(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.preendorsement:
                self.preendorsement = Id015PtlimaptOperationUnsigned.Preendorsement(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.dal_slot_availability:
                self.dal_slot_availability = Id015PtlimaptOperationUnsigned.DalSlotAvailability(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.seed_nonce_revelation:
                self.seed_nonce_revelation = Id015PtlimaptOperationUnsigned.SeedNonceRevelation(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.vdf_revelation:
                self.vdf_revelation = Id015PtlimaptOperationUnsigned.Solution(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.double_endorsement_evidence:
                self.double_endorsement_evidence = Id015PtlimaptOperationUnsigned.DoubleEndorsementEvidence(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.double_preendorsement_evidence:
                self.double_preendorsement_evidence = Id015PtlimaptOperationUnsigned.DoublePreendorsementEvidence(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.double_baking_evidence:
                self.double_baking_evidence = Id015PtlimaptOperationUnsigned.DoubleBakingEvidence(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.activate_account:
                self.activate_account = Id015PtlimaptOperationUnsigned.ActivateAccount(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.proposals:
                self.proposals = Id015PtlimaptOperationUnsigned.Proposals1(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.ballot:
                self.ballot = Id015PtlimaptOperationUnsigned.Ballot(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.reveal:
                self.reveal = Id015PtlimaptOperationUnsigned.Reveal(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.transaction:
                self.transaction = Id015PtlimaptOperationUnsigned.Transaction(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.origination:
                self.origination = Id015PtlimaptOperationUnsigned.Origination(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.delegation:
                self.delegation = Id015PtlimaptOperationUnsigned.Delegation(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.set_deposits_limit:
                self.set_deposits_limit = Id015PtlimaptOperationUnsigned.SetDepositsLimit(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.increase_paid_storage:
                self.increase_paid_storage = Id015PtlimaptOperationUnsigned.IncreasePaidStorage(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.update_consensus_key:
                self.update_consensus_key = Id015PtlimaptOperationUnsigned.UpdateConsensusKey(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.drain_delegate:
                self.drain_delegate = Id015PtlimaptOperationUnsigned.DrainDelegate(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.failing_noop:
                self.failing_noop = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.register_global_constant:
                self.register_global_constant = Id015PtlimaptOperationUnsigned.RegisterGlobalConstant(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_origination:
                self.tx_rollup_origination = Id015PtlimaptOperationUnsigned.TxRollupOrigination(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_submit_batch:
                self.tx_rollup_submit_batch = Id015PtlimaptOperationUnsigned.TxRollupSubmitBatch(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_commit:
                self.tx_rollup_commit = Id015PtlimaptOperationUnsigned.TxRollupCommit(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_return_bond:
                self.tx_rollup_return_bond = Id015PtlimaptOperationUnsigned.TxRollupReturnBond(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_finalize_commitment:
                self.tx_rollup_finalize_commitment = Id015PtlimaptOperationUnsigned.TxRollupFinalizeCommitment(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_remove_commitment:
                self.tx_rollup_remove_commitment = Id015PtlimaptOperationUnsigned.TxRollupRemoveCommitment(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_rejection:
                self.tx_rollup_rejection = Id015PtlimaptOperationUnsigned.TxRollupRejection(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.tx_rollup_dispatch_tickets:
                self.tx_rollup_dispatch_tickets = Id015PtlimaptOperationUnsigned.TxRollupDispatchTickets(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.transfer_ticket:
                self.transfer_ticket = Id015PtlimaptOperationUnsigned.TransferTicket(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.dal_publish_slot_header:
                self.dal_publish_slot_header = Id015PtlimaptOperationUnsigned.DalPublishSlotHeader(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_originate:
                self.sc_rollup_originate = Id015PtlimaptOperationUnsigned.ScRollupOriginate(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_add_messages:
                self.sc_rollup_add_messages = Id015PtlimaptOperationUnsigned.ScRollupAddMessages(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_cement:
                self.sc_rollup_cement = Id015PtlimaptOperationUnsigned.ScRollupCement(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_publish:
                self.sc_rollup_publish = Id015PtlimaptOperationUnsigned.ScRollupPublish(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_refute:
                self.sc_rollup_refute = Id015PtlimaptOperationUnsigned.ScRollupRefute(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_timeout:
                self.sc_rollup_timeout = Id015PtlimaptOperationUnsigned.ScRollupTimeout(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_execute_outbox_message:
                self.sc_rollup_execute_outbox_message = Id015PtlimaptOperationUnsigned.ScRollupExecuteOutboxMessage(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_recover_bond:
                self.sc_rollup_recover_bond = Id015PtlimaptOperationUnsigned.ScRollupRecoverBond(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.sc_rollup_dal_slot_subscribe:
                self.sc_rollup_dal_slot_subscribe = Id015PtlimaptOperationUnsigned.ScRollupDalSlotSubscribe(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.zk_rollup_origination:
                self.zk_rollup_origination = Id015PtlimaptOperationUnsigned.ZkRollupOrigination(self._io, self, self._root)

            if self.id_015__ptlimapt__operation__alpha__contents_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContentsTag.zk_rollup_publish:
                self.zk_rollup_publish = Id015PtlimaptOperationUnsigned.ZkRollupPublish(self._io, self, self._root)



    class Case58Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__58_field1_elt_field0 = self._io.read_u1()
            self.case__58_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Refutation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.choice = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.step = Id015PtlimaptOperationUnsigned.Step(self._io, self, self._root)


    class ScRollupDalSlotSubscribe(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.slot_index = self._io.read_u1()


    class Case166Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__166_elt_field0 = Id015PtlimaptOperationUnsigned.Case166EltField00(self._io, self, self._root)
            self.case__166_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case188EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__188_elt_field0 = self._io.read_u1()
            if not self.len_case__188_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__188_elt_field0, self._io, u"/types/case__188_elt_field0_0/seq/0")
            self._raw_case__188_elt_field0 = self._io.read_bytes(self.len_case__188_elt_field0)
            _io__raw_case__188_elt_field0 = KaitaiStream(BytesIO(self._raw_case__188_elt_field0))
            self.case__188_elt_field0 = Id015PtlimaptOperationUnsigned.Case188EltField0(_io__raw_case__188_elt_field0, self, self._root)


    class ScRollupOriginate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.pvm_kind = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.PvmKind, self._io.read_u1())
            self.boot_sector = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.origination_proof = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.parameters_ty = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case62(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__62_field0 = self._io.read_s4be()
            self.case__62_field1 = Id015PtlimaptOperationUnsigned.Case62Field10(self._io, self, self._root)


    class Case40(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__4_field0 = self._io.read_u1()
            self.case__4_field1 = []
            for i in range(1):
                self.case__4_field1.append(Id015PtlimaptOperationUnsigned.Case4Field1Entries0(self._io, self, self._root))



    class SeedNonceRevelation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.nonce = self._io.read_bytes(32)


    class Case27(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__27_field0 = self._io.read_s8be()
            self.case__27_field1 = []
            for i in range(6):
                self.case__27_field1.append(Id015PtlimaptOperationUnsigned.Case27Field1Entries(self._io, self, self._root))



    class Case136EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__136_elt_field0 = self._io.read_u1()
            if not self.len_case__136_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__136_elt_field0, self._io, u"/types/case__136_elt_field0_0/seq/0")
            self._raw_case__136_elt_field0 = self._io.read_bytes(self.len_case__136_elt_field0)
            _io__raw_case__136_elt_field0 = KaitaiStream(BytesIO(self._raw_case__136_elt_field0))
            self.case__136_elt_field0 = Id015PtlimaptOperationUnsigned.Case136EltField0(_io__raw_case__136_elt_field0, self, self._root)


    class Case208Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__208_field1 = self._io.read_u1()
            if not self.len_case__208_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__208_field1, self._io, u"/types/case__208_field1_0/seq/0")
            self._raw_case__208_field1 = self._io.read_bytes(self.len_case__208_field1)
            _io__raw_case__208_field1 = KaitaiStream(BytesIO(self._raw_case__208_field1))
            self.case__208_field1 = Id015PtlimaptOperationUnsigned.Case208Field1(_io__raw_case__208_field1, self, self._root)


    class Preendorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class Case142EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__142_elt_field0 = self._io.read_u1()
            if not self.len_case__142_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__142_elt_field0, self._io, u"/types/case__142_elt_field0_0/seq/0")
            self._raw_case__142_elt_field0 = self._io.read_bytes(self.len_case__142_elt_field0)
            _io__raw_case__142_elt_field0 = KaitaiStream(BytesIO(self._raw_case__142_elt_field0))
            self.case__142_elt_field0 = Id015PtlimaptOperationUnsigned.Case142EltField0(_io__raw_case__142_elt_field0, self, self._root)


    class Case179EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__179_elt_field0 = self._io.read_u1()
            if not self.len_case__179_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__179_elt_field0, self._io, u"/types/case__179_elt_field0_0/seq/0")
            self._raw_case__179_elt_field0 = self._io.read_bytes(self.len_case__179_elt_field0)
            _io__raw_case__179_elt_field0 = KaitaiStream(BytesIO(self._raw_case__179_elt_field0))
            self.case__179_elt_field0 = Id015PtlimaptOperationUnsigned.Case179EltField0(_io__raw_case__179_elt_field0, self, self._root)


    class Case143EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__143_elt_field0 = self._io.read_bytes_full()


    class Case135EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__135_elt_field0 = self._io.read_u1()
            if not self.len_case__135_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__135_elt_field0, self._io, u"/types/case__135_elt_field0_0/seq/0")
            self._raw_case__135_elt_field0 = self._io.read_bytes(self.len_case__135_elt_field0)
            _io__raw_case__135_elt_field0 = KaitaiStream(BytesIO(self._raw_case__135_elt_field0))
            self.case__135_elt_field0 = Id015PtlimaptOperationUnsigned.Case135EltField0(_io__raw_case__135_elt_field0, self, self._root)


    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptOperationUnsigned.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.annots = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case49(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__49_field0 = self._io.read_u2be()
            self.case__49_field1 = []
            for i in range(12):
                self.case__49_field1.append(Id015PtlimaptOperationUnsigned.Case49Field1Entries(self._io, self, self._root))



    class Case11(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__11_field0 = self._io.read_s8be()
            self.case__11_field1 = []
            for i in range(2):
                self.case__11_field1.append(Id015PtlimaptOperationUnsigned.Case11Field1Entries(self._io, self, self._root))



    class Case153EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__153_elt_field0 = self._io.read_u1()
            if not self.len_case__153_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__153_elt_field0, self._io, u"/types/case__153_elt_field0_0/seq/0")
            self._raw_case__153_elt_field0 = self._io.read_bytes(self.len_case__153_elt_field0)
            _io__raw_case__153_elt_field0 = KaitaiStream(BytesIO(self._raw_case__153_elt_field0))
            self.case__153_elt_field0 = Id015PtlimaptOperationUnsigned.Case153EltField0(_io__raw_case__153_elt_field0, self, self._root)


    class Case178Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__178_elt_field0 = Id015PtlimaptOperationUnsigned.Case178EltField00(self._io, self, self._root)
            self.case__178_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case184EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__184_elt_field0 = self._io.read_bytes_full()


    class Case16(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__16_field0 = self._io.read_u1()
            self.case__16_field1 = []
            for i in range(4):
                self.case__16_field1.append(Id015PtlimaptOperationUnsigned.Case16Field1Entries(self._io, self, self._root))



    class Case162EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__162_elt_field0 = self._io.read_u1()
            if not self.len_case__162_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__162_elt_field0, self._io, u"/types/case__162_elt_field0_0/seq/0")
            self._raw_case__162_elt_field0 = self._io.read_bytes(self.len_case__162_elt_field0)
            _io__raw_case__162_elt_field0 = KaitaiStream(BytesIO(self._raw_case__162_elt_field0))
            self.case__162_elt_field0 = Id015PtlimaptOperationUnsigned.Case162EltField0(_io__raw_case__162_elt_field0, self, self._root)


    class Case5Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__5_field1_elt_field0 = self._io.read_u1()
            self.case__5_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case190EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__190_elt_field0 = self._io.read_bytes_full()


    class Case163EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__163_elt_field0 = self._io.read_bytes_full()


    class Case187EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__187_elt_field0 = self._io.read_u1()
            if not self.len_case__187_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__187_elt_field0, self._io, u"/types/case__187_elt_field0_0/seq/0")
            self._raw_case__187_elt_field0 = self._io.read_bytes(self.len_case__187_elt_field0)
            _io__raw_case__187_elt_field0 = KaitaiStream(BytesIO(self._raw_case__187_elt_field0))
            self.case__187_elt_field0 = Id015PtlimaptOperationUnsigned.Case187EltField0(_io__raw_case__187_elt_field0, self, self._root)


    class Case167EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__167_elt_field0 = self._io.read_bytes_full()


    class Case161Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__161_elt_field0 = Id015PtlimaptOperationUnsigned.Case161EltField00(self._io, self, self._root)
            self.case__161_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case39Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__39_field1_elt_field0 = self._io.read_u1()
            self.case__39_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case219(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__219_field0 = self._io.read_s8be()
            self.case__219_field1 = Id015PtlimaptOperationUnsigned.Case219Field10(self._io, self, self._root)
            self.case__219_field2 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case50Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__50_field1_elt_field0 = self._io.read_u1()
            self.case__50_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case157EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__157_elt_field0 = self._io.read_bytes_full()


    class Case11Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__11_field1_elt_field0 = self._io.read_u1()
            self.case__11_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Id015PtlimaptContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__contract_id_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Id015PtlimaptContractIdTag, self._io.read_u1())
            if self.id_015__ptlimapt__contract_id_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptContractIdTag.implicit:
                self.implicit = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)

            if self.id_015__ptlimapt__contract_id_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptContractIdTag.originated:
                self.originated = Id015PtlimaptOperationUnsigned.Originated(self._io, self, self._root)



    class Case182EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__182_elt_field0 = self._io.read_u1()
            if not self.len_case__182_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__182_elt_field0, self._io, u"/types/case__182_elt_field0_0/seq/0")
            self._raw_case__182_elt_field0 = self._io.read_bytes(self.len_case__182_elt_field0)
            _io__raw_case__182_elt_field0 = KaitaiStream(BytesIO(self._raw_case__182_elt_field0))
            self.case__182_elt_field0 = Id015PtlimaptOperationUnsigned.Case182EltField0(_io__raw_case__182_elt_field0, self, self._root)


    class Case137EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__137_elt_field0 = self._io.read_bytes_full()


    class Case219Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__219_field1 = self._io.read_u1()
            if not self.len_case__219_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__219_field1, self._io, u"/types/case__219_field1_0/seq/0")
            self._raw_case__219_field1 = self._io.read_bytes(self.len_case__219_field1)
            _io__raw_case__219_field1 = KaitaiStream(BytesIO(self._raw_case__219_field1))
            self.case__219_field1 = Id015PtlimaptOperationUnsigned.Case219Field1(_io__raw_case__219_field1, self, self._root)


    class TxRollupCommit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptTxRollupId(self._io, self, self._root)
            self.commitment = Id015PtlimaptOperationUnsigned.Commitment(self._io, self, self._root)


    class DoubleBakingEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bh1 = Id015PtlimaptOperationUnsigned.Bh10(self._io, self, self._root)
            self.bh2 = Id015PtlimaptOperationUnsigned.Bh20(self._io, self, self._root)


    class Case1911(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__191 = self._io.read_u4be()
            if not self.len_case__191 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__191, self._io, u"/types/case__191_1/seq/0")
            self._raw_case__191 = self._io.read_bytes(self.len_case__191)
            _io__raw_case__191 = KaitaiStream(BytesIO(self._raw_case__191))
            self.case__191 = Id015PtlimaptOperationUnsigned.Case191(_io__raw_case__191, self, self._root)


    class Case45(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__45_field0 = self._io.read_u2be()
            self.case__45_field1 = []
            for i in range(11):
                self.case__45_field1.append(Id015PtlimaptOperationUnsigned.Case45Field1Entries(self._io, self, self._root))



    class Case217Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__217_field1 = self._io.read_u1()
            if not self.len_case__217_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__217_field1, self._io, u"/types/case__217_field1_0/seq/0")
            self._raw_case__217_field1 = self._io.read_bytes(self.len_case__217_field1)
            _io__raw_case__217_field1 = KaitaiStream(BytesIO(self._raw_case__217_field1))
            self.case__217_field1 = Id015PtlimaptOperationUnsigned.Case217Field1(_io__raw_case__217_field1, self, self._root)


    class Case129EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__129_elt_field0 = self._io.read_u1()
            if not self.len_case__129_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__129_elt_field0, self._io, u"/types/case__129_elt_field0_0/seq/0")
            self._raw_case__129_elt_field0 = self._io.read_bytes(self.len_case__129_elt_field0)
            _io__raw_case__129_elt_field0 = KaitaiStream(BytesIO(self._raw_case__129_elt_field0))
            self.case__129_elt_field0 = Id015PtlimaptOperationUnsigned.Case129EltField0(_io__raw_case__129_elt_field0, self, self._root)


    class Case152EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__152_elt_field0 = self._io.read_u1()
            if not self.len_case__152_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__152_elt_field0, self._io, u"/types/case__152_elt_field0_0/seq/0")
            self._raw_case__152_elt_field0 = self._io.read_bytes(self.len_case__152_elt_field0)
            _io__raw_case__152_elt_field0 = KaitaiStream(BytesIO(self._raw_case__152_elt_field0))
            self.case__152_elt_field0 = Id015PtlimaptOperationUnsigned.Case152EltField0(_io__raw_case__152_elt_field0, self, self._root)


    class Case27Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__27_field1_elt_field0 = self._io.read_u1()
            self.case__27_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case160Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__160_elt_field0 = Id015PtlimaptOperationUnsigned.Case160EltField00(self._io, self, self._root)
            self.case__160_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id015PtlimaptOperationUnsigned.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id015PtlimaptOperationUnsigned.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id015PtlimaptOperationUnsigned.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Case165Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__165_elt_field0 = Id015PtlimaptOperationUnsigned.Case165EltField00(self._io, self, self._root)
            self.case__165_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case12(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__12_field0 = self._io.read_u1()
            self.case__12_field1 = []
            for i in range(3):
                self.case__12_field1.append(Id015PtlimaptOperationUnsigned.Case12Field1Entries(self._io, self, self._root))



    class TxRollupDispatchTickets(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.tx_rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptTxRollupId(self._io, self, self._root)
            self.level = self._io.read_s4be()
            self.context_hash = self._io.read_bytes(32)
            self.message_index = Id015PtlimaptOperationUnsigned.Int31(self._io, self, self._root)
            self.message_result_path = Id015PtlimaptOperationUnsigned.MessageResultPath0(self._io, self, self._root)
            self.tickets_info = Id015PtlimaptOperationUnsigned.TicketsInfo0(self._io, self, self._root)


    class Case210Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__210_field1 = self._io.read_u1()
            if not self.len_case__210_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__210_field1, self._io, u"/types/case__210_field1_0/seq/0")
            self._raw_case__210_field1 = self._io.read_bytes(self.len_case__210_field1)
            _io__raw_case__210_field1 = KaitaiStream(BytesIO(self._raw_case__210_field1))
            self.case__210_field1 = Id015PtlimaptOperationUnsigned.Case210Field1(_io__raw_case__210_field1, self, self._root)


    class Bh2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__block_header__alpha__full_header = Id015PtlimaptOperationUnsigned.Id015PtlimaptBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Case139EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__139_elt_field0 = self._io.read_u1()
            if not self.len_case__139_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__139_elt_field0, self._io, u"/types/case__139_elt_field0_0/seq/0")
            self._raw_case__139_elt_field0 = self._io.read_bytes(self.len_case__139_elt_field0)
            _io__raw_case__139_elt_field0 = KaitaiStream(BytesIO(self._raw_case__139_elt_field0))
            self.case__139_elt_field0 = Id015PtlimaptOperationUnsigned.Case139EltField0(_io__raw_case__139_elt_field0, self, self._root)


    class PreviousMessageResultPathEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_list_hash = self._io.read_bytes(32)


    class Case36Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__36_field1_elt_field0 = self._io.read_u1()
            self.case__36_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case141EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__141_elt_field0 = self._io.read_u1()
            if not self.len_case__141_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__141_elt_field0, self._io, u"/types/case__141_elt_field0_0/seq/0")
            self._raw_case__141_elt_field0 = self._io.read_bytes(self.len_case__141_elt_field0)
            _io__raw_case__141_elt_field0 = KaitaiStream(BytesIO(self._raw_case__141_elt_field0))
            self.case__141_elt_field0 = Id015PtlimaptOperationUnsigned.Case141EltField0(_io__raw_case__141_elt_field0, self, self._root)


    class Case157EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__157_elt_field0 = self._io.read_u1()
            if not self.len_case__157_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__157_elt_field0, self._io, u"/types/case__157_elt_field0_0/seq/0")
            self._raw_case__157_elt_field0 = self._io.read_bytes(self.len_case__157_elt_field0)
            _io__raw_case__157_elt_field0 = KaitaiStream(BytesIO(self._raw_case__157_elt_field0))
            self.case__157_elt_field0 = Id015PtlimaptOperationUnsigned.Case157EltField0(_io__raw_case__157_elt_field0, self, self._root)


    class Case147EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__147_elt_field0 = self._io.read_u1()
            if not self.len_case__147_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__147_elt_field0, self._io, u"/types/case__147_elt_field0_0/seq/0")
            self._raw_case__147_elt_field0 = self._io.read_bytes(self.len_case__147_elt_field0)
            _io__raw_case__147_elt_field0 = KaitaiStream(BytesIO(self._raw_case__147_elt_field0))
            self.case__147_elt_field0 = Id015PtlimaptOperationUnsigned.Case147EltField0(_io__raw_case__147_elt_field0, self, self._root)


    class Id015PtlimaptMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__mutez = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)


    class TreeEncoding(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.tree_encoding_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.TreeEncodingTag, self._io.read_u1())
            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__4:
                self.case__4 = Id015PtlimaptOperationUnsigned.Case40(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__8:
                self.case__8 = Id015PtlimaptOperationUnsigned.Case8(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__12:
                self.case__12 = Id015PtlimaptOperationUnsigned.Case12(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__16:
                self.case__16 = Id015PtlimaptOperationUnsigned.Case16(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__20:
                self.case__20 = Id015PtlimaptOperationUnsigned.Case20(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__24:
                self.case__24 = Id015PtlimaptOperationUnsigned.Case24(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__28:
                self.case__28 = Id015PtlimaptOperationUnsigned.Case28(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__32:
                self.case__32 = Id015PtlimaptOperationUnsigned.Case32(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__36:
                self.case__36 = Id015PtlimaptOperationUnsigned.Case36(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__40:
                self.case__40 = Id015PtlimaptOperationUnsigned.Case40(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__44:
                self.case__44 = Id015PtlimaptOperationUnsigned.Case44(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__48:
                self.case__48 = Id015PtlimaptOperationUnsigned.Case48(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__52:
                self.case__52 = Id015PtlimaptOperationUnsigned.Case52(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__56:
                self.case__56 = Id015PtlimaptOperationUnsigned.Case56(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__60:
                self.case__60 = Id015PtlimaptOperationUnsigned.Case60(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__64:
                self.case__64 = Id015PtlimaptOperationUnsigned.Case64(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__5:
                self.case__5 = Id015PtlimaptOperationUnsigned.Case5(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__9:
                self.case__9 = Id015PtlimaptOperationUnsigned.Case9(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__13:
                self.case__13 = Id015PtlimaptOperationUnsigned.Case13(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__17:
                self.case__17 = Id015PtlimaptOperationUnsigned.Case17(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__21:
                self.case__21 = Id015PtlimaptOperationUnsigned.Case21(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__25:
                self.case__25 = Id015PtlimaptOperationUnsigned.Case25(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__29:
                self.case__29 = Id015PtlimaptOperationUnsigned.Case29(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__33:
                self.case__33 = Id015PtlimaptOperationUnsigned.Case33(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__37:
                self.case__37 = Id015PtlimaptOperationUnsigned.Case37(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__41:
                self.case__41 = Id015PtlimaptOperationUnsigned.Case41(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__45:
                self.case__45 = Id015PtlimaptOperationUnsigned.Case45(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__49:
                self.case__49 = Id015PtlimaptOperationUnsigned.Case49(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__53:
                self.case__53 = Id015PtlimaptOperationUnsigned.Case53(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__57:
                self.case__57 = Id015PtlimaptOperationUnsigned.Case57(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__61:
                self.case__61 = Id015PtlimaptOperationUnsigned.Case61(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__65:
                self.case__65 = Id015PtlimaptOperationUnsigned.Case65(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__6:
                self.case__6 = Id015PtlimaptOperationUnsigned.Case6(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__10:
                self.case__10 = Id015PtlimaptOperationUnsigned.Case10(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__14:
                self.case__14 = Id015PtlimaptOperationUnsigned.Case14(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__18:
                self.case__18 = Id015PtlimaptOperationUnsigned.Case18(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__22:
                self.case__22 = Id015PtlimaptOperationUnsigned.Case22(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__26:
                self.case__26 = Id015PtlimaptOperationUnsigned.Case26(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__30:
                self.case__30 = Id015PtlimaptOperationUnsigned.Case30(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__34:
                self.case__34 = Id015PtlimaptOperationUnsigned.Case34(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__38:
                self.case__38 = Id015PtlimaptOperationUnsigned.Case38(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__42:
                self.case__42 = Id015PtlimaptOperationUnsigned.Case42(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__46:
                self.case__46 = Id015PtlimaptOperationUnsigned.Case46(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__50:
                self.case__50 = Id015PtlimaptOperationUnsigned.Case50(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__54:
                self.case__54 = Id015PtlimaptOperationUnsigned.Case54(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__58:
                self.case__58 = Id015PtlimaptOperationUnsigned.Case58(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__62:
                self.case__62 = Id015PtlimaptOperationUnsigned.Case62(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__66:
                self.case__66 = Id015PtlimaptOperationUnsigned.Case66(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__3:
                self.case__3 = self._io.read_s8be()

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__7:
                self.case__7 = Id015PtlimaptOperationUnsigned.Case7(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__11:
                self.case__11 = Id015PtlimaptOperationUnsigned.Case11(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__15:
                self.case__15 = Id015PtlimaptOperationUnsigned.Case15(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__19:
                self.case__19 = Id015PtlimaptOperationUnsigned.Case19(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__23:
                self.case__23 = Id015PtlimaptOperationUnsigned.Case23(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__27:
                self.case__27 = Id015PtlimaptOperationUnsigned.Case27(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__31:
                self.case__31 = Id015PtlimaptOperationUnsigned.Case31(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__35:
                self.case__35 = Id015PtlimaptOperationUnsigned.Case35(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__39:
                self.case__39 = Id015PtlimaptOperationUnsigned.Case39(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__43:
                self.case__43 = Id015PtlimaptOperationUnsigned.Case43(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__47:
                self.case__47 = Id015PtlimaptOperationUnsigned.Case47(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__51:
                self.case__51 = Id015PtlimaptOperationUnsigned.Case51(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__55:
                self.case__55 = Id015PtlimaptOperationUnsigned.Case55(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__59:
                self.case__59 = Id015PtlimaptOperationUnsigned.Case59(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__63:
                self.case__63 = Id015PtlimaptOperationUnsigned.Case63(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__67:
                self.case__67 = Id015PtlimaptOperationUnsigned.Case67(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__129:
                self.case__129 = Id015PtlimaptOperationUnsigned.Case129Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__130:
                self.case__130 = Id015PtlimaptOperationUnsigned.Case130Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__131:
                self.case__131 = Id015PtlimaptOperationUnsigned.Case131Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__132:
                self.case__132 = Id015PtlimaptOperationUnsigned.Case132Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__133:
                self.case__133 = Id015PtlimaptOperationUnsigned.Case133Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__134:
                self.case__134 = Id015PtlimaptOperationUnsigned.Case134Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__135:
                self.case__135 = Id015PtlimaptOperationUnsigned.Case135Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__136:
                self.case__136 = Id015PtlimaptOperationUnsigned.Case136Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__137:
                self.case__137 = Id015PtlimaptOperationUnsigned.Case137Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__138:
                self.case__138 = Id015PtlimaptOperationUnsigned.Case138Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__139:
                self.case__139 = Id015PtlimaptOperationUnsigned.Case139Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__140:
                self.case__140 = Id015PtlimaptOperationUnsigned.Case140Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__141:
                self.case__141 = Id015PtlimaptOperationUnsigned.Case141Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__142:
                self.case__142 = Id015PtlimaptOperationUnsigned.Case142Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__143:
                self.case__143 = Id015PtlimaptOperationUnsigned.Case143Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__144:
                self.case__144 = Id015PtlimaptOperationUnsigned.Case144Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__145:
                self.case__145 = Id015PtlimaptOperationUnsigned.Case145Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__146:
                self.case__146 = Id015PtlimaptOperationUnsigned.Case146Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__147:
                self.case__147 = Id015PtlimaptOperationUnsigned.Case147Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__148:
                self.case__148 = Id015PtlimaptOperationUnsigned.Case148Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__149:
                self.case__149 = Id015PtlimaptOperationUnsigned.Case149Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__150:
                self.case__150 = Id015PtlimaptOperationUnsigned.Case150Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__151:
                self.case__151 = Id015PtlimaptOperationUnsigned.Case151Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__152:
                self.case__152 = Id015PtlimaptOperationUnsigned.Case152Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__153:
                self.case__153 = Id015PtlimaptOperationUnsigned.Case153Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__154:
                self.case__154 = Id015PtlimaptOperationUnsigned.Case154Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__155:
                self.case__155 = Id015PtlimaptOperationUnsigned.Case155Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__156:
                self.case__156 = Id015PtlimaptOperationUnsigned.Case156Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__157:
                self.case__157 = Id015PtlimaptOperationUnsigned.Case157Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__158:
                self.case__158 = Id015PtlimaptOperationUnsigned.Case158Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__159:
                self.case__159 = Id015PtlimaptOperationUnsigned.Case159Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__160:
                self.case__160 = Id015PtlimaptOperationUnsigned.Case160Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__161:
                self.case__161 = Id015PtlimaptOperationUnsigned.Case161Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__162:
                self.case__162 = Id015PtlimaptOperationUnsigned.Case162Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__163:
                self.case__163 = Id015PtlimaptOperationUnsigned.Case163Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__164:
                self.case__164 = Id015PtlimaptOperationUnsigned.Case164Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__165:
                self.case__165 = Id015PtlimaptOperationUnsigned.Case165Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__166:
                self.case__166 = Id015PtlimaptOperationUnsigned.Case166Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__167:
                self.case__167 = Id015PtlimaptOperationUnsigned.Case167Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__168:
                self.case__168 = Id015PtlimaptOperationUnsigned.Case168Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__169:
                self.case__169 = Id015PtlimaptOperationUnsigned.Case169Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__170:
                self.case__170 = Id015PtlimaptOperationUnsigned.Case170Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__171:
                self.case__171 = Id015PtlimaptOperationUnsigned.Case171Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__172:
                self.case__172 = Id015PtlimaptOperationUnsigned.Case172Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__173:
                self.case__173 = Id015PtlimaptOperationUnsigned.Case173Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__174:
                self.case__174 = Id015PtlimaptOperationUnsigned.Case174Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__175:
                self.case__175 = Id015PtlimaptOperationUnsigned.Case175Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__176:
                self.case__176 = Id015PtlimaptOperationUnsigned.Case176Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__177:
                self.case__177 = Id015PtlimaptOperationUnsigned.Case177Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__178:
                self.case__178 = Id015PtlimaptOperationUnsigned.Case178Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__179:
                self.case__179 = Id015PtlimaptOperationUnsigned.Case179Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__180:
                self.case__180 = Id015PtlimaptOperationUnsigned.Case180Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__181:
                self.case__181 = Id015PtlimaptOperationUnsigned.Case181Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__182:
                self.case__182 = Id015PtlimaptOperationUnsigned.Case182Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__183:
                self.case__183 = Id015PtlimaptOperationUnsigned.Case183Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__184:
                self.case__184 = Id015PtlimaptOperationUnsigned.Case184Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__185:
                self.case__185 = Id015PtlimaptOperationUnsigned.Case185Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__186:
                self.case__186 = Id015PtlimaptOperationUnsigned.Case186Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__187:
                self.case__187 = Id015PtlimaptOperationUnsigned.Case187Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__188:
                self.case__188 = Id015PtlimaptOperationUnsigned.Case188Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__189:
                self.case__189 = Id015PtlimaptOperationUnsigned.Case189Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__190:
                self.case__190 = Id015PtlimaptOperationUnsigned.Case190Entries(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__191:
                self.case__191 = Id015PtlimaptOperationUnsigned.Case1911(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__192:
                self.case__192 = Id015PtlimaptOperationUnsigned.Case1920(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__193:
                self.case__193 = Id015PtlimaptOperationUnsigned.Case1930(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__195:
                self.case__195 = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__200:
                self.case__200 = self._io.read_bytes(32)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__208:
                self.case__208 = self._io.read_bytes(32)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__216:
                self.case__216 = Id015PtlimaptOperationUnsigned.Case216(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__217:
                self.case__217 = Id015PtlimaptOperationUnsigned.Case217(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__218:
                self.case__218 = Id015PtlimaptOperationUnsigned.Case218(self._io, self, self._root)

            if self.tree_encoding_tag == Id015PtlimaptOperationUnsigned.TreeEncodingTag.case__219:
                self.case__219 = Id015PtlimaptOperationUnsigned.Case219(self._io, self, self._root)



    class PvmStep(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pvm_step_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.PvmStepTag, self._io.read_u1())
            if self.pvm_step_tag == Id015PtlimaptOperationUnsigned.PvmStepTag.arithmetic__pvm__with__proof:
                self.arithmetic__pvm__with__proof = Id015PtlimaptOperationUnsigned.Proof(self._io, self, self._root)

            if self.pvm_step_tag == Id015PtlimaptOperationUnsigned.PvmStepTag.wasm__2__0__0__pvm__with__proof:
                self.wasm__2__0__0__pvm__with__proof = Id015PtlimaptOperationUnsigned.Proof0(self._io, self, self._root)



    class Case208(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__208_field0 = self._io.read_u1()
            self.case__208_field1 = Id015PtlimaptOperationUnsigned.Case208Field10(self._io, self, self._root)
            self.case__208_field2 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case49Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__49_field1_elt_field0 = self._io.read_u1()
            self.case__49_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class ScRollupCement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.commitment = self._io.read_bytes(32)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Case30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__30_field0 = self._io.read_s4be()
            self.case__30_field1 = []
            for i in range(7):
                self.case__30_field1.append(Id015PtlimaptOperationUnsigned.Case30Field1Entries(self._io, self, self._root))



    class Case67(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__67_field0 = self._io.read_s8be()
            self.case__67_field1 = []
            for i in range(32):
                self.case__67_field1.append(Id015PtlimaptOperationUnsigned.Case67Field1Entries(self._io, self, self._root))



    class Case143Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__143_elt_field0 = Id015PtlimaptOperationUnsigned.Case143EltField00(self._io, self, self._root)
            self.case__143_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Amount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.AmountTag, self._io.read_u1())
            if self.amount_tag == Id015PtlimaptOperationUnsigned.AmountTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.amount_tag == Id015PtlimaptOperationUnsigned.AmountTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.amount_tag == Id015PtlimaptOperationUnsigned.AmountTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.amount_tag == Id015PtlimaptOperationUnsigned.AmountTag.case__3:
                self.case__3 = self._io.read_s8be()



    class Case170Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__170_elt_field0 = Id015PtlimaptOperationUnsigned.Case170EltField00(self._io, self, self._root)
            self.case__170_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptOperationUnsigned.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.args = Id015PtlimaptOperationUnsigned.Args0(self._io, self, self._root)
            self.annots = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case41Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__41_field1_elt_field0 = self._io.read_u1()
            self.case__41_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case185Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__185_elt_field0 = Id015PtlimaptOperationUnsigned.Case185EltField00(self._io, self, self._root)
            self.case__185_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case158EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__158_elt_field0 = self._io.read_u1()
            if not self.len_case__158_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__158_elt_field0, self._io, u"/types/case__158_elt_field0_0/seq/0")
            self._raw_case__158_elt_field0 = self._io.read_bytes(self.len_case__158_elt_field0)
            _io__raw_case__158_elt_field0 = KaitaiStream(BytesIO(self._raw_case__158_elt_field0))
            self.case__158_elt_field0 = Id015PtlimaptOperationUnsigned.Case158EltField0(_io__raw_case__158_elt_field0, self, self._root)


    class Case189EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__189_elt_field0 = self._io.read_u1()
            if not self.len_case__189_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__189_elt_field0, self._io, u"/types/case__189_elt_field0_0/seq/0")
            self._raw_case__189_elt_field0 = self._io.read_bytes(self.len_case__189_elt_field0)
            _io__raw_case__189_elt_field0 = KaitaiStream(BytesIO(self._raw_case__189_elt_field0))
            self.case__189_elt_field0 = Id015PtlimaptOperationUnsigned.Case189EltField0(_io__raw_case__189_elt_field0, self, self._root)


    class Case175EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__175_elt_field0 = self._io.read_bytes_full()


    class Bh20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh2 = self._io.read_u4be()
            if not self.len_bh2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh2, self._io, u"/types/bh2_0/seq/0")
            self._raw_bh2 = self._io.read_bytes(self.len_bh2)
            _io__raw_bh2 = KaitaiStream(BytesIO(self._raw_bh2))
            self.bh2 = Id015PtlimaptOperationUnsigned.Bh2(_io__raw_bh2, self, self._root)


    class Case151Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__151_elt_field0 = Id015PtlimaptOperationUnsigned.Case151EltField00(self._io, self, self._root)
            self.case__151_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case173Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__173_elt_field0 = Id015PtlimaptOperationUnsigned.Case173EltField00(self._io, self, self._root)
            self.case__173_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case190EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__190_elt_field0 = self._io.read_u1()
            if not self.len_case__190_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__190_elt_field0, self._io, u"/types/case__190_elt_field0_0/seq/0")
            self._raw_case__190_elt_field0 = self._io.read_bytes(self.len_case__190_elt_field0)
            _io__raw_case__190_elt_field0 = KaitaiStream(BytesIO(self._raw_case__190_elt_field0))
            self.case__190_elt_field0 = Id015PtlimaptOperationUnsigned.Case190EltField0(_io__raw_case__190_elt_field0, self, self._root)


    class Case28Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__28_field1_elt_field0 = self._io.read_u1()
            self.case__28_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Commitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.messages = Id015PtlimaptOperationUnsigned.Messages0(self._io, self, self._root)
            self.predecessor = Id015PtlimaptOperationUnsigned.Predecessor(self._io, self, self._root)
            self.inbox_merkle_root = self._io.read_bytes(32)


    class Case2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__2_field0 = self._io.read_s2be()
            self.case__2_field1 = self._io.read_bytes(32)
            self.case__2_field2 = self._io.read_bytes(32)
            self.case__2_field3 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case162EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__162_elt_field0 = self._io.read_bytes_full()


    class TicketsInfoEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.ty = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.ticketer = Id015PtlimaptOperationUnsigned.Id015PtlimaptContractId(self._io, self, self._root)
            self.amount = Id015PtlimaptOperationUnsigned.Amount(self._io, self, self._root)
            self.claimer = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)


    class Case184EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__184_elt_field0 = self._io.read_u1()
            if not self.len_case__184_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__184_elt_field0, self._io, u"/types/case__184_elt_field0_0/seq/0")
            self._raw_case__184_elt_field0 = self._io.read_bytes(self.len_case__184_elt_field0)
            _io__raw_case__184_elt_field0 = KaitaiStream(BytesIO(self._raw_case__184_elt_field0))
            self.case__184_elt_field0 = Id015PtlimaptOperationUnsigned.Case184EltField0(_io__raw_case__184_elt_field0, self, self._root)


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Bool, self._io.read_u1())
            if self.delegate_tag == Id015PtlimaptOperationUnsigned.Bool.true:
                self.delegate = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)



    class Case56(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__56_field0 = self._io.read_u1()
            self.case__56_field1 = []
            for i in range(14):
                self.case__56_field1.append(Id015PtlimaptOperationUnsigned.Case56Field1Entries(self._io, self, self._root))



    class Case167Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__167_elt_field0 = Id015PtlimaptOperationUnsigned.Case167EltField00(self._io, self, self._root)
            self.case__167_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class RegisterGlobalConstant(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.value = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case53(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__53_field0 = self._io.read_u2be()
            self.case__53_field1 = []
            for i in range(13):
                self.case__53_field1.append(Id015PtlimaptOperationUnsigned.Case53Field1Entries(self._io, self, self._root))



    class Case161EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__161_elt_field0 = self._io.read_bytes_full()


    class Solution(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.solution_field0 = self._io.read_bytes(100)
            self.solution_field1 = self._io.read_bytes(100)


    class Case67Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__67_field1_elt = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case32Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__32_field1_elt_field0 = self._io.read_u1()
            self.case__32_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case166EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__166_elt_field0 = self._io.read_bytes_full()


    class Case64Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__64_field1_elt = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case129Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__129_elt_field0 = Id015PtlimaptOperationUnsigned.Case129EltField00(self._io, self, self._root)
            self.case__129_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case19Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__19_field1_elt_field0 = self._io.read_u1()
            self.case__19_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case176Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__176_elt_field0 = Id015PtlimaptOperationUnsigned.Case176EltField00(self._io, self, self._root)
            self.case__176_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case186EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__186_elt_field0 = self._io.read_u1()
            if not self.len_case__186_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__186_elt_field0, self._io, u"/types/case__186_elt_field0_0/seq/0")
            self._raw_case__186_elt_field0 = self._io.read_bytes(self.len_case__186_elt_field0)
            _io__raw_case__186_elt_field0 = KaitaiStream(BytesIO(self._raw_case__186_elt_field0))
            self.case__186_elt_field0 = Id015PtlimaptOperationUnsigned.Case186EltField0(_io__raw_case__186_elt_field0, self, self._root)


    class ScRollupRecoverBond(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)


    class Case37Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__37_field1_elt_field0 = self._io.read_u1()
            self.case__37_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case176EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__176_elt_field0 = self._io.read_bytes_full()


    class PreviousMessageResult(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.context_hash = self._io.read_bytes(32)
            self.withdraw_list_hash = self._io.read_bytes(32)


    class Case20Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__20_field1_elt_field0 = self._io.read_u1()
            self.case__20_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class PayloadEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_elt = self._io.read_bytes(32)


    class Case217(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__217_field0 = self._io.read_u2be()
            self.case__217_field1 = Id015PtlimaptOperationUnsigned.Case217Field10(self._io, self, self._root)
            self.case__217_field2 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case219Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__219_field1 = self._io.read_bytes_full()


    class Case130EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__130_elt_field0 = self._io.read_bytes_full()


    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = Id015PtlimaptOperationUnsigned.Sequence(_io__raw_sequence, self, self._root)


    class Case179Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__179_elt_field0 = Id015PtlimaptOperationUnsigned.Case179EltField00(self._io, self, self._root)
            self.case__179_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case170EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__170_elt_field0 = self._io.read_bytes_full()


    class Case173EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__173_elt_field0 = self._io.read_u1()
            if not self.len_case__173_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__173_elt_field0, self._io, u"/types/case__173_elt_field0_0/seq/0")
            self._raw_case__173_elt_field0 = self._io.read_bytes(self.len_case__173_elt_field0)
            _io__raw_case__173_elt_field0 = KaitaiStream(BytesIO(self._raw_case__173_elt_field0))
            self.case__173_elt_field0 = Id015PtlimaptOperationUnsigned.Case173EltField0(_io__raw_case__173_elt_field0, self, self._root)


    class Case6(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__6_field0 = self._io.read_s4be()
            self.case__6_field1 = []
            for i in range(1):
                self.case__6_field1.append(Id015PtlimaptOperationUnsigned.Case6Field1Entries(self._io, self, self._root))



    class Case139Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__139_elt_field0 = Id015PtlimaptOperationUnsigned.Case139EltField00(self._io, self, self._root)
            self.case__139_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class ContentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__operation__alpha__contents = Id015PtlimaptOperationUnsigned.Id015PtlimaptOperationAlphaContents(self._io, self, self._root)


    class Case158EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__158_elt_field0 = self._io.read_bytes_full()


    class Case4Field1Entries0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__4_field1_elt_field0 = self._io.read_u1()
            self.case__4_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class PreviousMessageResultPath(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.previous_message_result_path_entries = []
            i = 0
            while not self._io.is_eof():
                self.previous_message_result_path_entries.append(Id015PtlimaptOperationUnsigned.PreviousMessageResultPathEntries(self._io, self, self._root))
                i += 1



    class Case168Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__168_elt_field0 = Id015PtlimaptOperationUnsigned.Case168EltField00(self._io, self, self._root)
            self.case__168_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case186EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__186_elt_field0 = self._io.read_bytes_full()


    class ScRollupAddMessages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.message = Id015PtlimaptOperationUnsigned.Message1(self._io, self, self._root)


    class Case172EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__172_elt_field0 = self._io.read_u1()
            if not self.len_case__172_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__172_elt_field0, self._io, u"/types/case__172_elt_field0_0/seq/0")
            self._raw_case__172_elt_field0 = self._io.read_bytes(self.len_case__172_elt_field0)
            _io__raw_case__172_elt_field0 = KaitaiStream(BytesIO(self._raw_case__172_elt_field0))
            self.case__172_elt_field0 = Id015PtlimaptOperationUnsigned.Case172EltField0(_io__raw_case__172_elt_field0, self, self._root)


    class Id015PtlimaptOperationAlphaUnsignedOperation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__operation__alpha__unsigned_operation = operation__shell_header.OperationShellHeader(self._io)
            self.contents = []
            i = 0
            while not self._io.is_eof():
                self.contents.append(Id015PtlimaptOperationUnsigned.ContentsEntries(self._io, self, self._root))
                i += 1



    class Case174EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__174_elt_field0 = self._io.read_bytes_full()


    class Case38Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__38_field1_elt_field0 = self._io.read_u1()
            self.case__38_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case150EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__150_elt_field0 = self._io.read_u1()
            if not self.len_case__150_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__150_elt_field0, self._io, u"/types/case__150_elt_field0_0/seq/0")
            self._raw_case__150_elt_field0 = self._io.read_bytes(self.len_case__150_elt_field0)
            _io__raw_case__150_elt_field0 = KaitaiStream(BytesIO(self._raw_case__150_elt_field0))
            self.case__150_elt_field0 = Id015PtlimaptOperationUnsigned.Case150EltField0(_io__raw_case__150_elt_field0, self, self._root)


    class Case174Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__174_elt_field0 = Id015PtlimaptOperationUnsigned.Case174EltField00(self._io, self, self._root)
            self.case__174_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(Id015PtlimaptOperationUnsigned.SequenceEntries(self._io, self, self._root))
                i += 1



    class Case48(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__48_field0 = self._io.read_u1()
            self.case__48_field1 = []
            for i in range(12):
                self.case__48_field1.append(Id015PtlimaptOperationUnsigned.Case48Field1Entries(self._io, self, self._root))



    class PreviousMessageResultPath0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_previous_message_result_path = self._io.read_u4be()
            if not self.len_previous_message_result_path <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_previous_message_result_path, self._io, u"/types/previous_message_result_path_0/seq/0")
            self._raw_previous_message_result_path = self._io.read_bytes(self.len_previous_message_result_path)
            _io__raw_previous_message_result_path = KaitaiStream(BytesIO(self._raw_previous_message_result_path))
            self.previous_message_result_path = Id015PtlimaptOperationUnsigned.PreviousMessageResultPath(_io__raw_previous_message_result_path, self, self._root)


    class Case133EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__133_elt_field0 = self._io.read_bytes_full()


    class Case149EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__149_elt_field0 = self._io.read_u1()
            if not self.len_case__149_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__149_elt_field0, self._io, u"/types/case__149_elt_field0_0/seq/0")
            self._raw_case__149_elt_field0 = self._io.read_bytes(self.len_case__149_elt_field0)
            _io__raw_case__149_elt_field0 = KaitaiStream(BytesIO(self._raw_case__149_elt_field0))
            self.case__149_elt_field0 = Id015PtlimaptOperationUnsigned.Case149EltField0(_io__raw_case__149_elt_field0, self, self._root)


    class Bh10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh1 = self._io.read_u4be()
            if not self.len_bh1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh1, self._io, u"/types/bh1_0/seq/0")
            self._raw_bh1 = self._io.read_bytes(self.len_bh1)
            _io__raw_bh1 = KaitaiStream(BytesIO(self._raw_bh1))
            self.bh1 = Id015PtlimaptOperationUnsigned.Bh1(_io__raw_bh1, self, self._root)


    class Case144EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__144_elt_field0 = self._io.read_bytes_full()


    class Case133Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__133_elt_field0 = Id015PtlimaptOperationUnsigned.Case133EltField00(self._io, self, self._root)
            self.case__133_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case140EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__140_elt_field0 = self._io.read_u1()
            if not self.len_case__140_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__140_elt_field0, self._io, u"/types/case__140_elt_field0_0/seq/0")
            self._raw_case__140_elt_field0 = self._io.read_bytes(self.len_case__140_elt_field0)
            _io__raw_case__140_elt_field0 = KaitaiStream(BytesIO(self._raw_case__140_elt_field0))
            self.case__140_elt_field0 = Id015PtlimaptOperationUnsigned.Case140EltField0(_io__raw_case__140_elt_field0, self, self._root)


    class Case183EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__183_elt_field0 = self._io.read_bytes_full()


    class Case131EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_elt_field0 = self._io.read_bytes_full()


    class Case3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__3_field0 = self._io.read_s2be()
            self.case__3_field1 = self._io.read_bytes(32)
            self.case__3_field2 = self._io.read_bytes(32)
            self.case__3_field3 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Op22(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_2/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id015PtlimaptOperationUnsigned.Op21(_io__raw_op2, self, self._root)


    class Case136EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__136_elt_field0 = self._io.read_bytes_full()


    class Case45Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__45_field1_elt_field0 = self._io.read_u1()
            self.case__45_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case59(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__59_field0 = self._io.read_s8be()
            self.case__59_field1 = []
            for i in range(14):
                self.case__59_field1.append(Id015PtlimaptOperationUnsigned.Case59Field1Entries(self._io, self, self._root))



    class ScRollupTimeout(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.stakers = Id015PtlimaptOperationUnsigned.Stakers(self._io, self, self._root)


    class CircuitsInfoEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.circuits_info_elt_field0 = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.circuits_info_elt_field1 = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Bool, self._io.read_u1())


    class Case190Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__190_elt_field0 = Id015PtlimaptOperationUnsigned.Case190EltField00(self._io, self, self._root)
            self.case__190_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case29(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__29_field0 = self._io.read_u2be()
            self.case__29_field1 = []
            for i in range(7):
                self.case__29_field1.append(Id015PtlimaptOperationUnsigned.Case29Field1Entries(self._io, self, self._root))



    class Message0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_entries.append(Id015PtlimaptOperationUnsigned.MessageEntries(self._io, self, self._root))
                i += 1



    class Case26Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__26_field1_elt_field0 = self._io.read_u1()
            self.case__26_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class InboxProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.message_counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.serialized_proof = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Id015PtlimaptInlinedEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__endorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id015PtlimaptOperationUnsigned.Id015PtlimaptInlinedEndorsementMempoolContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Bool, self._io.read_u1())
            if self.signature_tag == Id015PtlimaptOperationUnsigned.Bool.true:
                self.signature = self._io.read_bytes(64)



    class Case16Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__16_field1_elt_field0 = self._io.read_u1()
            self.case__16_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case211Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__211_field1 = self._io.read_u1()
            if not self.len_case__211_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__211_field1, self._io, u"/types/case__211_field1_0/seq/0")
            self._raw_case__211_field1 = self._io.read_bytes(self.len_case__211_field1)
            _io__raw_case__211_field1 = KaitaiStream(BytesIO(self._raw_case__211_field1))
            self.case__211_field1 = Id015PtlimaptOperationUnsigned.Case211Field1(_io__raw_case__211_field1, self, self._root)


    class Case1930(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__193 = self._io.read_u2be()
            if not self.len_case__193 <= 65535:
                raise kaitaistruct.ValidationGreaterThanError(65535, self.len_case__193, self._io, u"/types/case__193_0/seq/0")
            self._raw_case__193 = self._io.read_bytes(self.len_case__193)
            _io__raw_case__193 = KaitaiStream(BytesIO(self._raw_case__193))
            self.case__193 = Id015PtlimaptOperationUnsigned.Case193(_io__raw_case__193, self, self._root)


    class OpEltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_elt_field1_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.OpEltField1Tag, self._io.read_u1())
            if self.op_elt_field1_tag == Id015PtlimaptOperationUnsigned.OpEltField1Tag.some:
                self.some = Id015PtlimaptOperationUnsigned.Some(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Case131Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_elt_field0 = Id015PtlimaptOperationUnsigned.Case131EltField00(self._io, self, self._root)
            self.case__131_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case138Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__138_elt_field0 = Id015PtlimaptOperationUnsigned.Case138EltField00(self._io, self, self._root)
            self.case__138_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Op0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op = self._io.read_u4be()
            if not self.len_op <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op, self._io, u"/types/op_0/seq/0")
            self._raw_op = self._io.read_bytes(self.len_op)
            _io__raw_op = KaitaiStream(BytesIO(self._raw_op))
            self.op = Id015PtlimaptOperationUnsigned.Op(_io__raw_op, self, self._root)


    class Proof1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pvm_step = Id015PtlimaptOperationUnsigned.PvmStep(self._io, self, self._root)
            self.input_proof_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Bool, self._io.read_u1())
            if self.input_proof_tag == Id015PtlimaptOperationUnsigned.Bool.true:
                self.input_proof = Id015PtlimaptOperationUnsigned.InputProof(self._io, self, self._root)



    class Bh1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__block_header__alpha__full_header = Id015PtlimaptOperationUnsigned.Id015PtlimaptBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Case63(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__63_field0 = self._io.read_s8be()
            self.case__63_field1 = Id015PtlimaptOperationUnsigned.Case63Field10(self._io, self, self._root)


    class Case34Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__34_field1_elt_field0 = self._io.read_u1()
            self.case__34_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Payload0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_payload = self._io.read_u4be()
            if not self.len_payload <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_payload, self._io, u"/types/payload_0/seq/0")
            self._raw_payload = self._io.read_bytes(self.len_payload)
            _io__raw_payload = KaitaiStream(BytesIO(self._raw_payload))
            self.payload = Id015PtlimaptOperationUnsigned.Payload(_io__raw_payload, self, self._root)


    class Case161EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__161_elt_field0 = self._io.read_u1()
            if not self.len_case__161_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__161_elt_field0, self._io, u"/types/case__161_elt_field0_0/seq/0")
            self._raw_case__161_elt_field0 = self._io.read_bytes(self.len_case__161_elt_field0)
            _io__raw_case__161_elt_field0 = KaitaiStream(BytesIO(self._raw_case__161_elt_field0))
            self.case__161_elt_field0 = Id015PtlimaptOperationUnsigned.Case161EltField0(_io__raw_case__161_elt_field0, self, self._root)


    class Case156EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__156_elt_field0 = self._io.read_u1()
            if not self.len_case__156_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__156_elt_field0, self._io, u"/types/case__156_elt_field0_0/seq/0")
            self._raw_case__156_elt_field0 = self._io.read_bytes(self.len_case__156_elt_field0)
            _io__raw_case__156_elt_field0 = KaitaiStream(BytesIO(self._raw_case__156_elt_field0))
            self.case__156_elt_field0 = Id015PtlimaptOperationUnsigned.Case156EltField0(_io__raw_case__156_elt_field0, self, self._root)


    class Dissection0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_dissection = self._io.read_u4be()
            if not self.len_dissection <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_dissection, self._io, u"/types/dissection_0/seq/0")
            self._raw_dissection = self._io.read_bytes(self.len_dissection)
            _io__raw_dissection = KaitaiStream(BytesIO(self._raw_dissection))
            self.dissection = Id015PtlimaptOperationUnsigned.Dissection(_io__raw_dissection, self, self._root)


    class Case177EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__177_elt_field0 = self._io.read_bytes_full()


    class Case142EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__142_elt_field0 = self._io.read_bytes_full()


    class Case25Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__25_field1_elt_field0 = self._io.read_u1()
            self.case__25_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class TxRollupRejection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptTxRollupId(self._io, self, self._root)
            self.level = self._io.read_s4be()
            self.message = Id015PtlimaptOperationUnsigned.Message(self._io, self, self._root)
            self.message_position = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.message_path = Id015PtlimaptOperationUnsigned.MessagePath0(self._io, self, self._root)
            self.message_result_hash = self._io.read_bytes(32)
            self.message_result_path = Id015PtlimaptOperationUnsigned.MessageResultPath0(self._io, self, self._root)
            self.previous_message_result = Id015PtlimaptOperationUnsigned.PreviousMessageResult(self._io, self, self._root)
            self.previous_message_result_path = Id015PtlimaptOperationUnsigned.PreviousMessageResultPath0(self._io, self, self._root)
            self.proof = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class SetDepositsLimit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.limit_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Bool, self._io.read_u1())
            if self.limit_tag == Id015PtlimaptOperationUnsigned.Bool.true:
                self.limit = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)



    class Case184Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__184_elt_field0 = Id015PtlimaptOperationUnsigned.Case184EltField00(self._io, self, self._root)
            self.case__184_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case185EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__185_elt_field0 = self._io.read_bytes_full()


    class Case159EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__159_elt_field0 = self._io.read_u1()
            if not self.len_case__159_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__159_elt_field0, self._io, u"/types/case__159_elt_field0_0/seq/0")
            self._raw_case__159_elt_field0 = self._io.read_bytes(self.len_case__159_elt_field0)
            _io__raw_case__159_elt_field0 = KaitaiStream(BytesIO(self._raw_case__159_elt_field0))
            self.case__159_elt_field0 = Id015PtlimaptOperationUnsigned.Case159EltField0(_io__raw_case__159_elt_field0, self, self._root)


    class Case169Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__169_elt_field0 = Id015PtlimaptOperationUnsigned.Case169EltField00(self._io, self, self._root)
            self.case__169_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case137Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__137_elt_field0 = Id015PtlimaptOperationUnsigned.Case137EltField00(self._io, self, self._root)
            self.case__137_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case15(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__15_field0 = self._io.read_s8be()
            self.case__15_field1 = []
            for i in range(3):
                self.case__15_field1.append(Id015PtlimaptOperationUnsigned.Case15Field1Entries(self._io, self, self._root))



    class Case133EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__133_elt_field0 = self._io.read_u1()
            if not self.len_case__133_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__133_elt_field0, self._io, u"/types/case__133_elt_field0_0/seq/0")
            self._raw_case__133_elt_field0 = self._io.read_bytes(self.len_case__133_elt_field0)
            _io__raw_case__133_elt_field0 = KaitaiStream(BytesIO(self._raw_case__133_elt_field0))
            self.case__133_elt_field0 = Id015PtlimaptOperationUnsigned.Case133EltField0(_io__raw_case__133_elt_field0, self, self._root)


    class Message1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message = self._io.read_u4be()
            if not self.len_message <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message, self._io, u"/types/message_1/seq/0")
            self._raw_message = self._io.read_bytes(self.len_message)
            _io__raw_message = KaitaiStream(BytesIO(self._raw_message))
            self.message = Id015PtlimaptOperationUnsigned.Message0(_io__raw_message, self, self._root)


    class Case176EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__176_elt_field0 = self._io.read_u1()
            if not self.len_case__176_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__176_elt_field0, self._io, u"/types/case__176_elt_field0_0/seq/0")
            self._raw_case__176_elt_field0 = self._io.read_bytes(self.len_case__176_elt_field0)
            _io__raw_case__176_elt_field0 = KaitaiStream(BytesIO(self._raw_case__176_elt_field0))
            self.case__176_elt_field0 = Id015PtlimaptOperationUnsigned.Case176EltField0(_io__raw_case__176_elt_field0, self, self._root)


    class Case152Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__152_elt_field0 = Id015PtlimaptOperationUnsigned.Case152EltField00(self._io, self, self._root)
            self.case__152_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class ScRollupRefute(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptRollupAddress(self._io, self, self._root)
            self.opponent = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.refutation_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Bool, self._io.read_u1())
            if self.refutation_tag == Id015PtlimaptOperationUnsigned.Bool.true:
                self.refutation = Id015PtlimaptOperationUnsigned.Refutation(self._io, self, self._root)



    class Op10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_0/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id015PtlimaptOperationUnsigned.Op1(_io__raw_op1, self, self._root)


    class Case63Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__63_field1_elt_field0 = self._io.read_u1()
            self.case__63_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class InitState0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_init_state = self._io.read_u4be()
            if not self.len_init_state <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_init_state, self._io, u"/types/init_state_0/seq/0")
            self._raw_init_state = self._io.read_bytes(self.len_init_state)
            _io__raw_init_state = KaitaiStream(BytesIO(self._raw_init_state))
            self.init_state = Id015PtlimaptOperationUnsigned.InitState(_io__raw_init_state, self, self._root)


    class Case156Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__156_elt_field0 = Id015PtlimaptOperationUnsigned.Case156EltField00(self._io, self, self._root)
            self.case__156_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case209Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__209_field1 = self._io.read_u1()
            if not self.len_case__209_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__209_field1, self._io, u"/types/case__209_field1_0/seq/0")
            self._raw_case__209_field1 = self._io.read_bytes(self.len_case__209_field1)
            _io__raw_case__209_field1 = KaitaiStream(BytesIO(self._raw_case__209_field1))
            self.case__209_field1 = Id015PtlimaptOperationUnsigned.Case209Field1(_io__raw_case__209_field1, self, self._root)


    class Case147EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__147_elt_field0 = self._io.read_bytes_full()


    class Op21(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__preendorsement = Id015PtlimaptOperationUnsigned.Id015PtlimaptInlinedPreendorsement(self._io, self, self._root)


    class Case20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__20_field0 = self._io.read_u1()
            self.case__20_field1 = []
            for i in range(5):
                self.case__20_field1.append(Id015PtlimaptOperationUnsigned.Case20Field1Entries(self._io, self, self._root))



    class Case155EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__155_elt_field0 = self._io.read_bytes_full()


    class Proof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proof_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.ProofTag, self._io.read_u1())
            if self.proof_tag == Id015PtlimaptOperationUnsigned.ProofTag.case__0:
                self.case__0 = Id015PtlimaptOperationUnsigned.Case0(self._io, self, self._root)

            if self.proof_tag == Id015PtlimaptOperationUnsigned.ProofTag.case__2:
                self.case__2 = Id015PtlimaptOperationUnsigned.Case2(self._io, self, self._root)

            if self.proof_tag == Id015PtlimaptOperationUnsigned.ProofTag.case__1:
                self.case__1 = Id015PtlimaptOperationUnsigned.Case1(self._io, self, self._root)

            if self.proof_tag == Id015PtlimaptOperationUnsigned.ProofTag.case__3:
                self.case__3 = Id015PtlimaptOperationUnsigned.Case3(self._io, self, self._root)



    class Case180Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__180_elt_field0 = Id015PtlimaptOperationUnsigned.Case180EltField00(self._io, self, self._root)
            self.case__180_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case151EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__151_elt_field0 = self._io.read_bytes_full()


    class Case191EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__191_elt_field0 = self._io.read_bytes_full()


    class Commitment0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.compressed_state = self._io.read_bytes(32)
            self.inbox_level = self._io.read_s4be()
            self.predecessor = self._io.read_bytes(32)
            self.number_of_ticks = self._io.read_s8be()


    class Case143EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__143_elt_field0 = self._io.read_u1()
            if not self.len_case__143_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__143_elt_field0, self._io, u"/types/case__143_elt_field0_0/seq/0")
            self._raw_case__143_elt_field0 = self._io.read_bytes(self.len_case__143_elt_field0)
            _io__raw_case__143_elt_field0 = KaitaiStream(BytesIO(self._raw_case__143_elt_field0))
            self.case__143_elt_field0 = Id015PtlimaptOperationUnsigned.Case143EltField0(_io__raw_case__143_elt_field0, self, self._root)


    class Case37(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__37_field0 = self._io.read_u2be()
            self.case__37_field1 = []
            for i in range(9):
                self.case__37_field1.append(Id015PtlimaptOperationUnsigned.Case37Field1Entries(self._io, self, self._root))



    class Case35Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__35_field1_elt_field0 = self._io.read_u1()
            self.case__35_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.balance = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Bool, self._io.read_u1())
            if self.delegate_tag == Id015PtlimaptOperationUnsigned.Bool.true:
                self.delegate = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)

            self.script = Id015PtlimaptOperationUnsigned.Id015PtlimaptScriptedContracts(self._io, self, self._root)


    class Case149Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__149_elt_field0 = Id015PtlimaptOperationUnsigned.Case149EltField00(self._io, self, self._root)
            self.case__149_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class DrainDelegate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.consensus_key = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.delegate = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.destination = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)


    class Case46Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__46_field1_elt_field0 = self._io.read_u1()
            self.case__46_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case164EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__164_elt_field0 = self._io.read_bytes_full()


    class Case26(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__26_field0 = self._io.read_s4be()
            self.case__26_field1 = []
            for i in range(6):
                self.case__26_field1.append(Id015PtlimaptOperationUnsigned.Case26Field1Entries(self._io, self, self._root))



    class Case31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__31_field0 = self._io.read_s8be()
            self.case__31_field1 = []
            for i in range(7):
                self.case__31_field1.append(Id015PtlimaptOperationUnsigned.Case31Field1Entries(self._io, self, self._root))



    class Case19(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__19_field0 = self._io.read_s8be()
            self.case__19_field1 = []
            for i in range(4):
                self.case__19_field1.append(Id015PtlimaptOperationUnsigned.Case19Field1Entries(self._io, self, self._root))



    class Id015PtlimaptContractIdOriginated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__contract_id__originated_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Id015PtlimaptContractIdOriginatedTag, self._io.read_u1())
            if self.id_015__ptlimapt__contract_id__originated_tag == Id015PtlimaptOperationUnsigned.Id015PtlimaptContractIdOriginatedTag.originated:
                self.originated = Id015PtlimaptOperationUnsigned.Originated(self._io, self, self._root)



    class Micheline015PtlimaptMichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__015__ptlimapt__michelson_v1__expression_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1ExpressionTag.int:
                self.int = Id015PtlimaptOperationUnsigned.Z(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1ExpressionTag.string:
                self.string = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1ExpressionTag.sequence:
                self.sequence = Id015PtlimaptOperationUnsigned.Sequence0(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = Id015PtlimaptOperationUnsigned.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = Id015PtlimaptOperationUnsigned.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = Id015PtlimaptOperationUnsigned.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = Id015PtlimaptOperationUnsigned.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = Id015PtlimaptOperationUnsigned.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = Id015PtlimaptOperationUnsigned.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = Id015PtlimaptOperationUnsigned.PrimGeneric(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1ExpressionTag.bytes:
                self.bytes = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)



    class Case43Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__43_field1_elt_field0 = self._io.read_u1()
            self.case__43_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case160EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__160_elt_field0 = self._io.read_u1()
            if not self.len_case__160_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__160_elt_field0, self._io, u"/types/case__160_elt_field0_0/seq/0")
            self._raw_case__160_elt_field0 = self._io.read_bytes(self.len_case__160_elt_field0)
            _io__raw_case__160_elt_field0 = KaitaiStream(BytesIO(self._raw_case__160_elt_field0))
            self.case__160_elt_field0 = Id015PtlimaptOperationUnsigned.Case160EltField0(_io__raw_case__160_elt_field0, self, self._root)


    class Case41(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__41_field0 = self._io.read_u2be()
            self.case__41_field1 = []
            for i in range(10):
                self.case__41_field1.append(Id015PtlimaptOperationUnsigned.Case41Field1Entries(self._io, self, self._root))



    class Case209(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__209_field0 = self._io.read_u2be()
            self.case__209_field1 = Id015PtlimaptOperationUnsigned.Case209Field10(self._io, self, self._root)
            self.case__209_field2 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field0 = self._io.read_s2be()
            self.case__0_field1 = self._io.read_bytes(32)
            self.case__0_field2 = self._io.read_bytes(32)
            self.case__0_field3 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case173EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__173_elt_field0 = self._io.read_bytes_full()


    class Case12Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__12_field1_elt_field0 = self._io.read_u1()
            self.case__12_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case14Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__14_field1_elt_field0 = self._io.read_u1()
            self.case__14_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Message(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.MessageTag, self._io.read_u1())
            if self.message_tag == Id015PtlimaptOperationUnsigned.MessageTag.batch:
                self.batch = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)

            if self.message_tag == Id015PtlimaptOperationUnsigned.MessageTag.deposit:
                self.deposit = Id015PtlimaptOperationUnsigned.Deposit(self._io, self, self._root)



    class Case30Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__30_field1_elt_field0 = self._io.read_u1()
            self.case__30_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case141Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__141_elt_field0 = Id015PtlimaptOperationUnsigned.Case141EltField00(self._io, self, self._root)
            self.case__141_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case178EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__178_elt_field0 = self._io.read_bytes_full()


    class InitState(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.init_state_entries = []
            i = 0
            while not self._io.is_eof():
                self.init_state_entries.append(Id015PtlimaptOperationUnsigned.InitStateEntries(self._io, self, self._root))
                i += 1



    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(Id015PtlimaptOperationUnsigned.ArgsEntries(self._io, self, self._root))
                i += 1



    class Case172EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__172_elt_field0 = self._io.read_bytes_full()


    class Case139EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__139_elt_field0 = self._io.read_bytes_full()


    class Case209Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__209_field1 = self._io.read_bytes_full()


    class ZkRollupOrigination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.public_parameters = Id015PtlimaptOperationUnsigned.PublicParameters(self._io, self, self._root)
            self.circuits_info = Id015PtlimaptOperationUnsigned.CircuitsInfo0(self._io, self, self._root)
            self.init_state = Id015PtlimaptOperationUnsigned.InitState0(self._io, self, self._root)
            self.nb_ops = Id015PtlimaptOperationUnsigned.Int31(self._io, self, self._root)


    class Case159EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__159_elt_field0 = self._io.read_bytes_full()


    class Case216(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__216_field0 = self._io.read_u1()
            self.case__216_field1 = Id015PtlimaptOperationUnsigned.Case216Field10(self._io, self, self._root)
            self.case__216_field2 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Op2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__endorsement = Id015PtlimaptOperationUnsigned.Id015PtlimaptInlinedEndorsement(self._io, self, self._root)


    class Case56Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__56_field1_elt_field0 = self._io.read_u1()
            self.case__56_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case216Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__216_field1 = self._io.read_u1()
            if not self.len_case__216_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__216_field1, self._io, u"/types/case__216_field1_0/seq/0")
            self._raw_case__216_field1 = self._io.read_bytes(self.len_case__216_field1)
            _io__raw_case__216_field1 = KaitaiStream(BytesIO(self._raw_case__216_field1))
            self.case__216_field1 = Id015PtlimaptOperationUnsigned.Case216Field1(_io__raw_case__216_field1, self, self._root)


    class Case191(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__191_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__191_entries.append(Id015PtlimaptOperationUnsigned.Case191Entries(self._io, self, self._root))
                i += 1



    class Case153Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__153_elt_field0 = Id015PtlimaptOperationUnsigned.Case153EltField00(self._io, self, self._root)
            self.case__153_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case63Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__63_field1 = self._io.read_u4be()
            if not self.len_case__63_field1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__63_field1, self._io, u"/types/case__63_field1_0/seq/0")
            self._raw_case__63_field1 = self._io.read_bytes(self.len_case__63_field1)
            _io__raw_case__63_field1 = KaitaiStream(BytesIO(self._raw_case__63_field1))
            self.case__63_field1 = Id015PtlimaptOperationUnsigned.Case63Field1(_io__raw_case__63_field1, self, self._root)


    class Case7(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__7_field0 = self._io.read_s8be()
            self.case__7_field1 = []
            for i in range(1):
                self.case__7_field1.append(Id015PtlimaptOperationUnsigned.Case7Field1Entries(self._io, self, self._root))



    class Case218Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__218_field1 = self._io.read_bytes_full()


    class Case15Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__15_field1_elt_field0 = self._io.read_u1()
            self.case__15_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case156EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__156_elt_field0 = self._io.read_bytes_full()


    class Case174EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__174_elt_field0 = self._io.read_u1()
            if not self.len_case__174_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__174_elt_field0, self._io, u"/types/case__174_elt_field0_0/seq/0")
            self._raw_case__174_elt_field0 = self._io.read_bytes(self.len_case__174_elt_field0)
            _io__raw_case__174_elt_field0 = KaitaiStream(BytesIO(self._raw_case__174_elt_field0))
            self.case__174_elt_field0 = Id015PtlimaptOperationUnsigned.Case174EltField0(_io__raw_case__174_elt_field0, self, self._root)


    class Case160EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__160_elt_field0 = self._io.read_bytes_full()


    class TxRollupReturnBond(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptTxRollupId(self._io, self, self._root)


    class Case169EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__169_elt_field0 = self._io.read_u1()
            if not self.len_case__169_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__169_elt_field0, self._io, u"/types/case__169_elt_field0_0/seq/0")
            self._raw_case__169_elt_field0 = self._io.read_bytes(self.len_case__169_elt_field0)
            _io__raw_case__169_elt_field0 = KaitaiStream(BytesIO(self._raw_case__169_elt_field0))
            self.case__169_elt_field0 = Id015PtlimaptOperationUnsigned.Case169EltField0(_io__raw_case__169_elt_field0, self, self._root)


    class Dissection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dissection_entries = []
            i = 0
            while not self._io.is_eof():
                self.dissection_entries.append(Id015PtlimaptOperationUnsigned.DissectionEntries(self._io, self, self._root))
                i += 1



    class TicketsInfo0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_tickets_info = self._io.read_u4be()
            if not self.len_tickets_info <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_tickets_info, self._io, u"/types/tickets_info_0/seq/0")
            self._raw_tickets_info = self._io.read_bytes(self.len_tickets_info)
            _io__raw_tickets_info = KaitaiStream(BytesIO(self._raw_tickets_info))
            self.tickets_info = Id015PtlimaptOperationUnsigned.TicketsInfo(_io__raw_tickets_info, self, self._root)


    class TxRollupFinalizeCommitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptTxRollupId(self._io, self, self._root)


    class Case17Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__17_field1_elt_field0 = self._io.read_u1()
            self.case__17_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class UpdateConsensusKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.pk = Id015PtlimaptOperationUnsigned.PublicKey(self._io, self, self._root)


    class Case145EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__145_elt_field0 = self._io.read_bytes_full()


    class TxRollupOrigination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)


    class Case47(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__47_field0 = self._io.read_s8be()
            self.case__47_field1 = []
            for i in range(11):
                self.case__47_field1.append(Id015PtlimaptOperationUnsigned.Case47Field1Entries(self._io, self, self._root))



    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptOperationUnsigned.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class Case21Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__21_field1_elt_field0 = self._io.read_u1()
            self.case__21_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case60(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__60_field0 = self._io.read_u1()
            self.case__60_field1 = Id015PtlimaptOperationUnsigned.Case60Field10(self._io, self, self._root)


    class Case52Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__52_field1_elt_field0 = self._io.read_u1()
            self.case__52_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class MessageResultPath(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_path_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_result_path_entries.append(Id015PtlimaptOperationUnsigned.MessageResultPathEntries(self._io, self, self._root))
                i += 1



    class Id015PtlimaptMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__michelson__v1__primitives = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Id015PtlimaptMichelsonV1Primitives, self._io.read_u1())


    class Case175Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__175_elt_field0 = Id015PtlimaptOperationUnsigned.Case175EltField00(self._io, self, self._root)
            self.case__175_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case177EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__177_elt_field0 = self._io.read_u1()
            if not self.len_case__177_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__177_elt_field0, self._io, u"/types/case__177_elt_field0_0/seq/0")
            self._raw_case__177_elt_field0 = self._io.read_bytes(self.len_case__177_elt_field0)
            _io__raw_case__177_elt_field0 = KaitaiStream(BytesIO(self._raw_case__177_elt_field0))
            self.case__177_elt_field0 = Id015PtlimaptOperationUnsigned.Case177EltField0(_io__raw_case__177_elt_field0, self, self._root)


    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class Case181EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__181_elt_field0 = self._io.read_u1()
            if not self.len_case__181_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__181_elt_field0, self._io, u"/types/case__181_elt_field0_0/seq/0")
            self._raw_case__181_elt_field0 = self._io.read_bytes(self.len_case__181_elt_field0)
            _io__raw_case__181_elt_field0 = KaitaiStream(BytesIO(self._raw_case__181_elt_field0))
            self.case__181_elt_field0 = Id015PtlimaptOperationUnsigned.Case181EltField0(_io__raw_case__181_elt_field0, self, self._root)


    class Case8Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__8_field1_elt_field0 = self._io.read_u1()
            self.case__8_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptOperationUnsigned.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class MessageResultPath0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message_result_path = self._io.read_u4be()
            if not self.len_message_result_path <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message_result_path, self._io, u"/types/message_result_path_0/seq/0")
            self._raw_message_result_path = self._io.read_bytes(self.len_message_result_path)
            _io__raw_message_result_path = KaitaiStream(BytesIO(self._raw_message_result_path))
            self.message_result_path = Id015PtlimaptOperationUnsigned.MessageResultPath(_io__raw_message_result_path, self, self._root)


    class Case58(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__58_field0 = self._io.read_s4be()
            self.case__58_field1 = []
            for i in range(14):
                self.case__58_field1.append(Id015PtlimaptOperationUnsigned.Case58Field1Entries(self._io, self, self._root))



    class Case181EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__181_elt_field0 = self._io.read_bytes_full()


    class MessagePathEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inbox_list_hash = self._io.read_bytes(32)


    class PublicParameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_parameters_field0 = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.public_parameters_field1 = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case24Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__24_field1_elt_field0 = self._io.read_u1()
            self.case__24_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case154EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__154_elt_field0 = self._io.read_u1()
            if not self.len_case__154_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__154_elt_field0, self._io, u"/types/case__154_elt_field0_0/seq/0")
            self._raw_case__154_elt_field0 = self._io.read_bytes(self.len_case__154_elt_field0)
            _io__raw_case__154_elt_field0 = KaitaiStream(BytesIO(self._raw_case__154_elt_field0))
            self.case__154_elt_field0 = Id015PtlimaptOperationUnsigned.Case154EltField0(_io__raw_case__154_elt_field0, self, self._root)


    class InitStateEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.init_state_elt = self._io.read_bytes(32)


    class Case179EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__179_elt_field0 = self._io.read_bytes_full()


    class DalSlotAvailability(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.endorser = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.endorsement = Id015PtlimaptOperationUnsigned.Z(self._io, self, self._root)


    class Slot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.index = self._io.read_u1()
            self.header = self._io.read_bytes(48)


    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class Case135EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__135_elt_field0 = self._io.read_bytes_full()


    class Case153EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__153_elt_field0 = self._io.read_bytes_full()


    class Deposit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sender = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.destination = self._io.read_bytes(20)
            self.ticket_hash = self._io.read_bytes(32)
            self.amount = Id015PtlimaptOperationUnsigned.Amount(self._io, self, self._root)


    class Case44Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__44_field1_elt_field0 = self._io.read_u1()
            self.case__44_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case132Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__132_elt_field0 = Id015PtlimaptOperationUnsigned.Case132EltField00(self._io, self, self._root)
            self.case__132_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class MessagePath(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_path_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_path_entries.append(Id015PtlimaptOperationUnsigned.MessagePathEntries(self._io, self, self._root))
                i += 1



    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = Id015PtlimaptOperationUnsigned.Args(_io__raw_args, self, self._root)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id015PtlimaptOperationUnsigned.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.amount = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.destination = Id015PtlimaptOperationUnsigned.Id015PtlimaptContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Bool, self._io.read_u1())
            if self.parameters_tag == Id015PtlimaptOperationUnsigned.Bool.true:
                self.parameters = Id015PtlimaptOperationUnsigned.Parameters(self._io, self, self._root)



    class Case144EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__144_elt_field0 = self._io.read_u1()
            if not self.len_case__144_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__144_elt_field0, self._io, u"/types/case__144_elt_field0_0/seq/0")
            self._raw_case__144_elt_field0 = self._io.read_bytes(self.len_case__144_elt_field0)
            _io__raw_case__144_elt_field0 = KaitaiStream(BytesIO(self._raw_case__144_elt_field0))
            self.case__144_elt_field0 = Id015PtlimaptOperationUnsigned.Case144EltField0(_io__raw_case__144_elt_field0, self, self._root)


    class Case47Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__47_field1_elt_field0 = self._io.read_u1()
            self.case__47_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case14(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__14_field0 = self._io.read_s4be()
            self.case__14_field1 = []
            for i in range(3):
                self.case__14_field1.append(Id015PtlimaptOperationUnsigned.Case14Field1Entries(self._io, self, self._root))



    class Case218Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__218_field1 = self._io.read_u1()
            if not self.len_case__218_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__218_field1, self._io, u"/types/case__218_field1_0/seq/0")
            self._raw_case__218_field1 = self._io.read_bytes(self.len_case__218_field1)
            _io__raw_case__218_field1 = KaitaiStream(BytesIO(self._raw_case__218_field1))
            self.case__218_field1 = Id015PtlimaptOperationUnsigned.Case218Field1(_io__raw_case__218_field1, self, self._root)


    class Case169EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__169_elt_field0 = self._io.read_bytes_full()


    class Case168EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__168_elt_field0 = self._io.read_u1()
            if not self.len_case__168_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__168_elt_field0, self._io, u"/types/case__168_elt_field0_0/seq/0")
            self._raw_case__168_elt_field0 = self._io.read_bytes(self.len_case__168_elt_field0)
            _io__raw_case__168_elt_field0 = KaitaiStream(BytesIO(self._raw_case__168_elt_field0))
            self.case__168_elt_field0 = Id015PtlimaptOperationUnsigned.Case168EltField0(_io__raw_case__168_elt_field0, self, self._root)


    class Case140Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__140_elt_field0 = Id015PtlimaptOperationUnsigned.Case140EltField00(self._io, self, self._root)
            self.case__140_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case182Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__182_elt_field0 = Id015PtlimaptOperationUnsigned.Case182EltField00(self._io, self, self._root)
            self.case__182_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id015PtlimaptOperationUnsigned.Id015PtlimaptEntrypoint(self._io, self, self._root)
            self.value = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Some(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents = Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.ty = Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.ticketer = Id015PtlimaptOperationUnsigned.Id015PtlimaptContractId(self._io, self, self._root)


    class Case25(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__25_field0 = self._io.read_u2be()
            self.case__25_field1 = []
            for i in range(6):
                self.case__25_field1.append(Id015PtlimaptOperationUnsigned.Case25Field1Entries(self._io, self, self._root))



    class Case4Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__4_field1_elt_field0 = self._io.read_u1()
            self.case__4_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Messages0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_messages = self._io.read_u4be()
            if not self.len_messages <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_messages, self._io, u"/types/messages_0/seq/0")
            self._raw_messages = self._io.read_bytes(self.len_messages)
            _io__raw_messages = KaitaiStream(BytesIO(self._raw_messages))
            self.messages = Id015PtlimaptOperationUnsigned.Messages(_io__raw_messages, self, self._root)


    class Case171Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__171_elt_field0 = Id015PtlimaptOperationUnsigned.Case171EltField00(self._io, self, self._root)
            self.case__171_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptOperationUnsigned.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id015PtlimaptOperationUnsigned.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case1920(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__192 = self._io.read_u1()
            if not self.len_case__192 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__192, self._io, u"/types/case__192_0/seq/0")
            self._raw_case__192 = self._io.read_bytes(self.len_case__192)
            _io__raw_case__192 = KaitaiStream(BytesIO(self._raw_case__192))
            self.case__192 = Id015PtlimaptOperationUnsigned.Case192(_io__raw_case__192, self, self._root)


    class Proof0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proof_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.ProofTag, self._io.read_u1())
            if self.proof_tag == Id015PtlimaptOperationUnsigned.ProofTag.case__0:
                self.case__0 = Id015PtlimaptOperationUnsigned.Case00(self._io, self, self._root)

            if self.proof_tag == Id015PtlimaptOperationUnsigned.ProofTag.case__2:
                self.case__2 = Id015PtlimaptOperationUnsigned.Case2(self._io, self, self._root)

            if self.proof_tag == Id015PtlimaptOperationUnsigned.ProofTag.case__1:
                self.case__1 = Id015PtlimaptOperationUnsigned.Case1(self._io, self, self._root)

            if self.proof_tag == Id015PtlimaptOperationUnsigned.ProofTag.case__3:
                self.case__3 = Id015PtlimaptOperationUnsigned.Case3(self._io, self, self._root)



    class Case146Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__146_elt_field0 = Id015PtlimaptOperationUnsigned.Case146EltField00(self._io, self, self._root)
            self.case__146_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case167EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__167_elt_field0 = self._io.read_u1()
            if not self.len_case__167_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__167_elt_field0, self._io, u"/types/case__167_elt_field0_0/seq/0")
            self._raw_case__167_elt_field0 = self._io.read_bytes(self.len_case__167_elt_field0)
            _io__raw_case__167_elt_field0 = KaitaiStream(BytesIO(self._raw_case__167_elt_field0))
            self.case__167_elt_field0 = Id015PtlimaptOperationUnsigned.Case167EltField0(_io__raw_case__167_elt_field0, self, self._root)


    class Case164EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__164_elt_field0 = self._io.read_u1()
            if not self.len_case__164_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__164_elt_field0, self._io, u"/types/case__164_elt_field0_0/seq/0")
            self._raw_case__164_elt_field0 = self._io.read_bytes(self.len_case__164_elt_field0)
            _io__raw_case__164_elt_field0 = KaitaiStream(BytesIO(self._raw_case__164_elt_field0))
            self.case__164_elt_field0 = Id015PtlimaptOperationUnsigned.Case164EltField0(_io__raw_case__164_elt_field0, self, self._root)


    class Proposals1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposals = Id015PtlimaptOperationUnsigned.Proposals0(self._io, self, self._root)


    class TxRollupSubmitBatch(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id015PtlimaptOperationUnsigned.Id015PtlimaptTxRollupId(self._io, self, self._root)
            self.content = Id015PtlimaptOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.burn_limit_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Bool, self._io.read_u1())
            if self.burn_limit_tag == Id015PtlimaptOperationUnsigned.Bool.true:
                self.burn_limit = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)



    class Case64(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__64_field0 = self._io.read_u1()
            self.case__64_field1 = []
            for i in range(32):
                self.case__64_field1.append(Id015PtlimaptOperationUnsigned.Case64Field1Entries(self._io, self, self._root))



    class Case188EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__188_elt_field0 = self._io.read_bytes_full()


    class Case33Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__33_field1_elt_field0 = self._io.read_u1()
            self.case__33_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Case183EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__183_elt_field0 = self._io.read_u1()
            if not self.len_case__183_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__183_elt_field0, self._io, u"/types/case__183_elt_field0_0/seq/0")
            self._raw_case__183_elt_field0 = self._io.read_bytes(self.len_case__183_elt_field0)
            _io__raw_case__183_elt_field0 = KaitaiStream(BytesIO(self._raw_case__183_elt_field0))
            self.case__183_elt_field0 = Id015PtlimaptOperationUnsigned.Case183EltField0(_io__raw_case__183_elt_field0, self, self._root)


    class Case151EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__151_elt_field0 = self._io.read_u1()
            if not self.len_case__151_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__151_elt_field0, self._io, u"/types/case__151_elt_field0_0/seq/0")
            self._raw_case__151_elt_field0 = self._io.read_bytes(self.len_case__151_elt_field0)
            _io__raw_case__151_elt_field0 = KaitaiStream(BytesIO(self._raw_case__151_elt_field0))
            self.case__151_elt_field0 = Id015PtlimaptOperationUnsigned.Case151EltField0(_io__raw_case__151_elt_field0, self, self._root)


    class DissectionEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.state_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Bool, self._io.read_u1())
            if self.state_tag == Id015PtlimaptOperationUnsigned.Bool.true:
                self.state = self._io.read_bytes(32)

            self.tick = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)


    class Case130EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__130_elt_field0 = self._io.read_u1()
            if not self.len_case__130_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__130_elt_field0, self._io, u"/types/case__130_elt_field0_0/seq/0")
            self._raw_case__130_elt_field0 = self._io.read_bytes(self.len_case__130_elt_field0)
            _io__raw_case__130_elt_field0 = KaitaiStream(BytesIO(self._raw_case__130_elt_field0))
            self.case__130_elt_field0 = Id015PtlimaptOperationUnsigned.Case130EltField0(_io__raw_case__130_elt_field0, self, self._root)


    class TicketsInfo(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.tickets_info_entries = []
            i = 0
            while not self._io.is_eof():
                self.tickets_info_entries.append(Id015PtlimaptOperationUnsigned.TicketsInfoEntries(self._io, self, self._root))
                i += 1



    class Case141EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__141_elt_field0 = self._io.read_bytes_full()


    class Case166EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__166_elt_field0 = self._io.read_u1()
            if not self.len_case__166_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__166_elt_field0, self._io, u"/types/case__166_elt_field0_0/seq/0")
            self._raw_case__166_elt_field0 = self._io.read_bytes(self.len_case__166_elt_field0)
            _io__raw_case__166_elt_field0 = KaitaiStream(BytesIO(self._raw_case__166_elt_field0))
            self.case__166_elt_field0 = Id015PtlimaptOperationUnsigned.Case166EltField0(_io__raw_case__166_elt_field0, self, self._root)


    class Case182EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__182_elt_field0 = self._io.read_bytes_full()


    class Case42(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__42_field0 = self._io.read_s4be()
            self.case__42_field1 = []
            for i in range(10):
                self.case__42_field1.append(Id015PtlimaptOperationUnsigned.Case42Field1Entries(self._io, self, self._root))



    class Case148Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__148_elt_field0 = Id015PtlimaptOperationUnsigned.Case148EltField00(self._io, self, self._root)
            self.case__148_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Ballot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposal = self._io.read_bytes(32)
            self.ballot = self._io.read_s1()


    class Case165EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__165_elt_field0 = self._io.read_u1()
            if not self.len_case__165_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__165_elt_field0, self._io, u"/types/case__165_elt_field0_0/seq/0")
            self._raw_case__165_elt_field0 = self._io.read_bytes(self.len_case__165_elt_field0)
            _io__raw_case__165_elt_field0 = KaitaiStream(BytesIO(self._raw_case__165_elt_field0))
            self.case__165_elt_field0 = Id015PtlimaptOperationUnsigned.Case165EltField0(_io__raw_case__165_elt_field0, self, self._root)


    class Predecessor(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.predecessor_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.PredecessorTag, self._io.read_u1())
            if self.predecessor_tag == Id015PtlimaptOperationUnsigned.PredecessorTag.some:
                self.some = self._io.read_bytes(32)



    class DalPublishSlotHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.slot = Id015PtlimaptOperationUnsigned.Slot(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id015PtlimaptOperationUnsigned.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id015PtlimaptOperationUnsigned.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id015PtlimaptOperationUnsigned.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class MessagePath0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message_path = self._io.read_u4be()
            if not self.len_message_path <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message_path, self._io, u"/types/message_path_0/seq/0")
            self._raw_message_path = self._io.read_bytes(self.len_message_path)
            _io__raw_message_path = KaitaiStream(BytesIO(self._raw_message_path))
            self.message_path = Id015PtlimaptOperationUnsigned.MessagePath(_io__raw_message_path, self, self._root)


    class Case154Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__154_elt_field0 = Id015PtlimaptOperationUnsigned.Case154EltField00(self._io, self, self._root)
            self.case__154_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Case18(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__18_field0 = self._io.read_s4be()
            self.case__18_field1 = []
            for i in range(4):
                self.case__18_field1.append(Id015PtlimaptOperationUnsigned.Case18Field1Entries(self._io, self, self._root))



    class Case50(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__50_field0 = self._io.read_s4be()
            self.case__50_field1 = []
            for i in range(12):
                self.case__50_field1.append(Id015PtlimaptOperationUnsigned.Case50Field1Entries(self._io, self, self._root))



    class Op11(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__preendorsement = Id015PtlimaptOperationUnsigned.Id015PtlimaptInlinedPreendorsement(self._io, self, self._root)


    class Case171EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__171_elt_field0 = self._io.read_u1()
            if not self.len_case__171_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__171_elt_field0, self._io, u"/types/case__171_elt_field0_0/seq/0")
            self._raw_case__171_elt_field0 = self._io.read_bytes(self.len_case__171_elt_field0)
            _io__raw_case__171_elt_field0 = KaitaiStream(BytesIO(self._raw_case__171_elt_field0))
            self.case__171_elt_field0 = Id015PtlimaptOperationUnsigned.Case171EltField0(_io__raw_case__171_elt_field0, self, self._root)


    class Proposals(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proposals_entries = []
            i = 0
            while not self._io.is_eof():
                self.proposals_entries.append(Id015PtlimaptOperationUnsigned.ProposalsEntries(self._io, self, self._root))
                i += 1



    class Case146EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__146_elt_field0 = self._io.read_u1()
            if not self.len_case__146_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__146_elt_field0, self._io, u"/types/case__146_elt_field0_0/seq/0")
            self._raw_case__146_elt_field0 = self._io.read_bytes(self.len_case__146_elt_field0)
            _io__raw_case__146_elt_field0 = KaitaiStream(BytesIO(self._raw_case__146_elt_field0))
            self.case__146_elt_field0 = Id015PtlimaptOperationUnsigned.Case146EltField0(_io__raw_case__146_elt_field0, self, self._root)


    class Case61Field1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__61_field1_elt_field0 = self._io.read_u1()
            self.case__61_field1_elt_field1 = Id015PtlimaptOperationUnsigned.InodeTree(self._io, self, self._root)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id015PtlimaptOperationUnsigned.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1



    class Case36(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__36_field0 = self._io.read_u1()
            self.case__36_field1 = []
            for i in range(9):
                self.case__36_field1.append(Id015PtlimaptOperationUnsigned.Case36Field1Entries(self._io, self, self._root))



    class OpEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_elt_field0 = Id015PtlimaptOperationUnsigned.OpEltField0(self._io, self, self._root)
            self.op_elt_field1 = Id015PtlimaptOperationUnsigned.OpEltField1(self._io, self, self._root)


    class Case158Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__158_elt_field0 = Id015PtlimaptOperationUnsigned.Case158EltField00(self._io, self, self._root)
            self.case__158_elt_field1 = Id015PtlimaptOperationUnsigned.TreeEncoding(self._io, self, self._root)


    class Id015PtlimaptBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_hash = self._io.read_bytes(32)
            self.payload_round = self._io.read_s4be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id015PtlimaptOperationUnsigned.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id015PtlimaptOperationUnsigned.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.liquidity_baking_toggle_vote = Id015PtlimaptOperationUnsigned.Id015PtlimaptLiquidityBakingToggleVote(self._io, self, self._root)


    class ProposalsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol_hash = self._io.read_bytes(32)


    class Op1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__inlined__endorsement = Id015PtlimaptOperationUnsigned.Id015PtlimaptInlinedEndorsement(self._io, self, self._root)


    class ZkRollupPublish(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id015PtlimaptOperationUnsigned.Id015PtlimaptMutez(self._io, self, self._root)
            self.counter = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id015PtlimaptOperationUnsigned.N(self._io, self, self._root)
            self.zk_rollup = self._io.read_bytes(20)
            self.op = Id015PtlimaptOperationUnsigned.Op0(self._io, self, self._root)


    class Case134EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__134_elt_field0 = self._io.read_bytes_full()


    class Op(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_entries = []
            i = 0
            while not self._io.is_eof():
                self.op_entries.append(Id015PtlimaptOperationUnsigned.OpEntries(self._io, self, self._root))
                i += 1



    class DoublePreendorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id015PtlimaptOperationUnsigned.Op12(self._io, self, self._root)
            self.op2 = Id015PtlimaptOperationUnsigned.Op22(self._io, self, self._root)


    class Case137EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__137_elt_field0 = self._io.read_u1()
            if not self.len_case__137_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__137_elt_field0, self._io, u"/types/case__137_elt_field0_0/seq/0")
            self._raw_case__137_elt_field0 = self._io.read_bytes(self.len_case__137_elt_field0)
            _io__raw_case__137_elt_field0 = KaitaiStream(BytesIO(self._raw_case__137_elt_field0))
            self.case__137_elt_field0 = Id015PtlimaptOperationUnsigned.Case137EltField0(_io__raw_case__137_elt_field0, self, self._root)



