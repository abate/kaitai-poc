# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id005Psbabym1ScriptExpr(KaitaiStruct):
    """Encoding id: 005-PsBabyM1.script.expr."""

    class Id005Psbabym1MichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left = 5
        none_0 = 6
        pair_0 = 7
        right_0 = 8
        some_0 = 9
        true = 10
        unit_1 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left_0 = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none = 62
        not = 63
        now = 64
        or_0 = 65
        pair_1 = 66
        push = 67
        right = 68
        size = 69
        some = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_0 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117

    class Micheline005Psbabym1MichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.micheline__005__psbabym1__michelson_v1__expression = Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1Expression(self._io, self, self._root)

    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id005Psbabym1ScriptExpr.Id005Psbabym1MichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1Expression(self._io, self, self._root)
            self.annots = Id005Psbabym1ScriptExpr.BytesDynUint30(self._io, self, self._root)


    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id005Psbabym1ScriptExpr.Id005Psbabym1MichelsonV1Primitives(self._io, self, self._root)
            self.annots = Id005Psbabym1ScriptExpr.BytesDynUint30(self._io, self, self._root)


    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id005Psbabym1ScriptExpr.Id005Psbabym1MichelsonV1Primitives(self._io, self, self._root)
            self.args = Id005Psbabym1ScriptExpr.Args0(self._io, self, self._root)
            self.annots = Id005Psbabym1ScriptExpr.BytesDynUint30(self._io, self, self._root)


    class Micheline005Psbabym1MichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__005__psbabym1__michelson_v1__expression_tag = KaitaiStream.resolve_enum(Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__005__psbabym1__michelson_v1__expression_tag == Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1ExpressionTag.int:
                self.int = Id005Psbabym1ScriptExpr.Z(self._io, self, self._root)

            if self.micheline__005__psbabym1__michelson_v1__expression_tag == Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1ExpressionTag.string:
                self.string = Id005Psbabym1ScriptExpr.BytesDynUint30(self._io, self, self._root)

            if self.micheline__005__psbabym1__michelson_v1__expression_tag == Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1ExpressionTag.sequence:
                self.sequence = Id005Psbabym1ScriptExpr.Sequence0(self._io, self, self._root)

            if self.micheline__005__psbabym1__michelson_v1__expression_tag == Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = Id005Psbabym1ScriptExpr.Id005Psbabym1MichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__005__psbabym1__michelson_v1__expression_tag == Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = Id005Psbabym1ScriptExpr.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__005__psbabym1__michelson_v1__expression_tag == Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = Id005Psbabym1ScriptExpr.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__005__psbabym1__michelson_v1__expression_tag == Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = Id005Psbabym1ScriptExpr.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__005__psbabym1__michelson_v1__expression_tag == Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = Id005Psbabym1ScriptExpr.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__005__psbabym1__michelson_v1__expression_tag == Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = Id005Psbabym1ScriptExpr.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__005__psbabym1__michelson_v1__expression_tag == Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = Id005Psbabym1ScriptExpr.PrimGeneric(self._io, self, self._root)

            if self.micheline__005__psbabym1__michelson_v1__expression_tag == Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1ExpressionTag.bytes:
                self.bytes = Id005Psbabym1ScriptExpr.BytesDynUint30(self._io, self, self._root)



    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = Id005Psbabym1ScriptExpr.Sequence(_io__raw_sequence, self, self._root)


    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(Id005Psbabym1ScriptExpr.SequenceEntries(self._io, self, self._root))
                i += 1



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(Id005Psbabym1ScriptExpr.ArgsEntries(self._io, self, self._root))
                i += 1



    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id005Psbabym1ScriptExpr.Id005Psbabym1MichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1Expression(self._io, self, self._root)


    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1Expression(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id005Psbabym1ScriptExpr.Id005Psbabym1MichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1Expression(self._io, self, self._root)


    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1Expression(self._io, self, self._root)


    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = Id005Psbabym1ScriptExpr.Args(_io__raw_args, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id005Psbabym1ScriptExpr.Id005Psbabym1MichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id005Psbabym1ScriptExpr.Micheline005Psbabym1MichelsonV1Expression(self._io, self, self._root)
            self.annots = Id005Psbabym1ScriptExpr.BytesDynUint30(self._io, self, self._root)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id005Psbabym1ScriptExpr.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1



    class Id005Psbabym1MichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__michelson__v1__primitives = KaitaiStream.resolve_enum(Id005Psbabym1ScriptExpr.Id005Psbabym1MichelsonV1Primitives, self._io.read_u1())



