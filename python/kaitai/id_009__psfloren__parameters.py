# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id009PsflorenParameters(KaitaiStruct):
    """Encoding id: 009-PsFLoren.parameters."""

    class Bool(Enum):
        false = 0
        true = 255

    class BootstrapAccountsEltTag(Enum):
        public_key_known = 0
        public_key_unknown = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.bootstrap_accounts = Id009PsflorenParameters.BootstrapAccounts0(self._io, self, self._root)
        self.bootstrap_contracts = Id009PsflorenParameters.BootstrapContracts0(self._io, self, self._root)
        self.commitments = Id009PsflorenParameters.Commitments0(self._io, self, self._root)
        self.security_deposit_ramp_up_cycles_tag = KaitaiStream.resolve_enum(Id009PsflorenParameters.Bool, self._io.read_u1())
        if self.security_deposit_ramp_up_cycles_tag == Id009PsflorenParameters.Bool.true:
            self.security_deposit_ramp_up_cycles = Id009PsflorenParameters.Int31(self._io, self, self._root)

        self.no_reward_cycles_tag = KaitaiStream.resolve_enum(Id009PsflorenParameters.Bool, self._io.read_u1())
        if self.no_reward_cycles_tag == Id009PsflorenParameters.Bool.true:
            self.no_reward_cycles = Id009PsflorenParameters.Int31(self._io, self, self._root)

        self.preserved_cycles = self._io.read_u1()
        self.blocks_per_cycle = self._io.read_s4be()
        self.blocks_per_commitment = self._io.read_s4be()
        self.blocks_per_roll_snapshot = self._io.read_s4be()
        self.blocks_per_voting_period = self._io.read_s4be()
        self.time_between_blocks = Id009PsflorenParameters.TimeBetweenBlocks0(self._io, self, self._root)
        self.endorsers_per_block = self._io.read_u2be()
        self.hard_gas_limit_per_operation = Id009PsflorenParameters.Z(self._io, self, self._root)
        self.hard_gas_limit_per_block = Id009PsflorenParameters.Z(self._io, self, self._root)
        self.proof_of_work_threshold = self._io.read_s8be()
        self.tokens_per_roll = Id009PsflorenParameters.Id009PsflorenMutez(self._io, self, self._root)
        self.michelson_maximum_type_size = self._io.read_u2be()
        self.seed_nonce_revelation_tip = Id009PsflorenParameters.Id009PsflorenMutez(self._io, self, self._root)
        self.origination_size = Id009PsflorenParameters.Int31(self._io, self, self._root)
        self.block_security_deposit = Id009PsflorenParameters.Id009PsflorenMutez(self._io, self, self._root)
        self.endorsement_security_deposit = Id009PsflorenParameters.Id009PsflorenMutez(self._io, self, self._root)
        self.baking_reward_per_endorsement = Id009PsflorenParameters.BakingRewardPerEndorsement0(self._io, self, self._root)
        self.endorsement_reward = Id009PsflorenParameters.EndorsementReward0(self._io, self, self._root)
        self.cost_per_byte = Id009PsflorenParameters.Id009PsflorenMutez(self._io, self, self._root)
        self.hard_storage_limit_per_operation = Id009PsflorenParameters.Z(self._io, self, self._root)
        self.test_chain_duration = self._io.read_s8be()
        self.quorum_min = self._io.read_s4be()
        self.quorum_max = self._io.read_s4be()
        self.min_proposal_quorum = self._io.read_s4be()
        self.initial_endorsers = self._io.read_u2be()
        self.delay_per_missing_endorsement = self._io.read_s8be()

    class TimeBetweenBlocksEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.time_between_blocks_elt = self._io.read_s8be()


    class PublicKeyKnown(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_known_field0 = Id009PsflorenParameters.PublicKey(self._io, self, self._root)
            self.public_key_known_field1 = Id009PsflorenParameters.Id009PsflorenMutez(self._io, self, self._root)


    class Commitments(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_entries = []
            i = 0
            while not self._io.is_eof():
                self.commitments_entries.append(Id009PsflorenParameters.CommitmentsEntries(self._io, self, self._root))
                i += 1



    class Id009PsflorenScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id009PsflorenParameters.BytesDynUint30(self._io, self, self._root)
            self.storage = Id009PsflorenParameters.BytesDynUint30(self._io, self, self._root)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id009PsflorenParameters.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class TimeBetweenBlocks(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.time_between_blocks_entries = []
            i = 0
            while not self._io.is_eof():
                self.time_between_blocks_entries.append(Id009PsflorenParameters.TimeBetweenBlocksEntries(self._io, self, self._root))
                i += 1



    class EndorsementReward(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.endorsement_reward_entries = []
            i = 0
            while not self._io.is_eof():
                self.endorsement_reward_entries.append(Id009PsflorenParameters.EndorsementRewardEntries(self._io, self, self._root))
                i += 1



    class CommitmentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_elt_field0 = self._io.read_bytes(20)
            self.commitments_elt_field1 = Id009PsflorenParameters.Id009PsflorenMutez(self._io, self, self._root)


    class EndorsementReward0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_endorsement_reward = self._io.read_u4be()
            if not self.len_endorsement_reward <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_endorsement_reward, self._io, u"/types/endorsement_reward_0/seq/0")
            self._raw_endorsement_reward = self._io.read_bytes(self.len_endorsement_reward)
            _io__raw_endorsement_reward = KaitaiStream(BytesIO(self._raw_endorsement_reward))
            self.endorsement_reward = Id009PsflorenParameters.EndorsementReward(_io__raw_endorsement_reward, self, self._root)


    class Commitments0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_commitments = self._io.read_u4be()
            if not self.len_commitments <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_commitments, self._io, u"/types/commitments_0/seq/0")
            self._raw_commitments = self._io.read_bytes(self.len_commitments)
            _io__raw_commitments = KaitaiStream(BytesIO(self._raw_commitments))
            self.commitments = Id009PsflorenParameters.Commitments(_io__raw_commitments, self, self._root)


    class Id009PsflorenMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_009__psfloren__mutez = Id009PsflorenParameters.N(self._io, self, self._root)


    class BootstrapAccounts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_accounts = self._io.read_u4be()
            if not self.len_bootstrap_accounts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_accounts, self._io, u"/types/bootstrap_accounts_0/seq/0")
            self._raw_bootstrap_accounts = self._io.read_bytes(self.len_bootstrap_accounts)
            _io__raw_bootstrap_accounts = KaitaiStream(BytesIO(self._raw_bootstrap_accounts))
            self.bootstrap_accounts = Id009PsflorenParameters.BootstrapAccounts(_io__raw_bootstrap_accounts, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id009PsflorenParameters.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id009PsflorenParameters.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id009PsflorenParameters.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id009PsflorenParameters.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class PublicKeyUnknown(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_unknown_field0 = Id009PsflorenParameters.PublicKeyHash(self._io, self, self._root)
            self.public_key_unknown_field1 = Id009PsflorenParameters.Id009PsflorenMutez(self._io, self, self._root)


    class BootstrapAccountsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_accounts_elt_tag = KaitaiStream.resolve_enum(Id009PsflorenParameters.BootstrapAccountsEltTag, self._io.read_u1())
            if self.bootstrap_accounts_elt_tag == Id009PsflorenParameters.BootstrapAccountsEltTag.public_key_known:
                self.public_key_known = Id009PsflorenParameters.PublicKeyKnown(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == Id009PsflorenParameters.BootstrapAccountsEltTag.public_key_unknown:
                self.public_key_unknown = Id009PsflorenParameters.PublicKeyUnknown(self._io, self, self._root)



    class BootstrapContracts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_contracts = self._io.read_u4be()
            if not self.len_bootstrap_contracts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_contracts, self._io, u"/types/bootstrap_contracts_0/seq/0")
            self._raw_bootstrap_contracts = self._io.read_bytes(self.len_bootstrap_contracts)
            _io__raw_bootstrap_contracts = KaitaiStream(BytesIO(self._raw_bootstrap_contracts))
            self.bootstrap_contracts = Id009PsflorenParameters.BootstrapContracts(_io__raw_bootstrap_contracts, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class EndorsementRewardEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_009__psfloren__mutez = Id009PsflorenParameters.Id009PsflorenMutez(self._io, self, self._root)


    class BakingRewardPerEndorsement0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_baking_reward_per_endorsement = self._io.read_u4be()
            if not self.len_baking_reward_per_endorsement <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_baking_reward_per_endorsement, self._io, u"/types/baking_reward_per_endorsement_0/seq/0")
            self._raw_baking_reward_per_endorsement = self._io.read_bytes(self.len_baking_reward_per_endorsement)
            _io__raw_baking_reward_per_endorsement = KaitaiStream(BytesIO(self._raw_baking_reward_per_endorsement))
            self.baking_reward_per_endorsement = Id009PsflorenParameters.BakingRewardPerEndorsement(_io__raw_baking_reward_per_endorsement, self, self._root)


    class TimeBetweenBlocks0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_time_between_blocks = self._io.read_u4be()
            if not self.len_time_between_blocks <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_time_between_blocks, self._io, u"/types/time_between_blocks_0/seq/0")
            self._raw_time_between_blocks = self._io.read_bytes(self.len_time_between_blocks)
            _io__raw_time_between_blocks = KaitaiStream(BytesIO(self._raw_time_between_blocks))
            self.time_between_blocks = Id009PsflorenParameters.TimeBetweenBlocks(_io__raw_time_between_blocks, self, self._root)


    class BakingRewardPerEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.baking_reward_per_endorsement_entries = []
            i = 0
            while not self._io.is_eof():
                self.baking_reward_per_endorsement_entries.append(Id009PsflorenParameters.BakingRewardPerEndorsementEntries(self._io, self, self._root))
                i += 1



    class BootstrapContractsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id009PsflorenParameters.PublicKeyHash(self._io, self, self._root)
            self.amount = Id009PsflorenParameters.Id009PsflorenMutez(self._io, self, self._root)
            self.script = Id009PsflorenParameters.Id009PsflorenScriptedContracts(self._io, self, self._root)


    class BootstrapContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_contracts_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_contracts_entries.append(Id009PsflorenParameters.BootstrapContractsEntries(self._io, self, self._root))
                i += 1



    class BootstrapAccounts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_accounts_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_accounts_entries.append(Id009PsflorenParameters.BootstrapAccountsEntries(self._io, self, self._root))
                i += 1



    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id009PsflorenParameters.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id009PsflorenParameters.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id009PsflorenParameters.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id009PsflorenParameters.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id009PsflorenParameters.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1



    class BakingRewardPerEndorsementEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_009__psfloren__mutez = Id009PsflorenParameters.Id009PsflorenMutez(self._io, self, self._root)



