# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id018ProxfordReceiptBalanceUpdates(KaitaiStruct):
    """Encoding id: 018-Proxford.receipt.balance_updates."""

    class Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag(Enum):
        contract = 0
        block_fees = 2
        deposits = 4
        nonce_revelation_rewards = 5
        attesting_rewards = 7
        baking_rewards = 8
        baking_bonuses = 9
        storage_fees = 11
        double_signing_punishments = 12
        lost_attesting_rewards = 13
        liquidity_baking_subsidies = 14
        burned = 15
        commitments = 16
        bootstrap = 17
        invoice = 18
        initial_commitments = 19
        minted = 20
        frozen_bonds = 21
        smart_rollup_refutation_punishments = 24
        smart_rollup_refutation_rewards = 25
        unstaked_deposits = 26
        staking_delegator_numerator = 27
        staking_delegate_denominator = 28

    class Id018ProxfordStakerTag(Enum):
        single = 0
        shared = 1

    class Id018ProxfordOperationMetadataAlphaUpdateOriginTag(Enum):
        block_application = 0
        protocol_migration = 1
        subsidy = 2
        simulation = 3
        delayed_operation = 4

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class Bool(Enum):
        false = 0
        true = 255

    class Id018ProxfordBondIdTag(Enum):
        smart_rollup_bond_id = 1

    class Id018ProxfordFrozenStakerTag(Enum):
        single = 0
        shared = 1
        baker = 2

    class Id018ProxfordContractIdTag(Enum):
        implicit = 0
        originated = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_018__proxford__operation_metadata__alpha__balance_updates = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Commitments(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.committer = self._io.read_bytes(20)
            self.id_018__proxford__operation_metadata__alpha__tez_balance_update = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class Id018ProxfordOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_018__proxford__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_018__proxford__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_018__proxford__operation_metadata__alpha__balance_updates, self._io, u"/types/id_018__proxford__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_018__proxford__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_018__proxford__operation_metadata__alpha__balance_updates)
            _io__raw_id_018__proxford__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_018__proxford__operation_metadata__alpha__balance_updates))
            self.id_018__proxford__operation_metadata__alpha__balance_updates = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceUpdates(_io__raw_id_018__proxford__operation_metadata__alpha__balance_updates, self, self._root)


    class FrozenBonds(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordContractId(self._io, self, self._root)
            self.bond_id = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordBondId(self._io, self, self._root)
            self.id_018__proxford__operation_metadata__alpha__tez_balance_update = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id018ProxfordContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__contract_id_tag = KaitaiStream.resolve_enum(Id018ProxfordReceiptBalanceUpdates.Id018ProxfordContractIdTag, self._io.read_u1())
            if self.id_018__proxford__contract_id_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordContractIdTag.implicit:
                self.implicit = Id018ProxfordReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_018__proxford__contract_id_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordContractIdTag.originated:
                self.originated = Id018ProxfordReceiptBalanceUpdates.Originated(self._io, self, self._root)



    class Id018ProxfordOperationMetadataAlphaBalanceAndUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag = KaitaiStream.resolve_enum(Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag, self._io.read_u1())
            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.contract:
                self.contract = Id018ProxfordReceiptBalanceUpdates.Contract(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.block_fees:
                self.block_fees = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.deposits:
                self.deposits = Id018ProxfordReceiptBalanceUpdates.Deposits(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.nonce_revelation_rewards:
                self.nonce_revelation_rewards = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.attesting_rewards:
                self.attesting_rewards = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.baking_rewards:
                self.baking_rewards = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.baking_bonuses:
                self.baking_bonuses = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.storage_fees:
                self.storage_fees = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.double_signing_punishments:
                self.double_signing_punishments = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.lost_attesting_rewards:
                self.lost_attesting_rewards = Id018ProxfordReceiptBalanceUpdates.LostAttestingRewards(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.liquidity_baking_subsidies:
                self.liquidity_baking_subsidies = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.burned:
                self.burned = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.commitments:
                self.commitments = Id018ProxfordReceiptBalanceUpdates.Commitments(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.bootstrap:
                self.bootstrap = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.invoice:
                self.invoice = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.initial_commitments:
                self.initial_commitments = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.minted:
                self.minted = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.frozen_bonds:
                self.frozen_bonds = Id018ProxfordReceiptBalanceUpdates.FrozenBonds(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.smart_rollup_refutation_punishments:
                self.smart_rollup_refutation_punishments = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.smart_rollup_refutation_rewards:
                self.smart_rollup_refutation_rewards = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.unstaked_deposits:
                self.unstaked_deposits = Id018ProxfordReceiptBalanceUpdates.UnstakedDeposits(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.staking_delegator_numerator:
                self.staking_delegator_numerator = Id018ProxfordReceiptBalanceUpdates.StakingDelegatorNumerator(self._io, self, self._root)

            if self.id_018__proxford__operation_metadata__alpha__balance_and_update_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdateTag.staking_delegate_denominator:
                self.staking_delegate_denominator = Id018ProxfordReceiptBalanceUpdates.StakingDelegateDenominator(self._io, self, self._root)



    class StakingDelegateDenominator(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id018ProxfordReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.id_018__proxford__operation_metadata__alpha__staking_abstract_quantity = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaStakingAbstractQuantity(self._io, self, self._root)


    class Id018ProxfordOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_018__proxford__operation_metadata__alpha__balance_updates_entries.append(Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class Single(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordContractId(self._io, self, self._root)
            self.delegate = Id018ProxfordReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)


    class Deposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.staker = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordFrozenStaker(self._io, self, self._root)
            self.id_018__proxford__operation_metadata__alpha__tez_balance_update = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class Id018ProxfordStaker(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__staker_tag = KaitaiStream.resolve_enum(Id018ProxfordReceiptBalanceUpdates.Id018ProxfordStakerTag, self._io.read_u1())
            if self.id_018__proxford__staker_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordStakerTag.single:
                self.single = Id018ProxfordReceiptBalanceUpdates.Single(self._io, self, self._root)

            if self.id_018__proxford__staker_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordStakerTag.shared:
                self.shared = Id018ProxfordReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)



    class Id018ProxfordBondId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__bond_id_tag = KaitaiStream.resolve_enum(Id018ProxfordReceiptBalanceUpdates.Id018ProxfordBondIdTag, self._io.read_u1())
            if self.id_018__proxford__bond_id_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordBondIdTag.smart_rollup_bond_id:
                self.smart_rollup_bond_id = self._io.read_bytes(20)



    class Id018ProxfordOperationMetadataAlphaStakingAbstractQuantity(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Id018ProxfordFrozenStaker(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__frozen_staker_tag = KaitaiStream.resolve_enum(Id018ProxfordReceiptBalanceUpdates.Id018ProxfordFrozenStakerTag, self._io.read_u1())
            if self.id_018__proxford__frozen_staker_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordFrozenStakerTag.single:
                self.single = Id018ProxfordReceiptBalanceUpdates.Single(self._io, self, self._root)

            if self.id_018__proxford__frozen_staker_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordFrozenStakerTag.shared:
                self.shared = Id018ProxfordReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_018__proxford__frozen_staker_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordFrozenStakerTag.baker:
                self.baker = Id018ProxfordReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)



    class LostAttestingRewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id018ProxfordReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.participation = KaitaiStream.resolve_enum(Id018ProxfordReceiptBalanceUpdates.Bool, self._io.read_u1())
            self.revelation = KaitaiStream.resolve_enum(Id018ProxfordReceiptBalanceUpdates.Bool, self._io.read_u1())
            self.id_018__proxford__operation_metadata__alpha__tez_balance_update = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class UnstakedDeposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.staker = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordStaker(self._io, self, self._root)
            self.cycle = self._io.read_s4be()
            self.id_018__proxford__operation_metadata__alpha__tez_balance_update = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class Id018ProxfordOperationMetadataAlphaUpdateOrigin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__operation_metadata__alpha__update_origin_tag = KaitaiStream.resolve_enum(Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaUpdateOriginTag, self._io.read_u1())
            if self.id_018__proxford__operation_metadata__alpha__update_origin_tag == Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaUpdateOriginTag.delayed_operation:
                self.delayed_operation = self._io.read_bytes(32)



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id018ProxfordReceiptBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id018ProxfordReceiptBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id018ProxfordReceiptBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id018ProxfordReceiptBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id018ProxfordReceiptBalanceUpdates.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class Contract(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordContractId(self._io, self, self._root)
            self.id_018__proxford__operation_metadata__alpha__tez_balance_update = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class StakingDelegatorNumerator(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegator = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordContractId(self._io, self, self._root)
            self.id_018__proxford__operation_metadata__alpha__staking_abstract_quantity = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaStakingAbstractQuantity(self._io, self, self._root)


    class Id018ProxfordOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__operation_metadata__alpha__balance_and_update = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaBalanceAndUpdate(self._io, self, self._root)
            self.id_018__proxford__operation_metadata__alpha__update_origin = Id018ProxfordReceiptBalanceUpdates.Id018ProxfordOperationMetadataAlphaUpdateOrigin(self._io, self, self._root)



