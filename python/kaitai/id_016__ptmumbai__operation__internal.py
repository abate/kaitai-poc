# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id016PtmumbaiOperationInternal(KaitaiStruct):
    """Encoding id: 016-PtMumbai.operation.internal."""

    class Id016PtmumbaiMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left = 5
        none_0 = 6
        pair_1 = 7
        right = 8
        some_0 = 9
        true = 10
        unit_0 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left_0 = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right_0 = 68
        size = 69
        some = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_1 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction_deprecated = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_deprecated = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140
        chest = 141
        chest_key = 142
        open_chest = 143
        view_0 = 144
        view = 145
        constant = 146
        sub_mutez = 147
        tx_rollup_l2_address = 148
        min_block_time = 149
        sapling_transaction = 150
        emit = 151
        lambda_rec = 152
        lambda_rec_0 = 153
        ticket_0 = 154
        bytes_0 = 155
        nat_0 = 156

    class Micheline016PtmumbaiMichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class Bool(Enum):
        false = 0
        true = 255

    class Id016PtmumbaiApplyInternalResultsAlphaOperationResultTag(Enum):
        transaction = 1
        origination = 2
        delegation = 3
        event = 4

    class Id016PtmumbaiEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        deposit = 5
        named = 255

    class Id016PtmumbaiTransactionDestinationTag(Enum):
        implicit = 0
        originated = 1
        tx_rollup = 2
        smart_rollup = 3
        zk_rollup = 4
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_016__ptmumbai__apply_internal_results__alpha__operation_result = Id016PtmumbaiOperationInternal.Id016PtmumbaiApplyInternalResultsAlphaOperationResult(self._io, self, self._root)

    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id016PtmumbaiOperationInternal.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id016PtmumbaiMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_016__ptmumbai__michelson__v1__primitives = KaitaiStream.resolve_enum(Id016PtmumbaiOperationInternal.Id016PtmumbaiMichelsonV1Primitives, self._io.read_u1())


    class SmartRollup(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.smart_rollup_hash = self._io.read_bytes(20)
            self.smart_rollup_padding = self._io.read_bytes(1)


    class Event(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.type = Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1Expression(self._io, self, self._root)
            self.tag_tag = KaitaiStream.resolve_enum(Id016PtmumbaiOperationInternal.Bool, self._io.read_u1())
            if self.tag_tag == Id016PtmumbaiOperationInternal.Bool.true:
                self.tag = Id016PtmumbaiOperationInternal.Id016PtmumbaiEntrypoint(self._io, self, self._root)

            self.payload_tag = KaitaiStream.resolve_enum(Id016PtmumbaiOperationInternal.Bool, self._io.read_u1())
            if self.payload_tag == Id016PtmumbaiOperationInternal.Bool.true:
                self.payload = Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1Expression(self._io, self, self._root)



    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id016PtmumbaiOperationInternal.Id016PtmumbaiMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id016PtmumbaiOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Id016PtmumbaiTxRollupId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup_hash = self._io.read_bytes(20)


    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id016PtmumbaiOperationInternal.Id016PtmumbaiMichelsonV1Primitives(self._io, self, self._root)
            self.annots = Id016PtmumbaiOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Id016PtmumbaiApplyInternalResultsAlphaOperationResult(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id016PtmumbaiOperationInternal.Id016PtmumbaiTransactionDestination(self._io, self, self._root)
            self.nonce = self._io.read_u2be()
            self.id_016__ptmumbai__apply_internal_results__alpha__operation_result_tag = KaitaiStream.resolve_enum(Id016PtmumbaiOperationInternal.Id016PtmumbaiApplyInternalResultsAlphaOperationResultTag, self._io.read_u1())
            if self.id_016__ptmumbai__apply_internal_results__alpha__operation_result_tag == Id016PtmumbaiOperationInternal.Id016PtmumbaiApplyInternalResultsAlphaOperationResultTag.transaction:
                self.transaction = Id016PtmumbaiOperationInternal.Transaction(self._io, self, self._root)

            if self.id_016__ptmumbai__apply_internal_results__alpha__operation_result_tag == Id016PtmumbaiOperationInternal.Id016PtmumbaiApplyInternalResultsAlphaOperationResultTag.origination:
                self.origination = Id016PtmumbaiOperationInternal.Origination(self._io, self, self._root)

            if self.id_016__ptmumbai__apply_internal_results__alpha__operation_result_tag == Id016PtmumbaiOperationInternal.Id016PtmumbaiApplyInternalResultsAlphaOperationResultTag.delegation:
                self.delegation = Id016PtmumbaiOperationInternal.Delegation(self._io, self, self._root)

            if self.id_016__ptmumbai__apply_internal_results__alpha__operation_result_tag == Id016PtmumbaiOperationInternal.Id016PtmumbaiApplyInternalResultsAlphaOperationResultTag.event:
                self.event = Id016PtmumbaiOperationInternal.Event(self._io, self, self._root)



    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id016PtmumbaiOperationInternal.Id016PtmumbaiMichelsonV1Primitives(self._io, self, self._root)
            self.args = Id016PtmumbaiOperationInternal.Args0(self._io, self, self._root)
            self.annots = Id016PtmumbaiOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id016PtmumbaiOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id016PtmumbaiOperationInternal.Bool.true:
                self.delegate = Id016PtmumbaiOperationInternal.PublicKeyHash(self._io, self, self._root)



    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = Id016PtmumbaiOperationInternal.Sequence(_io__raw_sequence, self, self._root)


    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(Id016PtmumbaiOperationInternal.SequenceEntries(self._io, self, self._root))
                i += 1



    class ZkRollup(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.zk_rollup_hash = self._io.read_bytes(20)
            self.zk_rollup_padding = self._io.read_bytes(1)


    class Id016PtmumbaiEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_016__ptmumbai__entrypoint_tag = KaitaiStream.resolve_enum(Id016PtmumbaiOperationInternal.Id016PtmumbaiEntrypointTag, self._io.read_u1())
            if self.id_016__ptmumbai__entrypoint_tag == Id016PtmumbaiOperationInternal.Id016PtmumbaiEntrypointTag.named:
                self.named = Id016PtmumbaiOperationInternal.Named0(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.balance = Id016PtmumbaiOperationInternal.Id016PtmumbaiMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id016PtmumbaiOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id016PtmumbaiOperationInternal.Bool.true:
                self.delegate = Id016PtmumbaiOperationInternal.PublicKeyHash(self._io, self, self._root)

            self.script = Id016PtmumbaiOperationInternal.Id016PtmumbaiScriptedContracts(self._io, self, self._root)


    class Id016PtmumbaiScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id016PtmumbaiOperationInternal.BytesDynUint30(self._io, self, self._root)
            self.storage = Id016PtmumbaiOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(Id016PtmumbaiOperationInternal.ArgsEntries(self._io, self, self._root))
                i += 1



    class Micheline016PtmumbaiMichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__016__ptmumbai__michelson_v1__expression_tag = KaitaiStream.resolve_enum(Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__016__ptmumbai__michelson_v1__expression_tag == Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1ExpressionTag.int:
                self.int = Id016PtmumbaiOperationInternal.Z(self._io, self, self._root)

            if self.micheline__016__ptmumbai__michelson_v1__expression_tag == Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1ExpressionTag.string:
                self.string = Id016PtmumbaiOperationInternal.BytesDynUint30(self._io, self, self._root)

            if self.micheline__016__ptmumbai__michelson_v1__expression_tag == Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1ExpressionTag.sequence:
                self.sequence = Id016PtmumbaiOperationInternal.Sequence0(self._io, self, self._root)

            if self.micheline__016__ptmumbai__michelson_v1__expression_tag == Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = Id016PtmumbaiOperationInternal.Id016PtmumbaiMichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__016__ptmumbai__michelson_v1__expression_tag == Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = Id016PtmumbaiOperationInternal.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__016__ptmumbai__michelson_v1__expression_tag == Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = Id016PtmumbaiOperationInternal.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__016__ptmumbai__michelson_v1__expression_tag == Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = Id016PtmumbaiOperationInternal.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__016__ptmumbai__michelson_v1__expression_tag == Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = Id016PtmumbaiOperationInternal.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__016__ptmumbai__michelson_v1__expression_tag == Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = Id016PtmumbaiOperationInternal.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__016__ptmumbai__michelson_v1__expression_tag == Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = Id016PtmumbaiOperationInternal.PrimGeneric(self._io, self, self._root)

            if self.micheline__016__ptmumbai__michelson_v1__expression_tag == Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1ExpressionTag.bytes:
                self.bytes = Id016PtmumbaiOperationInternal.BytesDynUint30(self._io, self, self._root)



    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id016PtmumbaiOperationInternal.Id016PtmumbaiMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1Expression(self._io, self, self._root)


    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1Expression(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id016PtmumbaiOperationInternal.Id016PtmumbaiMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1Expression(self._io, self, self._root)


    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1Expression(self._io, self, self._root)


    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = Id016PtmumbaiOperationInternal.Args(_io__raw_args, self, self._root)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id016PtmumbaiOperationInternal.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount = Id016PtmumbaiOperationInternal.Id016PtmumbaiMutez(self._io, self, self._root)
            self.destination = Id016PtmumbaiOperationInternal.Id016PtmumbaiTransactionDestination(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id016PtmumbaiOperationInternal.Bool, self._io.read_u1())
            if self.parameters_tag == Id016PtmumbaiOperationInternal.Bool.true:
                self.parameters = Id016PtmumbaiOperationInternal.Parameters(self._io, self, self._root)



    class Id016PtmumbaiTransactionDestination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_016__ptmumbai__transaction_destination_tag = KaitaiStream.resolve_enum(Id016PtmumbaiOperationInternal.Id016PtmumbaiTransactionDestinationTag, self._io.read_u1())
            if self.id_016__ptmumbai__transaction_destination_tag == Id016PtmumbaiOperationInternal.Id016PtmumbaiTransactionDestinationTag.implicit:
                self.implicit = Id016PtmumbaiOperationInternal.PublicKeyHash(self._io, self, self._root)

            if self.id_016__ptmumbai__transaction_destination_tag == Id016PtmumbaiOperationInternal.Id016PtmumbaiTransactionDestinationTag.originated:
                self.originated = Id016PtmumbaiOperationInternal.Originated(self._io, self, self._root)

            if self.id_016__ptmumbai__transaction_destination_tag == Id016PtmumbaiOperationInternal.Id016PtmumbaiTransactionDestinationTag.tx_rollup:
                self.tx_rollup = Id016PtmumbaiOperationInternal.TxRollup(self._io, self, self._root)

            if self.id_016__ptmumbai__transaction_destination_tag == Id016PtmumbaiOperationInternal.Id016PtmumbaiTransactionDestinationTag.smart_rollup:
                self.smart_rollup = Id016PtmumbaiOperationInternal.SmartRollup(self._io, self, self._root)

            if self.id_016__ptmumbai__transaction_destination_tag == Id016PtmumbaiOperationInternal.Id016PtmumbaiTransactionDestinationTag.zk_rollup:
                self.zk_rollup = Id016PtmumbaiOperationInternal.ZkRollup(self._io, self, self._root)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id016PtmumbaiOperationInternal.Id016PtmumbaiEntrypoint(self._io, self, self._root)
            self.value = Id016PtmumbaiOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id016PtmumbaiOperationInternal.Id016PtmumbaiMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id016PtmumbaiOperationInternal.Micheline016PtmumbaiMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id016PtmumbaiOperationInternal.BytesDynUint30(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id016PtmumbaiOperationInternal.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id016PtmumbaiOperationInternal.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id016PtmumbaiOperationInternal.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id016PtmumbaiOperationInternal.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id016PtmumbaiOperationInternal.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class TxRollup(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_016__ptmumbai__tx_rollup_id = Id016PtmumbaiOperationInternal.Id016PtmumbaiTxRollupId(self._io, self, self._root)
            self.tx_rollup_padding = self._io.read_bytes(1)


    class Id016PtmumbaiMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_016__ptmumbai__mutez = Id016PtmumbaiOperationInternal.N(self._io, self, self._root)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id016PtmumbaiOperationInternal.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




