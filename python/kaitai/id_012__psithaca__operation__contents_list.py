# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import block_header__shell
from kaitai import operation__shell_header
class Id012PsithacaOperationContentsList(KaitaiStruct):
    """Encoding id: 012-Psithaca.operation.contents_list."""

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Id012PsithacaEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255

    class Id012PsithacaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id012PsithacaInlinedEndorsementMempoolContentsTag(Enum):
        endorsement = 21

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id012PsithacaInlinedPreendorsementContentsTag(Enum):
        preendorsement = 20

    class Id012PsithacaOperationAlphaContentsTag(Enum):
        seed_nonce_revelation = 1
        double_endorsement_evidence = 2
        double_baking_evidence = 3
        activate_account = 4
        proposals = 5
        ballot = 6
        double_preendorsement_evidence = 7
        failing_noop = 17
        preendorsement = 20
        endorsement = 21
        reveal = 107
        transaction = 108
        origination = 109
        delegation = 110
        register_global_constant = 111
        set_deposits_limit = 112
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_012__psithaca__operation__contents_list_entries = []
        i = 0
        while not self._io.is_eof():
            self.id_012__psithaca__operation__contents_list_entries.append(Id012PsithacaOperationContentsList.Id012PsithacaOperationContentsListEntries(self._io, self, self._root))
            i += 1


    class Op20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_0/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id012PsithacaOperationContentsList.Op2(_io__raw_op2, self, self._root)


    class ActivateAccount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = self._io.read_bytes(20)
            self.secret = self._io.read_bytes(20)


    class Id012PsithacaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__contract_id_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Id012PsithacaContractIdTag, self._io.read_u1())
            if self.id_012__psithaca__contract_id_tag == Id012PsithacaOperationContentsList.Id012PsithacaContractIdTag.implicit:
                self.implicit = Id012PsithacaOperationContentsList.PublicKeyHash(self._io, self, self._root)

            if self.id_012__psithaca__contract_id_tag == Id012PsithacaOperationContentsList.Id012PsithacaContractIdTag.originated:
                self.originated = Id012PsithacaOperationContentsList.Originated(self._io, self, self._root)



    class DoubleEndorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id012PsithacaOperationContentsList.Op10(self._io, self, self._root)
            self.op2 = Id012PsithacaOperationContentsList.Op20(self._io, self, self._root)


    class Id012PsithacaScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id012PsithacaOperationContentsList.BytesDynUint30(self._io, self, self._root)
            self.storage = Id012PsithacaOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class Id012PsithacaEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__entrypoint_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Id012PsithacaEntrypointTag, self._io.read_u1())
            if self.id_012__psithaca__entrypoint_tag == Id012PsithacaOperationContentsList.Id012PsithacaEntrypointTag.named:
                self.named = Id012PsithacaOperationContentsList.Named0(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id012PsithacaInlinedEndorsementMempoolContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__inlined__endorsement_mempool__contents_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Id012PsithacaInlinedEndorsementMempoolContentsTag, self._io.read_u1())
            if self.id_012__psithaca__inlined__endorsement_mempool__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaInlinedEndorsementMempoolContentsTag.endorsement:
                self.endorsement = Id012PsithacaOperationContentsList.Endorsement(self._io, self, self._root)



    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id012PsithacaOperationContentsList.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Endorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class Proposals0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_proposals = self._io.read_u4be()
            if not self.len_proposals <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_proposals, self._io, u"/types/proposals_0/seq/0")
            self._raw_proposals = self._io.read_bytes(self.len_proposals)
            _io__raw_proposals = KaitaiStream(BytesIO(self._raw_proposals))
            self.proposals = Id012PsithacaOperationContentsList.Proposals(_io__raw_proposals, self, self._root)


    class Op12(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_2/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id012PsithacaOperationContentsList.Op11(_io__raw_op1, self, self._root)


    class Id012PsithacaBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__block_header__alpha__unsigned_contents = Id012PsithacaOperationContentsList.Id012PsithacaBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes(64)


    class Id012PsithacaBlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_012__psithaca__block_header__alpha__signed_contents = Id012PsithacaOperationContentsList.Id012PsithacaBlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Reveal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id012PsithacaOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id012PsithacaOperationContentsList.Id012PsithacaMutez(self._io, self, self._root)
            self.counter = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.public_key = Id012PsithacaOperationContentsList.PublicKey(self._io, self, self._root)


    class Id012PsithacaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__mutez = Id012PsithacaOperationContentsList.N(self._io, self, self._root)


    class SeedNonceRevelation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.nonce = self._io.read_bytes(32)


    class Preendorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class DoubleBakingEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bh1 = Id012PsithacaOperationContentsList.Bh10(self._io, self, self._root)
            self.bh2 = Id012PsithacaOperationContentsList.Bh20(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id012PsithacaOperationContentsList.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id012PsithacaOperationContentsList.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id012PsithacaOperationContentsList.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Bh2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__block_header__alpha__full_header = Id012PsithacaOperationContentsList.Id012PsithacaBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Bh20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh2 = self._io.read_u4be()
            if not self.len_bh2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh2, self._io, u"/types/bh2_0/seq/0")
            self._raw_bh2 = self._io.read_bytes(self.len_bh2)
            _io__raw_bh2 = KaitaiStream(BytesIO(self._raw_bh2))
            self.bh2 = Id012PsithacaOperationContentsList.Bh2(_io__raw_bh2, self, self._root)


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id012PsithacaOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id012PsithacaOperationContentsList.Id012PsithacaMutez(self._io, self, self._root)
            self.counter = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Bool, self._io.read_u1())
            if self.delegate_tag == Id012PsithacaOperationContentsList.Bool.true:
                self.delegate = Id012PsithacaOperationContentsList.PublicKeyHash(self._io, self, self._root)



    class RegisterGlobalConstant(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id012PsithacaOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id012PsithacaOperationContentsList.Id012PsithacaMutez(self._io, self, self._root)
            self.counter = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.value = Id012PsithacaOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class Id012PsithacaInlinedPreendorsementContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__inlined__preendorsement__contents_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Id012PsithacaInlinedPreendorsementContentsTag, self._io.read_u1())
            if self.id_012__psithaca__inlined__preendorsement__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaInlinedPreendorsementContentsTag.preendorsement:
                self.preendorsement = Id012PsithacaOperationContentsList.Preendorsement(self._io, self, self._root)



    class Bh10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh1 = self._io.read_u4be()
            if not self.len_bh1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh1, self._io, u"/types/bh1_0/seq/0")
            self._raw_bh1 = self._io.read_bytes(self.len_bh1)
            _io__raw_bh1 = KaitaiStream(BytesIO(self._raw_bh1))
            self.bh1 = Id012PsithacaOperationContentsList.Bh1(_io__raw_bh1, self, self._root)


    class Op22(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_2/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id012PsithacaOperationContentsList.Op21(_io__raw_op2, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Bh1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__block_header__alpha__full_header = Id012PsithacaOperationContentsList.Id012PsithacaBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class SetDepositsLimit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id012PsithacaOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id012PsithacaOperationContentsList.Id012PsithacaMutez(self._io, self, self._root)
            self.counter = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.limit_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Bool, self._io.read_u1())
            if self.limit_tag == Id012PsithacaOperationContentsList.Bool.true:
                self.limit = Id012PsithacaOperationContentsList.Id012PsithacaMutez(self._io, self, self._root)



    class Id012PsithacaInlinedEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__inlined__endorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id012PsithacaOperationContentsList.Id012PsithacaInlinedEndorsementMempoolContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Bool, self._io.read_u1())
            if self.signature_tag == Id012PsithacaOperationContentsList.Bool.true:
                self.signature = self._io.read_bytes(64)



    class Op10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_0/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id012PsithacaOperationContentsList.Op1(_io__raw_op1, self, self._root)


    class Op21(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__inlined__preendorsement = Id012PsithacaOperationContentsList.Id012PsithacaInlinedPreendorsement(self._io, self, self._root)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id012PsithacaOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id012PsithacaOperationContentsList.Id012PsithacaMutez(self._io, self, self._root)
            self.counter = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.balance = Id012PsithacaOperationContentsList.Id012PsithacaMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Bool, self._io.read_u1())
            if self.delegate_tag == Id012PsithacaOperationContentsList.Bool.true:
                self.delegate = Id012PsithacaOperationContentsList.PublicKeyHash(self._io, self, self._root)

            self.script = Id012PsithacaOperationContentsList.Id012PsithacaScriptedContracts(self._io, self, self._root)


    class Op2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__inlined__endorsement = Id012PsithacaOperationContentsList.Id012PsithacaInlinedEndorsement(self._io, self, self._root)


    class Id012PsithacaOperationContentsListEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__operation__alpha__contents = Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContents(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Id012PsithacaInlinedPreendorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__inlined__preendorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id012PsithacaOperationContentsList.Id012PsithacaInlinedPreendorsementContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Bool, self._io.read_u1())
            if self.signature_tag == Id012PsithacaOperationContentsList.Bool.true:
                self.signature = self._io.read_bytes(64)



    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id012PsithacaOperationContentsList.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id012PsithacaOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id012PsithacaOperationContentsList.Id012PsithacaMutez(self._io, self, self._root)
            self.counter = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id012PsithacaOperationContentsList.N(self._io, self, self._root)
            self.amount = Id012PsithacaOperationContentsList.Id012PsithacaMutez(self._io, self, self._root)
            self.destination = Id012PsithacaOperationContentsList.Id012PsithacaContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Bool, self._io.read_u1())
            if self.parameters_tag == Id012PsithacaOperationContentsList.Bool.true:
                self.parameters = Id012PsithacaOperationContentsList.Parameters(self._io, self, self._root)



    class Id012PsithacaOperationAlphaContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__operation__alpha__contents_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag, self._io.read_u1())
            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.endorsement:
                self.endorsement = Id012PsithacaOperationContentsList.Endorsement(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.preendorsement:
                self.preendorsement = Id012PsithacaOperationContentsList.Preendorsement(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.seed_nonce_revelation:
                self.seed_nonce_revelation = Id012PsithacaOperationContentsList.SeedNonceRevelation(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.double_endorsement_evidence:
                self.double_endorsement_evidence = Id012PsithacaOperationContentsList.DoubleEndorsementEvidence(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.double_preendorsement_evidence:
                self.double_preendorsement_evidence = Id012PsithacaOperationContentsList.DoublePreendorsementEvidence(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.double_baking_evidence:
                self.double_baking_evidence = Id012PsithacaOperationContentsList.DoubleBakingEvidence(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.activate_account:
                self.activate_account = Id012PsithacaOperationContentsList.ActivateAccount(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.proposals:
                self.proposals = Id012PsithacaOperationContentsList.Proposals1(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.ballot:
                self.ballot = Id012PsithacaOperationContentsList.Ballot(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.reveal:
                self.reveal = Id012PsithacaOperationContentsList.Reveal(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.transaction:
                self.transaction = Id012PsithacaOperationContentsList.Transaction(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.origination:
                self.origination = Id012PsithacaOperationContentsList.Origination(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.delegation:
                self.delegation = Id012PsithacaOperationContentsList.Delegation(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.set_deposits_limit:
                self.set_deposits_limit = Id012PsithacaOperationContentsList.SetDepositsLimit(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.failing_noop:
                self.failing_noop = Id012PsithacaOperationContentsList.BytesDynUint30(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__contents_tag == Id012PsithacaOperationContentsList.Id012PsithacaOperationAlphaContentsTag.register_global_constant:
                self.register_global_constant = Id012PsithacaOperationContentsList.RegisterGlobalConstant(self._io, self, self._root)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id012PsithacaOperationContentsList.Id012PsithacaEntrypoint(self._io, self, self._root)
            self.value = Id012PsithacaOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class Proposals1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id012PsithacaOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposals = Id012PsithacaOperationContentsList.Proposals0(self._io, self, self._root)


    class Id012PsithacaBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_hash = self._io.read_bytes(32)
            self.payload_round = self._io.read_s4be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id012PsithacaOperationContentsList.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.liquidity_baking_escape_vote = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.Bool, self._io.read_u1())


    class Ballot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id012PsithacaOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposal = self._io.read_bytes(32)
            self.ballot = self._io.read_s1()


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationContentsList.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id012PsithacaOperationContentsList.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id012PsithacaOperationContentsList.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id012PsithacaOperationContentsList.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Op11(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__inlined__preendorsement = Id012PsithacaOperationContentsList.Id012PsithacaInlinedPreendorsement(self._io, self, self._root)


    class Proposals(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proposals_entries = []
            i = 0
            while not self._io.is_eof():
                self.proposals_entries.append(Id012PsithacaOperationContentsList.ProposalsEntries(self._io, self, self._root))
                i += 1



    class ProposalsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol_hash = self._io.read_bytes(32)


    class Op1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__inlined__endorsement = Id012PsithacaOperationContentsList.Id012PsithacaInlinedEndorsement(self._io, self, self._root)


    class DoublePreendorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id012PsithacaOperationContentsList.Op12(self._io, self, self._root)
            self.op2 = Id012PsithacaOperationContentsList.Op22(self._io, self, self._root)



