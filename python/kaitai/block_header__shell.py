# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import timestamp__protocol
class BlockHeaderShell(KaitaiStruct):
    """Encoding id: block_header.shell
    Description: Block header's shell-related content. It contains information such as the block level, its predecessor and timestamp."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.level = self._io.read_s4be()
        self.proto = self._io.read_u1()
        self.predecessor = self._io.read_bytes(32)
        self.timestamp = timestamp__protocol.TimestampProtocol(self._io)
        self.validation_pass = self._io.read_u1()
        self.operations_hash = self._io.read_bytes(32)
        self.fitness = BlockHeaderShell.Fitness0(self._io, self, self._root)
        self.context = self._io.read_bytes(32)

    class FitnessElem(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.fitness__elem = BlockHeaderShell.BytesDynUint30(self._io, self, self._root)


    class Fitness0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_fitness = self._io.read_u4be()
            if not self.len_fitness <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_fitness, self._io, u"/types/fitness_0/seq/0")
            self._raw_fitness = self._io.read_bytes(self.len_fitness)
            _io__raw_fitness = KaitaiStream(BytesIO(self._raw_fitness))
            self.fitness = BlockHeaderShell.Fitness(_io__raw_fitness, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Fitness(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.fitness_entries = []
            i = 0
            while not self._io.is_eof():
                self.fitness_entries.append(BlockHeaderShell.FitnessEntries(self._io, self, self._root))
                i += 1



    class FitnessEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.fitness__elem = BlockHeaderShell.FitnessElem(self._io, self, self._root)



