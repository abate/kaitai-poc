# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id019PtparisaSmartRollupReveal(KaitaiStruct):
    """Encoding id: 019-PtParisA.smart_rollup.reveal."""

    class Id019PtparisaSmartRollupRevealTag(Enum):
        reveal_raw_data = 0
        reveal_metadata = 1
        request_dal_page = 2
        reveal_dal_parameters = 3

    class InputHashTag(Enum):
        reveal_data_hash_v0 = 0
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_019__ptparisa__smart_rollup__reveal_tag = KaitaiStream.resolve_enum(Id019PtparisaSmartRollupReveal.Id019PtparisaSmartRollupRevealTag, self._io.read_u1())
        if self.id_019__ptparisa__smart_rollup__reveal_tag == Id019PtparisaSmartRollupReveal.Id019PtparisaSmartRollupRevealTag.reveal_raw_data:
            self.reveal_raw_data = Id019PtparisaSmartRollupReveal.InputHash(self._io, self, self._root)

        if self.id_019__ptparisa__smart_rollup__reveal_tag == Id019PtparisaSmartRollupReveal.Id019PtparisaSmartRollupRevealTag.request_dal_page:
            self.request_dal_page = Id019PtparisaSmartRollupReveal.PageId(self._io, self, self._root)


    class InputHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.input_hash_tag = KaitaiStream.resolve_enum(Id019PtparisaSmartRollupReveal.InputHashTag, self._io.read_u1())
            if self.input_hash_tag == Id019PtparisaSmartRollupReveal.InputHashTag.reveal_data_hash_v0:
                self.reveal_data_hash_v0 = self._io.read_bytes(32)



    class PageId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.published_level = self._io.read_s4be()
            self.slot_index = self._io.read_u1()
            self.page_index = self._io.read_s2be()



