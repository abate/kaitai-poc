# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class AlphaSmartRollupWasm200OutputProof(KaitaiStruct):
    """Encoding id: alpha.smart_rollup.wasm_2_0_0.output.proof."""

    class MichelineAlphaMichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10

    class AlphaMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left = 5
        none = 6
        pair_1 = 7
        right = 8
        some_0 = 9
        true = 10
        unit_1 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left_0 = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none_0 = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right_0 = 68
        size = 69
        some = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_0 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction_deprecated = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_deprecated = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140
        chest = 141
        chest_key = 142
        open_chest = 143
        view_0 = 144
        view = 145
        constant = 146
        sub_mutez = 147
        tx_rollup_l2_address = 148
        min_block_time = 149
        sapling_transaction = 150
        emit = 151
        lambda_rec_0 = 152
        lambda_rec = 153
        ticket_0 = 154
        bytes_0 = 155
        nat_0 = 156
        ticket_1 = 157

    class TreeEncodingTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__192 = 192
        case__193 = 193
        case__195 = 195
        case__200 = 200
        case__208 = 208
        case__216 = 216
        case__217 = 217
        case__218 = 218
        case__219 = 219
        case__224 = 224

    class OutputProofTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class AlphaContractIdOriginatedTag(Enum):
        originated = 1

    class Bool(Enum):
        false = 0
        true = 255

    class InodeTreeTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__192 = 192
        case__208 = 208
        case__209 = 209
        case__210 = 210
        case__211 = 211
        case__224 = 224

    class MessageTag(Enum):
        atomic_transaction_batch = 0
        atomic_transaction_batch_typed = 1
        whitelist_update = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.output_proof = AlphaSmartRollupWasm200OutputProof.OutputProof(self._io, self, self._root)
        self.output_proof_output = AlphaSmartRollupWasm200OutputProof.OutputProofOutput(self._io, self, self._root)

    class Case131EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131_elt_field0 = self._io.read_u1()
            if not self.len_case__131_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__131_elt_field0, self._io, u"/types/case__131_elt_field0_0/seq/0")
            self._raw_case__131_elt_field0 = self._io.read_bytes(self.len_case__131_elt_field0)
            _io__raw_case__131_elt_field0 = KaitaiStream(BytesIO(self._raw_case__131_elt_field0))
            self.case__131_elt_field0 = AlphaSmartRollupWasm200OutputProof.Case131EltField0(_io__raw_case__131_elt_field0, self, self._root)


    class Case211Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__211_field1 = self._io.read_bytes_full()


    class Case1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__1_field0 = self._io.read_u2be()
            self.case__1_field1 = AlphaSmartRollupWasm200OutputProof.Case1Field1(self._io, self, self._root)


    class Case2Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__2_field1_field0 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)
            self.case__2_field1_field1 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class Case192(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__192 = self._io.read_bytes_full()


    class Case211(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__211_field0 = self._io.read_s8be()
            self.case__211_field1 = AlphaSmartRollupWasm200OutputProof.Case211Field10(self._io, self, self._root)
            self.case__211_field2 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class Case10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__1_field0 = self._io.read_s2be()
            self.case__1_field1 = self._io.read_bytes(32)
            self.case__1_field2 = self._io.read_bytes(32)
            self.case__1_field3 = AlphaSmartRollupWasm200OutputProof.TreeEncoding(self._io, self, self._root)


    class Whitelist0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_whitelist = self._io.read_u4be()
            if not self.len_whitelist <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_whitelist, self._io, u"/types/whitelist_0/seq/0")
            self._raw_whitelist = self._io.read_bytes(self.len_whitelist)
            _io__raw_whitelist = KaitaiStream(BytesIO(self._raw_whitelist))
            self.whitelist = AlphaSmartRollupWasm200OutputProof.Whitelist(_io__raw_whitelist, self, self._root)


    class Transactions2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_transactions = self._io.read_u4be()
            if not self.len_transactions <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_transactions, self._io, u"/types/transactions_2/seq/0")
            self._raw_transactions = self._io.read_bytes(self.len_transactions)
            _io__raw_transactions = KaitaiStream(BytesIO(self._raw_transactions))
            self.transactions = AlphaSmartRollupWasm200OutputProof.Transactions1(_io__raw_transactions, self, self._root)


    class InodeTree(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inode_tree_tag = KaitaiStream.resolve_enum(AlphaSmartRollupWasm200OutputProof.InodeTreeTag, self._io.read_u1())
            if self.inode_tree_tag == AlphaSmartRollupWasm200OutputProof.InodeTreeTag.case__0:
                self.case__0 = AlphaSmartRollupWasm200OutputProof.Case0(self._io, self, self._root)

            if self.inode_tree_tag == AlphaSmartRollupWasm200OutputProof.InodeTreeTag.case__1:
                self.case__1 = AlphaSmartRollupWasm200OutputProof.Case1(self._io, self, self._root)

            if self.inode_tree_tag == AlphaSmartRollupWasm200OutputProof.InodeTreeTag.case__2:
                self.case__2 = AlphaSmartRollupWasm200OutputProof.Case2(self._io, self, self._root)

            if self.inode_tree_tag == AlphaSmartRollupWasm200OutputProof.InodeTreeTag.case__3:
                self.case__3 = AlphaSmartRollupWasm200OutputProof.Case3(self._io, self, self._root)

            if self.inode_tree_tag == AlphaSmartRollupWasm200OutputProof.InodeTreeTag.case__129:
                self.case__129 = AlphaSmartRollupWasm200OutputProof.Case129Entries(self._io, self, self._root)

            if self.inode_tree_tag == AlphaSmartRollupWasm200OutputProof.InodeTreeTag.case__130:
                self.case__130 = AlphaSmartRollupWasm200OutputProof.Case130Entries(self._io, self, self._root)

            if self.inode_tree_tag == AlphaSmartRollupWasm200OutputProof.InodeTreeTag.case__131:
                self.case__131 = AlphaSmartRollupWasm200OutputProof.Case1310(self._io, self, self._root)

            if self.inode_tree_tag == AlphaSmartRollupWasm200OutputProof.InodeTreeTag.case__192:
                self.case__192 = self._io.read_bytes(32)

            if self.inode_tree_tag == AlphaSmartRollupWasm200OutputProof.InodeTreeTag.case__208:
                self.case__208 = AlphaSmartRollupWasm200OutputProof.Case208(self._io, self, self._root)

            if self.inode_tree_tag == AlphaSmartRollupWasm200OutputProof.InodeTreeTag.case__209:
                self.case__209 = AlphaSmartRollupWasm200OutputProof.Case209(self._io, self, self._root)

            if self.inode_tree_tag == AlphaSmartRollupWasm200OutputProof.InodeTreeTag.case__210:
                self.case__210 = AlphaSmartRollupWasm200OutputProof.Case210(self._io, self, self._root)

            if self.inode_tree_tag == AlphaSmartRollupWasm200OutputProof.InodeTreeTag.case__211:
                self.case__211 = AlphaSmartRollupWasm200OutputProof.Case211(self._io, self, self._root)



    class Case20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__2_field0 = self._io.read_s2be()
            self.case__2_field1 = self._io.read_bytes(32)
            self.case__2_field2 = self._io.read_bytes(32)
            self.case__2_field3 = AlphaSmartRollupWasm200OutputProof.TreeEncoding(self._io, self, self._root)


    class Case0Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field1_field0 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)
            self.case__0_field1_field1 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = AlphaSmartRollupWasm200OutputProof.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Case130Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__130_elt_field0 = AlphaSmartRollupWasm200OutputProof.Case130EltField00(self._io, self, self._root)
            self.case__130_elt_field1 = AlphaSmartRollupWasm200OutputProof.TreeEncoding(self._io, self, self._root)


    class AlphaMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__michelson__v1__primitives = KaitaiStream.resolve_enum(AlphaSmartRollupWasm200OutputProof.AlphaMichelsonV1Primitives, self._io.read_u1())


    class Case208Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__208_field1 = self._io.read_bytes_full()


    class Case129EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__129_elt_field0 = self._io.read_bytes_full()


    class Case218(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__218_field0 = self._io.read_s4be()
            self.case__218_field1 = AlphaSmartRollupWasm200OutputProof.Case218Field10(self._io, self, self._root)
            self.case__218_field2 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class Case1310(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131 = self._io.read_u4be()
            if not self.len_case__131 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__131, self._io, u"/types/case__131_0/seq/0")
            self._raw_case__131 = self._io.read_bytes(self.len_case__131)
            _io__raw_case__131 = KaitaiStream(BytesIO(self._raw_case__131))
            self.case__131 = AlphaSmartRollupWasm200OutputProof.Case131(_io__raw_case__131, self, self._root)


    class Case0Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field1_field0 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)
            self.case__0_field1_field1 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class OutputProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.output_proof_tag = KaitaiStream.resolve_enum(AlphaSmartRollupWasm200OutputProof.OutputProofTag, self._io.read_u1())
            if self.output_proof_tag == AlphaSmartRollupWasm200OutputProof.OutputProofTag.case__0:
                self.case__0 = AlphaSmartRollupWasm200OutputProof.Case01(self._io, self, self._root)

            if self.output_proof_tag == AlphaSmartRollupWasm200OutputProof.OutputProofTag.case__2:
                self.case__2 = AlphaSmartRollupWasm200OutputProof.Case20(self._io, self, self._root)

            if self.output_proof_tag == AlphaSmartRollupWasm200OutputProof.OutputProofTag.case__1:
                self.case__1 = AlphaSmartRollupWasm200OutputProof.Case10(self._io, self, self._root)

            if self.output_proof_tag == AlphaSmartRollupWasm200OutputProof.OutputProofTag.case__3:
                self.case__3 = AlphaSmartRollupWasm200OutputProof.Case30(self._io, self, self._root)



    class Case0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field0 = self._io.read_u1()
            self.case__0_field1 = AlphaSmartRollupWasm200OutputProof.Case0Field1(self._io, self, self._root)


    class Case210Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__210_field1 = self._io.read_bytes_full()


    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = AlphaSmartRollupWasm200OutputProof.AlphaMichelsonV1Primitives(self._io, self, self._root)
            self.arg = AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.annots = AlphaSmartRollupWasm200OutputProof.BytesDynUint30(self._io, self, self._root)


    class Case210(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__210_field0 = self._io.read_s4be()
            self.case__210_field1 = AlphaSmartRollupWasm200OutputProof.Case210Field10(self._io, self, self._root)
            self.case__210_field2 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class OutputProofOutput(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.outbox_level = self._io.read_s4be()
            self.message_index = AlphaSmartRollupWasm200OutputProof.N(self._io, self, self._root)
            self.message = AlphaSmartRollupWasm200OutputProof.Message(self._io, self, self._root)


    class Case216Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__216_field1 = self._io.read_bytes_full()


    class AlphaContractIdOriginated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__contract_id__originated_tag = KaitaiStream.resolve_enum(AlphaSmartRollupWasm200OutputProof.AlphaContractIdOriginatedTag, self._io.read_u1())
            if self.alpha__contract_id__originated_tag == AlphaSmartRollupWasm200OutputProof.AlphaContractIdOriginatedTag.originated:
                self.originated = AlphaSmartRollupWasm200OutputProof.Originated(self._io, self, self._root)



    class Case217Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__217_field1 = self._io.read_bytes_full()


    class Case193(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__193 = self._io.read_bytes_full()


    class Case30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__3_field0 = self._io.read_s2be()
            self.case__3_field1 = self._io.read_bytes(32)
            self.case__3_field2 = self._io.read_bytes(32)
            self.case__3_field3 = AlphaSmartRollupWasm200OutputProof.TreeEncoding(self._io, self, self._root)


    class Case208Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__208_field1 = self._io.read_u1()
            if not self.len_case__208_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__208_field1, self._io, u"/types/case__208_field1_0/seq/0")
            self._raw_case__208_field1 = self._io.read_bytes(self.len_case__208_field1)
            _io__raw_case__208_field1 = KaitaiStream(BytesIO(self._raw_case__208_field1))
            self.case__208_field1 = AlphaSmartRollupWasm200OutputProof.Case208Field1(_io__raw_case__208_field1, self, self._root)


    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = AlphaSmartRollupWasm200OutputProof.AlphaMichelsonV1Primitives(self._io, self, self._root)
            self.annots = AlphaSmartRollupWasm200OutputProof.BytesDynUint30(self._io, self, self._root)


    class Case1Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__1_field1_field0 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)
            self.case__1_field1_field1 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class WhitelistEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.signature__public_key_hash = AlphaSmartRollupWasm200OutputProof.PublicKeyHash(self._io, self, self._root)


    class Case219(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__219_field0 = self._io.read_s8be()
            self.case__219_field1 = AlphaSmartRollupWasm200OutputProof.Case219Field10(self._io, self, self._root)
            self.case__219_field2 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class Case219Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__219_field1 = self._io.read_u1()
            if not self.len_case__219_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__219_field1, self._io, u"/types/case__219_field1_0/seq/0")
            self._raw_case__219_field1 = self._io.read_bytes(self.len_case__219_field1)
            _io__raw_case__219_field1 = KaitaiStream(BytesIO(self._raw_case__219_field1))
            self.case__219_field1 = AlphaSmartRollupWasm200OutputProof.Case219Field1(_io__raw_case__219_field1, self, self._root)


    class TransactionsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.parameters = AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.destination = AlphaSmartRollupWasm200OutputProof.AlphaContractIdOriginated(self._io, self, self._root)
            self.entrypoint = AlphaSmartRollupWasm200OutputProof.BytesDynUint30(self._io, self, self._root)


    class Case217Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__217_field1 = self._io.read_u1()
            if not self.len_case__217_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__217_field1, self._io, u"/types/case__217_field1_0/seq/0")
            self._raw_case__217_field1 = self._io.read_bytes(self.len_case__217_field1)
            _io__raw_case__217_field1 = KaitaiStream(BytesIO(self._raw_case__217_field1))
            self.case__217_field1 = AlphaSmartRollupWasm200OutputProof.Case217Field1(_io__raw_case__217_field1, self, self._root)


    class Case129EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__129_elt_field0 = self._io.read_u1()
            if not self.len_case__129_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__129_elt_field0, self._io, u"/types/case__129_elt_field0_0/seq/0")
            self._raw_case__129_elt_field0 = self._io.read_bytes(self.len_case__129_elt_field0)
            _io__raw_case__129_elt_field0 = KaitaiStream(BytesIO(self._raw_case__129_elt_field0))
            self.case__129_elt_field0 = AlphaSmartRollupWasm200OutputProof.Case129EltField0(_io__raw_case__129_elt_field0, self, self._root)


    class Case210Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__210_field1 = self._io.read_u1()
            if not self.len_case__210_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__210_field1, self._io, u"/types/case__210_field1_0/seq/0")
            self._raw_case__210_field1 = self._io.read_bytes(self.len_case__210_field1)
            _io__raw_case__210_field1 = KaitaiStream(BytesIO(self._raw_case__210_field1))
            self.case__210_field1 = AlphaSmartRollupWasm200OutputProof.Case210Field1(_io__raw_case__210_field1, self, self._root)


    class TreeEncoding(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.tree_encoding_tag = KaitaiStream.resolve_enum(AlphaSmartRollupWasm200OutputProof.TreeEncodingTag, self._io.read_u1())
            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__0:
                self.case__0 = AlphaSmartRollupWasm200OutputProof.Case00(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__1:
                self.case__1 = AlphaSmartRollupWasm200OutputProof.Case1(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__2:
                self.case__2 = AlphaSmartRollupWasm200OutputProof.Case2(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__3:
                self.case__3 = AlphaSmartRollupWasm200OutputProof.Case3(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__129:
                self.case__129 = AlphaSmartRollupWasm200OutputProof.Case129Entries(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__130:
                self.case__130 = AlphaSmartRollupWasm200OutputProof.Case130Entries(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__131:
                self.case__131 = AlphaSmartRollupWasm200OutputProof.Case1311(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__192:
                self.case__192 = AlphaSmartRollupWasm200OutputProof.Case1920(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__193:
                self.case__193 = AlphaSmartRollupWasm200OutputProof.Case1930(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__195:
                self.case__195 = AlphaSmartRollupWasm200OutputProof.BytesDynUint30(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__200:
                self.case__200 = self._io.read_bytes(32)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__208:
                self.case__208 = self._io.read_bytes(32)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__216:
                self.case__216 = AlphaSmartRollupWasm200OutputProof.Case216(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__217:
                self.case__217 = AlphaSmartRollupWasm200OutputProof.Case217(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__218:
                self.case__218 = AlphaSmartRollupWasm200OutputProof.Case218(self._io, self, self._root)

            if self.tree_encoding_tag == AlphaSmartRollupWasm200OutputProof.TreeEncodingTag.case__219:
                self.case__219 = AlphaSmartRollupWasm200OutputProof.Case219(self._io, self, self._root)



    class Case208(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__208_field0 = self._io.read_u1()
            self.case__208_field1 = AlphaSmartRollupWasm200OutputProof.Case208Field10(self._io, self, self._root)
            self.case__208_field2 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class Case1311(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131 = self._io.read_u4be()
            if not self.len_case__131 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__131, self._io, u"/types/case__131_1/seq/0")
            self._raw_case__131 = self._io.read_bytes(self.len_case__131)
            _io__raw_case__131 = KaitaiStream(BytesIO(self._raw_case__131))
            self.case__131 = AlphaSmartRollupWasm200OutputProof.Case131(_io__raw_case__131, self, self._root)


    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = AlphaSmartRollupWasm200OutputProof.AlphaMichelsonV1Primitives(self._io, self, self._root)
            self.args = AlphaSmartRollupWasm200OutputProof.Args0(self._io, self, self._root)
            self.annots = AlphaSmartRollupWasm200OutputProof.BytesDynUint30(self._io, self, self._root)


    class Case131(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__131_entries.append(AlphaSmartRollupWasm200OutputProof.Case131Entries(self._io, self, self._root))
                i += 1



    class Case01(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field0 = self._io.read_s2be()
            self.case__0_field1 = self._io.read_bytes(32)
            self.case__0_field2 = self._io.read_bytes(32)
            self.case__0_field3 = AlphaSmartRollupWasm200OutputProof.TreeEncoding(self._io, self, self._root)


    class Case2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__2_field0 = self._io.read_s4be()
            self.case__2_field1 = AlphaSmartRollupWasm200OutputProof.Case2Field1(self._io, self, self._root)


    class TransactionsEntries0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.parameters = AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.parameters_ty = AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.destination = AlphaSmartRollupWasm200OutputProof.AlphaContractIdOriginated(self._io, self, self._root)
            self.entrypoint = AlphaSmartRollupWasm200OutputProof.BytesDynUint30(self._io, self, self._root)


    class Transactions0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_transactions = self._io.read_u4be()
            if not self.len_transactions <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_transactions, self._io, u"/types/transactions_0/seq/0")
            self._raw_transactions = self._io.read_bytes(self.len_transactions)
            _io__raw_transactions = KaitaiStream(BytesIO(self._raw_transactions))
            self.transactions = AlphaSmartRollupWasm200OutputProof.Transactions(_io__raw_transactions, self, self._root)


    class Case129Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__129_elt_field0 = AlphaSmartRollupWasm200OutputProof.Case129EltField00(self._io, self, self._root)
            self.case__129_elt_field1 = AlphaSmartRollupWasm200OutputProof.TreeEncoding(self._io, self, self._root)


    class Case217(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__217_field0 = self._io.read_u2be()
            self.case__217_field1 = AlphaSmartRollupWasm200OutputProof.Case217Field10(self._io, self, self._root)
            self.case__217_field2 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class Case219Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__219_field1 = self._io.read_bytes_full()


    class Case130EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__130_elt_field0 = self._io.read_bytes_full()


    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = AlphaSmartRollupWasm200OutputProof.Sequence(_io__raw_sequence, self, self._root)


    class MichelineAlphaMichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__alpha__michelson_v1__expression_tag = KaitaiStream.resolve_enum(AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__alpha__michelson_v1__expression_tag == AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1ExpressionTag.int:
                self.int = AlphaSmartRollupWasm200OutputProof.Z(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1ExpressionTag.string:
                self.string = AlphaSmartRollupWasm200OutputProof.BytesDynUint30(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1ExpressionTag.sequence:
                self.sequence = AlphaSmartRollupWasm200OutputProof.Sequence0(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = AlphaSmartRollupWasm200OutputProof.AlphaMichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = AlphaSmartRollupWasm200OutputProof.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = AlphaSmartRollupWasm200OutputProof.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = AlphaSmartRollupWasm200OutputProof.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = AlphaSmartRollupWasm200OutputProof.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = AlphaSmartRollupWasm200OutputProof.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = AlphaSmartRollupWasm200OutputProof.PrimGeneric(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1ExpressionTag.bytes:
                self.bytes = AlphaSmartRollupWasm200OutputProof.BytesDynUint30(self._io, self, self._root)



    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(AlphaSmartRollupWasm200OutputProof.SequenceEntries(self._io, self, self._root))
                i += 1



    class Case131EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_elt_field0 = self._io.read_bytes_full()


    class Case3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__3_field0 = self._io.read_s8be()
            self.case__3_field1 = AlphaSmartRollupWasm200OutputProof.Case3Field1(self._io, self, self._root)


    class Case211Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__211_field1 = self._io.read_u1()
            if not self.len_case__211_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__211_field1, self._io, u"/types/case__211_field1_0/seq/0")
            self._raw_case__211_field1 = self._io.read_bytes(self.len_case__211_field1)
            _io__raw_case__211_field1 = KaitaiStream(BytesIO(self._raw_case__211_field1))
            self.case__211_field1 = AlphaSmartRollupWasm200OutputProof.Case211Field1(_io__raw_case__211_field1, self, self._root)


    class Case1930(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__193 = self._io.read_u2be()
            if not self.len_case__193 <= 65535:
                raise kaitaistruct.ValidationGreaterThanError(65535, self.len_case__193, self._io, u"/types/case__193_0/seq/0")
            self._raw_case__193 = self._io.read_bytes(self.len_case__193)
            _io__raw_case__193 = KaitaiStream(BytesIO(self._raw_case__193))
            self.case__193 = AlphaSmartRollupWasm200OutputProof.Case193(_io__raw_case__193, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Case131Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_elt_field0 = AlphaSmartRollupWasm200OutputProof.Case131EltField00(self._io, self, self._root)
            self.case__131_elt_field1 = AlphaSmartRollupWasm200OutputProof.TreeEncoding(self._io, self, self._root)


    class Case209Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__209_field1 = self._io.read_u1()
            if not self.len_case__209_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__209_field1, self._io, u"/types/case__209_field1_0/seq/0")
            self._raw_case__209_field1 = self._io.read_bytes(self.len_case__209_field1)
            _io__raw_case__209_field1 = KaitaiStream(BytesIO(self._raw_case__209_field1))
            self.case__209_field1 = AlphaSmartRollupWasm200OutputProof.Case209Field1(_io__raw_case__209_field1, self, self._root)


    class WhitelistUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.whitelist_tag = KaitaiStream.resolve_enum(AlphaSmartRollupWasm200OutputProof.Bool, self._io.read_u1())
            if self.whitelist_tag == AlphaSmartRollupWasm200OutputProof.Bool.true:
                self.whitelist = AlphaSmartRollupWasm200OutputProof.Whitelist0(self._io, self, self._root)



    class Case209(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__209_field0 = self._io.read_u2be()
            self.case__209_field1 = AlphaSmartRollupWasm200OutputProof.Case209Field10(self._io, self, self._root)
            self.case__209_field2 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class Case00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field0 = self._io.read_u1()
            self.case__0_field1 = AlphaSmartRollupWasm200OutputProof.Case0Field10(self._io, self, self._root)


    class Message(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_tag = KaitaiStream.resolve_enum(AlphaSmartRollupWasm200OutputProof.MessageTag, self._io.read_u1())
            if self.message_tag == AlphaSmartRollupWasm200OutputProof.MessageTag.atomic_transaction_batch:
                self.atomic_transaction_batch = AlphaSmartRollupWasm200OutputProof.Transactions0(self._io, self, self._root)

            if self.message_tag == AlphaSmartRollupWasm200OutputProof.MessageTag.atomic_transaction_batch_typed:
                self.atomic_transaction_batch_typed = AlphaSmartRollupWasm200OutputProof.Transactions2(self._io, self, self._root)

            if self.message_tag == AlphaSmartRollupWasm200OutputProof.MessageTag.whitelist_update:
                self.whitelist_update = AlphaSmartRollupWasm200OutputProof.WhitelistUpdate(self._io, self, self._root)



    class Whitelist(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.whitelist_entries = []
            i = 0
            while not self._io.is_eof():
                self.whitelist_entries.append(AlphaSmartRollupWasm200OutputProof.WhitelistEntries(self._io, self, self._root))
                i += 1



    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(AlphaSmartRollupWasm200OutputProof.ArgsEntries(self._io, self, self._root))
                i += 1



    class Case209Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__209_field1 = self._io.read_bytes_full()


    class Case216(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__216_field0 = self._io.read_u1()
            self.case__216_field1 = AlphaSmartRollupWasm200OutputProof.Case216Field10(self._io, self, self._root)
            self.case__216_field2 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class Case216Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__216_field1 = self._io.read_u1()
            if not self.len_case__216_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__216_field1, self._io, u"/types/case__216_field1_0/seq/0")
            self._raw_case__216_field1 = self._io.read_bytes(self.len_case__216_field1)
            _io__raw_case__216_field1 = KaitaiStream(BytesIO(self._raw_case__216_field1))
            self.case__216_field1 = AlphaSmartRollupWasm200OutputProof.Case216Field1(_io__raw_case__216_field1, self, self._root)


    class Case218Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__218_field1 = self._io.read_bytes_full()


    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = AlphaSmartRollupWasm200OutputProof.AlphaMichelsonV1Primitives(self._io, self, self._root)
            self.arg = AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)


    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = AlphaSmartRollupWasm200OutputProof.AlphaMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)


    class Transactions1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.transactions_entries = []
            i = 0
            while not self._io.is_eof():
                self.transactions_entries.append(AlphaSmartRollupWasm200OutputProof.TransactionsEntries0(self._io, self, self._root))
                i += 1



    class Case3Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__3_field1_field0 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)
            self.case__3_field1_field1 = AlphaSmartRollupWasm200OutputProof.InodeTree(self._io, self, self._root)


    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)


    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = AlphaSmartRollupWasm200OutputProof.Args(_io__raw_args, self, self._root)


    class Case218Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__218_field1 = self._io.read_u1()
            if not self.len_case__218_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__218_field1, self._io, u"/types/case__218_field1_0/seq/0")
            self._raw_case__218_field1 = self._io.read_bytes(self.len_case__218_field1)
            _io__raw_case__218_field1 = KaitaiStream(BytesIO(self._raw_case__218_field1))
            self.case__218_field1 = AlphaSmartRollupWasm200OutputProof.Case218Field1(_io__raw_case__218_field1, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = AlphaSmartRollupWasm200OutputProof.AlphaMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = AlphaSmartRollupWasm200OutputProof.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.annots = AlphaSmartRollupWasm200OutputProof.BytesDynUint30(self._io, self, self._root)


    class Case1920(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__192 = self._io.read_u1()
            if not self.len_case__192 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__192, self._io, u"/types/case__192_0/seq/0")
            self._raw_case__192 = self._io.read_bytes(self.len_case__192)
            _io__raw_case__192 = KaitaiStream(BytesIO(self._raw_case__192))
            self.case__192 = AlphaSmartRollupWasm200OutputProof.Case192(_io__raw_case__192, self, self._root)


    class Case130EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__130_elt_field0 = self._io.read_u1()
            if not self.len_case__130_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__130_elt_field0, self._io, u"/types/case__130_elt_field0_0/seq/0")
            self._raw_case__130_elt_field0 = self._io.read_bytes(self.len_case__130_elt_field0)
            _io__raw_case__130_elt_field0 = KaitaiStream(BytesIO(self._raw_case__130_elt_field0))
            self.case__130_elt_field0 = AlphaSmartRollupWasm200OutputProof.Case130EltField0(_io__raw_case__130_elt_field0, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(AlphaSmartRollupWasm200OutputProof.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == AlphaSmartRollupWasm200OutputProof.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaSmartRollupWasm200OutputProof.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaSmartRollupWasm200OutputProof.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaSmartRollupWasm200OutputProof.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class Transactions(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.transactions_entries = []
            i = 0
            while not self._io.is_eof():
                self.transactions_entries.append(AlphaSmartRollupWasm200OutputProof.TransactionsEntries(self._io, self, self._root))
                i += 1



    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = AlphaSmartRollupWasm200OutputProof.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




