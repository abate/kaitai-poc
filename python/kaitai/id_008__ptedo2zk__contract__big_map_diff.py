# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id008Ptedo2zkContractBigMapDiff(KaitaiStruct):
    """Encoding id: 008-PtEdo2Zk.contract.big_map_diff."""

    class Bool(Enum):
        false = 0
        true = 255

    class Id008Ptedo2zkContractBigMapDiffEltTag(Enum):
        update = 0
        remove = 1
        copy = 2
        alloc = 3

    class Id008Ptedo2zkMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left_0 = 5
        none = 6
        pair_1 = 7
        right_0 = 8
        some = 9
        true = 10
        unit_0 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none_0 = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right = 68
        size = 69
        some_0 = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_1 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_0 = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140

    class Micheline008Ptedo2zkMichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.len_id_008__ptedo2zk__contract__big_map_diff = self._io.read_u4be()
        if not self.len_id_008__ptedo2zk__contract__big_map_diff <= 1073741823:
            raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_008__ptedo2zk__contract__big_map_diff, self._io, u"/seq/0")
        self._raw_id_008__ptedo2zk__contract__big_map_diff = self._io.read_bytes(self.len_id_008__ptedo2zk__contract__big_map_diff)
        _io__raw_id_008__ptedo2zk__contract__big_map_diff = KaitaiStream(BytesIO(self._raw_id_008__ptedo2zk__contract__big_map_diff))
        self.id_008__ptedo2zk__contract__big_map_diff = Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiff(_io__raw_id_008__ptedo2zk__contract__big_map_diff, self, self._root)

    class Alloc(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.big_map = Id008Ptedo2zkContractBigMapDiff.Z(self._io, self, self._root)
            self.key_type = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression(self._io, self, self._root)
            self.value_type = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression(self._io, self, self._root)


    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id008Ptedo2zkContractBigMapDiff.BytesDynUint30(self._io, self, self._root)


    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkMichelsonV1Primitives(self._io, self, self._root)
            self.annots = Id008Ptedo2zkContractBigMapDiff.BytesDynUint30(self._io, self, self._root)


    class Id008Ptedo2zkContractBigMapDiffEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__contract__big_map_diff_elt_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiffEltTag, self._io.read_u1())
            if self.id_008__ptedo2zk__contract__big_map_diff_elt_tag == Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiffEltTag.update:
                self.update = Id008Ptedo2zkContractBigMapDiff.Update(self._io, self, self._root)

            if self.id_008__ptedo2zk__contract__big_map_diff_elt_tag == Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiffEltTag.remove:
                self.remove = Id008Ptedo2zkContractBigMapDiff.Z(self._io, self, self._root)

            if self.id_008__ptedo2zk__contract__big_map_diff_elt_tag == Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiffEltTag.copy:
                self.copy = Id008Ptedo2zkContractBigMapDiff.Copy(self._io, self, self._root)

            if self.id_008__ptedo2zk__contract__big_map_diff_elt_tag == Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiffEltTag.alloc:
                self.alloc = Id008Ptedo2zkContractBigMapDiff.Alloc(self._io, self, self._root)



    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkMichelsonV1Primitives(self._io, self, self._root)
            self.args = Id008Ptedo2zkContractBigMapDiff.Args0(self._io, self, self._root)
            self.annots = Id008Ptedo2zkContractBigMapDiff.BytesDynUint30(self._io, self, self._root)


    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = Id008Ptedo2zkContractBigMapDiff.Sequence(_io__raw_sequence, self, self._root)


    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(Id008Ptedo2zkContractBigMapDiff.SequenceEntries(self._io, self, self._root))
                i += 1



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Copy(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source_big_map = Id008Ptedo2zkContractBigMapDiff.Z(self._io, self, self._root)
            self.destination_big_map = Id008Ptedo2zkContractBigMapDiff.Z(self._io, self, self._root)


    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(Id008Ptedo2zkContractBigMapDiff.ArgsEntries(self._io, self, self._root))
                i += 1



    class Micheline008Ptedo2zkMichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__008__ptedo2zk__michelson_v1__expression_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__008__ptedo2zk__michelson_v1__expression_tag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.int:
                self.int = Id008Ptedo2zkContractBigMapDiff.Z(self._io, self, self._root)

            if self.micheline__008__ptedo2zk__michelson_v1__expression_tag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.string:
                self.string = Id008Ptedo2zkContractBigMapDiff.BytesDynUint30(self._io, self, self._root)

            if self.micheline__008__ptedo2zk__michelson_v1__expression_tag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.sequence:
                self.sequence = Id008Ptedo2zkContractBigMapDiff.Sequence0(self._io, self, self._root)

            if self.micheline__008__ptedo2zk__michelson_v1__expression_tag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkMichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__008__ptedo2zk__michelson_v1__expression_tag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = Id008Ptedo2zkContractBigMapDiff.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__008__ptedo2zk__michelson_v1__expression_tag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = Id008Ptedo2zkContractBigMapDiff.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__008__ptedo2zk__michelson_v1__expression_tag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = Id008Ptedo2zkContractBigMapDiff.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__008__ptedo2zk__michelson_v1__expression_tag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = Id008Ptedo2zkContractBigMapDiff.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__008__ptedo2zk__michelson_v1__expression_tag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = Id008Ptedo2zkContractBigMapDiff.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__008__ptedo2zk__michelson_v1__expression_tag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = Id008Ptedo2zkContractBigMapDiff.PrimGeneric(self._io, self, self._root)

            if self.micheline__008__ptedo2zk__michelson_v1__expression_tag == Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1ExpressionTag.bytes:
                self.bytes = Id008Ptedo2zkContractBigMapDiff.BytesDynUint30(self._io, self, self._root)



    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression(self._io, self, self._root)


    class Update(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.big_map = Id008Ptedo2zkContractBigMapDiff.Z(self._io, self, self._root)
            self.key_hash = self._io.read_bytes(32)
            self.key = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression(self._io, self, self._root)
            self.value_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkContractBigMapDiff.Bool, self._io.read_u1())
            if self.value_tag == Id008Ptedo2zkContractBigMapDiff.Bool.true:
                self.value = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression(self._io, self, self._root)



    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression(self._io, self, self._root)


    class Id008Ptedo2zkMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__michelson__v1__primitives = KaitaiStream.resolve_enum(Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkMichelsonV1Primitives, self._io.read_u1())


    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression(self._io, self, self._root)


    class Id008Ptedo2zkContractBigMapDiff(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__contract__big_map_diff_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_008__ptedo2zk__contract__big_map_diff_entries.append(Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkContractBigMapDiffEntries(self._io, self, self._root))
                i += 1



    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = Id008Ptedo2zkContractBigMapDiff.Args(_io__raw_args, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id008Ptedo2zkContractBigMapDiff.Id008Ptedo2zkMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id008Ptedo2zkContractBigMapDiff.Micheline008Ptedo2zkMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id008Ptedo2zkContractBigMapDiff.BytesDynUint30(self._io, self, self._root)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id008Ptedo2zkContractBigMapDiff.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




