# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id012PsithacaReceiptBalanceUpdates(KaitaiStruct):
    """Encoding id: 012-Psithaca.receipt.balance_updates."""

    class Id012PsithacaOperationMetadataAlphaBalanceTag(Enum):
        contract = 0
        legacy_rewards = 1
        block_fees = 2
        legacy_deposits = 3
        deposits = 4
        nonce_revelation_rewards = 5
        double_signing_evidence_rewards = 6
        endorsing_rewards = 7
        baking_rewards = 8
        baking_bonuses = 9
        legacy_fees = 10
        storage_fees = 11
        double_signing_punishments = 12
        lost_endorsing_rewards = 13
        liquidity_baking_subsidies = 14
        burned = 15
        commitments = 16
        bootstrap = 17
        invoice = 18
        initial_commitments = 19
        minted = 20

    class Id012PsithacaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class OriginTag(Enum):
        block_application = 0
        protocol_migration = 1
        subsidy = 2
        simulation = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_012__psithaca__operation_metadata__alpha__balance_updates = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Id012PsithacaOperationMetadataAlphaBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Id012PsithacaOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__operation_metadata__alpha__balance = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalance(self._io, self, self._root)
            self.id_012__psithaca__operation_metadata__alpha__balance_update = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceUpdate(self._io, self, self._root)
            self.id_012__psithaca__operation_metadata__alpha__update_origin = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaUpdateOrigin(self._io, self, self._root)


    class Id012PsithacaOperationMetadataAlphaUpdateOrigin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.origin = KaitaiStream.resolve_enum(Id012PsithacaReceiptBalanceUpdates.OriginTag, self._io.read_u1())


    class Id012PsithacaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__contract_id_tag = KaitaiStream.resolve_enum(Id012PsithacaReceiptBalanceUpdates.Id012PsithacaContractIdTag, self._io.read_u1())
            if self.id_012__psithaca__contract_id_tag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaContractIdTag.implicit:
                self.implicit = Id012PsithacaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_012__psithaca__contract_id_tag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaContractIdTag.originated:
                self.originated = Id012PsithacaReceiptBalanceUpdates.Originated(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class LostEndorsingRewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id012PsithacaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.participation = KaitaiStream.resolve_enum(Id012PsithacaReceiptBalanceUpdates.Bool, self._io.read_u1())
            self.revelation = KaitaiStream.resolve_enum(Id012PsithacaReceiptBalanceUpdates.Bool, self._io.read_u1())


    class LegacyDeposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id012PsithacaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id012PsithacaOperationMetadataAlphaBalance(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__operation_metadata__alpha__balance_tag = KaitaiStream.resolve_enum(Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag, self._io.read_u1())
            if self.id_012__psithaca__operation_metadata__alpha__balance_tag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.contract:
                self.contract = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaContractId(self._io, self, self._root)

            if self.id_012__psithaca__operation_metadata__alpha__balance_tag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.legacy_rewards:
                self.legacy_rewards = Id012PsithacaReceiptBalanceUpdates.LegacyRewards(self._io, self, self._root)

            if self.id_012__psithaca__operation_metadata__alpha__balance_tag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.legacy_deposits:
                self.legacy_deposits = Id012PsithacaReceiptBalanceUpdates.LegacyDeposits(self._io, self, self._root)

            if self.id_012__psithaca__operation_metadata__alpha__balance_tag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.deposits:
                self.deposits = Id012PsithacaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_012__psithaca__operation_metadata__alpha__balance_tag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.legacy_fees:
                self.legacy_fees = Id012PsithacaReceiptBalanceUpdates.LegacyFees(self._io, self, self._root)

            if self.id_012__psithaca__operation_metadata__alpha__balance_tag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.lost_endorsing_rewards:
                self.lost_endorsing_rewards = Id012PsithacaReceiptBalanceUpdates.LostEndorsingRewards(self._io, self, self._root)

            if self.id_012__psithaca__operation_metadata__alpha__balance_tag == Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceTag.commitments:
                self.commitments = self._io.read_bytes(20)



    class LegacyRewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id012PsithacaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id012PsithacaOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_012__psithaca__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_012__psithaca__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_012__psithaca__operation_metadata__alpha__balance_updates, self._io, u"/types/id_012__psithaca__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_012__psithaca__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_012__psithaca__operation_metadata__alpha__balance_updates)
            _io__raw_id_012__psithaca__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_012__psithaca__operation_metadata__alpha__balance_updates))
            self.id_012__psithaca__operation_metadata__alpha__balance_updates = Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceUpdates(_io__raw_id_012__psithaca__operation_metadata__alpha__balance_updates, self, self._root)


    class LegacyFees(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id012PsithacaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id012PsithacaOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_012__psithaca__operation_metadata__alpha__balance_updates_entries.append(Id012PsithacaReceiptBalanceUpdates.Id012PsithacaOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id012PsithacaReceiptBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id012PsithacaReceiptBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id012PsithacaReceiptBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id012PsithacaReceiptBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




