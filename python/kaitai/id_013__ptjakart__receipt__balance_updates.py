# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id013PtjakartReceiptBalanceUpdates(KaitaiStruct):
    """Encoding id: 013-PtJakart.receipt.balance_updates."""

    class Id013PtjakartBondIdTag(Enum):
        tx_rollup_bond_id = 0

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class OriginTag(Enum):
        block_application = 0
        protocol_migration = 1
        subsidy = 2
        simulation = 3

    class Id013PtjakartContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id013PtjakartOperationMetadataAlphaBalanceTag(Enum):
        contract = 0
        block_fees = 2
        deposits = 4
        nonce_revelation_rewards = 5
        double_signing_evidence_rewards = 6
        endorsing_rewards = 7
        baking_rewards = 8
        baking_bonuses = 9
        storage_fees = 11
        double_signing_punishments = 12
        lost_endorsing_rewards = 13
        liquidity_baking_subsidies = 14
        burned = 15
        commitments = 16
        bootstrap = 17
        invoice = 18
        initial_commitments = 19
        minted = 20
        frozen_bonds = 21
        tx_rollup_rejection_rewards = 22
        tx_rollup_rejection_punishments = 23
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_013__ptjakart__operation_metadata__alpha__balance_updates = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Id013PtjakartOperationMetadataAlphaBalance(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__operation_metadata__alpha__balance_tag = KaitaiStream.resolve_enum(Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceTag, self._io.read_u1())
            if self.id_013__ptjakart__operation_metadata__alpha__balance_tag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceTag.contract:
                self.contract = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartContractId(self._io, self, self._root)

            if self.id_013__ptjakart__operation_metadata__alpha__balance_tag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceTag.deposits:
                self.deposits = Id013PtjakartReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_013__ptjakart__operation_metadata__alpha__balance_tag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceTag.lost_endorsing_rewards:
                self.lost_endorsing_rewards = Id013PtjakartReceiptBalanceUpdates.LostEndorsingRewards(self._io, self, self._root)

            if self.id_013__ptjakart__operation_metadata__alpha__balance_tag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceTag.commitments:
                self.commitments = self._io.read_bytes(20)

            if self.id_013__ptjakart__operation_metadata__alpha__balance_tag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceTag.frozen_bonds:
                self.frozen_bonds = Id013PtjakartReceiptBalanceUpdates.FrozenBonds(self._io, self, self._root)



    class FrozenBonds(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartContractId(self._io, self, self._root)
            self.bond_id = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartBondId(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id013PtjakartTxRollupId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup_hash = self._io.read_bytes(20)


    class LostEndorsingRewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id013PtjakartReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.participation = KaitaiStream.resolve_enum(Id013PtjakartReceiptBalanceUpdates.Bool, self._io.read_u1())
            self.revelation = KaitaiStream.resolve_enum(Id013PtjakartReceiptBalanceUpdates.Bool, self._io.read_u1())


    class Id013PtjakartOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__operation_metadata__alpha__balance = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalance(self._io, self, self._root)
            self.id_013__ptjakart__operation_metadata__alpha__balance_update = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceUpdate(self._io, self, self._root)
            self.id_013__ptjakart__operation_metadata__alpha__update_origin = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaUpdateOrigin(self._io, self, self._root)


    class Id013PtjakartContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__contract_id_tag = KaitaiStream.resolve_enum(Id013PtjakartReceiptBalanceUpdates.Id013PtjakartContractIdTag, self._io.read_u1())
            if self.id_013__ptjakart__contract_id_tag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartContractIdTag.implicit:
                self.implicit = Id013PtjakartReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_013__ptjakart__contract_id_tag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartContractIdTag.originated:
                self.originated = Id013PtjakartReceiptBalanceUpdates.Originated(self._io, self, self._root)



    class Id013PtjakartOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_013__ptjakart__operation_metadata__alpha__balance_updates_entries.append(Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class Id013PtjakartOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_013__ptjakart__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_013__ptjakart__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_013__ptjakart__operation_metadata__alpha__balance_updates, self._io, u"/types/id_013__ptjakart__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_013__ptjakart__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_013__ptjakart__operation_metadata__alpha__balance_updates)
            _io__raw_id_013__ptjakart__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_013__ptjakart__operation_metadata__alpha__balance_updates))
            self.id_013__ptjakart__operation_metadata__alpha__balance_updates = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartOperationMetadataAlphaBalanceUpdates(_io__raw_id_013__ptjakart__operation_metadata__alpha__balance_updates, self, self._root)


    class Id013PtjakartOperationMetadataAlphaUpdateOrigin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.origin = KaitaiStream.resolve_enum(Id013PtjakartReceiptBalanceUpdates.OriginTag, self._io.read_u1())


    class Id013PtjakartOperationMetadataAlphaBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Id013PtjakartBondId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__bond_id_tag = KaitaiStream.resolve_enum(Id013PtjakartReceiptBalanceUpdates.Id013PtjakartBondIdTag, self._io.read_u1())
            if self.id_013__ptjakart__bond_id_tag == Id013PtjakartReceiptBalanceUpdates.Id013PtjakartBondIdTag.tx_rollup_bond_id:
                self.tx_rollup_bond_id = Id013PtjakartReceiptBalanceUpdates.Id013PtjakartTxRollupId(self._io, self, self._root)



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id013PtjakartReceiptBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id013PtjakartReceiptBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id013PtjakartReceiptBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id013PtjakartReceiptBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




