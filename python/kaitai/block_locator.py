# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import block_header
class BlockLocator(KaitaiStruct):
    """Encoding id: block_locator
    Description: A sparse block locator à la Bitcoin."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.current_head = BlockLocator.CurrentHead0(self._io, self, self._root)
        self.history = []
        i = 0
        while not self._io.is_eof():
            self.history.append(BlockLocator.HistoryEntries(self._io, self, self._root))
            i += 1


    class CurrentHead(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.current_head = block_header.BlockHeader(self._io)


    class CurrentHead0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_current_head = self._io.read_u4be()
            if not self.len_current_head <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_current_head, self._io, u"/types/current_head_0/seq/0")
            self._raw_current_head = self._io.read_bytes(self.len_current_head)
            _io__raw_current_head = KaitaiStream(BytesIO(self._raw_current_head))
            self.current_head = BlockLocator.CurrentHead(_io__raw_current_head, self, self._root)


    class HistoryEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.block_hash = self._io.read_bytes(32)



