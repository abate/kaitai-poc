# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import operation__shell_header
from kaitai import block_header__shell
class Id008Ptedo2zkOperation(KaitaiStruct):
    """Encoding id: 008-PtEdo2Zk.operation."""

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Id008Ptedo2zkContractIdTag(Enum):
        implicit = 0
        originated = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id008Ptedo2zkInlinedEndorsementContentsTag(Enum):
        endorsement = 0

    class Id008Ptedo2zkOperationAlphaContentsTag(Enum):
        endorsement = 0
        seed_nonce_revelation = 1
        double_endorsement_evidence = 2
        double_baking_evidence = 3
        activate_account = 4
        proposals = 5
        ballot = 6
        reveal = 107
        transaction = 108
        origination = 109
        delegation = 110

    class Id008Ptedo2zkEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_008__ptedo2zk__operation = operation__shell_header.OperationShellHeader(self._io)
        self.id_008__ptedo2zk__operation__alpha__contents_and_signature = Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsAndSignature(self._io, self, self._root)

    class Op20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_0/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id008Ptedo2zkOperation.Op2(_io__raw_op2, self, self._root)


    class Id008Ptedo2zkBlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_008__ptedo2zk__block_header__alpha__signed_contents = Id008Ptedo2zkOperation.Id008Ptedo2zkBlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Id008Ptedo2zkScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id008Ptedo2zkOperation.BytesDynUint30(self._io, self, self._root)
            self.storage = Id008Ptedo2zkOperation.BytesDynUint30(self._io, self, self._root)


    class Id008Ptedo2zkInlinedEndorsementContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__inlined__endorsement__contents_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperation.Id008Ptedo2zkInlinedEndorsementContentsTag, self._io.read_u1())
            if self.id_008__ptedo2zk__inlined__endorsement__contents_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkInlinedEndorsementContentsTag.endorsement:
                self.endorsement = self._io.read_s4be()



    class ActivateAccount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = self._io.read_bytes(20)
            self.secret = self._io.read_bytes(20)


    class Id008Ptedo2zkMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__mutez = Id008Ptedo2zkOperation.N(self._io, self, self._root)


    class DoubleEndorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id008Ptedo2zkOperation.Op10(self._io, self, self._root)
            self.op2 = Id008Ptedo2zkOperation.Op20(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id008Ptedo2zkOperation.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Proposals0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_proposals = self._io.read_u4be()
            if not self.len_proposals <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_proposals, self._io, u"/types/proposals_0/seq/0")
            self._raw_proposals = self._io.read_bytes(self.len_proposals)
            _io__raw_proposals = KaitaiStream(BytesIO(self._raw_proposals))
            self.proposals = Id008Ptedo2zkOperation.Proposals(_io__raw_proposals, self, self._root)


    class Id008Ptedo2zkBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.priority = self._io.read_u2be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperation.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id008Ptedo2zkOperation.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)



    class Reveal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id008Ptedo2zkOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id008Ptedo2zkOperation.Id008Ptedo2zkMutez(self._io, self, self._root)
            self.counter = Id008Ptedo2zkOperation.N(self._io, self, self._root)
            self.gas_limit = Id008Ptedo2zkOperation.N(self._io, self, self._root)
            self.storage_limit = Id008Ptedo2zkOperation.N(self._io, self, self._root)
            self.public_key = Id008Ptedo2zkOperation.PublicKey(self._io, self, self._root)


    class SeedNonceRevelation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.nonce = self._io.read_bytes(32)


    class Id008Ptedo2zkOperationAlphaContentsAndSignature(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents = []
            i = 0
            while not self._io.is_eof():
                self.contents.append(Id008Ptedo2zkOperation.ContentsEntries(self._io, self, self._root))
                i += 1

            self.signature = self._io.read_bytes(64)


    class DoubleBakingEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bh1 = Id008Ptedo2zkOperation.Bh10(self._io, self, self._root)
            self.bh2 = Id008Ptedo2zkOperation.Bh20(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperation.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id008Ptedo2zkOperation.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id008Ptedo2zkOperation.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id008Ptedo2zkOperation.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Bh2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__block_header__alpha__full_header = Id008Ptedo2zkOperation.Id008Ptedo2zkBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Bh20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh2 = self._io.read_u4be()
            if not self.len_bh2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh2, self._io, u"/types/bh2_0/seq/0")
            self._raw_bh2 = self._io.read_bytes(self.len_bh2)
            _io__raw_bh2 = KaitaiStream(BytesIO(self._raw_bh2))
            self.bh2 = Id008Ptedo2zkOperation.Bh2(_io__raw_bh2, self, self._root)


    class Id008Ptedo2zkEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__entrypoint_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperation.Id008Ptedo2zkEntrypointTag, self._io.read_u1())
            if self.id_008__ptedo2zk__entrypoint_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkEntrypointTag.named:
                self.named = Id008Ptedo2zkOperation.Named0(self._io, self, self._root)



    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id008Ptedo2zkOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id008Ptedo2zkOperation.Id008Ptedo2zkMutez(self._io, self, self._root)
            self.counter = Id008Ptedo2zkOperation.N(self._io, self, self._root)
            self.gas_limit = Id008Ptedo2zkOperation.N(self._io, self, self._root)
            self.storage_limit = Id008Ptedo2zkOperation.N(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperation.Bool, self._io.read_u1())
            if self.delegate_tag == Id008Ptedo2zkOperation.Bool.true:
                self.delegate = Id008Ptedo2zkOperation.PublicKeyHash(self._io, self, self._root)



    class Id008Ptedo2zkInlinedEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__inlined__endorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id008Ptedo2zkOperation.Id008Ptedo2zkInlinedEndorsementContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperation.Bool, self._io.read_u1())
            if self.signature_tag == Id008Ptedo2zkOperation.Bool.true:
                self.signature = self._io.read_bytes(64)



    class ContentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__operation__alpha__contents = Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContents(self._io, self, self._root)


    class Bh10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh1 = self._io.read_u4be()
            if not self.len_bh1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh1, self._io, u"/types/bh1_0/seq/0")
            self._raw_bh1 = self._io.read_bytes(self.len_bh1)
            _io__raw_bh1 = KaitaiStream(BytesIO(self._raw_bh1))
            self.bh1 = Id008Ptedo2zkOperation.Bh1(_io__raw_bh1, self, self._root)


    class Id008Ptedo2zkContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__contract_id_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperation.Id008Ptedo2zkContractIdTag, self._io.read_u1())
            if self.id_008__ptedo2zk__contract_id_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkContractIdTag.implicit:
                self.implicit = Id008Ptedo2zkOperation.PublicKeyHash(self._io, self, self._root)

            if self.id_008__ptedo2zk__contract_id_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkContractIdTag.originated:
                self.originated = Id008Ptedo2zkOperation.Originated(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Bh1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__block_header__alpha__full_header = Id008Ptedo2zkOperation.Id008Ptedo2zkBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Op10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_0/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id008Ptedo2zkOperation.Op1(_io__raw_op1, self, self._root)


    class Id008Ptedo2zkBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__block_header__alpha__unsigned_contents = Id008Ptedo2zkOperation.Id008Ptedo2zkBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes(64)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id008Ptedo2zkOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id008Ptedo2zkOperation.Id008Ptedo2zkMutez(self._io, self, self._root)
            self.counter = Id008Ptedo2zkOperation.N(self._io, self, self._root)
            self.gas_limit = Id008Ptedo2zkOperation.N(self._io, self, self._root)
            self.storage_limit = Id008Ptedo2zkOperation.N(self._io, self, self._root)
            self.balance = Id008Ptedo2zkOperation.Id008Ptedo2zkMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperation.Bool, self._io.read_u1())
            if self.delegate_tag == Id008Ptedo2zkOperation.Bool.true:
                self.delegate = Id008Ptedo2zkOperation.PublicKeyHash(self._io, self, self._root)

            self.script = Id008Ptedo2zkOperation.Id008Ptedo2zkScriptedContracts(self._io, self, self._root)


    class Id008Ptedo2zkOperationAlphaContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__operation__alpha__contents_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsTag, self._io.read_u1())
            if self.id_008__ptedo2zk__operation__alpha__contents_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsTag.endorsement:
                self.endorsement = self._io.read_s4be()

            if self.id_008__ptedo2zk__operation__alpha__contents_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsTag.seed_nonce_revelation:
                self.seed_nonce_revelation = Id008Ptedo2zkOperation.SeedNonceRevelation(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation__alpha__contents_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsTag.double_endorsement_evidence:
                self.double_endorsement_evidence = Id008Ptedo2zkOperation.DoubleEndorsementEvidence(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation__alpha__contents_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsTag.double_baking_evidence:
                self.double_baking_evidence = Id008Ptedo2zkOperation.DoubleBakingEvidence(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation__alpha__contents_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsTag.activate_account:
                self.activate_account = Id008Ptedo2zkOperation.ActivateAccount(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation__alpha__contents_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsTag.proposals:
                self.proposals = Id008Ptedo2zkOperation.Proposals1(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation__alpha__contents_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsTag.ballot:
                self.ballot = Id008Ptedo2zkOperation.Ballot(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation__alpha__contents_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsTag.reveal:
                self.reveal = Id008Ptedo2zkOperation.Reveal(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation__alpha__contents_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsTag.transaction:
                self.transaction = Id008Ptedo2zkOperation.Transaction(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation__alpha__contents_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsTag.origination:
                self.origination = Id008Ptedo2zkOperation.Origination(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation__alpha__contents_tag == Id008Ptedo2zkOperation.Id008Ptedo2zkOperationAlphaContentsTag.delegation:
                self.delegation = Id008Ptedo2zkOperation.Delegation(self._io, self, self._root)



    class Op2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__inlined__endorsement = Id008Ptedo2zkOperation.Id008Ptedo2zkInlinedEndorsement(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id008Ptedo2zkOperation.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id008Ptedo2zkOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id008Ptedo2zkOperation.Id008Ptedo2zkMutez(self._io, self, self._root)
            self.counter = Id008Ptedo2zkOperation.N(self._io, self, self._root)
            self.gas_limit = Id008Ptedo2zkOperation.N(self._io, self, self._root)
            self.storage_limit = Id008Ptedo2zkOperation.N(self._io, self, self._root)
            self.amount = Id008Ptedo2zkOperation.Id008Ptedo2zkMutez(self._io, self, self._root)
            self.destination = Id008Ptedo2zkOperation.Id008Ptedo2zkContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperation.Bool, self._io.read_u1())
            if self.parameters_tag == Id008Ptedo2zkOperation.Bool.true:
                self.parameters = Id008Ptedo2zkOperation.Parameters(self._io, self, self._root)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id008Ptedo2zkOperation.Id008Ptedo2zkEntrypoint(self._io, self, self._root)
            self.value = Id008Ptedo2zkOperation.BytesDynUint30(self._io, self, self._root)


    class Proposals1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id008Ptedo2zkOperation.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposals = Id008Ptedo2zkOperation.Proposals0(self._io, self, self._root)


    class Ballot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id008Ptedo2zkOperation.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposal = self._io.read_bytes(32)
            self.ballot = self._io.read_s1()


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperation.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id008Ptedo2zkOperation.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id008Ptedo2zkOperation.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id008Ptedo2zkOperation.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Proposals(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proposals_entries = []
            i = 0
            while not self._io.is_eof():
                self.proposals_entries.append(Id008Ptedo2zkOperation.ProposalsEntries(self._io, self, self._root))
                i += 1



    class ProposalsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol_hash = self._io.read_bytes(32)


    class Op1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__inlined__endorsement = Id008Ptedo2zkOperation.Id008Ptedo2zkInlinedEndorsement(self._io, self, self._root)



