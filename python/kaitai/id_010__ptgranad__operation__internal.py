# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id010PtgranadOperationInternal(KaitaiStruct):
    """Encoding id: 010-PtGRANAD.operation.internal."""

    class Id010PtgranadContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id010PtgranadOperationAlphaInternalOperationTag(Enum):
        reveal = 0
        transaction = 1
        origination = 2
        delegation = 3

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id010PtgranadEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_010__ptgranad__operation__alpha__internal_operation = Id010PtgranadOperationInternal.Id010PtgranadOperationAlphaInternalOperation(self._io, self, self._root)

    class Id010PtgranadEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__entrypoint_tag = KaitaiStream.resolve_enum(Id010PtgranadOperationInternal.Id010PtgranadEntrypointTag, self._io.read_u1())
            if self.id_010__ptgranad__entrypoint_tag == Id010PtgranadOperationInternal.Id010PtgranadEntrypointTag.named:
                self.named = Id010PtgranadOperationInternal.Named0(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id010PtgranadOperationInternal.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id010PtgranadScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id010PtgranadOperationInternal.BytesDynUint30(self._io, self, self._root)
            self.storage = Id010PtgranadOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Id010PtgranadContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__contract_id_tag = KaitaiStream.resolve_enum(Id010PtgranadOperationInternal.Id010PtgranadContractIdTag, self._io.read_u1())
            if self.id_010__ptgranad__contract_id_tag == Id010PtgranadOperationInternal.Id010PtgranadContractIdTag.implicit:
                self.implicit = Id010PtgranadOperationInternal.PublicKeyHash(self._io, self, self._root)

            if self.id_010__ptgranad__contract_id_tag == Id010PtgranadOperationInternal.Id010PtgranadContractIdTag.originated:
                self.originated = Id010PtgranadOperationInternal.Originated(self._io, self, self._root)



    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id010PtgranadOperationInternal.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id010PtgranadOperationInternal.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id010PtgranadOperationInternal.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id010PtgranadOperationInternal.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id010PtgranadOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id010PtgranadOperationInternal.Bool.true:
                self.delegate = Id010PtgranadOperationInternal.PublicKeyHash(self._io, self, self._root)



    class Id010PtgranadOperationAlphaInternalOperation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id010PtgranadOperationInternal.Id010PtgranadContractId(self._io, self, self._root)
            self.nonce = self._io.read_u2be()
            self.id_010__ptgranad__operation__alpha__internal_operation_tag = KaitaiStream.resolve_enum(Id010PtgranadOperationInternal.Id010PtgranadOperationAlphaInternalOperationTag, self._io.read_u1())
            if self.id_010__ptgranad__operation__alpha__internal_operation_tag == Id010PtgranadOperationInternal.Id010PtgranadOperationAlphaInternalOperationTag.reveal:
                self.reveal = Id010PtgranadOperationInternal.PublicKey(self._io, self, self._root)

            if self.id_010__ptgranad__operation__alpha__internal_operation_tag == Id010PtgranadOperationInternal.Id010PtgranadOperationAlphaInternalOperationTag.transaction:
                self.transaction = Id010PtgranadOperationInternal.Transaction(self._io, self, self._root)

            if self.id_010__ptgranad__operation__alpha__internal_operation_tag == Id010PtgranadOperationInternal.Id010PtgranadOperationAlphaInternalOperationTag.origination:
                self.origination = Id010PtgranadOperationInternal.Origination(self._io, self, self._root)

            if self.id_010__ptgranad__operation__alpha__internal_operation_tag == Id010PtgranadOperationInternal.Id010PtgranadOperationAlphaInternalOperationTag.delegation:
                self.delegation = Id010PtgranadOperationInternal.Delegation(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.balance = Id010PtgranadOperationInternal.Id010PtgranadMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id010PtgranadOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id010PtgranadOperationInternal.Bool.true:
                self.delegate = Id010PtgranadOperationInternal.PublicKeyHash(self._io, self, self._root)

            self.script = Id010PtgranadOperationInternal.Id010PtgranadScriptedContracts(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id010PtgranadOperationInternal.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount = Id010PtgranadOperationInternal.Id010PtgranadMutez(self._io, self, self._root)
            self.destination = Id010PtgranadOperationInternal.Id010PtgranadContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id010PtgranadOperationInternal.Bool, self._io.read_u1())
            if self.parameters_tag == Id010PtgranadOperationInternal.Bool.true:
                self.parameters = Id010PtgranadOperationInternal.Parameters(self._io, self, self._root)



    class Id010PtgranadMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__mutez = Id010PtgranadOperationInternal.N(self._io, self, self._root)


    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id010PtgranadOperationInternal.Id010PtgranadEntrypoint(self._io, self, self._root)
            self.value = Id010PtgranadOperationInternal.BytesDynUint30(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id010PtgranadOperationInternal.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id010PtgranadOperationInternal.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id010PtgranadOperationInternal.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id010PtgranadOperationInternal.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




