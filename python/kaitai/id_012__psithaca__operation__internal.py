# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id012PsithacaOperationInternal(KaitaiStruct):
    """Encoding id: 012-Psithaca.operation.internal."""

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Id012PsithacaEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255

    class Id012PsithacaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Id012PsithacaOperationAlphaInternalOperationTag(Enum):
        reveal = 0
        transaction = 1
        origination = 2
        delegation = 3
        register_global_constant = 4
        set_deposits_limit = 5

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_012__psithaca__operation__alpha__internal_operation = Id012PsithacaOperationInternal.Id012PsithacaOperationAlphaInternalOperation(self._io, self, self._root)

    class Id012PsithacaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__contract_id_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationInternal.Id012PsithacaContractIdTag, self._io.read_u1())
            if self.id_012__psithaca__contract_id_tag == Id012PsithacaOperationInternal.Id012PsithacaContractIdTag.implicit:
                self.implicit = Id012PsithacaOperationInternal.PublicKeyHash(self._io, self, self._root)

            if self.id_012__psithaca__contract_id_tag == Id012PsithacaOperationInternal.Id012PsithacaContractIdTag.originated:
                self.originated = Id012PsithacaOperationInternal.Originated(self._io, self, self._root)



    class Id012PsithacaScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id012PsithacaOperationInternal.BytesDynUint30(self._io, self, self._root)
            self.storage = Id012PsithacaOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Id012PsithacaEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__entrypoint_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationInternal.Id012PsithacaEntrypointTag, self._io.read_u1())
            if self.id_012__psithaca__entrypoint_tag == Id012PsithacaOperationInternal.Id012PsithacaEntrypointTag.named:
                self.named = Id012PsithacaOperationInternal.Named0(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id012PsithacaOperationInternal.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id012PsithacaOperationAlphaInternalOperation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id012PsithacaOperationInternal.Id012PsithacaContractId(self._io, self, self._root)
            self.nonce = self._io.read_u2be()
            self.id_012__psithaca__operation__alpha__internal_operation_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationInternal.Id012PsithacaOperationAlphaInternalOperationTag, self._io.read_u1())
            if self.id_012__psithaca__operation__alpha__internal_operation_tag == Id012PsithacaOperationInternal.Id012PsithacaOperationAlphaInternalOperationTag.reveal:
                self.reveal = Id012PsithacaOperationInternal.PublicKey(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__internal_operation_tag == Id012PsithacaOperationInternal.Id012PsithacaOperationAlphaInternalOperationTag.transaction:
                self.transaction = Id012PsithacaOperationInternal.Transaction(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__internal_operation_tag == Id012PsithacaOperationInternal.Id012PsithacaOperationAlphaInternalOperationTag.origination:
                self.origination = Id012PsithacaOperationInternal.Origination(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__internal_operation_tag == Id012PsithacaOperationInternal.Id012PsithacaOperationAlphaInternalOperationTag.delegation:
                self.delegation = Id012PsithacaOperationInternal.Delegation(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__internal_operation_tag == Id012PsithacaOperationInternal.Id012PsithacaOperationAlphaInternalOperationTag.register_global_constant:
                self.register_global_constant = Id012PsithacaOperationInternal.BytesDynUint30(self._io, self, self._root)

            if self.id_012__psithaca__operation__alpha__internal_operation_tag == Id012PsithacaOperationInternal.Id012PsithacaOperationAlphaInternalOperationTag.set_deposits_limit:
                self.set_deposits_limit = Id012PsithacaOperationInternal.SetDepositsLimit(self._io, self, self._root)



    class Id012PsithacaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__mutez = Id012PsithacaOperationInternal.N(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationInternal.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id012PsithacaOperationInternal.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id012PsithacaOperationInternal.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id012PsithacaOperationInternal.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id012PsithacaOperationInternal.Bool.true:
                self.delegate = Id012PsithacaOperationInternal.PublicKeyHash(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class SetDepositsLimit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.limit_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationInternal.Bool, self._io.read_u1())
            if self.limit_tag == Id012PsithacaOperationInternal.Bool.true:
                self.limit = Id012PsithacaOperationInternal.Id012PsithacaMutez(self._io, self, self._root)



    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.balance = Id012PsithacaOperationInternal.Id012PsithacaMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id012PsithacaOperationInternal.Bool.true:
                self.delegate = Id012PsithacaOperationInternal.PublicKeyHash(self._io, self, self._root)

            self.script = Id012PsithacaOperationInternal.Id012PsithacaScriptedContracts(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id012PsithacaOperationInternal.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount = Id012PsithacaOperationInternal.Id012PsithacaMutez(self._io, self, self._root)
            self.destination = Id012PsithacaOperationInternal.Id012PsithacaContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationInternal.Bool, self._io.read_u1())
            if self.parameters_tag == Id012PsithacaOperationInternal.Bool.true:
                self.parameters = Id012PsithacaOperationInternal.Parameters(self._io, self, self._root)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id012PsithacaOperationInternal.Id012PsithacaEntrypoint(self._io, self, self._root)
            self.value = Id012PsithacaOperationInternal.BytesDynUint30(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id012PsithacaOperationInternal.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id012PsithacaOperationInternal.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id012PsithacaOperationInternal.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id012PsithacaOperationInternal.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




