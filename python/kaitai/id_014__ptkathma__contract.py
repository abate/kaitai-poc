# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id014PtkathmaContract(KaitaiStruct):
    """Encoding id: 014-PtKathma.contract."""

    class Id014PtkathmaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_014__ptkathma__contract_id = Id014PtkathmaContract.Id014PtkathmaContractId(self._io, self, self._root)

    class Id014PtkathmaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__contract_id_tag = KaitaiStream.resolve_enum(Id014PtkathmaContract.Id014PtkathmaContractIdTag, self._io.read_u1())
            if self.id_014__ptkathma__contract_id_tag == Id014PtkathmaContract.Id014PtkathmaContractIdTag.implicit:
                self.implicit = Id014PtkathmaContract.PublicKeyHash(self._io, self, self._root)

            if self.id_014__ptkathma__contract_id_tag == Id014PtkathmaContract.Id014PtkathmaContractIdTag.originated:
                self.originated = Id014PtkathmaContract.Originated(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id014PtkathmaContract.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id014PtkathmaContract.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id014PtkathmaContract.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id014PtkathmaContract.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




