# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import timestamp__system
class P2pPointPoolEvent(KaitaiStruct):
    """Encoding id: p2p_point.pool_event
    Description: Events happening during maintenance of and operations on a peer point pool (such as connections, disconnections, connection requests)."""

    class Bool(Enum):
        false = 0
        true = 255

    class P2pPointPoolEventField1Tag(Enum):
        outgoing_request = 0
        accepting_request = 1
        rejecting_request = 2
        rejecting_rejected = 3
        connection_established = 4
        disconnection = 5
        external_disconnection = 6
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.p2p_point__pool_event_field0 = timestamp__system.TimestampSystem(self._io)
        self.p2p_point__pool_event_field1 = P2pPointPoolEvent.P2pPointPoolEventField1(self._io, self, self._root)

    class P2pPointPoolEventField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.p2p_point__pool_event_field1_tag = KaitaiStream.resolve_enum(P2pPointPoolEvent.P2pPointPoolEventField1Tag, self._io.read_u1())
            if self.p2p_point__pool_event_field1_tag == P2pPointPoolEvent.P2pPointPoolEventField1Tag.accepting_request:
                self.accepting_request = self._io.read_bytes(16)

            if self.p2p_point__pool_event_field1_tag == P2pPointPoolEvent.P2pPointPoolEventField1Tag.rejecting_request:
                self.rejecting_request = self._io.read_bytes(16)

            if self.p2p_point__pool_event_field1_tag == P2pPointPoolEvent.P2pPointPoolEventField1Tag.rejecting_rejected:
                self.rejecting_rejected = P2pPointPoolEvent.RejectingRejected(self._io, self, self._root)

            if self.p2p_point__pool_event_field1_tag == P2pPointPoolEvent.P2pPointPoolEventField1Tag.connection_established:
                self.connection_established = self._io.read_bytes(16)

            if self.p2p_point__pool_event_field1_tag == P2pPointPoolEvent.P2pPointPoolEventField1Tag.disconnection:
                self.disconnection = self._io.read_bytes(16)

            if self.p2p_point__pool_event_field1_tag == P2pPointPoolEvent.P2pPointPoolEventField1Tag.external_disconnection:
                self.external_disconnection = self._io.read_bytes(16)



    class RejectingRejected(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.p2p_peer_id_tag = KaitaiStream.resolve_enum(P2pPointPoolEvent.Bool, self._io.read_u1())
            if self.p2p_peer_id_tag == P2pPointPoolEvent.Bool.true:
                self.p2p_peer_id = self._io.read_bytes(16)




