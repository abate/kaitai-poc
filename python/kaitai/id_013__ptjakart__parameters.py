# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id013PtjakartParameters(KaitaiStruct):
    """Encoding id: 013-PtJakart.parameters."""

    class Bool(Enum):
        false = 0
        true = 255

    class BootstrapAccountsEltTag(Enum):
        public_key_known = 0
        public_key_unknown = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.bootstrap_accounts = Id013PtjakartParameters.BootstrapAccounts0(self._io, self, self._root)
        self.bootstrap_contracts = Id013PtjakartParameters.BootstrapContracts0(self._io, self, self._root)
        self.commitments = Id013PtjakartParameters.Commitments0(self._io, self, self._root)
        self.security_deposit_ramp_up_cycles_tag = KaitaiStream.resolve_enum(Id013PtjakartParameters.Bool, self._io.read_u1())
        if self.security_deposit_ramp_up_cycles_tag == Id013PtjakartParameters.Bool.true:
            self.security_deposit_ramp_up_cycles = Id013PtjakartParameters.Int31(self._io, self, self._root)

        self.no_reward_cycles_tag = KaitaiStream.resolve_enum(Id013PtjakartParameters.Bool, self._io.read_u1())
        if self.no_reward_cycles_tag == Id013PtjakartParameters.Bool.true:
            self.no_reward_cycles = Id013PtjakartParameters.Int31(self._io, self, self._root)

        self.preserved_cycles = self._io.read_u1()
        self.blocks_per_cycle = self._io.read_s4be()
        self.blocks_per_commitment = self._io.read_s4be()
        self.blocks_per_stake_snapshot = self._io.read_s4be()
        self.cycles_per_voting_period = self._io.read_s4be()
        self.hard_gas_limit_per_operation = Id013PtjakartParameters.Z(self._io, self, self._root)
        self.hard_gas_limit_per_block = Id013PtjakartParameters.Z(self._io, self, self._root)
        self.proof_of_work_threshold = self._io.read_s8be()
        self.tokens_per_roll = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)
        self.seed_nonce_revelation_tip = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)
        self.origination_size = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.baking_reward_fixed_portion = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)
        self.baking_reward_bonus_per_slot = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)
        self.endorsing_reward_per_slot = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)
        self.cost_per_byte = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)
        self.hard_storage_limit_per_operation = Id013PtjakartParameters.Z(self._io, self, self._root)
        self.quorum_min = self._io.read_s4be()
        self.quorum_max = self._io.read_s4be()
        self.min_proposal_quorum = self._io.read_s4be()
        self.liquidity_baking_subsidy = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)
        self.liquidity_baking_sunset_level = self._io.read_s4be()
        self.liquidity_baking_toggle_ema_threshold = self._io.read_s4be()
        self.max_operations_time_to_live = self._io.read_s2be()
        self.minimal_block_delay = self._io.read_s8be()
        self.delay_increment_per_round = self._io.read_s8be()
        self.consensus_committee_size = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.consensus_threshold = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.minimal_participation_ratio = Id013PtjakartParameters.MinimalParticipationRatio(self._io, self, self._root)
        self.max_slashing_period = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.frozen_deposits_percentage = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.double_baking_punishment = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)
        self.ratio_of_frozen_deposits_slashed_per_double_endorsement = Id013PtjakartParameters.RatioOfFrozenDepositsSlashedPerDoubleEndorsement(self._io, self, self._root)
        self.initial_seed_tag = KaitaiStream.resolve_enum(Id013PtjakartParameters.Bool, self._io.read_u1())
        if self.initial_seed_tag == Id013PtjakartParameters.Bool.true:
            self.initial_seed = self._io.read_bytes(32)

        self.cache_script_size = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.cache_stake_distribution_cycles = self._io.read_s1()
        self.cache_sampler_state_cycles = self._io.read_s1()
        self.tx_rollup_enable = KaitaiStream.resolve_enum(Id013PtjakartParameters.Bool, self._io.read_u1())
        self.tx_rollup_origination_size = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.tx_rollup_hard_size_limit_per_inbox = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.tx_rollup_hard_size_limit_per_message = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_withdrawals_per_batch = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.tx_rollup_commitment_bond = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)
        self.tx_rollup_finality_period = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.tx_rollup_withdraw_period = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_inboxes_count = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_messages_per_inbox = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_commitments_count = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.tx_rollup_cost_per_byte_ema_factor = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_ticket_payload_size = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.tx_rollup_rejection_max_proof_size = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.tx_rollup_sunset_level = self._io.read_s4be()
        self.sc_rollup_enable = KaitaiStream.resolve_enum(Id013PtjakartParameters.Bool, self._io.read_u1())
        self.sc_rollup_origination_size = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.sc_rollup_challenge_window_in_blocks = Id013PtjakartParameters.Int31(self._io, self, self._root)
        self.sc_rollup_max_available_messages = Id013PtjakartParameters.Int31(self._io, self, self._root)

    class RatioOfFrozenDepositsSlashedPerDoubleEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class PublicKeyKnown(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_known_field0 = Id013PtjakartParameters.PublicKey(self._io, self, self._root)
            self.public_key_known_field1 = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)


    class Commitments(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_entries = []
            i = 0
            while not self._io.is_eof():
                self.commitments_entries.append(Id013PtjakartParameters.CommitmentsEntries(self._io, self, self._root))
                i += 1



    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id013PtjakartParameters.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class CommitmentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_elt_field0 = self._io.read_bytes(20)
            self.commitments_elt_field1 = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)


    class Commitments0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_commitments = self._io.read_u4be()
            if not self.len_commitments <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_commitments, self._io, u"/types/commitments_0/seq/0")
            self._raw_commitments = self._io.read_bytes(self.len_commitments)
            _io__raw_commitments = KaitaiStream(BytesIO(self._raw_commitments))
            self.commitments = Id013PtjakartParameters.Commitments(_io__raw_commitments, self, self._root)


    class MinimalParticipationRatio(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class BootstrapAccounts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_accounts = self._io.read_u4be()
            if not self.len_bootstrap_accounts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_accounts, self._io, u"/types/bootstrap_accounts_0/seq/0")
            self._raw_bootstrap_accounts = self._io.read_bytes(self.len_bootstrap_accounts)
            _io__raw_bootstrap_accounts = KaitaiStream(BytesIO(self._raw_bootstrap_accounts))
            self.bootstrap_accounts = Id013PtjakartParameters.BootstrapAccounts(_io__raw_bootstrap_accounts, self, self._root)


    class Id013PtjakartScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id013PtjakartParameters.BytesDynUint30(self._io, self, self._root)
            self.storage = Id013PtjakartParameters.BytesDynUint30(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id013PtjakartParameters.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id013PtjakartParameters.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id013PtjakartParameters.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id013PtjakartParameters.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class PublicKeyUnknown(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_unknown_field0 = Id013PtjakartParameters.PublicKeyHash(self._io, self, self._root)
            self.public_key_unknown_field1 = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)


    class BootstrapAccountsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_accounts_elt_tag = KaitaiStream.resolve_enum(Id013PtjakartParameters.BootstrapAccountsEltTag, self._io.read_u1())
            if self.bootstrap_accounts_elt_tag == Id013PtjakartParameters.BootstrapAccountsEltTag.public_key_known:
                self.public_key_known = Id013PtjakartParameters.PublicKeyKnown(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == Id013PtjakartParameters.BootstrapAccountsEltTag.public_key_unknown:
                self.public_key_unknown = Id013PtjakartParameters.PublicKeyUnknown(self._io, self, self._root)



    class BootstrapContracts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_contracts = self._io.read_u4be()
            if not self.len_bootstrap_contracts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_contracts, self._io, u"/types/bootstrap_contracts_0/seq/0")
            self._raw_bootstrap_contracts = self._io.read_bytes(self.len_bootstrap_contracts)
            _io__raw_bootstrap_contracts = KaitaiStream(BytesIO(self._raw_bootstrap_contracts))
            self.bootstrap_contracts = Id013PtjakartParameters.BootstrapContracts(_io__raw_bootstrap_contracts, self, self._root)


    class Id013PtjakartMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__mutez = Id013PtjakartParameters.N(self._io, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class BootstrapContractsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id013PtjakartParameters.Bool, self._io.read_u1())
            if self.delegate_tag == Id013PtjakartParameters.Bool.true:
                self.delegate = Id013PtjakartParameters.PublicKeyHash(self._io, self, self._root)

            self.amount = Id013PtjakartParameters.Id013PtjakartMutez(self._io, self, self._root)
            self.script = Id013PtjakartParameters.Id013PtjakartScriptedContracts(self._io, self, self._root)


    class BootstrapContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_contracts_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_contracts_entries.append(Id013PtjakartParameters.BootstrapContractsEntries(self._io, self, self._root))
                i += 1



    class BootstrapAccounts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_accounts_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_accounts_entries.append(Id013PtjakartParameters.BootstrapAccountsEntries(self._io, self, self._root))
                i += 1



    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id013PtjakartParameters.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id013PtjakartParameters.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id013PtjakartParameters.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id013PtjakartParameters.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id013PtjakartParameters.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




