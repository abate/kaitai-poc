# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import p2p_point__id
from kaitai import p2p_connection__id
class P2pConnectionPoolEvent(KaitaiStruct):
    """Encoding id: p2p_connection.pool_event
    Description: An event that may happen during maintenance of and other operations on the p2p connection pool. Typically, it includes connection errors, peer swaps, etc."""

    class Bool(Enum):
        false = 0
        true = 255

    class P2pConnectionPoolEventTag(Enum):
        too_few_connections = 0
        too_many_connections = 1
        new_point = 2
        new_peer = 3
        incoming_connection = 4
        outgoing_connection = 5
        authentication_failed = 6
        accepting_request = 7
        rejecting_request = 8
        request_rejected = 9
        connection_established = 10
        disconnection = 11
        external_disconnection = 12
        gc_points = 13
        gc_peer_ids = 14
        swap_request_received = 15
        swap_ack_received = 16
        swap_request_sent = 17
        swap_ack_sent = 18
        swap_request_ignored = 19
        swap_success = 20
        swap_failure = 21
        bootstrap_sent = 22
        bootstrap_received = 23
        advertise_sent = 24
        advertise_received = 25
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.p2p_connection__pool_event_tag = KaitaiStream.resolve_enum(P2pConnectionPoolEvent.P2pConnectionPoolEventTag, self._io.read_u1())
        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.new_point:
            self.new_point = p2p_point__id.P2pPointId(self._io)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.new_peer:
            self.new_peer = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.incoming_connection:
            self.incoming_connection = p2p_point__id.P2pPointId(self._io)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.outgoing_connection:
            self.outgoing_connection = p2p_point__id.P2pPointId(self._io)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.authentication_failed:
            self.authentication_failed = p2p_point__id.P2pPointId(self._io)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.accepting_request:
            self.accepting_request = P2pConnectionPoolEvent.AcceptingRequest(self._io, self, self._root)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.rejecting_request:
            self.rejecting_request = P2pConnectionPoolEvent.RejectingRequest(self._io, self, self._root)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.request_rejected:
            self.request_rejected = P2pConnectionPoolEvent.RequestRejected(self._io, self, self._root)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.connection_established:
            self.connection_established = P2pConnectionPoolEvent.ConnectionEstablished(self._io, self, self._root)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.disconnection:
            self.disconnection = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.external_disconnection:
            self.external_disconnection = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.swap_request_received:
            self.swap_request_received = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.swap_ack_received:
            self.swap_ack_received = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.swap_request_sent:
            self.swap_request_sent = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.swap_ack_sent:
            self.swap_ack_sent = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.swap_request_ignored:
            self.swap_request_ignored = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.swap_success:
            self.swap_success = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.swap_failure:
            self.swap_failure = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.bootstrap_sent:
            self.bootstrap_sent = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.bootstrap_received:
            self.bootstrap_received = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.advertise_sent:
            self.advertise_sent = self._io.read_bytes(16)

        if self.p2p_connection__pool_event_tag == P2pConnectionPoolEvent.P2pConnectionPoolEventTag.advertise_received:
            self.advertise_received = self._io.read_bytes(16)


    class RequestRejected(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.point = p2p_point__id.P2pPointId(self._io)
            self.identity_tag = KaitaiStream.resolve_enum(P2pConnectionPoolEvent.Bool, self._io.read_u1())
            if self.identity_tag == P2pConnectionPoolEvent.Bool.true:
                self.identity = P2pConnectionPoolEvent.Identity(self._io, self, self._root)



    class RejectingRequest(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.point = p2p_point__id.P2pPointId(self._io)
            self.id_point = p2p_connection__id.P2pConnectionId(self._io)
            self.peer_id = self._io.read_bytes(16)


    class ConnectionEstablished(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_point = p2p_connection__id.P2pConnectionId(self._io)
            self.peer_id = self._io.read_bytes(16)


    class AcceptingRequest(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.point = p2p_point__id.P2pPointId(self._io)
            self.id_point = p2p_connection__id.P2pConnectionId(self._io)
            self.peer_id = self._io.read_bytes(16)


    class Identity(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.identity_field0 = p2p_connection__id.P2pConnectionId(self._io)
            self.identity_field1 = self._io.read_bytes(16)



