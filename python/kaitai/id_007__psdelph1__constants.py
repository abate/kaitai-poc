# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id007Psdelph1Constants(KaitaiStruct):
    """Encoding id: 007-PsDELPH1.constants."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.proof_of_work_nonce_size = self._io.read_u1()
        self.nonce_length = self._io.read_u1()
        self.max_anon_ops_per_block = self._io.read_u1()
        self.max_operation_data_length = Id007Psdelph1Constants.Int31(self._io, self, self._root)
        self.max_proposals_per_delegate = self._io.read_u1()
        self.preserved_cycles = self._io.read_u1()
        self.blocks_per_cycle = self._io.read_s4be()
        self.blocks_per_commitment = self._io.read_s4be()
        self.blocks_per_roll_snapshot = self._io.read_s4be()
        self.blocks_per_voting_period = self._io.read_s4be()
        self.time_between_blocks = Id007Psdelph1Constants.TimeBetweenBlocks0(self._io, self, self._root)
        self.endorsers_per_block = self._io.read_u2be()
        self.hard_gas_limit_per_operation = Id007Psdelph1Constants.Z(self._io, self, self._root)
        self.hard_gas_limit_per_block = Id007Psdelph1Constants.Z(self._io, self, self._root)
        self.proof_of_work_threshold = self._io.read_s8be()
        self.tokens_per_roll = Id007Psdelph1Constants.Id007Psdelph1Mutez(self._io, self, self._root)
        self.michelson_maximum_type_size = self._io.read_u2be()
        self.seed_nonce_revelation_tip = Id007Psdelph1Constants.Id007Psdelph1Mutez(self._io, self, self._root)
        self.origination_size = Id007Psdelph1Constants.Int31(self._io, self, self._root)
        self.block_security_deposit = Id007Psdelph1Constants.Id007Psdelph1Mutez(self._io, self, self._root)
        self.endorsement_security_deposit = Id007Psdelph1Constants.Id007Psdelph1Mutez(self._io, self, self._root)
        self.baking_reward_per_endorsement = Id007Psdelph1Constants.BakingRewardPerEndorsement0(self._io, self, self._root)
        self.endorsement_reward = Id007Psdelph1Constants.EndorsementReward0(self._io, self, self._root)
        self.cost_per_byte = Id007Psdelph1Constants.Id007Psdelph1Mutez(self._io, self, self._root)
        self.hard_storage_limit_per_operation = Id007Psdelph1Constants.Z(self._io, self, self._root)
        self.test_chain_duration = self._io.read_s8be()
        self.quorum_min = self._io.read_s4be()
        self.quorum_max = self._io.read_s4be()
        self.min_proposal_quorum = self._io.read_s4be()
        self.initial_endorsers = self._io.read_u2be()
        self.delay_per_missing_endorsement = self._io.read_s8be()

    class TimeBetweenBlocksEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.time_between_blocks_elt = self._io.read_s8be()


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id007Psdelph1Constants.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class TimeBetweenBlocks(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.time_between_blocks_entries = []
            i = 0
            while not self._io.is_eof():
                self.time_between_blocks_entries.append(Id007Psdelph1Constants.TimeBetweenBlocksEntries(self._io, self, self._root))
                i += 1



    class EndorsementReward(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.endorsement_reward_entries = []
            i = 0
            while not self._io.is_eof():
                self.endorsement_reward_entries.append(Id007Psdelph1Constants.EndorsementRewardEntries(self._io, self, self._root))
                i += 1



    class Id007Psdelph1Mutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_007__psdelph1__mutez = Id007Psdelph1Constants.N(self._io, self, self._root)


    class EndorsementReward0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_endorsement_reward = self._io.read_u4be()
            if not self.len_endorsement_reward <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_endorsement_reward, self._io, u"/types/endorsement_reward_0/seq/0")
            self._raw_endorsement_reward = self._io.read_bytes(self.len_endorsement_reward)
            _io__raw_endorsement_reward = KaitaiStream(BytesIO(self._raw_endorsement_reward))
            self.endorsement_reward = Id007Psdelph1Constants.EndorsementReward(_io__raw_endorsement_reward, self, self._root)


    class EndorsementRewardEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_007__psdelph1__mutez = Id007Psdelph1Constants.Id007Psdelph1Mutez(self._io, self, self._root)


    class BakingRewardPerEndorsement0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_baking_reward_per_endorsement = self._io.read_u4be()
            if not self.len_baking_reward_per_endorsement <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_baking_reward_per_endorsement, self._io, u"/types/baking_reward_per_endorsement_0/seq/0")
            self._raw_baking_reward_per_endorsement = self._io.read_bytes(self.len_baking_reward_per_endorsement)
            _io__raw_baking_reward_per_endorsement = KaitaiStream(BytesIO(self._raw_baking_reward_per_endorsement))
            self.baking_reward_per_endorsement = Id007Psdelph1Constants.BakingRewardPerEndorsement(_io__raw_baking_reward_per_endorsement, self, self._root)


    class TimeBetweenBlocks0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_time_between_blocks = self._io.read_u4be()
            if not self.len_time_between_blocks <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_time_between_blocks, self._io, u"/types/time_between_blocks_0/seq/0")
            self._raw_time_between_blocks = self._io.read_bytes(self.len_time_between_blocks)
            _io__raw_time_between_blocks = KaitaiStream(BytesIO(self._raw_time_between_blocks))
            self.time_between_blocks = Id007Psdelph1Constants.TimeBetweenBlocks(_io__raw_time_between_blocks, self, self._root)


    class BakingRewardPerEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.baking_reward_per_endorsement_entries = []
            i = 0
            while not self._io.is_eof():
                self.baking_reward_per_endorsement_entries.append(Id007Psdelph1Constants.BakingRewardPerEndorsementEntries(self._io, self, self._root))
                i += 1



    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id007Psdelph1Constants.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1



    class BakingRewardPerEndorsementEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_007__psdelph1__mutez = Id007Psdelph1Constants.Id007Psdelph1Mutez(self._io, self, self._root)



