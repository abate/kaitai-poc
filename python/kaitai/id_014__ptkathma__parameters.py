# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id014PtkathmaParameters(KaitaiStruct):
    """Encoding id: 014-PtKathma.parameters."""

    class Bool(Enum):
        false = 0
        true = 255

    class BootstrapAccountsEltTag(Enum):
        public_key_known = 0
        public_key_unknown = 1
        public_key_known_with_delegate = 2
        public_key_unknown_with_delegate = 3

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.bootstrap_accounts = Id014PtkathmaParameters.BootstrapAccounts0(self._io, self, self._root)
        self.bootstrap_contracts = Id014PtkathmaParameters.BootstrapContracts0(self._io, self, self._root)
        self.commitments = Id014PtkathmaParameters.Commitments0(self._io, self, self._root)
        self.security_deposit_ramp_up_cycles_tag = KaitaiStream.resolve_enum(Id014PtkathmaParameters.Bool, self._io.read_u1())
        if self.security_deposit_ramp_up_cycles_tag == Id014PtkathmaParameters.Bool.true:
            self.security_deposit_ramp_up_cycles = Id014PtkathmaParameters.Int31(self._io, self, self._root)

        self.no_reward_cycles_tag = KaitaiStream.resolve_enum(Id014PtkathmaParameters.Bool, self._io.read_u1())
        if self.no_reward_cycles_tag == Id014PtkathmaParameters.Bool.true:
            self.no_reward_cycles = Id014PtkathmaParameters.Int31(self._io, self, self._root)

        self.preserved_cycles = self._io.read_u1()
        self.blocks_per_cycle = self._io.read_s4be()
        self.blocks_per_commitment = self._io.read_s4be()
        self.nonce_revelation_threshold = self._io.read_s4be()
        self.blocks_per_stake_snapshot = self._io.read_s4be()
        self.cycles_per_voting_period = self._io.read_s4be()
        self.hard_gas_limit_per_operation = Id014PtkathmaParameters.Z(self._io, self, self._root)
        self.hard_gas_limit_per_block = Id014PtkathmaParameters.Z(self._io, self, self._root)
        self.proof_of_work_threshold = self._io.read_s8be()
        self.tokens_per_roll = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
        self.vdf_difficulty = self._io.read_s8be()
        self.seed_nonce_revelation_tip = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
        self.origination_size = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.baking_reward_fixed_portion = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
        self.baking_reward_bonus_per_slot = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
        self.endorsing_reward_per_slot = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
        self.cost_per_byte = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
        self.hard_storage_limit_per_operation = Id014PtkathmaParameters.Z(self._io, self, self._root)
        self.quorum_min = self._io.read_s4be()
        self.quorum_max = self._io.read_s4be()
        self.min_proposal_quorum = self._io.read_s4be()
        self.liquidity_baking_subsidy = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
        self.liquidity_baking_sunset_level = self._io.read_s4be()
        self.liquidity_baking_toggle_ema_threshold = self._io.read_s4be()
        self.max_operations_time_to_live = self._io.read_s2be()
        self.minimal_block_delay = self._io.read_s8be()
        self.delay_increment_per_round = self._io.read_s8be()
        self.consensus_committee_size = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.consensus_threshold = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.minimal_participation_ratio = Id014PtkathmaParameters.MinimalParticipationRatio(self._io, self, self._root)
        self.max_slashing_period = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.frozen_deposits_percentage = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.double_baking_punishment = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
        self.ratio_of_frozen_deposits_slashed_per_double_endorsement = Id014PtkathmaParameters.RatioOfFrozenDepositsSlashedPerDoubleEndorsement(self._io, self, self._root)
        self.testnet_dictator_tag = KaitaiStream.resolve_enum(Id014PtkathmaParameters.Bool, self._io.read_u1())
        if self.testnet_dictator_tag == Id014PtkathmaParameters.Bool.true:
            self.testnet_dictator = Id014PtkathmaParameters.PublicKeyHash(self._io, self, self._root)

        self.initial_seed_tag = KaitaiStream.resolve_enum(Id014PtkathmaParameters.Bool, self._io.read_u1())
        if self.initial_seed_tag == Id014PtkathmaParameters.Bool.true:
            self.initial_seed = self._io.read_bytes(32)

        self.cache_script_size = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.cache_stake_distribution_cycles = self._io.read_s1()
        self.cache_sampler_state_cycles = self._io.read_s1()
        self.tx_rollup_enable = KaitaiStream.resolve_enum(Id014PtkathmaParameters.Bool, self._io.read_u1())
        self.tx_rollup_origination_size = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.tx_rollup_hard_size_limit_per_inbox = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.tx_rollup_hard_size_limit_per_message = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_withdrawals_per_batch = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.tx_rollup_commitment_bond = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
        self.tx_rollup_finality_period = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.tx_rollup_withdraw_period = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_inboxes_count = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_messages_per_inbox = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_commitments_count = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.tx_rollup_cost_per_byte_ema_factor = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.tx_rollup_max_ticket_payload_size = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.tx_rollup_rejection_max_proof_size = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.tx_rollup_sunset_level = self._io.read_s4be()
        self.dal_parametric = Id014PtkathmaParameters.DalParametric(self._io, self, self._root)
        self.sc_rollup_enable = KaitaiStream.resolve_enum(Id014PtkathmaParameters.Bool, self._io.read_u1())
        self.sc_rollup_origination_size = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.sc_rollup_challenge_window_in_blocks = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.sc_rollup_max_available_messages = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.sc_rollup_stake_amount = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
        self.sc_rollup_commitment_period_in_blocks = Id014PtkathmaParameters.Int31(self._io, self, self._root)
        self.sc_rollup_max_lookahead_in_blocks = self._io.read_s4be()
        self.sc_rollup_max_active_outbox_levels = self._io.read_s4be()
        self.sc_rollup_max_outbox_messages_per_level = Id014PtkathmaParameters.Int31(self._io, self, self._root)

    class RatioOfFrozenDepositsSlashedPerDoubleEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class PublicKeyKnown(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_known_field0 = Id014PtkathmaParameters.PublicKey(self._io, self, self._root)
            self.public_key_known_field1 = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)


    class Commitments(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_entries = []
            i = 0
            while not self._io.is_eof():
                self.commitments_entries.append(Id014PtkathmaParameters.CommitmentsEntries(self._io, self, self._root))
                i += 1



    class DalParametric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.feature_enable = KaitaiStream.resolve_enum(Id014PtkathmaParameters.Bool, self._io.read_u1())
            self.number_of_slots = self._io.read_s2be()
            self.number_of_shards = self._io.read_s2be()
            self.endorsement_lag = self._io.read_s2be()
            self.availability_threshold = self._io.read_s2be()


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id014PtkathmaParameters.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class CommitmentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_elt_field0 = self._io.read_bytes(20)
            self.commitments_elt_field1 = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)


    class Id014PtkathmaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__mutez = Id014PtkathmaParameters.N(self._io, self, self._root)


    class Commitments0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_commitments = self._io.read_u4be()
            if not self.len_commitments <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_commitments, self._io, u"/types/commitments_0/seq/0")
            self._raw_commitments = self._io.read_bytes(self.len_commitments)
            _io__raw_commitments = KaitaiStream(BytesIO(self._raw_commitments))
            self.commitments = Id014PtkathmaParameters.Commitments(_io__raw_commitments, self, self._root)


    class MinimalParticipationRatio(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class BootstrapAccounts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_accounts = self._io.read_u4be()
            if not self.len_bootstrap_accounts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_accounts, self._io, u"/types/bootstrap_accounts_0/seq/0")
            self._raw_bootstrap_accounts = self._io.read_bytes(self.len_bootstrap_accounts)
            _io__raw_bootstrap_accounts = KaitaiStream(BytesIO(self._raw_bootstrap_accounts))
            self.bootstrap_accounts = Id014PtkathmaParameters.BootstrapAccounts(_io__raw_bootstrap_accounts, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id014PtkathmaParameters.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id014PtkathmaParameters.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id014PtkathmaParameters.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id014PtkathmaParameters.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class PublicKeyUnknown(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_unknown_field0 = Id014PtkathmaParameters.PublicKeyHash(self._io, self, self._root)
            self.public_key_unknown_field1 = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)


    class PublicKeyUnknownWithDelegate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_unknown_with_delegate_field0 = Id014PtkathmaParameters.PublicKeyHash(self._io, self, self._root)
            self.public_key_unknown_with_delegate_field1 = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
            self.public_key_unknown_with_delegate_field2 = Id014PtkathmaParameters.PublicKeyHash(self._io, self, self._root)


    class BootstrapAccountsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_accounts_elt_tag = KaitaiStream.resolve_enum(Id014PtkathmaParameters.BootstrapAccountsEltTag, self._io.read_u1())
            if self.bootstrap_accounts_elt_tag == Id014PtkathmaParameters.BootstrapAccountsEltTag.public_key_known:
                self.public_key_known = Id014PtkathmaParameters.PublicKeyKnown(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == Id014PtkathmaParameters.BootstrapAccountsEltTag.public_key_unknown:
                self.public_key_unknown = Id014PtkathmaParameters.PublicKeyUnknown(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == Id014PtkathmaParameters.BootstrapAccountsEltTag.public_key_known_with_delegate:
                self.public_key_known_with_delegate = Id014PtkathmaParameters.PublicKeyKnownWithDelegate(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == Id014PtkathmaParameters.BootstrapAccountsEltTag.public_key_unknown_with_delegate:
                self.public_key_unknown_with_delegate = Id014PtkathmaParameters.PublicKeyUnknownWithDelegate(self._io, self, self._root)



    class BootstrapContracts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_contracts = self._io.read_u4be()
            if not self.len_bootstrap_contracts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_contracts, self._io, u"/types/bootstrap_contracts_0/seq/0")
            self._raw_bootstrap_contracts = self._io.read_bytes(self.len_bootstrap_contracts)
            _io__raw_bootstrap_contracts = KaitaiStream(BytesIO(self._raw_bootstrap_contracts))
            self.bootstrap_contracts = Id014PtkathmaParameters.BootstrapContracts(_io__raw_bootstrap_contracts, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class PublicKeyKnownWithDelegate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_known_with_delegate_field0 = Id014PtkathmaParameters.PublicKey(self._io, self, self._root)
            self.public_key_known_with_delegate_field1 = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
            self.public_key_known_with_delegate_field2 = Id014PtkathmaParameters.PublicKeyHash(self._io, self, self._root)


    class BootstrapContractsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id014PtkathmaParameters.Bool, self._io.read_u1())
            if self.delegate_tag == Id014PtkathmaParameters.Bool.true:
                self.delegate = Id014PtkathmaParameters.PublicKeyHash(self._io, self, self._root)

            self.amount = Id014PtkathmaParameters.Id014PtkathmaMutez(self._io, self, self._root)
            self.script = Id014PtkathmaParameters.Id014PtkathmaScriptedContracts(self._io, self, self._root)


    class BootstrapContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_contracts_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_contracts_entries.append(Id014PtkathmaParameters.BootstrapContractsEntries(self._io, self, self._root))
                i += 1



    class BootstrapAccounts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_accounts_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_accounts_entries.append(Id014PtkathmaParameters.BootstrapAccountsEntries(self._io, self, self._root))
                i += 1



    class Id014PtkathmaScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id014PtkathmaParameters.BytesDynUint30(self._io, self, self._root)
            self.storage = Id014PtkathmaParameters.BytesDynUint30(self._io, self, self._root)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id014PtkathmaParameters.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id014PtkathmaParameters.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id014PtkathmaParameters.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id014PtkathmaParameters.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id014PtkathmaParameters.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




