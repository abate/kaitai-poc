# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id018ProxfordFitness(KaitaiStruct):
    """Encoding id: 018-Proxford.fitness."""

    class LockedRoundTag(Enum):
        none = 0
        some = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.level = self._io.read_s4be()
        self.locked_round = Id018ProxfordFitness.LockedRound(self._io, self, self._root)
        self.predecessor_round = self._io.read_s4be()
        self.round = self._io.read_s4be()

    class LockedRound(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.locked_round_tag = KaitaiStream.resolve_enum(Id018ProxfordFitness.LockedRoundTag, self._io.read_u1())
            if self.locked_round_tag == Id018ProxfordFitness.LockedRoundTag.some:
                self.some = self._io.read_s4be()




