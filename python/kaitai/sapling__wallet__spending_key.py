# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class SaplingWalletSpendingKey(KaitaiStruct):
    """Encoding id: sapling.wallet.spending_key."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.depth = self._io.read_bytes(1)
        self.parent_fvk_tag = self._io.read_bytes(4)
        self.child_index = self._io.read_bytes(4)
        self.chain_code = self._io.read_bytes(32)
        self.expsk = SaplingWalletSpendingKey.SaplingWalletExpandedSpendingKey(self._io, self, self._root)
        self.dk = self._io.read_bytes(32)

    class SaplingWalletExpandedSpendingKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.ask = self._io.read_bytes(32)
            self.nsk = self._io.read_bytes(32)
            self.ovk = self._io.read_bytes(32)



