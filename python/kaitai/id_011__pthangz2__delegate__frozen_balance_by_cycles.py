# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id011Pthangz2DelegateFrozenBalanceByCycles(KaitaiStruct):
    """Encoding id: 011-PtHangz2.delegate.frozen_balance_by_cycles."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.len_id_011__pthangz2__delegate__frozen_balance_by_cycles = self._io.read_u4be()
        if not self.len_id_011__pthangz2__delegate__frozen_balance_by_cycles <= 1073741823:
            raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_011__pthangz2__delegate__frozen_balance_by_cycles, self._io, u"/seq/0")
        self._raw_id_011__pthangz2__delegate__frozen_balance_by_cycles = self._io.read_bytes(self.len_id_011__pthangz2__delegate__frozen_balance_by_cycles)
        _io__raw_id_011__pthangz2__delegate__frozen_balance_by_cycles = KaitaiStream(BytesIO(self._raw_id_011__pthangz2__delegate__frozen_balance_by_cycles))
        self.id_011__pthangz2__delegate__frozen_balance_by_cycles = Id011Pthangz2DelegateFrozenBalanceByCycles.Id011Pthangz2DelegateFrozenBalanceByCycles(_io__raw_id_011__pthangz2__delegate__frozen_balance_by_cycles, self, self._root)

    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id011Pthangz2DelegateFrozenBalanceByCycles.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id011Pthangz2DelegateFrozenBalanceByCycles(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__delegate__frozen_balance_by_cycles_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_011__pthangz2__delegate__frozen_balance_by_cycles_entries.append(Id011Pthangz2DelegateFrozenBalanceByCycles.Id011Pthangz2DelegateFrozenBalanceByCyclesEntries(self._io, self, self._root))
                i += 1



    class Id011Pthangz2Mutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__mutez = Id011Pthangz2DelegateFrozenBalanceByCycles.N(self._io, self, self._root)


    class Id011Pthangz2DelegateFrozenBalanceByCyclesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.cycle = self._io.read_s4be()
            self.deposits = Id011Pthangz2DelegateFrozenBalanceByCycles.Id011Pthangz2Mutez(self._io, self, self._root)
            self.fees = Id011Pthangz2DelegateFrozenBalanceByCycles.Id011Pthangz2Mutez(self._io, self, self._root)
            self.rewards = Id011Pthangz2DelegateFrozenBalanceByCycles.Id011Pthangz2Mutez(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)



