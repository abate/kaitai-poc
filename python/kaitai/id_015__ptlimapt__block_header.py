# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import block_header__shell
class Id015PtlimaptBlockHeader(KaitaiStruct):
    """Encoding id: 015-PtLimaPt.block_header."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_015__ptlimapt__block_header__alpha__full_header = Id015PtlimaptBlockHeader.Id015PtlimaptBlockHeaderAlphaFullHeader(self._io, self, self._root)

    class Id015PtlimaptBlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_015__ptlimapt__block_header__alpha__signed_contents = Id015PtlimaptBlockHeader.Id015PtlimaptBlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Id015PtlimaptBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__block_header__alpha__unsigned_contents = Id015PtlimaptBlockHeader.Id015PtlimaptBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes(64)


    class Id015PtlimaptBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_hash = self._io.read_bytes(32)
            self.payload_round = self._io.read_s4be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id015PtlimaptBlockHeader.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id015PtlimaptBlockHeader.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.liquidity_baking_toggle_vote = Id015PtlimaptBlockHeader.Id015PtlimaptLiquidityBakingToggleVote(self._io, self, self._root)


    class Id015PtlimaptLiquidityBakingToggleVote(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__liquidity_baking_toggle_vote = self._io.read_s1()



