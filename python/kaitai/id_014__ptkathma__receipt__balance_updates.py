# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id014PtkathmaReceiptBalanceUpdates(KaitaiStruct):
    """Encoding id: 014-PtKathma.receipt.balance_updates."""

    class Id014PtkathmaBondIdTag(Enum):
        tx_rollup_bond_id = 0
        sc_rollup_bond_id = 1

    class Id014PtkathmaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id014PtkathmaOperationMetadataAlphaBalanceTag(Enum):
        contract = 0
        block_fees = 2
        deposits = 4
        nonce_revelation_rewards = 5
        double_signing_evidence_rewards = 6
        endorsing_rewards = 7
        baking_rewards = 8
        baking_bonuses = 9
        storage_fees = 11
        double_signing_punishments = 12
        lost_endorsing_rewards = 13
        liquidity_baking_subsidies = 14
        burned = 15
        commitments = 16
        bootstrap = 17
        invoice = 18
        initial_commitments = 19
        minted = 20
        frozen_bonds = 21
        tx_rollup_rejection_rewards = 22
        tx_rollup_rejection_punishments = 23
        sc_rollup_refutation_punishments = 24

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class OriginTag(Enum):
        block_application = 0
        protocol_migration = 1
        subsidy = 2
        simulation = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_014__ptkathma__operation_metadata__alpha__balance_updates = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Id014PtkathmaRollupAddress(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__rollup_address = Id014PtkathmaReceiptBalanceUpdates.BytesDynUint30(self._io, self, self._root)


    class FrozenBonds(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaContractId(self._io, self, self._root)
            self.bond_id = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaBondId(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class LostEndorsingRewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id014PtkathmaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.participation = KaitaiStream.resolve_enum(Id014PtkathmaReceiptBalanceUpdates.Bool, self._io.read_u1())
            self.revelation = KaitaiStream.resolve_enum(Id014PtkathmaReceiptBalanceUpdates.Bool, self._io.read_u1())


    class Id014PtkathmaBondId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__bond_id_tag = KaitaiStream.resolve_enum(Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaBondIdTag, self._io.read_u1())
            if self.id_014__ptkathma__bond_id_tag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaBondIdTag.tx_rollup_bond_id:
                self.tx_rollup_bond_id = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaTxRollupId(self._io, self, self._root)

            if self.id_014__ptkathma__bond_id_tag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaBondIdTag.sc_rollup_bond_id:
                self.sc_rollup_bond_id = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaRollupAddress(self._io, self, self._root)



    class Id014PtkathmaOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_014__ptkathma__operation_metadata__alpha__balance_updates_entries.append(Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class Id014PtkathmaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__contract_id_tag = KaitaiStream.resolve_enum(Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaContractIdTag, self._io.read_u1())
            if self.id_014__ptkathma__contract_id_tag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaContractIdTag.implicit:
                self.implicit = Id014PtkathmaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_014__ptkathma__contract_id_tag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaContractIdTag.originated:
                self.originated = Id014PtkathmaReceiptBalanceUpdates.Originated(self._io, self, self._root)



    class Id014PtkathmaOperationMetadataAlphaBalance(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__operation_metadata__alpha__balance_tag = KaitaiStream.resolve_enum(Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceTag, self._io.read_u1())
            if self.id_014__ptkathma__operation_metadata__alpha__balance_tag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceTag.contract:
                self.contract = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaContractId(self._io, self, self._root)

            if self.id_014__ptkathma__operation_metadata__alpha__balance_tag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceTag.deposits:
                self.deposits = Id014PtkathmaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_014__ptkathma__operation_metadata__alpha__balance_tag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceTag.lost_endorsing_rewards:
                self.lost_endorsing_rewards = Id014PtkathmaReceiptBalanceUpdates.LostEndorsingRewards(self._io, self, self._root)

            if self.id_014__ptkathma__operation_metadata__alpha__balance_tag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceTag.commitments:
                self.commitments = self._io.read_bytes(20)

            if self.id_014__ptkathma__operation_metadata__alpha__balance_tag == Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceTag.frozen_bonds:
                self.frozen_bonds = Id014PtkathmaReceiptBalanceUpdates.FrozenBonds(self._io, self, self._root)



    class Id014PtkathmaOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_014__ptkathma__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_014__ptkathma__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_014__ptkathma__operation_metadata__alpha__balance_updates, self._io, u"/types/id_014__ptkathma__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_014__ptkathma__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_014__ptkathma__operation_metadata__alpha__balance_updates)
            _io__raw_id_014__ptkathma__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_014__ptkathma__operation_metadata__alpha__balance_updates))
            self.id_014__ptkathma__operation_metadata__alpha__balance_updates = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceUpdates(_io__raw_id_014__ptkathma__operation_metadata__alpha__balance_updates, self, self._root)


    class Id014PtkathmaOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__operation_metadata__alpha__balance = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalance(self._io, self, self._root)
            self.id_014__ptkathma__operation_metadata__alpha__balance_update = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaBalanceUpdate(self._io, self, self._root)
            self.id_014__ptkathma__operation_metadata__alpha__update_origin = Id014PtkathmaReceiptBalanceUpdates.Id014PtkathmaOperationMetadataAlphaUpdateOrigin(self._io, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Id014PtkathmaOperationMetadataAlphaBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Id014PtkathmaTxRollupId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup_hash = self._io.read_bytes(20)


    class Id014PtkathmaOperationMetadataAlphaUpdateOrigin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.origin = KaitaiStream.resolve_enum(Id014PtkathmaReceiptBalanceUpdates.OriginTag, self._io.read_u1())


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id014PtkathmaReceiptBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id014PtkathmaReceiptBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id014PtkathmaReceiptBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id014PtkathmaReceiptBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




