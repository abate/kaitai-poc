# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class SignerMessagesRequest(KaitaiStruct):
    """Encoding id: signer_messages.request."""

    class Bool(Enum):
        false = 0
        true = 255

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class SignerMessagesRequestTag(Enum):
        sign = 0
        public_key = 1
        authorized_keys = 2
        deterministic_nonce = 3
        deterministic_nonce_hash = 4
        supports_deterministic_nonces = 5
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.signer_messages__request_tag = KaitaiStream.resolve_enum(SignerMessagesRequest.SignerMessagesRequestTag, self._io.read_u1())
        if self.signer_messages__request_tag == SignerMessagesRequest.SignerMessagesRequestTag.sign:
            self.sign = SignerMessagesRequest.Sign(self._io, self, self._root)

        if self.signer_messages__request_tag == SignerMessagesRequest.SignerMessagesRequestTag.public_key:
            self.public_key = SignerMessagesRequest.SignerMessagesPublicKeyRequest(self._io, self, self._root)

        if self.signer_messages__request_tag == SignerMessagesRequest.SignerMessagesRequestTag.deterministic_nonce:
            self.deterministic_nonce = SignerMessagesRequest.DeterministicNonce(self._io, self, self._root)

        if self.signer_messages__request_tag == SignerMessagesRequest.SignerMessagesRequestTag.deterministic_nonce_hash:
            self.deterministic_nonce_hash = SignerMessagesRequest.DeterministicNonceHash(self._io, self, self._root)

        if self.signer_messages__request_tag == SignerMessagesRequest.SignerMessagesRequestTag.supports_deterministic_nonces:
            self.supports_deterministic_nonces = SignerMessagesRequest.SignerMessagesSupportsDeterministicNoncesRequest(self._io, self, self._root)


    class SignerMessagesPublicKeyRequest(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = SignerMessagesRequest.PublicKeyHash(self._io, self, self._root)


    class DeterministicNonce(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = SignerMessagesRequest.PublicKeyHash(self._io, self, self._root)
            self.data = SignerMessagesRequest.BytesDynUint30(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(SignerMessagesRequest.Bool, self._io.read_u1())
            if self.signature_tag == SignerMessagesRequest.Bool.true:
                self.signature = self._io.read_bytes_full()



    class Sign(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = SignerMessagesRequest.PublicKeyHash(self._io, self, self._root)
            self.data = SignerMessagesRequest.BytesDynUint30(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(SignerMessagesRequest.Bool, self._io.read_u1())
            if self.signature_tag == SignerMessagesRequest.Bool.true:
                self.signature = self._io.read_bytes_full()



    class SignerMessagesSupportsDeterministicNoncesRequest(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = SignerMessagesRequest.PublicKeyHash(self._io, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class DeterministicNonceHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = SignerMessagesRequest.PublicKeyHash(self._io, self, self._root)
            self.data = SignerMessagesRequest.BytesDynUint30(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(SignerMessagesRequest.Bool, self._io.read_u1())
            if self.signature_tag == SignerMessagesRequest.Bool.true:
                self.signature = self._io.read_bytes_full()



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(SignerMessagesRequest.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == SignerMessagesRequest.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == SignerMessagesRequest.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == SignerMessagesRequest.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == SignerMessagesRequest.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)




