# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id006PscarthaDelegateBalanceUpdates(KaitaiStruct):
    """Encoding id: 006-PsCARTHA.delegate.balance_updates."""

    class Id006PscarthaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id006PscarthaOperationMetadataAlphaBalanceTag(Enum):
        contract = 0
        rewards = 1
        fees = 2
        deposits = 3

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_006__pscartha__operation_metadata__alpha__balance_updates = Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Id006PscarthaOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__operation_metadata__alpha__balance = Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalance(self._io, self, self._root)
            self.id_006__pscartha__operation_metadata__alpha__balance_update = Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceUpdate(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id006PscarthaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__contract_id_tag = KaitaiStream.resolve_enum(Id006PscarthaDelegateBalanceUpdates.Id006PscarthaContractIdTag, self._io.read_u1())
            if self.id_006__pscartha__contract_id_tag == Id006PscarthaDelegateBalanceUpdates.Id006PscarthaContractIdTag.implicit:
                self.implicit = Id006PscarthaDelegateBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_006__pscartha__contract_id_tag == Id006PscarthaDelegateBalanceUpdates.Id006PscarthaContractIdTag.originated:
                self.originated = Id006PscarthaDelegateBalanceUpdates.Originated(self._io, self, self._root)



    class Id006PscarthaOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_006__pscartha__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_006__pscartha__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_006__pscartha__operation_metadata__alpha__balance_updates, self._io, u"/types/id_006__pscartha__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_006__pscartha__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_006__pscartha__operation_metadata__alpha__balance_updates)
            _io__raw_id_006__pscartha__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_006__pscartha__operation_metadata__alpha__balance_updates))
            self.id_006__pscartha__operation_metadata__alpha__balance_updates = Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceUpdates(_io__raw_id_006__pscartha__operation_metadata__alpha__balance_updates, self, self._root)


    class Id006PscarthaOperationMetadataAlphaBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Rewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id006PscarthaDelegateBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id006PscarthaOperationMetadataAlphaBalance(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__operation_metadata__alpha__balance_tag = KaitaiStream.resolve_enum(Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceTag, self._io.read_u1())
            if self.id_006__pscartha__operation_metadata__alpha__balance_tag == Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceTag.contract:
                self.contract = Id006PscarthaDelegateBalanceUpdates.Id006PscarthaContractId(self._io, self, self._root)

            if self.id_006__pscartha__operation_metadata__alpha__balance_tag == Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceTag.rewards:
                self.rewards = Id006PscarthaDelegateBalanceUpdates.Rewards(self._io, self, self._root)

            if self.id_006__pscartha__operation_metadata__alpha__balance_tag == Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceTag.fees:
                self.fees = Id006PscarthaDelegateBalanceUpdates.Fees(self._io, self, self._root)

            if self.id_006__pscartha__operation_metadata__alpha__balance_tag == Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceTag.deposits:
                self.deposits = Id006PscarthaDelegateBalanceUpdates.Deposits(self._io, self, self._root)



    class Deposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id006PscarthaDelegateBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Fees(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id006PscarthaDelegateBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id006PscarthaOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_006__pscartha__operation_metadata__alpha__balance_updates_entries.append(Id006PscarthaDelegateBalanceUpdates.Id006PscarthaOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id006PscarthaDelegateBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id006PscarthaDelegateBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id006PscarthaDelegateBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id006PscarthaDelegateBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




