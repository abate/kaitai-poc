# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import sapling__transaction__commitment
from kaitai import sapling__transaction__ciphertext
class SaplingTransactionOutput(KaitaiStruct):
    """Encoding id: sapling.transaction.output
    Description: Output of a transaction."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.cm = sapling__transaction__commitment.SaplingTransactionCommitment(self._io)
        self.proof_o = self._io.read_bytes(192)
        self.ciphertext = sapling__transaction__ciphertext.SaplingTransactionCiphertext(self._io)


