# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class AlphaParameters(KaitaiStruct):
    """Encoding id: alpha.parameters."""

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class PvmKind(Enum):
        arith = 0
        wasm_2_0_0 = 1
        riscv = 2

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class Bool(Enum):
        false = 0
        true = 255

    class BootstrapAccountsEltTag(Enum):
        public_key_known = 0
        public_key_unknown = 1
        public_key_known_with_delegate = 2
        public_key_unknown_with_delegate = 3
        public_key_known_with_consensus_key = 4
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.bootstrap_accounts = AlphaParameters.BootstrapAccounts0(self._io, self, self._root)
        self.bootstrap_contracts = AlphaParameters.BootstrapContracts0(self._io, self, self._root)
        self.bootstrap_smart_rollups = AlphaParameters.BootstrapSmartRollups0(self._io, self, self._root)
        self.commitments = AlphaParameters.Commitments0(self._io, self, self._root)
        self.security_deposit_ramp_up_cycles_tag = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
        if self.security_deposit_ramp_up_cycles_tag == AlphaParameters.Bool.true:
            self.security_deposit_ramp_up_cycles = AlphaParameters.Int31(self._io, self, self._root)

        self.no_reward_cycles_tag = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
        if self.no_reward_cycles_tag == AlphaParameters.Bool.true:
            self.no_reward_cycles = AlphaParameters.Int31(self._io, self, self._root)

        self.consensus_rights_delay = self._io.read_u1()
        self.blocks_preservation_cycles = self._io.read_u1()
        self.delegate_parameters_activation_delay = self._io.read_u1()
        self.blocks_per_cycle = self._io.read_s4be()
        self.blocks_per_commitment = self._io.read_s4be()
        self.nonce_revelation_threshold = self._io.read_s4be()
        self.cycles_per_voting_period = self._io.read_s4be()
        self.hard_gas_limit_per_operation = AlphaParameters.Z(self._io, self, self._root)
        self.hard_gas_limit_per_block = AlphaParameters.Z(self._io, self, self._root)
        self.proof_of_work_threshold = self._io.read_s8be()
        self.minimal_stake = AlphaParameters.AlphaMutez(self._io, self, self._root)
        self.minimal_frozen_stake = AlphaParameters.AlphaMutez(self._io, self, self._root)
        self.vdf_difficulty = self._io.read_s8be()
        self.origination_size = AlphaParameters.Int31(self._io, self, self._root)
        self.issuance_weights = AlphaParameters.IssuanceWeights(self._io, self, self._root)
        self.cost_per_byte = AlphaParameters.AlphaMutez(self._io, self, self._root)
        self.hard_storage_limit_per_operation = AlphaParameters.Z(self._io, self, self._root)
        self.quorum_min = self._io.read_s4be()
        self.quorum_max = self._io.read_s4be()
        self.min_proposal_quorum = self._io.read_s4be()
        self.liquidity_baking_subsidy = AlphaParameters.AlphaMutez(self._io, self, self._root)
        self.liquidity_baking_toggle_ema_threshold = self._io.read_s4be()
        self.max_operations_time_to_live = self._io.read_s2be()
        self.minimal_block_delay = self._io.read_s8be()
        self.delay_increment_per_round = self._io.read_s8be()
        self.consensus_committee_size = AlphaParameters.Int31(self._io, self, self._root)
        self.consensus_threshold = AlphaParameters.Int31(self._io, self, self._root)
        self.minimal_participation_ratio = AlphaParameters.MinimalParticipationRatio(self._io, self, self._root)
        self.limit_of_delegation_over_baking = self._io.read_u1()
        self.percentage_of_frozen_deposits_slashed_per_double_baking = self._io.read_u2be()
        self.percentage_of_frozen_deposits_slashed_per_double_attestation = self._io.read_u2be()
        self.max_slashing_per_block = self._io.read_u2be()
        self.max_slashing_threshold = AlphaParameters.Int31(self._io, self, self._root)
        self.testnet_dictator_tag = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
        if self.testnet_dictator_tag == AlphaParameters.Bool.true:
            self.testnet_dictator = AlphaParameters.PublicKeyHash(self._io, self, self._root)

        self.initial_seed_tag = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
        if self.initial_seed_tag == AlphaParameters.Bool.true:
            self.initial_seed = self._io.read_bytes(32)

        self.cache_script_size = AlphaParameters.Int31(self._io, self, self._root)
        self.cache_stake_distribution_cycles = self._io.read_s1()
        self.cache_sampler_state_cycles = self._io.read_s1()
        self.dal_parametric = AlphaParameters.DalParametric(self._io, self, self._root)
        self.smart_rollup_arith_pvm_enable = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
        self.smart_rollup_origination_size = AlphaParameters.Int31(self._io, self, self._root)
        self.smart_rollup_challenge_window_in_blocks = AlphaParameters.Int31(self._io, self, self._root)
        self.smart_rollup_stake_amount = AlphaParameters.AlphaMutez(self._io, self, self._root)
        self.smart_rollup_commitment_period_in_blocks = AlphaParameters.Int31(self._io, self, self._root)
        self.smart_rollup_max_lookahead_in_blocks = self._io.read_s4be()
        self.smart_rollup_max_active_outbox_levels = self._io.read_s4be()
        self.smart_rollup_max_outbox_messages_per_level = AlphaParameters.Int31(self._io, self, self._root)
        self.smart_rollup_number_of_sections_in_dissection = self._io.read_u1()
        self.smart_rollup_timeout_period_in_blocks = AlphaParameters.Int31(self._io, self, self._root)
        self.smart_rollup_max_number_of_cemented_commitments = AlphaParameters.Int31(self._io, self, self._root)
        self.smart_rollup_max_number_of_parallel_games = AlphaParameters.Int31(self._io, self, self._root)
        self.smart_rollup_reveal_activation_level = AlphaParameters.SmartRollupRevealActivationLevel(self._io, self, self._root)
        self.smart_rollup_private_enable = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
        self.smart_rollup_riscv_pvm_enable = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
        self.zk_rollup_enable = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
        self.zk_rollup_origination_size = AlphaParameters.Int31(self._io, self, self._root)
        self.zk_rollup_min_pending_to_process = AlphaParameters.Int31(self._io, self, self._root)
        self.zk_rollup_max_ticket_payload_size = AlphaParameters.Int31(self._io, self, self._root)
        self.global_limit_of_staking_over_baking = self._io.read_u1()
        self.edge_of_staking_over_delegation = self._io.read_u1()
        self.adaptive_issuance_launch_ema_threshold = self._io.read_s4be()
        self.adaptive_rewards_params = AlphaParameters.AdaptiveRewardsParams(self._io, self, self._root)
        self.adaptive_issuance_activation_vote_enable = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
        self.autostaking_enable = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
        self.adaptive_issuance_force_activation = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
        self.ns_enable = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
        self.direct_ticket_spending_enable = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())

    class BootstrapSmartRollups0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_smart_rollups = self._io.read_u4be()
            if not self.len_bootstrap_smart_rollups <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_smart_rollups, self._io, u"/types/bootstrap_smart_rollups_0/seq/0")
            self._raw_bootstrap_smart_rollups = self._io.read_bytes(self.len_bootstrap_smart_rollups)
            _io__raw_bootstrap_smart_rollups = KaitaiStream(BytesIO(self._raw_bootstrap_smart_rollups))
            self.bootstrap_smart_rollups = AlphaParameters.BootstrapSmartRollups(_io__raw_bootstrap_smart_rollups, self, self._root)


    class AdaptiveRewardsParams(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.issuance_ratio_final_min = AlphaParameters.IssuanceRatioFinalMin(self._io, self, self._root)
            self.issuance_ratio_final_max = AlphaParameters.IssuanceRatioFinalMax(self._io, self, self._root)
            self.issuance_ratio_initial_min = AlphaParameters.IssuanceRatioInitialMin(self._io, self, self._root)
            self.issuance_ratio_initial_max = AlphaParameters.IssuanceRatioInitialMax(self._io, self, self._root)
            self.initial_period = self._io.read_u1()
            self.transition_period = self._io.read_u1()
            self.max_bonus = self._io.read_s8be()
            self.growth_rate = AlphaParameters.GrowthRate(self._io, self, self._root)
            self.center_dz = AlphaParameters.CenterDz(self._io, self, self._root)
            self.radius_dz = AlphaParameters.RadiusDz(self._io, self, self._root)


    class Whitelist0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_whitelist = self._io.read_u4be()
            if not self.len_whitelist <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_whitelist, self._io, u"/types/whitelist_0/seq/0")
            self._raw_whitelist = self._io.read_bytes(self.len_whitelist)
            _io__raw_whitelist = KaitaiStream(BytesIO(self._raw_whitelist))
            self.whitelist = AlphaParameters.Whitelist(_io__raw_whitelist, self, self._root)


    class CenterDz(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaParameters.Z(self._io, self, self._root)
            self.denominator = AlphaParameters.Z(self._io, self, self._root)


    class IssuanceWeights(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.base_total_issued_per_minute = AlphaParameters.AlphaMutez(self._io, self, self._root)
            self.baking_reward_fixed_portion_weight = AlphaParameters.Int31(self._io, self, self._root)
            self.baking_reward_bonus_weight = AlphaParameters.Int31(self._io, self, self._root)
            self.attesting_reward_weight = AlphaParameters.Int31(self._io, self, self._root)
            self.seed_nonce_revelation_tip_weight = AlphaParameters.Int31(self._io, self, self._root)
            self.vdf_revelation_tip_weight = AlphaParameters.Int31(self._io, self, self._root)


    class PublicKeyKnown(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_known_field0 = AlphaParameters.PublicKey(self._io, self, self._root)
            self.public_key_known_field1 = AlphaParameters.AlphaMutez(self._io, self, self._root)


    class Commitments(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_entries = []
            i = 0
            while not self._io.is_eof():
                self.commitments_entries.append(AlphaParameters.CommitmentsEntries(self._io, self, self._root))
                i += 1



    class DalParametric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.feature_enable = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
            self.incentives_enable = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
            self.number_of_slots = self._io.read_u2be()
            self.attestation_lag = self._io.read_u1()
            self.attestation_threshold = self._io.read_u1()
            self.redundancy_factor = self._io.read_u1()
            self.page_size = self._io.read_u2be()
            self.slot_size = AlphaParameters.Int31(self._io, self, self._root)
            self.number_of_shards = self._io.read_u2be()


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = AlphaParameters.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class CommitmentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_elt_field0 = self._io.read_bytes(20)
            self.commitments_elt_field1 = AlphaParameters.AlphaMutez(self._io, self, self._root)


    class Commitments0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_commitments = self._io.read_u4be()
            if not self.len_commitments <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_commitments, self._io, u"/types/commitments_0/seq/0")
            self._raw_commitments = self._io.read_bytes(self.len_commitments)
            _io__raw_commitments = KaitaiStream(BytesIO(self._raw_commitments))
            self.commitments = AlphaParameters.Commitments(_io__raw_commitments, self, self._root)


    class MinimalParticipationRatio(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class WhitelistEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.signature__public_key_hash = AlphaParameters.PublicKeyHash(self._io, self, self._root)


    class BootstrapAccounts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_accounts = self._io.read_u4be()
            if not self.len_bootstrap_accounts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_accounts, self._io, u"/types/bootstrap_accounts_0/seq/0")
            self._raw_bootstrap_accounts = self._io.read_bytes(self.len_bootstrap_accounts)
            _io__raw_bootstrap_accounts = KaitaiStream(BytesIO(self._raw_bootstrap_accounts))
            self.bootstrap_accounts = AlphaParameters.BootstrapAccounts(_io__raw_bootstrap_accounts, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(AlphaParameters.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == AlphaParameters.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == AlphaParameters.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == AlphaParameters.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)

            if self.public_key_tag == AlphaParameters.PublicKeyTag.bls:
                self.bls = self._io.read_bytes(48)



    class PublicKeyUnknown(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_unknown_field0 = AlphaParameters.PublicKeyHash(self._io, self, self._root)
            self.public_key_unknown_field1 = AlphaParameters.AlphaMutez(self._io, self, self._root)


    class GrowthRate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaParameters.Z(self._io, self, self._root)
            self.denominator = AlphaParameters.Z(self._io, self, self._root)


    class BootstrapSmartRollups(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_smart_rollups_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_smart_rollups_entries.append(AlphaParameters.BootstrapSmartRollupsEntries(self._io, self, self._root))
                i += 1



    class PublicKeyUnknownWithDelegate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_unknown_with_delegate_field0 = AlphaParameters.PublicKeyHash(self._io, self, self._root)
            self.public_key_unknown_with_delegate_field1 = AlphaParameters.AlphaMutez(self._io, self, self._root)
            self.public_key_unknown_with_delegate_field2 = AlphaParameters.PublicKeyHash(self._io, self, self._root)


    class BootstrapAccountsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_accounts_elt_tag = KaitaiStream.resolve_enum(AlphaParameters.BootstrapAccountsEltTag, self._io.read_u1())
            if self.bootstrap_accounts_elt_tag == AlphaParameters.BootstrapAccountsEltTag.public_key_known:
                self.public_key_known = AlphaParameters.PublicKeyKnown(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == AlphaParameters.BootstrapAccountsEltTag.public_key_unknown:
                self.public_key_unknown = AlphaParameters.PublicKeyUnknown(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == AlphaParameters.BootstrapAccountsEltTag.public_key_known_with_delegate:
                self.public_key_known_with_delegate = AlphaParameters.PublicKeyKnownWithDelegate(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == AlphaParameters.BootstrapAccountsEltTag.public_key_unknown_with_delegate:
                self.public_key_unknown_with_delegate = AlphaParameters.PublicKeyUnknownWithDelegate(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == AlphaParameters.BootstrapAccountsEltTag.public_key_known_with_consensus_key:
                self.public_key_known_with_consensus_key = AlphaParameters.PublicKeyKnownWithConsensusKey(self._io, self, self._root)



    class BootstrapContracts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_contracts = self._io.read_u4be()
            if not self.len_bootstrap_contracts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_contracts, self._io, u"/types/bootstrap_contracts_0/seq/0")
            self._raw_bootstrap_contracts = self._io.read_bytes(self.len_bootstrap_contracts)
            _io__raw_bootstrap_contracts = KaitaiStream(BytesIO(self._raw_bootstrap_contracts))
            self.bootstrap_contracts = AlphaParameters.BootstrapContracts(_io__raw_bootstrap_contracts, self, self._root)


    class SmartRollupRevealActivationLevel(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.raw_data = self._io.read_s4be()
            self.metadata = self._io.read_s4be()
            self.dal_page = self._io.read_s4be()
            self.dal_parameters = self._io.read_s4be()
            self.dal_attested_slots_validity_lag = AlphaParameters.Int31(self._io, self, self._root)


    class IssuanceRatioFinalMin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaParameters.Z(self._io, self, self._root)
            self.denominator = AlphaParameters.Z(self._io, self, self._root)


    class IssuanceRatioInitialMax(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaParameters.Z(self._io, self, self._root)
            self.denominator = AlphaParameters.Z(self._io, self, self._root)


    class BootstrapSmartRollupsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.address = self._io.read_bytes(20)
            self.pvm_kind = KaitaiStream.resolve_enum(AlphaParameters.PvmKind, self._io.read_u1())
            self.kernel = AlphaParameters.BytesDynUint30(self._io, self, self._root)
            self.parameters_ty = AlphaParameters.BytesDynUint30(self._io, self, self._root)
            self.whitelist_tag = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
            if self.whitelist_tag == AlphaParameters.Bool.true:
                self.whitelist = AlphaParameters.Whitelist0(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class PublicKeyKnownWithDelegate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_known_with_delegate_field0 = AlphaParameters.PublicKey(self._io, self, self._root)
            self.public_key_known_with_delegate_field1 = AlphaParameters.AlphaMutez(self._io, self, self._root)
            self.public_key_known_with_delegate_field2 = AlphaParameters.PublicKeyHash(self._io, self, self._root)


    class IssuanceRatioInitialMin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaParameters.Z(self._io, self, self._root)
            self.denominator = AlphaParameters.Z(self._io, self, self._root)


    class AlphaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__mutez = AlphaParameters.N(self._io, self, self._root)


    class Whitelist(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.whitelist_entries = []
            i = 0
            while not self._io.is_eof():
                self.whitelist_entries.append(AlphaParameters.WhitelistEntries(self._io, self, self._root))
                i += 1



    class BootstrapContractsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
            if self.delegate_tag == AlphaParameters.Bool.true:
                self.delegate = AlphaParameters.PublicKeyHash(self._io, self, self._root)

            self.amount = AlphaParameters.AlphaMutez(self._io, self, self._root)
            self.script = AlphaParameters.AlphaScriptedContracts(self._io, self, self._root)
            self.hash_tag = KaitaiStream.resolve_enum(AlphaParameters.Bool, self._io.read_u1())
            if self.hash_tag == AlphaParameters.Bool.true:
                self.hash = self._io.read_bytes(20)



    class BootstrapContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_contracts_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_contracts_entries.append(AlphaParameters.BootstrapContractsEntries(self._io, self, self._root))
                i += 1



    class BootstrapAccounts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_accounts_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_accounts_entries.append(AlphaParameters.BootstrapAccountsEntries(self._io, self, self._root))
                i += 1



    class RadiusDz(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaParameters.Z(self._io, self, self._root)
            self.denominator = AlphaParameters.Z(self._io, self, self._root)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class AlphaScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = AlphaParameters.BytesDynUint30(self._io, self, self._root)
            self.storage = AlphaParameters.BytesDynUint30(self._io, self, self._root)


    class PublicKeyKnownWithConsensusKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_known_with_consensus_key_field0 = AlphaParameters.PublicKey(self._io, self, self._root)
            self.public_key_known_with_consensus_key_field1 = AlphaParameters.AlphaMutez(self._io, self, self._root)
            self.public_key_known_with_consensus_key_field2 = AlphaParameters.PublicKey(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(AlphaParameters.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == AlphaParameters.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaParameters.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaParameters.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaParameters.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class IssuanceRatioFinalMax(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaParameters.Z(self._io, self, self._root)
            self.denominator = AlphaParameters.Z(self._io, self, self._root)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = AlphaParameters.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




