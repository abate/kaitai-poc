# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id018ProxfordFrozenStaker(KaitaiStruct):
    """Encoding id: 018-Proxford.frozen_staker."""

    class Id018ProxfordContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id018ProxfordFrozenStakerTag(Enum):
        single = 0
        shared = 1
        baker = 2

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_018__proxford__frozen_staker = Id018ProxfordFrozenStaker.Id018ProxfordFrozenStaker(self._io, self, self._root)

    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id018ProxfordContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__contract_id_tag = KaitaiStream.resolve_enum(Id018ProxfordFrozenStaker.Id018ProxfordContractIdTag, self._io.read_u1())
            if self.id_018__proxford__contract_id_tag == Id018ProxfordFrozenStaker.Id018ProxfordContractIdTag.implicit:
                self.implicit = Id018ProxfordFrozenStaker.PublicKeyHash(self._io, self, self._root)

            if self.id_018__proxford__contract_id_tag == Id018ProxfordFrozenStaker.Id018ProxfordContractIdTag.originated:
                self.originated = Id018ProxfordFrozenStaker.Originated(self._io, self, self._root)



    class Single(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id018ProxfordFrozenStaker.Id018ProxfordContractId(self._io, self, self._root)
            self.delegate = Id018ProxfordFrozenStaker.PublicKeyHash(self._io, self, self._root)


    class Id018ProxfordFrozenStaker(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__frozen_staker_tag = KaitaiStream.resolve_enum(Id018ProxfordFrozenStaker.Id018ProxfordFrozenStakerTag, self._io.read_u1())
            if self.id_018__proxford__frozen_staker_tag == Id018ProxfordFrozenStaker.Id018ProxfordFrozenStakerTag.single:
                self.single = Id018ProxfordFrozenStaker.Single(self._io, self, self._root)

            if self.id_018__proxford__frozen_staker_tag == Id018ProxfordFrozenStaker.Id018ProxfordFrozenStakerTag.shared:
                self.shared = Id018ProxfordFrozenStaker.PublicKeyHash(self._io, self, self._root)

            if self.id_018__proxford__frozen_staker_tag == Id018ProxfordFrozenStaker.Id018ProxfordFrozenStakerTag.baker:
                self.baker = Id018ProxfordFrozenStaker.PublicKeyHash(self._io, self, self._root)



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id018ProxfordFrozenStaker.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id018ProxfordFrozenStaker.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id018ProxfordFrozenStaker.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id018ProxfordFrozenStaker.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id018ProxfordFrozenStaker.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)




