# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class AlphaUnstakedFrozenStaker(KaitaiStruct):
    """Encoding id: alpha.unstaked_frozen_staker."""

    class AlphaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class AlphaStakerTag(Enum):
        single = 0
        shared = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.alpha__staker = AlphaUnstakedFrozenStaker.AlphaStaker(self._io, self, self._root)

    class AlphaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__contract_id_tag = KaitaiStream.resolve_enum(AlphaUnstakedFrozenStaker.AlphaContractIdTag, self._io.read_u1())
            if self.alpha__contract_id_tag == AlphaUnstakedFrozenStaker.AlphaContractIdTag.implicit:
                self.implicit = AlphaUnstakedFrozenStaker.PublicKeyHash(self._io, self, self._root)

            if self.alpha__contract_id_tag == AlphaUnstakedFrozenStaker.AlphaContractIdTag.originated:
                self.originated = AlphaUnstakedFrozenStaker.Originated(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Single(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = AlphaUnstakedFrozenStaker.AlphaContractId(self._io, self, self._root)
            self.delegate = AlphaUnstakedFrozenStaker.PublicKeyHash(self._io, self, self._root)


    class AlphaStaker(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__staker_tag = KaitaiStream.resolve_enum(AlphaUnstakedFrozenStaker.AlphaStakerTag, self._io.read_u1())
            if self.alpha__staker_tag == AlphaUnstakedFrozenStaker.AlphaStakerTag.single:
                self.single = AlphaUnstakedFrozenStaker.Single(self._io, self, self._root)

            if self.alpha__staker_tag == AlphaUnstakedFrozenStaker.AlphaStakerTag.shared:
                self.shared = AlphaUnstakedFrozenStaker.PublicKeyHash(self._io, self, self._root)



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(AlphaUnstakedFrozenStaker.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == AlphaUnstakedFrozenStaker.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaUnstakedFrozenStaker.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaUnstakedFrozenStaker.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaUnstakedFrozenStaker.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)




