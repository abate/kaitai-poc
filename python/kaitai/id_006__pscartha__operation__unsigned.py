# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import operation__shell_header
from kaitai import block_header__shell
class Id006PscarthaOperationUnsigned(KaitaiStruct):
    """Encoding id: 006-PsCARTHA.operation.unsigned."""

    class Id006PscarthaOperationAlphaContentsTag(Enum):
        endorsement = 0
        seed_nonce_revelation = 1
        double_endorsement_evidence = 2
        double_baking_evidence = 3
        activate_account = 4
        proposals = 5
        ballot = 6
        reveal = 107
        transaction = 108
        origination = 109
        delegation = 110

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id006PscarthaEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255

    class Id006PscarthaInlinedEndorsementContentsTag(Enum):
        endorsement = 0

    class Id006PscarthaContractIdTag(Enum):
        implicit = 0
        originated = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_006__pscartha__operation__alpha__unsigned_operation = Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaUnsignedOperation(self._io, self, self._root)

    class Op20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_0/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id006PscarthaOperationUnsigned.Op2(_io__raw_op2, self, self._root)


    class Id006PscarthaBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__block_header__alpha__unsigned_contents = Id006PscarthaOperationUnsigned.Id006PscarthaBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes(64)


    class ActivateAccount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = self._io.read_bytes(20)
            self.secret = self._io.read_bytes(20)


    class DoubleEndorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id006PscarthaOperationUnsigned.Op10(self._io, self, self._root)
            self.op2 = Id006PscarthaOperationUnsigned.Op20(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id006PscarthaOperationUnsigned.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Proposals0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_proposals = self._io.read_u4be()
            if not self.len_proposals <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_proposals, self._io, u"/types/proposals_0/seq/0")
            self._raw_proposals = self._io.read_bytes(self.len_proposals)
            _io__raw_proposals = KaitaiStream(BytesIO(self._raw_proposals))
            self.proposals = Id006PscarthaOperationUnsigned.Proposals(_io__raw_proposals, self, self._root)


    class Id006PscarthaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__contract_id_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationUnsigned.Id006PscarthaContractIdTag, self._io.read_u1())
            if self.id_006__pscartha__contract_id_tag == Id006PscarthaOperationUnsigned.Id006PscarthaContractIdTag.implicit:
                self.implicit = Id006PscarthaOperationUnsigned.PublicKeyHash(self._io, self, self._root)

            if self.id_006__pscartha__contract_id_tag == Id006PscarthaOperationUnsigned.Id006PscarthaContractIdTag.originated:
                self.originated = Id006PscarthaOperationUnsigned.Originated(self._io, self, self._root)



    class Reveal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id006PscarthaOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id006PscarthaOperationUnsigned.Id006PscarthaMutez(self._io, self, self._root)
            self.counter = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)
            self.public_key = Id006PscarthaOperationUnsigned.PublicKey(self._io, self, self._root)


    class Id006PscarthaBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.priority = self._io.read_u2be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationUnsigned.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id006PscarthaOperationUnsigned.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)



    class SeedNonceRevelation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.nonce = self._io.read_bytes(32)


    class Id006PscarthaInlinedEndorsementContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__inlined__endorsement__contents_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationUnsigned.Id006PscarthaInlinedEndorsementContentsTag, self._io.read_u1())
            if self.id_006__pscartha__inlined__endorsement__contents_tag == Id006PscarthaOperationUnsigned.Id006PscarthaInlinedEndorsementContentsTag.endorsement:
                self.endorsement = self._io.read_s4be()



    class DoubleBakingEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bh1 = Id006PscarthaOperationUnsigned.Bh10(self._io, self, self._root)
            self.bh2 = Id006PscarthaOperationUnsigned.Bh20(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationUnsigned.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id006PscarthaOperationUnsigned.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id006PscarthaOperationUnsigned.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id006PscarthaOperationUnsigned.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Bh2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__block_header__alpha__full_header = Id006PscarthaOperationUnsigned.Id006PscarthaBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Id006PscarthaInlinedEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__inlined__endorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id006PscarthaOperationUnsigned.Id006PscarthaInlinedEndorsementContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationUnsigned.Bool, self._io.read_u1())
            if self.signature_tag == Id006PscarthaOperationUnsigned.Bool.true:
                self.signature = self._io.read_bytes(64)



    class Bh20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh2 = self._io.read_u4be()
            if not self.len_bh2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh2, self._io, u"/types/bh2_0/seq/0")
            self._raw_bh2 = self._io.read_bytes(self.len_bh2)
            _io__raw_bh2 = KaitaiStream(BytesIO(self._raw_bh2))
            self.bh2 = Id006PscarthaOperationUnsigned.Bh2(_io__raw_bh2, self, self._root)


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id006PscarthaOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id006PscarthaOperationUnsigned.Id006PscarthaMutez(self._io, self, self._root)
            self.counter = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationUnsigned.Bool, self._io.read_u1())
            if self.delegate_tag == Id006PscarthaOperationUnsigned.Bool.true:
                self.delegate = Id006PscarthaOperationUnsigned.PublicKeyHash(self._io, self, self._root)



    class ContentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__operation__alpha__contents = Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContents(self._io, self, self._root)


    class Bh10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh1 = self._io.read_u4be()
            if not self.len_bh1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh1, self._io, u"/types/bh1_0/seq/0")
            self._raw_bh1 = self._io.read_bytes(self.len_bh1)
            _io__raw_bh1 = KaitaiStream(BytesIO(self._raw_bh1))
            self.bh1 = Id006PscarthaOperationUnsigned.Bh1(_io__raw_bh1, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Bh1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__block_header__alpha__full_header = Id006PscarthaOperationUnsigned.Id006PscarthaBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Op10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_0/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id006PscarthaOperationUnsigned.Op1(_io__raw_op1, self, self._root)


    class Id006PscarthaEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__entrypoint_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationUnsigned.Id006PscarthaEntrypointTag, self._io.read_u1())
            if self.id_006__pscartha__entrypoint_tag == Id006PscarthaOperationUnsigned.Id006PscarthaEntrypointTag.named:
                self.named = Id006PscarthaOperationUnsigned.Named0(self._io, self, self._root)



    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id006PscarthaOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id006PscarthaOperationUnsigned.Id006PscarthaMutez(self._io, self, self._root)
            self.counter = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)
            self.balance = Id006PscarthaOperationUnsigned.Id006PscarthaMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationUnsigned.Bool, self._io.read_u1())
            if self.delegate_tag == Id006PscarthaOperationUnsigned.Bool.true:
                self.delegate = Id006PscarthaOperationUnsigned.PublicKeyHash(self._io, self, self._root)

            self.script = Id006PscarthaOperationUnsigned.Id006PscarthaScriptedContracts(self._io, self, self._root)


    class Id006PscarthaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__mutez = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)


    class Op2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__inlined__endorsement = Id006PscarthaOperationUnsigned.Id006PscarthaInlinedEndorsement(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Id006PscarthaBlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_006__pscartha__block_header__alpha__signed_contents = Id006PscarthaOperationUnsigned.Id006PscarthaBlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id006PscarthaOperationUnsigned.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id006PscarthaOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id006PscarthaOperationUnsigned.Id006PscarthaMutez(self._io, self, self._root)
            self.counter = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id006PscarthaOperationUnsigned.N(self._io, self, self._root)
            self.amount = Id006PscarthaOperationUnsigned.Id006PscarthaMutez(self._io, self, self._root)
            self.destination = Id006PscarthaOperationUnsigned.Id006PscarthaContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationUnsigned.Bool, self._io.read_u1())
            if self.parameters_tag == Id006PscarthaOperationUnsigned.Bool.true:
                self.parameters = Id006PscarthaOperationUnsigned.Parameters(self._io, self, self._root)



    class Id006PscarthaScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id006PscarthaOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.storage = Id006PscarthaOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id006PscarthaOperationUnsigned.Id006PscarthaEntrypoint(self._io, self, self._root)
            self.value = Id006PscarthaOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Proposals1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id006PscarthaOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposals = Id006PscarthaOperationUnsigned.Proposals0(self._io, self, self._root)


    class Id006PscarthaOperationAlphaUnsignedOperation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__operation__alpha__unsigned_operation = operation__shell_header.OperationShellHeader(self._io)
            self.contents = []
            i = 0
            while not self._io.is_eof():
                self.contents.append(Id006PscarthaOperationUnsigned.ContentsEntries(self._io, self, self._root))
                i += 1



    class Ballot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id006PscarthaOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposal = self._io.read_bytes(32)
            self.ballot = self._io.read_s1()


    class Id006PscarthaOperationAlphaContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__operation__alpha__contents_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContentsTag, self._io.read_u1())
            if self.id_006__pscartha__operation__alpha__contents_tag == Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContentsTag.endorsement:
                self.endorsement = self._io.read_s4be()

            if self.id_006__pscartha__operation__alpha__contents_tag == Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContentsTag.seed_nonce_revelation:
                self.seed_nonce_revelation = Id006PscarthaOperationUnsigned.SeedNonceRevelation(self._io, self, self._root)

            if self.id_006__pscartha__operation__alpha__contents_tag == Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContentsTag.double_endorsement_evidence:
                self.double_endorsement_evidence = Id006PscarthaOperationUnsigned.DoubleEndorsementEvidence(self._io, self, self._root)

            if self.id_006__pscartha__operation__alpha__contents_tag == Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContentsTag.double_baking_evidence:
                self.double_baking_evidence = Id006PscarthaOperationUnsigned.DoubleBakingEvidence(self._io, self, self._root)

            if self.id_006__pscartha__operation__alpha__contents_tag == Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContentsTag.activate_account:
                self.activate_account = Id006PscarthaOperationUnsigned.ActivateAccount(self._io, self, self._root)

            if self.id_006__pscartha__operation__alpha__contents_tag == Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContentsTag.proposals:
                self.proposals = Id006PscarthaOperationUnsigned.Proposals1(self._io, self, self._root)

            if self.id_006__pscartha__operation__alpha__contents_tag == Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContentsTag.ballot:
                self.ballot = Id006PscarthaOperationUnsigned.Ballot(self._io, self, self._root)

            if self.id_006__pscartha__operation__alpha__contents_tag == Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContentsTag.reveal:
                self.reveal = Id006PscarthaOperationUnsigned.Reveal(self._io, self, self._root)

            if self.id_006__pscartha__operation__alpha__contents_tag == Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContentsTag.transaction:
                self.transaction = Id006PscarthaOperationUnsigned.Transaction(self._io, self, self._root)

            if self.id_006__pscartha__operation__alpha__contents_tag == Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContentsTag.origination:
                self.origination = Id006PscarthaOperationUnsigned.Origination(self._io, self, self._root)

            if self.id_006__pscartha__operation__alpha__contents_tag == Id006PscarthaOperationUnsigned.Id006PscarthaOperationAlphaContentsTag.delegation:
                self.delegation = Id006PscarthaOperationUnsigned.Delegation(self._io, self, self._root)



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationUnsigned.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id006PscarthaOperationUnsigned.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id006PscarthaOperationUnsigned.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id006PscarthaOperationUnsigned.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Proposals(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proposals_entries = []
            i = 0
            while not self._io.is_eof():
                self.proposals_entries.append(Id006PscarthaOperationUnsigned.ProposalsEntries(self._io, self, self._root))
                i += 1



    class ProposalsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol_hash = self._io.read_bytes(32)


    class Op1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__inlined__endorsement = Id006PscarthaOperationUnsigned.Id006PscarthaInlinedEndorsement(self._io, self, self._root)



