# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id007Psdelph1DelegateBalanceUpdates(KaitaiStruct):
    """Encoding id: 007-PsDELPH1.delegate.balance_updates."""

    class Id007Psdelph1ContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id007Psdelph1OperationMetadataAlphaBalanceTag(Enum):
        contract = 0
        rewards = 1
        fees = 2
        deposits = 3

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_007__psdelph1__operation_metadata__alpha__balance_updates = Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Id007Psdelph1OperationMetadataAlphaBalance(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_007__psdelph1__operation_metadata__alpha__balance_tag = KaitaiStream.resolve_enum(Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceTag, self._io.read_u1())
            if self.id_007__psdelph1__operation_metadata__alpha__balance_tag == Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceTag.contract:
                self.contract = Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1ContractId(self._io, self, self._root)

            if self.id_007__psdelph1__operation_metadata__alpha__balance_tag == Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceTag.rewards:
                self.rewards = Id007Psdelph1DelegateBalanceUpdates.Rewards(self._io, self, self._root)

            if self.id_007__psdelph1__operation_metadata__alpha__balance_tag == Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceTag.fees:
                self.fees = Id007Psdelph1DelegateBalanceUpdates.Fees(self._io, self, self._root)

            if self.id_007__psdelph1__operation_metadata__alpha__balance_tag == Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceTag.deposits:
                self.deposits = Id007Psdelph1DelegateBalanceUpdates.Deposits(self._io, self, self._root)



    class Id007Psdelph1ContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_007__psdelph1__contract_id_tag = KaitaiStream.resolve_enum(Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1ContractIdTag, self._io.read_u1())
            if self.id_007__psdelph1__contract_id_tag == Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1ContractIdTag.implicit:
                self.implicit = Id007Psdelph1DelegateBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_007__psdelph1__contract_id_tag == Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1ContractIdTag.originated:
                self.originated = Id007Psdelph1DelegateBalanceUpdates.Originated(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id007Psdelph1OperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_007__psdelph1__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_007__psdelph1__operation_metadata__alpha__balance_updates_entries.append(Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class Rewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id007Psdelph1DelegateBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Deposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id007Psdelph1DelegateBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id007Psdelph1OperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_007__psdelph1__operation_metadata__alpha__balance = Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalance(self._io, self, self._root)
            self.id_007__psdelph1__operation_metadata__alpha__balance_update = Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceUpdate(self._io, self, self._root)


    class Id007Psdelph1OperationMetadataAlphaBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Fees(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id007Psdelph1DelegateBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id007Psdelph1DelegateBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id007Psdelph1DelegateBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id007Psdelph1DelegateBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id007Psdelph1DelegateBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Id007Psdelph1OperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_007__psdelph1__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_007__psdelph1__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_007__psdelph1__operation_metadata__alpha__balance_updates, self._io, u"/types/id_007__psdelph1__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_007__psdelph1__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_007__psdelph1__operation_metadata__alpha__balance_updates)
            _io__raw_id_007__psdelph1__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_007__psdelph1__operation_metadata__alpha__balance_updates))
            self.id_007__psdelph1__operation_metadata__alpha__balance_updates = Id007Psdelph1DelegateBalanceUpdates.Id007Psdelph1OperationMetadataAlphaBalanceUpdates(_io__raw_id_007__psdelph1__operation_metadata__alpha__balance_updates, self, self._root)



