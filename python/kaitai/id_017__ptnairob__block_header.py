# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import block_header__shell
class Id017PtnairobBlockHeader(KaitaiStruct):
    """Encoding id: 017-PtNairob.block_header."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_017__ptnairob__block_header__alpha__full_header = Id017PtnairobBlockHeader.Id017PtnairobBlockHeaderAlphaFullHeader(self._io, self, self._root)

    class Id017PtnairobBlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_017__ptnairob__block_header__alpha__signed_contents = Id017PtnairobBlockHeader.Id017PtnairobBlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Id017PtnairobBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__block_header__alpha__unsigned_contents = Id017PtnairobBlockHeader.Id017PtnairobBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes_full()


    class Id017PtnairobBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_hash = self._io.read_bytes(32)
            self.payload_round = self._io.read_s4be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id017PtnairobBlockHeader.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id017PtnairobBlockHeader.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.liquidity_baking_toggle_vote = Id017PtnairobBlockHeader.Id017PtnairobLiquidityBakingToggleVote(self._io, self, self._root)


    class Id017PtnairobLiquidityBakingToggleVote(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__liquidity_baking_toggle_vote = self._io.read_s1()



