# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import operation__shell_header
from kaitai import block_header__shell
class Id005Psbabym1OperationContentsList(KaitaiStruct):
    """Encoding id: 005-PsBabyM1.operation.contents_list."""

    class Id005Psbabym1InlinedEndorsementContentsTag(Enum):
        endorsement = 0

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Id005Psbabym1EntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id005Psbabym1ContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id005Psbabym1OperationAlphaContentsTag(Enum):
        endorsement = 0
        seed_nonce_revelation = 1
        double_endorsement_evidence = 2
        double_baking_evidence = 3
        activate_account = 4
        proposals = 5
        ballot = 6
        reveal = 107
        transaction = 108
        origination = 109
        delegation = 110
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_005__psbabym1__operation__contents_list_entries = []
        i = 0
        while not self._io.is_eof():
            self.id_005__psbabym1__operation__contents_list_entries.append(Id005Psbabym1OperationContentsList.Id005Psbabym1OperationContentsListEntries(self._io, self, self._root))
            i += 1


    class Op20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_0/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id005Psbabym1OperationContentsList.Op2(_io__raw_op2, self, self._root)


    class Id005Psbabym1Entrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__entrypoint_tag = KaitaiStream.resolve_enum(Id005Psbabym1OperationContentsList.Id005Psbabym1EntrypointTag, self._io.read_u1())
            if self.id_005__psbabym1__entrypoint_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1EntrypointTag.named:
                self.named = Id005Psbabym1OperationContentsList.Named0(self._io, self, self._root)



    class ActivateAccount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = self._io.read_bytes(20)
            self.secret = self._io.read_bytes(20)


    class DoubleEndorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id005Psbabym1OperationContentsList.Op10(self._io, self, self._root)
            self.op2 = Id005Psbabym1OperationContentsList.Op20(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id005Psbabym1OperationContentsList.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Proposals0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_proposals = self._io.read_u4be()
            if not self.len_proposals <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_proposals, self._io, u"/types/proposals_0/seq/0")
            self._raw_proposals = self._io.read_bytes(self.len_proposals)
            _io__raw_proposals = KaitaiStream(BytesIO(self._raw_proposals))
            self.proposals = Id005Psbabym1OperationContentsList.Proposals(_io__raw_proposals, self, self._root)


    class Reveal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id005Psbabym1OperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id005Psbabym1OperationContentsList.Id005Psbabym1Mutez(self._io, self, self._root)
            self.counter = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)
            self.public_key = Id005Psbabym1OperationContentsList.PublicKey(self._io, self, self._root)


    class Id005Psbabym1Mutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__mutez = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)


    class Id005Psbabym1OperationAlphaContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__operation__alpha__contents_tag = KaitaiStream.resolve_enum(Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContentsTag, self._io.read_u1())
            if self.id_005__psbabym1__operation__alpha__contents_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContentsTag.endorsement:
                self.endorsement = self._io.read_s4be()

            if self.id_005__psbabym1__operation__alpha__contents_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContentsTag.seed_nonce_revelation:
                self.seed_nonce_revelation = Id005Psbabym1OperationContentsList.SeedNonceRevelation(self._io, self, self._root)

            if self.id_005__psbabym1__operation__alpha__contents_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContentsTag.double_endorsement_evidence:
                self.double_endorsement_evidence = Id005Psbabym1OperationContentsList.DoubleEndorsementEvidence(self._io, self, self._root)

            if self.id_005__psbabym1__operation__alpha__contents_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContentsTag.double_baking_evidence:
                self.double_baking_evidence = Id005Psbabym1OperationContentsList.DoubleBakingEvidence(self._io, self, self._root)

            if self.id_005__psbabym1__operation__alpha__contents_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContentsTag.activate_account:
                self.activate_account = Id005Psbabym1OperationContentsList.ActivateAccount(self._io, self, self._root)

            if self.id_005__psbabym1__operation__alpha__contents_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContentsTag.proposals:
                self.proposals = Id005Psbabym1OperationContentsList.Proposals1(self._io, self, self._root)

            if self.id_005__psbabym1__operation__alpha__contents_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContentsTag.ballot:
                self.ballot = Id005Psbabym1OperationContentsList.Ballot(self._io, self, self._root)

            if self.id_005__psbabym1__operation__alpha__contents_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContentsTag.reveal:
                self.reveal = Id005Psbabym1OperationContentsList.Reveal(self._io, self, self._root)

            if self.id_005__psbabym1__operation__alpha__contents_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContentsTag.transaction:
                self.transaction = Id005Psbabym1OperationContentsList.Transaction(self._io, self, self._root)

            if self.id_005__psbabym1__operation__alpha__contents_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContentsTag.origination:
                self.origination = Id005Psbabym1OperationContentsList.Origination(self._io, self, self._root)

            if self.id_005__psbabym1__operation__alpha__contents_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContentsTag.delegation:
                self.delegation = Id005Psbabym1OperationContentsList.Delegation(self._io, self, self._root)



    class SeedNonceRevelation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.nonce = self._io.read_bytes(32)


    class Id005Psbabym1InlinedEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__inlined__endorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id005Psbabym1OperationContentsList.Id005Psbabym1InlinedEndorsementContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id005Psbabym1OperationContentsList.Bool, self._io.read_u1())
            if self.signature_tag == Id005Psbabym1OperationContentsList.Bool.true:
                self.signature = self._io.read_bytes(64)



    class DoubleBakingEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bh1 = Id005Psbabym1OperationContentsList.Bh10(self._io, self, self._root)
            self.bh2 = Id005Psbabym1OperationContentsList.Bh20(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id005Psbabym1OperationContentsList.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id005Psbabym1OperationContentsList.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id005Psbabym1OperationContentsList.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id005Psbabym1OperationContentsList.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Id005Psbabym1BlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.priority = self._io.read_u2be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id005Psbabym1OperationContentsList.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id005Psbabym1OperationContentsList.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)



    class Bh2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__block_header__alpha__full_header = Id005Psbabym1OperationContentsList.Id005Psbabym1BlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Id005Psbabym1OperationContentsListEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__operation__alpha__contents = Id005Psbabym1OperationContentsList.Id005Psbabym1OperationAlphaContents(self._io, self, self._root)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Bh20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh2 = self._io.read_u4be()
            if not self.len_bh2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh2, self._io, u"/types/bh2_0/seq/0")
            self._raw_bh2 = self._io.read_bytes(self.len_bh2)
            _io__raw_bh2 = KaitaiStream(BytesIO(self._raw_bh2))
            self.bh2 = Id005Psbabym1OperationContentsList.Bh2(_io__raw_bh2, self, self._root)


    class Id005Psbabym1InlinedEndorsementContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__inlined__endorsement__contents_tag = KaitaiStream.resolve_enum(Id005Psbabym1OperationContentsList.Id005Psbabym1InlinedEndorsementContentsTag, self._io.read_u1())
            if self.id_005__psbabym1__inlined__endorsement__contents_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1InlinedEndorsementContentsTag.endorsement:
                self.endorsement = self._io.read_s4be()



    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id005Psbabym1OperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id005Psbabym1OperationContentsList.Id005Psbabym1Mutez(self._io, self, self._root)
            self.counter = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id005Psbabym1OperationContentsList.Bool, self._io.read_u1())
            if self.delegate_tag == Id005Psbabym1OperationContentsList.Bool.true:
                self.delegate = Id005Psbabym1OperationContentsList.PublicKeyHash(self._io, self, self._root)



    class Bh10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh1 = self._io.read_u4be()
            if not self.len_bh1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh1, self._io, u"/types/bh1_0/seq/0")
            self._raw_bh1 = self._io.read_bytes(self.len_bh1)
            _io__raw_bh1 = KaitaiStream(BytesIO(self._raw_bh1))
            self.bh1 = Id005Psbabym1OperationContentsList.Bh1(_io__raw_bh1, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Bh1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__block_header__alpha__full_header = Id005Psbabym1OperationContentsList.Id005Psbabym1BlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Op10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_0/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id005Psbabym1OperationContentsList.Op1(_io__raw_op1, self, self._root)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id005Psbabym1OperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id005Psbabym1OperationContentsList.Id005Psbabym1Mutez(self._io, self, self._root)
            self.counter = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)
            self.balance = Id005Psbabym1OperationContentsList.Id005Psbabym1Mutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id005Psbabym1OperationContentsList.Bool, self._io.read_u1())
            if self.delegate_tag == Id005Psbabym1OperationContentsList.Bool.true:
                self.delegate = Id005Psbabym1OperationContentsList.PublicKeyHash(self._io, self, self._root)

            self.script = Id005Psbabym1OperationContentsList.Id005Psbabym1ScriptedContracts(self._io, self, self._root)


    class Id005Psbabym1ScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id005Psbabym1OperationContentsList.BytesDynUint30(self._io, self, self._root)
            self.storage = Id005Psbabym1OperationContentsList.BytesDynUint30(self._io, self, self._root)


    class Op2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__inlined__endorsement = Id005Psbabym1OperationContentsList.Id005Psbabym1InlinedEndorsement(self._io, self, self._root)


    class Id005Psbabym1ContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__contract_id_tag = KaitaiStream.resolve_enum(Id005Psbabym1OperationContentsList.Id005Psbabym1ContractIdTag, self._io.read_u1())
            if self.id_005__psbabym1__contract_id_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1ContractIdTag.implicit:
                self.implicit = Id005Psbabym1OperationContentsList.PublicKeyHash(self._io, self, self._root)

            if self.id_005__psbabym1__contract_id_tag == Id005Psbabym1OperationContentsList.Id005Psbabym1ContractIdTag.originated:
                self.originated = Id005Psbabym1OperationContentsList.Originated(self._io, self, self._root)



    class Id005Psbabym1BlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_005__psbabym1__block_header__alpha__signed_contents = Id005Psbabym1OperationContentsList.Id005Psbabym1BlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Id005Psbabym1BlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__block_header__alpha__unsigned_contents = Id005Psbabym1OperationContentsList.Id005Psbabym1BlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes(64)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id005Psbabym1OperationContentsList.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id005Psbabym1OperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id005Psbabym1OperationContentsList.Id005Psbabym1Mutez(self._io, self, self._root)
            self.counter = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id005Psbabym1OperationContentsList.N(self._io, self, self._root)
            self.amount = Id005Psbabym1OperationContentsList.Id005Psbabym1Mutez(self._io, self, self._root)
            self.destination = Id005Psbabym1OperationContentsList.Id005Psbabym1ContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id005Psbabym1OperationContentsList.Bool, self._io.read_u1())
            if self.parameters_tag == Id005Psbabym1OperationContentsList.Bool.true:
                self.parameters = Id005Psbabym1OperationContentsList.Parameters(self._io, self, self._root)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id005Psbabym1OperationContentsList.Id005Psbabym1Entrypoint(self._io, self, self._root)
            self.value = Id005Psbabym1OperationContentsList.BytesDynUint30(self._io, self, self._root)


    class Proposals1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id005Psbabym1OperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposals = Id005Psbabym1OperationContentsList.Proposals0(self._io, self, self._root)


    class Ballot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id005Psbabym1OperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposal = self._io.read_bytes(32)
            self.ballot = self._io.read_s1()


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id005Psbabym1OperationContentsList.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id005Psbabym1OperationContentsList.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id005Psbabym1OperationContentsList.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id005Psbabym1OperationContentsList.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Proposals(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proposals_entries = []
            i = 0
            while not self._io.is_eof():
                self.proposals_entries.append(Id005Psbabym1OperationContentsList.ProposalsEntries(self._io, self, self._root))
                i += 1



    class ProposalsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol_hash = self._io.read_bytes(32)


    class Op1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__inlined__endorsement = Id005Psbabym1OperationContentsList.Id005Psbabym1InlinedEndorsement(self._io, self, self._root)



