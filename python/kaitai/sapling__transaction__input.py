# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import sapling__transaction__nullifier
from kaitai import sapling__transaction__commitment_value
class SaplingTransactionInput(KaitaiStruct):
    """Encoding id: sapling.transaction.input
    Description: Input of a transaction."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.cv = sapling__transaction__commitment_value.SaplingTransactionCommitmentValue(self._io)
        self.nf = sapling__transaction__nullifier.SaplingTransactionNullifier(self._io)
        self.rk = self._io.read_bytes(32)
        self.proof_i = self._io.read_bytes(192)
        self.signature = self._io.read_bytes(64)


