# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id012PsithacaConstants(KaitaiStruct):
    """Encoding id: 012-Psithaca.constants."""

    class DelegateSelectionTag(Enum):
        random_delegate_selection = 0
        round_robin_over_delegates = 1

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.proof_of_work_nonce_size = self._io.read_u1()
        self.nonce_length = self._io.read_u1()
        self.max_anon_ops_per_block = self._io.read_u1()
        self.max_operation_data_length = Id012PsithacaConstants.Int31(self._io, self, self._root)
        self.max_proposals_per_delegate = self._io.read_u1()
        self.max_micheline_node_count = Id012PsithacaConstants.Int31(self._io, self, self._root)
        self.max_micheline_bytes_limit = Id012PsithacaConstants.Int31(self._io, self, self._root)
        self.max_allowed_global_constants_depth = Id012PsithacaConstants.Int31(self._io, self, self._root)
        self.cache_layout = Id012PsithacaConstants.CacheLayout0(self._io, self, self._root)
        self.michelson_maximum_type_size = self._io.read_u2be()
        self.preserved_cycles = self._io.read_u1()
        self.blocks_per_cycle = self._io.read_s4be()
        self.blocks_per_commitment = self._io.read_s4be()
        self.blocks_per_stake_snapshot = self._io.read_s4be()
        self.blocks_per_voting_period = self._io.read_s4be()
        self.hard_gas_limit_per_operation = Id012PsithacaConstants.Z(self._io, self, self._root)
        self.hard_gas_limit_per_block = Id012PsithacaConstants.Z(self._io, self, self._root)
        self.proof_of_work_threshold = self._io.read_s8be()
        self.tokens_per_roll = Id012PsithacaConstants.Id012PsithacaMutez(self._io, self, self._root)
        self.seed_nonce_revelation_tip = Id012PsithacaConstants.Id012PsithacaMutez(self._io, self, self._root)
        self.origination_size = Id012PsithacaConstants.Int31(self._io, self, self._root)
        self.baking_reward_fixed_portion = Id012PsithacaConstants.Id012PsithacaMutez(self._io, self, self._root)
        self.baking_reward_bonus_per_slot = Id012PsithacaConstants.Id012PsithacaMutez(self._io, self, self._root)
        self.endorsing_reward_per_slot = Id012PsithacaConstants.Id012PsithacaMutez(self._io, self, self._root)
        self.cost_per_byte = Id012PsithacaConstants.Id012PsithacaMutez(self._io, self, self._root)
        self.hard_storage_limit_per_operation = Id012PsithacaConstants.Z(self._io, self, self._root)
        self.quorum_min = self._io.read_s4be()
        self.quorum_max = self._io.read_s4be()
        self.min_proposal_quorum = self._io.read_s4be()
        self.liquidity_baking_subsidy = Id012PsithacaConstants.Id012PsithacaMutez(self._io, self, self._root)
        self.liquidity_baking_sunset_level = self._io.read_s4be()
        self.liquidity_baking_escape_ema_threshold = self._io.read_s4be()
        self.max_operations_time_to_live = self._io.read_s2be()
        self.minimal_block_delay = self._io.read_s8be()
        self.delay_increment_per_round = self._io.read_s8be()
        self.consensus_committee_size = Id012PsithacaConstants.Int31(self._io, self, self._root)
        self.consensus_threshold = Id012PsithacaConstants.Int31(self._io, self, self._root)
        self.minimal_participation_ratio = Id012PsithacaConstants.MinimalParticipationRatio(self._io, self, self._root)
        self.max_slashing_period = Id012PsithacaConstants.Int31(self._io, self, self._root)
        self.frozen_deposits_percentage = Id012PsithacaConstants.Int31(self._io, self, self._root)
        self.double_baking_punishment = Id012PsithacaConstants.Id012PsithacaMutez(self._io, self, self._root)
        self.ratio_of_frozen_deposits_slashed_per_double_endorsement = Id012PsithacaConstants.RatioOfFrozenDepositsSlashedPerDoubleEndorsement(self._io, self, self._root)
        self.delegate_selection = Id012PsithacaConstants.DelegateSelection(self._io, self, self._root)

    class RatioOfFrozenDepositsSlashedPerDoubleEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id012PsithacaConstants.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class RoundRobinOverDelegatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_round_robin_over_delegates_elt = self._io.read_u4be()
            if not self.len_round_robin_over_delegates_elt <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_round_robin_over_delegates_elt, self._io, u"/types/round_robin_over_delegates_entries/seq/0")
            self._raw_round_robin_over_delegates_elt = self._io.read_bytes(self.len_round_robin_over_delegates_elt)
            _io__raw_round_robin_over_delegates_elt = KaitaiStream(BytesIO(self._raw_round_robin_over_delegates_elt))
            self.round_robin_over_delegates_elt = Id012PsithacaConstants.RoundRobinOverDelegatesElt(_io__raw_round_robin_over_delegates_elt, self, self._root)


    class RoundRobinOverDelegates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.round_robin_over_delegates_entries = []
            i = 0
            while not self._io.is_eof():
                self.round_robin_over_delegates_entries.append(Id012PsithacaConstants.RoundRobinOverDelegatesEntries(self._io, self, self._root))
                i += 1



    class CacheLayoutEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.cache_layout_elt = self._io.read_s8be()


    class RoundRobinOverDelegatesElt(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.round_robin_over_delegates_elt_entries = []
            i = 0
            while not self._io.is_eof():
                self.round_robin_over_delegates_elt_entries.append(Id012PsithacaConstants.RoundRobinOverDelegatesEltEntries(self._io, self, self._root))
                i += 1



    class CacheLayout0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_cache_layout = self._io.read_u4be()
            if not self.len_cache_layout <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_cache_layout, self._io, u"/types/cache_layout_0/seq/0")
            self._raw_cache_layout = self._io.read_bytes(self.len_cache_layout)
            _io__raw_cache_layout = KaitaiStream(BytesIO(self._raw_cache_layout))
            self.cache_layout = Id012PsithacaConstants.CacheLayout(_io__raw_cache_layout, self, self._root)


    class Id012PsithacaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__mutez = Id012PsithacaConstants.N(self._io, self, self._root)


    class RoundRobinOverDelegates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_round_robin_over_delegates = self._io.read_u4be()
            if not self.len_round_robin_over_delegates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_round_robin_over_delegates, self._io, u"/types/round_robin_over_delegates_0/seq/0")
            self._raw_round_robin_over_delegates = self._io.read_bytes(self.len_round_robin_over_delegates)
            _io__raw_round_robin_over_delegates = KaitaiStream(BytesIO(self._raw_round_robin_over_delegates))
            self.round_robin_over_delegates = Id012PsithacaConstants.RoundRobinOverDelegates(_io__raw_round_robin_over_delegates, self, self._root)


    class CacheLayout(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.cache_layout_entries = []
            i = 0
            while not self._io.is_eof():
                self.cache_layout_entries.append(Id012PsithacaConstants.CacheLayoutEntries(self._io, self, self._root))
                i += 1



    class MinimalParticipationRatio(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id012PsithacaConstants.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id012PsithacaConstants.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id012PsithacaConstants.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id012PsithacaConstants.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class DelegateSelection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_selection_tag = KaitaiStream.resolve_enum(Id012PsithacaConstants.DelegateSelectionTag, self._io.read_u1())
            if self.delegate_selection_tag == Id012PsithacaConstants.DelegateSelectionTag.round_robin_over_delegates:
                self.round_robin_over_delegates = Id012PsithacaConstants.RoundRobinOverDelegates0(self._io, self, self._root)



    class RoundRobinOverDelegatesEltEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.signature__v0__public_key = Id012PsithacaConstants.PublicKey(self._io, self, self._root)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id012PsithacaConstants.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




