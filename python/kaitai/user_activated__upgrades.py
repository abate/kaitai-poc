# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class UserActivatedUpgrades(KaitaiStruct):
    """Encoding id: user_activated.upgrades
    Description: User activated upgrades: at given level, switch to given protocol."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.len_user_activated__upgrades = self._io.read_u4be()
        if not self.len_user_activated__upgrades <= 1073741823:
            raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_user_activated__upgrades, self._io, u"/seq/0")
        self._raw_user_activated__upgrades = self._io.read_bytes(self.len_user_activated__upgrades)
        _io__raw_user_activated__upgrades = KaitaiStream(BytesIO(self._raw_user_activated__upgrades))
        self.user_activated__upgrades = UserActivatedUpgrades.UserActivatedUpgrades(_io__raw_user_activated__upgrades, self, self._root)

    class UserActivatedUpgrades(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.user_activated__upgrades_entries = []
            i = 0
            while not self._io.is_eof():
                self.user_activated__upgrades_entries.append(UserActivatedUpgrades.UserActivatedUpgradesEntries(self._io, self, self._root))
                i += 1



    class UserActivatedUpgradesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.replacement_protocol = self._io.read_bytes(32)



