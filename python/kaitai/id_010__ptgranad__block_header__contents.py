# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id010PtgranadBlockHeaderContents(KaitaiStruct):
    """Encoding id: 010-PtGRANAD.block_header.contents."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_010__ptgranad__block_header__alpha__unsigned_contents = Id010PtgranadBlockHeaderContents.Id010PtgranadBlockHeaderAlphaUnsignedContents(self._io, self, self._root)

    class Id010PtgranadBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.priority = self._io.read_u2be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id010PtgranadBlockHeaderContents.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id010PtgranadBlockHeaderContents.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.liquidity_baking_escape_vote = KaitaiStream.resolve_enum(Id010PtgranadBlockHeaderContents.Bool, self._io.read_u1())



