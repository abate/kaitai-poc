# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import sapling__transaction__nullifier
from kaitai import sapling__transaction__commitment
from kaitai import sapling__transaction__ciphertext
class Id015PtlimaptLazyStorageDiff(KaitaiStruct):
    """Encoding id: 015-PtLimaPt.lazy_storage_diff."""

    class DiffTag(Enum):
        update = 0
        remove = 1
        copy = 2
        alloc = 3

    class Micheline015PtlimaptMichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10

    class Bool(Enum):
        false = 0
        true = 255

    class Id015PtlimaptMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left = 5
        none_0 = 6
        pair_1 = 7
        right = 8
        some_0 = 9
        true = 10
        unit_0 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left_0 = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right_0 = 68
        size = 69
        some = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_1 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction_deprecated = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_deprecated = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140
        chest = 141
        chest_key = 142
        open_chest = 143
        view_0 = 144
        view = 145
        constant = 146
        sub_mutez = 147
        tx_rollup_l2_address = 148
        min_block_time = 149
        sapling_transaction = 150
        emit = 151
        lambda_rec = 152
        lambda_rec_0 = 153
        ticket_0 = 154

    class Id015PtlimaptLazyStorageDiffEltTag(Enum):
        big_map = 0
        sapling_state = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.len_id_015__ptlimapt__lazy_storage_diff = self._io.read_u4be()
        if not self.len_id_015__ptlimapt__lazy_storage_diff <= 1073741823:
            raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_015__ptlimapt__lazy_storage_diff, self._io, u"/seq/0")
        self._raw_id_015__ptlimapt__lazy_storage_diff = self._io.read_bytes(self.len_id_015__ptlimapt__lazy_storage_diff)
        _io__raw_id_015__ptlimapt__lazy_storage_diff = KaitaiStream(BytesIO(self._raw_id_015__ptlimapt__lazy_storage_diff))
        self.id_015__ptlimapt__lazy_storage_diff = Id015PtlimaptLazyStorageDiff.Id015PtlimaptLazyStorageDiff(_io__raw_id_015__ptlimapt__lazy_storage_diff, self, self._root)

    class Nullifiers(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.nullifiers_entries = []
            i = 0
            while not self._io.is_eof():
                self.nullifiers_entries.append(Id015PtlimaptLazyStorageDiff.NullifiersEntries(self._io, self, self._root))
                i += 1



    class Alloc(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.updates = Id015PtlimaptLazyStorageDiff.Updates0(self._io, self, self._root)
            self.key_type = Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.value_type = Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptLazyStorageDiff.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id015PtlimaptLazyStorageDiff.BytesDynUint30(self._io, self, self._root)


    class Nullifiers0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_nullifiers = self._io.read_u4be()
            if not self.len_nullifiers <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_nullifiers, self._io, u"/types/nullifiers_0/seq/0")
            self._raw_nullifiers = self._io.read_bytes(self.len_nullifiers)
            _io__raw_nullifiers = KaitaiStream(BytesIO(self._raw_nullifiers))
            self.nullifiers = Id015PtlimaptLazyStorageDiff.Nullifiers(_io__raw_nullifiers, self, self._root)


    class Copy0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptLazyStorageDiff.Id015PtlimaptSaplingStateId(self._io, self, self._root)
            self.updates = Id015PtlimaptLazyStorageDiff.Updates1(self._io, self, self._root)


    class Id015PtlimaptLazyStorageDiffEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__lazy_storage_diff_elt_tag = KaitaiStream.resolve_enum(Id015PtlimaptLazyStorageDiff.Id015PtlimaptLazyStorageDiffEltTag, self._io.read_u1())
            if self.id_015__ptlimapt__lazy_storage_diff_elt_tag == Id015PtlimaptLazyStorageDiff.Id015PtlimaptLazyStorageDiffEltTag.big_map:
                self.big_map = Id015PtlimaptLazyStorageDiff.BigMap(self._io, self, self._root)

            if self.id_015__ptlimapt__lazy_storage_diff_elt_tag == Id015PtlimaptLazyStorageDiff.Id015PtlimaptLazyStorageDiffEltTag.sapling_state:
                self.sapling_state = Id015PtlimaptLazyStorageDiff.SaplingState(self._io, self, self._root)



    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptLazyStorageDiff.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.annots = Id015PtlimaptLazyStorageDiff.BytesDynUint30(self._io, self, self._root)


    class CommitmentsAndCiphertexts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_and_ciphertexts_entries = []
            i = 0
            while not self._io.is_eof():
                self.commitments_and_ciphertexts_entries.append(Id015PtlimaptLazyStorageDiff.CommitmentsAndCiphertextsEntries(self._io, self, self._root))
                i += 1



    class Diff0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.diff_tag = KaitaiStream.resolve_enum(Id015PtlimaptLazyStorageDiff.DiffTag, self._io.read_u1())
            if self.diff_tag == Id015PtlimaptLazyStorageDiff.DiffTag.update:
                self.update = Id015PtlimaptLazyStorageDiff.Updates1(self._io, self, self._root)

            if self.diff_tag == Id015PtlimaptLazyStorageDiff.DiffTag.copy:
                self.copy = Id015PtlimaptLazyStorageDiff.Copy0(self._io, self, self._root)

            if self.diff_tag == Id015PtlimaptLazyStorageDiff.DiffTag.alloc:
                self.alloc = Id015PtlimaptLazyStorageDiff.Alloc0(self._io, self, self._root)



    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptLazyStorageDiff.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.args = Id015PtlimaptLazyStorageDiff.Args0(self._io, self, self._root)
            self.annots = Id015PtlimaptLazyStorageDiff.BytesDynUint30(self._io, self, self._root)


    class SaplingState(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id = Id015PtlimaptLazyStorageDiff.Id015PtlimaptSaplingStateId(self._io, self, self._root)
            self.diff = Id015PtlimaptLazyStorageDiff.Diff0(self._io, self, self._root)


    class Id015PtlimaptLazyStorageDiff(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__lazy_storage_diff_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_015__ptlimapt__lazy_storage_diff_entries.append(Id015PtlimaptLazyStorageDiff.Id015PtlimaptLazyStorageDiffEntries(self._io, self, self._root))
                i += 1



    class CommitmentsAndCiphertexts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_commitments_and_ciphertexts = self._io.read_u4be()
            if not self.len_commitments_and_ciphertexts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_commitments_and_ciphertexts, self._io, u"/types/commitments_and_ciphertexts_0/seq/0")
            self._raw_commitments_and_ciphertexts = self._io.read_bytes(self.len_commitments_and_ciphertexts)
            _io__raw_commitments_and_ciphertexts = KaitaiStream(BytesIO(self._raw_commitments_and_ciphertexts))
            self.commitments_and_ciphertexts = Id015PtlimaptLazyStorageDiff.CommitmentsAndCiphertexts(_io__raw_commitments_and_ciphertexts, self, self._root)


    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = Id015PtlimaptLazyStorageDiff.Sequence(_io__raw_sequence, self, self._root)


    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(Id015PtlimaptLazyStorageDiff.SequenceEntries(self._io, self, self._root))
                i += 1



    class Updates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.updates_entries.append(Id015PtlimaptLazyStorageDiff.UpdatesEntries(self._io, self, self._root))
                i += 1



    class Diff(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.diff_tag = KaitaiStream.resolve_enum(Id015PtlimaptLazyStorageDiff.DiffTag, self._io.read_u1())
            if self.diff_tag == Id015PtlimaptLazyStorageDiff.DiffTag.update:
                self.update = Id015PtlimaptLazyStorageDiff.Updates0(self._io, self, self._root)

            if self.diff_tag == Id015PtlimaptLazyStorageDiff.DiffTag.copy:
                self.copy = Id015PtlimaptLazyStorageDiff.Copy(self._io, self, self._root)

            if self.diff_tag == Id015PtlimaptLazyStorageDiff.DiffTag.alloc:
                self.alloc = Id015PtlimaptLazyStorageDiff.Alloc(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class CommitmentsAndCiphertextsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_and_ciphertexts_elt_field0 = sapling__transaction__commitment.SaplingTransactionCommitment(self._io)
            self.commitments_and_ciphertexts_elt_field1 = sapling__transaction__ciphertext.SaplingTransactionCiphertext(self._io)


    class Updates1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_and_ciphertexts = Id015PtlimaptLazyStorageDiff.CommitmentsAndCiphertexts0(self._io, self, self._root)
            self.nullifiers = Id015PtlimaptLazyStorageDiff.Nullifiers0(self._io, self, self._root)


    class Micheline015PtlimaptMichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__015__ptlimapt__michelson_v1__expression_tag = KaitaiStream.resolve_enum(Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1ExpressionTag.int:
                self.int = Id015PtlimaptLazyStorageDiff.Z(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1ExpressionTag.string:
                self.string = Id015PtlimaptLazyStorageDiff.BytesDynUint30(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1ExpressionTag.sequence:
                self.sequence = Id015PtlimaptLazyStorageDiff.Sequence0(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = Id015PtlimaptLazyStorageDiff.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = Id015PtlimaptLazyStorageDiff.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = Id015PtlimaptLazyStorageDiff.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = Id015PtlimaptLazyStorageDiff.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = Id015PtlimaptLazyStorageDiff.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = Id015PtlimaptLazyStorageDiff.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = Id015PtlimaptLazyStorageDiff.PrimGeneric(self._io, self, self._root)

            if self.micheline__015__ptlimapt__michelson_v1__expression_tag == Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1ExpressionTag.bytes:
                self.bytes = Id015PtlimaptLazyStorageDiff.BytesDynUint30(self._io, self, self._root)



    class Copy(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id015PtlimaptLazyStorageDiff.Id015PtlimaptBigMapId(self._io, self, self._root)
            self.updates = Id015PtlimaptLazyStorageDiff.Updates0(self._io, self, self._root)


    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(Id015PtlimaptLazyStorageDiff.ArgsEntries(self._io, self, self._root))
                i += 1



    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptLazyStorageDiff.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class Id015PtlimaptBigMapId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__big_map_id = Id015PtlimaptLazyStorageDiff.Z(self._io, self, self._root)


    class Id015PtlimaptMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__michelson__v1__primitives = KaitaiStream.resolve_enum(Id015PtlimaptLazyStorageDiff.Id015PtlimaptMichelsonV1Primitives, self._io.read_u1())


    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptLazyStorageDiff.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class Alloc0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.updates = Id015PtlimaptLazyStorageDiff.Updates1(self._io, self, self._root)
            self.memo_size = self._io.read_u2be()


    class NullifiersEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.nullifiers_elt = sapling__transaction__nullifier.SaplingTransactionNullifier(self._io)


    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)


    class BigMap(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id = Id015PtlimaptLazyStorageDiff.Id015PtlimaptBigMapId(self._io, self, self._root)
            self.diff = Id015PtlimaptLazyStorageDiff.Diff(self._io, self, self._root)


    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = Id015PtlimaptLazyStorageDiff.Args(_io__raw_args, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id015PtlimaptLazyStorageDiff.Id015PtlimaptMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id015PtlimaptLazyStorageDiff.BytesDynUint30(self._io, self, self._root)


    class Id015PtlimaptSaplingStateId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__sapling_state_id = Id015PtlimaptLazyStorageDiff.Z(self._io, self, self._root)


    class UpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.key_hash = self._io.read_bytes(32)
            self.key = Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)
            self.value_tag = KaitaiStream.resolve_enum(Id015PtlimaptLazyStorageDiff.Bool, self._io.read_u1())
            if self.value_tag == Id015PtlimaptLazyStorageDiff.Bool.true:
                self.value = Id015PtlimaptLazyStorageDiff.Micheline015PtlimaptMichelsonV1Expression(self._io, self, self._root)



    class Updates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_updates = self._io.read_u4be()
            if not self.len_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_updates, self._io, u"/types/updates_0/seq/0")
            self._raw_updates = self._io.read_bytes(self.len_updates)
            _io__raw_updates = KaitaiStream(BytesIO(self._raw_updates))
            self.updates = Id015PtlimaptLazyStorageDiff.Updates(_io__raw_updates, self, self._root)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id015PtlimaptLazyStorageDiff.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




