# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import block_header__shell
class BlockHeaderPrihk96nbafs(KaitaiStruct):
    """Encoding id: block_header__PrihK96nBAFS."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.chain_id = self._io.read_bytes(4)
        self.hash = self._io.read_bytes(32)
        self.raw_block_header = BlockHeaderPrihk96nbafs.RawBlockHeader(self._io, self, self._root)

    class RawBlockHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.raw_block_header = block_header__shell.BlockHeaderShell(self._io)



