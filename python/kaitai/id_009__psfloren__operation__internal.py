# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id009PsflorenOperationInternal(KaitaiStruct):
    """Encoding id: 009-PsFLoren.operation.internal."""

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Id009PsflorenContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id009PsflorenEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Id009PsflorenOperationAlphaInternalOperationTag(Enum):
        reveal = 0
        transaction = 1
        origination = 2
        delegation = 3

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_009__psfloren__operation__alpha__internal_operation = Id009PsflorenOperationInternal.Id009PsflorenOperationAlphaInternalOperation(self._io, self, self._root)

    class Id009PsflorenContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_009__psfloren__contract_id_tag = KaitaiStream.resolve_enum(Id009PsflorenOperationInternal.Id009PsflorenContractIdTag, self._io.read_u1())
            if self.id_009__psfloren__contract_id_tag == Id009PsflorenOperationInternal.Id009PsflorenContractIdTag.implicit:
                self.implicit = Id009PsflorenOperationInternal.PublicKeyHash(self._io, self, self._root)

            if self.id_009__psfloren__contract_id_tag == Id009PsflorenOperationInternal.Id009PsflorenContractIdTag.originated:
                self.originated = Id009PsflorenOperationInternal.Originated(self._io, self, self._root)



    class Id009PsflorenScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id009PsflorenOperationInternal.BytesDynUint30(self._io, self, self._root)
            self.storage = Id009PsflorenOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id009PsflorenOperationInternal.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id009PsflorenOperationAlphaInternalOperation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id009PsflorenOperationInternal.Id009PsflorenContractId(self._io, self, self._root)
            self.nonce = self._io.read_u2be()
            self.id_009__psfloren__operation__alpha__internal_operation_tag = KaitaiStream.resolve_enum(Id009PsflorenOperationInternal.Id009PsflorenOperationAlphaInternalOperationTag, self._io.read_u1())
            if self.id_009__psfloren__operation__alpha__internal_operation_tag == Id009PsflorenOperationInternal.Id009PsflorenOperationAlphaInternalOperationTag.reveal:
                self.reveal = Id009PsflorenOperationInternal.PublicKey(self._io, self, self._root)

            if self.id_009__psfloren__operation__alpha__internal_operation_tag == Id009PsflorenOperationInternal.Id009PsflorenOperationAlphaInternalOperationTag.transaction:
                self.transaction = Id009PsflorenOperationInternal.Transaction(self._io, self, self._root)

            if self.id_009__psfloren__operation__alpha__internal_operation_tag == Id009PsflorenOperationInternal.Id009PsflorenOperationAlphaInternalOperationTag.origination:
                self.origination = Id009PsflorenOperationInternal.Origination(self._io, self, self._root)

            if self.id_009__psfloren__operation__alpha__internal_operation_tag == Id009PsflorenOperationInternal.Id009PsflorenOperationAlphaInternalOperationTag.delegation:
                self.delegation = Id009PsflorenOperationInternal.Delegation(self._io, self, self._root)



    class Id009PsflorenMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_009__psfloren__mutez = Id009PsflorenOperationInternal.N(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id009PsflorenOperationInternal.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id009PsflorenOperationInternal.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id009PsflorenOperationInternal.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id009PsflorenOperationInternal.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id009PsflorenOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id009PsflorenOperationInternal.Bool.true:
                self.delegate = Id009PsflorenOperationInternal.PublicKeyHash(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Id009PsflorenEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_009__psfloren__entrypoint_tag = KaitaiStream.resolve_enum(Id009PsflorenOperationInternal.Id009PsflorenEntrypointTag, self._io.read_u1())
            if self.id_009__psfloren__entrypoint_tag == Id009PsflorenOperationInternal.Id009PsflorenEntrypointTag.named:
                self.named = Id009PsflorenOperationInternal.Named0(self._io, self, self._root)



    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.balance = Id009PsflorenOperationInternal.Id009PsflorenMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id009PsflorenOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id009PsflorenOperationInternal.Bool.true:
                self.delegate = Id009PsflorenOperationInternal.PublicKeyHash(self._io, self, self._root)

            self.script = Id009PsflorenOperationInternal.Id009PsflorenScriptedContracts(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id009PsflorenOperationInternal.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount = Id009PsflorenOperationInternal.Id009PsflorenMutez(self._io, self, self._root)
            self.destination = Id009PsflorenOperationInternal.Id009PsflorenContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id009PsflorenOperationInternal.Bool, self._io.read_u1())
            if self.parameters_tag == Id009PsflorenOperationInternal.Bool.true:
                self.parameters = Id009PsflorenOperationInternal.Parameters(self._io, self, self._root)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id009PsflorenOperationInternal.Id009PsflorenEntrypoint(self._io, self, self._root)
            self.value = Id009PsflorenOperationInternal.BytesDynUint30(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id009PsflorenOperationInternal.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id009PsflorenOperationInternal.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id009PsflorenOperationInternal.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id009PsflorenOperationInternal.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




