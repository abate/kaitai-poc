# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id019PtparisaSmartRollupOutboxMessage(KaitaiStruct):
    """Encoding id: 019-PtParisA.smart_rollup.outbox.message."""

    class Id019PtparisaContractIdOriginatedTag(Enum):
        originated = 1

    class Id019PtparisaMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left = 5
        none = 6
        pair_1 = 7
        right = 8
        some_0 = 9
        true = 10
        unit_1 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left_0 = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none_0 = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right_0 = 68
        size = 69
        some = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_0 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction_deprecated = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_deprecated = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140
        chest = 141
        chest_key = 142
        open_chest = 143
        view_0 = 144
        view = 145
        constant = 146
        sub_mutez = 147
        tx_rollup_l2_address = 148
        min_block_time = 149
        sapling_transaction = 150
        emit = 151
        lambda_rec_0 = 152
        lambda_rec = 153
        ticket_0 = 154
        bytes_0 = 155
        nat_0 = 156
        ticket_1 = 157

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class Bool(Enum):
        false = 0
        true = 255

    class Micheline019PtparisaMichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10

    class Id019PtparisaSmartRollupOutboxMessageTag(Enum):
        atomic_transaction_batch = 0
        atomic_transaction_batch_typed = 1
        whitelist_update = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_019__ptparisa__smart_rollup__outbox__message_tag = KaitaiStream.resolve_enum(Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaSmartRollupOutboxMessageTag, self._io.read_u1())
        if self.id_019__ptparisa__smart_rollup__outbox__message_tag == Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaSmartRollupOutboxMessageTag.atomic_transaction_batch:
            self.atomic_transaction_batch = Id019PtparisaSmartRollupOutboxMessage.Transactions0(self._io, self, self._root)

        if self.id_019__ptparisa__smart_rollup__outbox__message_tag == Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaSmartRollupOutboxMessageTag.atomic_transaction_batch_typed:
            self.atomic_transaction_batch_typed = Id019PtparisaSmartRollupOutboxMessage.Transactions2(self._io, self, self._root)

        if self.id_019__ptparisa__smart_rollup__outbox__message_tag == Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaSmartRollupOutboxMessageTag.whitelist_update:
            self.whitelist_update = Id019PtparisaSmartRollupOutboxMessage.WhitelistUpdate(self._io, self, self._root)


    class Whitelist0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_whitelist = self._io.read_u4be()
            if not self.len_whitelist <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_whitelist, self._io, u"/types/whitelist_0/seq/0")
            self._raw_whitelist = self._io.read_bytes(self.len_whitelist)
            _io__raw_whitelist = KaitaiStream(BytesIO(self._raw_whitelist))
            self.whitelist = Id019PtparisaSmartRollupOutboxMessage.Whitelist(_io__raw_whitelist, self, self._root)


    class Transactions2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_transactions = self._io.read_u4be()
            if not self.len_transactions <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_transactions, self._io, u"/types/transactions_2/seq/0")
            self._raw_transactions = self._io.read_bytes(self.len_transactions)
            _io__raw_transactions = KaitaiStream(BytesIO(self._raw_transactions))
            self.transactions = Id019PtparisaSmartRollupOutboxMessage.Transactions1(_io__raw_transactions, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id019PtparisaSmartRollupOutboxMessage.BytesDynUint30(self._io, self, self._root)


    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaMichelsonV1Primitives(self._io, self, self._root)
            self.annots = Id019PtparisaSmartRollupOutboxMessage.BytesDynUint30(self._io, self, self._root)


    class WhitelistEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.signature__public_key_hash = Id019PtparisaSmartRollupOutboxMessage.PublicKeyHash(self._io, self, self._root)


    class TransactionsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.parameters = Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1Expression(self._io, self, self._root)
            self.destination = Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaContractIdOriginated(self._io, self, self._root)
            self.entrypoint = Id019PtparisaSmartRollupOutboxMessage.BytesDynUint30(self._io, self, self._root)


    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaMichelsonV1Primitives(self._io, self, self._root)
            self.args = Id019PtparisaSmartRollupOutboxMessage.Args0(self._io, self, self._root)
            self.annots = Id019PtparisaSmartRollupOutboxMessage.BytesDynUint30(self._io, self, self._root)


    class Id019PtparisaMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__michelson__v1__primitives = KaitaiStream.resolve_enum(Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaMichelsonV1Primitives, self._io.read_u1())


    class TransactionsEntries0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.parameters = Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1Expression(self._io, self, self._root)
            self.parameters_ty = Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1Expression(self._io, self, self._root)
            self.destination = Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaContractIdOriginated(self._io, self, self._root)
            self.entrypoint = Id019PtparisaSmartRollupOutboxMessage.BytesDynUint30(self._io, self, self._root)


    class Transactions0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_transactions = self._io.read_u4be()
            if not self.len_transactions <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_transactions, self._io, u"/types/transactions_0/seq/0")
            self._raw_transactions = self._io.read_bytes(self.len_transactions)
            _io__raw_transactions = KaitaiStream(BytesIO(self._raw_transactions))
            self.transactions = Id019PtparisaSmartRollupOutboxMessage.Transactions(_io__raw_transactions, self, self._root)


    class Id019PtparisaContractIdOriginated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__contract_id__originated_tag = KaitaiStream.resolve_enum(Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaContractIdOriginatedTag, self._io.read_u1())
            if self.id_019__ptparisa__contract_id__originated_tag == Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaContractIdOriginatedTag.originated:
                self.originated = Id019PtparisaSmartRollupOutboxMessage.Originated(self._io, self, self._root)



    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = Id019PtparisaSmartRollupOutboxMessage.Sequence(_io__raw_sequence, self, self._root)


    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(Id019PtparisaSmartRollupOutboxMessage.SequenceEntries(self._io, self, self._root))
                i += 1



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class WhitelistUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.whitelist_tag = KaitaiStream.resolve_enum(Id019PtparisaSmartRollupOutboxMessage.Bool, self._io.read_u1())
            if self.whitelist_tag == Id019PtparisaSmartRollupOutboxMessage.Bool.true:
                self.whitelist = Id019PtparisaSmartRollupOutboxMessage.Whitelist0(self._io, self, self._root)



    class Whitelist(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.whitelist_entries = []
            i = 0
            while not self._io.is_eof():
                self.whitelist_entries.append(Id019PtparisaSmartRollupOutboxMessage.WhitelistEntries(self._io, self, self._root))
                i += 1



    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(Id019PtparisaSmartRollupOutboxMessage.ArgsEntries(self._io, self, self._root))
                i += 1



    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1Expression(self._io, self, self._root)


    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1Expression(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1Expression(self._io, self, self._root)


    class Transactions1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.transactions_entries = []
            i = 0
            while not self._io.is_eof():
                self.transactions_entries.append(Id019PtparisaSmartRollupOutboxMessage.TransactionsEntries0(self._io, self, self._root))
                i += 1



    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1Expression(self._io, self, self._root)


    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = Id019PtparisaSmartRollupOutboxMessage.Args(_io__raw_args, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id019PtparisaSmartRollupOutboxMessage.BytesDynUint30(self._io, self, self._root)


    class Micheline019PtparisaMichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__019__ptparisa__michelson_v1__expression_tag = KaitaiStream.resolve_enum(Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__019__ptparisa__michelson_v1__expression_tag == Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1ExpressionTag.int:
                self.int = Id019PtparisaSmartRollupOutboxMessage.Z(self._io, self, self._root)

            if self.micheline__019__ptparisa__michelson_v1__expression_tag == Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1ExpressionTag.string:
                self.string = Id019PtparisaSmartRollupOutboxMessage.BytesDynUint30(self._io, self, self._root)

            if self.micheline__019__ptparisa__michelson_v1__expression_tag == Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1ExpressionTag.sequence:
                self.sequence = Id019PtparisaSmartRollupOutboxMessage.Sequence0(self._io, self, self._root)

            if self.micheline__019__ptparisa__michelson_v1__expression_tag == Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = Id019PtparisaSmartRollupOutboxMessage.Id019PtparisaMichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__019__ptparisa__michelson_v1__expression_tag == Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = Id019PtparisaSmartRollupOutboxMessage.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__019__ptparisa__michelson_v1__expression_tag == Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = Id019PtparisaSmartRollupOutboxMessage.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__019__ptparisa__michelson_v1__expression_tag == Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = Id019PtparisaSmartRollupOutboxMessage.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__019__ptparisa__michelson_v1__expression_tag == Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = Id019PtparisaSmartRollupOutboxMessage.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__019__ptparisa__michelson_v1__expression_tag == Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = Id019PtparisaSmartRollupOutboxMessage.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__019__ptparisa__michelson_v1__expression_tag == Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = Id019PtparisaSmartRollupOutboxMessage.PrimGeneric(self._io, self, self._root)

            if self.micheline__019__ptparisa__michelson_v1__expression_tag == Id019PtparisaSmartRollupOutboxMessage.Micheline019PtparisaMichelsonV1ExpressionTag.bytes:
                self.bytes = Id019PtparisaSmartRollupOutboxMessage.BytesDynUint30(self._io, self, self._root)



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id019PtparisaSmartRollupOutboxMessage.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id019PtparisaSmartRollupOutboxMessage.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id019PtparisaSmartRollupOutboxMessage.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id019PtparisaSmartRollupOutboxMessage.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id019PtparisaSmartRollupOutboxMessage.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class Transactions(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.transactions_entries = []
            i = 0
            while not self._io.is_eof():
                self.transactions_entries.append(Id019PtparisaSmartRollupOutboxMessage.TransactionsEntries(self._io, self, self._root))
                i += 1



    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id019PtparisaSmartRollupOutboxMessage.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




