# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id005Psbabym1DelegateFrozenBalanceByCycles(KaitaiStruct):
    """Encoding id: 005-PsBabyM1.delegate.frozen_balance_by_cycles."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.len_id_005__psbabym1__delegate__frozen_balance_by_cycles = self._io.read_u4be()
        if not self.len_id_005__psbabym1__delegate__frozen_balance_by_cycles <= 1073741823:
            raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_005__psbabym1__delegate__frozen_balance_by_cycles, self._io, u"/seq/0")
        self._raw_id_005__psbabym1__delegate__frozen_balance_by_cycles = self._io.read_bytes(self.len_id_005__psbabym1__delegate__frozen_balance_by_cycles)
        _io__raw_id_005__psbabym1__delegate__frozen_balance_by_cycles = KaitaiStream(BytesIO(self._raw_id_005__psbabym1__delegate__frozen_balance_by_cycles))
        self.id_005__psbabym1__delegate__frozen_balance_by_cycles = Id005Psbabym1DelegateFrozenBalanceByCycles.Id005Psbabym1DelegateFrozenBalanceByCycles(_io__raw_id_005__psbabym1__delegate__frozen_balance_by_cycles, self, self._root)

    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id005Psbabym1DelegateFrozenBalanceByCycles.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id005Psbabym1Mutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__mutez = Id005Psbabym1DelegateFrozenBalanceByCycles.N(self._io, self, self._root)


    class Id005Psbabym1DelegateFrozenBalanceByCycles(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_005__psbabym1__delegate__frozen_balance_by_cycles_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_005__psbabym1__delegate__frozen_balance_by_cycles_entries.append(Id005Psbabym1DelegateFrozenBalanceByCycles.Id005Psbabym1DelegateFrozenBalanceByCyclesEntries(self._io, self, self._root))
                i += 1



    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Id005Psbabym1DelegateFrozenBalanceByCyclesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.cycle = self._io.read_s4be()
            self.deposit = Id005Psbabym1DelegateFrozenBalanceByCycles.Id005Psbabym1Mutez(self._io, self, self._root)
            self.fees = Id005Psbabym1DelegateFrozenBalanceByCycles.Id005Psbabym1Mutez(self._io, self, self._root)
            self.rewards = Id005Psbabym1DelegateFrozenBalanceByCycles.Id005Psbabym1Mutez(self._io, self, self._root)



