# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import block_header__shell
class BlockHeaderPsflorenauuu(KaitaiStruct):
    """Encoding id: block_header__PsFLorenaUUu."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.chain_id = self._io.read_bytes(4)
        self.hash = self._io.read_bytes(32)
        self.raw_block_header = BlockHeaderPsflorenauuu.RawBlockHeader(self._io, self, self._root)

    class Id009PsflorenBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_009__psfloren__block_header__alpha__unsigned_contents = BlockHeaderPsflorenauuu.Id009PsflorenBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes(64)


    class Id009PsflorenBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.priority = self._io.read_u2be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(BlockHeaderPsflorenauuu.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == BlockHeaderPsflorenauuu.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)



    class RawBlockHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.raw_block_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_009__psfloren__block_header__alpha__signed_contents = BlockHeaderPsflorenauuu.Id009PsflorenBlockHeaderAlphaSignedContents(self._io, self, self._root)



