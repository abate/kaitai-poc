# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class P2pPeerState(KaitaiStruct):
    """Encoding id: p2p_peer.state
    Description: The state a peer connection can be in: accepted (when the connection is being established), running (when the connection is already established), disconnected (otherwise)."""

    class P2pPeerState(Enum):
        accepted = 0
        running = 1
        disconnected = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.p2p_peer__state = KaitaiStream.resolve_enum(P2pPeerState.P2pPeerState, self._io.read_u1())


