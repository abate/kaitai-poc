# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class UserActivatedProtocolOverrides(KaitaiStruct):
    """Encoding id: user_activated.protocol_overrides
    Description: User activated protocol overrides: activate a protocol instead of another."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.len_user_activated__protocol_overrides = self._io.read_u4be()
        if not self.len_user_activated__protocol_overrides <= 1073741823:
            raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_user_activated__protocol_overrides, self._io, u"/seq/0")
        self._raw_user_activated__protocol_overrides = self._io.read_bytes(self.len_user_activated__protocol_overrides)
        _io__raw_user_activated__protocol_overrides = KaitaiStream(BytesIO(self._raw_user_activated__protocol_overrides))
        self.user_activated__protocol_overrides = UserActivatedProtocolOverrides.UserActivatedProtocolOverrides(_io__raw_user_activated__protocol_overrides, self, self._root)

    class UserActivatedProtocolOverrides(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.user_activated__protocol_overrides_entries = []
            i = 0
            while not self._io.is_eof():
                self.user_activated__protocol_overrides_entries.append(UserActivatedProtocolOverrides.UserActivatedProtocolOverridesEntries(self._io, self, self._root))
                i += 1



    class UserActivatedProtocolOverridesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.replaced_protocol = self._io.read_bytes(32)
            self.replacement_protocol = self._io.read_bytes(32)



