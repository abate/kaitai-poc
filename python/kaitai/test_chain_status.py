# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import timestamp__protocol
class TestChainStatus(KaitaiStruct):
    """Encoding id: test_chain_status
    Description: The status of the test chain: not_running (there is no test chain at the moment), forking (the test chain is being setup), running (the test chain is running)."""

    class TestChainStatusTag(Enum):
        not_running = 0
        forking = 1
        running = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.test_chain_status_tag = KaitaiStream.resolve_enum(TestChainStatus.TestChainStatusTag, self._io.read_u1())
        if self.test_chain_status_tag == TestChainStatus.TestChainStatusTag.forking:
            self.forking = TestChainStatus.Forking(self._io, self, self._root)

        if self.test_chain_status_tag == TestChainStatus.TestChainStatusTag.running:
            self.running = TestChainStatus.Running(self._io, self, self._root)


    class Forking(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol = self._io.read_bytes(32)
            self.expiration = timestamp__protocol.TimestampProtocol(self._io)


    class Running(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.chain_id = self._io.read_bytes(4)
            self.genesis = self._io.read_bytes(32)
            self.protocol = self._io.read_bytes(32)
            self.expiration = timestamp__protocol.TimestampProtocol(self._io)



