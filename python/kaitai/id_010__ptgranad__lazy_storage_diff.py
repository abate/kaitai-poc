# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import sapling__transaction__nullifier
from kaitai import sapling__transaction__commitment
from kaitai import sapling__transaction__ciphertext
class Id010PtgranadLazyStorageDiff(KaitaiStruct):
    """Encoding id: 010-PtGRANAD.lazy_storage_diff."""

    class Id010PtgranadMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left_0 = 5
        none = 6
        pair_1 = 7
        right_0 = 8
        some = 9
        true = 10
        unit_0 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none_0 = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right = 68
        size = 69
        some_0 = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_1 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_0 = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140

    class DiffTag(Enum):
        update = 0
        remove = 1
        copy = 2
        alloc = 3

    class Micheline010PtgranadMichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10

    class Bool(Enum):
        false = 0
        true = 255

    class Id010PtgranadLazyStorageDiffEltTag(Enum):
        big_map = 0
        sapling_state = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.len_id_010__ptgranad__lazy_storage_diff = self._io.read_u4be()
        if not self.len_id_010__ptgranad__lazy_storage_diff <= 1073741823:
            raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_010__ptgranad__lazy_storage_diff, self._io, u"/seq/0")
        self._raw_id_010__ptgranad__lazy_storage_diff = self._io.read_bytes(self.len_id_010__ptgranad__lazy_storage_diff)
        _io__raw_id_010__ptgranad__lazy_storage_diff = KaitaiStream(BytesIO(self._raw_id_010__ptgranad__lazy_storage_diff))
        self.id_010__ptgranad__lazy_storage_diff = Id010PtgranadLazyStorageDiff.Id010PtgranadLazyStorageDiff(_io__raw_id_010__ptgranad__lazy_storage_diff, self, self._root)

    class Id010PtgranadMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__michelson__v1__primitives = KaitaiStream.resolve_enum(Id010PtgranadLazyStorageDiff.Id010PtgranadMichelsonV1Primitives, self._io.read_u1())


    class Nullifiers(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.nullifiers_entries = []
            i = 0
            while not self._io.is_eof():
                self.nullifiers_entries.append(Id010PtgranadLazyStorageDiff.NullifiersEntries(self._io, self, self._root))
                i += 1



    class Alloc(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.updates = Id010PtgranadLazyStorageDiff.Updates0(self._io, self, self._root)
            self.key_type = Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1Expression(self._io, self, self._root)
            self.value_type = Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1Expression(self._io, self, self._root)


    class Id010PtgranadLazyStorageDiff(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__lazy_storage_diff_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_010__ptgranad__lazy_storage_diff_entries.append(Id010PtgranadLazyStorageDiff.Id010PtgranadLazyStorageDiffEntries(self._io, self, self._root))
                i += 1



    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id010PtgranadLazyStorageDiff.Id010PtgranadMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id010PtgranadLazyStorageDiff.BytesDynUint30(self._io, self, self._root)


    class Nullifiers0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_nullifiers = self._io.read_u4be()
            if not self.len_nullifiers <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_nullifiers, self._io, u"/types/nullifiers_0/seq/0")
            self._raw_nullifiers = self._io.read_bytes(self.len_nullifiers)
            _io__raw_nullifiers = KaitaiStream(BytesIO(self._raw_nullifiers))
            self.nullifiers = Id010PtgranadLazyStorageDiff.Nullifiers(_io__raw_nullifiers, self, self._root)


    class Copy0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id010PtgranadLazyStorageDiff.Id010PtgranadSaplingStateId(self._io, self, self._root)
            self.updates = Id010PtgranadLazyStorageDiff.Updates1(self._io, self, self._root)


    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id010PtgranadLazyStorageDiff.Id010PtgranadMichelsonV1Primitives(self._io, self, self._root)
            self.annots = Id010PtgranadLazyStorageDiff.BytesDynUint30(self._io, self, self._root)


    class CommitmentsAndCiphertexts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_and_ciphertexts_entries = []
            i = 0
            while not self._io.is_eof():
                self.commitments_and_ciphertexts_entries.append(Id010PtgranadLazyStorageDiff.CommitmentsAndCiphertextsEntries(self._io, self, self._root))
                i += 1



    class Diff0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.diff_tag = KaitaiStream.resolve_enum(Id010PtgranadLazyStorageDiff.DiffTag, self._io.read_u1())
            if self.diff_tag == Id010PtgranadLazyStorageDiff.DiffTag.update:
                self.update = Id010PtgranadLazyStorageDiff.Updates1(self._io, self, self._root)

            if self.diff_tag == Id010PtgranadLazyStorageDiff.DiffTag.copy:
                self.copy = Id010PtgranadLazyStorageDiff.Copy0(self._io, self, self._root)

            if self.diff_tag == Id010PtgranadLazyStorageDiff.DiffTag.alloc:
                self.alloc = Id010PtgranadLazyStorageDiff.Alloc0(self._io, self, self._root)



    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id010PtgranadLazyStorageDiff.Id010PtgranadMichelsonV1Primitives(self._io, self, self._root)
            self.args = Id010PtgranadLazyStorageDiff.Args0(self._io, self, self._root)
            self.annots = Id010PtgranadLazyStorageDiff.BytesDynUint30(self._io, self, self._root)


    class SaplingState(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id = Id010PtgranadLazyStorageDiff.Id010PtgranadSaplingStateId(self._io, self, self._root)
            self.diff = Id010PtgranadLazyStorageDiff.Diff0(self._io, self, self._root)


    class CommitmentsAndCiphertexts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_commitments_and_ciphertexts = self._io.read_u4be()
            if not self.len_commitments_and_ciphertexts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_commitments_and_ciphertexts, self._io, u"/types/commitments_and_ciphertexts_0/seq/0")
            self._raw_commitments_and_ciphertexts = self._io.read_bytes(self.len_commitments_and_ciphertexts)
            _io__raw_commitments_and_ciphertexts = KaitaiStream(BytesIO(self._raw_commitments_and_ciphertexts))
            self.commitments_and_ciphertexts = Id010PtgranadLazyStorageDiff.CommitmentsAndCiphertexts(_io__raw_commitments_and_ciphertexts, self, self._root)


    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = Id010PtgranadLazyStorageDiff.Sequence(_io__raw_sequence, self, self._root)


    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(Id010PtgranadLazyStorageDiff.SequenceEntries(self._io, self, self._root))
                i += 1



    class Updates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.updates_entries.append(Id010PtgranadLazyStorageDiff.UpdatesEntries(self._io, self, self._root))
                i += 1



    class Diff(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.diff_tag = KaitaiStream.resolve_enum(Id010PtgranadLazyStorageDiff.DiffTag, self._io.read_u1())
            if self.diff_tag == Id010PtgranadLazyStorageDiff.DiffTag.update:
                self.update = Id010PtgranadLazyStorageDiff.Updates0(self._io, self, self._root)

            if self.diff_tag == Id010PtgranadLazyStorageDiff.DiffTag.copy:
                self.copy = Id010PtgranadLazyStorageDiff.Copy(self._io, self, self._root)

            if self.diff_tag == Id010PtgranadLazyStorageDiff.DiffTag.alloc:
                self.alloc = Id010PtgranadLazyStorageDiff.Alloc(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class CommitmentsAndCiphertextsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_and_ciphertexts_elt_field0 = sapling__transaction__commitment.SaplingTransactionCommitment(self._io)
            self.commitments_and_ciphertexts_elt_field1 = sapling__transaction__ciphertext.SaplingTransactionCiphertext(self._io)


    class Updates1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_and_ciphertexts = Id010PtgranadLazyStorageDiff.CommitmentsAndCiphertexts0(self._io, self, self._root)
            self.nullifiers = Id010PtgranadLazyStorageDiff.Nullifiers0(self._io, self, self._root)


    class Micheline010PtgranadMichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__010__ptgranad__michelson_v1__expression_tag = KaitaiStream.resolve_enum(Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__010__ptgranad__michelson_v1__expression_tag == Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1ExpressionTag.int:
                self.int = Id010PtgranadLazyStorageDiff.Z(self._io, self, self._root)

            if self.micheline__010__ptgranad__michelson_v1__expression_tag == Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1ExpressionTag.string:
                self.string = Id010PtgranadLazyStorageDiff.BytesDynUint30(self._io, self, self._root)

            if self.micheline__010__ptgranad__michelson_v1__expression_tag == Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1ExpressionTag.sequence:
                self.sequence = Id010PtgranadLazyStorageDiff.Sequence0(self._io, self, self._root)

            if self.micheline__010__ptgranad__michelson_v1__expression_tag == Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = Id010PtgranadLazyStorageDiff.Id010PtgranadMichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__010__ptgranad__michelson_v1__expression_tag == Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = Id010PtgranadLazyStorageDiff.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__010__ptgranad__michelson_v1__expression_tag == Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = Id010PtgranadLazyStorageDiff.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__010__ptgranad__michelson_v1__expression_tag == Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = Id010PtgranadLazyStorageDiff.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__010__ptgranad__michelson_v1__expression_tag == Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = Id010PtgranadLazyStorageDiff.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__010__ptgranad__michelson_v1__expression_tag == Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = Id010PtgranadLazyStorageDiff.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__010__ptgranad__michelson_v1__expression_tag == Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = Id010PtgranadLazyStorageDiff.PrimGeneric(self._io, self, self._root)

            if self.micheline__010__ptgranad__michelson_v1__expression_tag == Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1ExpressionTag.bytes:
                self.bytes = Id010PtgranadLazyStorageDiff.BytesDynUint30(self._io, self, self._root)



    class Id010PtgranadLazyStorageDiffEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__lazy_storage_diff_elt_tag = KaitaiStream.resolve_enum(Id010PtgranadLazyStorageDiff.Id010PtgranadLazyStorageDiffEltTag, self._io.read_u1())
            if self.id_010__ptgranad__lazy_storage_diff_elt_tag == Id010PtgranadLazyStorageDiff.Id010PtgranadLazyStorageDiffEltTag.big_map:
                self.big_map = Id010PtgranadLazyStorageDiff.BigMap(self._io, self, self._root)

            if self.id_010__ptgranad__lazy_storage_diff_elt_tag == Id010PtgranadLazyStorageDiff.Id010PtgranadLazyStorageDiffEltTag.sapling_state:
                self.sapling_state = Id010PtgranadLazyStorageDiff.SaplingState(self._io, self, self._root)



    class Copy(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id010PtgranadLazyStorageDiff.Id010PtgranadBigMapId(self._io, self, self._root)
            self.updates = Id010PtgranadLazyStorageDiff.Updates0(self._io, self, self._root)


    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(Id010PtgranadLazyStorageDiff.ArgsEntries(self._io, self, self._root))
                i += 1



    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id010PtgranadLazyStorageDiff.Id010PtgranadMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1Expression(self._io, self, self._root)


    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1Expression(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id010PtgranadLazyStorageDiff.Id010PtgranadMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1Expression(self._io, self, self._root)


    class Alloc0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.updates = Id010PtgranadLazyStorageDiff.Updates1(self._io, self, self._root)
            self.memo_size = self._io.read_u2be()


    class NullifiersEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.nullifiers_elt = sapling__transaction__nullifier.SaplingTransactionNullifier(self._io)


    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1Expression(self._io, self, self._root)


    class BigMap(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id = Id010PtgranadLazyStorageDiff.Id010PtgranadBigMapId(self._io, self, self._root)
            self.diff = Id010PtgranadLazyStorageDiff.Diff(self._io, self, self._root)


    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = Id010PtgranadLazyStorageDiff.Args(_io__raw_args, self, self._root)


    class Id010PtgranadBigMapId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__big_map_id = Id010PtgranadLazyStorageDiff.Z(self._io, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id010PtgranadLazyStorageDiff.Id010PtgranadMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id010PtgranadLazyStorageDiff.BytesDynUint30(self._io, self, self._root)


    class Id010PtgranadSaplingStateId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__sapling_state_id = Id010PtgranadLazyStorageDiff.Z(self._io, self, self._root)


    class UpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.key_hash = self._io.read_bytes(32)
            self.key = Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1Expression(self._io, self, self._root)
            self.value_tag = KaitaiStream.resolve_enum(Id010PtgranadLazyStorageDiff.Bool, self._io.read_u1())
            if self.value_tag == Id010PtgranadLazyStorageDiff.Bool.true:
                self.value = Id010PtgranadLazyStorageDiff.Micheline010PtgranadMichelsonV1Expression(self._io, self, self._root)



    class Updates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_updates = self._io.read_u4be()
            if not self.len_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_updates, self._io, u"/types/updates_0/seq/0")
            self._raw_updates = self._io.read_bytes(self.len_updates)
            _io__raw_updates = KaitaiStream(BytesIO(self._raw_updates))
            self.updates = Id010PtgranadLazyStorageDiff.Updates(_io__raw_updates, self, self._root)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id010PtgranadLazyStorageDiff.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




