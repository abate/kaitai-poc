# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class ProtocolMeta(KaitaiStruct):
    """Encoding id: protocol.meta
    Description: Protocol metadata: the hash of the protocol, the expected environment version and the list of modules comprising the protocol."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.hash_tag = KaitaiStream.resolve_enum(ProtocolMeta.Bool, self._io.read_u1())
        if self.hash_tag == ProtocolMeta.Bool.true:
            self.hash = self._io.read_bytes(32)

        self.expected_env_version_tag = KaitaiStream.resolve_enum(ProtocolMeta.Bool, self._io.read_u1())
        if self.expected_env_version_tag == ProtocolMeta.Bool.true:
            self.expected_env_version = ProtocolMeta.ProtocolEnvironmentVersion(self._io, self, self._root)

        self.modules = ProtocolMeta.Modules0(self._io, self, self._root)

    class ModulesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.modules_elt = ProtocolMeta.BytesDynUint30(self._io, self, self._root)


    class Modules0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_modules = self._io.read_u4be()
            if not self.len_modules <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_modules, self._io, u"/types/modules_0/seq/0")
            self._raw_modules = self._io.read_bytes(self.len_modules)
            _io__raw_modules = KaitaiStream(BytesIO(self._raw_modules))
            self.modules = ProtocolMeta.Modules(_io__raw_modules, self, self._root)


    class Modules(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.modules_entries = []
            i = 0
            while not self._io.is_eof():
                self.modules_entries.append(ProtocolMeta.ModulesEntries(self._io, self, self._root))
                i += 1



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class ProtocolEnvironmentVersion(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol__environment_version = self._io.read_u2be()



