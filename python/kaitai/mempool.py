# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Mempool(KaitaiStruct):
    """Encoding id: mempool
    Description: A batch of operation. This format is used to gossip operations between peers."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.known_valid = Mempool.KnownValid0(self._io, self, self._root)
        self.pending = Mempool.Pending1(self._io, self, self._root)

    class Pending0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_pending = self._io.read_u4be()
            if not self.len_pending <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_pending, self._io, u"/types/pending_0/seq/0")
            self._raw_pending = self._io.read_bytes(self.len_pending)
            _io__raw_pending = KaitaiStream(BytesIO(self._raw_pending))
            self.pending = Mempool.Pending(_io__raw_pending, self, self._root)


    class KnownValid(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.known_valid_entries = []
            i = 0
            while not self._io.is_eof():
                self.known_valid_entries.append(Mempool.KnownValidEntries(self._io, self, self._root))
                i += 1



    class Pending1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_pending = self._io.read_u4be()
            if not self.len_pending <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_pending, self._io, u"/types/pending_1/seq/0")
            self._raw_pending = self._io.read_bytes(self.len_pending)
            _io__raw_pending = KaitaiStream(BytesIO(self._raw_pending))
            self.pending = Mempool.Pending0(_io__raw_pending, self, self._root)


    class KnownValid0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_known_valid = self._io.read_u4be()
            if not self.len_known_valid <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_known_valid, self._io, u"/types/known_valid_0/seq/0")
            self._raw_known_valid = self._io.read_bytes(self.len_known_valid)
            _io__raw_known_valid = KaitaiStream(BytesIO(self._raw_known_valid))
            self.known_valid = Mempool.KnownValid(_io__raw_known_valid, self, self._root)


    class PendingEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.operation_hash = self._io.read_bytes(32)


    class Pending(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pending_entries = []
            i = 0
            while not self._io.is_eof():
                self.pending_entries.append(Mempool.PendingEntries(self._io, self, self._root))
                i += 1



    class KnownValidEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.operation_hash = self._io.read_bytes(32)



