# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id019PtparisaUnstakedFrozenStaker(KaitaiStruct):
    """Encoding id: 019-PtParisA.unstaked_frozen_staker."""

    class Id019PtparisaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id019PtparisaStakerTag(Enum):
        single = 0
        shared = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_019__ptparisa__staker = Id019PtparisaUnstakedFrozenStaker.Id019PtparisaStaker(self._io, self, self._root)

    class Id019PtparisaStaker(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__staker_tag = KaitaiStream.resolve_enum(Id019PtparisaUnstakedFrozenStaker.Id019PtparisaStakerTag, self._io.read_u1())
            if self.id_019__ptparisa__staker_tag == Id019PtparisaUnstakedFrozenStaker.Id019PtparisaStakerTag.single:
                self.single = Id019PtparisaUnstakedFrozenStaker.Single(self._io, self, self._root)

            if self.id_019__ptparisa__staker_tag == Id019PtparisaUnstakedFrozenStaker.Id019PtparisaStakerTag.shared:
                self.shared = Id019PtparisaUnstakedFrozenStaker.PublicKeyHash(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id019PtparisaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__contract_id_tag = KaitaiStream.resolve_enum(Id019PtparisaUnstakedFrozenStaker.Id019PtparisaContractIdTag, self._io.read_u1())
            if self.id_019__ptparisa__contract_id_tag == Id019PtparisaUnstakedFrozenStaker.Id019PtparisaContractIdTag.implicit:
                self.implicit = Id019PtparisaUnstakedFrozenStaker.PublicKeyHash(self._io, self, self._root)

            if self.id_019__ptparisa__contract_id_tag == Id019PtparisaUnstakedFrozenStaker.Id019PtparisaContractIdTag.originated:
                self.originated = Id019PtparisaUnstakedFrozenStaker.Originated(self._io, self, self._root)



    class Single(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id019PtparisaUnstakedFrozenStaker.Id019PtparisaContractId(self._io, self, self._root)
            self.delegate = Id019PtparisaUnstakedFrozenStaker.PublicKeyHash(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id019PtparisaUnstakedFrozenStaker.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id019PtparisaUnstakedFrozenStaker.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id019PtparisaUnstakedFrozenStaker.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id019PtparisaUnstakedFrozenStaker.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id019PtparisaUnstakedFrozenStaker.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)




