# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id013PtjakartConstants(KaitaiStruct):
    """Encoding id: 013-PtJakart.constants."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.proof_of_work_nonce_size = self._io.read_u1()
        self.nonce_length = self._io.read_u1()
        self.max_anon_ops_per_block = self._io.read_u1()
        self.max_operation_data_length = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.max_proposals_per_delegate = self._io.read_u1()
        self.max_micheline_node_count = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.max_micheline_bytes_limit = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.max_allowed_global_constants_depth = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.cache_layout_size = self._io.read_u1()
        self.michelson_maximum_type_size = self._io.read_u2be()
        self.preserved_cycles = self._io.read_u1()
        self.blocks_per_cycle = self._io.read_s4be()
        self.blocks_per_commitment = self._io.read_s4be()
        self.blocks_per_stake_snapshot = self._io.read_s4be()
        self.cycles_per_voting_period = self._io.read_s4be()
        self.hard_gas_limit_per_operation = Id013PtjakartConstants.Z(self._io, self, self._root)
        self.hard_gas_limit_per_block = Id013PtjakartConstants.Z(self._io, self, self._root)
        self.proof_of_work_threshold = self._io.read_s8be()
        self.tokens_per_roll = Id013PtjakartConstants.Id013PtjakartMutez(self._io, self, self._root)
        self.seed_nonce_revelation_tip = Id013PtjakartConstants.Id013PtjakartMutez(self._io, self, self._root)
        self.origination_size = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.baking_reward_fixed_portion = Id013PtjakartConstants.Id013PtjakartMutez(self._io, self, self._root)
        self.baking_reward_bonus_per_slot = Id013PtjakartConstants.Id013PtjakartMutez(self._io, self, self._root)
        self.endorsing_reward_per_slot = Id013PtjakartConstants.Id013PtjakartMutez(self._io, self, self._root)
        self.cost_per_byte = Id013PtjakartConstants.Id013PtjakartMutez(self._io, self, self._root)
        self.hard_storage_limit_per_operation = Id013PtjakartConstants.Z(self._io, self, self._root)
        self.quorum_min = self._io.read_s4be()
        self.quorum_max = self._io.read_s4be()
        self.min_proposal_quorum = self._io.read_s4be()
        self.liquidity_baking_subsidy = Id013PtjakartConstants.Id013PtjakartMutez(self._io, self, self._root)
        self.liquidity_baking_sunset_level = self._io.read_s4be()
        self.liquidity_baking_toggle_ema_threshold = self._io.read_s4be()
        self.max_operations_time_to_live = self._io.read_s2be()
        self.minimal_block_delay = self._io.read_s8be()
        self.delay_increment_per_round = self._io.read_s8be()
        self.consensus_committee_size = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.consensus_threshold = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.minimal_participation_ratio = Id013PtjakartConstants.MinimalParticipationRatio(self._io, self, self._root)
        self.max_slashing_period = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.frozen_deposits_percentage = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.double_baking_punishment = Id013PtjakartConstants.Id013PtjakartMutez(self._io, self, self._root)
        self.ratio_of_frozen_deposits_slashed_per_double_endorsement = Id013PtjakartConstants.RatioOfFrozenDepositsSlashedPerDoubleEndorsement(self._io, self, self._root)
        self.initial_seed_tag = KaitaiStream.resolve_enum(Id013PtjakartConstants.Bool, self._io.read_u1())
        if self.initial_seed_tag == Id013PtjakartConstants.Bool.true:
            self.initial_seed = self._io.read_bytes(32)

        self.cache_script_size = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.cache_stake_distribution_cycles = self._io.read_s1()
        self.cache_sampler_state_cycles = self._io.read_s1()
        self.tx_rollup_enable = KaitaiStream.resolve_enum(Id013PtjakartConstants.Bool, self._io.read_u1())
        self.tx_rollup_origination_size = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.tx_rollup_hard_size_limit_per_inbox = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.tx_rollup_hard_size_limit_per_message = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.tx_rollup_max_withdrawals_per_batch = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.tx_rollup_commitment_bond = Id013PtjakartConstants.Id013PtjakartMutez(self._io, self, self._root)
        self.tx_rollup_finality_period = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.tx_rollup_withdraw_period = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.tx_rollup_max_inboxes_count = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.tx_rollup_max_messages_per_inbox = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.tx_rollup_max_commitments_count = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.tx_rollup_cost_per_byte_ema_factor = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.tx_rollup_max_ticket_payload_size = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.tx_rollup_rejection_max_proof_size = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.tx_rollup_sunset_level = self._io.read_s4be()
        self.sc_rollup_enable = KaitaiStream.resolve_enum(Id013PtjakartConstants.Bool, self._io.read_u1())
        self.sc_rollup_origination_size = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.sc_rollup_challenge_window_in_blocks = Id013PtjakartConstants.Int31(self._io, self, self._root)
        self.sc_rollup_max_available_messages = Id013PtjakartConstants.Int31(self._io, self, self._root)

    class RatioOfFrozenDepositsSlashedPerDoubleEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id013PtjakartConstants.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class MinimalParticipationRatio(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class Id013PtjakartMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__mutez = Id013PtjakartConstants.N(self._io, self, self._root)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id013PtjakartConstants.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




