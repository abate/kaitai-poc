# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id006PscarthaDelegateFrozenBalance(KaitaiStruct):
    """Encoding id: 006-PsCARTHA.delegate.frozen_balance."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.deposit = Id006PscarthaDelegateFrozenBalance.Id006PscarthaMutez(self._io, self, self._root)
        self.fees = Id006PscarthaDelegateFrozenBalance.Id006PscarthaMutez(self._io, self, self._root)
        self.rewards = Id006PscarthaDelegateFrozenBalance.Id006PscarthaMutez(self._io, self, self._root)

    class Id006PscarthaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__mutez = Id006PscarthaDelegateFrozenBalance.N(self._io, self, self._root)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id006PscarthaDelegateFrozenBalance.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)



