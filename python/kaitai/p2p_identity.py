# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class P2pIdentity(KaitaiStruct):
    """Encoding id: p2p_identity
    Description: The identity of a peer. This includes cryptographic keys as well as a proof-of-work."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.peer_id_tag = KaitaiStream.resolve_enum(P2pIdentity.Bool, self._io.read_u1())
        if self.peer_id_tag == P2pIdentity.Bool.true:
            self.peer_id = self._io.read_bytes(16)

        self.public_key = self._io.read_bytes(32)
        self.secret_key = self._io.read_bytes(32)
        self.proof_of_work_stamp = self._io.read_bytes(24)


