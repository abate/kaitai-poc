# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import operation__shell_header
from kaitai import block_header__shell
class Id014PtkathmaOperation(KaitaiStruct):
    """Encoding id: 014-PtKathma.operation."""

    class AfterTag(Enum):
        value = 0
        node = 1

    class Id014PtkathmaOperationAlphaContentsTag(Enum):
        seed_nonce_revelation = 1
        double_endorsement_evidence = 2
        double_baking_evidence = 3
        activate_account = 4
        proposals = 5
        ballot = 6
        double_preendorsement_evidence = 7
        vdf_revelation = 8
        failing_noop = 17
        preendorsement = 20
        endorsement = 21
        dal_slot_availability = 22
        reveal = 107
        transaction = 108
        origination = 109
        delegation = 110
        register_global_constant = 111
        set_deposits_limit = 112
        increase_paid_storage = 113
        tx_rollup_origination = 150
        tx_rollup_submit_batch = 151
        tx_rollup_commit = 152
        tx_rollup_return_bond = 153
        tx_rollup_finalize_commitment = 154
        tx_rollup_remove_commitment = 155
        tx_rollup_rejection = 156
        tx_rollup_dispatch_tickets = 157
        transfer_ticket = 158
        sc_rollup_originate = 200
        sc_rollup_add_messages = 201
        sc_rollup_cement = 202
        sc_rollup_publish = 203
        sc_rollup_refute = 204
        sc_rollup_timeout = 205
        sc_rollup_execute_outbox_message = 206
        sc_rollup_recover_bond = 207
        sc_rollup_dal_slot_subscribe = 208
        dal_publish_slot_header = 230

    class BeforeTag(Enum):
        value = 0
        node = 1

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Id014PtkathmaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class PvmStepTag(Enum):
        arithmetic__pvm__with__proof = 0
        wasm__2__0__0__pvm__with__proof = 1

    class GivenTag(Enum):
        none = 0
        some = 1

    class DissectionEltField0Tag(Enum):
        none = 0
        some = 1

    class Case131EltField1Tag(Enum):
        case__0 = 0
        case__1 = 1

    class ProofTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3

    class TreeEncodingTag(Enum):
        value = 0
        blinded_value = 1
        node = 2
        blinded_node = 3
        inode = 4
        extender = 5
        none = 6

    class Id014PtkathmaInlinedPreendorsementContentsTag(Enum):
        preendorsement = 20

    class InboxTag(Enum):
        none = 0
        some = 1

    class Case129EltField1Tag(Enum):
        case__0 = 0
        case__1 = 1

    class Id014PtkathmaContractIdOriginatedTag(Enum):
        originated = 1

    class RequestedTag(Enum):
        no_input_required = 0
        initial = 1
        first_after = 2

    class KindTag(Enum):
        example_arith__smart__contract__rollup__kind = 0
        wasm__2__0__0__smart__contract__rollup__kind = 1

    class Case3Field3EltTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__7 = 7
        case__8 = 8
        case__9 = 9
        case__10 = 10
        case__11 = 11
        case__12 = 12
        case__13 = 13
        case__14 = 14
        case__15 = 15
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__192 = 192
        case__193 = 193
        case__195 = 195
        case__224 = 224
        case__225 = 225
        case__226 = 226
        case__227 = 227

    class Case1Field3EltTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__7 = 7
        case__8 = 8
        case__9 = 9
        case__10 = 10
        case__11 = 11
        case__12 = 12
        case__13 = 13
        case__14 = 14
        case__15 = 15
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__192 = 192
        case__193 = 193
        case__195 = 195
        case__224 = 224
        case__225 = 225
        case__226 = 226
        case__227 = 227

    class Id014PtkathmaInlinedEndorsementMempoolContentsTag(Enum):
        endorsement = 21

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class ProofsTag(Enum):
        sparse_proof = 0
        dense_proof = 1

    class Bool(Enum):
        false = 0
        true = 255

    class Case130EltField1Tag(Enum):
        case__0 = 0
        case__1 = 1

    class PredecessorTag(Enum):
        none = 0
        some = 1

    class AmountTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3

    class Case2Field3EltTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__7 = 7
        case__8 = 8
        case__9 = 9
        case__10 = 10
        case__11 = 11
        case__12 = 12
        case__13 = 13
        case__14 = 14
        case__15 = 15
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__192 = 192
        case__193 = 193
        case__195 = 195
        case__224 = 224
        case__225 = 225
        case__226 = 226
        case__227 = 227

    class InodeTreeTag(Enum):
        blinded_inode = 0
        inode_values = 1
        inode_tree = 2
        inode_extender = 3
        none = 4

    class StepTag(Enum):
        dissection = 0
        proof = 1

    class MessageTag(Enum):
        batch = 0
        deposit = 1

    class Id014PtkathmaEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255

    class Case0Field3EltTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__7 = 7
        case__8 = 8
        case__9 = 9
        case__10 = 10
        case__11 = 11
        case__12 = 12
        case__13 = 13
        case__14 = 14
        case__15 = 15
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__192 = 192
        case__193 = 193
        case__195 = 195
        case__224 = 224
        case__225 = 225
        case__226 = 226
        case__227 = 227
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_014__ptkathma__operation = operation__shell_header.OperationShellHeader(self._io)
        self.id_014__ptkathma__operation__alpha__contents_and_signature = Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsAndSignature(self._io, self, self._root)

    class Case131EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131_elt_field0 = self._io.read_u1()
            if not self.len_case__131_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__131_elt_field0, self._io, u"/types/case__131_elt_field0_0/seq/0")
            self._raw_case__131_elt_field0 = self._io.read_bytes(self.len_case__131_elt_field0)
            _io__raw_case__131_elt_field0 = KaitaiStream(BytesIO(self._raw_case__131_elt_field0))
            self.case__131_elt_field0 = Id014PtkathmaOperation.Case131EltField0(_io__raw_case__131_elt_field0, self, self._root)


    class ArithmeticPvmWithProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.tree_proof = Id014PtkathmaOperation.TreeProof(self._io, self, self._root)
            self.given = Id014PtkathmaOperation.Given(self._io, self, self._root)
            self.requested = Id014PtkathmaOperation.Requested(self._io, self, self._root)


    class Op20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_0/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id014PtkathmaOperation.Op2(_io__raw_op2, self, self._root)


    class Case1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__1_field0 = self._io.read_s2be()
            self.case__1_field1 = self._io.read_bytes(32)
            self.case__1_field2 = self._io.read_bytes(32)
            self.case__1_field3 = Id014PtkathmaOperation.Case1Field30(self._io, self, self._root)


    class Wasm200PvmWithProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.tree_proof = Id014PtkathmaOperation.TreeProof(self._io, self, self._root)
            self.given = Id014PtkathmaOperation.Given(self._io, self, self._root)
            self.requested = Id014PtkathmaOperation.Requested(self._io, self, self._root)


    class Case192(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__192 = self._io.read_bytes_full()


    class Messages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.messages_entries = []
            i = 0
            while not self._io.is_eof():
                self.messages_entries.append(Id014PtkathmaOperation.MessagesEntries(self._io, self, self._root))
                i += 1



    class DenseProofEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dense_proof_elt = Id014PtkathmaOperation.InodeTree(self._io, self, self._root)


    class Stakers(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alice = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.bob = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)


    class MessageResultPathEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_list_hash = self._io.read_bytes(32)


    class InodeTree(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.length = self._io.read_s8be()
            self.proofs = Id014PtkathmaOperation.Proofs(self._io, self, self._root)


    class IncreasePaidStorage(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.amount = Id014PtkathmaOperation.Z(self._io, self, self._root)
            self.destination = Id014PtkathmaOperation.Id014PtkathmaContractIdOriginated(self._io, self, self._root)


    class Id014PtkathmaRollupAddress(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__rollup_address = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)


    class ScRollupPublish(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaRollupAddress(self._io, self, self._root)
            self.commitment = Id014PtkathmaOperation.Commitment0(self._io, self, self._root)


    class Case1933(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__193 = self._io.read_u2be()
            if not self.len_case__193 <= 65535:
                raise kaitaistruct.ValidationGreaterThanError(65535, self.len_case__193, self._io, u"/types/case__193_3/seq/0")
            self._raw_case__193 = self._io.read_bytes(self.len_case__193)
            _io__raw_case__193 = KaitaiStream(BytesIO(self._raw_case__193))
            self.case__193 = Id014PtkathmaOperation.Case193(_io__raw_case__193, self, self._root)


    class SparseProof2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sparse_proof = self._io.read_u4be()
            if not self.len_sparse_proof <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sparse_proof, self._io, u"/types/sparse_proof_2/seq/0")
            self._raw_sparse_proof = self._io.read_bytes(self.len_sparse_proof)
            _io__raw_sparse_proof = KaitaiStream(BytesIO(self._raw_sparse_proof))
            self.sparse_proof = Id014PtkathmaOperation.SparseProof1(_io__raw_sparse_proof, self, self._root)


    class Case8(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__8_field0 = self._io.read_u1()
            self.case__8_field1 = self._io.read_bytes(32)


    class Case10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__10_field0 = self._io.read_s4be()
            self.case__10_field1 = self._io.read_bytes(32)


    class Case2Field3Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__2_field3_elt_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Case2Field3EltTag, self._io.read_u1())
            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__8:
                self.case__8 = Id014PtkathmaOperation.Case8(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__4:
                self.case__4 = Id014PtkathmaOperation.Case4(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__12:
                self.case__12 = Id014PtkathmaOperation.Case12(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__9:
                self.case__9 = Id014PtkathmaOperation.Case9(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__5:
                self.case__5 = Id014PtkathmaOperation.Case5(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__13:
                self.case__13 = Id014PtkathmaOperation.Case13(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__10:
                self.case__10 = Id014PtkathmaOperation.Case10(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__6:
                self.case__6 = Id014PtkathmaOperation.Case6(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__14:
                self.case__14 = Id014PtkathmaOperation.Case14(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__3:
                self.case__3 = self._io.read_s8be()

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__11:
                self.case__11 = Id014PtkathmaOperation.Case11(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__7:
                self.case__7 = Id014PtkathmaOperation.Case7(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__15:
                self.case__15 = Id014PtkathmaOperation.Case15(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__129:
                self.case__129 = Id014PtkathmaOperation.Case129Entries(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__130:
                self.case__130 = Id014PtkathmaOperation.Case130Entries(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__131:
                self.case__131 = Id014PtkathmaOperation.Case1311(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__192:
                self.case__192 = Id014PtkathmaOperation.Case1921(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__193:
                self.case__193 = Id014PtkathmaOperation.Case1931(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__195:
                self.case__195 = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__224:
                self.case__224 = Id014PtkathmaOperation.Case224(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__225:
                self.case__225 = Id014PtkathmaOperation.Case225(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__226:
                self.case__226 = Id014PtkathmaOperation.Case226(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id014PtkathmaOperation.Case2Field3EltTag.case__227:
                self.case__227 = Id014PtkathmaOperation.Case227(self._io, self, self._root)



    class Case3Field30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__3_field3 = self._io.read_u4be()
            if not self.len_case__3_field3 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__3_field3, self._io, u"/types/case__3_field3_0/seq/0")
            self._raw_case__3_field3 = self._io.read_bytes(self.len_case__3_field3)
            _io__raw_case__3_field3 = KaitaiStream(BytesIO(self._raw_case__3_field3))
            self.case__3_field3 = Id014PtkathmaOperation.Case3Field3(_io__raw_case__3_field3, self, self._root)


    class ActivateAccount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = self._io.read_bytes(20)
            self.secret = self._io.read_bytes(20)


    class Case12Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__12_field1_field0 = self._io.read_bytes(32)
            self.case__12_field1_field1 = self._io.read_bytes(32)


    class DoubleEndorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id014PtkathmaOperation.Op10(self._io, self, self._root)
            self.op2 = Id014PtkathmaOperation.Op20(self._io, self, self._root)


    class Step(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.step_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.StepTag, self._io.read_u1())
            if self.step_tag == Id014PtkathmaOperation.StepTag.dissection:
                self.dissection = Id014PtkathmaOperation.Dissection0(self._io, self, self._root)

            if self.step_tag == Id014PtkathmaOperation.StepTag.proof:
                self.proof = Id014PtkathmaOperation.Proof0(self._io, self, self._root)



    class BackPointers0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_back_pointers = self._io.read_u4be()
            if not self.len_back_pointers <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_back_pointers, self._io, u"/types/back_pointers_0/seq/0")
            self._raw_back_pointers = self._io.read_bytes(self.len_back_pointers)
            _io__raw_back_pointers = KaitaiStream(BytesIO(self._raw_back_pointers))
            self.back_pointers = Id014PtkathmaOperation.BackPointers(_io__raw_back_pointers, self, self._root)


    class Case226(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__226_field0 = self._io.read_s4be()
            self.case__226_field1 = Id014PtkathmaOperation.Case226Field10(self._io, self, self._root)
            self.case__226_field2 = self._io.read_bytes(32)


    class Case9(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__9_field0 = self._io.read_u2be()
            self.case__9_field1 = self._io.read_bytes(32)


    class Case13(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__13_field0 = self._io.read_u2be()
            self.case__13_field1 = Id014PtkathmaOperation.Case13Field1(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Given(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.given_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.GivenTag, self._io.read_u1())
            if self.given_tag == Id014PtkathmaOperation.GivenTag.some:
                self.some = Id014PtkathmaOperation.Some(self._io, self, self._root)



    class Case226Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__226_field1 = self._io.read_bytes_full()


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id014PtkathmaOperation.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Case131EltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_elt_field1_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Case131EltField1Tag, self._io.read_u1())
            if self.case__131_elt_field1_tag == Id014PtkathmaOperation.Case131EltField1Tag.case__0:
                self.case__0 = self._io.read_bytes(32)

            if self.case__131_elt_field1_tag == Id014PtkathmaOperation.Case131EltField1Tag.case__1:
                self.case__1 = self._io.read_bytes(32)



    class Case227(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__227_field0 = self._io.read_s8be()
            self.case__227_field1 = Id014PtkathmaOperation.Case227Field10(self._io, self, self._root)
            self.case__227_field2 = self._io.read_bytes(32)


    class Case4(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__4_field0 = self._io.read_u1()
            self.case__4_field1 = self._io.read_bytes(32)


    class Endorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class Case130Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__130_elt_field0 = Id014PtkathmaOperation.Case130EltField00(self._io, self, self._root)
            self.case__130_elt_field1 = Id014PtkathmaOperation.Case130EltField1(self._io, self, self._root)


    class Id014PtkathmaInlinedEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__inlined__endorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id014PtkathmaOperation.Id014PtkathmaInlinedEndorsementMempoolContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Bool, self._io.read_u1())
            if self.signature_tag == Id014PtkathmaOperation.Bool.true:
                self.signature = self._io.read_bytes(64)



    class Requested(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.requested_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.RequestedTag, self._io.read_u1())
            if self.requested_tag == Id014PtkathmaOperation.RequestedTag.first_after:
                self.first_after = Id014PtkathmaOperation.FirstAfter(self._io, self, self._root)



    class Proposals0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_proposals = self._io.read_u4be()
            if not self.len_proposals <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_proposals, self._io, u"/types/proposals_0/seq/0")
            self._raw_proposals = self._io.read_bytes(self.len_proposals)
            _io__raw_proposals = KaitaiStream(BytesIO(self._raw_proposals))
            self.proposals = Id014PtkathmaOperation.Proposals(_io__raw_proposals, self, self._root)


    class MessagesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_hash = self._io.read_bytes(32)


    class Case13Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__13_field1_field0 = self._io.read_bytes(32)
            self.case__13_field1_field1 = self._io.read_bytes(32)


    class NodeEltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_node_elt_field0 = self._io.read_u1()
            if not self.len_node_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_node_elt_field0, self._io, u"/types/node_elt_field0_0/seq/0")
            self._raw_node_elt_field0 = self._io.read_bytes(self.len_node_elt_field0)
            _io__raw_node_elt_field0 = KaitaiStream(BytesIO(self._raw_node_elt_field0))
            self.node_elt_field0 = Id014PtkathmaOperation.NodeEltField0(_io__raw_node_elt_field0, self, self._root)


    class TransferTicket(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.ticket_contents = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)
            self.ticket_ty = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)
            self.ticket_ticketer = Id014PtkathmaOperation.Id014PtkathmaContractId(self._io, self, self._root)
            self.ticket_amount = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.destination = Id014PtkathmaOperation.Id014PtkathmaContractId(self._io, self, self._root)
            self.entrypoint = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)


    class Case129EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__129_elt_field0 = self._io.read_bytes_full()


    class Case1310(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131 = self._io.read_u4be()
            if not self.len_case__131 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__131, self._io, u"/types/case__131_0/seq/0")
            self._raw_case__131 = self._io.read_bytes(self.len_case__131)
            _io__raw_case__131 = KaitaiStream(BytesIO(self._raw_case__131))
            self.case__131 = Id014PtkathmaOperation.Case131(_io__raw_case__131, self, self._root)


    class Inbox(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inbox_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.InboxTag, self._io.read_u1())
            if self.inbox_tag == Id014PtkathmaOperation.InboxTag.some:
                self.some = Id014PtkathmaOperation.Some0(self._io, self, self._root)



    class Case15Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__15_field1_field0 = self._io.read_bytes(32)
            self.case__15_field1_field1 = self._io.read_bytes(32)


    class NodeEltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.node_elt_field0 = self._io.read_bytes_full()


    class Op12(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_2/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id014PtkathmaOperation.Op11(_io__raw_op1, self, self._root)


    class ScRollupExecuteOutboxMessage(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaRollupAddress(self._io, self, self._root)
            self.cemented_commitment = self._io.read_bytes(32)
            self.outbox_level = self._io.read_s4be()
            self.message_index = Id014PtkathmaOperation.Int31(self._io, self, self._root)
            self.inclusion__proof = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)
            self.message = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)


    class MessageProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version = self._io.read_s2be()
            self.before = Id014PtkathmaOperation.Before(self._io, self, self._root)
            self.after = Id014PtkathmaOperation.After(self._io, self, self._root)
            self.state = Id014PtkathmaOperation.TreeEncoding(self._io, self, self._root)


    class TxRollupRemoveCommitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaTxRollupId(self._io, self, self._root)


    class Case0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field0 = self._io.read_s2be()
            self.case__0_field1 = self._io.read_bytes(32)
            self.case__0_field2 = self._io.read_bytes(32)
            self.case__0_field3 = Id014PtkathmaOperation.Case0Field30(self._io, self, self._root)


    class Node0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_node = self._io.read_u4be()
            if not self.len_node <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_node, self._io, u"/types/node_0/seq/0")
            self._raw_node = self._io.read_bytes(self.len_node)
            _io__raw_node = KaitaiStream(BytesIO(self._raw_node))
            self.node = Id014PtkathmaOperation.Node(_io__raw_node, self, self._root)


    class Case1921(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__192 = self._io.read_u1()
            if not self.len_case__192 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__192, self._io, u"/types/case__192_1/seq/0")
            self._raw_case__192 = self._io.read_bytes(self.len_case__192)
            _io__raw_case__192 = KaitaiStream(BytesIO(self._raw_case__192))
            self.case__192 = Id014PtkathmaOperation.Case192(_io__raw_case__192, self, self._root)


    class DissectionEltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dissection_elt_field0_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.DissectionEltField0Tag, self._io.read_u1())
            if self.dissection_elt_field0_tag == Id014PtkathmaOperation.DissectionEltField0Tag.some:
                self.some = self._io.read_bytes(32)



    class Id014PtkathmaBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_hash = self._io.read_bytes(32)
            self.payload_round = self._io.read_s4be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id014PtkathmaOperation.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.liquidity_baking_toggle_vote = Id014PtkathmaOperation.Id014PtkathmaLiquidityBakingToggleVote(self._io, self, self._root)


    class Reveal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.public_key = Id014PtkathmaOperation.PublicKey(self._io, self, self._root)


    class Case1922(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__192 = self._io.read_u1()
            if not self.len_case__192 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__192, self._io, u"/types/case__192_2/seq/0")
            self._raw_case__192 = self._io.read_bytes(self.len_case__192)
            _io__raw_case__192 = KaitaiStream(BytesIO(self._raw_case__192))
            self.case__192 = Id014PtkathmaOperation.Case192(_io__raw_case__192, self, self._root)


    class Id014PtkathmaLiquidityBakingToggleVote(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__liquidity_baking_toggle_vote = self._io.read_s1()


    class MessageEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_elt = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)


    class Before(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.before_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.BeforeTag, self._io.read_u1())
            if self.before_tag == Id014PtkathmaOperation.BeforeTag.value:
                self.value = self._io.read_bytes(32)

            if self.before_tag == Id014PtkathmaOperation.BeforeTag.node:
                self.node = self._io.read_bytes(32)



    class Id014PtkathmaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__mutez = Id014PtkathmaOperation.N(self._io, self, self._root)


    class Case5(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__5_field0 = self._io.read_u2be()
            self.case__5_field1 = self._io.read_bytes(32)


    class Case193(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__193 = self._io.read_bytes_full()


    class Refutation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.choice = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.step = Id014PtkathmaOperation.Step(self._io, self, self._root)


    class ScRollupDalSlotSubscribe(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaRollupAddress(self._io, self, self._root)
            self.slot_index = self._io.read_u1()


    class SparseProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sparse_proof_entries = []
            i = 0
            while not self._io.is_eof():
                self.sparse_proof_entries.append(Id014PtkathmaOperation.SparseProofEntries(self._io, self, self._root))
                i += 1



    class Id014PtkathmaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__contract_id_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Id014PtkathmaContractIdTag, self._io.read_u1())
            if self.id_014__ptkathma__contract_id_tag == Id014PtkathmaOperation.Id014PtkathmaContractIdTag.implicit:
                self.implicit = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)

            if self.id_014__ptkathma__contract_id_tag == Id014PtkathmaOperation.Id014PtkathmaContractIdTag.originated:
                self.originated = Id014PtkathmaOperation.Originated(self._io, self, self._root)



    class ScRollupOriginate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.kind = KaitaiStream.resolve_enum(Id014PtkathmaOperation.KindTag, self._io.read_u2be())
            self.boot_sector = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)
            self.parameters_ty = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)


    class SeedNonceRevelation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.nonce = self._io.read_bytes(32)


    class Case3Field3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__3_field3_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__3_field3_entries.append(Id014PtkathmaOperation.Case3Field3Entries(self._io, self, self._root))
                i += 1



    class Case225Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__225_field1 = self._io.read_u1()
            if not self.len_case__225_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__225_field1, self._io, u"/types/case__225_field1_0/seq/0")
            self._raw_case__225_field1 = self._io.read_bytes(self.len_case__225_field1)
            _io__raw_case__225_field1 = KaitaiStream(BytesIO(self._raw_case__225_field1))
            self.case__225_field1 = Id014PtkathmaOperation.Case225Field1(_io__raw_case__225_field1, self, self._root)


    class Preendorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class Inode(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.length = self._io.read_s8be()
            self.proofs = Id014PtkathmaOperation.Proofs0(self._io, self, self._root)


    class Case11(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__11_field0 = self._io.read_s8be()
            self.case__11_field1 = self._io.read_bytes(32)


    class SkipsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.skips_elt_field0 = Id014PtkathmaOperation.SkipsEltField0(self._io, self, self._root)
            self.skips_elt_field1 = Id014PtkathmaOperation.SkipsEltField10(self._io, self, self._root)


    class TxRollupCommit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaTxRollupId(self._io, self, self._root)
            self.commitment = Id014PtkathmaOperation.Commitment(self._io, self, self._root)


    class DoubleBakingEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bh1 = Id014PtkathmaOperation.Bh10(self._io, self, self._root)
            self.bh2 = Id014PtkathmaOperation.Bh20(self._io, self, self._root)


    class Id014PtkathmaBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__block_header__alpha__unsigned_contents = Id014PtkathmaOperation.Id014PtkathmaBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes(64)


    class Extender(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.length = self._io.read_s8be()
            self.segment = Id014PtkathmaOperation.Segment0(self._io, self, self._root)
            self.proof = Id014PtkathmaOperation.InodeTree(self._io, self, self._root)


    class Case225Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__225_field1 = self._io.read_bytes_full()


    class Case129EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__129_elt_field0 = self._io.read_u1()
            if not self.len_case__129_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__129_elt_field0, self._io, u"/types/case__129_elt_field0_0/seq/0")
            self._raw_case__129_elt_field0 = self._io.read_bytes(self.len_case__129_elt_field0)
            _io__raw_case__129_elt_field0 = KaitaiStream(BytesIO(self._raw_case__129_elt_field0))
            self.case__129_elt_field0 = Id014PtkathmaOperation.Case129EltField0(_io__raw_case__129_elt_field0, self, self._root)


    class Case3Field3Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__3_field3_elt_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Case3Field3EltTag, self._io.read_u1())
            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__8:
                self.case__8 = Id014PtkathmaOperation.Case8(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__4:
                self.case__4 = Id014PtkathmaOperation.Case4(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__12:
                self.case__12 = Id014PtkathmaOperation.Case12(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__9:
                self.case__9 = Id014PtkathmaOperation.Case9(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__5:
                self.case__5 = Id014PtkathmaOperation.Case5(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__13:
                self.case__13 = Id014PtkathmaOperation.Case13(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__10:
                self.case__10 = Id014PtkathmaOperation.Case10(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__6:
                self.case__6 = Id014PtkathmaOperation.Case6(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__14:
                self.case__14 = Id014PtkathmaOperation.Case14(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__3:
                self.case__3 = self._io.read_s8be()

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__11:
                self.case__11 = Id014PtkathmaOperation.Case11(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__7:
                self.case__7 = Id014PtkathmaOperation.Case7(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__15:
                self.case__15 = Id014PtkathmaOperation.Case15(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__129:
                self.case__129 = Id014PtkathmaOperation.Case129Entries(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__130:
                self.case__130 = Id014PtkathmaOperation.Case130Entries(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__131:
                self.case__131 = Id014PtkathmaOperation.Case1313(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__192:
                self.case__192 = Id014PtkathmaOperation.Case1923(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__193:
                self.case__193 = Id014PtkathmaOperation.Case1933(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__195:
                self.case__195 = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__224:
                self.case__224 = Id014PtkathmaOperation.Case224(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__225:
                self.case__225 = Id014PtkathmaOperation.Case225(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__226:
                self.case__226 = Id014PtkathmaOperation.Case226(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id014PtkathmaOperation.Case3Field3EltTag.case__227:
                self.case__227 = Id014PtkathmaOperation.Case227(self._io, self, self._root)



    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id014PtkathmaOperation.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id014PtkathmaOperation.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id014PtkathmaOperation.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Skips0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_skips = self._io.read_u4be()
            if not self.len_skips <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_skips, self._io, u"/types/skips_0/seq/0")
            self._raw_skips = self._io.read_bytes(self.len_skips)
            _io__raw_skips = KaitaiStream(BytesIO(self._raw_skips))
            self.skips = Id014PtkathmaOperation.Skips(_io__raw_skips, self, self._root)


    class Case12(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__12_field0 = self._io.read_u1()
            self.case__12_field1 = Id014PtkathmaOperation.Case12Field1(self._io, self, self._root)


    class TxRollupDispatchTickets(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.tx_rollup = Id014PtkathmaOperation.Id014PtkathmaTxRollupId(self._io, self, self._root)
            self.level = self._io.read_s4be()
            self.context_hash = self._io.read_bytes(32)
            self.message_index = Id014PtkathmaOperation.Int31(self._io, self, self._root)
            self.message_result_path = Id014PtkathmaOperation.MessageResultPath0(self._io, self, self._root)
            self.tickets_info = Id014PtkathmaOperation.TicketsInfo0(self._io, self, self._root)


    class Bh2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__block_header__alpha__full_header = Id014PtkathmaOperation.Id014PtkathmaBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class SkipsEltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.skips_elt_field1_entries = []
            i = 0
            while not self._io.is_eof():
                self.skips_elt_field1_entries.append(Id014PtkathmaOperation.SkipsEltField1Entries(self._io, self, self._root))
                i += 1



    class PreviousMessageResultPathEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_list_hash = self._io.read_bytes(32)


    class Segment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.segment = self._io.read_bytes_full()


    class TreeEncoding(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.tree_encoding_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.TreeEncodingTag, self._io.read_u1())
            if self.tree_encoding_tag == Id014PtkathmaOperation.TreeEncodingTag.value:
                self.value = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)

            if self.tree_encoding_tag == Id014PtkathmaOperation.TreeEncodingTag.blinded_value:
                self.blinded_value = self._io.read_bytes(32)

            if self.tree_encoding_tag == Id014PtkathmaOperation.TreeEncodingTag.node:
                self.node = Id014PtkathmaOperation.Node0(self._io, self, self._root)

            if self.tree_encoding_tag == Id014PtkathmaOperation.TreeEncodingTag.blinded_node:
                self.blinded_node = self._io.read_bytes(32)

            if self.tree_encoding_tag == Id014PtkathmaOperation.TreeEncodingTag.inode:
                self.inode = Id014PtkathmaOperation.Inode(self._io, self, self._root)

            if self.tree_encoding_tag == Id014PtkathmaOperation.TreeEncodingTag.extender:
                self.extender = Id014PtkathmaOperation.Extender(self._io, self, self._root)



    class PvmStep(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pvm_step_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.PvmStepTag, self._io.read_u1())
            if self.pvm_step_tag == Id014PtkathmaOperation.PvmStepTag.arithmetic__pvm__with__proof:
                self.arithmetic__pvm__with__proof = Id014PtkathmaOperation.ArithmeticPvmWithProof(self._io, self, self._root)

            if self.pvm_step_tag == Id014PtkathmaOperation.PvmStepTag.wasm__2__0__0__pvm__with__proof:
                self.wasm__2__0__0__pvm__with__proof = Id014PtkathmaOperation.Wasm200PvmWithProof(self._io, self, self._root)



    class Case2Field30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__2_field3 = self._io.read_u4be()
            if not self.len_case__2_field3 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__2_field3, self._io, u"/types/case__2_field3_0/seq/0")
            self._raw_case__2_field3 = self._io.read_bytes(self.len_case__2_field3)
            _io__raw_case__2_field3 = KaitaiStream(BytesIO(self._raw_case__2_field3))
            self.case__2_field3 = Id014PtkathmaOperation.Case2Field3(_io__raw_case__2_field3, self, self._root)


    class ScRollupCement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaRollupAddress(self._io, self, self._root)
            self.commitment = self._io.read_bytes(32)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class InodeValues(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inode_values_entries = []
            i = 0
            while not self._io.is_eof():
                self.inode_values_entries.append(Id014PtkathmaOperation.InodeValuesEntries(self._io, self, self._root))
                i += 1



    class Amount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.AmountTag, self._io.read_u1())
            if self.amount_tag == Id014PtkathmaOperation.AmountTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.amount_tag == Id014PtkathmaOperation.AmountTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.amount_tag == Id014PtkathmaOperation.AmountTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.amount_tag == Id014PtkathmaOperation.AmountTag.case__3:
                self.case__3 = self._io.read_s8be()



    class Case1311(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131 = self._io.read_u4be()
            if not self.len_case__131 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__131, self._io, u"/types/case__131_1/seq/0")
            self._raw_case__131 = self._io.read_bytes(self.len_case__131)
            _io__raw_case__131 = KaitaiStream(BytesIO(self._raw_case__131))
            self.case__131 = Id014PtkathmaOperation.Case131(_io__raw_case__131, self, self._root)


    class Case129EltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__129_elt_field1_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Case129EltField1Tag, self._io.read_u1())
            if self.case__129_elt_field1_tag == Id014PtkathmaOperation.Case129EltField1Tag.case__0:
                self.case__0 = self._io.read_bytes(32)

            if self.case__129_elt_field1_tag == Id014PtkathmaOperation.Case129EltField1Tag.case__1:
                self.case__1 = self._io.read_bytes(32)



    class Case131(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__131_entries.append(Id014PtkathmaOperation.Case131Entries(self._io, self, self._root))
                i += 1



    class Bh20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh2 = self._io.read_u4be()
            if not self.len_bh2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh2, self._io, u"/types/bh2_0/seq/0")
            self._raw_bh2 = self._io.read_bytes(self.len_bh2)
            _io__raw_bh2 = KaitaiStream(BytesIO(self._raw_bh2))
            self.bh2 = Id014PtkathmaOperation.Bh2(_io__raw_bh2, self, self._root)


    class Commitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.messages = Id014PtkathmaOperation.Messages0(self._io, self, self._root)
            self.predecessor = Id014PtkathmaOperation.Predecessor(self._io, self, self._root)
            self.inbox_merkle_root = self._io.read_bytes(32)


    class Case2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__2_field0 = self._io.read_s2be()
            self.case__2_field1 = self._io.read_bytes(32)
            self.case__2_field2 = self._io.read_bytes(32)
            self.case__2_field3 = Id014PtkathmaOperation.Case2Field30(self._io, self, self._root)


    class TicketsInfoEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)
            self.ty = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)
            self.ticketer = Id014PtkathmaOperation.Id014PtkathmaContractId(self._io, self, self._root)
            self.amount = Id014PtkathmaOperation.Amount(self._io, self, self._root)
            self.claimer = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Bool, self._io.read_u1())
            if self.delegate_tag == Id014PtkathmaOperation.Bool.true:
                self.delegate = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)



    class Id014PtkathmaOperationAlphaContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__operation__alpha__contents_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag, self._io.read_u1())
            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.endorsement:
                self.endorsement = Id014PtkathmaOperation.Endorsement(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.preendorsement:
                self.preendorsement = Id014PtkathmaOperation.Preendorsement(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.dal_slot_availability:
                self.dal_slot_availability = Id014PtkathmaOperation.DalSlotAvailability(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.seed_nonce_revelation:
                self.seed_nonce_revelation = Id014PtkathmaOperation.SeedNonceRevelation(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.vdf_revelation:
                self.vdf_revelation = Id014PtkathmaOperation.Solution(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.double_endorsement_evidence:
                self.double_endorsement_evidence = Id014PtkathmaOperation.DoubleEndorsementEvidence(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.double_preendorsement_evidence:
                self.double_preendorsement_evidence = Id014PtkathmaOperation.DoublePreendorsementEvidence(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.double_baking_evidence:
                self.double_baking_evidence = Id014PtkathmaOperation.DoubleBakingEvidence(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.activate_account:
                self.activate_account = Id014PtkathmaOperation.ActivateAccount(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.proposals:
                self.proposals = Id014PtkathmaOperation.Proposals1(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.ballot:
                self.ballot = Id014PtkathmaOperation.Ballot(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.reveal:
                self.reveal = Id014PtkathmaOperation.Reveal(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.transaction:
                self.transaction = Id014PtkathmaOperation.Transaction(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.origination:
                self.origination = Id014PtkathmaOperation.Origination(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.delegation:
                self.delegation = Id014PtkathmaOperation.Delegation(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.set_deposits_limit:
                self.set_deposits_limit = Id014PtkathmaOperation.SetDepositsLimit(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.increase_paid_storage:
                self.increase_paid_storage = Id014PtkathmaOperation.IncreasePaidStorage(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.failing_noop:
                self.failing_noop = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.register_global_constant:
                self.register_global_constant = Id014PtkathmaOperation.RegisterGlobalConstant(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.tx_rollup_origination:
                self.tx_rollup_origination = Id014PtkathmaOperation.TxRollupOrigination(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.tx_rollup_submit_batch:
                self.tx_rollup_submit_batch = Id014PtkathmaOperation.TxRollupSubmitBatch(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.tx_rollup_commit:
                self.tx_rollup_commit = Id014PtkathmaOperation.TxRollupCommit(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.tx_rollup_return_bond:
                self.tx_rollup_return_bond = Id014PtkathmaOperation.TxRollupReturnBond(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.tx_rollup_finalize_commitment:
                self.tx_rollup_finalize_commitment = Id014PtkathmaOperation.TxRollupFinalizeCommitment(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.tx_rollup_remove_commitment:
                self.tx_rollup_remove_commitment = Id014PtkathmaOperation.TxRollupRemoveCommitment(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.tx_rollup_rejection:
                self.tx_rollup_rejection = Id014PtkathmaOperation.TxRollupRejection(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.tx_rollup_dispatch_tickets:
                self.tx_rollup_dispatch_tickets = Id014PtkathmaOperation.TxRollupDispatchTickets(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.transfer_ticket:
                self.transfer_ticket = Id014PtkathmaOperation.TransferTicket(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.dal_publish_slot_header:
                self.dal_publish_slot_header = Id014PtkathmaOperation.DalPublishSlotHeader(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.sc_rollup_originate:
                self.sc_rollup_originate = Id014PtkathmaOperation.ScRollupOriginate(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.sc_rollup_add_messages:
                self.sc_rollup_add_messages = Id014PtkathmaOperation.ScRollupAddMessages(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.sc_rollup_cement:
                self.sc_rollup_cement = Id014PtkathmaOperation.ScRollupCement(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.sc_rollup_publish:
                self.sc_rollup_publish = Id014PtkathmaOperation.ScRollupPublish(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.sc_rollup_refute:
                self.sc_rollup_refute = Id014PtkathmaOperation.ScRollupRefute(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.sc_rollup_timeout:
                self.sc_rollup_timeout = Id014PtkathmaOperation.ScRollupTimeout(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.sc_rollup_execute_outbox_message:
                self.sc_rollup_execute_outbox_message = Id014PtkathmaOperation.ScRollupExecuteOutboxMessage(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.sc_rollup_recover_bond:
                self.sc_rollup_recover_bond = Id014PtkathmaOperation.ScRollupRecoverBond(self._io, self, self._root)

            if self.id_014__ptkathma__operation__alpha__contents_tag == Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContentsTag.sc_rollup_dal_slot_subscribe:
                self.sc_rollup_dal_slot_subscribe = Id014PtkathmaOperation.ScRollupDalSlotSubscribe(self._io, self, self._root)



    class Case1923(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__192 = self._io.read_u1()
            if not self.len_case__192 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__192, self._io, u"/types/case__192_3/seq/0")
            self._raw_case__192 = self._io.read_bytes(self.len_case__192)
            _io__raw_case__192 = KaitaiStream(BytesIO(self._raw_case__192))
            self.case__192 = Id014PtkathmaOperation.Case192(_io__raw_case__192, self, self._root)


    class Segment0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_segment = self._io.read_u1()
            if not self.len_segment <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_segment, self._io, u"/types/segment_0/seq/0")
            self._raw_segment = self._io.read_bytes(self.len_segment)
            _io__raw_segment = KaitaiStream(BytesIO(self._raw_segment))
            self.segment = Id014PtkathmaOperation.Segment(_io__raw_segment, self, self._root)


    class RegisterGlobalConstant(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.value = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)


    class Solution(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.solution_field0 = self._io.read_bytes(100)
            self.solution_field1 = self._io.read_bytes(100)


    class Case129Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__129_elt_field0 = Id014PtkathmaOperation.Case129EltField00(self._io, self, self._root)
            self.case__129_elt_field1 = Id014PtkathmaOperation.Case129EltField1(self._io, self, self._root)


    class ScRollupRecoverBond(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)


    class FirstAfter(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.first_after_field0 = self._io.read_s4be()
            self.first_after_field1 = Id014PtkathmaOperation.N(self._io, self, self._root)


    class Case2Field3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__2_field3_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__2_field3_entries.append(Id014PtkathmaOperation.Case2Field3Entries(self._io, self, self._root))
                i += 1



    class PreviousMessageResult(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.context_hash = self._io.read_bytes(32)
            self.withdraw_list_hash = self._io.read_bytes(32)


    class Case1Field30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__1_field3 = self._io.read_u4be()
            if not self.len_case__1_field3 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__1_field3, self._io, u"/types/case__1_field3_0/seq/0")
            self._raw_case__1_field3 = self._io.read_bytes(self.len_case__1_field3)
            _io__raw_case__1_field3 = KaitaiStream(BytesIO(self._raw_case__1_field3))
            self.case__1_field3 = Id014PtkathmaOperation.Case1Field3(_io__raw_case__1_field3, self, self._root)


    class SparseProofEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sparse_proof_elt_field0 = self._io.read_u1()
            self.sparse_proof_elt_field1 = Id014PtkathmaOperation.InodeTree(self._io, self, self._root)


    class Case130EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__130_elt_field0 = self._io.read_bytes_full()


    class SkipsEltField10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_skips_elt_field1 = self._io.read_u4be()
            if not self.len_skips_elt_field1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_skips_elt_field1, self._io, u"/types/skips_elt_field1_0/seq/0")
            self._raw_skips_elt_field1 = self._io.read_bytes(self.len_skips_elt_field1)
            _io__raw_skips_elt_field1 = KaitaiStream(BytesIO(self._raw_skips_elt_field1))
            self.skips_elt_field1 = Id014PtkathmaOperation.SkipsEltField1(_io__raw_skips_elt_field1, self, self._root)


    class Case6(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__6_field0 = self._io.read_s4be()
            self.case__6_field1 = self._io.read_bytes(32)


    class ContentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__operation__alpha__contents = Id014PtkathmaOperation.Id014PtkathmaOperationAlphaContents(self._io, self, self._root)


    class PreviousMessageResultPath(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.previous_message_result_path_entries = []
            i = 0
            while not self._io.is_eof():
                self.previous_message_result_path_entries.append(Id014PtkathmaOperation.PreviousMessageResultPathEntries(self._io, self, self._root))
                i += 1



    class IncEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.index = Id014PtkathmaOperation.Int31(self._io, self, self._root)
            self.content = self._io.read_bytes(32)
            self.back_pointers = Id014PtkathmaOperation.BackPointers0(self._io, self, self._root)


    class ScRollupAddMessages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaRollupAddress(self._io, self, self._root)
            self.message = Id014PtkathmaOperation.Message1(self._io, self, self._root)


    class Case1Field3Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__1_field3_elt_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Case1Field3EltTag, self._io.read_u1())
            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__8:
                self.case__8 = Id014PtkathmaOperation.Case8(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__4:
                self.case__4 = Id014PtkathmaOperation.Case4(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__12:
                self.case__12 = Id014PtkathmaOperation.Case12(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__9:
                self.case__9 = Id014PtkathmaOperation.Case9(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__5:
                self.case__5 = Id014PtkathmaOperation.Case5(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__13:
                self.case__13 = Id014PtkathmaOperation.Case13(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__10:
                self.case__10 = Id014PtkathmaOperation.Case10(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__6:
                self.case__6 = Id014PtkathmaOperation.Case6(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__14:
                self.case__14 = Id014PtkathmaOperation.Case14(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__3:
                self.case__3 = self._io.read_s8be()

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__11:
                self.case__11 = Id014PtkathmaOperation.Case11(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__7:
                self.case__7 = Id014PtkathmaOperation.Case7(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__15:
                self.case__15 = Id014PtkathmaOperation.Case15(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__129:
                self.case__129 = Id014PtkathmaOperation.Case129Entries(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__130:
                self.case__130 = Id014PtkathmaOperation.Case130Entries(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__131:
                self.case__131 = Id014PtkathmaOperation.Case1312(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__192:
                self.case__192 = Id014PtkathmaOperation.Case1922(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__193:
                self.case__193 = Id014PtkathmaOperation.Case1932(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__195:
                self.case__195 = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__224:
                self.case__224 = Id014PtkathmaOperation.Case224(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__225:
                self.case__225 = Id014PtkathmaOperation.Case225(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__226:
                self.case__226 = Id014PtkathmaOperation.Case226(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id014PtkathmaOperation.Case1Field3EltTag.case__227:
                self.case__227 = Id014PtkathmaOperation.Case227(self._io, self, self._root)



    class PreviousMessageResultPath0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_previous_message_result_path = self._io.read_u4be()
            if not self.len_previous_message_result_path <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_previous_message_result_path, self._io, u"/types/previous_message_result_path_0/seq/0")
            self._raw_previous_message_result_path = self._io.read_bytes(self.len_previous_message_result_path)
            _io__raw_previous_message_result_path = KaitaiStream(BytesIO(self._raw_previous_message_result_path))
            self.previous_message_result_path = Id014PtkathmaOperation.PreviousMessageResultPath(_io__raw_previous_message_result_path, self, self._root)


    class Case0Field3Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field3_elt_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Case0Field3EltTag, self._io.read_u1())
            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__8:
                self.case__8 = Id014PtkathmaOperation.Case8(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__4:
                self.case__4 = Id014PtkathmaOperation.Case4(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__12:
                self.case__12 = Id014PtkathmaOperation.Case12(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__9:
                self.case__9 = Id014PtkathmaOperation.Case9(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__5:
                self.case__5 = Id014PtkathmaOperation.Case5(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__13:
                self.case__13 = Id014PtkathmaOperation.Case13(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__10:
                self.case__10 = Id014PtkathmaOperation.Case10(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__6:
                self.case__6 = Id014PtkathmaOperation.Case6(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__14:
                self.case__14 = Id014PtkathmaOperation.Case14(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__3:
                self.case__3 = self._io.read_s8be()

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__11:
                self.case__11 = Id014PtkathmaOperation.Case11(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__7:
                self.case__7 = Id014PtkathmaOperation.Case7(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__15:
                self.case__15 = Id014PtkathmaOperation.Case15(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__129:
                self.case__129 = Id014PtkathmaOperation.Case129Entries(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__130:
                self.case__130 = Id014PtkathmaOperation.Case130Entries(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__131:
                self.case__131 = Id014PtkathmaOperation.Case1310(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__192:
                self.case__192 = Id014PtkathmaOperation.Case1920(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__193:
                self.case__193 = Id014PtkathmaOperation.Case1930(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__195:
                self.case__195 = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__224:
                self.case__224 = Id014PtkathmaOperation.Case224(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__225:
                self.case__225 = Id014PtkathmaOperation.Case225(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__226:
                self.case__226 = Id014PtkathmaOperation.Case226(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id014PtkathmaOperation.Case0Field3EltTag.case__227:
                self.case__227 = Id014PtkathmaOperation.Case227(self._io, self, self._root)



    class Bh10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh1 = self._io.read_u4be()
            if not self.len_bh1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh1, self._io, u"/types/bh1_0/seq/0")
            self._raw_bh1 = self._io.read_bytes(self.len_bh1)
            _io__raw_bh1 = KaitaiStream(BytesIO(self._raw_bh1))
            self.bh1 = Id014PtkathmaOperation.Bh1(_io__raw_bh1, self, self._root)


    class Some0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.skips = Id014PtkathmaOperation.Skips0(self._io, self, self._root)
            self.level = Id014PtkathmaOperation.Level(self._io, self, self._root)
            self.inc = Id014PtkathmaOperation.Inc0(self._io, self, self._root)
            self.message_proof = Id014PtkathmaOperation.MessageProof(self._io, self, self._root)


    class OldLevelsMessages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.index = Id014PtkathmaOperation.Int31(self._io, self, self._root)
            self.content = self._io.read_bytes(32)
            self.back_pointers = Id014PtkathmaOperation.BackPointers0(self._io, self, self._root)


    class After(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.after_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.AfterTag, self._io.read_u1())
            if self.after_tag == Id014PtkathmaOperation.AfterTag.value:
                self.value = self._io.read_bytes(32)

            if self.after_tag == Id014PtkathmaOperation.AfterTag.node:
                self.node = self._io.read_bytes(32)



    class Case131EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_elt_field0 = self._io.read_bytes_full()


    class Case3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__3_field0 = self._io.read_s2be()
            self.case__3_field1 = self._io.read_bytes(32)
            self.case__3_field2 = self._io.read_bytes(32)
            self.case__3_field3 = Id014PtkathmaOperation.Case3Field30(self._io, self, self._root)


    class Case224(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__224_field0 = self._io.read_u1()
            self.case__224_field1 = Id014PtkathmaOperation.Case224Field10(self._io, self, self._root)
            self.case__224_field2 = self._io.read_bytes(32)


    class Op22(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_2/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id014PtkathmaOperation.Op21(_io__raw_op2, self, self._root)


    class Case14Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__14_field1_field0 = self._io.read_bytes(32)
            self.case__14_field1_field1 = self._io.read_bytes(32)


    class Skips(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.skips_entries = []
            i = 0
            while not self._io.is_eof():
                self.skips_entries.append(Id014PtkathmaOperation.SkipsEntries(self._io, self, self._root))
                i += 1



    class Inc0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_inc = self._io.read_u4be()
            if not self.len_inc <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_inc, self._io, u"/types/inc_0/seq/0")
            self._raw_inc = self._io.read_bytes(self.len_inc)
            _io__raw_inc = KaitaiStream(BytesIO(self._raw_inc))
            self.inc = Id014PtkathmaOperation.Inc(_io__raw_inc, self, self._root)


    class ScRollupTimeout(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaRollupAddress(self._io, self, self._root)
            self.stakers = Id014PtkathmaOperation.Stakers(self._io, self, self._root)


    class Case227Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__227_field1 = self._io.read_u1()
            if not self.len_case__227_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__227_field1, self._io, u"/types/case__227_field1_0/seq/0")
            self._raw_case__227_field1 = self._io.read_bytes(self.len_case__227_field1)
            _io__raw_case__227_field1 = KaitaiStream(BytesIO(self._raw_case__227_field1))
            self.case__227_field1 = Id014PtkathmaOperation.Case227Field1(_io__raw_case__227_field1, self, self._root)


    class SparseProof0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sparse_proof = self._io.read_u4be()
            if not self.len_sparse_proof <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sparse_proof, self._io, u"/types/sparse_proof_0/seq/0")
            self._raw_sparse_proof = self._io.read_bytes(self.len_sparse_proof)
            _io__raw_sparse_proof = KaitaiStream(BytesIO(self._raw_sparse_proof))
            self.sparse_proof = Id014PtkathmaOperation.SparseProof(_io__raw_sparse_proof, self, self._root)


    class Message0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_entries.append(Id014PtkathmaOperation.MessageEntries(self._io, self, self._root))
                i += 1



    class Case1930(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__193 = self._io.read_u2be()
            if not self.len_case__193 <= 65535:
                raise kaitaistruct.ValidationGreaterThanError(65535, self.len_case__193, self._io, u"/types/case__193_0/seq/0")
            self._raw_case__193 = self._io.read_bytes(self.len_case__193)
            _io__raw_case__193 = KaitaiStream(BytesIO(self._raw_case__193))
            self.case__193 = Id014PtkathmaOperation.Case193(_io__raw_case__193, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Case131Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_elt_field0 = Id014PtkathmaOperation.Case131EltField00(self._io, self, self._root)
            self.case__131_elt_field1 = Id014PtkathmaOperation.Case131EltField1(self._io, self, self._root)


    class Bh1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__block_header__alpha__full_header = Id014PtkathmaOperation.Id014PtkathmaBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class SkipsEltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaRollupAddress(self._io, self, self._root)
            self.message_counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.nb_available_messages = self._io.read_s8be()
            self.nb_messages_in_commitment_period = self._io.read_s8be()
            self.starting_level_of_current_commitment_period = self._io.read_s4be()
            self.level = self._io.read_s4be()
            self.current_messages_hash = self._io.read_bytes(32)
            self.old_levels_messages = Id014PtkathmaOperation.OldLevelsMessages(self._io, self, self._root)


    class Dissection0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_dissection = self._io.read_u4be()
            if not self.len_dissection <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_dissection, self._io, u"/types/dissection_0/seq/0")
            self._raw_dissection = self._io.read_bytes(self.len_dissection)
            _io__raw_dissection = KaitaiStream(BytesIO(self._raw_dissection))
            self.dissection = Id014PtkathmaOperation.Dissection(_io__raw_dissection, self, self._root)


    class Case0Field3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field3_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__0_field3_entries.append(Id014PtkathmaOperation.Case0Field3Entries(self._io, self, self._root))
                i += 1



    class TxRollupRejection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaTxRollupId(self._io, self, self._root)
            self.level = self._io.read_s4be()
            self.message = Id014PtkathmaOperation.Message(self._io, self, self._root)
            self.message_position = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.message_path = Id014PtkathmaOperation.MessagePath0(self._io, self, self._root)
            self.message_result_hash = self._io.read_bytes(32)
            self.message_result_path = Id014PtkathmaOperation.MessageResultPath0(self._io, self, self._root)
            self.previous_message_result = Id014PtkathmaOperation.PreviousMessageResult(self._io, self, self._root)
            self.previous_message_result_path = Id014PtkathmaOperation.PreviousMessageResultPath0(self._io, self, self._root)
            self.proof = Id014PtkathmaOperation.Proof(self._io, self, self._root)


    class SetDepositsLimit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.limit_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Bool, self._io.read_u1())
            if self.limit_tag == Id014PtkathmaOperation.Bool.true:
                self.limit = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)



    class Case15(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__15_field0 = self._io.read_s8be()
            self.case__15_field1 = Id014PtkathmaOperation.Case15Field1(self._io, self, self._root)


    class Message1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message = self._io.read_u4be()
            if not self.len_message <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message, self._io, u"/types/message_1/seq/0")
            self._raw_message = self._io.read_bytes(self.len_message)
            _io__raw_message = KaitaiStream(BytesIO(self._raw_message))
            self.message = Id014PtkathmaOperation.Message0(_io__raw_message, self, self._root)


    class ScRollupRefute(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaRollupAddress(self._io, self, self._root)
            self.opponent = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.refutation = Id014PtkathmaOperation.Refutation(self._io, self, self._root)
            self.is_opening_move = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Bool, self._io.read_u1())


    class Op10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_0/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id014PtkathmaOperation.Op1(_io__raw_op1, self, self._root)


    class InodeExtender(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.length = self._io.read_s8be()
            self.segment = Id014PtkathmaOperation.Segment0(self._io, self, self._root)
            self.proof = Id014PtkathmaOperation.InodeTree(self._io, self, self._root)


    class Op21(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__inlined__preendorsement = Id014PtkathmaOperation.Id014PtkathmaInlinedPreendorsement(self._io, self, self._root)


    class Proof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proof_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.ProofTag, self._io.read_u1())
            if self.proof_tag == Id014PtkathmaOperation.ProofTag.case__0:
                self.case__0 = Id014PtkathmaOperation.Case0(self._io, self, self._root)

            if self.proof_tag == Id014PtkathmaOperation.ProofTag.case__2:
                self.case__2 = Id014PtkathmaOperation.Case2(self._io, self, self._root)

            if self.proof_tag == Id014PtkathmaOperation.ProofTag.case__1:
                self.case__1 = Id014PtkathmaOperation.Case1(self._io, self, self._root)

            if self.proof_tag == Id014PtkathmaOperation.ProofTag.case__3:
                self.case__3 = Id014PtkathmaOperation.Case3(self._io, self, self._root)



    class Id014PtkathmaContractIdOriginated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__contract_id__originated_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Id014PtkathmaContractIdOriginatedTag, self._io.read_u1())
            if self.id_014__ptkathma__contract_id__originated_tag == Id014PtkathmaOperation.Id014PtkathmaContractIdOriginatedTag.originated:
                self.originated = Id014PtkathmaOperation.Originated(self._io, self, self._root)



    class Commitment0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.compressed_state = self._io.read_bytes(32)
            self.inbox_level = self._io.read_s4be()
            self.predecessor = self._io.read_bytes(32)
            self.number_of_messages = self._io.read_s4be()
            self.number_of_ticks = self._io.read_s4be()


    class Inc(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inc_entries = []
            i = 0
            while not self._io.is_eof():
                self.inc_entries.append(Id014PtkathmaOperation.IncEntries(self._io, self, self._root))
                i += 1



    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.balance = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Bool, self._io.read_u1())
            if self.delegate_tag == Id014PtkathmaOperation.Bool.true:
                self.delegate = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)

            self.script = Id014PtkathmaOperation.Id014PtkathmaScriptedContracts(self._io, self, self._root)


    class Case226Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__226_field1 = self._io.read_u1()
            if not self.len_case__226_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__226_field1, self._io, u"/types/case__226_field1_0/seq/0")
            self._raw_case__226_field1 = self._io.read_bytes(self.len_case__226_field1)
            _io__raw_case__226_field1 = KaitaiStream(BytesIO(self._raw_case__226_field1))
            self.case__226_field1 = Id014PtkathmaOperation.Case226Field1(_io__raw_case__226_field1, self, self._root)


    class BackPointers(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.back_pointers_entries = []
            i = 0
            while not self._io.is_eof():
                self.back_pointers_entries.append(Id014PtkathmaOperation.BackPointersEntries(self._io, self, self._root))
                i += 1



    class Case1312(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131 = self._io.read_u4be()
            if not self.len_case__131 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__131, self._io, u"/types/case__131_2/seq/0")
            self._raw_case__131 = self._io.read_bytes(self.len_case__131)
            _io__raw_case__131 = KaitaiStream(BytesIO(self._raw_case__131))
            self.case__131 = Id014PtkathmaOperation.Case131(_io__raw_case__131, self, self._root)


    class Case130EltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__130_elt_field1_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Case130EltField1Tag, self._io.read_u1())
            if self.case__130_elt_field1_tag == Id014PtkathmaOperation.Case130EltField1Tag.case__0:
                self.case__0 = self._io.read_bytes(32)

            if self.case__130_elt_field1_tag == Id014PtkathmaOperation.Case130EltField1Tag.case__1:
                self.case__1 = self._io.read_bytes(32)



    class Message(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.MessageTag, self._io.read_u1())
            if self.message_tag == Id014PtkathmaOperation.MessageTag.batch:
                self.batch = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)

            if self.message_tag == Id014PtkathmaOperation.MessageTag.deposit:
                self.deposit = Id014PtkathmaOperation.Deposit(self._io, self, self._root)



    class Case1932(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__193 = self._io.read_u2be()
            if not self.len_case__193 <= 65535:
                raise kaitaistruct.ValidationGreaterThanError(65535, self.len_case__193, self._io, u"/types/case__193_2/seq/0")
            self._raw_case__193 = self._io.read_bytes(self.len_case__193)
            _io__raw_case__193 = KaitaiStream(BytesIO(self._raw_case__193))
            self.case__193 = Id014PtkathmaOperation.Case193(_io__raw_case__193, self, self._root)


    class Node(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.node_entries = []
            i = 0
            while not self._io.is_eof():
                self.node_entries.append(Id014PtkathmaOperation.NodeEntries(self._io, self, self._root))
                i += 1



    class Op2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__inlined__endorsement = Id014PtkathmaOperation.Id014PtkathmaInlinedEndorsement(self._io, self, self._root)


    class Proofs0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proofs_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.ProofsTag, self._io.read_u1())
            if self.proofs_tag == Id014PtkathmaOperation.ProofsTag.sparse_proof:
                self.sparse_proof = Id014PtkathmaOperation.SparseProof2(self._io, self, self._root)

            if self.proofs_tag == Id014PtkathmaOperation.ProofsTag.dense_proof:
                self.dense_proof = Id014PtkathmaOperation.DenseProofEntries(self._io, self, self._root)



    class Case7(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__7_field0 = self._io.read_s8be()
            self.case__7_field1 = self._io.read_bytes(32)


    class NodeEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.node_elt_field0 = Id014PtkathmaOperation.NodeEltField00(self._io, self, self._root)
            self.node_elt_field1 = Id014PtkathmaOperation.TreeEncoding(self._io, self, self._root)


    class TxRollupReturnBond(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaTxRollupId(self._io, self, self._root)


    class Dissection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dissection_entries = []
            i = 0
            while not self._io.is_eof():
                self.dissection_entries.append(Id014PtkathmaOperation.DissectionEntries(self._io, self, self._root))
                i += 1



    class Case224Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__224_field1 = self._io.read_u1()
            if not self.len_case__224_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__224_field1, self._io, u"/types/case__224_field1_0/seq/0")
            self._raw_case__224_field1 = self._io.read_bytes(self.len_case__224_field1)
            _io__raw_case__224_field1 = KaitaiStream(BytesIO(self._raw_case__224_field1))
            self.case__224_field1 = Id014PtkathmaOperation.Case224Field1(_io__raw_case__224_field1, self, self._root)


    class TicketsInfo0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_tickets_info = self._io.read_u4be()
            if not self.len_tickets_info <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_tickets_info, self._io, u"/types/tickets_info_0/seq/0")
            self._raw_tickets_info = self._io.read_bytes(self.len_tickets_info)
            _io__raw_tickets_info = KaitaiStream(BytesIO(self._raw_tickets_info))
            self.tickets_info = Id014PtkathmaOperation.TicketsInfo(_io__raw_tickets_info, self, self._root)


    class TxRollupFinalizeCommitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaTxRollupId(self._io, self, self._root)


    class Id014PtkathmaScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)
            self.storage = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)


    class InodeTree0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inode_tree_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.InodeTreeTag, self._io.read_u1())
            if self.inode_tree_tag == Id014PtkathmaOperation.InodeTreeTag.blinded_inode:
                self.blinded_inode = self._io.read_bytes(32)

            if self.inode_tree_tag == Id014PtkathmaOperation.InodeTreeTag.inode_values:
                self.inode_values = Id014PtkathmaOperation.InodeValues0(self._io, self, self._root)

            if self.inode_tree_tag == Id014PtkathmaOperation.InodeTreeTag.inode_tree:
                self.inode_tree = Id014PtkathmaOperation.InodeTree(self._io, self, self._root)

            if self.inode_tree_tag == Id014PtkathmaOperation.InodeTreeTag.inode_extender:
                self.inode_extender = Id014PtkathmaOperation.InodeExtender(self._io, self, self._root)



    class TxRollupOrigination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)


    class Case227Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__227_field1 = self._io.read_bytes_full()


    class MessageResultPath(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_path_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_result_path_entries.append(Id014PtkathmaOperation.MessageResultPathEntries(self._io, self, self._root))
                i += 1



    class Id014PtkathmaInlinedEndorsementMempoolContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__inlined__endorsement_mempool__contents_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Id014PtkathmaInlinedEndorsementMempoolContentsTag, self._io.read_u1())
            if self.id_014__ptkathma__inlined__endorsement_mempool__contents_tag == Id014PtkathmaOperation.Id014PtkathmaInlinedEndorsementMempoolContentsTag.endorsement:
                self.endorsement = Id014PtkathmaOperation.Endorsement(self._io, self, self._root)



    class Id014PtkathmaOperationAlphaContentsAndSignature(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents = []
            i = 0
            while not self._io.is_eof():
                self.contents.append(Id014PtkathmaOperation.ContentsEntries(self._io, self, self._root))
                i += 1

            self.signature = self._io.read_bytes(64)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class MessageResultPath0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message_result_path = self._io.read_u4be()
            if not self.len_message_result_path <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message_result_path, self._io, u"/types/message_result_path_0/seq/0")
            self._raw_message_result_path = self._io.read_bytes(self.len_message_result_path)
            _io__raw_message_result_path = KaitaiStream(BytesIO(self._raw_message_result_path))
            self.message_result_path = Id014PtkathmaOperation.MessageResultPath(_io__raw_message_result_path, self, self._root)


    class InodeValuesEltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inode_values_elt_field0 = self._io.read_bytes_full()


    class InodeValuesEltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_inode_values_elt_field0 = self._io.read_u1()
            if not self.len_inode_values_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_inode_values_elt_field0, self._io, u"/types/inode_values_elt_field0_0/seq/0")
            self._raw_inode_values_elt_field0 = self._io.read_bytes(self.len_inode_values_elt_field0)
            _io__raw_inode_values_elt_field0 = KaitaiStream(BytesIO(self._raw_inode_values_elt_field0))
            self.inode_values_elt_field0 = Id014PtkathmaOperation.InodeValuesEltField0(_io__raw_inode_values_elt_field0, self, self._root)


    class MessagePathEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inbox_list_hash = self._io.read_bytes(32)


    class Id014PtkathmaEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__entrypoint_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Id014PtkathmaEntrypointTag, self._io.read_u1())
            if self.id_014__ptkathma__entrypoint_tag == Id014PtkathmaOperation.Id014PtkathmaEntrypointTag.named:
                self.named = Id014PtkathmaOperation.Named0(self._io, self, self._root)



    class Proofs(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proofs_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.ProofsTag, self._io.read_u1())
            if self.proofs_tag == Id014PtkathmaOperation.ProofsTag.sparse_proof:
                self.sparse_proof = Id014PtkathmaOperation.SparseProof0(self._io, self, self._root)

            if self.proofs_tag == Id014PtkathmaOperation.ProofsTag.dense_proof:
                self.dense_proof = Id014PtkathmaOperation.DenseProofEntries(self._io, self, self._root)



    class SparseProof1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sparse_proof_entries = []
            i = 0
            while not self._io.is_eof():
                self.sparse_proof_entries.append(Id014PtkathmaOperation.SparseProofEntries0(self._io, self, self._root))
                i += 1



    class SkipsEltField1Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.index = Id014PtkathmaOperation.Int31(self._io, self, self._root)
            self.content = self._io.read_bytes(32)
            self.back_pointers = Id014PtkathmaOperation.BackPointers0(self._io, self, self._root)


    class TreeProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.version = self._io.read_s2be()
            self.before = Id014PtkathmaOperation.Before(self._io, self, self._root)
            self.after = Id014PtkathmaOperation.After(self._io, self, self._root)
            self.state = Id014PtkathmaOperation.TreeEncoding(self._io, self, self._root)


    class DalSlotAvailability(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.endorser = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.endorsement = Id014PtkathmaOperation.Z(self._io, self, self._root)


    class Id014PtkathmaTxRollupId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup_hash = self._io.read_bytes(20)


    class Slot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.index = self._io.read_u1()
            self.header = Id014PtkathmaOperation.Int31(self._io, self, self._root)


    class Id014PtkathmaInlinedPreendorsementContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__inlined__preendorsement__contents_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Id014PtkathmaInlinedPreendorsementContentsTag, self._io.read_u1())
            if self.id_014__ptkathma__inlined__preendorsement__contents_tag == Id014PtkathmaOperation.Id014PtkathmaInlinedPreendorsementContentsTag.preendorsement:
                self.preendorsement = Id014PtkathmaOperation.Preendorsement(self._io, self, self._root)



    class Deposit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sender = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.destination = self._io.read_bytes(20)
            self.ticket_hash = self._io.read_bytes(32)
            self.amount = Id014PtkathmaOperation.Amount(self._io, self, self._root)


    class MessagePath(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_path_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_path_entries.append(Id014PtkathmaOperation.MessagePathEntries(self._io, self, self._root))
                i += 1



    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id014PtkathmaOperation.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.amount = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.destination = Id014PtkathmaOperation.Id014PtkathmaContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Bool, self._io.read_u1())
            if self.parameters_tag == Id014PtkathmaOperation.Bool.true:
                self.parameters = Id014PtkathmaOperation.Parameters(self._io, self, self._root)



    class Case14(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__14_field0 = self._io.read_s4be()
            self.case__14_field1 = Id014PtkathmaOperation.Case14Field1(self._io, self, self._root)


    class Id014PtkathmaBlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_014__ptkathma__block_header__alpha__signed_contents = Id014PtkathmaOperation.Id014PtkathmaBlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id014PtkathmaOperation.Id014PtkathmaEntrypoint(self._io, self, self._root)
            self.value = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)


    class Some(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inbox_level = self._io.read_s4be()
            self.message_counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.payload = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)


    class BackPointersEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inbox_hash = self._io.read_bytes(32)


    class Case225(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__225_field0 = self._io.read_u2be()
            self.case__225_field1 = Id014PtkathmaOperation.Case225Field10(self._io, self, self._root)
            self.case__225_field2 = self._io.read_bytes(32)


    class Case224Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__224_field1 = self._io.read_bytes_full()


    class Messages0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_messages = self._io.read_u4be()
            if not self.len_messages <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_messages, self._io, u"/types/messages_0/seq/0")
            self._raw_messages = self._io.read_bytes(self.len_messages)
            _io__raw_messages = KaitaiStream(BytesIO(self._raw_messages))
            self.messages = Id014PtkathmaOperation.Messages(_io__raw_messages, self, self._root)


    class InodeValuesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inode_values_elt_field0 = Id014PtkathmaOperation.InodeValuesEltField00(self._io, self, self._root)
            self.inode_values_elt_field1 = Id014PtkathmaOperation.TreeEncoding(self._io, self, self._root)


    class Case1920(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__192 = self._io.read_u1()
            if not self.len_case__192 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__192, self._io, u"/types/case__192_0/seq/0")
            self._raw_case__192 = self._io.read_bytes(self.len_case__192)
            _io__raw_case__192 = KaitaiStream(BytesIO(self._raw_case__192))
            self.case__192 = Id014PtkathmaOperation.Case192(_io__raw_case__192, self, self._root)


    class Proof0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pvm_step = Id014PtkathmaOperation.PvmStep(self._io, self, self._root)
            self.inbox = Id014PtkathmaOperation.Inbox(self._io, self, self._root)


    class Case1313(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131 = self._io.read_u4be()
            if not self.len_case__131 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__131, self._io, u"/types/case__131_3/seq/0")
            self._raw_case__131 = self._io.read_bytes(self.len_case__131)
            _io__raw_case__131 = KaitaiStream(BytesIO(self._raw_case__131))
            self.case__131 = Id014PtkathmaOperation.Case131(_io__raw_case__131, self, self._root)


    class Proposals1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposals = Id014PtkathmaOperation.Proposals0(self._io, self, self._root)


    class TxRollupSubmitBatch(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaTxRollupId(self._io, self, self._root)
            self.content = Id014PtkathmaOperation.BytesDynUint30(self._io, self, self._root)
            self.burn_limit_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Bool, self._io.read_u1())
            if self.burn_limit_tag == Id014PtkathmaOperation.Bool.true:
                self.burn_limit = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)



    class Id014PtkathmaInlinedPreendorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__inlined__preendorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id014PtkathmaOperation.Id014PtkathmaInlinedPreendorsementContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.Bool, self._io.read_u1())
            if self.signature_tag == Id014PtkathmaOperation.Bool.true:
                self.signature = self._io.read_bytes(64)



    class DissectionEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dissection_elt_field0 = Id014PtkathmaOperation.DissectionEltField0(self._io, self, self._root)
            self.dissection_elt_field1 = Id014PtkathmaOperation.N(self._io, self, self._root)


    class Case130EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__130_elt_field0 = self._io.read_u1()
            if not self.len_case__130_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__130_elt_field0, self._io, u"/types/case__130_elt_field0_0/seq/0")
            self._raw_case__130_elt_field0 = self._io.read_bytes(self.len_case__130_elt_field0)
            _io__raw_case__130_elt_field0 = KaitaiStream(BytesIO(self._raw_case__130_elt_field0))
            self.case__130_elt_field0 = Id014PtkathmaOperation.Case130EltField0(_io__raw_case__130_elt_field0, self, self._root)


    class TicketsInfo(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.tickets_info_entries = []
            i = 0
            while not self._io.is_eof():
                self.tickets_info_entries.append(Id014PtkathmaOperation.TicketsInfoEntries(self._io, self, self._root))
                i += 1



    class Ballot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposal = self._io.read_bytes(32)
            self.ballot = self._io.read_s1()


    class Predecessor(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.predecessor_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.PredecessorTag, self._io.read_u1())
            if self.predecessor_tag == Id014PtkathmaOperation.PredecessorTag.some:
                self.some = self._io.read_bytes(32)



    class DalPublishSlotHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id014PtkathmaOperation.PublicKeyHash(self._io, self, self._root)
            self.fee = Id014PtkathmaOperation.Id014PtkathmaMutez(self._io, self, self._root)
            self.counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.gas_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.storage_limit = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.slot = Id014PtkathmaOperation.Slot(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id014PtkathmaOperation.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id014PtkathmaOperation.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id014PtkathmaOperation.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id014PtkathmaOperation.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Case1Field3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__1_field3_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__1_field3_entries.append(Id014PtkathmaOperation.Case1Field3Entries(self._io, self, self._root))
                i += 1



    class MessagePath0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message_path = self._io.read_u4be()
            if not self.len_message_path <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message_path, self._io, u"/types/message_path_0/seq/0")
            self._raw_message_path = self._io.read_bytes(self.len_message_path)
            _io__raw_message_path = KaitaiStream(BytesIO(self._raw_message_path))
            self.message_path = Id014PtkathmaOperation.MessagePath(_io__raw_message_path, self, self._root)


    class Op11(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__inlined__preendorsement = Id014PtkathmaOperation.Id014PtkathmaInlinedPreendorsement(self._io, self, self._root)


    class Proposals(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proposals_entries = []
            i = 0
            while not self._io.is_eof():
                self.proposals_entries.append(Id014PtkathmaOperation.ProposalsEntries(self._io, self, self._root))
                i += 1



    class Case1931(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__193 = self._io.read_u2be()
            if not self.len_case__193 <= 65535:
                raise kaitaistruct.ValidationGreaterThanError(65535, self.len_case__193, self._io, u"/types/case__193_1/seq/0")
            self._raw_case__193 = self._io.read_bytes(self.len_case__193)
            _io__raw_case__193 = KaitaiStream(BytesIO(self._raw_case__193))
            self.case__193 = Id014PtkathmaOperation.Case193(_io__raw_case__193, self, self._root)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id014PtkathmaOperation.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1



    class SparseProofEntries0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sparse_proof_elt_field0 = self._io.read_u1()
            self.sparse_proof_elt_field1 = Id014PtkathmaOperation.InodeTree0(self._io, self, self._root)


    class ProposalsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol_hash = self._io.read_bytes(32)


    class Op1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__inlined__endorsement = Id014PtkathmaOperation.Id014PtkathmaInlinedEndorsement(self._io, self, self._root)


    class Level(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup = Id014PtkathmaOperation.Id014PtkathmaRollupAddress(self._io, self, self._root)
            self.message_counter = Id014PtkathmaOperation.N(self._io, self, self._root)
            self.nb_available_messages = self._io.read_s8be()
            self.nb_messages_in_commitment_period = self._io.read_s8be()
            self.starting_level_of_current_commitment_period = self._io.read_s4be()
            self.level = self._io.read_s4be()
            self.current_messages_hash = self._io.read_bytes(32)
            self.old_levels_messages = Id014PtkathmaOperation.OldLevelsMessages(self._io, self, self._root)


    class Case0Field30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__0_field3 = self._io.read_u4be()
            if not self.len_case__0_field3 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__0_field3, self._io, u"/types/case__0_field3_0/seq/0")
            self._raw_case__0_field3 = self._io.read_bytes(self.len_case__0_field3)
            _io__raw_case__0_field3 = KaitaiStream(BytesIO(self._raw_case__0_field3))
            self.case__0_field3 = Id014PtkathmaOperation.Case0Field3(_io__raw_case__0_field3, self, self._root)


    class DoublePreendorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id014PtkathmaOperation.Op12(self._io, self, self._root)
            self.op2 = Id014PtkathmaOperation.Op22(self._io, self, self._root)


    class InodeValues0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_inode_values = self._io.read_u4be()
            if not self.len_inode_values <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_inode_values, self._io, u"/types/inode_values_0/seq/0")
            self._raw_inode_values = self._io.read_bytes(self.len_inode_values)
            _io__raw_inode_values = KaitaiStream(BytesIO(self._raw_inode_values))
            self.inode_values = Id014PtkathmaOperation.InodeValues(_io__raw_inode_values, self, self._root)



