# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import sapling__transaction__input
from kaitai import sapling__transaction__commitment_hash
from kaitai import sapling__transaction__output
from kaitai import sapling__transaction__binding_sig
class SaplingTransaction(KaitaiStruct):
    """Encoding id: sapling.transaction
    Description: A Sapling transaction with inputs, outputs, balance, root, bound_data and binding sig."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.inputs = SaplingTransaction.Inputs0(self._io, self, self._root)
        self.outputs = SaplingTransaction.Outputs0(self._io, self, self._root)
        self.binding_sig = sapling__transaction__binding_sig.SaplingTransactionBindingSig(self._io)
        self.balance = self._io.read_s8be()
        self.root = sapling__transaction__commitment_hash.SaplingTransactionCommitmentHash(self._io)
        self.bound_data = SaplingTransaction.BytesDynUint30(self._io, self, self._root)

    class Inputs0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_inputs = self._io.read_u4be()
            if not self.len_inputs <= 1833216:
                raise kaitaistruct.ValidationGreaterThanError(1833216, self.len_inputs, self._io, u"/types/inputs_0/seq/0")
            self._raw_inputs = self._io.read_bytes(self.len_inputs)
            _io__raw_inputs = KaitaiStream(BytesIO(self._raw_inputs))
            self.inputs = SaplingTransaction.Inputs(_io__raw_inputs, self, self._root)


    class Outputs(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.outputs_entries = []
            i = 0
            while not self._io.is_eof():
                self.outputs_entries.append(SaplingTransaction.OutputsEntries(self._io, self, self._root))
                i += 1



    class InputsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inputs_elt = sapling__transaction__input.SaplingTransactionInput(self._io)


    class Inputs(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inputs_entries = []
            i = 0
            while not self._io.is_eof():
                self.inputs_entries.append(SaplingTransaction.InputsEntries(self._io, self, self._root))
                i += 1



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Outputs0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_outputs = self._io.read_u4be()
            if not self.len_outputs <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_outputs, self._io, u"/types/outputs_0/seq/0")
            self._raw_outputs = self._io.read_bytes(self.len_outputs)
            _io__raw_outputs = KaitaiStream(BytesIO(self._raw_outputs))
            self.outputs = SaplingTransaction.Outputs(_io__raw_outputs, self, self._root)


    class OutputsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.outputs_elt = sapling__transaction__output.SaplingTransactionOutput(self._io)



