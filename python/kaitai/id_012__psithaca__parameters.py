# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id012PsithacaParameters(KaitaiStruct):
    """Encoding id: 012-Psithaca.parameters."""

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class DelegateSelectionTag(Enum):
        random_delegate_selection = 0
        round_robin_over_delegates = 1

    class BootstrapAccountsEltTag(Enum):
        public_key_known = 0
        public_key_unknown = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.bootstrap_accounts = Id012PsithacaParameters.BootstrapAccounts0(self._io, self, self._root)
        self.bootstrap_contracts = Id012PsithacaParameters.BootstrapContracts0(self._io, self, self._root)
        self.commitments = Id012PsithacaParameters.Commitments0(self._io, self, self._root)
        self.security_deposit_ramp_up_cycles_tag = KaitaiStream.resolve_enum(Id012PsithacaParameters.Bool, self._io.read_u1())
        if self.security_deposit_ramp_up_cycles_tag == Id012PsithacaParameters.Bool.true:
            self.security_deposit_ramp_up_cycles = Id012PsithacaParameters.Int31(self._io, self, self._root)

        self.no_reward_cycles_tag = KaitaiStream.resolve_enum(Id012PsithacaParameters.Bool, self._io.read_u1())
        if self.no_reward_cycles_tag == Id012PsithacaParameters.Bool.true:
            self.no_reward_cycles = Id012PsithacaParameters.Int31(self._io, self, self._root)

        self.preserved_cycles = self._io.read_u1()
        self.blocks_per_cycle = self._io.read_s4be()
        self.blocks_per_commitment = self._io.read_s4be()
        self.blocks_per_stake_snapshot = self._io.read_s4be()
        self.blocks_per_voting_period = self._io.read_s4be()
        self.hard_gas_limit_per_operation = Id012PsithacaParameters.Z(self._io, self, self._root)
        self.hard_gas_limit_per_block = Id012PsithacaParameters.Z(self._io, self, self._root)
        self.proof_of_work_threshold = self._io.read_s8be()
        self.tokens_per_roll = Id012PsithacaParameters.Id012PsithacaMutez(self._io, self, self._root)
        self.seed_nonce_revelation_tip = Id012PsithacaParameters.Id012PsithacaMutez(self._io, self, self._root)
        self.origination_size = Id012PsithacaParameters.Int31(self._io, self, self._root)
        self.baking_reward_fixed_portion = Id012PsithacaParameters.Id012PsithacaMutez(self._io, self, self._root)
        self.baking_reward_bonus_per_slot = Id012PsithacaParameters.Id012PsithacaMutez(self._io, self, self._root)
        self.endorsing_reward_per_slot = Id012PsithacaParameters.Id012PsithacaMutez(self._io, self, self._root)
        self.cost_per_byte = Id012PsithacaParameters.Id012PsithacaMutez(self._io, self, self._root)
        self.hard_storage_limit_per_operation = Id012PsithacaParameters.Z(self._io, self, self._root)
        self.quorum_min = self._io.read_s4be()
        self.quorum_max = self._io.read_s4be()
        self.min_proposal_quorum = self._io.read_s4be()
        self.liquidity_baking_subsidy = Id012PsithacaParameters.Id012PsithacaMutez(self._io, self, self._root)
        self.liquidity_baking_sunset_level = self._io.read_s4be()
        self.liquidity_baking_escape_ema_threshold = self._io.read_s4be()
        self.max_operations_time_to_live = self._io.read_s2be()
        self.minimal_block_delay = self._io.read_s8be()
        self.delay_increment_per_round = self._io.read_s8be()
        self.consensus_committee_size = Id012PsithacaParameters.Int31(self._io, self, self._root)
        self.consensus_threshold = Id012PsithacaParameters.Int31(self._io, self, self._root)
        self.minimal_participation_ratio = Id012PsithacaParameters.MinimalParticipationRatio(self._io, self, self._root)
        self.max_slashing_period = Id012PsithacaParameters.Int31(self._io, self, self._root)
        self.frozen_deposits_percentage = Id012PsithacaParameters.Int31(self._io, self, self._root)
        self.double_baking_punishment = Id012PsithacaParameters.Id012PsithacaMutez(self._io, self, self._root)
        self.ratio_of_frozen_deposits_slashed_per_double_endorsement = Id012PsithacaParameters.RatioOfFrozenDepositsSlashedPerDoubleEndorsement(self._io, self, self._root)
        self.delegate_selection = Id012PsithacaParameters.DelegateSelection(self._io, self, self._root)

    class RatioOfFrozenDepositsSlashedPerDoubleEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class PublicKeyKnown(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_known_field0 = Id012PsithacaParameters.PublicKey(self._io, self, self._root)
            self.public_key_known_field1 = Id012PsithacaParameters.Id012PsithacaMutez(self._io, self, self._root)


    class Commitments(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_entries = []
            i = 0
            while not self._io.is_eof():
                self.commitments_entries.append(Id012PsithacaParameters.CommitmentsEntries(self._io, self, self._root))
                i += 1



    class Id012PsithacaScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id012PsithacaParameters.BytesDynUint30(self._io, self, self._root)
            self.storage = Id012PsithacaParameters.BytesDynUint30(self._io, self, self._root)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id012PsithacaParameters.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class RoundRobinOverDelegatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_round_robin_over_delegates_elt = self._io.read_u4be()
            if not self.len_round_robin_over_delegates_elt <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_round_robin_over_delegates_elt, self._io, u"/types/round_robin_over_delegates_entries/seq/0")
            self._raw_round_robin_over_delegates_elt = self._io.read_bytes(self.len_round_robin_over_delegates_elt)
            _io__raw_round_robin_over_delegates_elt = KaitaiStream(BytesIO(self._raw_round_robin_over_delegates_elt))
            self.round_robin_over_delegates_elt = Id012PsithacaParameters.RoundRobinOverDelegatesElt(_io__raw_round_robin_over_delegates_elt, self, self._root)


    class RoundRobinOverDelegates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.round_robin_over_delegates_entries = []
            i = 0
            while not self._io.is_eof():
                self.round_robin_over_delegates_entries.append(Id012PsithacaParameters.RoundRobinOverDelegatesEntries(self._io, self, self._root))
                i += 1



    class RoundRobinOverDelegatesElt(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.round_robin_over_delegates_elt_entries = []
            i = 0
            while not self._io.is_eof():
                self.round_robin_over_delegates_elt_entries.append(Id012PsithacaParameters.RoundRobinOverDelegatesEltEntries(self._io, self, self._root))
                i += 1



    class Id012PsithacaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__mutez = Id012PsithacaParameters.N(self._io, self, self._root)


    class RoundRobinOverDelegates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_round_robin_over_delegates = self._io.read_u4be()
            if not self.len_round_robin_over_delegates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_round_robin_over_delegates, self._io, u"/types/round_robin_over_delegates_0/seq/0")
            self._raw_round_robin_over_delegates = self._io.read_bytes(self.len_round_robin_over_delegates)
            _io__raw_round_robin_over_delegates = KaitaiStream(BytesIO(self._raw_round_robin_over_delegates))
            self.round_robin_over_delegates = Id012PsithacaParameters.RoundRobinOverDelegates(_io__raw_round_robin_over_delegates, self, self._root)


    class CommitmentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.commitments_elt_field0 = self._io.read_bytes(20)
            self.commitments_elt_field1 = Id012PsithacaParameters.Id012PsithacaMutez(self._io, self, self._root)


    class Commitments0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_commitments = self._io.read_u4be()
            if not self.len_commitments <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_commitments, self._io, u"/types/commitments_0/seq/0")
            self._raw_commitments = self._io.read_bytes(self.len_commitments)
            _io__raw_commitments = KaitaiStream(BytesIO(self._raw_commitments))
            self.commitments = Id012PsithacaParameters.Commitments(_io__raw_commitments, self, self._root)


    class MinimalParticipationRatio(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class BootstrapAccounts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_accounts = self._io.read_u4be()
            if not self.len_bootstrap_accounts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_accounts, self._io, u"/types/bootstrap_accounts_0/seq/0")
            self._raw_bootstrap_accounts = self._io.read_bytes(self.len_bootstrap_accounts)
            _io__raw_bootstrap_accounts = KaitaiStream(BytesIO(self._raw_bootstrap_accounts))
            self.bootstrap_accounts = Id012PsithacaParameters.BootstrapAccounts(_io__raw_bootstrap_accounts, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id012PsithacaParameters.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id012PsithacaParameters.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id012PsithacaParameters.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id012PsithacaParameters.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class PublicKeyUnknown(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_unknown_field0 = Id012PsithacaParameters.PublicKeyHash(self._io, self, self._root)
            self.public_key_unknown_field1 = Id012PsithacaParameters.Id012PsithacaMutez(self._io, self, self._root)


    class BootstrapAccountsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_accounts_elt_tag = KaitaiStream.resolve_enum(Id012PsithacaParameters.BootstrapAccountsEltTag, self._io.read_u1())
            if self.bootstrap_accounts_elt_tag == Id012PsithacaParameters.BootstrapAccountsEltTag.public_key_known:
                self.public_key_known = Id012PsithacaParameters.PublicKeyKnown(self._io, self, self._root)

            if self.bootstrap_accounts_elt_tag == Id012PsithacaParameters.BootstrapAccountsEltTag.public_key_unknown:
                self.public_key_unknown = Id012PsithacaParameters.PublicKeyUnknown(self._io, self, self._root)



    class BootstrapContracts0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bootstrap_contracts = self._io.read_u4be()
            if not self.len_bootstrap_contracts <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bootstrap_contracts, self._io, u"/types/bootstrap_contracts_0/seq/0")
            self._raw_bootstrap_contracts = self._io.read_bytes(self.len_bootstrap_contracts)
            _io__raw_bootstrap_contracts = KaitaiStream(BytesIO(self._raw_bootstrap_contracts))
            self.bootstrap_contracts = Id012PsithacaParameters.BootstrapContracts(_io__raw_bootstrap_contracts, self, self._root)


    class DelegateSelection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_selection_tag = KaitaiStream.resolve_enum(Id012PsithacaParameters.DelegateSelectionTag, self._io.read_u1())
            if self.delegate_selection_tag == Id012PsithacaParameters.DelegateSelectionTag.round_robin_over_delegates:
                self.round_robin_over_delegates = Id012PsithacaParameters.RoundRobinOverDelegates0(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class BootstrapContractsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id012PsithacaParameters.Bool, self._io.read_u1())
            if self.delegate_tag == Id012PsithacaParameters.Bool.true:
                self.delegate = Id012PsithacaParameters.PublicKeyHash(self._io, self, self._root)

            self.amount = Id012PsithacaParameters.Id012PsithacaMutez(self._io, self, self._root)
            self.script = Id012PsithacaParameters.Id012PsithacaScriptedContracts(self._io, self, self._root)


    class RoundRobinOverDelegatesEltEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.signature__v0__public_key = Id012PsithacaParameters.PublicKey(self._io, self, self._root)


    class BootstrapContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_contracts_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_contracts_entries.append(Id012PsithacaParameters.BootstrapContractsEntries(self._io, self, self._root))
                i += 1



    class BootstrapAccounts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bootstrap_accounts_entries = []
            i = 0
            while not self._io.is_eof():
                self.bootstrap_accounts_entries.append(Id012PsithacaParameters.BootstrapAccountsEntries(self._io, self, self._root))
                i += 1



    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id012PsithacaParameters.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id012PsithacaParameters.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id012PsithacaParameters.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id012PsithacaParameters.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id012PsithacaParameters.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




