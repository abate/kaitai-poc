# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import sapling__transaction__commitment_value
class SaplingTransactionCiphertext(KaitaiStruct):
    """Encoding id: sapling.transaction.ciphertext."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.cv = sapling__transaction__commitment_value.SaplingTransactionCommitmentValue(self._io)
        self.epk = SaplingTransactionCiphertext.SaplingDhEpk(self._io, self, self._root)
        self.payload_enc = SaplingTransactionCiphertext.BytesDynUint30(self._io, self, self._root)
        self.nonce_enc = self._io.read_bytes(24)
        self.payload_out = self._io.read_bytes(80)
        self.nonce_out = self._io.read_bytes(24)

    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class SaplingDhEpk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sapling__dh__epk = self._io.read_bytes(32)



