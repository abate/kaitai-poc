# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id007Psdelph1OperationInternal(KaitaiStruct):
    """Encoding id: 007-PsDELPH1.operation.internal."""

    class Id007Psdelph1OperationAlphaInternalOperationTag(Enum):
        reveal = 0
        transaction = 1
        origination = 2
        delegation = 3

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Id007Psdelph1EntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255

    class Id007Psdelph1ContractIdTag(Enum):
        implicit = 0
        originated = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_007__psdelph1__operation__alpha__internal_operation = Id007Psdelph1OperationInternal.Id007Psdelph1OperationAlphaInternalOperation(self._io, self, self._root)

    class Id007Psdelph1ContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_007__psdelph1__contract_id_tag = KaitaiStream.resolve_enum(Id007Psdelph1OperationInternal.Id007Psdelph1ContractIdTag, self._io.read_u1())
            if self.id_007__psdelph1__contract_id_tag == Id007Psdelph1OperationInternal.Id007Psdelph1ContractIdTag.implicit:
                self.implicit = Id007Psdelph1OperationInternal.PublicKeyHash(self._io, self, self._root)

            if self.id_007__psdelph1__contract_id_tag == Id007Psdelph1OperationInternal.Id007Psdelph1ContractIdTag.originated:
                self.originated = Id007Psdelph1OperationInternal.Originated(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id007Psdelph1OperationInternal.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id007Psdelph1OperationAlphaInternalOperation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id007Psdelph1OperationInternal.Id007Psdelph1ContractId(self._io, self, self._root)
            self.nonce = self._io.read_u2be()
            self.id_007__psdelph1__operation__alpha__internal_operation_tag = KaitaiStream.resolve_enum(Id007Psdelph1OperationInternal.Id007Psdelph1OperationAlphaInternalOperationTag, self._io.read_u1())
            if self.id_007__psdelph1__operation__alpha__internal_operation_tag == Id007Psdelph1OperationInternal.Id007Psdelph1OperationAlphaInternalOperationTag.reveal:
                self.reveal = Id007Psdelph1OperationInternal.PublicKey(self._io, self, self._root)

            if self.id_007__psdelph1__operation__alpha__internal_operation_tag == Id007Psdelph1OperationInternal.Id007Psdelph1OperationAlphaInternalOperationTag.transaction:
                self.transaction = Id007Psdelph1OperationInternal.Transaction(self._io, self, self._root)

            if self.id_007__psdelph1__operation__alpha__internal_operation_tag == Id007Psdelph1OperationInternal.Id007Psdelph1OperationAlphaInternalOperationTag.origination:
                self.origination = Id007Psdelph1OperationInternal.Origination(self._io, self, self._root)

            if self.id_007__psdelph1__operation__alpha__internal_operation_tag == Id007Psdelph1OperationInternal.Id007Psdelph1OperationAlphaInternalOperationTag.delegation:
                self.delegation = Id007Psdelph1OperationInternal.Delegation(self._io, self, self._root)



    class Id007Psdelph1Mutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_007__psdelph1__mutez = Id007Psdelph1OperationInternal.N(self._io, self, self._root)


    class Id007Psdelph1Entrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_007__psdelph1__entrypoint_tag = KaitaiStream.resolve_enum(Id007Psdelph1OperationInternal.Id007Psdelph1EntrypointTag, self._io.read_u1())
            if self.id_007__psdelph1__entrypoint_tag == Id007Psdelph1OperationInternal.Id007Psdelph1EntrypointTag.named:
                self.named = Id007Psdelph1OperationInternal.Named0(self._io, self, self._root)



    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id007Psdelph1OperationInternal.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id007Psdelph1OperationInternal.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id007Psdelph1OperationInternal.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id007Psdelph1OperationInternal.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id007Psdelph1OperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id007Psdelph1OperationInternal.Bool.true:
                self.delegate = Id007Psdelph1OperationInternal.PublicKeyHash(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.balance = Id007Psdelph1OperationInternal.Id007Psdelph1Mutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id007Psdelph1OperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id007Psdelph1OperationInternal.Bool.true:
                self.delegate = Id007Psdelph1OperationInternal.PublicKeyHash(self._io, self, self._root)

            self.script = Id007Psdelph1OperationInternal.Id007Psdelph1ScriptedContracts(self._io, self, self._root)


    class Id007Psdelph1ScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id007Psdelph1OperationInternal.BytesDynUint30(self._io, self, self._root)
            self.storage = Id007Psdelph1OperationInternal.BytesDynUint30(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id007Psdelph1OperationInternal.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount = Id007Psdelph1OperationInternal.Id007Psdelph1Mutez(self._io, self, self._root)
            self.destination = Id007Psdelph1OperationInternal.Id007Psdelph1ContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id007Psdelph1OperationInternal.Bool, self._io.read_u1())
            if self.parameters_tag == Id007Psdelph1OperationInternal.Bool.true:
                self.parameters = Id007Psdelph1OperationInternal.Parameters(self._io, self, self._root)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id007Psdelph1OperationInternal.Id007Psdelph1Entrypoint(self._io, self, self._root)
            self.value = Id007Psdelph1OperationInternal.BytesDynUint30(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id007Psdelph1OperationInternal.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id007Psdelph1OperationInternal.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id007Psdelph1OperationInternal.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id007Psdelph1OperationInternal.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




