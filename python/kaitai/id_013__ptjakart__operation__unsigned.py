# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import operation__shell_header
from kaitai import block_header__shell
class Id013PtjakartOperationUnsigned(KaitaiStruct):
    """Encoding id: 013-PtJakart.operation.unsigned."""

    class Id013PtjakartInlinedEndorsementMempoolContentsTag(Enum):
        endorsement = 21

    class Id013PtjakartInlinedPreendorsementContentsTag(Enum):
        preendorsement = 20

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Case131EltField1Tag(Enum):
        case__0 = 0
        case__1 = 1

    class ProofTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3

    class Case129EltField1Tag(Enum):
        case__0 = 0
        case__1 = 1

    class KindTag(Enum):
        example_arith__smart__contract__rollup__kind = 0

    class Case3Field3EltTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__7 = 7
        case__8 = 8
        case__9 = 9
        case__10 = 10
        case__11 = 11
        case__12 = 12
        case__13 = 13
        case__14 = 14
        case__15 = 15
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__192 = 192
        case__193 = 193
        case__195 = 195
        case__224 = 224
        case__225 = 225
        case__226 = 226
        case__227 = 227

    class Case1Field3EltTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__7 = 7
        case__8 = 8
        case__9 = 9
        case__10 = 10
        case__11 = 11
        case__12 = 12
        case__13 = 13
        case__14 = 14
        case__15 = 15
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__192 = 192
        case__193 = 193
        case__195 = 195
        case__224 = 224
        case__225 = 225
        case__226 = 226
        case__227 = 227

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id013PtjakartContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Case130EltField1Tag(Enum):
        case__0 = 0
        case__1 = 1

    class Id013PtjakartOperationAlphaContentsTag(Enum):
        seed_nonce_revelation = 1
        double_endorsement_evidence = 2
        double_baking_evidence = 3
        activate_account = 4
        proposals = 5
        ballot = 6
        double_preendorsement_evidence = 7
        failing_noop = 17
        preendorsement = 20
        endorsement = 21
        reveal = 107
        transaction = 108
        origination = 109
        delegation = 110
        register_global_constant = 111
        set_deposits_limit = 112
        tx_rollup_origination = 150
        tx_rollup_submit_batch = 151
        tx_rollup_commit = 152
        tx_rollup_return_bond = 153
        tx_rollup_finalize_commitment = 154
        tx_rollup_remove_commitment = 155
        tx_rollup_rejection = 156
        tx_rollup_dispatch_tickets = 157
        transfer_ticket = 158
        sc_rollup_originate = 200
        sc_rollup_add_messages = 201
        sc_rollup_cement = 202
        sc_rollup_publish = 203

    class Id013PtjakartEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255

    class Id013PtjakartTransactionDestinationTag(Enum):
        implicit = 0
        originated = 1
        tx_rollup = 2

    class PredecessorTag(Enum):
        none = 0
        some = 1

    class AmountTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3

    class Case2Field3EltTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__7 = 7
        case__8 = 8
        case__9 = 9
        case__10 = 10
        case__11 = 11
        case__12 = 12
        case__13 = 13
        case__14 = 14
        case__15 = 15
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__192 = 192
        case__193 = 193
        case__195 = 195
        case__224 = 224
        case__225 = 225
        case__226 = 226
        case__227 = 227

    class MessageTag(Enum):
        batch = 0
        deposit = 1

    class Case0Field3EltTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__3 = 3
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__7 = 7
        case__8 = 8
        case__9 = 9
        case__10 = 10
        case__11 = 11
        case__12 = 12
        case__13 = 13
        case__14 = 14
        case__15 = 15
        case__128 = 128
        case__129 = 129
        case__130 = 130
        case__131 = 131
        case__192 = 192
        case__193 = 193
        case__195 = 195
        case__224 = 224
        case__225 = 225
        case__226 = 226
        case__227 = 227
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_013__ptjakart__operation__alpha__unsigned_operation = Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaUnsignedOperation(self._io, self, self._root)

    class Case131EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131_elt_field0 = self._io.read_u1()
            if not self.len_case__131_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__131_elt_field0, self._io, u"/types/case__131_elt_field0_0/seq/0")
            self._raw_case__131_elt_field0 = self._io.read_bytes(self.len_case__131_elt_field0)
            _io__raw_case__131_elt_field0 = KaitaiStream(BytesIO(self._raw_case__131_elt_field0))
            self.case__131_elt_field0 = Id013PtjakartOperationUnsigned.Case131EltField0(_io__raw_case__131_elt_field0, self, self._root)


    class Op20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_0/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id013PtjakartOperationUnsigned.Op2(_io__raw_op2, self, self._root)


    class Case1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__1_field0 = self._io.read_s2be()
            self.case__1_field1 = self._io.read_bytes(32)
            self.case__1_field2 = self._io.read_bytes(32)
            self.case__1_field3 = Id013PtjakartOperationUnsigned.Case1Field30(self._io, self, self._root)


    class Case192(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__192 = self._io.read_bytes_full()


    class Messages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.messages_entries = []
            i = 0
            while not self._io.is_eof():
                self.messages_entries.append(Id013PtjakartOperationUnsigned.MessagesEntries(self._io, self, self._root))
                i += 1



    class MessageResultPathEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_list_hash = self._io.read_bytes(32)


    class ScRollupPublish(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id013PtjakartOperationUnsigned.Id013PtjakartRollupAddress(self._io, self, self._root)
            self.commitment = Id013PtjakartOperationUnsigned.Commitment0(self._io, self, self._root)


    class Case1933(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__193 = self._io.read_u2be()
            if not self.len_case__193 <= 65535:
                raise kaitaistruct.ValidationGreaterThanError(65535, self.len_case__193, self._io, u"/types/case__193_3/seq/0")
            self._raw_case__193 = self._io.read_bytes(self.len_case__193)
            _io__raw_case__193 = KaitaiStream(BytesIO(self._raw_case__193))
            self.case__193 = Id013PtjakartOperationUnsigned.Case193(_io__raw_case__193, self, self._root)


    class Case8(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__8_field0 = self._io.read_u1()
            self.case__8_field1 = self._io.read_bytes(32)


    class Case10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__10_field0 = self._io.read_s4be()
            self.case__10_field1 = self._io.read_bytes(32)


    class Case2Field3Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__2_field3_elt_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Case2Field3EltTag, self._io.read_u1())
            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__8:
                self.case__8 = Id013PtjakartOperationUnsigned.Case8(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__4:
                self.case__4 = Id013PtjakartOperationUnsigned.Case4(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__12:
                self.case__12 = Id013PtjakartOperationUnsigned.Case12(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__9:
                self.case__9 = Id013PtjakartOperationUnsigned.Case9(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__5:
                self.case__5 = Id013PtjakartOperationUnsigned.Case5(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__13:
                self.case__13 = Id013PtjakartOperationUnsigned.Case13(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__10:
                self.case__10 = Id013PtjakartOperationUnsigned.Case10(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__6:
                self.case__6 = Id013PtjakartOperationUnsigned.Case6(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__14:
                self.case__14 = Id013PtjakartOperationUnsigned.Case14(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__3:
                self.case__3 = self._io.read_s8be()

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__11:
                self.case__11 = Id013PtjakartOperationUnsigned.Case11(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__7:
                self.case__7 = Id013PtjakartOperationUnsigned.Case7(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__15:
                self.case__15 = Id013PtjakartOperationUnsigned.Case15(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__129:
                self.case__129 = Id013PtjakartOperationUnsigned.Case129Entries(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__130:
                self.case__130 = Id013PtjakartOperationUnsigned.Case130Entries(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__131:
                self.case__131 = Id013PtjakartOperationUnsigned.Case1311(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__192:
                self.case__192 = Id013PtjakartOperationUnsigned.Case1921(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__193:
                self.case__193 = Id013PtjakartOperationUnsigned.Case1931(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__195:
                self.case__195 = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__224:
                self.case__224 = Id013PtjakartOperationUnsigned.Case224(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__225:
                self.case__225 = Id013PtjakartOperationUnsigned.Case225(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__226:
                self.case__226 = Id013PtjakartOperationUnsigned.Case226(self._io, self, self._root)

            if self.case__2_field3_elt_tag == Id013PtjakartOperationUnsigned.Case2Field3EltTag.case__227:
                self.case__227 = Id013PtjakartOperationUnsigned.Case227(self._io, self, self._root)



    class Case3Field30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__3_field3 = self._io.read_u4be()
            if not self.len_case__3_field3 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__3_field3, self._io, u"/types/case__3_field3_0/seq/0")
            self._raw_case__3_field3 = self._io.read_bytes(self.len_case__3_field3)
            _io__raw_case__3_field3 = KaitaiStream(BytesIO(self._raw_case__3_field3))
            self.case__3_field3 = Id013PtjakartOperationUnsigned.Case3Field3(_io__raw_case__3_field3, self, self._root)


    class ActivateAccount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = self._io.read_bytes(20)
            self.secret = self._io.read_bytes(20)


    class Case12Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__12_field1_field0 = self._io.read_bytes(32)
            self.case__12_field1_field1 = self._io.read_bytes(32)


    class DoubleEndorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id013PtjakartOperationUnsigned.Op10(self._io, self, self._root)
            self.op2 = Id013PtjakartOperationUnsigned.Op20(self._io, self, self._root)


    class Id013PtjakartBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_hash = self._io.read_bytes(32)
            self.payload_round = self._io.read_s4be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id013PtjakartOperationUnsigned.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.liquidity_baking_toggle_vote = Id013PtjakartOperationUnsigned.Id013PtjakartLiquidityBakingToggleVote(self._io, self, self._root)


    class Case226(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__226_field0 = self._io.read_s4be()
            self.case__226_field1 = Id013PtjakartOperationUnsigned.Case226Field10(self._io, self, self._root)
            self.case__226_field2 = self._io.read_bytes(32)


    class Case9(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__9_field0 = self._io.read_u2be()
            self.case__9_field1 = self._io.read_bytes(32)


    class Case13(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__13_field0 = self._io.read_u2be()
            self.case__13_field1 = Id013PtjakartOperationUnsigned.Case13Field1(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Case226Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__226_field1 = self._io.read_bytes_full()


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id013PtjakartOperationUnsigned.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Case131EltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_elt_field1_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Case131EltField1Tag, self._io.read_u1())
            if self.case__131_elt_field1_tag == Id013PtjakartOperationUnsigned.Case131EltField1Tag.case__0:
                self.case__0 = self._io.read_bytes(32)

            if self.case__131_elt_field1_tag == Id013PtjakartOperationUnsigned.Case131EltField1Tag.case__1:
                self.case__1 = self._io.read_bytes(32)



    class Id013PtjakartEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__entrypoint_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Id013PtjakartEntrypointTag, self._io.read_u1())
            if self.id_013__ptjakart__entrypoint_tag == Id013PtjakartOperationUnsigned.Id013PtjakartEntrypointTag.named:
                self.named = Id013PtjakartOperationUnsigned.Named0(self._io, self, self._root)



    class Case227(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__227_field0 = self._io.read_s8be()
            self.case__227_field1 = Id013PtjakartOperationUnsigned.Case227Field10(self._io, self, self._root)
            self.case__227_field2 = self._io.read_bytes(32)


    class Case4(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__4_field0 = self._io.read_u1()
            self.case__4_field1 = self._io.read_bytes(32)


    class Endorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class Case130Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__130_elt_field0 = Id013PtjakartOperationUnsigned.Case130EltField00(self._io, self, self._root)
            self.case__130_elt_field1 = Id013PtjakartOperationUnsigned.Case130EltField1(self._io, self, self._root)


    class Id013PtjakartTxRollupId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup_hash = self._io.read_bytes(20)


    class Proposals0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_proposals = self._io.read_u4be()
            if not self.len_proposals <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_proposals, self._io, u"/types/proposals_0/seq/0")
            self._raw_proposals = self._io.read_bytes(self.len_proposals)
            _io__raw_proposals = KaitaiStream(BytesIO(self._raw_proposals))
            self.proposals = Id013PtjakartOperationUnsigned.Proposals(_io__raw_proposals, self, self._root)


    class MessagesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_hash = self._io.read_bytes(32)


    class Case13Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__13_field1_field0 = self._io.read_bytes(32)
            self.case__13_field1_field1 = self._io.read_bytes(32)


    class TransferTicket(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.ticket_contents = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.ticket_ty = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.ticket_ticketer = Id013PtjakartOperationUnsigned.Id013PtjakartContractId(self._io, self, self._root)
            self.ticket_amount = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.destination = Id013PtjakartOperationUnsigned.Id013PtjakartContractId(self._io, self, self._root)
            self.entrypoint = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case129EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__129_elt_field0 = self._io.read_bytes_full()


    class Case1310(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131 = self._io.read_u4be()
            if not self.len_case__131 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__131, self._io, u"/types/case__131_0/seq/0")
            self._raw_case__131 = self._io.read_bytes(self.len_case__131)
            _io__raw_case__131 = KaitaiStream(BytesIO(self._raw_case__131))
            self.case__131 = Id013PtjakartOperationUnsigned.Case131(_io__raw_case__131, self, self._root)


    class Case15Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__15_field1_field0 = self._io.read_bytes(32)
            self.case__15_field1_field1 = self._io.read_bytes(32)


    class Op12(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_2/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id013PtjakartOperationUnsigned.Op11(_io__raw_op1, self, self._root)


    class TxRollupRemoveCommitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id013PtjakartOperationUnsigned.Id013PtjakartTxRollupId(self._io, self, self._root)


    class Case0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field0 = self._io.read_s2be()
            self.case__0_field1 = self._io.read_bytes(32)
            self.case__0_field2 = self._io.read_bytes(32)
            self.case__0_field3 = Id013PtjakartOperationUnsigned.Case0Field30(self._io, self, self._root)


    class Case1921(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__192 = self._io.read_u1()
            if not self.len_case__192 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__192, self._io, u"/types/case__192_1/seq/0")
            self._raw_case__192 = self._io.read_bytes(self.len_case__192)
            _io__raw_case__192 = KaitaiStream(BytesIO(self._raw_case__192))
            self.case__192 = Id013PtjakartOperationUnsigned.Case192(_io__raw_case__192, self, self._root)


    class Id013PtjakartBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__block_header__alpha__unsigned_contents = Id013PtjakartOperationUnsigned.Id013PtjakartBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes(64)


    class Reveal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.public_key = Id013PtjakartOperationUnsigned.PublicKey(self._io, self, self._root)


    class Id013PtjakartInlinedEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__inlined__endorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id013PtjakartOperationUnsigned.Id013PtjakartInlinedEndorsementMempoolContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Bool, self._io.read_u1())
            if self.signature_tag == Id013PtjakartOperationUnsigned.Bool.true:
                self.signature = self._io.read_bytes(64)



    class Case1922(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__192 = self._io.read_u1()
            if not self.len_case__192 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__192, self._io, u"/types/case__192_2/seq/0")
            self._raw_case__192 = self._io.read_bytes(self.len_case__192)
            _io__raw_case__192 = KaitaiStream(BytesIO(self._raw_case__192))
            self.case__192 = Id013PtjakartOperationUnsigned.Case192(_io__raw_case__192, self, self._root)


    class MessageEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_elt = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Id013PtjakartContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__contract_id_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Id013PtjakartContractIdTag, self._io.read_u1())
            if self.id_013__ptjakart__contract_id_tag == Id013PtjakartOperationUnsigned.Id013PtjakartContractIdTag.implicit:
                self.implicit = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)

            if self.id_013__ptjakart__contract_id_tag == Id013PtjakartOperationUnsigned.Id013PtjakartContractIdTag.originated:
                self.originated = Id013PtjakartOperationUnsigned.Originated(self._io, self, self._root)



    class Case5(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__5_field0 = self._io.read_u2be()
            self.case__5_field1 = self._io.read_bytes(32)


    class Case193(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__193 = self._io.read_bytes_full()


    class ScRollupOriginate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.kind = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.KindTag, self._io.read_u2be())
            self.boot_sector = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class SeedNonceRevelation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.nonce = self._io.read_bytes(32)


    class Case3Field3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__3_field3_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__3_field3_entries.append(Id013PtjakartOperationUnsigned.Case3Field3Entries(self._io, self, self._root))
                i += 1



    class Case225Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__225_field1 = self._io.read_u1()
            if not self.len_case__225_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__225_field1, self._io, u"/types/case__225_field1_0/seq/0")
            self._raw_case__225_field1 = self._io.read_bytes(self.len_case__225_field1)
            _io__raw_case__225_field1 = KaitaiStream(BytesIO(self._raw_case__225_field1))
            self.case__225_field1 = Id013PtjakartOperationUnsigned.Case225Field1(_io__raw_case__225_field1, self, self._root)


    class Id013PtjakartTransactionDestination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__transaction_destination_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Id013PtjakartTransactionDestinationTag, self._io.read_u1())
            if self.id_013__ptjakart__transaction_destination_tag == Id013PtjakartOperationUnsigned.Id013PtjakartTransactionDestinationTag.implicit:
                self.implicit = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)

            if self.id_013__ptjakart__transaction_destination_tag == Id013PtjakartOperationUnsigned.Id013PtjakartTransactionDestinationTag.originated:
                self.originated = Id013PtjakartOperationUnsigned.Originated(self._io, self, self._root)

            if self.id_013__ptjakart__transaction_destination_tag == Id013PtjakartOperationUnsigned.Id013PtjakartTransactionDestinationTag.tx_rollup:
                self.tx_rollup = Id013PtjakartOperationUnsigned.TxRollup(self._io, self, self._root)



    class Preendorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class Case11(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__11_field0 = self._io.read_s8be()
            self.case__11_field1 = self._io.read_bytes(32)


    class Id013PtjakartOperationAlphaContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__operation__alpha__contents_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag, self._io.read_u1())
            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.endorsement:
                self.endorsement = Id013PtjakartOperationUnsigned.Endorsement(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.preendorsement:
                self.preendorsement = Id013PtjakartOperationUnsigned.Preendorsement(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.seed_nonce_revelation:
                self.seed_nonce_revelation = Id013PtjakartOperationUnsigned.SeedNonceRevelation(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.double_endorsement_evidence:
                self.double_endorsement_evidence = Id013PtjakartOperationUnsigned.DoubleEndorsementEvidence(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.double_preendorsement_evidence:
                self.double_preendorsement_evidence = Id013PtjakartOperationUnsigned.DoublePreendorsementEvidence(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.double_baking_evidence:
                self.double_baking_evidence = Id013PtjakartOperationUnsigned.DoubleBakingEvidence(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.activate_account:
                self.activate_account = Id013PtjakartOperationUnsigned.ActivateAccount(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.proposals:
                self.proposals = Id013PtjakartOperationUnsigned.Proposals1(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.ballot:
                self.ballot = Id013PtjakartOperationUnsigned.Ballot(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.reveal:
                self.reveal = Id013PtjakartOperationUnsigned.Reveal(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.transaction:
                self.transaction = Id013PtjakartOperationUnsigned.Transaction(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.origination:
                self.origination = Id013PtjakartOperationUnsigned.Origination(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.delegation:
                self.delegation = Id013PtjakartOperationUnsigned.Delegation(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.set_deposits_limit:
                self.set_deposits_limit = Id013PtjakartOperationUnsigned.SetDepositsLimit(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.failing_noop:
                self.failing_noop = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.register_global_constant:
                self.register_global_constant = Id013PtjakartOperationUnsigned.RegisterGlobalConstant(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.tx_rollup_origination:
                self.tx_rollup_origination = Id013PtjakartOperationUnsigned.TxRollupOrigination(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.tx_rollup_submit_batch:
                self.tx_rollup_submit_batch = Id013PtjakartOperationUnsigned.TxRollupSubmitBatch(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.tx_rollup_commit:
                self.tx_rollup_commit = Id013PtjakartOperationUnsigned.TxRollupCommit(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.tx_rollup_return_bond:
                self.tx_rollup_return_bond = Id013PtjakartOperationUnsigned.TxRollupReturnBond(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.tx_rollup_finalize_commitment:
                self.tx_rollup_finalize_commitment = Id013PtjakartOperationUnsigned.TxRollupFinalizeCommitment(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.tx_rollup_remove_commitment:
                self.tx_rollup_remove_commitment = Id013PtjakartOperationUnsigned.TxRollupRemoveCommitment(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.tx_rollup_rejection:
                self.tx_rollup_rejection = Id013PtjakartOperationUnsigned.TxRollupRejection(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.tx_rollup_dispatch_tickets:
                self.tx_rollup_dispatch_tickets = Id013PtjakartOperationUnsigned.TxRollupDispatchTickets(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.transfer_ticket:
                self.transfer_ticket = Id013PtjakartOperationUnsigned.TransferTicket(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.sc_rollup_originate:
                self.sc_rollup_originate = Id013PtjakartOperationUnsigned.ScRollupOriginate(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.sc_rollup_add_messages:
                self.sc_rollup_add_messages = Id013PtjakartOperationUnsigned.ScRollupAddMessages(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.sc_rollup_cement:
                self.sc_rollup_cement = Id013PtjakartOperationUnsigned.ScRollupCement(self._io, self, self._root)

            if self.id_013__ptjakart__operation__alpha__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContentsTag.sc_rollup_publish:
                self.sc_rollup_publish = Id013PtjakartOperationUnsigned.ScRollupPublish(self._io, self, self._root)



    class TxRollupCommit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id013PtjakartOperationUnsigned.Id013PtjakartTxRollupId(self._io, self, self._root)
            self.commitment = Id013PtjakartOperationUnsigned.Commitment(self._io, self, self._root)


    class DoubleBakingEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bh1 = Id013PtjakartOperationUnsigned.Bh10(self._io, self, self._root)
            self.bh2 = Id013PtjakartOperationUnsigned.Bh20(self._io, self, self._root)


    class Id013PtjakartScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.storage = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case225Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__225_field1 = self._io.read_bytes_full()


    class Case129EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__129_elt_field0 = self._io.read_u1()
            if not self.len_case__129_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__129_elt_field0, self._io, u"/types/case__129_elt_field0_0/seq/0")
            self._raw_case__129_elt_field0 = self._io.read_bytes(self.len_case__129_elt_field0)
            _io__raw_case__129_elt_field0 = KaitaiStream(BytesIO(self._raw_case__129_elt_field0))
            self.case__129_elt_field0 = Id013PtjakartOperationUnsigned.Case129EltField0(_io__raw_case__129_elt_field0, self, self._root)


    class Case3Field3Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__3_field3_elt_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Case3Field3EltTag, self._io.read_u1())
            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__8:
                self.case__8 = Id013PtjakartOperationUnsigned.Case8(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__4:
                self.case__4 = Id013PtjakartOperationUnsigned.Case4(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__12:
                self.case__12 = Id013PtjakartOperationUnsigned.Case12(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__9:
                self.case__9 = Id013PtjakartOperationUnsigned.Case9(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__5:
                self.case__5 = Id013PtjakartOperationUnsigned.Case5(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__13:
                self.case__13 = Id013PtjakartOperationUnsigned.Case13(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__10:
                self.case__10 = Id013PtjakartOperationUnsigned.Case10(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__6:
                self.case__6 = Id013PtjakartOperationUnsigned.Case6(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__14:
                self.case__14 = Id013PtjakartOperationUnsigned.Case14(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__3:
                self.case__3 = self._io.read_s8be()

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__11:
                self.case__11 = Id013PtjakartOperationUnsigned.Case11(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__7:
                self.case__7 = Id013PtjakartOperationUnsigned.Case7(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__15:
                self.case__15 = Id013PtjakartOperationUnsigned.Case15(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__129:
                self.case__129 = Id013PtjakartOperationUnsigned.Case129Entries(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__130:
                self.case__130 = Id013PtjakartOperationUnsigned.Case130Entries(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__131:
                self.case__131 = Id013PtjakartOperationUnsigned.Case1313(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__192:
                self.case__192 = Id013PtjakartOperationUnsigned.Case1923(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__193:
                self.case__193 = Id013PtjakartOperationUnsigned.Case1933(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__195:
                self.case__195 = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__224:
                self.case__224 = Id013PtjakartOperationUnsigned.Case224(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__225:
                self.case__225 = Id013PtjakartOperationUnsigned.Case225(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__226:
                self.case__226 = Id013PtjakartOperationUnsigned.Case226(self._io, self, self._root)

            if self.case__3_field3_elt_tag == Id013PtjakartOperationUnsigned.Case3Field3EltTag.case__227:
                self.case__227 = Id013PtjakartOperationUnsigned.Case227(self._io, self, self._root)



    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id013PtjakartOperationUnsigned.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id013PtjakartOperationUnsigned.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id013PtjakartOperationUnsigned.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Case12(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__12_field0 = self._io.read_u1()
            self.case__12_field1 = Id013PtjakartOperationUnsigned.Case12Field1(self._io, self, self._root)


    class TxRollupDispatchTickets(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.tx_rollup = Id013PtjakartOperationUnsigned.Id013PtjakartTxRollupId(self._io, self, self._root)
            self.level = self._io.read_s4be()
            self.context_hash = self._io.read_bytes(32)
            self.message_index = Id013PtjakartOperationUnsigned.Int31(self._io, self, self._root)
            self.message_result_path = Id013PtjakartOperationUnsigned.MessageResultPath0(self._io, self, self._root)
            self.tickets_info = Id013PtjakartOperationUnsigned.TicketsInfo0(self._io, self, self._root)


    class Bh2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__block_header__alpha__full_header = Id013PtjakartOperationUnsigned.Id013PtjakartBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Id013PtjakartOperationAlphaUnsignedOperation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__operation__alpha__unsigned_operation = operation__shell_header.OperationShellHeader(self._io)
            self.contents = []
            i = 0
            while not self._io.is_eof():
                self.contents.append(Id013PtjakartOperationUnsigned.ContentsEntries(self._io, self, self._root))
                i += 1



    class PreviousMessageResultPathEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_list_hash = self._io.read_bytes(32)


    class Case2Field30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__2_field3 = self._io.read_u4be()
            if not self.len_case__2_field3 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__2_field3, self._io, u"/types/case__2_field3_0/seq/0")
            self._raw_case__2_field3 = self._io.read_bytes(self.len_case__2_field3)
            _io__raw_case__2_field3 = KaitaiStream(BytesIO(self._raw_case__2_field3))
            self.case__2_field3 = Id013PtjakartOperationUnsigned.Case2Field3(_io__raw_case__2_field3, self, self._root)


    class ScRollupCement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id013PtjakartOperationUnsigned.Id013PtjakartRollupAddress(self._io, self, self._root)
            self.commitment = self._io.read_bytes(32)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Amount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.AmountTag, self._io.read_u1())
            if self.amount_tag == Id013PtjakartOperationUnsigned.AmountTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.amount_tag == Id013PtjakartOperationUnsigned.AmountTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.amount_tag == Id013PtjakartOperationUnsigned.AmountTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.amount_tag == Id013PtjakartOperationUnsigned.AmountTag.case__3:
                self.case__3 = self._io.read_s8be()



    class Case1311(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131 = self._io.read_u4be()
            if not self.len_case__131 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__131, self._io, u"/types/case__131_1/seq/0")
            self._raw_case__131 = self._io.read_bytes(self.len_case__131)
            _io__raw_case__131 = KaitaiStream(BytesIO(self._raw_case__131))
            self.case__131 = Id013PtjakartOperationUnsigned.Case131(_io__raw_case__131, self, self._root)


    class Case129EltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__129_elt_field1_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Case129EltField1Tag, self._io.read_u1())
            if self.case__129_elt_field1_tag == Id013PtjakartOperationUnsigned.Case129EltField1Tag.case__0:
                self.case__0 = self._io.read_bytes(32)

            if self.case__129_elt_field1_tag == Id013PtjakartOperationUnsigned.Case129EltField1Tag.case__1:
                self.case__1 = self._io.read_bytes(32)



    class Case131(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__131_entries.append(Id013PtjakartOperationUnsigned.Case131Entries(self._io, self, self._root))
                i += 1



    class Bh20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh2 = self._io.read_u4be()
            if not self.len_bh2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh2, self._io, u"/types/bh2_0/seq/0")
            self._raw_bh2 = self._io.read_bytes(self.len_bh2)
            _io__raw_bh2 = KaitaiStream(BytesIO(self._raw_bh2))
            self.bh2 = Id013PtjakartOperationUnsigned.Bh2(_io__raw_bh2, self, self._root)


    class Commitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.messages = Id013PtjakartOperationUnsigned.Messages0(self._io, self, self._root)
            self.predecessor = Id013PtjakartOperationUnsigned.Predecessor(self._io, self, self._root)
            self.inbox_merkle_root = self._io.read_bytes(32)


    class Case2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__2_field0 = self._io.read_s2be()
            self.case__2_field1 = self._io.read_bytes(32)
            self.case__2_field2 = self._io.read_bytes(32)
            self.case__2_field3 = Id013PtjakartOperationUnsigned.Case2Field30(self._io, self, self._root)


    class TicketsInfoEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.ty = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.ticketer = Id013PtjakartOperationUnsigned.Id013PtjakartContractId(self._io, self, self._root)
            self.amount = Id013PtjakartOperationUnsigned.Amount(self._io, self, self._root)
            self.claimer = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)


    class Id013PtjakartInlinedPreendorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__inlined__preendorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id013PtjakartOperationUnsigned.Id013PtjakartInlinedPreendorsementContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Bool, self._io.read_u1())
            if self.signature_tag == Id013PtjakartOperationUnsigned.Bool.true:
                self.signature = self._io.read_bytes(64)



    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Bool, self._io.read_u1())
            if self.delegate_tag == Id013PtjakartOperationUnsigned.Bool.true:
                self.delegate = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)



    class Case1923(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__192 = self._io.read_u1()
            if not self.len_case__192 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__192, self._io, u"/types/case__192_3/seq/0")
            self._raw_case__192 = self._io.read_bytes(self.len_case__192)
            _io__raw_case__192 = KaitaiStream(BytesIO(self._raw_case__192))
            self.case__192 = Id013PtjakartOperationUnsigned.Case192(_io__raw_case__192, self, self._root)


    class RegisterGlobalConstant(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.value = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case129Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__129_elt_field0 = Id013PtjakartOperationUnsigned.Case129EltField00(self._io, self, self._root)
            self.case__129_elt_field1 = Id013PtjakartOperationUnsigned.Case129EltField1(self._io, self, self._root)


    class Case2Field3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__2_field3_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__2_field3_entries.append(Id013PtjakartOperationUnsigned.Case2Field3Entries(self._io, self, self._root))
                i += 1



    class PreviousMessageResult(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.context_hash = self._io.read_bytes(32)
            self.withdraw_list_hash = self._io.read_bytes(32)


    class Case1Field30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__1_field3 = self._io.read_u4be()
            if not self.len_case__1_field3 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__1_field3, self._io, u"/types/case__1_field3_0/seq/0")
            self._raw_case__1_field3 = self._io.read_bytes(self.len_case__1_field3)
            _io__raw_case__1_field3 = KaitaiStream(BytesIO(self._raw_case__1_field3))
            self.case__1_field3 = Id013PtjakartOperationUnsigned.Case1Field3(_io__raw_case__1_field3, self, self._root)


    class Case130EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__130_elt_field0 = self._io.read_bytes_full()


    class Case6(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__6_field0 = self._io.read_s4be()
            self.case__6_field1 = self._io.read_bytes(32)


    class ContentsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__operation__alpha__contents = Id013PtjakartOperationUnsigned.Id013PtjakartOperationAlphaContents(self._io, self, self._root)


    class PreviousMessageResultPath(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.previous_message_result_path_entries = []
            i = 0
            while not self._io.is_eof():
                self.previous_message_result_path_entries.append(Id013PtjakartOperationUnsigned.PreviousMessageResultPathEntries(self._io, self, self._root))
                i += 1



    class ScRollupAddMessages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id013PtjakartOperationUnsigned.Id013PtjakartRollupAddress(self._io, self, self._root)
            self.message = Id013PtjakartOperationUnsigned.Message1(self._io, self, self._root)


    class Id013PtjakartLiquidityBakingToggleVote(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__liquidity_baking_toggle_vote = self._io.read_s1()


    class Case1Field3Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__1_field3_elt_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Case1Field3EltTag, self._io.read_u1())
            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__8:
                self.case__8 = Id013PtjakartOperationUnsigned.Case8(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__4:
                self.case__4 = Id013PtjakartOperationUnsigned.Case4(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__12:
                self.case__12 = Id013PtjakartOperationUnsigned.Case12(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__9:
                self.case__9 = Id013PtjakartOperationUnsigned.Case9(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__5:
                self.case__5 = Id013PtjakartOperationUnsigned.Case5(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__13:
                self.case__13 = Id013PtjakartOperationUnsigned.Case13(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__10:
                self.case__10 = Id013PtjakartOperationUnsigned.Case10(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__6:
                self.case__6 = Id013PtjakartOperationUnsigned.Case6(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__14:
                self.case__14 = Id013PtjakartOperationUnsigned.Case14(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__3:
                self.case__3 = self._io.read_s8be()

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__11:
                self.case__11 = Id013PtjakartOperationUnsigned.Case11(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__7:
                self.case__7 = Id013PtjakartOperationUnsigned.Case7(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__15:
                self.case__15 = Id013PtjakartOperationUnsigned.Case15(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__129:
                self.case__129 = Id013PtjakartOperationUnsigned.Case129Entries(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__130:
                self.case__130 = Id013PtjakartOperationUnsigned.Case130Entries(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__131:
                self.case__131 = Id013PtjakartOperationUnsigned.Case1312(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__192:
                self.case__192 = Id013PtjakartOperationUnsigned.Case1922(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__193:
                self.case__193 = Id013PtjakartOperationUnsigned.Case1932(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__195:
                self.case__195 = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__224:
                self.case__224 = Id013PtjakartOperationUnsigned.Case224(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__225:
                self.case__225 = Id013PtjakartOperationUnsigned.Case225(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__226:
                self.case__226 = Id013PtjakartOperationUnsigned.Case226(self._io, self, self._root)

            if self.case__1_field3_elt_tag == Id013PtjakartOperationUnsigned.Case1Field3EltTag.case__227:
                self.case__227 = Id013PtjakartOperationUnsigned.Case227(self._io, self, self._root)



    class Id013PtjakartMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__mutez = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)


    class PreviousMessageResultPath0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_previous_message_result_path = self._io.read_u4be()
            if not self.len_previous_message_result_path <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_previous_message_result_path, self._io, u"/types/previous_message_result_path_0/seq/0")
            self._raw_previous_message_result_path = self._io.read_bytes(self.len_previous_message_result_path)
            _io__raw_previous_message_result_path = KaitaiStream(BytesIO(self._raw_previous_message_result_path))
            self.previous_message_result_path = Id013PtjakartOperationUnsigned.PreviousMessageResultPath(_io__raw_previous_message_result_path, self, self._root)


    class Case0Field3Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field3_elt_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Case0Field3EltTag, self._io.read_u1())
            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__0:
                self.case__0 = self._io.read_u1()

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__8:
                self.case__8 = Id013PtjakartOperationUnsigned.Case8(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__4:
                self.case__4 = Id013PtjakartOperationUnsigned.Case4(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__12:
                self.case__12 = Id013PtjakartOperationUnsigned.Case12(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__1:
                self.case__1 = self._io.read_u2be()

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__9:
                self.case__9 = Id013PtjakartOperationUnsigned.Case9(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__5:
                self.case__5 = Id013PtjakartOperationUnsigned.Case5(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__13:
                self.case__13 = Id013PtjakartOperationUnsigned.Case13(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__2:
                self.case__2 = self._io.read_s4be()

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__10:
                self.case__10 = Id013PtjakartOperationUnsigned.Case10(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__6:
                self.case__6 = Id013PtjakartOperationUnsigned.Case6(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__14:
                self.case__14 = Id013PtjakartOperationUnsigned.Case14(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__3:
                self.case__3 = self._io.read_s8be()

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__11:
                self.case__11 = Id013PtjakartOperationUnsigned.Case11(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__7:
                self.case__7 = Id013PtjakartOperationUnsigned.Case7(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__15:
                self.case__15 = Id013PtjakartOperationUnsigned.Case15(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__129:
                self.case__129 = Id013PtjakartOperationUnsigned.Case129Entries(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__130:
                self.case__130 = Id013PtjakartOperationUnsigned.Case130Entries(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__131:
                self.case__131 = Id013PtjakartOperationUnsigned.Case1310(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__192:
                self.case__192 = Id013PtjakartOperationUnsigned.Case1920(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__193:
                self.case__193 = Id013PtjakartOperationUnsigned.Case1930(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__195:
                self.case__195 = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__224:
                self.case__224 = Id013PtjakartOperationUnsigned.Case224(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__225:
                self.case__225 = Id013PtjakartOperationUnsigned.Case225(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__226:
                self.case__226 = Id013PtjakartOperationUnsigned.Case226(self._io, self, self._root)

            if self.case__0_field3_elt_tag == Id013PtjakartOperationUnsigned.Case0Field3EltTag.case__227:
                self.case__227 = Id013PtjakartOperationUnsigned.Case227(self._io, self, self._root)



    class Bh10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh1 = self._io.read_u4be()
            if not self.len_bh1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh1, self._io, u"/types/bh1_0/seq/0")
            self._raw_bh1 = self._io.read_bytes(self.len_bh1)
            _io__raw_bh1 = KaitaiStream(BytesIO(self._raw_bh1))
            self.bh1 = Id013PtjakartOperationUnsigned.Bh1(_io__raw_bh1, self, self._root)


    class Case131EltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_elt_field0 = self._io.read_bytes_full()


    class Case3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__3_field0 = self._io.read_s2be()
            self.case__3_field1 = self._io.read_bytes(32)
            self.case__3_field2 = self._io.read_bytes(32)
            self.case__3_field3 = Id013PtjakartOperationUnsigned.Case3Field30(self._io, self, self._root)


    class Case224(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__224_field0 = self._io.read_u1()
            self.case__224_field1 = Id013PtjakartOperationUnsigned.Case224Field10(self._io, self, self._root)
            self.case__224_field2 = self._io.read_bytes(32)


    class Op22(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_2/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id013PtjakartOperationUnsigned.Op21(_io__raw_op2, self, self._root)


    class Case14Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__14_field1_field0 = self._io.read_bytes(32)
            self.case__14_field1_field1 = self._io.read_bytes(32)


    class Case227Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__227_field1 = self._io.read_u1()
            if not self.len_case__227_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__227_field1, self._io, u"/types/case__227_field1_0/seq/0")
            self._raw_case__227_field1 = self._io.read_bytes(self.len_case__227_field1)
            _io__raw_case__227_field1 = KaitaiStream(BytesIO(self._raw_case__227_field1))
            self.case__227_field1 = Id013PtjakartOperationUnsigned.Case227Field1(_io__raw_case__227_field1, self, self._root)


    class Message0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_entries.append(Id013PtjakartOperationUnsigned.MessageEntries(self._io, self, self._root))
                i += 1



    class Case1930(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__193 = self._io.read_u2be()
            if not self.len_case__193 <= 65535:
                raise kaitaistruct.ValidationGreaterThanError(65535, self.len_case__193, self._io, u"/types/case__193_0/seq/0")
            self._raw_case__193 = self._io.read_bytes(self.len_case__193)
            _io__raw_case__193 = KaitaiStream(BytesIO(self._raw_case__193))
            self.case__193 = Id013PtjakartOperationUnsigned.Case193(_io__raw_case__193, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Case131Entries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__131_elt_field0 = Id013PtjakartOperationUnsigned.Case131EltField00(self._io, self, self._root)
            self.case__131_elt_field1 = Id013PtjakartOperationUnsigned.Case131EltField1(self._io, self, self._root)


    class Bh1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__block_header__alpha__full_header = Id013PtjakartOperationUnsigned.Id013PtjakartBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Case0Field3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__0_field3_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__0_field3_entries.append(Id013PtjakartOperationUnsigned.Case0Field3Entries(self._io, self, self._root))
                i += 1



    class TxRollupRejection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id013PtjakartOperationUnsigned.Id013PtjakartTxRollupId(self._io, self, self._root)
            self.level = self._io.read_s4be()
            self.message = Id013PtjakartOperationUnsigned.Message(self._io, self, self._root)
            self.message_position = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.message_path = Id013PtjakartOperationUnsigned.MessagePath0(self._io, self, self._root)
            self.message_result_hash = self._io.read_bytes(32)
            self.message_result_path = Id013PtjakartOperationUnsigned.MessageResultPath0(self._io, self, self._root)
            self.previous_message_result = Id013PtjakartOperationUnsigned.PreviousMessageResult(self._io, self, self._root)
            self.previous_message_result_path = Id013PtjakartOperationUnsigned.PreviousMessageResultPath0(self._io, self, self._root)
            self.proof = Id013PtjakartOperationUnsigned.Proof(self._io, self, self._root)


    class SetDepositsLimit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.limit_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Bool, self._io.read_u1())
            if self.limit_tag == Id013PtjakartOperationUnsigned.Bool.true:
                self.limit = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)



    class Case15(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__15_field0 = self._io.read_s8be()
            self.case__15_field1 = Id013PtjakartOperationUnsigned.Case15Field1(self._io, self, self._root)


    class Message1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message = self._io.read_u4be()
            if not self.len_message <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message, self._io, u"/types/message_1/seq/0")
            self._raw_message = self._io.read_bytes(self.len_message)
            _io__raw_message = KaitaiStream(BytesIO(self._raw_message))
            self.message = Id013PtjakartOperationUnsigned.Message0(_io__raw_message, self, self._root)


    class Op10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_0/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id013PtjakartOperationUnsigned.Op1(_io__raw_op1, self, self._root)


    class Op21(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__inlined__preendorsement = Id013PtjakartOperationUnsigned.Id013PtjakartInlinedPreendorsement(self._io, self, self._root)


    class Id013PtjakartBlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_013__ptjakart__block_header__alpha__signed_contents = Id013PtjakartOperationUnsigned.Id013PtjakartBlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Proof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proof_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.ProofTag, self._io.read_u1())
            if self.proof_tag == Id013PtjakartOperationUnsigned.ProofTag.case__0:
                self.case__0 = Id013PtjakartOperationUnsigned.Case0(self._io, self, self._root)

            if self.proof_tag == Id013PtjakartOperationUnsigned.ProofTag.case__2:
                self.case__2 = Id013PtjakartOperationUnsigned.Case2(self._io, self, self._root)

            if self.proof_tag == Id013PtjakartOperationUnsigned.ProofTag.case__1:
                self.case__1 = Id013PtjakartOperationUnsigned.Case1(self._io, self, self._root)

            if self.proof_tag == Id013PtjakartOperationUnsigned.ProofTag.case__3:
                self.case__3 = Id013PtjakartOperationUnsigned.Case3(self._io, self, self._root)



    class Commitment0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.compressed_state = self._io.read_bytes(32)
            self.inbox_level = self._io.read_s4be()
            self.predecessor = self._io.read_bytes(32)
            self.number_of_messages = self._io.read_s4be()
            self.number_of_ticks = self._io.read_s4be()


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.balance = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Bool, self._io.read_u1())
            if self.delegate_tag == Id013PtjakartOperationUnsigned.Bool.true:
                self.delegate = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)

            self.script = Id013PtjakartOperationUnsigned.Id013PtjakartScriptedContracts(self._io, self, self._root)


    class Case226Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__226_field1 = self._io.read_u1()
            if not self.len_case__226_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__226_field1, self._io, u"/types/case__226_field1_0/seq/0")
            self._raw_case__226_field1 = self._io.read_bytes(self.len_case__226_field1)
            _io__raw_case__226_field1 = KaitaiStream(BytesIO(self._raw_case__226_field1))
            self.case__226_field1 = Id013PtjakartOperationUnsigned.Case226Field1(_io__raw_case__226_field1, self, self._root)


    class Case1312(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131 = self._io.read_u4be()
            if not self.len_case__131 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__131, self._io, u"/types/case__131_2/seq/0")
            self._raw_case__131 = self._io.read_bytes(self.len_case__131)
            _io__raw_case__131 = KaitaiStream(BytesIO(self._raw_case__131))
            self.case__131 = Id013PtjakartOperationUnsigned.Case131(_io__raw_case__131, self, self._root)


    class Case130EltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__130_elt_field1_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Case130EltField1Tag, self._io.read_u1())
            if self.case__130_elt_field1_tag == Id013PtjakartOperationUnsigned.Case130EltField1Tag.case__0:
                self.case__0 = self._io.read_bytes(32)

            if self.case__130_elt_field1_tag == Id013PtjakartOperationUnsigned.Case130EltField1Tag.case__1:
                self.case__1 = self._io.read_bytes(32)



    class Message(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.MessageTag, self._io.read_u1())
            if self.message_tag == Id013PtjakartOperationUnsigned.MessageTag.batch:
                self.batch = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)

            if self.message_tag == Id013PtjakartOperationUnsigned.MessageTag.deposit:
                self.deposit = Id013PtjakartOperationUnsigned.Deposit(self._io, self, self._root)



    class Case1932(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__193 = self._io.read_u2be()
            if not self.len_case__193 <= 65535:
                raise kaitaistruct.ValidationGreaterThanError(65535, self.len_case__193, self._io, u"/types/case__193_2/seq/0")
            self._raw_case__193 = self._io.read_bytes(self.len_case__193)
            _io__raw_case__193 = KaitaiStream(BytesIO(self._raw_case__193))
            self.case__193 = Id013PtjakartOperationUnsigned.Case193(_io__raw_case__193, self, self._root)


    class Op2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__inlined__endorsement = Id013PtjakartOperationUnsigned.Id013PtjakartInlinedEndorsement(self._io, self, self._root)


    class Case7(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__7_field0 = self._io.read_s8be()
            self.case__7_field1 = self._io.read_bytes(32)


    class TxRollupReturnBond(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id013PtjakartOperationUnsigned.Id013PtjakartTxRollupId(self._io, self, self._root)


    class Case224Field10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__224_field1 = self._io.read_u1()
            if not self.len_case__224_field1 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__224_field1, self._io, u"/types/case__224_field1_0/seq/0")
            self._raw_case__224_field1 = self._io.read_bytes(self.len_case__224_field1)
            _io__raw_case__224_field1 = KaitaiStream(BytesIO(self._raw_case__224_field1))
            self.case__224_field1 = Id013PtjakartOperationUnsigned.Case224Field1(_io__raw_case__224_field1, self, self._root)


    class TicketsInfo0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_tickets_info = self._io.read_u4be()
            if not self.len_tickets_info <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_tickets_info, self._io, u"/types/tickets_info_0/seq/0")
            self._raw_tickets_info = self._io.read_bytes(self.len_tickets_info)
            _io__raw_tickets_info = KaitaiStream(BytesIO(self._raw_tickets_info))
            self.tickets_info = Id013PtjakartOperationUnsigned.TicketsInfo(_io__raw_tickets_info, self, self._root)


    class TxRollupFinalizeCommitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id013PtjakartOperationUnsigned.Id013PtjakartTxRollupId(self._io, self, self._root)


    class TxRollupOrigination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)


    class Case227Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__227_field1 = self._io.read_bytes_full()


    class Id013PtjakartRollupAddress(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__rollup_address = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class MessageResultPath(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_result_path_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_result_path_entries.append(Id013PtjakartOperationUnsigned.MessageResultPathEntries(self._io, self, self._root))
                i += 1



    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class MessageResultPath0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message_result_path = self._io.read_u4be()
            if not self.len_message_result_path <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message_result_path, self._io, u"/types/message_result_path_0/seq/0")
            self._raw_message_result_path = self._io.read_bytes(self.len_message_result_path)
            _io__raw_message_result_path = KaitaiStream(BytesIO(self._raw_message_result_path))
            self.message_result_path = Id013PtjakartOperationUnsigned.MessageResultPath(_io__raw_message_result_path, self, self._root)


    class MessagePathEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.inbox_list_hash = self._io.read_bytes(32)


    class Deposit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sender = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.destination = self._io.read_bytes(20)
            self.ticket_hash = self._io.read_bytes(32)
            self.amount = Id013PtjakartOperationUnsigned.Amount(self._io, self, self._root)


    class Id013PtjakartInlinedEndorsementMempoolContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__inlined__endorsement_mempool__contents_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Id013PtjakartInlinedEndorsementMempoolContentsTag, self._io.read_u1())
            if self.id_013__ptjakart__inlined__endorsement_mempool__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartInlinedEndorsementMempoolContentsTag.endorsement:
                self.endorsement = Id013PtjakartOperationUnsigned.Endorsement(self._io, self, self._root)



    class MessagePath(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_path_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_path_entries.append(Id013PtjakartOperationUnsigned.MessagePathEntries(self._io, self, self._root))
                i += 1



    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id013PtjakartOperationUnsigned.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.amount = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.destination = Id013PtjakartOperationUnsigned.Id013PtjakartTransactionDestination(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Bool, self._io.read_u1())
            if self.parameters_tag == Id013PtjakartOperationUnsigned.Bool.true:
                self.parameters = Id013PtjakartOperationUnsigned.Parameters(self._io, self, self._root)



    class Case14(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__14_field0 = self._io.read_s4be()
            self.case__14_field1 = Id013PtjakartOperationUnsigned.Case14Field1(self._io, self, self._root)


    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id013PtjakartOperationUnsigned.Id013PtjakartEntrypoint(self._io, self, self._root)
            self.value = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)


    class Case225(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__225_field0 = self._io.read_u2be()
            self.case__225_field1 = Id013PtjakartOperationUnsigned.Case225Field10(self._io, self, self._root)
            self.case__225_field2 = self._io.read_bytes(32)


    class Case224Field1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__224_field1 = self._io.read_bytes_full()


    class Messages0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_messages = self._io.read_u4be()
            if not self.len_messages <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_messages, self._io, u"/types/messages_0/seq/0")
            self._raw_messages = self._io.read_bytes(self.len_messages)
            _io__raw_messages = KaitaiStream(BytesIO(self._raw_messages))
            self.messages = Id013PtjakartOperationUnsigned.Messages(_io__raw_messages, self, self._root)


    class Case1920(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__192 = self._io.read_u1()
            if not self.len_case__192 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__192, self._io, u"/types/case__192_0/seq/0")
            self._raw_case__192 = self._io.read_bytes(self.len_case__192)
            _io__raw_case__192 = KaitaiStream(BytesIO(self._raw_case__192))
            self.case__192 = Id013PtjakartOperationUnsigned.Case192(_io__raw_case__192, self, self._root)


    class Case1313(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__131 = self._io.read_u4be()
            if not self.len_case__131 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__131, self._io, u"/types/case__131_3/seq/0")
            self._raw_case__131 = self._io.read_bytes(self.len_case__131)
            _io__raw_case__131 = KaitaiStream(BytesIO(self._raw_case__131))
            self.case__131 = Id013PtjakartOperationUnsigned.Case131(_io__raw_case__131, self, self._root)


    class Proposals1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposals = Id013PtjakartOperationUnsigned.Proposals0(self._io, self, self._root)


    class TxRollupSubmitBatch(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.fee = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)
            self.counter = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.gas_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.storage_limit = Id013PtjakartOperationUnsigned.N(self._io, self, self._root)
            self.rollup = Id013PtjakartOperationUnsigned.Id013PtjakartTxRollupId(self._io, self, self._root)
            self.content = Id013PtjakartOperationUnsigned.BytesDynUint30(self._io, self, self._root)
            self.burn_limit_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Bool, self._io.read_u1())
            if self.burn_limit_tag == Id013PtjakartOperationUnsigned.Bool.true:
                self.burn_limit = Id013PtjakartOperationUnsigned.Id013PtjakartMutez(self._io, self, self._root)



    class Case130EltField00(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__130_elt_field0 = self._io.read_u1()
            if not self.len_case__130_elt_field0 <= 255:
                raise kaitaistruct.ValidationGreaterThanError(255, self.len_case__130_elt_field0, self._io, u"/types/case__130_elt_field0_0/seq/0")
            self._raw_case__130_elt_field0 = self._io.read_bytes(self.len_case__130_elt_field0)
            _io__raw_case__130_elt_field0 = KaitaiStream(BytesIO(self._raw_case__130_elt_field0))
            self.case__130_elt_field0 = Id013PtjakartOperationUnsigned.Case130EltField0(_io__raw_case__130_elt_field0, self, self._root)


    class TicketsInfo(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.tickets_info_entries = []
            i = 0
            while not self._io.is_eof():
                self.tickets_info_entries.append(Id013PtjakartOperationUnsigned.TicketsInfoEntries(self._io, self, self._root))
                i += 1



    class Ballot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationUnsigned.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposal = self._io.read_bytes(32)
            self.ballot = self._io.read_s1()


    class Predecessor(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.predecessor_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.PredecessorTag, self._io.read_u1())
            if self.predecessor_tag == Id013PtjakartOperationUnsigned.PredecessorTag.some:
                self.some = self._io.read_bytes(32)



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id013PtjakartOperationUnsigned.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id013PtjakartOperationUnsigned.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id013PtjakartOperationUnsigned.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Case1Field3(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.case__1_field3_entries = []
            i = 0
            while not self._io.is_eof():
                self.case__1_field3_entries.append(Id013PtjakartOperationUnsigned.Case1Field3Entries(self._io, self, self._root))
                i += 1



    class MessagePath0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message_path = self._io.read_u4be()
            if not self.len_message_path <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message_path, self._io, u"/types/message_path_0/seq/0")
            self._raw_message_path = self._io.read_bytes(self.len_message_path)
            _io__raw_message_path = KaitaiStream(BytesIO(self._raw_message_path))
            self.message_path = Id013PtjakartOperationUnsigned.MessagePath(_io__raw_message_path, self, self._root)


    class TxRollup(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__tx_rollup_id = Id013PtjakartOperationUnsigned.Id013PtjakartTxRollupId(self._io, self, self._root)
            self.tx_rollup_padding = self._io.read_bytes(1)


    class Op11(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__inlined__preendorsement = Id013PtjakartOperationUnsigned.Id013PtjakartInlinedPreendorsement(self._io, self, self._root)


    class Proposals(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proposals_entries = []
            i = 0
            while not self._io.is_eof():
                self.proposals_entries.append(Id013PtjakartOperationUnsigned.ProposalsEntries(self._io, self, self._root))
                i += 1



    class Case1931(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__193 = self._io.read_u2be()
            if not self.len_case__193 <= 65535:
                raise kaitaistruct.ValidationGreaterThanError(65535, self.len_case__193, self._io, u"/types/case__193_1/seq/0")
            self._raw_case__193 = self._io.read_bytes(self.len_case__193)
            _io__raw_case__193 = KaitaiStream(BytesIO(self._raw_case__193))
            self.case__193 = Id013PtjakartOperationUnsigned.Case193(_io__raw_case__193, self, self._root)


    class ProposalsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol_hash = self._io.read_bytes(32)


    class Id013PtjakartInlinedPreendorsementContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__inlined__preendorsement__contents_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationUnsigned.Id013PtjakartInlinedPreendorsementContentsTag, self._io.read_u1())
            if self.id_013__ptjakart__inlined__preendorsement__contents_tag == Id013PtjakartOperationUnsigned.Id013PtjakartInlinedPreendorsementContentsTag.preendorsement:
                self.preendorsement = Id013PtjakartOperationUnsigned.Preendorsement(self._io, self, self._root)



    class Op1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__inlined__endorsement = Id013PtjakartOperationUnsigned.Id013PtjakartInlinedEndorsement(self._io, self, self._root)


    class Case0Field30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_case__0_field3 = self._io.read_u4be()
            if not self.len_case__0_field3 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_case__0_field3, self._io, u"/types/case__0_field3_0/seq/0")
            self._raw_case__0_field3 = self._io.read_bytes(self.len_case__0_field3)
            _io__raw_case__0_field3 = KaitaiStream(BytesIO(self._raw_case__0_field3))
            self.case__0_field3 = Id013PtjakartOperationUnsigned.Case0Field3(_io__raw_case__0_field3, self, self._root)


    class DoublePreendorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id013PtjakartOperationUnsigned.Op12(self._io, self, self._root)
            self.op2 = Id013PtjakartOperationUnsigned.Op22(self._io, self, self._root)



