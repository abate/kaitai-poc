# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id017PtnairobOperationInternal(KaitaiStruct):
    """Encoding id: 017-PtNairob.operation.internal."""

    class Id017PtnairobEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        deposit = 5
        named = 255

    class Id017PtnairobMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left = 5
        none_0 = 6
        pair_1 = 7
        right = 8
        some_0 = 9
        true = 10
        unit_0 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left_0 = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right_0 = 68
        size = 69
        some = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_1 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction_deprecated = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_deprecated = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140
        chest = 141
        chest_key = 142
        open_chest = 143
        view_0 = 144
        view = 145
        constant = 146
        sub_mutez = 147
        tx_rollup_l2_address = 148
        min_block_time = 149
        sapling_transaction = 150
        emit = 151
        lambda_rec = 152
        lambda_rec_0 = 153
        ticket_0 = 154
        bytes_0 = 155
        nat_0 = 156

    class Micheline017PtnairobMichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10

    class Id017PtnairobApplyInternalResultsAlphaOperationResultTag(Enum):
        transaction = 1
        origination = 2
        delegation = 3
        event = 4

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class Bool(Enum):
        false = 0
        true = 255

    class Id017PtnairobTransactionDestinationTag(Enum):
        implicit = 0
        originated = 1
        tx_rollup = 2
        smart_rollup = 3
        zk_rollup = 4
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_017__ptnairob__apply_internal_results__alpha__operation_result = Id017PtnairobOperationInternal.Id017PtnairobApplyInternalResultsAlphaOperationResult(self._io, self, self._root)

    class Id017PtnairobApplyInternalResultsAlphaOperationResult(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationInternal.Id017PtnairobTransactionDestination(self._io, self, self._root)
            self.nonce = self._io.read_u2be()
            self.id_017__ptnairob__apply_internal_results__alpha__operation_result_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationInternal.Id017PtnairobApplyInternalResultsAlphaOperationResultTag, self._io.read_u1())
            if self.id_017__ptnairob__apply_internal_results__alpha__operation_result_tag == Id017PtnairobOperationInternal.Id017PtnairobApplyInternalResultsAlphaOperationResultTag.transaction:
                self.transaction = Id017PtnairobOperationInternal.Transaction(self._io, self, self._root)

            if self.id_017__ptnairob__apply_internal_results__alpha__operation_result_tag == Id017PtnairobOperationInternal.Id017PtnairobApplyInternalResultsAlphaOperationResultTag.origination:
                self.origination = Id017PtnairobOperationInternal.Origination(self._io, self, self._root)

            if self.id_017__ptnairob__apply_internal_results__alpha__operation_result_tag == Id017PtnairobOperationInternal.Id017PtnairobApplyInternalResultsAlphaOperationResultTag.delegation:
                self.delegation = Id017PtnairobOperationInternal.Delegation(self._io, self, self._root)

            if self.id_017__ptnairob__apply_internal_results__alpha__operation_result_tag == Id017PtnairobOperationInternal.Id017PtnairobApplyInternalResultsAlphaOperationResultTag.event:
                self.event = Id017PtnairobOperationInternal.Event(self._io, self, self._root)



    class Id017PtnairobMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__michelson__v1__primitives = KaitaiStream.resolve_enum(Id017PtnairobOperationInternal.Id017PtnairobMichelsonV1Primitives, self._io.read_u1())


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id017PtnairobOperationInternal.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class SmartRollup(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.smart_rollup_hash = self._io.read_bytes(20)
            self.smart_rollup_padding = self._io.read_bytes(1)


    class Event(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.type = Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)
            self.tag_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationInternal.Bool, self._io.read_u1())
            if self.tag_tag == Id017PtnairobOperationInternal.Bool.true:
                self.tag = Id017PtnairobOperationInternal.Id017PtnairobEntrypoint(self._io, self, self._root)

            self.payload_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationInternal.Bool, self._io.read_u1())
            if self.payload_tag == Id017PtnairobOperationInternal.Bool.true:
                self.payload = Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)



    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id017PtnairobOperationInternal.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id017PtnairobOperationInternal.BytesDynUint30(self._io, self, self._root)


    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id017PtnairobOperationInternal.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)
            self.annots = Id017PtnairobOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Id017PtnairobTxRollupId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup_hash = self._io.read_bytes(20)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id017PtnairobOperationInternal.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)
            self.args = Id017PtnairobOperationInternal.Args0(self._io, self, self._root)
            self.annots = Id017PtnairobOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id017PtnairobOperationInternal.Bool.true:
                self.delegate = Id017PtnairobOperationInternal.PublicKeyHash(self._io, self, self._root)



    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = Id017PtnairobOperationInternal.Sequence(_io__raw_sequence, self, self._root)


    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(Id017PtnairobOperationInternal.SequenceEntries(self._io, self, self._root))
                i += 1



    class Id017PtnairobScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id017PtnairobOperationInternal.BytesDynUint30(self._io, self, self._root)
            self.storage = Id017PtnairobOperationInternal.BytesDynUint30(self._io, self, self._root)


    class ZkRollup(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.zk_rollup_hash = self._io.read_bytes(20)
            self.zk_rollup_padding = self._io.read_bytes(1)


    class Id017PtnairobTransactionDestination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__transaction_destination_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationInternal.Id017PtnairobTransactionDestinationTag, self._io.read_u1())
            if self.id_017__ptnairob__transaction_destination_tag == Id017PtnairobOperationInternal.Id017PtnairobTransactionDestinationTag.implicit:
                self.implicit = Id017PtnairobOperationInternal.PublicKeyHash(self._io, self, self._root)

            if self.id_017__ptnairob__transaction_destination_tag == Id017PtnairobOperationInternal.Id017PtnairobTransactionDestinationTag.originated:
                self.originated = Id017PtnairobOperationInternal.Originated(self._io, self, self._root)

            if self.id_017__ptnairob__transaction_destination_tag == Id017PtnairobOperationInternal.Id017PtnairobTransactionDestinationTag.tx_rollup:
                self.tx_rollup = Id017PtnairobOperationInternal.TxRollup(self._io, self, self._root)

            if self.id_017__ptnairob__transaction_destination_tag == Id017PtnairobOperationInternal.Id017PtnairobTransactionDestinationTag.smart_rollup:
                self.smart_rollup = Id017PtnairobOperationInternal.SmartRollup(self._io, self, self._root)

            if self.id_017__ptnairob__transaction_destination_tag == Id017PtnairobOperationInternal.Id017PtnairobTransactionDestinationTag.zk_rollup:
                self.zk_rollup = Id017PtnairobOperationInternal.ZkRollup(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Id017PtnairobEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__entrypoint_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationInternal.Id017PtnairobEntrypointTag, self._io.read_u1())
            if self.id_017__ptnairob__entrypoint_tag == Id017PtnairobOperationInternal.Id017PtnairobEntrypointTag.named:
                self.named = Id017PtnairobOperationInternal.Named0(self._io, self, self._root)



    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.balance = Id017PtnairobOperationInternal.Id017PtnairobMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id017PtnairobOperationInternal.Bool.true:
                self.delegate = Id017PtnairobOperationInternal.PublicKeyHash(self._io, self, self._root)

            self.script = Id017PtnairobOperationInternal.Id017PtnairobScriptedContracts(self._io, self, self._root)


    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(Id017PtnairobOperationInternal.ArgsEntries(self._io, self, self._root))
                i += 1



    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id017PtnairobOperationInternal.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)


    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id017PtnairobOperationInternal.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)


    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)


    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = Id017PtnairobOperationInternal.Args(_io__raw_args, self, self._root)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id017PtnairobOperationInternal.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount = Id017PtnairobOperationInternal.Id017PtnairobMutez(self._io, self, self._root)
            self.destination = Id017PtnairobOperationInternal.Id017PtnairobTransactionDestination(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationInternal.Bool, self._io.read_u1())
            if self.parameters_tag == Id017PtnairobOperationInternal.Bool.true:
                self.parameters = Id017PtnairobOperationInternal.Parameters(self._io, self, self._root)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id017PtnairobOperationInternal.Id017PtnairobEntrypoint(self._io, self, self._root)
            self.value = Id017PtnairobOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Id017PtnairobMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__mutez = Id017PtnairobOperationInternal.N(self._io, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id017PtnairobOperationInternal.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id017PtnairobOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Micheline017PtnairobMichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__017__ptnairob__michelson_v1__expression_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1ExpressionTag.int:
                self.int = Id017PtnairobOperationInternal.Z(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1ExpressionTag.string:
                self.string = Id017PtnairobOperationInternal.BytesDynUint30(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1ExpressionTag.sequence:
                self.sequence = Id017PtnairobOperationInternal.Sequence0(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = Id017PtnairobOperationInternal.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = Id017PtnairobOperationInternal.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = Id017PtnairobOperationInternal.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = Id017PtnairobOperationInternal.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = Id017PtnairobOperationInternal.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = Id017PtnairobOperationInternal.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = Id017PtnairobOperationInternal.PrimGeneric(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationInternal.Micheline017PtnairobMichelsonV1ExpressionTag.bytes:
                self.bytes = Id017PtnairobOperationInternal.BytesDynUint30(self._io, self, self._root)



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationInternal.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id017PtnairobOperationInternal.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id017PtnairobOperationInternal.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id017PtnairobOperationInternal.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id017PtnairobOperationInternal.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class TxRollup(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__tx_rollup_id = Id017PtnairobOperationInternal.Id017PtnairobTxRollupId(self._io, self, self._root)
            self.tx_rollup_padding = self._io.read_bytes(1)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id017PtnairobOperationInternal.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




