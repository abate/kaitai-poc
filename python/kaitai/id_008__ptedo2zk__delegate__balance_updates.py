# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id008Ptedo2zkDelegateBalanceUpdates(KaitaiStruct):
    """Encoding id: 008-PtEdo2Zk.delegate.balance_updates."""

    class Id008Ptedo2zkContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id008Ptedo2zkOperationMetadataAlphaBalanceTag(Enum):
        contract = 0
        rewards = 1
        fees = 2
        deposits = 3

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_008__ptedo2zk__operation_metadata__alpha__balance_updates = Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id008Ptedo2zkOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__operation_metadata__alpha__balance = Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalance(self._io, self, self._root)
            self.id_008__ptedo2zk__operation_metadata__alpha__balance_update = Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceUpdate(self._io, self, self._root)


    class Rewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Deposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_008__ptedo2zk__operation_metadata__alpha__balance_updates_entries.append(Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class Id008Ptedo2zkContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__contract_id_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkContractIdTag, self._io.read_u1())
            if self.id_008__ptedo2zk__contract_id_tag == Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkContractIdTag.implicit:
                self.implicit = Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_008__ptedo2zk__contract_id_tag == Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkContractIdTag.originated:
                self.originated = Id008Ptedo2zkDelegateBalanceUpdates.Originated(self._io, self, self._root)



    class Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_008__ptedo2zk__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_008__ptedo2zk__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_008__ptedo2zk__operation_metadata__alpha__balance_updates, self._io, u"/types/id_008__ptedo2zk__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_008__ptedo2zk__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_008__ptedo2zk__operation_metadata__alpha__balance_updates)
            _io__raw_id_008__ptedo2zk__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_008__ptedo2zk__operation_metadata__alpha__balance_updates))
            self.id_008__ptedo2zk__operation_metadata__alpha__balance_updates = Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceUpdates(_io__raw_id_008__ptedo2zk__operation_metadata__alpha__balance_updates, self, self._root)


    class Fees(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id008Ptedo2zkOperationMetadataAlphaBalance(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__operation_metadata__alpha__balance_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceTag, self._io.read_u1())
            if self.id_008__ptedo2zk__operation_metadata__alpha__balance_tag == Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceTag.contract:
                self.contract = Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkContractId(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation_metadata__alpha__balance_tag == Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceTag.rewards:
                self.rewards = Id008Ptedo2zkDelegateBalanceUpdates.Rewards(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation_metadata__alpha__balance_tag == Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceTag.fees:
                self.fees = Id008Ptedo2zkDelegateBalanceUpdates.Fees(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation_metadata__alpha__balance_tag == Id008Ptedo2zkDelegateBalanceUpdates.Id008Ptedo2zkOperationMetadataAlphaBalanceTag.deposits:
                self.deposits = Id008Ptedo2zkDelegateBalanceUpdates.Deposits(self._io, self, self._root)



    class Id008Ptedo2zkOperationMetadataAlphaBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id008Ptedo2zkDelegateBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




