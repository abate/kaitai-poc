# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class AlphaSmartRollupGame(KaitaiStruct):
    """Encoding id: alpha.smart_rollup.game."""

    class DalSnapshotTag(Enum):
        dal_skip_list_legacy = 0
        dal_skip_list = 1

    class ContentTag(Enum):
        unattested = 0
        attested = 1

    class Bool(Enum):
        false = 0
        true = 255

    class GameStateTag(Enum):
        dissecting = 0
        final_move = 1

    class AttestedTag(Enum):
        v0 = 0

    class TurnTag(Enum):
        alice = 0
        bob = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.turn = KaitaiStream.resolve_enum(AlphaSmartRollupGame.TurnTag, self._io.read_u1())
        self.inbox_snapshot = AlphaSmartRollupGame.InboxSnapshot(self._io, self, self._root)
        self.dal_snapshot = AlphaSmartRollupGame.DalSnapshot(self._io, self, self._root)
        self.start_level = self._io.read_s4be()
        self.inbox_level = self._io.read_s4be()
        self.game_state = AlphaSmartRollupGame.GameState(self._io, self, self._root)

    class BackPointers0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_back_pointers = self._io.read_u4be()
            if not self.len_back_pointers <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_back_pointers, self._io, u"/types/back_pointers_0/seq/0")
            self._raw_back_pointers = self._io.read_bytes(self.len_back_pointers)
            _io__raw_back_pointers = KaitaiStream(BytesIO(self._raw_back_pointers))
            self.back_pointers = AlphaSmartRollupGame.BackPointers(_io__raw_back_pointers, self, self._root)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = AlphaSmartRollupGame.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Attested(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.attested_tag = KaitaiStream.resolve_enum(AlphaSmartRollupGame.AttestedTag, self._io.read_u1())
            if self.attested_tag == AlphaSmartRollupGame.AttestedTag.v0:
                self.v0 = AlphaSmartRollupGame.V0(self._io, self, self._root)



    class BackPointersEntries0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dal_skip_list_pointer = self._io.read_bytes(32)


    class InboxSnapshot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.index = AlphaSmartRollupGame.N(self._io, self, self._root)
            self.content = AlphaSmartRollupGame.Content(self._io, self, self._root)
            self.back_pointers = AlphaSmartRollupGame.BackPointers0(self._io, self, self._root)


    class BackPointers2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_back_pointers = self._io.read_u4be()
            if not self.len_back_pointers <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_back_pointers, self._io, u"/types/back_pointers_2/seq/0")
            self._raw_back_pointers = self._io.read_bytes(self.len_back_pointers)
            _io__raw_back_pointers = KaitaiStream(BytesIO(self._raw_back_pointers))
            self.back_pointers = AlphaSmartRollupGame.BackPointers1(_io__raw_back_pointers, self, self._root)


    class GameState(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.game_state_tag = KaitaiStream.resolve_enum(AlphaSmartRollupGame.GameStateTag, self._io.read_u1())
            if self.game_state_tag == AlphaSmartRollupGame.GameStateTag.dissecting:
                self.dissecting = AlphaSmartRollupGame.Dissecting(self._io, self, self._root)

            if self.game_state_tag == AlphaSmartRollupGame.GameStateTag.final_move:
                self.final_move = AlphaSmartRollupGame.FinalMove(self._io, self, self._root)



    class AgreedStartChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.state_tag = KaitaiStream.resolve_enum(AlphaSmartRollupGame.Bool, self._io.read_u1())
            if self.state_tag == AlphaSmartRollupGame.Bool.true:
                self.state = self._io.read_bytes(32)

            self.tick = AlphaSmartRollupGame.N(self._io, self, self._root)


    class Dissecting(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dissection = AlphaSmartRollupGame.Dissection0(self._io, self, self._root)
            self.default_number_of_sections = self._io.read_u1()


    class RefutedStopChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.state_tag = KaitaiStream.resolve_enum(AlphaSmartRollupGame.Bool, self._io.read_u1())
            if self.state_tag == AlphaSmartRollupGame.Bool.true:
                self.state = self._io.read_bytes(32)

            self.tick = AlphaSmartRollupGame.N(self._io, self, self._root)


    class DalSnapshot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dal_snapshot_tag = KaitaiStream.resolve_enum(AlphaSmartRollupGame.DalSnapshotTag, self._io.read_u1())
            if self.dal_snapshot_tag == AlphaSmartRollupGame.DalSnapshotTag.dal_skip_list_legacy:
                self.dal_skip_list_legacy = self._io.read_bytes(57)

            if self.dal_snapshot_tag == AlphaSmartRollupGame.DalSnapshotTag.dal_skip_list:
                self.dal_skip_list = AlphaSmartRollupGame.SkipList(self._io, self, self._root)



    class Dissection0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_dissection = self._io.read_u4be()
            if not self.len_dissection <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_dissection, self._io, u"/types/dissection_0/seq/0")
            self._raw_dissection = self._io.read_bytes(self.len_dissection)
            _io__raw_dissection = KaitaiStream(BytesIO(self._raw_dissection))
            self.dissection = AlphaSmartRollupGame.Dissection(_io__raw_dissection, self, self._root)


    class V0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.index = self._io.read_u1()
            self.commitment = self._io.read_bytes(48)


    class Content(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.hash = self._io.read_bytes(32)
            self.level = self._io.read_s4be()


    class BackPointers(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.back_pointers_entries = []
            i = 0
            while not self._io.is_eof():
                self.back_pointers_entries.append(AlphaSmartRollupGame.BackPointersEntries(self._io, self, self._root))
                i += 1



    class BackPointers1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.back_pointers_entries = []
            i = 0
            while not self._io.is_eof():
                self.back_pointers_entries.append(AlphaSmartRollupGame.BackPointersEntries0(self._io, self, self._root))
                i += 1



    class Dissection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dissection_entries = []
            i = 0
            while not self._io.is_eof():
                self.dissection_entries.append(AlphaSmartRollupGame.DissectionEntries(self._io, self, self._root))
                i += 1



    class Content0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.content_tag = KaitaiStream.resolve_enum(AlphaSmartRollupGame.ContentTag, self._io.read_u1())
            if self.content_tag == AlphaSmartRollupGame.ContentTag.unattested:
                self.unattested = AlphaSmartRollupGame.Unattested(self._io, self, self._root)

            if self.content_tag == AlphaSmartRollupGame.ContentTag.attested:
                self.attested = AlphaSmartRollupGame.Attested(self._io, self, self._root)



    class Unattested(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.index = self._io.read_u1()


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class SkipList(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.index = AlphaSmartRollupGame.N(self._io, self, self._root)
            self.content = AlphaSmartRollupGame.Content0(self._io, self, self._root)
            self.back_pointers = AlphaSmartRollupGame.BackPointers2(self._io, self, self._root)


    class BackPointersEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.smart_rollup_inbox_hash = self._io.read_bytes(32)


    class DissectionEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.state_tag = KaitaiStream.resolve_enum(AlphaSmartRollupGame.Bool, self._io.read_u1())
            if self.state_tag == AlphaSmartRollupGame.Bool.true:
                self.state = self._io.read_bytes(32)

            self.tick = AlphaSmartRollupGame.N(self._io, self, self._root)


    class FinalMove(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.agreed_start_chunk = AlphaSmartRollupGame.AgreedStartChunk(self._io, self, self._root)
            self.refuted_stop_chunk = AlphaSmartRollupGame.RefutedStopChunk(self._io, self, self._root)



