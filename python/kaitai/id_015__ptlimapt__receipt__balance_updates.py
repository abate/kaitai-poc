# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id015PtlimaptReceiptBalanceUpdates(KaitaiStruct):
    """Encoding id: 015-PtLimaPt.receipt.balance_updates."""

    class Id015PtlimaptOperationMetadataAlphaBalanceTag(Enum):
        contract = 0
        block_fees = 2
        deposits = 4
        nonce_revelation_rewards = 5
        double_signing_evidence_rewards = 6
        endorsing_rewards = 7
        baking_rewards = 8
        baking_bonuses = 9
        storage_fees = 11
        double_signing_punishments = 12
        lost_endorsing_rewards = 13
        liquidity_baking_subsidies = 14
        burned = 15
        commitments = 16
        bootstrap = 17
        invoice = 18
        initial_commitments = 19
        minted = 20
        frozen_bonds = 21
        tx_rollup_rejection_rewards = 22
        tx_rollup_rejection_punishments = 23
        sc_rollup_refutation_punishments = 24
        sc_rollup_refutation_rewards = 25

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class OriginTag(Enum):
        block_application = 0
        protocol_migration = 1
        subsidy = 2
        simulation = 3

    class Id015PtlimaptContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id015PtlimaptBondIdTag(Enum):
        tx_rollup_bond_id = 0
        sc_rollup_bond_id = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_015__ptlimapt__operation_metadata__alpha__balance_updates = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Id015PtlimaptOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__operation_metadata__alpha__balance = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalance(self._io, self, self._root)
            self.id_015__ptlimapt__operation_metadata__alpha__balance_update = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceUpdate(self._io, self, self._root)
            self.id_015__ptlimapt__operation_metadata__alpha__update_origin = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaUpdateOrigin(self._io, self, self._root)


    class Id015PtlimaptTxRollupId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup_hash = self._io.read_bytes(20)


    class Id015PtlimaptRollupAddress(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__rollup_address = Id015PtlimaptReceiptBalanceUpdates.BytesDynUint30(self._io, self, self._root)


    class FrozenBonds(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptContractId(self._io, self, self._root)
            self.bond_id = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptBondId(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class LostEndorsingRewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id015PtlimaptReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.participation = KaitaiStream.resolve_enum(Id015PtlimaptReceiptBalanceUpdates.Bool, self._io.read_u1())
            self.revelation = KaitaiStream.resolve_enum(Id015PtlimaptReceiptBalanceUpdates.Bool, self._io.read_u1())


    class Id015PtlimaptOperationMetadataAlphaBalance(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__operation_metadata__alpha__balance_tag = KaitaiStream.resolve_enum(Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceTag, self._io.read_u1())
            if self.id_015__ptlimapt__operation_metadata__alpha__balance_tag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceTag.contract:
                self.contract = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptContractId(self._io, self, self._root)

            if self.id_015__ptlimapt__operation_metadata__alpha__balance_tag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceTag.deposits:
                self.deposits = Id015PtlimaptReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_015__ptlimapt__operation_metadata__alpha__balance_tag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceTag.lost_endorsing_rewards:
                self.lost_endorsing_rewards = Id015PtlimaptReceiptBalanceUpdates.LostEndorsingRewards(self._io, self, self._root)

            if self.id_015__ptlimapt__operation_metadata__alpha__balance_tag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceTag.commitments:
                self.commitments = self._io.read_bytes(20)

            if self.id_015__ptlimapt__operation_metadata__alpha__balance_tag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceTag.frozen_bonds:
                self.frozen_bonds = Id015PtlimaptReceiptBalanceUpdates.FrozenBonds(self._io, self, self._root)



    class Id015PtlimaptOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_015__ptlimapt__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_015__ptlimapt__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_015__ptlimapt__operation_metadata__alpha__balance_updates, self._io, u"/types/id_015__ptlimapt__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_015__ptlimapt__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_015__ptlimapt__operation_metadata__alpha__balance_updates)
            _io__raw_id_015__ptlimapt__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_015__ptlimapt__operation_metadata__alpha__balance_updates))
            self.id_015__ptlimapt__operation_metadata__alpha__balance_updates = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceUpdates(_io__raw_id_015__ptlimapt__operation_metadata__alpha__balance_updates, self, self._root)


    class Id015PtlimaptOperationMetadataAlphaUpdateOrigin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.origin = KaitaiStream.resolve_enum(Id015PtlimaptReceiptBalanceUpdates.OriginTag, self._io.read_u1())


    class Id015PtlimaptOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_015__ptlimapt__operation_metadata__alpha__balance_updates_entries.append(Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class Id015PtlimaptContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__contract_id_tag = KaitaiStream.resolve_enum(Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptContractIdTag, self._io.read_u1())
            if self.id_015__ptlimapt__contract_id_tag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptContractIdTag.implicit:
                self.implicit = Id015PtlimaptReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_015__ptlimapt__contract_id_tag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptContractIdTag.originated:
                self.originated = Id015PtlimaptReceiptBalanceUpdates.Originated(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Id015PtlimaptOperationMetadataAlphaBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Id015PtlimaptBondId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__bond_id_tag = KaitaiStream.resolve_enum(Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptBondIdTag, self._io.read_u1())
            if self.id_015__ptlimapt__bond_id_tag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptBondIdTag.tx_rollup_bond_id:
                self.tx_rollup_bond_id = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptTxRollupId(self._io, self, self._root)

            if self.id_015__ptlimapt__bond_id_tag == Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptBondIdTag.sc_rollup_bond_id:
                self.sc_rollup_bond_id = Id015PtlimaptReceiptBalanceUpdates.Id015PtlimaptRollupAddress(self._io, self, self._root)



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id015PtlimaptReceiptBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id015PtlimaptReceiptBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id015PtlimaptReceiptBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id015PtlimaptReceiptBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




