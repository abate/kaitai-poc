# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id011Pthangz2ReceiptBalanceUpdates(KaitaiStruct):
    """Encoding id: 011-PtHangz2.receipt.balance_updates."""

    class Id011Pthangz2ContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id011Pthangz2OperationMetadataAlphaBalanceTag(Enum):
        contract = 0
        rewards = 1
        fees = 2
        deposits = 3

    class OriginTag(Enum):
        block_application = 0
        protocol_migration = 1
        subsidy = 2

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_011__pthangz2__operation_metadata__alpha__balance_updates = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id011Pthangz2ContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__contract_id_tag = KaitaiStream.resolve_enum(Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2ContractIdTag, self._io.read_u1())
            if self.id_011__pthangz2__contract_id_tag == Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2ContractIdTag.implicit:
                self.implicit = Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_011__pthangz2__contract_id_tag == Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2ContractIdTag.originated:
                self.originated = Id011Pthangz2ReceiptBalanceUpdates.Originated(self._io, self, self._root)



    class Rewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id011Pthangz2OperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_011__pthangz2__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_011__pthangz2__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_011__pthangz2__operation_metadata__alpha__balance_updates, self._io, u"/types/id_011__pthangz2__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_011__pthangz2__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_011__pthangz2__operation_metadata__alpha__balance_updates)
            _io__raw_id_011__pthangz2__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_011__pthangz2__operation_metadata__alpha__balance_updates))
            self.id_011__pthangz2__operation_metadata__alpha__balance_updates = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceUpdates(_io__raw_id_011__pthangz2__operation_metadata__alpha__balance_updates, self, self._root)


    class Deposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id011Pthangz2OperationMetadataAlphaBalance(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__operation_metadata__alpha__balance_tag = KaitaiStream.resolve_enum(Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceTag, self._io.read_u1())
            if self.id_011__pthangz2__operation_metadata__alpha__balance_tag == Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceTag.contract:
                self.contract = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2ContractId(self._io, self, self._root)

            if self.id_011__pthangz2__operation_metadata__alpha__balance_tag == Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceTag.rewards:
                self.rewards = Id011Pthangz2ReceiptBalanceUpdates.Rewards(self._io, self, self._root)

            if self.id_011__pthangz2__operation_metadata__alpha__balance_tag == Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceTag.fees:
                self.fees = Id011Pthangz2ReceiptBalanceUpdates.Fees(self._io, self, self._root)

            if self.id_011__pthangz2__operation_metadata__alpha__balance_tag == Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceTag.deposits:
                self.deposits = Id011Pthangz2ReceiptBalanceUpdates.Deposits(self._io, self, self._root)



    class Fees(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id011Pthangz2OperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__operation_metadata__alpha__balance = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalance(self._io, self, self._root)
            self.id_011__pthangz2__operation_metadata__alpha__balance_update = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceUpdate(self._io, self, self._root)
            self.id_011__pthangz2__operation_metadata__alpha__update_origin = Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaUpdateOrigin(self._io, self, self._root)


    class Id011Pthangz2OperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_011__pthangz2__operation_metadata__alpha__balance_updates_entries.append(Id011Pthangz2ReceiptBalanceUpdates.Id011Pthangz2OperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class Id011Pthangz2OperationMetadataAlphaBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Id011Pthangz2OperationMetadataAlphaUpdateOrigin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.origin = KaitaiStream.resolve_enum(Id011Pthangz2ReceiptBalanceUpdates.OriginTag, self._io.read_u1())


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id011Pthangz2ReceiptBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




