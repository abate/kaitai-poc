# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id015PtlimaptConstants(KaitaiStruct):
    """Encoding id: 015-PtLimaPt.constants."""

    class Bool(Enum):
        false = 0
        true = 255

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.proof_of_work_nonce_size = self._io.read_u1()
        self.nonce_length = self._io.read_u1()
        self.max_anon_ops_per_block = self._io.read_u1()
        self.max_operation_data_length = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.max_proposals_per_delegate = self._io.read_u1()
        self.max_micheline_node_count = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.max_micheline_bytes_limit = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.max_allowed_global_constants_depth = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.cache_layout_size = self._io.read_u1()
        self.michelson_maximum_type_size = self._io.read_u2be()
        self.sc_max_wrapped_proof_binary_size = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.sc_rollup_message_size_limit = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.preserved_cycles = self._io.read_u1()
        self.blocks_per_cycle = self._io.read_s4be()
        self.blocks_per_commitment = self._io.read_s4be()
        self.nonce_revelation_threshold = self._io.read_s4be()
        self.blocks_per_stake_snapshot = self._io.read_s4be()
        self.cycles_per_voting_period = self._io.read_s4be()
        self.hard_gas_limit_per_operation = Id015PtlimaptConstants.Z(self._io, self, self._root)
        self.hard_gas_limit_per_block = Id015PtlimaptConstants.Z(self._io, self, self._root)
        self.proof_of_work_threshold = self._io.read_s8be()
        self.minimal_stake = Id015PtlimaptConstants.Id015PtlimaptMutez(self._io, self, self._root)
        self.vdf_difficulty = self._io.read_s8be()
        self.seed_nonce_revelation_tip = Id015PtlimaptConstants.Id015PtlimaptMutez(self._io, self, self._root)
        self.origination_size = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.baking_reward_fixed_portion = Id015PtlimaptConstants.Id015PtlimaptMutez(self._io, self, self._root)
        self.baking_reward_bonus_per_slot = Id015PtlimaptConstants.Id015PtlimaptMutez(self._io, self, self._root)
        self.endorsing_reward_per_slot = Id015PtlimaptConstants.Id015PtlimaptMutez(self._io, self, self._root)
        self.cost_per_byte = Id015PtlimaptConstants.Id015PtlimaptMutez(self._io, self, self._root)
        self.hard_storage_limit_per_operation = Id015PtlimaptConstants.Z(self._io, self, self._root)
        self.quorum_min = self._io.read_s4be()
        self.quorum_max = self._io.read_s4be()
        self.min_proposal_quorum = self._io.read_s4be()
        self.liquidity_baking_subsidy = Id015PtlimaptConstants.Id015PtlimaptMutez(self._io, self, self._root)
        self.liquidity_baking_toggle_ema_threshold = self._io.read_s4be()
        self.max_operations_time_to_live = self._io.read_s2be()
        self.minimal_block_delay = self._io.read_s8be()
        self.delay_increment_per_round = self._io.read_s8be()
        self.consensus_committee_size = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.consensus_threshold = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.minimal_participation_ratio = Id015PtlimaptConstants.MinimalParticipationRatio(self._io, self, self._root)
        self.max_slashing_period = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.frozen_deposits_percentage = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.double_baking_punishment = Id015PtlimaptConstants.Id015PtlimaptMutez(self._io, self, self._root)
        self.ratio_of_frozen_deposits_slashed_per_double_endorsement = Id015PtlimaptConstants.RatioOfFrozenDepositsSlashedPerDoubleEndorsement(self._io, self, self._root)
        self.testnet_dictator_tag = KaitaiStream.resolve_enum(Id015PtlimaptConstants.Bool, self._io.read_u1())
        if self.testnet_dictator_tag == Id015PtlimaptConstants.Bool.true:
            self.testnet_dictator = Id015PtlimaptConstants.PublicKeyHash(self._io, self, self._root)

        self.initial_seed_tag = KaitaiStream.resolve_enum(Id015PtlimaptConstants.Bool, self._io.read_u1())
        if self.initial_seed_tag == Id015PtlimaptConstants.Bool.true:
            self.initial_seed = self._io.read_bytes(32)

        self.cache_script_size = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.cache_stake_distribution_cycles = self._io.read_s1()
        self.cache_sampler_state_cycles = self._io.read_s1()
        self.tx_rollup_enable = KaitaiStream.resolve_enum(Id015PtlimaptConstants.Bool, self._io.read_u1())
        self.tx_rollup_origination_size = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.tx_rollup_hard_size_limit_per_inbox = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.tx_rollup_hard_size_limit_per_message = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.tx_rollup_max_withdrawals_per_batch = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.tx_rollup_commitment_bond = Id015PtlimaptConstants.Id015PtlimaptMutez(self._io, self, self._root)
        self.tx_rollup_finality_period = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.tx_rollup_withdraw_period = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.tx_rollup_max_inboxes_count = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.tx_rollup_max_messages_per_inbox = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.tx_rollup_max_commitments_count = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.tx_rollup_cost_per_byte_ema_factor = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.tx_rollup_max_ticket_payload_size = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.tx_rollup_rejection_max_proof_size = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.tx_rollup_sunset_level = self._io.read_s4be()
        self.dal_parametric = Id015PtlimaptConstants.DalParametric(self._io, self, self._root)
        self.sc_rollup_enable = KaitaiStream.resolve_enum(Id015PtlimaptConstants.Bool, self._io.read_u1())
        self.sc_rollup_origination_size = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.sc_rollup_challenge_window_in_blocks = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.sc_rollup_max_number_of_messages_per_commitment_period = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.sc_rollup_stake_amount = Id015PtlimaptConstants.Id015PtlimaptMutez(self._io, self, self._root)
        self.sc_rollup_commitment_period_in_blocks = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.sc_rollup_max_lookahead_in_blocks = self._io.read_s4be()
        self.sc_rollup_max_active_outbox_levels = self._io.read_s4be()
        self.sc_rollup_max_outbox_messages_per_level = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.sc_rollup_number_of_sections_in_dissection = self._io.read_u1()
        self.sc_rollup_timeout_period_in_blocks = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.sc_rollup_max_number_of_cemented_commitments = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.zk_rollup_enable = KaitaiStream.resolve_enum(Id015PtlimaptConstants.Bool, self._io.read_u1())
        self.zk_rollup_origination_size = Id015PtlimaptConstants.Int31(self._io, self, self._root)
        self.zk_rollup_min_pending_to_process = Id015PtlimaptConstants.Int31(self._io, self, self._root)

    class RatioOfFrozenDepositsSlashedPerDoubleEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class DalParametric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.feature_enable = KaitaiStream.resolve_enum(Id015PtlimaptConstants.Bool, self._io.read_u1())
            self.number_of_slots = self._io.read_s2be()
            self.number_of_shards = self._io.read_s2be()
            self.endorsement_lag = self._io.read_s2be()
            self.availability_threshold = self._io.read_s2be()
            self.slot_size = Id015PtlimaptConstants.Int31(self._io, self, self._root)
            self.redundancy_factor = self._io.read_u1()
            self.page_size = self._io.read_u2be()


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id015PtlimaptConstants.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class MinimalParticipationRatio(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class Id015PtlimaptMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_015__ptlimapt__mutez = Id015PtlimaptConstants.N(self._io, self, self._root)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id015PtlimaptConstants.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id015PtlimaptConstants.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id015PtlimaptConstants.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id015PtlimaptConstants.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id015PtlimaptConstants.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




