# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import block_header__shell
class Id014PtkathmaBlockHeaderUnsigned(KaitaiStruct):
    """Encoding id: 014-PtKathma.block_header.unsigned."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_014__ptkathma__block_header__unsigned = block_header__shell.BlockHeaderShell(self._io)
        self.id_014__ptkathma__block_header__alpha__unsigned_contents = Id014PtkathmaBlockHeaderUnsigned.Id014PtkathmaBlockHeaderAlphaUnsignedContents(self._io, self, self._root)

    class Id014PtkathmaBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_hash = self._io.read_bytes(32)
            self.payload_round = self._io.read_s4be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id014PtkathmaBlockHeaderUnsigned.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id014PtkathmaBlockHeaderUnsigned.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.liquidity_baking_toggle_vote = Id014PtkathmaBlockHeaderUnsigned.Id014PtkathmaLiquidityBakingToggleVote(self._io, self, self._root)


    class Id014PtkathmaLiquidityBakingToggleVote(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_014__ptkathma__liquidity_baking_toggle_vote = self._io.read_s1()



