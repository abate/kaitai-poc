# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id017PtnairobSmartRollupKind(KaitaiStruct):
    """Encoding id: 017-PtNairob.smart_rollup.kind."""

    class Id017PtnairobSmartRollupKind(Enum):
        arith = 0
        wasm_2_0_0 = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_017__ptnairob__smart_rollup__kind = KaitaiStream.resolve_enum(Id017PtnairobSmartRollupKind.Id017PtnairobSmartRollupKind, self._io.read_u1())


