# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id009PsflorenReceiptBalanceUpdates(KaitaiStruct):
    """Encoding id: 009-PsFLoren.receipt.balance_updates."""

    class Id009PsflorenContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id009PsflorenOperationMetadataAlphaBalanceTag(Enum):
        contract = 0
        rewards = 1
        fees = 2
        deposits = 3

    class OriginTag(Enum):
        block_application = 0
        protocol_migration = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_009__psfloren__operation_metadata__alpha__balance_updates = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Id009PsflorenOperationMetadataAlphaUpdateOrigin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.origin = KaitaiStream.resolve_enum(Id009PsflorenReceiptBalanceUpdates.OriginTag, self._io.read_u1())


    class Id009PsflorenContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_009__psfloren__contract_id_tag = KaitaiStream.resolve_enum(Id009PsflorenReceiptBalanceUpdates.Id009PsflorenContractIdTag, self._io.read_u1())
            if self.id_009__psfloren__contract_id_tag == Id009PsflorenReceiptBalanceUpdates.Id009PsflorenContractIdTag.implicit:
                self.implicit = Id009PsflorenReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_009__psfloren__contract_id_tag == Id009PsflorenReceiptBalanceUpdates.Id009PsflorenContractIdTag.originated:
                self.originated = Id009PsflorenReceiptBalanceUpdates.Originated(self._io, self, self._root)



    class Id009PsflorenOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_009__psfloren__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_009__psfloren__operation_metadata__alpha__balance_updates_entries.append(Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id009PsflorenOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_009__psfloren__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_009__psfloren__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_009__psfloren__operation_metadata__alpha__balance_updates, self._io, u"/types/id_009__psfloren__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_009__psfloren__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_009__psfloren__operation_metadata__alpha__balance_updates)
            _io__raw_id_009__psfloren__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_009__psfloren__operation_metadata__alpha__balance_updates))
            self.id_009__psfloren__operation_metadata__alpha__balance_updates = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceUpdates(_io__raw_id_009__psfloren__operation_metadata__alpha__balance_updates, self, self._root)


    class Rewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id009PsflorenReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Deposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id009PsflorenReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id009PsflorenOperationMetadataAlphaBalance(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_009__psfloren__operation_metadata__alpha__balance_tag = KaitaiStream.resolve_enum(Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceTag, self._io.read_u1())
            if self.id_009__psfloren__operation_metadata__alpha__balance_tag == Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceTag.contract:
                self.contract = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenContractId(self._io, self, self._root)

            if self.id_009__psfloren__operation_metadata__alpha__balance_tag == Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceTag.rewards:
                self.rewards = Id009PsflorenReceiptBalanceUpdates.Rewards(self._io, self, self._root)

            if self.id_009__psfloren__operation_metadata__alpha__balance_tag == Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceTag.fees:
                self.fees = Id009PsflorenReceiptBalanceUpdates.Fees(self._io, self, self._root)

            if self.id_009__psfloren__operation_metadata__alpha__balance_tag == Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceTag.deposits:
                self.deposits = Id009PsflorenReceiptBalanceUpdates.Deposits(self._io, self, self._root)



    class Fees(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id009PsflorenReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.cycle = self._io.read_s4be()


    class Id009PsflorenOperationMetadataAlphaBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Id009PsflorenOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_009__psfloren__operation_metadata__alpha__balance = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalance(self._io, self, self._root)
            self.id_009__psfloren__operation_metadata__alpha__balance_update = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaBalanceUpdate(self._io, self, self._root)
            self.id_009__psfloren__operation_metadata__alpha__update_origin = Id009PsflorenReceiptBalanceUpdates.Id009PsflorenOperationMetadataAlphaUpdateOrigin(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id009PsflorenReceiptBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id009PsflorenReceiptBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id009PsflorenReceiptBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id009PsflorenReceiptBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




