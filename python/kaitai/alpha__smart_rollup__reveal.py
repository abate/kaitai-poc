# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class AlphaSmartRollupReveal(KaitaiStruct):
    """Encoding id: alpha.smart_rollup.reveal."""

    class AlphaSmartRollupRevealTag(Enum):
        reveal_raw_data = 0
        reveal_metadata = 1
        request_dal_page = 2
        reveal_dal_parameters = 3

    class InputHashTag(Enum):
        reveal_data_hash_v0 = 0
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.alpha__smart_rollup__reveal_tag = KaitaiStream.resolve_enum(AlphaSmartRollupReveal.AlphaSmartRollupRevealTag, self._io.read_u1())
        if self.alpha__smart_rollup__reveal_tag == AlphaSmartRollupReveal.AlphaSmartRollupRevealTag.reveal_raw_data:
            self.reveal_raw_data = AlphaSmartRollupReveal.InputHash(self._io, self, self._root)

        if self.alpha__smart_rollup__reveal_tag == AlphaSmartRollupReveal.AlphaSmartRollupRevealTag.request_dal_page:
            self.request_dal_page = AlphaSmartRollupReveal.PageId(self._io, self, self._root)


    class InputHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.input_hash_tag = KaitaiStream.resolve_enum(AlphaSmartRollupReveal.InputHashTag, self._io.read_u1())
            if self.input_hash_tag == AlphaSmartRollupReveal.InputHashTag.reveal_data_hash_v0:
                self.reveal_data_hash_v0 = self._io.read_bytes(32)



    class PageId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.published_level = self._io.read_s4be()
            self.slot_index = self._io.read_u1()
            self.page_index = self._io.read_s2be()



