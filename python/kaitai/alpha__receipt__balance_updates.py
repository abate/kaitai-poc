# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class AlphaReceiptBalanceUpdates(KaitaiStruct):
    """Encoding id: alpha.receipt.balance_updates."""

    class AlphaOperationMetadataAlphaUpdateOriginTag(Enum):
        block_application = 0
        protocol_migration = 1
        subsidy = 2
        simulation = 3
        delayed_operation = 4

    class AlphaFrozenStakerTag(Enum):
        single = 0
        shared = 1
        baker = 2
        baker_edge = 3

    class AlphaOperationMetadataAlphaBalanceAndUpdateTag(Enum):
        contract = 0
        block_fees = 2
        deposits = 4
        nonce_revelation_rewards = 5
        attesting_rewards = 7
        baking_rewards = 8
        baking_bonuses = 9
        storage_fees = 11
        double_signing_punishments = 12
        lost_attesting_rewards = 13
        liquidity_baking_subsidies = 14
        burned = 15
        commitments = 16
        bootstrap = 17
        invoice = 18
        initial_commitments = 19
        minted = 20
        frozen_bonds = 21
        smart_rollup_refutation_punishments = 24
        smart_rollup_refutation_rewards = 25
        unstaked_deposits = 26
        staking_delegator_numerator = 27
        staking_delegate_denominator = 28

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class Bool(Enum):
        false = 0
        true = 255

    class AlphaBondIdTag(Enum):
        smart_rollup_bond_id = 1

    class AlphaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class AlphaStakerTag(Enum):
        single = 0
        shared = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.alpha__operation_metadata__alpha__balance_updates = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class AlphaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__contract_id_tag = KaitaiStream.resolve_enum(AlphaReceiptBalanceUpdates.AlphaContractIdTag, self._io.read_u1())
            if self.alpha__contract_id_tag == AlphaReceiptBalanceUpdates.AlphaContractIdTag.implicit:
                self.implicit = AlphaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.alpha__contract_id_tag == AlphaReceiptBalanceUpdates.AlphaContractIdTag.originated:
                self.originated = AlphaReceiptBalanceUpdates.Originated(self._io, self, self._root)



    class Commitments(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.committer = self._io.read_bytes(20)
            self.alpha__operation_metadata__alpha__tez_balance_update = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class AlphaBondId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__bond_id_tag = KaitaiStream.resolve_enum(AlphaReceiptBalanceUpdates.AlphaBondIdTag, self._io.read_u1())
            if self.alpha__bond_id_tag == AlphaReceiptBalanceUpdates.AlphaBondIdTag.smart_rollup_bond_id:
                self.smart_rollup_bond_id = self._io.read_bytes(20)



    class FrozenBonds(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = AlphaReceiptBalanceUpdates.AlphaContractId(self._io, self, self._root)
            self.bond_id = AlphaReceiptBalanceUpdates.AlphaBondId(self._io, self, self._root)
            self.alpha__operation_metadata__alpha__tez_balance_update = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class AlphaFrozenStaker(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__frozen_staker_tag = KaitaiStream.resolve_enum(AlphaReceiptBalanceUpdates.AlphaFrozenStakerTag, self._io.read_u1())
            if self.alpha__frozen_staker_tag == AlphaReceiptBalanceUpdates.AlphaFrozenStakerTag.single:
                self.single = AlphaReceiptBalanceUpdates.Single(self._io, self, self._root)

            if self.alpha__frozen_staker_tag == AlphaReceiptBalanceUpdates.AlphaFrozenStakerTag.shared:
                self.shared = AlphaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.alpha__frozen_staker_tag == AlphaReceiptBalanceUpdates.AlphaFrozenStakerTag.baker:
                self.baker = AlphaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.alpha__frozen_staker_tag == AlphaReceiptBalanceUpdates.AlphaFrozenStakerTag.baker_edge:
                self.baker_edge = AlphaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)



    class AlphaOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_alpha__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_alpha__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_alpha__operation_metadata__alpha__balance_updates, self._io, u"/types/alpha__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_alpha__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_alpha__operation_metadata__alpha__balance_updates)
            _io__raw_alpha__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_alpha__operation_metadata__alpha__balance_updates))
            self.alpha__operation_metadata__alpha__balance_updates = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceUpdates(_io__raw_alpha__operation_metadata__alpha__balance_updates, self, self._root)


    class AlphaOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__operation_metadata__alpha__balance_and_update = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdate(self._io, self, self._root)
            self.alpha__operation_metadata__alpha__update_origin = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaUpdateOrigin(self._io, self, self._root)


    class AlphaOperationMetadataAlphaTezBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class StakingDelegateDenominator(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = AlphaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.alpha__operation_metadata__alpha__staking_abstract_quantity = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaStakingAbstractQuantity(self._io, self, self._root)


    class AlphaOperationMetadataAlphaUpdateOrigin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__operation_metadata__alpha__update_origin_tag = KaitaiStream.resolve_enum(AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaUpdateOriginTag, self._io.read_u1())
            if self.alpha__operation_metadata__alpha__update_origin_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaUpdateOriginTag.delayed_operation:
                self.delayed_operation = self._io.read_bytes(32)



    class Single(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = AlphaReceiptBalanceUpdates.AlphaContractId(self._io, self, self._root)
            self.delegate = AlphaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)


    class Deposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.staker = AlphaReceiptBalanceUpdates.AlphaFrozenStaker(self._io, self, self._root)
            self.alpha__operation_metadata__alpha__tez_balance_update = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class AlphaOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.alpha__operation_metadata__alpha__balance_updates_entries.append(AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class AlphaOperationMetadataAlphaStakingAbstractQuantity(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class LostAttestingRewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = AlphaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.participation = KaitaiStream.resolve_enum(AlphaReceiptBalanceUpdates.Bool, self._io.read_u1())
            self.revelation = KaitaiStream.resolve_enum(AlphaReceiptBalanceUpdates.Bool, self._io.read_u1())
            self.alpha__operation_metadata__alpha__tez_balance_update = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class AlphaStaker(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__staker_tag = KaitaiStream.resolve_enum(AlphaReceiptBalanceUpdates.AlphaStakerTag, self._io.read_u1())
            if self.alpha__staker_tag == AlphaReceiptBalanceUpdates.AlphaStakerTag.single:
                self.single = AlphaReceiptBalanceUpdates.Single(self._io, self, self._root)

            if self.alpha__staker_tag == AlphaReceiptBalanceUpdates.AlphaStakerTag.shared:
                self.shared = AlphaReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)



    class UnstakedDeposits(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.staker = AlphaReceiptBalanceUpdates.AlphaStaker(self._io, self, self._root)
            self.cycle = self._io.read_s4be()
            self.alpha__operation_metadata__alpha__tez_balance_update = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class AlphaOperationMetadataAlphaBalanceAndUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__operation_metadata__alpha__balance_and_update_tag = KaitaiStream.resolve_enum(AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag, self._io.read_u1())
            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.contract:
                self.contract = AlphaReceiptBalanceUpdates.Contract(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.block_fees:
                self.block_fees = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.deposits:
                self.deposits = AlphaReceiptBalanceUpdates.Deposits(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.nonce_revelation_rewards:
                self.nonce_revelation_rewards = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.attesting_rewards:
                self.attesting_rewards = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.baking_rewards:
                self.baking_rewards = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.baking_bonuses:
                self.baking_bonuses = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.storage_fees:
                self.storage_fees = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.double_signing_punishments:
                self.double_signing_punishments = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.lost_attesting_rewards:
                self.lost_attesting_rewards = AlphaReceiptBalanceUpdates.LostAttestingRewards(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.liquidity_baking_subsidies:
                self.liquidity_baking_subsidies = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.burned:
                self.burned = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.commitments:
                self.commitments = AlphaReceiptBalanceUpdates.Commitments(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.bootstrap:
                self.bootstrap = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.invoice:
                self.invoice = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.initial_commitments:
                self.initial_commitments = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.minted:
                self.minted = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.frozen_bonds:
                self.frozen_bonds = AlphaReceiptBalanceUpdates.FrozenBonds(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.smart_rollup_refutation_punishments:
                self.smart_rollup_refutation_punishments = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.smart_rollup_refutation_rewards:
                self.smart_rollup_refutation_rewards = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.unstaked_deposits:
                self.unstaked_deposits = AlphaReceiptBalanceUpdates.UnstakedDeposits(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.staking_delegator_numerator:
                self.staking_delegator_numerator = AlphaReceiptBalanceUpdates.StakingDelegatorNumerator(self._io, self, self._root)

            if self.alpha__operation_metadata__alpha__balance_and_update_tag == AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaBalanceAndUpdateTag.staking_delegate_denominator:
                self.staking_delegate_denominator = AlphaReceiptBalanceUpdates.StakingDelegateDenominator(self._io, self, self._root)



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(AlphaReceiptBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == AlphaReceiptBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaReceiptBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaReceiptBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaReceiptBalanceUpdates.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class Contract(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = AlphaReceiptBalanceUpdates.AlphaContractId(self._io, self, self._root)
            self.alpha__operation_metadata__alpha__tez_balance_update = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaTezBalanceUpdate(self._io, self, self._root)


    class StakingDelegatorNumerator(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegator = AlphaReceiptBalanceUpdates.AlphaContractId(self._io, self, self._root)
            self.alpha__operation_metadata__alpha__staking_abstract_quantity = AlphaReceiptBalanceUpdates.AlphaOperationMetadataAlphaStakingAbstractQuantity(self._io, self, self._root)



