# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class AlphaConstants(KaitaiStruct):
    """Encoding id: alpha.constants."""

    class Bool(Enum):
        false = 0
        true = 255

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.proof_of_work_nonce_size = self._io.read_u1()
        self.nonce_length = self._io.read_u1()
        self.max_anon_ops_per_block = self._io.read_u1()
        self.max_operation_data_length = AlphaConstants.Int31(self._io, self, self._root)
        self.max_proposals_per_delegate = self._io.read_u1()
        self.max_micheline_node_count = AlphaConstants.Int31(self._io, self, self._root)
        self.max_micheline_bytes_limit = AlphaConstants.Int31(self._io, self, self._root)
        self.max_allowed_global_constants_depth = AlphaConstants.Int31(self._io, self, self._root)
        self.cache_layout_size = self._io.read_u1()
        self.michelson_maximum_type_size = self._io.read_u2be()
        self.max_slashing_period = self._io.read_u1()
        self.smart_rollup_max_wrapped_proof_binary_size = AlphaConstants.Int31(self._io, self, self._root)
        self.smart_rollup_message_size_limit = AlphaConstants.Int31(self._io, self, self._root)
        self.smart_rollup_max_number_of_messages_per_level = AlphaConstants.N(self._io, self, self._root)
        self.consensus_rights_delay = self._io.read_u1()
        self.blocks_preservation_cycles = self._io.read_u1()
        self.delegate_parameters_activation_delay = self._io.read_u1()
        self.blocks_per_cycle = self._io.read_s4be()
        self.blocks_per_commitment = self._io.read_s4be()
        self.nonce_revelation_threshold = self._io.read_s4be()
        self.cycles_per_voting_period = self._io.read_s4be()
        self.hard_gas_limit_per_operation = AlphaConstants.Z(self._io, self, self._root)
        self.hard_gas_limit_per_block = AlphaConstants.Z(self._io, self, self._root)
        self.proof_of_work_threshold = self._io.read_s8be()
        self.minimal_stake = AlphaConstants.AlphaMutez(self._io, self, self._root)
        self.minimal_frozen_stake = AlphaConstants.AlphaMutez(self._io, self, self._root)
        self.vdf_difficulty = self._io.read_s8be()
        self.origination_size = AlphaConstants.Int31(self._io, self, self._root)
        self.issuance_weights = AlphaConstants.IssuanceWeights(self._io, self, self._root)
        self.cost_per_byte = AlphaConstants.AlphaMutez(self._io, self, self._root)
        self.hard_storage_limit_per_operation = AlphaConstants.Z(self._io, self, self._root)
        self.quorum_min = self._io.read_s4be()
        self.quorum_max = self._io.read_s4be()
        self.min_proposal_quorum = self._io.read_s4be()
        self.liquidity_baking_subsidy = AlphaConstants.AlphaMutez(self._io, self, self._root)
        self.liquidity_baking_toggle_ema_threshold = self._io.read_s4be()
        self.max_operations_time_to_live = self._io.read_s2be()
        self.minimal_block_delay = self._io.read_s8be()
        self.delay_increment_per_round = self._io.read_s8be()
        self.consensus_committee_size = AlphaConstants.Int31(self._io, self, self._root)
        self.consensus_threshold = AlphaConstants.Int31(self._io, self, self._root)
        self.minimal_participation_ratio = AlphaConstants.MinimalParticipationRatio(self._io, self, self._root)
        self.limit_of_delegation_over_baking = self._io.read_u1()
        self.percentage_of_frozen_deposits_slashed_per_double_baking = self._io.read_u2be()
        self.percentage_of_frozen_deposits_slashed_per_double_attestation = self._io.read_u2be()
        self.max_slashing_per_block = self._io.read_u2be()
        self.max_slashing_threshold = AlphaConstants.Int31(self._io, self, self._root)
        self.testnet_dictator_tag = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())
        if self.testnet_dictator_tag == AlphaConstants.Bool.true:
            self.testnet_dictator = AlphaConstants.PublicKeyHash(self._io, self, self._root)

        self.initial_seed_tag = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())
        if self.initial_seed_tag == AlphaConstants.Bool.true:
            self.initial_seed = self._io.read_bytes(32)

        self.cache_script_size = AlphaConstants.Int31(self._io, self, self._root)
        self.cache_stake_distribution_cycles = self._io.read_s1()
        self.cache_sampler_state_cycles = self._io.read_s1()
        self.dal_parametric = AlphaConstants.DalParametric(self._io, self, self._root)
        self.smart_rollup_arith_pvm_enable = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())
        self.smart_rollup_origination_size = AlphaConstants.Int31(self._io, self, self._root)
        self.smart_rollup_challenge_window_in_blocks = AlphaConstants.Int31(self._io, self, self._root)
        self.smart_rollup_stake_amount = AlphaConstants.AlphaMutez(self._io, self, self._root)
        self.smart_rollup_commitment_period_in_blocks = AlphaConstants.Int31(self._io, self, self._root)
        self.smart_rollup_max_lookahead_in_blocks = self._io.read_s4be()
        self.smart_rollup_max_active_outbox_levels = self._io.read_s4be()
        self.smart_rollup_max_outbox_messages_per_level = AlphaConstants.Int31(self._io, self, self._root)
        self.smart_rollup_number_of_sections_in_dissection = self._io.read_u1()
        self.smart_rollup_timeout_period_in_blocks = AlphaConstants.Int31(self._io, self, self._root)
        self.smart_rollup_max_number_of_cemented_commitments = AlphaConstants.Int31(self._io, self, self._root)
        self.smart_rollup_max_number_of_parallel_games = AlphaConstants.Int31(self._io, self, self._root)
        self.smart_rollup_reveal_activation_level = AlphaConstants.SmartRollupRevealActivationLevel(self._io, self, self._root)
        self.smart_rollup_private_enable = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())
        self.smart_rollup_riscv_pvm_enable = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())
        self.zk_rollup_enable = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())
        self.zk_rollup_origination_size = AlphaConstants.Int31(self._io, self, self._root)
        self.zk_rollup_min_pending_to_process = AlphaConstants.Int31(self._io, self, self._root)
        self.zk_rollup_max_ticket_payload_size = AlphaConstants.Int31(self._io, self, self._root)
        self.global_limit_of_staking_over_baking = self._io.read_u1()
        self.edge_of_staking_over_delegation = self._io.read_u1()
        self.adaptive_issuance_launch_ema_threshold = self._io.read_s4be()
        self.adaptive_rewards_params = AlphaConstants.AdaptiveRewardsParams(self._io, self, self._root)
        self.adaptive_issuance_activation_vote_enable = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())
        self.autostaking_enable = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())
        self.adaptive_issuance_force_activation = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())
        self.ns_enable = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())
        self.direct_ticket_spending_enable = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())

    class AdaptiveRewardsParams(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.issuance_ratio_final_min = AlphaConstants.IssuanceRatioFinalMin(self._io, self, self._root)
            self.issuance_ratio_final_max = AlphaConstants.IssuanceRatioFinalMax(self._io, self, self._root)
            self.issuance_ratio_initial_min = AlphaConstants.IssuanceRatioInitialMin(self._io, self, self._root)
            self.issuance_ratio_initial_max = AlphaConstants.IssuanceRatioInitialMax(self._io, self, self._root)
            self.initial_period = self._io.read_u1()
            self.transition_period = self._io.read_u1()
            self.max_bonus = self._io.read_s8be()
            self.growth_rate = AlphaConstants.GrowthRate(self._io, self, self._root)
            self.center_dz = AlphaConstants.CenterDz(self._io, self, self._root)
            self.radius_dz = AlphaConstants.RadiusDz(self._io, self, self._root)


    class CenterDz(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaConstants.Z(self._io, self, self._root)
            self.denominator = AlphaConstants.Z(self._io, self, self._root)


    class IssuanceWeights(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.base_total_issued_per_minute = AlphaConstants.AlphaMutez(self._io, self, self._root)
            self.baking_reward_fixed_portion_weight = AlphaConstants.Int31(self._io, self, self._root)
            self.baking_reward_bonus_weight = AlphaConstants.Int31(self._io, self, self._root)
            self.attesting_reward_weight = AlphaConstants.Int31(self._io, self, self._root)
            self.seed_nonce_revelation_tip_weight = AlphaConstants.Int31(self._io, self, self._root)
            self.vdf_revelation_tip_weight = AlphaConstants.Int31(self._io, self, self._root)


    class DalParametric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.feature_enable = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())
            self.incentives_enable = KaitaiStream.resolve_enum(AlphaConstants.Bool, self._io.read_u1())
            self.number_of_slots = self._io.read_u2be()
            self.attestation_lag = self._io.read_u1()
            self.attestation_threshold = self._io.read_u1()
            self.redundancy_factor = self._io.read_u1()
            self.page_size = self._io.read_u2be()
            self.slot_size = AlphaConstants.Int31(self._io, self, self._root)
            self.number_of_shards = self._io.read_u2be()


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = AlphaConstants.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class MinimalParticipationRatio(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class GrowthRate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaConstants.Z(self._io, self, self._root)
            self.denominator = AlphaConstants.Z(self._io, self, self._root)


    class SmartRollupRevealActivationLevel(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.raw_data = self._io.read_s4be()
            self.metadata = self._io.read_s4be()
            self.dal_page = self._io.read_s4be()
            self.dal_parameters = self._io.read_s4be()
            self.dal_attested_slots_validity_lag = AlphaConstants.Int31(self._io, self, self._root)


    class IssuanceRatioFinalMin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaConstants.Z(self._io, self, self._root)
            self.denominator = AlphaConstants.Z(self._io, self, self._root)


    class IssuanceRatioInitialMax(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaConstants.Z(self._io, self, self._root)
            self.denominator = AlphaConstants.Z(self._io, self, self._root)


    class IssuanceRatioInitialMin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaConstants.Z(self._io, self, self._root)
            self.denominator = AlphaConstants.Z(self._io, self, self._root)


    class AlphaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__mutez = AlphaConstants.N(self._io, self, self._root)


    class RadiusDz(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaConstants.Z(self._io, self, self._root)
            self.denominator = AlphaConstants.Z(self._io, self, self._root)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(AlphaConstants.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == AlphaConstants.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaConstants.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaConstants.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaConstants.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class IssuanceRatioFinalMax(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = AlphaConstants.Z(self._io, self, self._root)
            self.denominator = AlphaConstants.Z(self._io, self, self._root)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = AlphaConstants.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




