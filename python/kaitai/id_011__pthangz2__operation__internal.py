# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id011Pthangz2OperationInternal(KaitaiStruct):
    """Encoding id: 011-PtHangz2.operation.internal."""

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Id011Pthangz2ContractIdTag(Enum):
        implicit = 0
        originated = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id011Pthangz2OperationAlphaInternalOperationTag(Enum):
        reveal = 0
        transaction = 1
        origination = 2
        delegation = 3
        register_global_constant = 4

    class Id011Pthangz2EntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_011__pthangz2__operation__alpha__internal_operation = Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperation(self._io, self, self._root)

    class Id011Pthangz2OperationAlphaInternalOperation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id011Pthangz2OperationInternal.Id011Pthangz2ContractId(self._io, self, self._root)
            self.nonce = self._io.read_u2be()
            self.id_011__pthangz2__operation__alpha__internal_operation_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperationTag, self._io.read_u1())
            if self.id_011__pthangz2__operation__alpha__internal_operation_tag == Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperationTag.reveal:
                self.reveal = Id011Pthangz2OperationInternal.PublicKey(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__internal_operation_tag == Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperationTag.transaction:
                self.transaction = Id011Pthangz2OperationInternal.Transaction(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__internal_operation_tag == Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperationTag.origination:
                self.origination = Id011Pthangz2OperationInternal.Origination(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__internal_operation_tag == Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperationTag.delegation:
                self.delegation = Id011Pthangz2OperationInternal.Delegation(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__internal_operation_tag == Id011Pthangz2OperationInternal.Id011Pthangz2OperationAlphaInternalOperationTag.register_global_constant:
                self.register_global_constant = Id011Pthangz2OperationInternal.BytesDynUint30(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id011Pthangz2OperationInternal.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id011Pthangz2ContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__contract_id_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationInternal.Id011Pthangz2ContractIdTag, self._io.read_u1())
            if self.id_011__pthangz2__contract_id_tag == Id011Pthangz2OperationInternal.Id011Pthangz2ContractIdTag.implicit:
                self.implicit = Id011Pthangz2OperationInternal.PublicKeyHash(self._io, self, self._root)

            if self.id_011__pthangz2__contract_id_tag == Id011Pthangz2OperationInternal.Id011Pthangz2ContractIdTag.originated:
                self.originated = Id011Pthangz2OperationInternal.Originated(self._io, self, self._root)



    class Id011Pthangz2Mutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__mutez = Id011Pthangz2OperationInternal.N(self._io, self, self._root)


    class Id011Pthangz2Entrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__entrypoint_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationInternal.Id011Pthangz2EntrypointTag, self._io.read_u1())
            if self.id_011__pthangz2__entrypoint_tag == Id011Pthangz2OperationInternal.Id011Pthangz2EntrypointTag.named:
                self.named = Id011Pthangz2OperationInternal.Named0(self._io, self, self._root)



    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationInternal.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id011Pthangz2OperationInternal.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id011Pthangz2OperationInternal.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id011Pthangz2OperationInternal.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id011Pthangz2OperationInternal.Bool.true:
                self.delegate = Id011Pthangz2OperationInternal.PublicKeyHash(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Id011Pthangz2ScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id011Pthangz2OperationInternal.BytesDynUint30(self._io, self, self._root)
            self.storage = Id011Pthangz2OperationInternal.BytesDynUint30(self._io, self, self._root)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.balance = Id011Pthangz2OperationInternal.Id011Pthangz2Mutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id011Pthangz2OperationInternal.Bool.true:
                self.delegate = Id011Pthangz2OperationInternal.PublicKeyHash(self._io, self, self._root)

            self.script = Id011Pthangz2OperationInternal.Id011Pthangz2ScriptedContracts(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id011Pthangz2OperationInternal.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount = Id011Pthangz2OperationInternal.Id011Pthangz2Mutez(self._io, self, self._root)
            self.destination = Id011Pthangz2OperationInternal.Id011Pthangz2ContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationInternal.Bool, self._io.read_u1())
            if self.parameters_tag == Id011Pthangz2OperationInternal.Bool.true:
                self.parameters = Id011Pthangz2OperationInternal.Parameters(self._io, self, self._root)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id011Pthangz2OperationInternal.Id011Pthangz2Entrypoint(self._io, self, self._root)
            self.value = Id011Pthangz2OperationInternal.BytesDynUint30(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationInternal.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id011Pthangz2OperationInternal.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id011Pthangz2OperationInternal.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id011Pthangz2OperationInternal.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




