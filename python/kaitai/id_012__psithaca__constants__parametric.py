# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id012PsithacaConstantsParametric(KaitaiStruct):
    """Encoding id: 012-Psithaca.constants.parametric."""

    class DelegateSelectionTag(Enum):
        random_delegate_selection = 0
        round_robin_over_delegates = 1

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.preserved_cycles = self._io.read_u1()
        self.blocks_per_cycle = self._io.read_s4be()
        self.blocks_per_commitment = self._io.read_s4be()
        self.blocks_per_stake_snapshot = self._io.read_s4be()
        self.blocks_per_voting_period = self._io.read_s4be()
        self.hard_gas_limit_per_operation = Id012PsithacaConstantsParametric.Z(self._io, self, self._root)
        self.hard_gas_limit_per_block = Id012PsithacaConstantsParametric.Z(self._io, self, self._root)
        self.proof_of_work_threshold = self._io.read_s8be()
        self.tokens_per_roll = Id012PsithacaConstantsParametric.Id012PsithacaMutez(self._io, self, self._root)
        self.seed_nonce_revelation_tip = Id012PsithacaConstantsParametric.Id012PsithacaMutez(self._io, self, self._root)
        self.origination_size = Id012PsithacaConstantsParametric.Int31(self._io, self, self._root)
        self.baking_reward_fixed_portion = Id012PsithacaConstantsParametric.Id012PsithacaMutez(self._io, self, self._root)
        self.baking_reward_bonus_per_slot = Id012PsithacaConstantsParametric.Id012PsithacaMutez(self._io, self, self._root)
        self.endorsing_reward_per_slot = Id012PsithacaConstantsParametric.Id012PsithacaMutez(self._io, self, self._root)
        self.cost_per_byte = Id012PsithacaConstantsParametric.Id012PsithacaMutez(self._io, self, self._root)
        self.hard_storage_limit_per_operation = Id012PsithacaConstantsParametric.Z(self._io, self, self._root)
        self.quorum_min = self._io.read_s4be()
        self.quorum_max = self._io.read_s4be()
        self.min_proposal_quorum = self._io.read_s4be()
        self.liquidity_baking_subsidy = Id012PsithacaConstantsParametric.Id012PsithacaMutez(self._io, self, self._root)
        self.liquidity_baking_sunset_level = self._io.read_s4be()
        self.liquidity_baking_escape_ema_threshold = self._io.read_s4be()
        self.max_operations_time_to_live = self._io.read_s2be()
        self.minimal_block_delay = self._io.read_s8be()
        self.delay_increment_per_round = self._io.read_s8be()
        self.consensus_committee_size = Id012PsithacaConstantsParametric.Int31(self._io, self, self._root)
        self.consensus_threshold = Id012PsithacaConstantsParametric.Int31(self._io, self, self._root)
        self.minimal_participation_ratio = Id012PsithacaConstantsParametric.MinimalParticipationRatio(self._io, self, self._root)
        self.max_slashing_period = Id012PsithacaConstantsParametric.Int31(self._io, self, self._root)
        self.frozen_deposits_percentage = Id012PsithacaConstantsParametric.Int31(self._io, self, self._root)
        self.double_baking_punishment = Id012PsithacaConstantsParametric.Id012PsithacaMutez(self._io, self, self._root)
        self.ratio_of_frozen_deposits_slashed_per_double_endorsement = Id012PsithacaConstantsParametric.RatioOfFrozenDepositsSlashedPerDoubleEndorsement(self._io, self, self._root)
        self.delegate_selection = Id012PsithacaConstantsParametric.DelegateSelection(self._io, self, self._root)

    class RatioOfFrozenDepositsSlashedPerDoubleEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id012PsithacaConstantsParametric.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class RoundRobinOverDelegatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_round_robin_over_delegates_elt = self._io.read_u4be()
            if not self.len_round_robin_over_delegates_elt <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_round_robin_over_delegates_elt, self._io, u"/types/round_robin_over_delegates_entries/seq/0")
            self._raw_round_robin_over_delegates_elt = self._io.read_bytes(self.len_round_robin_over_delegates_elt)
            _io__raw_round_robin_over_delegates_elt = KaitaiStream(BytesIO(self._raw_round_robin_over_delegates_elt))
            self.round_robin_over_delegates_elt = Id012PsithacaConstantsParametric.RoundRobinOverDelegatesElt(_io__raw_round_robin_over_delegates_elt, self, self._root)


    class RoundRobinOverDelegates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.round_robin_over_delegates_entries = []
            i = 0
            while not self._io.is_eof():
                self.round_robin_over_delegates_entries.append(Id012PsithacaConstantsParametric.RoundRobinOverDelegatesEntries(self._io, self, self._root))
                i += 1



    class RoundRobinOverDelegatesElt(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.round_robin_over_delegates_elt_entries = []
            i = 0
            while not self._io.is_eof():
                self.round_robin_over_delegates_elt_entries.append(Id012PsithacaConstantsParametric.RoundRobinOverDelegatesEltEntries(self._io, self, self._root))
                i += 1



    class Id012PsithacaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_012__psithaca__mutez = Id012PsithacaConstantsParametric.N(self._io, self, self._root)


    class RoundRobinOverDelegates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_round_robin_over_delegates = self._io.read_u4be()
            if not self.len_round_robin_over_delegates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_round_robin_over_delegates, self._io, u"/types/round_robin_over_delegates_0/seq/0")
            self._raw_round_robin_over_delegates = self._io.read_bytes(self.len_round_robin_over_delegates)
            _io__raw_round_robin_over_delegates = KaitaiStream(BytesIO(self._raw_round_robin_over_delegates))
            self.round_robin_over_delegates = Id012PsithacaConstantsParametric.RoundRobinOverDelegates(_io__raw_round_robin_over_delegates, self, self._root)


    class MinimalParticipationRatio(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.numerator = self._io.read_u2be()
            self.denominator = self._io.read_u2be()


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id012PsithacaConstantsParametric.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id012PsithacaConstantsParametric.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id012PsithacaConstantsParametric.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id012PsithacaConstantsParametric.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class DelegateSelection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_selection_tag = KaitaiStream.resolve_enum(Id012PsithacaConstantsParametric.DelegateSelectionTag, self._io.read_u1())
            if self.delegate_selection_tag == Id012PsithacaConstantsParametric.DelegateSelectionTag.round_robin_over_delegates:
                self.round_robin_over_delegates = Id012PsithacaConstantsParametric.RoundRobinOverDelegates0(self._io, self, self._root)



    class RoundRobinOverDelegatesEltEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.signature__v0__public_key = Id012PsithacaConstantsParametric.PublicKey(self._io, self, self._root)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id012PsithacaConstantsParametric.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




