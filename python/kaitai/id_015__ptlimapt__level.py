# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id015PtlimaptLevel(KaitaiStruct):
    """Encoding id: 015-PtLimaPt.level."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.level = self._io.read_s4be()
        self.level_position = self._io.read_s4be()
        self.cycle = self._io.read_s4be()
        self.cycle_position = self._io.read_s4be()
        self.expected_commitment = KaitaiStream.resolve_enum(Id015PtlimaptLevel.Bool, self._io.read_u1())


