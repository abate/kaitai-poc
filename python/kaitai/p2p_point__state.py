# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class P2pPointState(KaitaiStruct):
    """Encoding id: p2p_point.state
    Description: The state a connection to a peer point can be in: requested (connection open from here), accepted (handshake), running (connection already established), disconnected (no connection)."""

    class P2pPointStateTag(Enum):
        requested = 0
        accepted = 1
        running = 2
        disconnected = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.p2p_point__state_tag = KaitaiStream.resolve_enum(P2pPointState.P2pPointStateTag, self._io.read_u1())
        if self.p2p_point__state_tag == P2pPointState.P2pPointStateTag.accepted:
            self.accepted = self._io.read_bytes(16)

        if self.p2p_point__state_tag == P2pPointState.P2pPointStateTag.running:
            self.running = self._io.read_bytes(16)



