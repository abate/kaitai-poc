# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class AlphaFrozenStaker(KaitaiStruct):
    """Encoding id: alpha.frozen_staker."""

    class AlphaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class AlphaFrozenStakerTag(Enum):
        single = 0
        shared = 1
        baker = 2
        baker_edge = 3

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.alpha__frozen_staker = AlphaFrozenStaker.AlphaFrozenStaker(self._io, self, self._root)

    class AlphaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__contract_id_tag = KaitaiStream.resolve_enum(AlphaFrozenStaker.AlphaContractIdTag, self._io.read_u1())
            if self.alpha__contract_id_tag == AlphaFrozenStaker.AlphaContractIdTag.implicit:
                self.implicit = AlphaFrozenStaker.PublicKeyHash(self._io, self, self._root)

            if self.alpha__contract_id_tag == AlphaFrozenStaker.AlphaContractIdTag.originated:
                self.originated = AlphaFrozenStaker.Originated(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class AlphaFrozenStaker(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__frozen_staker_tag = KaitaiStream.resolve_enum(AlphaFrozenStaker.AlphaFrozenStakerTag, self._io.read_u1())
            if self.alpha__frozen_staker_tag == AlphaFrozenStaker.AlphaFrozenStakerTag.single:
                self.single = AlphaFrozenStaker.Single(self._io, self, self._root)

            if self.alpha__frozen_staker_tag == AlphaFrozenStaker.AlphaFrozenStakerTag.shared:
                self.shared = AlphaFrozenStaker.PublicKeyHash(self._io, self, self._root)

            if self.alpha__frozen_staker_tag == AlphaFrozenStaker.AlphaFrozenStakerTag.baker:
                self.baker = AlphaFrozenStaker.PublicKeyHash(self._io, self, self._root)

            if self.alpha__frozen_staker_tag == AlphaFrozenStaker.AlphaFrozenStakerTag.baker_edge:
                self.baker_edge = AlphaFrozenStaker.PublicKeyHash(self._io, self, self._root)



    class Single(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = AlphaFrozenStaker.AlphaContractId(self._io, self, self._root)
            self.delegate = AlphaFrozenStaker.PublicKeyHash(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(AlphaFrozenStaker.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == AlphaFrozenStaker.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaFrozenStaker.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaFrozenStaker.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaFrozenStaker.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)




