# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id018ProxfordSmartRollupProof(KaitaiStruct):
    """Encoding id: 018-Proxford.smart_rollup.proof."""

    class Bool(Enum):
        false = 0
        true = 255

    class InputProofTag(Enum):
        inbox__proof = 0
        reveal__proof = 1
        first__input = 2

    class RevealProofTag(Enum):
        raw__data__proof = 0
        metadata__proof = 1
        dal__page__proof = 2
        dal__parameters__proof = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.pvm_step = Id018ProxfordSmartRollupProof.BytesDynUint30(self._io, self, self._root)
        self.input_proof_tag = KaitaiStream.resolve_enum(Id018ProxfordSmartRollupProof.Bool, self._io.read_u1())
        if self.input_proof_tag == Id018ProxfordSmartRollupProof.Bool.true:
            self.input_proof = Id018ProxfordSmartRollupProof.InputProof(self._io, self, self._root)


    class DalPageId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.published_level = self._io.read_s4be()
            self.slot_index = self._io.read_u1()
            self.page_index = self._io.read_s2be()


    class DalPageProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dal_page_id = Id018ProxfordSmartRollupProof.DalPageId(self._io, self, self._root)
            self.dal_proof = Id018ProxfordSmartRollupProof.BytesDynUint30(self._io, self, self._root)


    class InputProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.input_proof_tag = KaitaiStream.resolve_enum(Id018ProxfordSmartRollupProof.InputProofTag, self._io.read_u1())
            if self.input_proof_tag == Id018ProxfordSmartRollupProof.InputProofTag.inbox__proof:
                self.inbox__proof = Id018ProxfordSmartRollupProof.InboxProof(self._io, self, self._root)

            if self.input_proof_tag == Id018ProxfordSmartRollupProof.InputProofTag.reveal__proof:
                self.reveal__proof = Id018ProxfordSmartRollupProof.RevealProof(self._io, self, self._root)



    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id018ProxfordSmartRollupProof.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class RevealProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.reveal_proof_tag = KaitaiStream.resolve_enum(Id018ProxfordSmartRollupProof.RevealProofTag, self._io.read_u1())
            if self.reveal_proof_tag == Id018ProxfordSmartRollupProof.RevealProofTag.raw__data__proof:
                self.raw__data__proof = Id018ProxfordSmartRollupProof.RawData0(self._io, self, self._root)

            if self.reveal_proof_tag == Id018ProxfordSmartRollupProof.RevealProofTag.dal__page__proof:
                self.dal__page__proof = Id018ProxfordSmartRollupProof.DalPageProof(self._io, self, self._root)



    class RawData(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.raw_data = self._io.read_bytes_full()


    class InboxProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.message_counter = Id018ProxfordSmartRollupProof.N(self._io, self, self._root)
            self.serialized_proof = Id018ProxfordSmartRollupProof.BytesDynUint30(self._io, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class RawData0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_raw_data = self._io.read_u2be()
            if not self.len_raw_data <= 4096:
                raise kaitaistruct.ValidationGreaterThanError(4096, self.len_raw_data, self._io, u"/types/raw_data_0/seq/0")
            self._raw_raw_data = self._io.read_bytes(self.len_raw_data)
            _io__raw_raw_data = KaitaiStream(BytesIO(self._raw_raw_data))
            self.raw_data = Id018ProxfordSmartRollupProof.RawData(_io__raw_raw_data, self, self._root)



