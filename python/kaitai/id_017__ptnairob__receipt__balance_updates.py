# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id017PtnairobReceiptBalanceUpdates(KaitaiStruct):
    """Encoding id: 017-PtNairob.receipt.balance_updates."""

    class Id017PtnairobContractIdTag(Enum):
        implicit = 0
        originated = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class Id017PtnairobBondIdTag(Enum):
        tx_rollup_bond_id = 0
        smart_rollup_bond_id = 1

    class Bool(Enum):
        false = 0
        true = 255

    class OriginTag(Enum):
        block_application = 0
        protocol_migration = 1
        subsidy = 2
        simulation = 3

    class Id017PtnairobOperationMetadataAlphaBalanceTag(Enum):
        contract = 0
        block_fees = 2
        deposits = 4
        nonce_revelation_rewards = 5
        double_signing_evidence_rewards = 6
        endorsing_rewards = 7
        baking_rewards = 8
        baking_bonuses = 9
        storage_fees = 11
        double_signing_punishments = 12
        lost_endorsing_rewards = 13
        liquidity_baking_subsidies = 14
        burned = 15
        commitments = 16
        bootstrap = 17
        invoice = 18
        initial_commitments = 19
        minted = 20
        frozen_bonds = 21
        tx_rollup_rejection_rewards = 22
        tx_rollup_rejection_punishments = 23
        smart_rollup_refutation_punishments = 24
        smart_rollup_refutation_rewards = 25
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_017__ptnairob__operation_metadata__alpha__balance_updates = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class FrozenBonds(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobContractId(self._io, self, self._root)
            self.bond_id = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobBondId(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class LostEndorsingRewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id017PtnairobReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.participation = KaitaiStream.resolve_enum(Id017PtnairobReceiptBalanceUpdates.Bool, self._io.read_u1())
            self.revelation = KaitaiStream.resolve_enum(Id017PtnairobReceiptBalanceUpdates.Bool, self._io.read_u1())


    class Id017PtnairobOperationMetadataAlphaBalance(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__operation_metadata__alpha__balance_tag = KaitaiStream.resolve_enum(Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceTag, self._io.read_u1())
            if self.id_017__ptnairob__operation_metadata__alpha__balance_tag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceTag.contract:
                self.contract = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobContractId(self._io, self, self._root)

            if self.id_017__ptnairob__operation_metadata__alpha__balance_tag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceTag.deposits:
                self.deposits = Id017PtnairobReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_017__ptnairob__operation_metadata__alpha__balance_tag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceTag.lost_endorsing_rewards:
                self.lost_endorsing_rewards = Id017PtnairobReceiptBalanceUpdates.LostEndorsingRewards(self._io, self, self._root)

            if self.id_017__ptnairob__operation_metadata__alpha__balance_tag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceTag.commitments:
                self.commitments = self._io.read_bytes(20)

            if self.id_017__ptnairob__operation_metadata__alpha__balance_tag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceTag.frozen_bonds:
                self.frozen_bonds = Id017PtnairobReceiptBalanceUpdates.FrozenBonds(self._io, self, self._root)



    class Id017PtnairobOperationMetadataAlphaBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Id017PtnairobTxRollupId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup_hash = self._io.read_bytes(20)


    class Id017PtnairobSmartRollupAddress(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.smart_rollup_hash = self._io.read_bytes(20)


    class Id017PtnairobBondId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__bond_id_tag = KaitaiStream.resolve_enum(Id017PtnairobReceiptBalanceUpdates.Id017PtnairobBondIdTag, self._io.read_u1())
            if self.id_017__ptnairob__bond_id_tag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobBondIdTag.tx_rollup_bond_id:
                self.tx_rollup_bond_id = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobTxRollupId(self._io, self, self._root)

            if self.id_017__ptnairob__bond_id_tag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobBondIdTag.smart_rollup_bond_id:
                self.smart_rollup_bond_id = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobSmartRollupAddress(self._io, self, self._root)



    class Id017PtnairobOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_017__ptnairob__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_017__ptnairob__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_017__ptnairob__operation_metadata__alpha__balance_updates, self._io, u"/types/id_017__ptnairob__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_017__ptnairob__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_017__ptnairob__operation_metadata__alpha__balance_updates)
            _io__raw_id_017__ptnairob__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_017__ptnairob__operation_metadata__alpha__balance_updates))
            self.id_017__ptnairob__operation_metadata__alpha__balance_updates = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceUpdates(_io__raw_id_017__ptnairob__operation_metadata__alpha__balance_updates, self, self._root)


    class Id017PtnairobContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__contract_id_tag = KaitaiStream.resolve_enum(Id017PtnairobReceiptBalanceUpdates.Id017PtnairobContractIdTag, self._io.read_u1())
            if self.id_017__ptnairob__contract_id_tag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobContractIdTag.implicit:
                self.implicit = Id017PtnairobReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_017__ptnairob__contract_id_tag == Id017PtnairobReceiptBalanceUpdates.Id017PtnairobContractIdTag.originated:
                self.originated = Id017PtnairobReceiptBalanceUpdates.Originated(self._io, self, self._root)



    class Id017PtnairobOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_017__ptnairob__operation_metadata__alpha__balance_updates_entries.append(Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1



    class Id017PtnairobOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__operation_metadata__alpha__balance = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalance(self._io, self, self._root)
            self.id_017__ptnairob__operation_metadata__alpha__balance_update = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaBalanceUpdate(self._io, self, self._root)
            self.id_017__ptnairob__operation_metadata__alpha__update_origin = Id017PtnairobReceiptBalanceUpdates.Id017PtnairobOperationMetadataAlphaUpdateOrigin(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id017PtnairobReceiptBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id017PtnairobReceiptBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id017PtnairobReceiptBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id017PtnairobReceiptBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id017PtnairobReceiptBalanceUpdates.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class Id017PtnairobOperationMetadataAlphaUpdateOrigin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.origin = KaitaiStream.resolve_enum(Id017PtnairobReceiptBalanceUpdates.OriginTag, self._io.read_u1())



