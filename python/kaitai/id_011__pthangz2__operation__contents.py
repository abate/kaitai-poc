# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import block_header__shell
from kaitai import operation__shell_header
class Id011Pthangz2OperationContents(KaitaiStruct):
    """Encoding id: 011-PtHangz2.operation.contents."""

    class Id011Pthangz2OperationAlphaContentsTag(Enum):
        endorsement = 0
        seed_nonce_revelation = 1
        double_endorsement_evidence = 2
        double_baking_evidence = 3
        activate_account = 4
        proposals = 5
        ballot = 6
        endorsement_with_slot = 10
        failing_noop = 17
        reveal = 107
        transaction = 108
        origination = 109
        delegation = 110
        register_global_constant = 111

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Id011Pthangz2InlinedEndorsementContentsTag(Enum):
        endorsement = 0

    class Id011Pthangz2ContractIdTag(Enum):
        implicit = 0
        originated = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id011Pthangz2EntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_011__pthangz2__operation__alpha__contents = Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContents(self._io, self, self._root)

    class Id011Pthangz2OperationAlphaContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__operation__alpha__contents_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag, self._io.read_u1())
            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.endorsement:
                self.endorsement = self._io.read_s4be()

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.seed_nonce_revelation:
                self.seed_nonce_revelation = Id011Pthangz2OperationContents.SeedNonceRevelation(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.endorsement_with_slot:
                self.endorsement_with_slot = Id011Pthangz2OperationContents.EndorsementWithSlot(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.double_endorsement_evidence:
                self.double_endorsement_evidence = Id011Pthangz2OperationContents.DoubleEndorsementEvidence(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.double_baking_evidence:
                self.double_baking_evidence = Id011Pthangz2OperationContents.DoubleBakingEvidence(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.activate_account:
                self.activate_account = Id011Pthangz2OperationContents.ActivateAccount(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.proposals:
                self.proposals = Id011Pthangz2OperationContents.Proposals1(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.ballot:
                self.ballot = Id011Pthangz2OperationContents.Ballot(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.reveal:
                self.reveal = Id011Pthangz2OperationContents.Reveal(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.transaction:
                self.transaction = Id011Pthangz2OperationContents.Transaction(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.origination:
                self.origination = Id011Pthangz2OperationContents.Origination(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.delegation:
                self.delegation = Id011Pthangz2OperationContents.Delegation(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.failing_noop:
                self.failing_noop = Id011Pthangz2OperationContents.BytesDynUint30(self._io, self, self._root)

            if self.id_011__pthangz2__operation__alpha__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2OperationAlphaContentsTag.register_global_constant:
                self.register_global_constant = Id011Pthangz2OperationContents.RegisterGlobalConstant(self._io, self, self._root)



    class Op20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_0/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id011Pthangz2OperationContents.Op2(_io__raw_op2, self, self._root)


    class ActivateAccount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = self._io.read_bytes(20)
            self.secret = self._io.read_bytes(20)


    class DoubleEndorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id011Pthangz2OperationContents.Op10(self._io, self, self._root)
            self.op2 = Id011Pthangz2OperationContents.Op20(self._io, self, self._root)
            self.slot = self._io.read_u2be()


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id011Pthangz2OperationContents.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Endorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__inlined__endorsement = Id011Pthangz2OperationContents.Id011Pthangz2InlinedEndorsement(self._io, self, self._root)


    class Proposals0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_proposals = self._io.read_u4be()
            if not self.len_proposals <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_proposals, self._io, u"/types/proposals_0/seq/0")
            self._raw_proposals = self._io.read_bytes(self.len_proposals)
            _io__raw_proposals = KaitaiStream(BytesIO(self._raw_proposals))
            self.proposals = Id011Pthangz2OperationContents.Proposals(_io__raw_proposals, self, self._root)


    class Id011Pthangz2InlinedEndorsementContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__inlined__endorsement__contents_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationContents.Id011Pthangz2InlinedEndorsementContentsTag, self._io.read_u1())
            if self.id_011__pthangz2__inlined__endorsement__contents_tag == Id011Pthangz2OperationContents.Id011Pthangz2InlinedEndorsementContentsTag.endorsement:
                self.endorsement = self._io.read_s4be()



    class Id011Pthangz2ContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__contract_id_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationContents.Id011Pthangz2ContractIdTag, self._io.read_u1())
            if self.id_011__pthangz2__contract_id_tag == Id011Pthangz2OperationContents.Id011Pthangz2ContractIdTag.implicit:
                self.implicit = Id011Pthangz2OperationContents.PublicKeyHash(self._io, self, self._root)

            if self.id_011__pthangz2__contract_id_tag == Id011Pthangz2OperationContents.Id011Pthangz2ContractIdTag.originated:
                self.originated = Id011Pthangz2OperationContents.Originated(self._io, self, self._root)



    class Reveal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id011Pthangz2OperationContents.PublicKeyHash(self._io, self, self._root)
            self.fee = Id011Pthangz2OperationContents.Id011Pthangz2Mutez(self._io, self, self._root)
            self.counter = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.gas_limit = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.storage_limit = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.public_key = Id011Pthangz2OperationContents.PublicKey(self._io, self, self._root)


    class Endorsement0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_endorsement = self._io.read_u4be()
            if not self.len_endorsement <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_endorsement, self._io, u"/types/endorsement_0/seq/0")
            self._raw_endorsement = self._io.read_bytes(self.len_endorsement)
            _io__raw_endorsement = KaitaiStream(BytesIO(self._raw_endorsement))
            self.endorsement = Id011Pthangz2OperationContents.Endorsement(_io__raw_endorsement, self, self._root)


    class Id011Pthangz2BlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__block_header__alpha__unsigned_contents = Id011Pthangz2OperationContents.Id011Pthangz2BlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes(64)


    class Id011Pthangz2Mutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__mutez = Id011Pthangz2OperationContents.N(self._io, self, self._root)


    class SeedNonceRevelation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.nonce = self._io.read_bytes(32)


    class Id011Pthangz2Entrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__entrypoint_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationContents.Id011Pthangz2EntrypointTag, self._io.read_u1())
            if self.id_011__pthangz2__entrypoint_tag == Id011Pthangz2OperationContents.Id011Pthangz2EntrypointTag.named:
                self.named = Id011Pthangz2OperationContents.Named0(self._io, self, self._root)



    class DoubleBakingEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bh1 = Id011Pthangz2OperationContents.Bh10(self._io, self, self._root)
            self.bh2 = Id011Pthangz2OperationContents.Bh20(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationContents.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id011Pthangz2OperationContents.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id011Pthangz2OperationContents.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id011Pthangz2OperationContents.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Bh2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__block_header__alpha__full_header = Id011Pthangz2OperationContents.Id011Pthangz2BlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Bh20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh2 = self._io.read_u4be()
            if not self.len_bh2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh2, self._io, u"/types/bh2_0/seq/0")
            self._raw_bh2 = self._io.read_bytes(self.len_bh2)
            _io__raw_bh2 = KaitaiStream(BytesIO(self._raw_bh2))
            self.bh2 = Id011Pthangz2OperationContents.Bh2(_io__raw_bh2, self, self._root)


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id011Pthangz2OperationContents.PublicKeyHash(self._io, self, self._root)
            self.fee = Id011Pthangz2OperationContents.Id011Pthangz2Mutez(self._io, self, self._root)
            self.counter = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.gas_limit = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.storage_limit = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationContents.Bool, self._io.read_u1())
            if self.delegate_tag == Id011Pthangz2OperationContents.Bool.true:
                self.delegate = Id011Pthangz2OperationContents.PublicKeyHash(self._io, self, self._root)



    class RegisterGlobalConstant(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id011Pthangz2OperationContents.PublicKeyHash(self._io, self, self._root)
            self.fee = Id011Pthangz2OperationContents.Id011Pthangz2Mutez(self._io, self, self._root)
            self.counter = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.gas_limit = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.storage_limit = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.value = Id011Pthangz2OperationContents.BytesDynUint30(self._io, self, self._root)


    class Bh10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh1 = self._io.read_u4be()
            if not self.len_bh1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh1, self._io, u"/types/bh1_0/seq/0")
            self._raw_bh1 = self._io.read_bytes(self.len_bh1)
            _io__raw_bh1 = KaitaiStream(BytesIO(self._raw_bh1))
            self.bh1 = Id011Pthangz2OperationContents.Bh1(_io__raw_bh1, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Bh1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__block_header__alpha__full_header = Id011Pthangz2OperationContents.Id011Pthangz2BlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Op10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_0/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id011Pthangz2OperationContents.Op1(_io__raw_op1, self, self._root)


    class EndorsementWithSlot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.endorsement = Id011Pthangz2OperationContents.Endorsement0(self._io, self, self._root)
            self.slot = self._io.read_u2be()


    class Id011Pthangz2ScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id011Pthangz2OperationContents.BytesDynUint30(self._io, self, self._root)
            self.storage = Id011Pthangz2OperationContents.BytesDynUint30(self._io, self, self._root)


    class Id011Pthangz2BlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_011__pthangz2__block_header__alpha__signed_contents = Id011Pthangz2OperationContents.Id011Pthangz2BlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id011Pthangz2OperationContents.PublicKeyHash(self._io, self, self._root)
            self.fee = Id011Pthangz2OperationContents.Id011Pthangz2Mutez(self._io, self, self._root)
            self.counter = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.gas_limit = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.storage_limit = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.balance = Id011Pthangz2OperationContents.Id011Pthangz2Mutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationContents.Bool, self._io.read_u1())
            if self.delegate_tag == Id011Pthangz2OperationContents.Bool.true:
                self.delegate = Id011Pthangz2OperationContents.PublicKeyHash(self._io, self, self._root)

            self.script = Id011Pthangz2OperationContents.Id011Pthangz2ScriptedContracts(self._io, self, self._root)


    class Op2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__inlined__endorsement = Id011Pthangz2OperationContents.Id011Pthangz2InlinedEndorsement(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id011Pthangz2OperationContents.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id011Pthangz2OperationContents.PublicKeyHash(self._io, self, self._root)
            self.fee = Id011Pthangz2OperationContents.Id011Pthangz2Mutez(self._io, self, self._root)
            self.counter = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.gas_limit = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.storage_limit = Id011Pthangz2OperationContents.N(self._io, self, self._root)
            self.amount = Id011Pthangz2OperationContents.Id011Pthangz2Mutez(self._io, self, self._root)
            self.destination = Id011Pthangz2OperationContents.Id011Pthangz2ContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationContents.Bool, self._io.read_u1())
            if self.parameters_tag == Id011Pthangz2OperationContents.Bool.true:
                self.parameters = Id011Pthangz2OperationContents.Parameters(self._io, self, self._root)



    class Id011Pthangz2InlinedEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__inlined__endorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id011Pthangz2OperationContents.Id011Pthangz2InlinedEndorsementContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationContents.Bool, self._io.read_u1())
            if self.signature_tag == Id011Pthangz2OperationContents.Bool.true:
                self.signature = self._io.read_bytes(64)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id011Pthangz2OperationContents.Id011Pthangz2Entrypoint(self._io, self, self._root)
            self.value = Id011Pthangz2OperationContents.BytesDynUint30(self._io, self, self._root)


    class Id011Pthangz2BlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.priority = self._io.read_u2be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationContents.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id011Pthangz2OperationContents.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.liquidity_baking_escape_vote = KaitaiStream.resolve_enum(Id011Pthangz2OperationContents.Bool, self._io.read_u1())


    class Proposals1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id011Pthangz2OperationContents.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposals = Id011Pthangz2OperationContents.Proposals0(self._io, self, self._root)


    class Ballot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id011Pthangz2OperationContents.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposal = self._io.read_bytes(32)
            self.ballot = self._io.read_s1()


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id011Pthangz2OperationContents.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id011Pthangz2OperationContents.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id011Pthangz2OperationContents.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id011Pthangz2OperationContents.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class Proposals(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proposals_entries = []
            i = 0
            while not self._io.is_eof():
                self.proposals_entries.append(Id011Pthangz2OperationContents.ProposalsEntries(self._io, self, self._root))
                i += 1



    class ProposalsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol_hash = self._io.read_bytes(32)


    class Op1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__inlined__endorsement = Id011Pthangz2OperationContents.Id011Pthangz2InlinedEndorsement(self._io, self, self._root)



