# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id016PtmumbaiVoteListings(KaitaiStruct):
    """Encoding id: 016-PtMumbai.vote.listings."""

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.len_id_016__ptmumbai__vote__listings = self._io.read_u4be()
        if not self.len_id_016__ptmumbai__vote__listings <= 1073741823:
            raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_016__ptmumbai__vote__listings, self._io, u"/seq/0")
        self._raw_id_016__ptmumbai__vote__listings = self._io.read_bytes(self.len_id_016__ptmumbai__vote__listings)
        _io__raw_id_016__ptmumbai__vote__listings = KaitaiStream(BytesIO(self._raw_id_016__ptmumbai__vote__listings))
        self.id_016__ptmumbai__vote__listings = Id016PtmumbaiVoteListings.Id016PtmumbaiVoteListings(_io__raw_id_016__ptmumbai__vote__listings, self, self._root)

    class Id016PtmumbaiVoteListings(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_016__ptmumbai__vote__listings_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_016__ptmumbai__vote__listings_entries.append(Id016PtmumbaiVoteListings.Id016PtmumbaiVoteListingsEntries(self._io, self, self._root))
                i += 1



    class Id016PtmumbaiVoteListingsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = Id016PtmumbaiVoteListings.PublicKeyHash(self._io, self, self._root)
            self.voting_power = self._io.read_s8be()


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id016PtmumbaiVoteListings.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id016PtmumbaiVoteListings.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id016PtmumbaiVoteListings.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id016PtmumbaiVoteListings.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id016PtmumbaiVoteListings.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)




