# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id008Ptedo2zkOperationInternal(KaitaiStruct):
    """Encoding id: 008-PtEdo2Zk.operation.internal."""

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Id008Ptedo2zkContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id008Ptedo2zkOperationAlphaInternalOperationTag(Enum):
        reveal = 0
        transaction = 1
        origination = 2
        delegation = 3

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id008Ptedo2zkEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_008__ptedo2zk__operation__alpha__internal_operation = Id008Ptedo2zkOperationInternal.Id008Ptedo2zkOperationAlphaInternalOperation(self._io, self, self._root)

    class Id008Ptedo2zkScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id008Ptedo2zkOperationInternal.BytesDynUint30(self._io, self, self._root)
            self.storage = Id008Ptedo2zkOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Id008Ptedo2zkMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__mutez = Id008Ptedo2zkOperationInternal.N(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id008Ptedo2zkOperationInternal.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id008Ptedo2zkOperationAlphaInternalOperation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id008Ptedo2zkOperationInternal.Id008Ptedo2zkContractId(self._io, self, self._root)
            self.nonce = self._io.read_u2be()
            self.id_008__ptedo2zk__operation__alpha__internal_operation_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperationInternal.Id008Ptedo2zkOperationAlphaInternalOperationTag, self._io.read_u1())
            if self.id_008__ptedo2zk__operation__alpha__internal_operation_tag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkOperationAlphaInternalOperationTag.reveal:
                self.reveal = Id008Ptedo2zkOperationInternal.PublicKey(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation__alpha__internal_operation_tag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkOperationAlphaInternalOperationTag.transaction:
                self.transaction = Id008Ptedo2zkOperationInternal.Transaction(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation__alpha__internal_operation_tag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkOperationAlphaInternalOperationTag.origination:
                self.origination = Id008Ptedo2zkOperationInternal.Origination(self._io, self, self._root)

            if self.id_008__ptedo2zk__operation__alpha__internal_operation_tag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkOperationAlphaInternalOperationTag.delegation:
                self.delegation = Id008Ptedo2zkOperationInternal.Delegation(self._io, self, self._root)



    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperationInternal.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id008Ptedo2zkOperationInternal.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id008Ptedo2zkOperationInternal.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id008Ptedo2zkOperationInternal.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Id008Ptedo2zkEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__entrypoint_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperationInternal.Id008Ptedo2zkEntrypointTag, self._io.read_u1())
            if self.id_008__ptedo2zk__entrypoint_tag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkEntrypointTag.named:
                self.named = Id008Ptedo2zkOperationInternal.Named0(self._io, self, self._root)



    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id008Ptedo2zkOperationInternal.Bool.true:
                self.delegate = Id008Ptedo2zkOperationInternal.PublicKeyHash(self._io, self, self._root)



    class Id008Ptedo2zkContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_008__ptedo2zk__contract_id_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperationInternal.Id008Ptedo2zkContractIdTag, self._io.read_u1())
            if self.id_008__ptedo2zk__contract_id_tag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkContractIdTag.implicit:
                self.implicit = Id008Ptedo2zkOperationInternal.PublicKeyHash(self._io, self, self._root)

            if self.id_008__ptedo2zk__contract_id_tag == Id008Ptedo2zkOperationInternal.Id008Ptedo2zkContractIdTag.originated:
                self.originated = Id008Ptedo2zkOperationInternal.Originated(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.balance = Id008Ptedo2zkOperationInternal.Id008Ptedo2zkMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id008Ptedo2zkOperationInternal.Bool.true:
                self.delegate = Id008Ptedo2zkOperationInternal.PublicKeyHash(self._io, self, self._root)

            self.script = Id008Ptedo2zkOperationInternal.Id008Ptedo2zkScriptedContracts(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id008Ptedo2zkOperationInternal.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount = Id008Ptedo2zkOperationInternal.Id008Ptedo2zkMutez(self._io, self, self._root)
            self.destination = Id008Ptedo2zkOperationInternal.Id008Ptedo2zkContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperationInternal.Bool, self._io.read_u1())
            if self.parameters_tag == Id008Ptedo2zkOperationInternal.Bool.true:
                self.parameters = Id008Ptedo2zkOperationInternal.Parameters(self._io, self, self._root)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id008Ptedo2zkOperationInternal.Id008Ptedo2zkEntrypoint(self._io, self, self._root)
            self.value = Id008Ptedo2zkOperationInternal.BytesDynUint30(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id008Ptedo2zkOperationInternal.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id008Ptedo2zkOperationInternal.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id008Ptedo2zkOperationInternal.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id008Ptedo2zkOperationInternal.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




