# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id007Psdelph1BlockHeaderContents(KaitaiStruct):
    """Encoding id: 007-PsDELPH1.block_header.contents."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_007__psdelph1__block_header__alpha__unsigned_contents = Id007Psdelph1BlockHeaderContents.Id007Psdelph1BlockHeaderAlphaUnsignedContents(self._io, self, self._root)

    class Id007Psdelph1BlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.priority = self._io.read_u2be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id007Psdelph1BlockHeaderContents.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id007Psdelph1BlockHeaderContents.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)




