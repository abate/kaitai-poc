# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id016PtmumbaiReceiptBalanceUpdates(KaitaiStruct):
    """Encoding id: 016-PtMumbai.receipt.balance_updates."""

    class Id016PtmumbaiBondIdTag(Enum):
        tx_rollup_bond_id = 0
        smart_rollup_bond_id = 1

    class Id016PtmumbaiOperationMetadataAlphaBalanceTag(Enum):
        contract = 0
        block_fees = 2
        deposits = 4
        nonce_revelation_rewards = 5
        double_signing_evidence_rewards = 6
        endorsing_rewards = 7
        baking_rewards = 8
        baking_bonuses = 9
        storage_fees = 11
        double_signing_punishments = 12
        lost_endorsing_rewards = 13
        liquidity_baking_subsidies = 14
        burned = 15
        commitments = 16
        bootstrap = 17
        invoice = 18
        initial_commitments = 19
        minted = 20
        frozen_bonds = 21
        tx_rollup_rejection_rewards = 22
        tx_rollup_rejection_punishments = 23
        smart_rollup_refutation_punishments = 24
        smart_rollup_refutation_rewards = 25

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class Bool(Enum):
        false = 0
        true = 255

    class OriginTag(Enum):
        block_application = 0
        protocol_migration = 1
        subsidy = 2
        simulation = 3

    class Id016PtmumbaiContractIdTag(Enum):
        implicit = 0
        originated = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_016__ptmumbai__operation_metadata__alpha__balance_updates = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceUpdates0(self._io, self, self._root)

    class Id016PtmumbaiSmartRollupAddress(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.smart_rollup_hash = self._io.read_bytes(20)


    class FrozenBonds(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiContractId(self._io, self, self._root)
            self.bond_id = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiBondId(self._io, self, self._root)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class LostEndorsingRewards(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate = Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)
            self.participation = KaitaiStream.resolve_enum(Id016PtmumbaiReceiptBalanceUpdates.Bool, self._io.read_u1())
            self.revelation = KaitaiStream.resolve_enum(Id016PtmumbaiReceiptBalanceUpdates.Bool, self._io.read_u1())


    class Id016PtmumbaiOperationMetadataAlphaBalanceUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.change = self._io.read_s8be()


    class Id016PtmumbaiOperationMetadataAlphaBalance(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_016__ptmumbai__operation_metadata__alpha__balance_tag = KaitaiStream.resolve_enum(Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceTag, self._io.read_u1())
            if self.id_016__ptmumbai__operation_metadata__alpha__balance_tag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceTag.contract:
                self.contract = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiContractId(self._io, self, self._root)

            if self.id_016__ptmumbai__operation_metadata__alpha__balance_tag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceTag.deposits:
                self.deposits = Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_016__ptmumbai__operation_metadata__alpha__balance_tag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceTag.lost_endorsing_rewards:
                self.lost_endorsing_rewards = Id016PtmumbaiReceiptBalanceUpdates.LostEndorsingRewards(self._io, self, self._root)

            if self.id_016__ptmumbai__operation_metadata__alpha__balance_tag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceTag.commitments:
                self.commitments = self._io.read_bytes(20)

            if self.id_016__ptmumbai__operation_metadata__alpha__balance_tag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceTag.frozen_bonds:
                self.frozen_bonds = Id016PtmumbaiReceiptBalanceUpdates.FrozenBonds(self._io, self, self._root)



    class Id016PtmumbaiTxRollupId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup_hash = self._io.read_bytes(20)


    class Id016PtmumbaiBondId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_016__ptmumbai__bond_id_tag = KaitaiStream.resolve_enum(Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiBondIdTag, self._io.read_u1())
            if self.id_016__ptmumbai__bond_id_tag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiBondIdTag.tx_rollup_bond_id:
                self.tx_rollup_bond_id = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiTxRollupId(self._io, self, self._root)

            if self.id_016__ptmumbai__bond_id_tag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiBondIdTag.smart_rollup_bond_id:
                self.smart_rollup_bond_id = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiSmartRollupAddress(self._io, self, self._root)



    class Id016PtmumbaiOperationMetadataAlphaUpdateOrigin(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.origin = KaitaiStream.resolve_enum(Id016PtmumbaiReceiptBalanceUpdates.OriginTag, self._io.read_u1())


    class Id016PtmumbaiOperationMetadataAlphaBalanceUpdatesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_016__ptmumbai__operation_metadata__alpha__balance = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalance(self._io, self, self._root)
            self.id_016__ptmumbai__operation_metadata__alpha__balance_update = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceUpdate(self._io, self, self._root)
            self.id_016__ptmumbai__operation_metadata__alpha__update_origin = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaUpdateOrigin(self._io, self, self._root)


    class Id016PtmumbaiOperationMetadataAlphaBalanceUpdates0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_id_016__ptmumbai__operation_metadata__alpha__balance_updates = self._io.read_u4be()
            if not self.len_id_016__ptmumbai__operation_metadata__alpha__balance_updates <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_016__ptmumbai__operation_metadata__alpha__balance_updates, self._io, u"/types/id_016__ptmumbai__operation_metadata__alpha__balance_updates_0/seq/0")
            self._raw_id_016__ptmumbai__operation_metadata__alpha__balance_updates = self._io.read_bytes(self.len_id_016__ptmumbai__operation_metadata__alpha__balance_updates)
            _io__raw_id_016__ptmumbai__operation_metadata__alpha__balance_updates = KaitaiStream(BytesIO(self._raw_id_016__ptmumbai__operation_metadata__alpha__balance_updates))
            self.id_016__ptmumbai__operation_metadata__alpha__balance_updates = Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceUpdates(_io__raw_id_016__ptmumbai__operation_metadata__alpha__balance_updates, self, self._root)


    class Id016PtmumbaiContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_016__ptmumbai__contract_id_tag = KaitaiStream.resolve_enum(Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiContractIdTag, self._io.read_u1())
            if self.id_016__ptmumbai__contract_id_tag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiContractIdTag.implicit:
                self.implicit = Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHash(self._io, self, self._root)

            if self.id_016__ptmumbai__contract_id_tag == Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiContractIdTag.originated:
                self.originated = Id016PtmumbaiReceiptBalanceUpdates.Originated(self._io, self, self._root)



    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id016PtmumbaiReceiptBalanceUpdates.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class Id016PtmumbaiOperationMetadataAlphaBalanceUpdates(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_016__ptmumbai__operation_metadata__alpha__balance_updates_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_016__ptmumbai__operation_metadata__alpha__balance_updates_entries.append(Id016PtmumbaiReceiptBalanceUpdates.Id016PtmumbaiOperationMetadataAlphaBalanceUpdatesEntries(self._io, self, self._root))
                i += 1




