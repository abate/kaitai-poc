# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import operation__shell_header
from kaitai import block_header__shell
class AlphaOperationWithLegacyAttestationName(KaitaiStruct):
    """Encoding id: alpha.operation_with_legacy_attestation_name."""

    class RevealProofTag(Enum):
        raw__data__proof = 0
        metadata__proof = 1
        dal__page__proof = 2
        dal__parameters__proof = 3

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class MichelineAlphaMichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10

    class PvmKind(Enum):
        arith = 0
        wasm_2_0_0 = 1
        riscv = 2

    class AlphaMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left = 5
        none = 6
        pair_1 = 7
        right = 8
        some_0 = 9
        true = 10
        unit_1 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left_0 = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none_0 = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right_0 = 68
        size = 69
        some = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_0 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction_deprecated = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_deprecated = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140
        chest = 141
        chest_key = 142
        open_chest = 143
        view_0 = 144
        view = 145
        constant = 146
        sub_mutez = 147
        tx_rollup_l2_address = 148
        min_block_time = 149
        sapling_transaction = 150
        emit = 151
        lambda_rec_0 = 152
        lambda_rec = 153
        ticket_0 = 154
        bytes_0 = 155
        nat_0 = 156
        ticket_1 = 157

    class RefutationTag(Enum):
        start = 0
        move = 1

    class AlphaOperationAlphaContentsOrSignaturePrefixTag(Enum):
        seed_nonce_revelation = 1
        double_attestation_evidence = 2
        double_baking_evidence = 3
        activate_account = 4
        proposals = 5
        ballot = 6
        double_preattestation_evidence = 7
        vdf_revelation = 8
        drain_delegate = 9
        failing_noop = 17
        preattestation = 20
        attestation = 21
        attestation_with_dal = 23
        reveal = 107
        transaction = 108
        origination = 109
        delegation = 110
        register_global_constant = 111
        set_deposits_limit = 112
        increase_paid_storage = 113
        update_consensus_key = 114
        transfer_ticket = 158
        smart_rollup_originate = 200
        smart_rollup_add_messages = 201
        smart_rollup_cement = 202
        smart_rollup_publish = 203
        smart_rollup_refute = 204
        smart_rollup_timeout = 205
        smart_rollup_execute_outbox_message = 206
        smart_rollup_recover_bond = 207
        dal_publish_commitment = 230
        zk_rollup_origination = 250
        zk_rollup_publish = 251
        zk_rollup_update = 252
        signature_prefix = 255

    class BlsSignaturePrefixTag(Enum):
        bls_prefix = 3

    class AlphaEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        deposit = 5
        stake = 6
        unstake = 7
        finalize_unstake = 8
        set_delegate_parameters = 9
        named = 255

    class AlphaPerBlockVotesTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__8 = 8
        case__9 = 9
        case__10 = 10

    class OpEltField1Tag(Enum):
        none = 0
        some = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class AlphaContractIdOriginatedTag(Enum):
        originated = 1

    class Bool(Enum):
        false = 0
        true = 255

    class CircuitsInfoEltField1Tag(Enum):
        public = 0
        private = 1
        fee = 2

    class AlphaInlinedPreattestationContentsTag(Enum):
        preattestation = 20

    class AlphaContractIdTag(Enum):
        implicit = 0
        originated = 1

    class AlphaInlinedAttestationMempoolContentsTag(Enum):
        attestation = 21
        attestation_with_dal = 23

    class StepTag(Enum):
        dissection = 0
        proof = 1

    class InputProofTag(Enum):
        inbox__proof = 0
        reveal__proof = 1
        first__input = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.alpha__operation_with_legacy_attestation_name = operation__shell_header.OperationShellHeader(self._io)
        self.alpha__operation_with_legacy_attestation_name__alpha__contents_and_signature = AlphaOperationWithLegacyAttestationName.AlphaOperationWithLegacyAttestationNameAlphaContentsAndSignature(self._io, self, self._root)

    class Op20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_0/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = AlphaOperationWithLegacyAttestationName.Op2(_io__raw_op2, self, self._root)


    class DalPageId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.published_level = self._io.read_s4be()
            self.slot_index = self._io.read_u1()
            self.page_index = self._io.read_s2be()


    class AlphaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__contract_id_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.AlphaContractIdTag, self._io.read_u1())
            if self.alpha__contract_id_tag == AlphaOperationWithLegacyAttestationName.AlphaContractIdTag.implicit:
                self.implicit = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)

            if self.alpha__contract_id_tag == AlphaOperationWithLegacyAttestationName.AlphaContractIdTag.originated:
                self.originated = AlphaOperationWithLegacyAttestationName.Originated(self._io, self, self._root)



    class Whitelist0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_whitelist = self._io.read_u4be()
            if not self.len_whitelist <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_whitelist, self._io, u"/types/whitelist_0/seq/0")
            self._raw_whitelist = self._io.read_bytes(self.len_whitelist)
            _io__raw_whitelist = KaitaiStream(BytesIO(self._raw_whitelist))
            self.whitelist = AlphaOperationWithLegacyAttestationName.Whitelist(_io__raw_whitelist, self, self._root)


    class SmartRollupExecuteOutboxMessage(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)
            self.cemented_commitment = self._io.read_bytes(32)
            self.output_proof = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class SmartRollupOriginate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.pvm_kind = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.PvmKind, self._io.read_u1())
            self.kernel = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)
            self.parameters_ty = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)
            self.whitelist_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.Bool, self._io.read_u1())
            if self.whitelist_tag == AlphaOperationWithLegacyAttestationName.Bool.true:
                self.whitelist = AlphaOperationWithLegacyAttestationName.Whitelist0(self._io, self, self._root)



    class AlphaBlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.alpha__block_header__alpha__signed_contents = AlphaOperationWithLegacyAttestationName.AlphaBlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Stakers(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alice = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.bob = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)


    class DalPageProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dal_page_id = AlphaOperationWithLegacyAttestationName.DalPageId(self._io, self, self._root)
            self.dal_proof = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class IncreasePaidStorage(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.amount = AlphaOperationWithLegacyAttestationName.Z(self._io, self, self._root)
            self.destination = AlphaOperationWithLegacyAttestationName.AlphaContractIdOriginated(self._io, self, self._root)


    class Move(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.choice = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.step = AlphaOperationWithLegacyAttestationName.Step(self._io, self, self._root)


    class InputProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.input_proof_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.InputProofTag, self._io.read_u1())
            if self.input_proof_tag == AlphaOperationWithLegacyAttestationName.InputProofTag.inbox__proof:
                self.inbox__proof = AlphaOperationWithLegacyAttestationName.InboxProof(self._io, self, self._root)

            if self.input_proof_tag == AlphaOperationWithLegacyAttestationName.InputProofTag.reveal__proof:
                self.reveal__proof = AlphaOperationWithLegacyAttestationName.RevealProof(self._io, self, self._root)



    class SmartRollupTimeout(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)
            self.stakers = AlphaOperationWithLegacyAttestationName.Stakers(self._io, self, self._root)


    class ActivateAccount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = self._io.read_bytes(20)
            self.secret = self._io.read_bytes(20)


    class Step(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.step_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.StepTag, self._io.read_u1())
            if self.step_tag == AlphaOperationWithLegacyAttestationName.StepTag.dissection:
                self.dissection = AlphaOperationWithLegacyAttestationName.Dissection0(self._io, self, self._root)

            if self.step_tag == AlphaOperationWithLegacyAttestationName.StepTag.proof:
                self.proof = AlphaOperationWithLegacyAttestationName.Proof(self._io, self, self._root)



    class PendingPisEltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.new_state = AlphaOperationWithLegacyAttestationName.NewState0(self._io, self, self._root)
            self.fee = self._io.read_bytes(32)
            self.exit_validity = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.Bool, self._io.read_u1())


    class NewStateEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.new_state_elt = self._io.read_bytes(32)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = AlphaOperationWithLegacyAttestationName.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class AlphaMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__michelson__v1__primitives = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.AlphaMichelsonV1Primitives, self._io.read_u1())


    class Proposals0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_proposals = self._io.read_u4be()
            if not self.len_proposals <= 640:
                raise kaitaistruct.ValidationGreaterThanError(640, self.len_proposals, self._io, u"/types/proposals_0/seq/0")
            self._raw_proposals = self._io.read_bytes(self.len_proposals)
            _io__raw_proposals = KaitaiStream(BytesIO(self._raw_proposals))
            self.proposals = AlphaOperationWithLegacyAttestationName.Proposals(_io__raw_proposals, self, self._root)


    class TransferTicket(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.ticket_contents = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)
            self.ticket_ty = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)
            self.ticket_ticketer = AlphaOperationWithLegacyAttestationName.AlphaContractId(self._io, self, self._root)
            self.ticket_amount = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.destination = AlphaOperationWithLegacyAttestationName.AlphaContractId(self._io, self, self._root)
            self.entrypoint = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class PendingPis(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pending_pis_entries = []
            i = 0
            while not self._io.is_eof():
                self.pending_pis_entries.append(AlphaOperationWithLegacyAttestationName.PendingPisEntries(self._io, self, self._root))
                i += 1



    class Payload(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_entries = []
            i = 0
            while not self._io.is_eof():
                self.payload_entries.append(AlphaOperationWithLegacyAttestationName.PayloadEntries(self._io, self, self._root))
                i += 1



    class SmartRollupCement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)


    class AlphaInlinedPreattestationContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__inlined__preattestation__contents_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.AlphaInlinedPreattestationContentsTag, self._io.read_u1())
            if self.alpha__inlined__preattestation__contents_tag == AlphaOperationWithLegacyAttestationName.AlphaInlinedPreattestationContentsTag.preattestation:
                self.preattestation = AlphaOperationWithLegacyAttestationName.Preattestation(self._io, self, self._root)



    class CircuitsInfo(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.circuits_info_entries = []
            i = 0
            while not self._io.is_eof():
                self.circuits_info_entries.append(AlphaOperationWithLegacyAttestationName.CircuitsInfoEntries(self._io, self, self._root))
                i += 1



    class Op12(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_2/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = AlphaOperationWithLegacyAttestationName.Op11(_io__raw_op1, self, self._root)


    class PendingPisEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pending_pis_elt_field0 = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)
            self.pending_pis_elt_field1 = AlphaOperationWithLegacyAttestationName.PendingPisEltField1(self._io, self, self._root)


    class SmartRollupRecoverBond(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)
            self.staker = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)


    class NewState0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_new_state = self._io.read_u4be()
            if not self.len_new_state <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_new_state, self._io, u"/types/new_state_0/seq/0")
            self._raw_new_state = self._io.read_bytes(self.len_new_state)
            _io__raw_new_state = KaitaiStream(BytesIO(self._raw_new_state))
            self.new_state = AlphaOperationWithLegacyAttestationName.NewState(_io__raw_new_state, self, self._root)


    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = AlphaOperationWithLegacyAttestationName.AlphaMichelsonV1Primitives(self._io, self, self._root)
            self.arg = AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.annots = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class AlphaOperationWithLegacyAttestationNameAlphaContentsAndSignature(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents_and_signature_prefix = []
            i = 0
            while not self._io.is_eof():
                self.contents_and_signature_prefix.append(AlphaOperationWithLegacyAttestationName.ContentsAndSignaturePrefixEntries(self._io, self, self._root))
                i += 1

            self.signature_suffix = self._io.read_bytes(64)


    class Reveal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.public_key = AlphaOperationWithLegacyAttestationName.PublicKey(self._io, self, self._root)


    class RevealProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.reveal_proof_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.RevealProofTag, self._io.read_u1())
            if self.reveal_proof_tag == AlphaOperationWithLegacyAttestationName.RevealProofTag.raw__data__proof:
                self.raw__data__proof = AlphaOperationWithLegacyAttestationName.RawData0(self._io, self, self._root)

            if self.reveal_proof_tag == AlphaOperationWithLegacyAttestationName.RevealProofTag.dal__page__proof:
                self.dal__page__proof = AlphaOperationWithLegacyAttestationName.DalPageProof(self._io, self, self._root)



    class RawData(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.raw_data = self._io.read_bytes_full()


    class SmartRollupAddMessages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.message = AlphaOperationWithLegacyAttestationName.Message0(self._io, self, self._root)


    class CircuitsInfo0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_circuits_info = self._io.read_u4be()
            if not self.len_circuits_info <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_circuits_info, self._io, u"/types/circuits_info_0/seq/0")
            self._raw_circuits_info = self._io.read_bytes(self.len_circuits_info)
            _io__raw_circuits_info = KaitaiStream(BytesIO(self._raw_circuits_info))
            self.circuits_info = AlphaOperationWithLegacyAttestationName.CircuitsInfo(_io__raw_circuits_info, self, self._root)


    class Price(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id = self._io.read_bytes(32)
            self.amount = AlphaOperationWithLegacyAttestationName.Z(self._io, self, self._root)


    class MessageEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_elt = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class OpEltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_code = AlphaOperationWithLegacyAttestationName.Int31(self._io, self, self._root)
            self.price = AlphaOperationWithLegacyAttestationName.Price(self._io, self, self._root)
            self.l1_dst = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.rollup_id = self._io.read_bytes(20)
            self.payload = AlphaOperationWithLegacyAttestationName.Payload0(self._io, self, self._root)


    class Preattestation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class AlphaContractIdOriginated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__contract_id__originated_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.AlphaContractIdOriginatedTag, self._io.read_u1())
            if self.alpha__contract_id__originated_tag == AlphaOperationWithLegacyAttestationName.AlphaContractIdOriginatedTag.originated:
                self.originated = AlphaOperationWithLegacyAttestationName.Originated(self._io, self, self._root)



    class PrivatePisEltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.new_state = AlphaOperationWithLegacyAttestationName.NewState0(self._io, self, self._root)
            self.fee = self._io.read_bytes(32)


    class Refutation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.refutation_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.RefutationTag, self._io.read_u1())
            if self.refutation_tag == AlphaOperationWithLegacyAttestationName.RefutationTag.start:
                self.start = AlphaOperationWithLegacyAttestationName.Start(self._io, self, self._root)

            if self.refutation_tag == AlphaOperationWithLegacyAttestationName.RefutationTag.move:
                self.move = AlphaOperationWithLegacyAttestationName.Move(self._io, self, self._root)



    class SeedNonceRevelation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.nonce = self._io.read_bytes(32)


    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = AlphaOperationWithLegacyAttestationName.AlphaMichelsonV1Primitives(self._io, self, self._root)
            self.annots = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class WhitelistEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.signature__public_key_hash = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)


    class Attestation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class AlphaOperationAlphaContentsOrSignaturePrefix(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__operation__alpha__contents_or_signature_prefix_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag, self._io.read_u1())
            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.signature_prefix:
                self.signature_prefix = AlphaOperationWithLegacyAttestationName.BlsSignaturePrefix(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.preattestation:
                self.preattestation = AlphaOperationWithLegacyAttestationName.Preattestation(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.attestation:
                self.attestation = AlphaOperationWithLegacyAttestationName.Attestation(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.attestation_with_dal:
                self.attestation_with_dal = AlphaOperationWithLegacyAttestationName.AttestationWithDal(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.double_preattestation_evidence:
                self.double_preattestation_evidence = AlphaOperationWithLegacyAttestationName.DoublePreattestationEvidence(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.double_attestation_evidence:
                self.double_attestation_evidence = AlphaOperationWithLegacyAttestationName.DoubleAttestationEvidence(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.seed_nonce_revelation:
                self.seed_nonce_revelation = AlphaOperationWithLegacyAttestationName.SeedNonceRevelation(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.vdf_revelation:
                self.vdf_revelation = AlphaOperationWithLegacyAttestationName.Solution(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.double_baking_evidence:
                self.double_baking_evidence = AlphaOperationWithLegacyAttestationName.DoubleBakingEvidence(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.activate_account:
                self.activate_account = AlphaOperationWithLegacyAttestationName.ActivateAccount(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.proposals:
                self.proposals = AlphaOperationWithLegacyAttestationName.Proposals1(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.ballot:
                self.ballot = AlphaOperationWithLegacyAttestationName.Ballot(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.reveal:
                self.reveal = AlphaOperationWithLegacyAttestationName.Reveal(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.transaction:
                self.transaction = AlphaOperationWithLegacyAttestationName.Transaction(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.origination:
                self.origination = AlphaOperationWithLegacyAttestationName.Origination(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.delegation:
                self.delegation = AlphaOperationWithLegacyAttestationName.Delegation(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.set_deposits_limit:
                self.set_deposits_limit = AlphaOperationWithLegacyAttestationName.SetDepositsLimit(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.increase_paid_storage:
                self.increase_paid_storage = AlphaOperationWithLegacyAttestationName.IncreasePaidStorage(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.update_consensus_key:
                self.update_consensus_key = AlphaOperationWithLegacyAttestationName.UpdateConsensusKey(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.drain_delegate:
                self.drain_delegate = AlphaOperationWithLegacyAttestationName.DrainDelegate(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.failing_noop:
                self.failing_noop = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.register_global_constant:
                self.register_global_constant = AlphaOperationWithLegacyAttestationName.RegisterGlobalConstant(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.transfer_ticket:
                self.transfer_ticket = AlphaOperationWithLegacyAttestationName.TransferTicket(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.dal_publish_commitment:
                self.dal_publish_commitment = AlphaOperationWithLegacyAttestationName.DalPublishCommitment(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_originate:
                self.smart_rollup_originate = AlphaOperationWithLegacyAttestationName.SmartRollupOriginate(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_add_messages:
                self.smart_rollup_add_messages = AlphaOperationWithLegacyAttestationName.SmartRollupAddMessages(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_cement:
                self.smart_rollup_cement = AlphaOperationWithLegacyAttestationName.SmartRollupCement(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_publish:
                self.smart_rollup_publish = AlphaOperationWithLegacyAttestationName.SmartRollupPublish(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_refute:
                self.smart_rollup_refute = AlphaOperationWithLegacyAttestationName.SmartRollupRefute(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_timeout:
                self.smart_rollup_timeout = AlphaOperationWithLegacyAttestationName.SmartRollupTimeout(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_execute_outbox_message:
                self.smart_rollup_execute_outbox_message = AlphaOperationWithLegacyAttestationName.SmartRollupExecuteOutboxMessage(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_recover_bond:
                self.smart_rollup_recover_bond = AlphaOperationWithLegacyAttestationName.SmartRollupRecoverBond(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.zk_rollup_origination:
                self.zk_rollup_origination = AlphaOperationWithLegacyAttestationName.ZkRollupOrigination(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.zk_rollup_publish:
                self.zk_rollup_publish = AlphaOperationWithLegacyAttestationName.ZkRollupPublish(self._io, self, self._root)

            if self.alpha__operation__alpha__contents_or_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefixTag.zk_rollup_update:
                self.zk_rollup_update = AlphaOperationWithLegacyAttestationName.ZkRollupUpdate(self._io, self, self._root)



    class DoubleBakingEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bh1 = AlphaOperationWithLegacyAttestationName.Bh10(self._io, self, self._root)
            self.bh2 = AlphaOperationWithLegacyAttestationName.Bh20(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == AlphaOperationWithLegacyAttestationName.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == AlphaOperationWithLegacyAttestationName.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == AlphaOperationWithLegacyAttestationName.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)

            if self.public_key_tag == AlphaOperationWithLegacyAttestationName.PublicKeyTag.bls:
                self.bls = self._io.read_bytes(48)



    class Bh2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__block_header__alpha__full_header = AlphaOperationWithLegacyAttestationName.AlphaBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class AttestationWithDal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)
            self.dal_attestation = AlphaOperationWithLegacyAttestationName.Z(self._io, self, self._root)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = AlphaOperationWithLegacyAttestationName.AlphaMichelsonV1Primitives(self._io, self, self._root)
            self.args = AlphaOperationWithLegacyAttestationName.Args0(self._io, self, self._root)
            self.annots = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class SlotHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot_index = self._io.read_u1()
            self.commitment = self._io.read_bytes(48)
            self.commitment_proof = self._io.read_bytes(96)


    class Bh20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh2 = self._io.read_u4be()
            if not self.len_bh2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh2, self._io, u"/types/bh2_0/seq/0")
            self._raw_bh2 = self._io.read_bytes(self.len_bh2)
            _io__raw_bh2 = KaitaiStream(BytesIO(self._raw_bh2))
            self.bh2 = AlphaOperationWithLegacyAttestationName.Bh2(_io__raw_bh2, self, self._root)


    class DalPublishCommitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.slot_header = AlphaOperationWithLegacyAttestationName.SlotHeader(self._io, self, self._root)


    class Commitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.compressed_state = self._io.read_bytes(32)
            self.inbox_level = self._io.read_s4be()
            self.predecessor = self._io.read_bytes(32)
            self.number_of_ticks = self._io.read_s8be()


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.Bool, self._io.read_u1())
            if self.delegate_tag == AlphaOperationWithLegacyAttestationName.Bool.true:
                self.delegate = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)



    class AlphaPerBlockVotes(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__per_block_votes_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.AlphaPerBlockVotesTag, self._io.read_u1())


    class AlphaBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__block_header__alpha__unsigned_contents = AlphaOperationWithLegacyAttestationName.AlphaBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes_full()


    class RegisterGlobalConstant(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.value = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class Solution(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.solution_field0 = self._io.read_bytes(100)
            self.solution_field1 = self._io.read_bytes(100)


    class PayloadEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_elt = self._io.read_bytes(32)


    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = AlphaOperationWithLegacyAttestationName.Sequence(_io__raw_sequence, self, self._root)


    class PendingPis0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_pending_pis = self._io.read_u4be()
            if not self.len_pending_pis <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_pending_pis, self._io, u"/types/pending_pis_0/seq/0")
            self._raw_pending_pis = self._io.read_bytes(self.len_pending_pis)
            _io__raw_pending_pis = KaitaiStream(BytesIO(self._raw_pending_pis))
            self.pending_pis = AlphaOperationWithLegacyAttestationName.PendingPis(_io__raw_pending_pis, self, self._root)


    class DoubleAttestationEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = AlphaOperationWithLegacyAttestationName.Op12(self._io, self, self._root)
            self.op2 = AlphaOperationWithLegacyAttestationName.Op22(self._io, self, self._root)


    class PrivatePis0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_private_pis = self._io.read_u4be()
            if not self.len_private_pis <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_private_pis, self._io, u"/types/private_pis_0/seq/0")
            self._raw_private_pis = self._io.read_bytes(self.len_private_pis)
            _io__raw_private_pis = KaitaiStream(BytesIO(self._raw_private_pis))
            self.private_pis = AlphaOperationWithLegacyAttestationName.PrivatePis(_io__raw_private_pis, self, self._root)


    class MichelineAlphaMichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__alpha__michelson_v1__expression_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__alpha__michelson_v1__expression_tag == AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1ExpressionTag.int:
                self.int = AlphaOperationWithLegacyAttestationName.Z(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1ExpressionTag.string:
                self.string = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1ExpressionTag.sequence:
                self.sequence = AlphaOperationWithLegacyAttestationName.Sequence0(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = AlphaOperationWithLegacyAttestationName.AlphaMichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = AlphaOperationWithLegacyAttestationName.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = AlphaOperationWithLegacyAttestationName.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = AlphaOperationWithLegacyAttestationName.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = AlphaOperationWithLegacyAttestationName.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = AlphaOperationWithLegacyAttestationName.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = AlphaOperationWithLegacyAttestationName.PrimGeneric(self._io, self, self._root)

            if self.micheline__alpha__michelson_v1__expression_tag == AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1ExpressionTag.bytes:
                self.bytes = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)



    class PrivatePisEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.private_pis_elt_field0 = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)
            self.private_pis_elt_field1 = AlphaOperationWithLegacyAttestationName.PrivatePisEltField1(self._io, self, self._root)


    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(AlphaOperationWithLegacyAttestationName.SequenceEntries(self._io, self, self._root))
                i += 1



    class Bh10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh1 = self._io.read_u4be()
            if not self.len_bh1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh1, self._io, u"/types/bh1_0/seq/0")
            self._raw_bh1 = self._io.read_bytes(self.len_bh1)
            _io__raw_bh1 = KaitaiStream(BytesIO(self._raw_bh1))
            self.bh1 = AlphaOperationWithLegacyAttestationName.Bh1(_io__raw_bh1, self, self._root)


    class Op22(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_2/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = AlphaOperationWithLegacyAttestationName.Op21(_io__raw_op2, self, self._root)


    class CircuitsInfoEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.circuits_info_elt_field0 = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)
            self.circuits_info_elt_field1 = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.CircuitsInfoEltField1Tag, self._io.read_u1())


    class Message0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message = self._io.read_u4be()
            if not self.len_message <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message, self._io, u"/types/message_0/seq/0")
            self._raw_message = self._io.read_bytes(self.len_message)
            _io__raw_message = KaitaiStream(BytesIO(self._raw_message))
            self.message = AlphaOperationWithLegacyAttestationName.Message(_io__raw_message, self, self._root)


    class InboxProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.message_counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.serialized_proof = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class OpEltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_elt_field1_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.OpEltField1Tag, self._io.read_u1())
            if self.op_elt_field1_tag == AlphaOperationWithLegacyAttestationName.OpEltField1Tag.some:
                self.some = AlphaOperationWithLegacyAttestationName.Some(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Op0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op = self._io.read_u4be()
            if not self.len_op <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op, self._io, u"/types/op_0/seq/0")
            self._raw_op = self._io.read_bytes(self.len_op)
            _io__raw_op = KaitaiStream(BytesIO(self._raw_op))
            self.op = AlphaOperationWithLegacyAttestationName.Op(_io__raw_op, self, self._root)


    class NewState(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.new_state_entries = []
            i = 0
            while not self._io.is_eof():
                self.new_state_entries.append(AlphaOperationWithLegacyAttestationName.NewStateEntries(self._io, self, self._root))
                i += 1



    class SmartRollupPublish(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)
            self.commitment = AlphaOperationWithLegacyAttestationName.Commitment(self._io, self, self._root)


    class Bh1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__block_header__alpha__full_header = AlphaOperationWithLegacyAttestationName.AlphaBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Payload0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_payload = self._io.read_u4be()
            if not self.len_payload <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_payload, self._io, u"/types/payload_0/seq/0")
            self._raw_payload = self._io.read_bytes(self.len_payload)
            _io__raw_payload = KaitaiStream(BytesIO(self._raw_payload))
            self.payload = AlphaOperationWithLegacyAttestationName.Payload(_io__raw_payload, self, self._root)


    class Dissection0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_dissection = self._io.read_u4be()
            if not self.len_dissection <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_dissection, self._io, u"/types/dissection_0/seq/0")
            self._raw_dissection = self._io.read_bytes(self.len_dissection)
            _io__raw_dissection = KaitaiStream(BytesIO(self._raw_dissection))
            self.dissection = AlphaOperationWithLegacyAttestationName.Dissection(_io__raw_dissection, self, self._root)


    class SetDepositsLimit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.limit_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.Bool, self._io.read_u1())
            if self.limit_tag == AlphaOperationWithLegacyAttestationName.Bool.true:
                self.limit = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)



    class Op10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_0/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = AlphaOperationWithLegacyAttestationName.Op1(_io__raw_op1, self, self._root)


    class AlphaBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_hash = self._io.read_bytes(32)
            self.payload_round = self._io.read_s4be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == AlphaOperationWithLegacyAttestationName.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.per_block_votes = AlphaOperationWithLegacyAttestationName.AlphaPerBlockVotes(self._io, self, self._root)


    class InitState0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_init_state = self._io.read_u4be()
            if not self.len_init_state <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_init_state, self._io, u"/types/init_state_0/seq/0")
            self._raw_init_state = self._io.read_bytes(self.len_init_state)
            _io__raw_init_state = KaitaiStream(BytesIO(self._raw_init_state))
            self.init_state = AlphaOperationWithLegacyAttestationName.InitState(_io__raw_init_state, self, self._root)


    class Op21(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__inlined__attestation = AlphaOperationWithLegacyAttestationName.AlphaInlinedAttestation(self._io, self, self._root)


    class BlsSignaturePrefix(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bls_signature_prefix_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.BlsSignaturePrefixTag, self._io.read_u1())
            if self.bls_signature_prefix_tag == AlphaOperationWithLegacyAttestationName.BlsSignaturePrefixTag.bls_prefix:
                self.bls_prefix = self._io.read_bytes(32)



    class Proof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pvm_step = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)
            self.input_proof_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.Bool, self._io.read_u1())
            if self.input_proof_tag == AlphaOperationWithLegacyAttestationName.Bool.true:
                self.input_proof = AlphaOperationWithLegacyAttestationName.InputProof(self._io, self, self._root)



    class SmartRollupRefute(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)
            self.opponent = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.refutation = AlphaOperationWithLegacyAttestationName.Refutation(self._io, self, self._root)


    class AlphaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__mutez = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.balance = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.Bool, self._io.read_u1())
            if self.delegate_tag == AlphaOperationWithLegacyAttestationName.Bool.true:
                self.delegate = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)

            self.script = AlphaOperationWithLegacyAttestationName.AlphaScriptedContracts(self._io, self, self._root)


    class DrainDelegate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.consensus_key = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.delegate = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.destination = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)


    class ZkRollupUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.zk_rollup = self._io.read_bytes(20)
            self.update = AlphaOperationWithLegacyAttestationName.Update(self._io, self, self._root)


    class Message(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_entries.append(AlphaOperationWithLegacyAttestationName.MessageEntries(self._io, self, self._root))
                i += 1



    class Whitelist(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.whitelist_entries = []
            i = 0
            while not self._io.is_eof():
                self.whitelist_entries.append(AlphaOperationWithLegacyAttestationName.WhitelistEntries(self._io, self, self._root))
                i += 1



    class InitState(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.init_state_entries = []
            i = 0
            while not self._io.is_eof():
                self.init_state_entries.append(AlphaOperationWithLegacyAttestationName.InitStateEntries(self._io, self, self._root))
                i += 1



    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(AlphaOperationWithLegacyAttestationName.ArgsEntries(self._io, self, self._root))
                i += 1



    class ZkRollupOrigination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.public_parameters = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)
            self.circuits_info = AlphaOperationWithLegacyAttestationName.CircuitsInfo0(self._io, self, self._root)
            self.init_state = AlphaOperationWithLegacyAttestationName.InitState0(self._io, self, self._root)
            self.nb_ops = AlphaOperationWithLegacyAttestationName.Int31(self._io, self, self._root)


    class AlphaInlinedAttestationMempoolContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__inlined__attestation_mempool__contents_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.AlphaInlinedAttestationMempoolContentsTag, self._io.read_u1())
            if self.alpha__inlined__attestation_mempool__contents_tag == AlphaOperationWithLegacyAttestationName.AlphaInlinedAttestationMempoolContentsTag.attestation:
                self.attestation = AlphaOperationWithLegacyAttestationName.Attestation(self._io, self, self._root)

            if self.alpha__inlined__attestation_mempool__contents_tag == AlphaOperationWithLegacyAttestationName.AlphaInlinedAttestationMempoolContentsTag.attestation_with_dal:
                self.attestation_with_dal = AlphaOperationWithLegacyAttestationName.AttestationWithDal(self._io, self, self._root)



    class DoublePreattestationEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = AlphaOperationWithLegacyAttestationName.Op10(self._io, self, self._root)
            self.op2 = AlphaOperationWithLegacyAttestationName.Op20(self._io, self, self._root)


    class Op2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__inlined__preattestation = AlphaOperationWithLegacyAttestationName.AlphaInlinedPreattestation(self._io, self, self._root)


    class Dissection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dissection_entries = []
            i = 0
            while not self._io.is_eof():
                self.dissection_entries.append(AlphaOperationWithLegacyAttestationName.DissectionEntries(self._io, self, self._root))
                i += 1



    class PrivatePis(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.private_pis_entries = []
            i = 0
            while not self._io.is_eof():
                self.private_pis_entries.append(AlphaOperationWithLegacyAttestationName.PrivatePisEntries(self._io, self, self._root))
                i += 1



    class AlphaEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__entrypoint_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.AlphaEntrypointTag, self._io.read_u1())
            if self.alpha__entrypoint_tag == AlphaOperationWithLegacyAttestationName.AlphaEntrypointTag.named:
                self.named = AlphaOperationWithLegacyAttestationName.Named0(self._io, self, self._root)



    class UpdateConsensusKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.pk = AlphaOperationWithLegacyAttestationName.PublicKey(self._io, self, self._root)


    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = AlphaOperationWithLegacyAttestationName.AlphaMichelsonV1Primitives(self._io, self, self._root)
            self.arg = AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)


    class ContentsAndSignaturePrefixEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__operation__alpha__contents_or_signature_prefix = AlphaOperationWithLegacyAttestationName.AlphaOperationAlphaContentsOrSignaturePrefix(self._io, self, self._root)


    class Update(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pending_pis = AlphaOperationWithLegacyAttestationName.PendingPis0(self._io, self, self._root)
            self.private_pis = AlphaOperationWithLegacyAttestationName.PrivatePis0(self._io, self, self._root)
            self.fee_pi = AlphaOperationWithLegacyAttestationName.NewState0(self._io, self, self._root)
            self.proof = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class AlphaInlinedPreattestation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__inlined__preattestation = operation__shell_header.OperationShellHeader(self._io)
            self.operations = AlphaOperationWithLegacyAttestationName.AlphaInlinedPreattestationContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.Bool, self._io.read_u1())
            if self.signature_tag == AlphaOperationWithLegacyAttestationName.Bool.true:
                self.signature = self._io.read_bytes_full()



    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = AlphaOperationWithLegacyAttestationName.AlphaMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)


    class AlphaInlinedAttestation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__inlined__attestation = operation__shell_header.OperationShellHeader(self._io)
            self.operations = AlphaOperationWithLegacyAttestationName.AlphaInlinedAttestationMempoolContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.Bool, self._io.read_u1())
            if self.signature_tag == AlphaOperationWithLegacyAttestationName.Bool.true:
                self.signature = self._io.read_bytes_full()



    class Start(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.player_commitment_hash = self._io.read_bytes(32)
            self.opponent_commitment_hash = self._io.read_bytes(32)


    class InitStateEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.init_state_elt = self._io.read_bytes(32)


    class AlphaScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)
            self.storage = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)


    class RawData0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_raw_data = self._io.read_u2be()
            if not self.len_raw_data <= 4096:
                raise kaitaistruct.ValidationGreaterThanError(4096, self.len_raw_data, self._io, u"/types/raw_data_0/seq/0")
            self._raw_raw_data = self._io.read_bytes(self.len_raw_data)
            _io__raw_raw_data = KaitaiStream(BytesIO(self._raw_raw_data))
            self.raw_data = AlphaOperationWithLegacyAttestationName.RawData(_io__raw_raw_data, self, self._root)


    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = AlphaOperationWithLegacyAttestationName.Args(_io__raw_args, self, self._root)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = AlphaOperationWithLegacyAttestationName.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.amount = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.destination = AlphaOperationWithLegacyAttestationName.AlphaContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.Bool, self._io.read_u1())
            if self.parameters_tag == AlphaOperationWithLegacyAttestationName.Bool.true:
                self.parameters = AlphaOperationWithLegacyAttestationName.Parameters(self._io, self, self._root)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = AlphaOperationWithLegacyAttestationName.AlphaEntrypoint(self._io, self, self._root)
            self.value = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class Some(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents = AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.ty = AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.ticketer = AlphaOperationWithLegacyAttestationName.AlphaContractId(self._io, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = AlphaOperationWithLegacyAttestationName.AlphaMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = AlphaOperationWithLegacyAttestationName.MichelineAlphaMichelsonV1Expression(self._io, self, self._root)
            self.annots = AlphaOperationWithLegacyAttestationName.BytesDynUint30(self._io, self, self._root)


    class Proposals1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposals = AlphaOperationWithLegacyAttestationName.Proposals0(self._io, self, self._root)


    class DissectionEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.state_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.Bool, self._io.read_u1())
            if self.state_tag == AlphaOperationWithLegacyAttestationName.Bool.true:
                self.state = self._io.read_bytes(32)

            self.tick = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)


    class Ballot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposal = self._io.read_bytes(32)
            self.ballot = self._io.read_s1()


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(AlphaOperationWithLegacyAttestationName.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == AlphaOperationWithLegacyAttestationName.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaOperationWithLegacyAttestationName.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaOperationWithLegacyAttestationName.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == AlphaOperationWithLegacyAttestationName.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class Op11(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__inlined__attestation = AlphaOperationWithLegacyAttestationName.AlphaInlinedAttestation(self._io, self, self._root)


    class Proposals(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proposals_entries = []
            i = 0
            while not self._io.is_eof():
                self.proposals_entries.append(AlphaOperationWithLegacyAttestationName.ProposalsEntries(self._io, self, self._root))
                i += 1



    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = AlphaOperationWithLegacyAttestationName.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1



    class OpEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_elt_field0 = AlphaOperationWithLegacyAttestationName.OpEltField0(self._io, self, self._root)
            self.op_elt_field1 = AlphaOperationWithLegacyAttestationName.OpEltField1(self._io, self, self._root)


    class ProposalsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol_hash = self._io.read_bytes(32)


    class Op1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__inlined__preattestation = AlphaOperationWithLegacyAttestationName.AlphaInlinedPreattestation(self._io, self, self._root)


    class ZkRollupPublish(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = AlphaOperationWithLegacyAttestationName.PublicKeyHash(self._io, self, self._root)
            self.fee = AlphaOperationWithLegacyAttestationName.AlphaMutez(self._io, self, self._root)
            self.counter = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.gas_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.storage_limit = AlphaOperationWithLegacyAttestationName.N(self._io, self, self._root)
            self.zk_rollup = self._io.read_bytes(20)
            self.op = AlphaOperationWithLegacyAttestationName.Op0(self._io, self, self._root)


    class Op(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_entries = []
            i = 0
            while not self._io.is_eof():
                self.op_entries.append(AlphaOperationWithLegacyAttestationName.OpEntries(self._io, self, self._root))
                i += 1




