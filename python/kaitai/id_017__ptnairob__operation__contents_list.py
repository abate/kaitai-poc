# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import operation__shell_header
from kaitai import block_header__shell
class Id017PtnairobOperationContentsList(KaitaiStruct):
    """Encoding id: 017-PtNairob.operation.contents_list."""

    class Id017PtnairobEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        deposit = 5
        named = 255

    class RevealProofTag(Enum):
        raw__data__proof = 0
        metadata__proof = 1
        dal__page__proof = 2

    class Id017PtnairobMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left = 5
        none_0 = 6
        pair_1 = 7
        right = 8
        some_0 = 9
        true = 10
        unit_0 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left_0 = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right_0 = 68
        size = 69
        some = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_1 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction_deprecated = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_deprecated = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140
        chest = 141
        chest_key = 142
        open_chest = 143
        view_0 = 144
        view = 145
        constant = 146
        sub_mutez = 147
        tx_rollup_l2_address = 148
        min_block_time = 149
        sapling_transaction = 150
        emit = 151
        lambda_rec = 152
        lambda_rec_0 = 153
        ticket_0 = 154
        bytes_0 = 155
        nat_0 = 156

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class PvmKind(Enum):
        arith = 0
        wasm_2_0_0 = 1

    class RefutationTag(Enum):
        start = 0
        move = 1

    class Id017PtnairobInlinedPreendorsementContentsTag(Enum):
        preendorsement = 20

    class Micheline017PtnairobMichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10

    class Id017PtnairobContractIdTag(Enum):
        implicit = 0
        originated = 1

    class OpEltField1Tag(Enum):
        none = 0
        some = 1

    class Id017PtnairobOperationAlphaContentsTag(Enum):
        seed_nonce_revelation = 1
        double_endorsement_evidence = 2
        double_baking_evidence = 3
        activate_account = 4
        proposals = 5
        ballot = 6
        double_preendorsement_evidence = 7
        vdf_revelation = 8
        drain_delegate = 9
        failing_noop = 17
        preendorsement = 20
        endorsement = 21
        dal_attestation = 22
        reveal = 107
        transaction = 108
        origination = 109
        delegation = 110
        register_global_constant = 111
        set_deposits_limit = 112
        increase_paid_storage = 113
        update_consensus_key = 114
        transfer_ticket = 158
        smart_rollup_originate = 200
        smart_rollup_add_messages = 201
        smart_rollup_cement = 202
        smart_rollup_publish = 203
        smart_rollup_refute = 204
        smart_rollup_timeout = 205
        smart_rollup_execute_outbox_message = 206
        smart_rollup_recover_bond = 207
        dal_publish_slot_header = 230
        zk_rollup_origination = 250
        zk_rollup_publish = 251
        zk_rollup_update = 252

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class Bool(Enum):
        false = 0
        true = 255

    class CircuitsInfoEltField1Tag(Enum):
        public = 0
        private = 1
        fee = 2

    class Id017PtnairobInlinedEndorsementMempoolContentsTag(Enum):
        endorsement = 21

    class Id017PtnairobContractIdOriginatedTag(Enum):
        originated = 1

    class StepTag(Enum):
        dissection = 0
        proof = 1

    class InputProofTag(Enum):
        inbox__proof = 0
        reveal__proof = 1
        first__input = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_017__ptnairob__operation__contents_list_entries = []
        i = 0
        while not self._io.is_eof():
            self.id_017__ptnairob__operation__contents_list_entries.append(Id017PtnairobOperationContentsList.Id017PtnairobOperationContentsListEntries(self._io, self, self._root))
            i += 1


    class Op20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_0/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id017PtnairobOperationContentsList.Op2(_io__raw_op2, self, self._root)


    class DalPageId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.published_level = self._io.read_s4be()
            self.slot_index = self._io.read_u1()
            self.page_index = self._io.read_s2be()


    class SmartRollupExecuteOutboxMessage(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.rollup = Id017PtnairobOperationContentsList.Id017PtnairobSmartRollupAddress(self._io, self, self._root)
            self.cemented_commitment = self._io.read_bytes(32)
            self.output_proof = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class SmartRollupOriginate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.pvm_kind = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.PvmKind, self._io.read_u1())
            self.kernel = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)
            self.origination_proof = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)
            self.parameters_ty = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class Stakers(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alice = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.bob = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)


    class DalPageProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dal_page_id = Id017PtnairobOperationContentsList.DalPageId(self._io, self, self._root)
            self.dal_proof = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class IncreasePaidStorage(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.amount = Id017PtnairobOperationContentsList.Z(self._io, self, self._root)
            self.destination = Id017PtnairobOperationContentsList.Id017PtnairobContractIdOriginated(self._io, self, self._root)


    class Id017PtnairobMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__michelson__v1__primitives = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Id017PtnairobMichelsonV1Primitives, self._io.read_u1())


    class Move(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.choice = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.step = Id017PtnairobOperationContentsList.Step(self._io, self, self._root)


    class InputProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.input_proof_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.InputProofTag, self._io.read_u1())
            if self.input_proof_tag == Id017PtnairobOperationContentsList.InputProofTag.inbox__proof:
                self.inbox__proof = Id017PtnairobOperationContentsList.InboxProof(self._io, self, self._root)

            if self.input_proof_tag == Id017PtnairobOperationContentsList.InputProofTag.reveal__proof:
                self.reveal__proof = Id017PtnairobOperationContentsList.RevealProof(self._io, self, self._root)



    class SmartRollupTimeout(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.rollup = Id017PtnairobOperationContentsList.Id017PtnairobSmartRollupAddress(self._io, self, self._root)
            self.stakers = Id017PtnairobOperationContentsList.Stakers(self._io, self, self._root)


    class ActivateAccount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = self._io.read_bytes(20)
            self.secret = self._io.read_bytes(20)


    class DoubleEndorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id017PtnairobOperationContentsList.Op10(self._io, self, self._root)
            self.op2 = Id017PtnairobOperationContentsList.Op20(self._io, self, self._root)


    class Step(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.step_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.StepTag, self._io.read_u1())
            if self.step_tag == Id017PtnairobOperationContentsList.StepTag.dissection:
                self.dissection = Id017PtnairobOperationContentsList.Dissection0(self._io, self, self._root)

            if self.step_tag == Id017PtnairobOperationContentsList.StepTag.proof:
                self.proof = Id017PtnairobOperationContentsList.Proof(self._io, self, self._root)



    class PendingPisEltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.new_state = Id017PtnairobOperationContentsList.NewState0(self._io, self, self._root)
            self.fee = self._io.read_bytes(32)
            self.exit_validity = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Bool, self._io.read_u1())


    class Id017PtnairobInlinedPreendorsementContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__inlined__preendorsement__contents_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Id017PtnairobInlinedPreendorsementContentsTag, self._io.read_u1())
            if self.id_017__ptnairob__inlined__preendorsement__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobInlinedPreendorsementContentsTag.preendorsement:
                self.preendorsement = Id017PtnairobOperationContentsList.Preendorsement(self._io, self, self._root)



    class NewStateEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.new_state_elt = self._io.read_bytes(32)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id017PtnairobOperationContentsList.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Endorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class Proposals0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_proposals = self._io.read_u4be()
            if not self.len_proposals <= 640:
                raise kaitaistruct.ValidationGreaterThanError(640, self.len_proposals, self._io, u"/types/proposals_0/seq/0")
            self._raw_proposals = self._io.read_bytes(self.len_proposals)
            _io__raw_proposals = KaitaiStream(BytesIO(self._raw_proposals))
            self.proposals = Id017PtnairobOperationContentsList.Proposals(_io__raw_proposals, self, self._root)


    class TransferTicket(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.ticket_contents = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)
            self.ticket_ty = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)
            self.ticket_ticketer = Id017PtnairobOperationContentsList.Id017PtnairobContractId(self._io, self, self._root)
            self.ticket_amount = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.destination = Id017PtnairobOperationContentsList.Id017PtnairobContractId(self._io, self, self._root)
            self.entrypoint = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class PendingPis(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pending_pis_entries = []
            i = 0
            while not self._io.is_eof():
                self.pending_pis_entries.append(Id017PtnairobOperationContentsList.PendingPisEntries(self._io, self, self._root))
                i += 1



    class Payload(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_entries = []
            i = 0
            while not self._io.is_eof():
                self.payload_entries.append(Id017PtnairobOperationContentsList.PayloadEntries(self._io, self, self._root))
                i += 1



    class Id017PtnairobContractIdOriginated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__contract_id__originated_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Id017PtnairobContractIdOriginatedTag, self._io.read_u1())
            if self.id_017__ptnairob__contract_id__originated_tag == Id017PtnairobOperationContentsList.Id017PtnairobContractIdOriginatedTag.originated:
                self.originated = Id017PtnairobOperationContentsList.Originated(self._io, self, self._root)



    class SmartRollupCement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.rollup = Id017PtnairobOperationContentsList.Id017PtnairobSmartRollupAddress(self._io, self, self._root)
            self.commitment = self._io.read_bytes(32)


    class CircuitsInfo(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.circuits_info_entries = []
            i = 0
            while not self._io.is_eof():
                self.circuits_info_entries.append(Id017PtnairobOperationContentsList.CircuitsInfoEntries(self._io, self, self._root))
                i += 1



    class Op12(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_2/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id017PtnairobOperationContentsList.Op11(_io__raw_op1, self, self._root)


    class PendingPisEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pending_pis_elt_field0 = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)
            self.pending_pis_elt_field1 = Id017PtnairobOperationContentsList.PendingPisEltField1(self._io, self, self._root)


    class SmartRollupRecoverBond(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)
            self.staker = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)


    class NewState0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_new_state = self._io.read_u4be()
            if not self.len_new_state <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_new_state, self._io, u"/types/new_state_0/seq/0")
            self._raw_new_state = self._io.read_bytes(self.len_new_state)
            _io__raw_new_state = KaitaiStream(BytesIO(self._raw_new_state))
            self.new_state = Id017PtnairobOperationContentsList.NewState(_io__raw_new_state, self, self._root)


    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id017PtnairobOperationContentsList.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class Reveal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.public_key = Id017PtnairobOperationContentsList.PublicKey(self._io, self, self._root)


    class Id017PtnairobInlinedEndorsementMempoolContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__inlined__endorsement_mempool__contents_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Id017PtnairobInlinedEndorsementMempoolContentsTag, self._io.read_u1())
            if self.id_017__ptnairob__inlined__endorsement_mempool__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobInlinedEndorsementMempoolContentsTag.endorsement:
                self.endorsement = Id017PtnairobOperationContentsList.Endorsement(self._io, self, self._root)



    class RevealProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.reveal_proof_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.RevealProofTag, self._io.read_u1())
            if self.reveal_proof_tag == Id017PtnairobOperationContentsList.RevealProofTag.raw__data__proof:
                self.raw__data__proof = Id017PtnairobOperationContentsList.RawData0(self._io, self, self._root)

            if self.reveal_proof_tag == Id017PtnairobOperationContentsList.RevealProofTag.dal__page__proof:
                self.dal__page__proof = Id017PtnairobOperationContentsList.DalPageProof(self._io, self, self._root)



    class RawData(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.raw_data = self._io.read_bytes_full()


    class SmartRollupAddMessages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.message = Id017PtnairobOperationContentsList.Message0(self._io, self, self._root)


    class CircuitsInfo0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_circuits_info = self._io.read_u4be()
            if not self.len_circuits_info <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_circuits_info, self._io, u"/types/circuits_info_0/seq/0")
            self._raw_circuits_info = self._io.read_bytes(self.len_circuits_info)
            _io__raw_circuits_info = KaitaiStream(BytesIO(self._raw_circuits_info))
            self.circuits_info = Id017PtnairobOperationContentsList.CircuitsInfo(_io__raw_circuits_info, self, self._root)


    class Price(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id = self._io.read_bytes(32)
            self.amount = Id017PtnairobOperationContentsList.Z(self._io, self, self._root)


    class MessageEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_elt = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class OpEltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_code = Id017PtnairobOperationContentsList.Int31(self._io, self, self._root)
            self.price = Id017PtnairobOperationContentsList.Price(self._io, self, self._root)
            self.l1_dst = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.rollup_id = self._io.read_bytes(20)
            self.payload = Id017PtnairobOperationContentsList.Payload0(self._io, self, self._root)


    class PrivatePisEltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.new_state = Id017PtnairobOperationContentsList.NewState0(self._io, self, self._root)
            self.fee = self._io.read_bytes(32)


    class Id017PtnairobLiquidityBakingToggleVote(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__liquidity_baking_toggle_vote = self._io.read_s1()


    class Refutation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.refutation_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.RefutationTag, self._io.read_u1())
            if self.refutation_tag == Id017PtnairobOperationContentsList.RefutationTag.start:
                self.start = Id017PtnairobOperationContentsList.Start(self._io, self, self._root)

            if self.refutation_tag == Id017PtnairobOperationContentsList.RefutationTag.move:
                self.move = Id017PtnairobOperationContentsList.Move(self._io, self, self._root)



    class Id017PtnairobBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_hash = self._io.read_bytes(32)
            self.payload_round = self._io.read_s4be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id017PtnairobOperationContentsList.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.liquidity_baking_toggle_vote = Id017PtnairobOperationContentsList.Id017PtnairobLiquidityBakingToggleVote(self._io, self, self._root)


    class SeedNonceRevelation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.nonce = self._io.read_bytes(32)


    class Preendorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id017PtnairobOperationContentsList.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)
            self.annots = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class Id017PtnairobOperationContentsListEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__operation__alpha__contents = Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContents(self._io, self, self._root)


    class Id017PtnairobSmartRollupAddress(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.smart_rollup_hash = self._io.read_bytes(20)


    class DoubleBakingEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bh1 = Id017PtnairobOperationContentsList.Bh10(self._io, self, self._root)
            self.bh2 = Id017PtnairobOperationContentsList.Bh20(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id017PtnairobOperationContentsList.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id017PtnairobOperationContentsList.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id017PtnairobOperationContentsList.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)

            if self.public_key_tag == Id017PtnairobOperationContentsList.PublicKeyTag.bls:
                self.bls = self._io.read_bytes(48)



    class Bh2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__block_header__alpha__full_header = Id017PtnairobOperationContentsList.Id017PtnairobBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id017PtnairobOperationContentsList.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)
            self.args = Id017PtnairobOperationContentsList.Args0(self._io, self, self._root)
            self.annots = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class SlotHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.published_level = self._io.read_s4be()
            self.slot_index = self._io.read_u1()
            self.commitment = self._io.read_bytes(48)
            self.commitment_proof = self._io.read_bytes(96)


    class Bh20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh2 = self._io.read_u4be()
            if not self.len_bh2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh2, self._io, u"/types/bh2_0/seq/0")
            self._raw_bh2 = self._io.read_bytes(self.len_bh2)
            _io__raw_bh2 = KaitaiStream(BytesIO(self._raw_bh2))
            self.bh2 = Id017PtnairobOperationContentsList.Bh2(_io__raw_bh2, self, self._root)


    class Commitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.compressed_state = self._io.read_bytes(32)
            self.inbox_level = self._io.read_s4be()
            self.predecessor = self._io.read_bytes(32)
            self.number_of_ticks = self._io.read_s8be()


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Bool, self._io.read_u1())
            if self.delegate_tag == Id017PtnairobOperationContentsList.Bool.true:
                self.delegate = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)



    class Id017PtnairobOperationAlphaContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__operation__alpha__contents_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag, self._io.read_u1())
            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.endorsement:
                self.endorsement = Id017PtnairobOperationContentsList.Endorsement(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.preendorsement:
                self.preendorsement = Id017PtnairobOperationContentsList.Preendorsement(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.dal_attestation:
                self.dal_attestation = Id017PtnairobOperationContentsList.DalAttestation(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.seed_nonce_revelation:
                self.seed_nonce_revelation = Id017PtnairobOperationContentsList.SeedNonceRevelation(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.vdf_revelation:
                self.vdf_revelation = Id017PtnairobOperationContentsList.Solution(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.double_endorsement_evidence:
                self.double_endorsement_evidence = Id017PtnairobOperationContentsList.DoubleEndorsementEvidence(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.double_preendorsement_evidence:
                self.double_preendorsement_evidence = Id017PtnairobOperationContentsList.DoublePreendorsementEvidence(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.double_baking_evidence:
                self.double_baking_evidence = Id017PtnairobOperationContentsList.DoubleBakingEvidence(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.activate_account:
                self.activate_account = Id017PtnairobOperationContentsList.ActivateAccount(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.proposals:
                self.proposals = Id017PtnairobOperationContentsList.Proposals1(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.ballot:
                self.ballot = Id017PtnairobOperationContentsList.Ballot(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.reveal:
                self.reveal = Id017PtnairobOperationContentsList.Reveal(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.transaction:
                self.transaction = Id017PtnairobOperationContentsList.Transaction(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.origination:
                self.origination = Id017PtnairobOperationContentsList.Origination(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.delegation:
                self.delegation = Id017PtnairobOperationContentsList.Delegation(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.set_deposits_limit:
                self.set_deposits_limit = Id017PtnairobOperationContentsList.SetDepositsLimit(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.increase_paid_storage:
                self.increase_paid_storage = Id017PtnairobOperationContentsList.IncreasePaidStorage(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.update_consensus_key:
                self.update_consensus_key = Id017PtnairobOperationContentsList.UpdateConsensusKey(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.drain_delegate:
                self.drain_delegate = Id017PtnairobOperationContentsList.DrainDelegate(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.failing_noop:
                self.failing_noop = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.register_global_constant:
                self.register_global_constant = Id017PtnairobOperationContentsList.RegisterGlobalConstant(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.transfer_ticket:
                self.transfer_ticket = Id017PtnairobOperationContentsList.TransferTicket(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.dal_publish_slot_header:
                self.dal_publish_slot_header = Id017PtnairobOperationContentsList.DalPublishSlotHeader(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.smart_rollup_originate:
                self.smart_rollup_originate = Id017PtnairobOperationContentsList.SmartRollupOriginate(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.smart_rollup_add_messages:
                self.smart_rollup_add_messages = Id017PtnairobOperationContentsList.SmartRollupAddMessages(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.smart_rollup_cement:
                self.smart_rollup_cement = Id017PtnairobOperationContentsList.SmartRollupCement(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.smart_rollup_publish:
                self.smart_rollup_publish = Id017PtnairobOperationContentsList.SmartRollupPublish(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.smart_rollup_refute:
                self.smart_rollup_refute = Id017PtnairobOperationContentsList.SmartRollupRefute(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.smart_rollup_timeout:
                self.smart_rollup_timeout = Id017PtnairobOperationContentsList.SmartRollupTimeout(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.smart_rollup_execute_outbox_message:
                self.smart_rollup_execute_outbox_message = Id017PtnairobOperationContentsList.SmartRollupExecuteOutboxMessage(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.smart_rollup_recover_bond:
                self.smart_rollup_recover_bond = Id017PtnairobOperationContentsList.SmartRollupRecoverBond(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.zk_rollup_origination:
                self.zk_rollup_origination = Id017PtnairobOperationContentsList.ZkRollupOrigination(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.zk_rollup_publish:
                self.zk_rollup_publish = Id017PtnairobOperationContentsList.ZkRollupPublish(self._io, self, self._root)

            if self.id_017__ptnairob__operation__alpha__contents_tag == Id017PtnairobOperationContentsList.Id017PtnairobOperationAlphaContentsTag.zk_rollup_update:
                self.zk_rollup_update = Id017PtnairobOperationContentsList.ZkRollupUpdate(self._io, self, self._root)



    class RegisterGlobalConstant(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.value = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class Solution(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.solution_field0 = self._io.read_bytes(100)
            self.solution_field1 = self._io.read_bytes(100)


    class PayloadEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_elt = self._io.read_bytes(32)


    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = Id017PtnairobOperationContentsList.Sequence(_io__raw_sequence, self, self._root)


    class PendingPis0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_pending_pis = self._io.read_u4be()
            if not self.len_pending_pis <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_pending_pis, self._io, u"/types/pending_pis_0/seq/0")
            self._raw_pending_pis = self._io.read_bytes(self.len_pending_pis)
            _io__raw_pending_pis = KaitaiStream(BytesIO(self._raw_pending_pis))
            self.pending_pis = Id017PtnairobOperationContentsList.PendingPis(_io__raw_pending_pis, self, self._root)


    class PrivatePis0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_private_pis = self._io.read_u4be()
            if not self.len_private_pis <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_private_pis, self._io, u"/types/private_pis_0/seq/0")
            self._raw_private_pis = self._io.read_bytes(self.len_private_pis)
            _io__raw_private_pis = KaitaiStream(BytesIO(self._raw_private_pis))
            self.private_pis = Id017PtnairobOperationContentsList.PrivatePis(_io__raw_private_pis, self, self._root)


    class Id017PtnairobBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__block_header__alpha__unsigned_contents = Id017PtnairobOperationContentsList.Id017PtnairobBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes_full()


    class PrivatePisEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.private_pis_elt_field0 = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)
            self.private_pis_elt_field1 = Id017PtnairobOperationContentsList.PrivatePisEltField1(self._io, self, self._root)


    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(Id017PtnairobOperationContentsList.SequenceEntries(self._io, self, self._root))
                i += 1



    class Bh10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh1 = self._io.read_u4be()
            if not self.len_bh1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh1, self._io, u"/types/bh1_0/seq/0")
            self._raw_bh1 = self._io.read_bytes(self.len_bh1)
            _io__raw_bh1 = KaitaiStream(BytesIO(self._raw_bh1))
            self.bh1 = Id017PtnairobOperationContentsList.Bh1(_io__raw_bh1, self, self._root)


    class Id017PtnairobScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)
            self.storage = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class Op22(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_2/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id017PtnairobOperationContentsList.Op21(_io__raw_op2, self, self._root)


    class CircuitsInfoEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.circuits_info_elt_field0 = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)
            self.circuits_info_elt_field1 = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.CircuitsInfoEltField1Tag, self._io.read_u1())


    class Message0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message = self._io.read_u4be()
            if not self.len_message <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message, self._io, u"/types/message_0/seq/0")
            self._raw_message = self._io.read_bytes(self.len_message)
            _io__raw_message = KaitaiStream(BytesIO(self._raw_message))
            self.message = Id017PtnairobOperationContentsList.Message(_io__raw_message, self, self._root)


    class InboxProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.message_counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.serialized_proof = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class OpEltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_elt_field1_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.OpEltField1Tag, self._io.read_u1())
            if self.op_elt_field1_tag == Id017PtnairobOperationContentsList.OpEltField1Tag.some:
                self.some = Id017PtnairobOperationContentsList.Some(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Op0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op = self._io.read_u4be()
            if not self.len_op <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op, self._io, u"/types/op_0/seq/0")
            self._raw_op = self._io.read_bytes(self.len_op)
            _io__raw_op = KaitaiStream(BytesIO(self._raw_op))
            self.op = Id017PtnairobOperationContentsList.Op(_io__raw_op, self, self._root)


    class NewState(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.new_state_entries = []
            i = 0
            while not self._io.is_eof():
                self.new_state_entries.append(Id017PtnairobOperationContentsList.NewStateEntries(self._io, self, self._root))
                i += 1



    class SmartRollupPublish(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.rollup = Id017PtnairobOperationContentsList.Id017PtnairobSmartRollupAddress(self._io, self, self._root)
            self.commitment = Id017PtnairobOperationContentsList.Commitment(self._io, self, self._root)


    class Bh1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__block_header__alpha__full_header = Id017PtnairobOperationContentsList.Id017PtnairobBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Payload0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_payload = self._io.read_u4be()
            if not self.len_payload <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_payload, self._io, u"/types/payload_0/seq/0")
            self._raw_payload = self._io.read_bytes(self.len_payload)
            _io__raw_payload = KaitaiStream(BytesIO(self._raw_payload))
            self.payload = Id017PtnairobOperationContentsList.Payload(_io__raw_payload, self, self._root)


    class Dissection0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_dissection = self._io.read_u4be()
            if not self.len_dissection <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_dissection, self._io, u"/types/dissection_0/seq/0")
            self._raw_dissection = self._io.read_bytes(self.len_dissection)
            _io__raw_dissection = KaitaiStream(BytesIO(self._raw_dissection))
            self.dissection = Id017PtnairobOperationContentsList.Dissection(_io__raw_dissection, self, self._root)


    class SetDepositsLimit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.limit_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Bool, self._io.read_u1())
            if self.limit_tag == Id017PtnairobOperationContentsList.Bool.true:
                self.limit = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)



    class Op10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_0/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id017PtnairobOperationContentsList.Op1(_io__raw_op1, self, self._root)


    class InitState0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_init_state = self._io.read_u4be()
            if not self.len_init_state <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_init_state, self._io, u"/types/init_state_0/seq/0")
            self._raw_init_state = self._io.read_bytes(self.len_init_state)
            _io__raw_init_state = KaitaiStream(BytesIO(self._raw_init_state))
            self.init_state = Id017PtnairobOperationContentsList.InitState(_io__raw_init_state, self, self._root)


    class Id017PtnairobContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__contract_id_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Id017PtnairobContractIdTag, self._io.read_u1())
            if self.id_017__ptnairob__contract_id_tag == Id017PtnairobOperationContentsList.Id017PtnairobContractIdTag.implicit:
                self.implicit = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)

            if self.id_017__ptnairob__contract_id_tag == Id017PtnairobOperationContentsList.Id017PtnairobContractIdTag.originated:
                self.originated = Id017PtnairobOperationContentsList.Originated(self._io, self, self._root)



    class Op21(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__inlined__preendorsement = Id017PtnairobOperationContentsList.Id017PtnairobInlinedPreendorsement(self._io, self, self._root)


    class Proof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pvm_step = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)
            self.input_proof_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Bool, self._io.read_u1())
            if self.input_proof_tag == Id017PtnairobOperationContentsList.Bool.true:
                self.input_proof = Id017PtnairobOperationContentsList.InputProof(self._io, self, self._root)



    class SmartRollupRefute(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.rollup = Id017PtnairobOperationContentsList.Id017PtnairobSmartRollupAddress(self._io, self, self._root)
            self.opponent = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.refutation = Id017PtnairobOperationContentsList.Refutation(self._io, self, self._root)


    class Id017PtnairobEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__entrypoint_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Id017PtnairobEntrypointTag, self._io.read_u1())
            if self.id_017__ptnairob__entrypoint_tag == Id017PtnairobOperationContentsList.Id017PtnairobEntrypointTag.named:
                self.named = Id017PtnairobOperationContentsList.Named0(self._io, self, self._root)



    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.balance = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Bool, self._io.read_u1())
            if self.delegate_tag == Id017PtnairobOperationContentsList.Bool.true:
                self.delegate = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)

            self.script = Id017PtnairobOperationContentsList.Id017PtnairobScriptedContracts(self._io, self, self._root)


    class DrainDelegate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.consensus_key = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.delegate = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.destination = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)


    class ZkRollupUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.zk_rollup = self._io.read_bytes(20)
            self.update = Id017PtnairobOperationContentsList.Update(self._io, self, self._root)


    class Message(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_entries.append(Id017PtnairobOperationContentsList.MessageEntries(self._io, self, self._root))
                i += 1



    class InitState(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.init_state_entries = []
            i = 0
            while not self._io.is_eof():
                self.init_state_entries.append(Id017PtnairobOperationContentsList.InitStateEntries(self._io, self, self._root))
                i += 1



    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(Id017PtnairobOperationContentsList.ArgsEntries(self._io, self, self._root))
                i += 1



    class ZkRollupOrigination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.public_parameters = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)
            self.circuits_info = Id017PtnairobOperationContentsList.CircuitsInfo0(self._io, self, self._root)
            self.init_state = Id017PtnairobOperationContentsList.InitState0(self._io, self, self._root)
            self.nb_ops = Id017PtnairobOperationContentsList.Int31(self._io, self, self._root)


    class Op2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__inlined__endorsement = Id017PtnairobOperationContentsList.Id017PtnairobInlinedEndorsement(self._io, self, self._root)


    class Dissection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dissection_entries = []
            i = 0
            while not self._io.is_eof():
                self.dissection_entries.append(Id017PtnairobOperationContentsList.DissectionEntries(self._io, self, self._root))
                i += 1



    class PrivatePis(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.private_pis_entries = []
            i = 0
            while not self._io.is_eof():
                self.private_pis_entries.append(Id017PtnairobOperationContentsList.PrivatePisEntries(self._io, self, self._root))
                i += 1



    class UpdateConsensusKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.pk = Id017PtnairobOperationContentsList.PublicKey(self._io, self, self._root)


    class Id017PtnairobInlinedEndorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__inlined__endorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id017PtnairobOperationContentsList.Id017PtnairobInlinedEndorsementMempoolContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Bool, self._io.read_u1())
            if self.signature_tag == Id017PtnairobOperationContentsList.Bool.true:
                self.signature = self._io.read_bytes_full()



    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id017PtnairobOperationContentsList.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)


    class Update(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pending_pis = Id017PtnairobOperationContentsList.PendingPis0(self._io, self, self._root)
            self.private_pis = Id017PtnairobOperationContentsList.PrivatePis0(self._io, self, self._root)
            self.fee_pi = Id017PtnairobOperationContentsList.NewState0(self._io, self, self._root)
            self.proof = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id017PtnairobOperationContentsList.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)


    class Start(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.player_commitment_hash = self._io.read_bytes(32)
            self.opponent_commitment_hash = self._io.read_bytes(32)


    class InitStateEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.init_state_elt = self._io.read_bytes(32)


    class Id017PtnairobInlinedPreendorsement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__inlined__preendorsement = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id017PtnairobOperationContentsList.Id017PtnairobInlinedPreendorsementContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Bool, self._io.read_u1())
            if self.signature_tag == Id017PtnairobOperationContentsList.Bool.true:
                self.signature = self._io.read_bytes_full()



    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)


    class RawData0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_raw_data = self._io.read_u2be()
            if not self.len_raw_data <= 4096:
                raise kaitaistruct.ValidationGreaterThanError(4096, self.len_raw_data, self._io, u"/types/raw_data_0/seq/0")
            self._raw_raw_data = self._io.read_bytes(self.len_raw_data)
            _io__raw_raw_data = KaitaiStream(BytesIO(self._raw_raw_data))
            self.raw_data = Id017PtnairobOperationContentsList.RawData(_io__raw_raw_data, self, self._root)


    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = Id017PtnairobOperationContentsList.Args(_io__raw_args, self, self._root)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id017PtnairobOperationContentsList.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.amount = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.destination = Id017PtnairobOperationContentsList.Id017PtnairobContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Bool, self._io.read_u1())
            if self.parameters_tag == Id017PtnairobOperationContentsList.Bool.true:
                self.parameters = Id017PtnairobOperationContentsList.Parameters(self._io, self, self._root)



    class DalAttestation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.attestor = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.attestation = Id017PtnairobOperationContentsList.Z(self._io, self, self._root)
            self.level = self._io.read_s4be()


    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id017PtnairobOperationContentsList.Id017PtnairobEntrypoint(self._io, self, self._root)
            self.value = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class Id017PtnairobMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__mutez = Id017PtnairobOperationContentsList.N(self._io, self, self._root)


    class Some(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents = Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)
            self.ty = Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)
            self.ticketer = Id017PtnairobOperationContentsList.Id017PtnairobContractId(self._io, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id017PtnairobOperationContentsList.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)


    class Proposals1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposals = Id017PtnairobOperationContentsList.Proposals0(self._io, self, self._root)


    class DissectionEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.state_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Bool, self._io.read_u1())
            if self.state_tag == Id017PtnairobOperationContentsList.Bool.true:
                self.state = self._io.read_bytes(32)

            self.tick = Id017PtnairobOperationContentsList.N(self._io, self, self._root)


    class Micheline017PtnairobMichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__017__ptnairob__michelson_v1__expression_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1ExpressionTag.int:
                self.int = Id017PtnairobOperationContentsList.Z(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1ExpressionTag.string:
                self.string = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1ExpressionTag.sequence:
                self.sequence = Id017PtnairobOperationContentsList.Sequence0(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = Id017PtnairobOperationContentsList.Id017PtnairobMichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = Id017PtnairobOperationContentsList.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = Id017PtnairobOperationContentsList.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = Id017PtnairobOperationContentsList.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = Id017PtnairobOperationContentsList.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = Id017PtnairobOperationContentsList.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = Id017PtnairobOperationContentsList.PrimGeneric(self._io, self, self._root)

            if self.micheline__017__ptnairob__michelson_v1__expression_tag == Id017PtnairobOperationContentsList.Micheline017PtnairobMichelsonV1ExpressionTag.bytes:
                self.bytes = Id017PtnairobOperationContentsList.BytesDynUint30(self._io, self, self._root)



    class Ballot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposal = self._io.read_bytes(32)
            self.ballot = self._io.read_s1()


    class DalPublishSlotHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.slot_header = Id017PtnairobOperationContentsList.SlotHeader(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id017PtnairobOperationContentsList.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id017PtnairobOperationContentsList.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id017PtnairobOperationContentsList.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id017PtnairobOperationContentsList.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id017PtnairobOperationContentsList.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class Op11(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__inlined__preendorsement = Id017PtnairobOperationContentsList.Id017PtnairobInlinedPreendorsement(self._io, self, self._root)


    class Proposals(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proposals_entries = []
            i = 0
            while not self._io.is_eof():
                self.proposals_entries.append(Id017PtnairobOperationContentsList.ProposalsEntries(self._io, self, self._root))
                i += 1



    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id017PtnairobOperationContentsList.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1



    class OpEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_elt_field0 = Id017PtnairobOperationContentsList.OpEltField0(self._io, self, self._root)
            self.op_elt_field1 = Id017PtnairobOperationContentsList.OpEltField1(self._io, self, self._root)


    class ProposalsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol_hash = self._io.read_bytes(32)


    class Id017PtnairobBlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_017__ptnairob__block_header__alpha__signed_contents = Id017PtnairobOperationContentsList.Id017PtnairobBlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Op1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_017__ptnairob__inlined__endorsement = Id017PtnairobOperationContentsList.Id017PtnairobInlinedEndorsement(self._io, self, self._root)


    class ZkRollupPublish(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id017PtnairobOperationContentsList.PublicKeyHash(self._io, self, self._root)
            self.fee = Id017PtnairobOperationContentsList.Id017PtnairobMutez(self._io, self, self._root)
            self.counter = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.gas_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.storage_limit = Id017PtnairobOperationContentsList.N(self._io, self, self._root)
            self.zk_rollup = self._io.read_bytes(20)
            self.op = Id017PtnairobOperationContentsList.Op0(self._io, self, self._root)


    class Op(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_entries = []
            i = 0
            while not self._io.is_eof():
                self.op_entries.append(Id017PtnairobOperationContentsList.OpEntries(self._io, self, self._root))
                i += 1



    class DoublePreendorsementEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id017PtnairobOperationContentsList.Op12(self._io, self, self._root)
            self.op2 = Id017PtnairobOperationContentsList.Op22(self._io, self, self._root)



