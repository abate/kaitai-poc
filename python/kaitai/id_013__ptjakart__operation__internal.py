# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id013PtjakartOperationInternal(KaitaiStruct):
    """Encoding id: 013-PtJakart.operation.internal."""

    class Id013PtjakartApplyResultsAlphaInternalOperationResultTag(Enum):
        transaction = 1
        origination = 2
        delegation = 3

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id013PtjakartContractIdTag(Enum):
        implicit = 0
        originated = 1

    class Id013PtjakartEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255

    class Id013PtjakartTransactionDestinationTag(Enum):
        implicit = 0
        originated = 1
        tx_rollup = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_013__ptjakart__apply_results__alpha__internal_operation_result = Id013PtjakartOperationInternal.Id013PtjakartApplyResultsAlphaInternalOperationResult(self._io, self, self._root)

    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id013PtjakartOperationInternal.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id013PtjakartEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__entrypoint_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationInternal.Id013PtjakartEntrypointTag, self._io.read_u1())
            if self.id_013__ptjakart__entrypoint_tag == Id013PtjakartOperationInternal.Id013PtjakartEntrypointTag.named:
                self.named = Id013PtjakartOperationInternal.Named0(self._io, self, self._root)



    class Id013PtjakartTxRollupId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.rollup_hash = self._io.read_bytes(20)


    class Id013PtjakartContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__contract_id_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationInternal.Id013PtjakartContractIdTag, self._io.read_u1())
            if self.id_013__ptjakart__contract_id_tag == Id013PtjakartOperationInternal.Id013PtjakartContractIdTag.implicit:
                self.implicit = Id013PtjakartOperationInternal.PublicKeyHash(self._io, self, self._root)

            if self.id_013__ptjakart__contract_id_tag == Id013PtjakartOperationInternal.Id013PtjakartContractIdTag.originated:
                self.originated = Id013PtjakartOperationInternal.Originated(self._io, self, self._root)



    class Id013PtjakartTransactionDestination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__transaction_destination_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationInternal.Id013PtjakartTransactionDestinationTag, self._io.read_u1())
            if self.id_013__ptjakart__transaction_destination_tag == Id013PtjakartOperationInternal.Id013PtjakartTransactionDestinationTag.implicit:
                self.implicit = Id013PtjakartOperationInternal.PublicKeyHash(self._io, self, self._root)

            if self.id_013__ptjakart__transaction_destination_tag == Id013PtjakartOperationInternal.Id013PtjakartTransactionDestinationTag.originated:
                self.originated = Id013PtjakartOperationInternal.Originated(self._io, self, self._root)

            if self.id_013__ptjakart__transaction_destination_tag == Id013PtjakartOperationInternal.Id013PtjakartTransactionDestinationTag.tx_rollup:
                self.tx_rollup = Id013PtjakartOperationInternal.TxRollup(self._io, self, self._root)



    class Id013PtjakartScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id013PtjakartOperationInternal.BytesDynUint30(self._io, self, self._root)
            self.storage = Id013PtjakartOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id013PtjakartOperationInternal.Bool.true:
                self.delegate = Id013PtjakartOperationInternal.PublicKeyHash(self._io, self, self._root)



    class Id013PtjakartApplyResultsAlphaInternalOperationResult(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id013PtjakartOperationInternal.Id013PtjakartContractId(self._io, self, self._root)
            self.nonce = self._io.read_u2be()
            self.id_013__ptjakart__apply_results__alpha__internal_operation_result_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationInternal.Id013PtjakartApplyResultsAlphaInternalOperationResultTag, self._io.read_u1())
            if self.id_013__ptjakart__apply_results__alpha__internal_operation_result_tag == Id013PtjakartOperationInternal.Id013PtjakartApplyResultsAlphaInternalOperationResultTag.transaction:
                self.transaction = Id013PtjakartOperationInternal.Transaction(self._io, self, self._root)

            if self.id_013__ptjakart__apply_results__alpha__internal_operation_result_tag == Id013PtjakartOperationInternal.Id013PtjakartApplyResultsAlphaInternalOperationResultTag.origination:
                self.origination = Id013PtjakartOperationInternal.Origination(self._io, self, self._root)

            if self.id_013__ptjakart__apply_results__alpha__internal_operation_result_tag == Id013PtjakartOperationInternal.Id013PtjakartApplyResultsAlphaInternalOperationResultTag.delegation:
                self.delegation = Id013PtjakartOperationInternal.Delegation(self._io, self, self._root)



    class Id013PtjakartMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__mutez = Id013PtjakartOperationInternal.N(self._io, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.balance = Id013PtjakartOperationInternal.Id013PtjakartMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id013PtjakartOperationInternal.Bool.true:
                self.delegate = Id013PtjakartOperationInternal.PublicKeyHash(self._io, self, self._root)

            self.script = Id013PtjakartOperationInternal.Id013PtjakartScriptedContracts(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id013PtjakartOperationInternal.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount = Id013PtjakartOperationInternal.Id013PtjakartMutez(self._io, self, self._root)
            self.destination = Id013PtjakartOperationInternal.Id013PtjakartTransactionDestination(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationInternal.Bool, self._io.read_u1())
            if self.parameters_tag == Id013PtjakartOperationInternal.Bool.true:
                self.parameters = Id013PtjakartOperationInternal.Parameters(self._io, self, self._root)



    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id013PtjakartOperationInternal.Id013PtjakartEntrypoint(self._io, self, self._root)
            self.value = Id013PtjakartOperationInternal.BytesDynUint30(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id013PtjakartOperationInternal.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id013PtjakartOperationInternal.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id013PtjakartOperationInternal.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id013PtjakartOperationInternal.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)



    class TxRollup(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_013__ptjakart__tx_rollup_id = Id013PtjakartOperationInternal.Id013PtjakartTxRollupId(self._io, self, self._root)
            self.tx_rollup_padding = self._io.read_bytes(1)



