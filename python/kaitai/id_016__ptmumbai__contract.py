# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id016PtmumbaiContract(KaitaiStruct):
    """Encoding id: 016-PtMumbai.contract."""

    class Id016PtmumbaiContractIdTag(Enum):
        implicit = 0
        originated = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_016__ptmumbai__contract_id = Id016PtmumbaiContract.Id016PtmumbaiContractId(self._io, self, self._root)

    class Id016PtmumbaiContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_016__ptmumbai__contract_id_tag = KaitaiStream.resolve_enum(Id016PtmumbaiContract.Id016PtmumbaiContractIdTag, self._io.read_u1())
            if self.id_016__ptmumbai__contract_id_tag == Id016PtmumbaiContract.Id016PtmumbaiContractIdTag.implicit:
                self.implicit = Id016PtmumbaiContract.PublicKeyHash(self._io, self, self._root)

            if self.id_016__ptmumbai__contract_id_tag == Id016PtmumbaiContract.Id016PtmumbaiContractIdTag.originated:
                self.originated = Id016PtmumbaiContract.Originated(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id016PtmumbaiContract.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id016PtmumbaiContract.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id016PtmumbaiContract.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id016PtmumbaiContract.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id016PtmumbaiContract.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)




