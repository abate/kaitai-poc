# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id019PtparisaScriptPrim(KaitaiStruct):
    """Encoding id: 019-PtParisA.script.prim."""

    class Id019PtparisaMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left = 5
        none = 6
        pair_1 = 7
        right = 8
        some_0 = 9
        true = 10
        unit_1 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left_0 = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none_0 = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right_0 = 68
        size = 69
        some = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_0 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction_deprecated = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_deprecated = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140
        chest = 141
        chest_key = 142
        open_chest = 143
        view_0 = 144
        view = 145
        constant = 146
        sub_mutez = 147
        tx_rollup_l2_address = 148
        min_block_time = 149
        sapling_transaction = 150
        emit = 151
        lambda_rec_0 = 152
        lambda_rec = 153
        ticket_0 = 154
        bytes_0 = 155
        nat_0 = 156
        ticket_1 = 157
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_019__ptparisa__michelson__v1__primitives = Id019PtparisaScriptPrim.Id019PtparisaMichelsonV1Primitives(self._io, self, self._root)

    class Id019PtparisaMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_019__ptparisa__michelson__v1__primitives = KaitaiStream.resolve_enum(Id019PtparisaScriptPrim.Id019PtparisaMichelsonV1Primitives, self._io.read_u1())



