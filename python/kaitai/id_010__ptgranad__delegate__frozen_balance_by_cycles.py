# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id010PtgranadDelegateFrozenBalanceByCycles(KaitaiStruct):
    """Encoding id: 010-PtGRANAD.delegate.frozen_balance_by_cycles."""
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.len_id_010__ptgranad__delegate__frozen_balance_by_cycles = self._io.read_u4be()
        if not self.len_id_010__ptgranad__delegate__frozen_balance_by_cycles <= 1073741823:
            raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_id_010__ptgranad__delegate__frozen_balance_by_cycles, self._io, u"/seq/0")
        self._raw_id_010__ptgranad__delegate__frozen_balance_by_cycles = self._io.read_bytes(self.len_id_010__ptgranad__delegate__frozen_balance_by_cycles)
        _io__raw_id_010__ptgranad__delegate__frozen_balance_by_cycles = KaitaiStream(BytesIO(self._raw_id_010__ptgranad__delegate__frozen_balance_by_cycles))
        self.id_010__ptgranad__delegate__frozen_balance_by_cycles = Id010PtgranadDelegateFrozenBalanceByCycles.Id010PtgranadDelegateFrozenBalanceByCycles(_io__raw_id_010__ptgranad__delegate__frozen_balance_by_cycles, self, self._root)

    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id010PtgranadDelegateFrozenBalanceByCycles.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id010PtgranadDelegateFrozenBalanceByCyclesEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.cycle = self._io.read_s4be()
            self.deposits = Id010PtgranadDelegateFrozenBalanceByCycles.Id010PtgranadMutez(self._io, self, self._root)
            self.fees = Id010PtgranadDelegateFrozenBalanceByCycles.Id010PtgranadMutez(self._io, self, self._root)
            self.rewards = Id010PtgranadDelegateFrozenBalanceByCycles.Id010PtgranadMutez(self._io, self, self._root)


    class Id010PtgranadDelegateFrozenBalanceByCycles(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__delegate__frozen_balance_by_cycles_entries = []
            i = 0
            while not self._io.is_eof():
                self.id_010__ptgranad__delegate__frozen_balance_by_cycles_entries.append(Id010PtgranadDelegateFrozenBalanceByCycles.Id010PtgranadDelegateFrozenBalanceByCyclesEntries(self._io, self, self._root))
                i += 1



    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Id010PtgranadMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_010__ptgranad__mutez = Id010PtgranadDelegateFrozenBalanceByCycles.N(self._io, self, self._root)



