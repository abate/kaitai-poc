# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id017PtnairobFa12TokenTransfer(KaitaiStruct):
    """Encoding id: 017-PtNairob.fa1.2.token_transfer."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.token_contract = Id017PtnairobFa12TokenTransfer.BytesDynUint30(self._io, self, self._root)
        self.destination = Id017PtnairobFa12TokenTransfer.BytesDynUint30(self._io, self, self._root)
        self.amount = Id017PtnairobFa12TokenTransfer.Z(self._io, self, self._root)
        self.tez__amount_tag = KaitaiStream.resolve_enum(Id017PtnairobFa12TokenTransfer.Bool, self._io.read_u1())
        if self.tez__amount_tag == Id017PtnairobFa12TokenTransfer.Bool.true:
            self.tez__amount = Id017PtnairobFa12TokenTransfer.BytesDynUint30(self._io, self, self._root)

        self.fee_tag = KaitaiStream.resolve_enum(Id017PtnairobFa12TokenTransfer.Bool, self._io.read_u1())
        if self.fee_tag == Id017PtnairobFa12TokenTransfer.Bool.true:
            self.fee = Id017PtnairobFa12TokenTransfer.BytesDynUint30(self._io, self, self._root)

        self.gas__limit_tag = KaitaiStream.resolve_enum(Id017PtnairobFa12TokenTransfer.Bool, self._io.read_u1())
        if self.gas__limit_tag == Id017PtnairobFa12TokenTransfer.Bool.true:
            self.gas__limit = Id017PtnairobFa12TokenTransfer.N(self._io, self, self._root)

        self.storage__limit_tag = KaitaiStream.resolve_enum(Id017PtnairobFa12TokenTransfer.Bool, self._io.read_u1())
        if self.storage__limit_tag == Id017PtnairobFa12TokenTransfer.Bool.true:
            self.storage__limit = Id017PtnairobFa12TokenTransfer.Z(self._io, self, self._root)


    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id017PtnairobFa12TokenTransfer.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id017PtnairobFa12TokenTransfer.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1




