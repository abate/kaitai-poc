# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import timestamp__system
from kaitai import p2p_point__state
class P2pPointInfo(KaitaiStruct):
    """Encoding id: p2p_point.info
    Description: Information about a peer point. Includes flags, state, and records about past events."""

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.trusted = KaitaiStream.resolve_enum(P2pPointInfo.Bool, self._io.read_u1())
        self.greylisted_until_tag = KaitaiStream.resolve_enum(P2pPointInfo.Bool, self._io.read_u1())
        if self.greylisted_until_tag == P2pPointInfo.Bool.true:
            self.greylisted_until = timestamp__system.TimestampSystem(self._io)

        self.state = p2p_point__state.P2pPointState(self._io)
        self.p2p_peer_id_tag = KaitaiStream.resolve_enum(P2pPointInfo.Bool, self._io.read_u1())
        if self.p2p_peer_id_tag == P2pPointInfo.Bool.true:
            self.p2p_peer_id = self._io.read_bytes(16)

        self.last_failed_connection_tag = KaitaiStream.resolve_enum(P2pPointInfo.Bool, self._io.read_u1())
        if self.last_failed_connection_tag == P2pPointInfo.Bool.true:
            self.last_failed_connection = timestamp__system.TimestampSystem(self._io)

        self.last_rejected_connection_tag = KaitaiStream.resolve_enum(P2pPointInfo.Bool, self._io.read_u1())
        if self.last_rejected_connection_tag == P2pPointInfo.Bool.true:
            self.last_rejected_connection = P2pPointInfo.LastRejectedConnection(self._io, self, self._root)

        self.last_established_connection_tag = KaitaiStream.resolve_enum(P2pPointInfo.Bool, self._io.read_u1())
        if self.last_established_connection_tag == P2pPointInfo.Bool.true:
            self.last_established_connection = P2pPointInfo.LastEstablishedConnection(self._io, self, self._root)

        self.last_disconnection_tag = KaitaiStream.resolve_enum(P2pPointInfo.Bool, self._io.read_u1())
        if self.last_disconnection_tag == P2pPointInfo.Bool.true:
            self.last_disconnection = P2pPointInfo.LastDisconnection(self._io, self, self._root)

        self.last_seen_tag = KaitaiStream.resolve_enum(P2pPointInfo.Bool, self._io.read_u1())
        if self.last_seen_tag == P2pPointInfo.Bool.true:
            self.last_seen = P2pPointInfo.LastSeen(self._io, self, self._root)

        self.last_miss_tag = KaitaiStream.resolve_enum(P2pPointInfo.Bool, self._io.read_u1())
        if self.last_miss_tag == P2pPointInfo.Bool.true:
            self.last_miss = timestamp__system.TimestampSystem(self._io)

        self.expected_peer_id_tag = KaitaiStream.resolve_enum(P2pPointInfo.Bool, self._io.read_u1())
        if self.expected_peer_id_tag == P2pPointInfo.Bool.true:
            self.expected_peer_id = self._io.read_bytes(16)


    class LastDisconnection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.last_disconnection_field0 = self._io.read_bytes(16)
            self.last_disconnection_field1 = timestamp__system.TimestampSystem(self._io)


    class LastEstablishedConnection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.last_established_connection_field0 = self._io.read_bytes(16)
            self.last_established_connection_field1 = timestamp__system.TimestampSystem(self._io)


    class LastRejectedConnection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.last_rejected_connection_field0 = self._io.read_bytes(16)
            self.last_rejected_connection_field1 = timestamp__system.TimestampSystem(self._io)


    class LastSeen(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.last_seen_field0 = self._io.read_bytes(16)
            self.last_seen_field1 = timestamp__system.TimestampSystem(self._io)



