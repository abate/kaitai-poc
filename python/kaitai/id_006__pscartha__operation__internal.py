# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id006PscarthaOperationInternal(KaitaiStruct):
    """Encoding id: 006-PsCARTHA.operation.internal."""

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2

    class Bool(Enum):
        false = 0
        true = 255

    class Id006PscarthaEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        named = 255

    class Id006PscarthaOperationAlphaInternalOperationTag(Enum):
        reveal = 0
        transaction = 1
        origination = 2
        delegation = 3

    class Id006PscarthaContractIdTag(Enum):
        implicit = 0
        originated = 1
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_006__pscartha__operation__alpha__internal_operation = Id006PscarthaOperationInternal.Id006PscarthaOperationAlphaInternalOperation(self._io, self, self._root)

    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id006PscarthaOperationInternal.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id006PscarthaContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__contract_id_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationInternal.Id006PscarthaContractIdTag, self._io.read_u1())
            if self.id_006__pscartha__contract_id_tag == Id006PscarthaOperationInternal.Id006PscarthaContractIdTag.implicit:
                self.implicit = Id006PscarthaOperationInternal.PublicKeyHash(self._io, self, self._root)

            if self.id_006__pscartha__contract_id_tag == Id006PscarthaOperationInternal.Id006PscarthaContractIdTag.originated:
                self.originated = Id006PscarthaOperationInternal.Originated(self._io, self, self._root)



    class Id006PscarthaOperationAlphaInternalOperation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id006PscarthaOperationInternal.Id006PscarthaContractId(self._io, self, self._root)
            self.nonce = self._io.read_u2be()
            self.id_006__pscartha__operation__alpha__internal_operation_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationInternal.Id006PscarthaOperationAlphaInternalOperationTag, self._io.read_u1())
            if self.id_006__pscartha__operation__alpha__internal_operation_tag == Id006PscarthaOperationInternal.Id006PscarthaOperationAlphaInternalOperationTag.reveal:
                self.reveal = Id006PscarthaOperationInternal.PublicKey(self._io, self, self._root)

            if self.id_006__pscartha__operation__alpha__internal_operation_tag == Id006PscarthaOperationInternal.Id006PscarthaOperationAlphaInternalOperationTag.transaction:
                self.transaction = Id006PscarthaOperationInternal.Transaction(self._io, self, self._root)

            if self.id_006__pscartha__operation__alpha__internal_operation_tag == Id006PscarthaOperationInternal.Id006PscarthaOperationAlphaInternalOperationTag.origination:
                self.origination = Id006PscarthaOperationInternal.Origination(self._io, self, self._root)

            if self.id_006__pscartha__operation__alpha__internal_operation_tag == Id006PscarthaOperationInternal.Id006PscarthaOperationAlphaInternalOperationTag.delegation:
                self.delegation = Id006PscarthaOperationInternal.Delegation(self._io, self, self._root)



    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationInternal.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id006PscarthaOperationInternal.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id006PscarthaOperationInternal.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id006PscarthaOperationInternal.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)



    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.delegate_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id006PscarthaOperationInternal.Bool.true:
                self.delegate = Id006PscarthaOperationInternal.PublicKeyHash(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Id006PscarthaEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__entrypoint_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationInternal.Id006PscarthaEntrypointTag, self._io.read_u1())
            if self.id_006__pscartha__entrypoint_tag == Id006PscarthaOperationInternal.Id006PscarthaEntrypointTag.named:
                self.named = Id006PscarthaOperationInternal.Named0(self._io, self, self._root)



    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.balance = Id006PscarthaOperationInternal.Id006PscarthaMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationInternal.Bool, self._io.read_u1())
            if self.delegate_tag == Id006PscarthaOperationInternal.Bool.true:
                self.delegate = Id006PscarthaOperationInternal.PublicKeyHash(self._io, self, self._root)

            self.script = Id006PscarthaOperationInternal.Id006PscarthaScriptedContracts(self._io, self, self._root)


    class Id006PscarthaMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_006__pscartha__mutez = Id006PscarthaOperationInternal.N(self._io, self, self._root)


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id006PscarthaOperationInternal.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.amount = Id006PscarthaOperationInternal.Id006PscarthaMutez(self._io, self, self._root)
            self.destination = Id006PscarthaOperationInternal.Id006PscarthaContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationInternal.Bool, self._io.read_u1())
            if self.parameters_tag == Id006PscarthaOperationInternal.Bool.true:
                self.parameters = Id006PscarthaOperationInternal.Parameters(self._io, self, self._root)



    class Id006PscarthaScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id006PscarthaOperationInternal.BytesDynUint30(self._io, self, self._root)
            self.storage = Id006PscarthaOperationInternal.BytesDynUint30(self._io, self, self._root)


    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id006PscarthaOperationInternal.Id006PscarthaEntrypoint(self._io, self, self._root)
            self.value = Id006PscarthaOperationInternal.BytesDynUint30(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id006PscarthaOperationInternal.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id006PscarthaOperationInternal.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id006PscarthaOperationInternal.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id006PscarthaOperationInternal.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




