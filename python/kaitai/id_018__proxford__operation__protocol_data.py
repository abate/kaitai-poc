# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import block_header__shell
from kaitai import operation__shell_header
class Id018ProxfordOperationProtocolData(KaitaiStruct):
    """Encoding id: 018-Proxford.operation.protocol_data."""

    class Id018ProxfordEntrypointTag(Enum):
        default = 0
        root = 1
        do = 2
        set_delegate = 3
        remove_delegate = 4
        deposit = 5
        stake = 6
        unstake = 7
        finalize_unstake = 8
        set_delegate_parameters = 9
        named = 255

    class RevealProofTag(Enum):
        raw__data__proof = 0
        metadata__proof = 1
        dal__page__proof = 2
        dal__parameters__proof = 3

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class PvmKind(Enum):
        arith = 0
        wasm_2_0_0 = 1
        riscv = 2

    class RefutationTag(Enum):
        start = 0
        move = 1

    class Id018ProxfordInlinedPreattestationContentsTag(Enum):
        preattestation = 20

    class Id018ProxfordInlinedAttestationMempoolContentsTag(Enum):
        attestation = 21

    class BlsSignaturePrefixTag(Enum):
        bls_prefix = 3

    class Id018ProxfordPerBlockVotesTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__8 = 8
        case__9 = 9
        case__10 = 10

    class OpEltField1Tag(Enum):
        none = 0
        some = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3

    class Bool(Enum):
        false = 0
        true = 255

    class CircuitsInfoEltField1Tag(Enum):
        public = 0
        private = 1
        fee = 2

    class Id018ProxfordMichelsonV1Primitives(Enum):
        parameter = 0
        storage = 1
        code = 2
        false = 3
        elt = 4
        left = 5
        none_0 = 6
        pair_1 = 7
        right = 8
        some_0 = 9
        true = 10
        unit_0 = 11
        pack = 12
        unpack = 13
        blake2b = 14
        sha256 = 15
        sha512 = 16
        abs = 17
        add = 18
        amount = 19
        and = 20
        balance = 21
        car = 22
        cdr = 23
        check_signature = 24
        compare = 25
        concat = 26
        cons = 27
        create_account = 28
        create_contract = 29
        implicit_account = 30
        dip = 31
        drop = 32
        dup = 33
        ediv = 34
        empty_map = 35
        empty_set = 36
        eq = 37
        exec = 38
        failwith = 39
        ge = 40
        get = 41
        gt = 42
        hash_key = 43
        if = 44
        if_cons = 45
        if_left = 46
        if_none = 47
        int_0 = 48
        lambda_0 = 49
        le = 50
        left_0 = 51
        loop = 52
        lsl = 53
        lsr = 54
        lt = 55
        map_0 = 56
        mem = 57
        mul = 58
        neg = 59
        neq = 60
        nil = 61
        none = 62
        not = 63
        now = 64
        or_0 = 65
        pair_0 = 66
        push = 67
        right_0 = 68
        size = 69
        some = 70
        source = 71
        sender = 72
        self = 73
        steps_to_quota = 74
        sub = 75
        swap = 76
        transfer_tokens = 77
        set_delegate = 78
        unit_1 = 79
        update = 80
        xor = 81
        iter = 82
        loop_left = 83
        address_0 = 84
        contract_0 = 85
        isnat = 86
        cast = 87
        rename = 88
        bool = 89
        contract = 90
        int = 91
        key = 92
        key_hash = 93
        lambda = 94
        list = 95
        map = 96
        big_map = 97
        nat = 98
        option = 99
        or = 100
        pair = 101
        set = 102
        signature = 103
        string = 104
        bytes = 105
        mutez = 106
        timestamp = 107
        unit = 108
        operation = 109
        address = 110
        slice = 111
        dig = 112
        dug = 113
        empty_big_map = 114
        apply = 115
        chain_id = 116
        chain_id_0 = 117
        level = 118
        self_address = 119
        never = 120
        never_0 = 121
        unpair = 122
        voting_power = 123
        total_voting_power = 124
        keccak = 125
        sha3 = 126
        pairing_check = 127
        bls12_381_g1 = 128
        bls12_381_g2 = 129
        bls12_381_fr = 130
        sapling_state = 131
        sapling_transaction_deprecated = 132
        sapling_empty_state = 133
        sapling_verify_update = 134
        ticket = 135
        ticket_deprecated = 136
        read_ticket = 137
        split_ticket = 138
        join_tickets = 139
        get_and_update = 140
        chest = 141
        chest_key = 142
        open_chest = 143
        view_0 = 144
        view = 145
        constant = 146
        sub_mutez = 147
        tx_rollup_l2_address = 148
        min_block_time = 149
        sapling_transaction = 150
        emit = 151
        lambda_rec = 152
        lambda_rec_0 = 153
        ticket_0 = 154
        bytes_0 = 155
        nat_0 = 156

    class Id018ProxfordContractIdOriginatedTag(Enum):
        originated = 1

    class Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag(Enum):
        seed_nonce_revelation = 1
        double_attestation_evidence = 2
        double_baking_evidence = 3
        activate_account = 4
        proposals = 5
        ballot = 6
        double_preattestation_evidence = 7
        vdf_revelation = 8
        drain_delegate = 9
        failing_noop = 17
        preattestation = 20
        attestation = 21
        dal_attestation = 22
        reveal = 107
        transaction = 108
        origination = 109
        delegation = 110
        register_global_constant = 111
        set_deposits_limit = 112
        increase_paid_storage = 113
        update_consensus_key = 114
        transfer_ticket = 158
        smart_rollup_originate = 200
        smart_rollup_add_messages = 201
        smart_rollup_cement = 202
        smart_rollup_publish = 203
        smart_rollup_refute = 204
        smart_rollup_timeout = 205
        smart_rollup_execute_outbox_message = 206
        smart_rollup_recover_bond = 207
        dal_publish_slot_header = 230
        zk_rollup_origination = 250
        zk_rollup_publish = 251
        zk_rollup_update = 252
        signature_prefix = 255

    class Micheline018ProxfordMichelsonV1ExpressionTag(Enum):
        int = 0
        string = 1
        sequence = 2
        prim__no_args__no_annots = 3
        prim__no_args__some_annots = 4
        prim__1_arg__no_annots = 5
        prim__1_arg__some_annots = 6
        prim__2_args__no_annots = 7
        prim__2_args__some_annots = 8
        prim__generic = 9
        bytes = 10

    class StepTag(Enum):
        dissection = 0
        proof = 1

    class Id018ProxfordContractIdTag(Enum):
        implicit = 0
        originated = 1

    class InputProofTag(Enum):
        inbox__proof = 0
        reveal__proof = 1
        first__input = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_018__proxford__operation_with_legacy_attestation_name__alpha__contents_and_signature = Id018ProxfordOperationProtocolData.Id018ProxfordOperationWithLegacyAttestationNameAlphaContentsAndSignature(self._io, self, self._root)

    class Id018ProxfordBlockHeaderAlphaFullHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__block_header__alpha__full_header = block_header__shell.BlockHeaderShell(self._io)
            self.id_018__proxford__block_header__alpha__signed_contents = Id018ProxfordOperationProtocolData.Id018ProxfordBlockHeaderAlphaSignedContents(self._io, self, self._root)


    class Op20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_0/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id018ProxfordOperationProtocolData.Op2(_io__raw_op2, self, self._root)


    class DalPageId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.published_level = self._io.read_s4be()
            self.slot_index = self._io.read_u1()
            self.page_index = self._io.read_s2be()


    class Whitelist0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_whitelist = self._io.read_u4be()
            if not self.len_whitelist <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_whitelist, self._io, u"/types/whitelist_0/seq/0")
            self._raw_whitelist = self._io.read_bytes(self.len_whitelist)
            _io__raw_whitelist = KaitaiStream(BytesIO(self._raw_whitelist))
            self.whitelist = Id018ProxfordOperationProtocolData.Whitelist(_io__raw_whitelist, self, self._root)


    class SmartRollupExecuteOutboxMessage(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)
            self.cemented_commitment = self._io.read_bytes(32)
            self.output_proof = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class SmartRollupOriginate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.pvm_kind = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.PvmKind, self._io.read_u1())
            self.kernel = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.parameters_ty = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.whitelist_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Bool, self._io.read_u1())
            if self.whitelist_tag == Id018ProxfordOperationProtocolData.Bool.true:
                self.whitelist = Id018ProxfordOperationProtocolData.Whitelist0(self._io, self, self._root)



    class Stakers(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alice = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.bob = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)


    class DalPageProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dal_page_id = Id018ProxfordOperationProtocolData.DalPageId(self._io, self, self._root)
            self.dal_proof = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class IncreasePaidStorage(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.amount = Id018ProxfordOperationProtocolData.Z(self._io, self, self._root)
            self.destination = Id018ProxfordOperationProtocolData.Id018ProxfordContractIdOriginated(self._io, self, self._root)


    class Move(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.choice = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.step = Id018ProxfordOperationProtocolData.Step(self._io, self, self._root)


    class InputProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.input_proof_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.InputProofTag, self._io.read_u1())
            if self.input_proof_tag == Id018ProxfordOperationProtocolData.InputProofTag.inbox__proof:
                self.inbox__proof = Id018ProxfordOperationProtocolData.InboxProof(self._io, self, self._root)

            if self.input_proof_tag == Id018ProxfordOperationProtocolData.InputProofTag.reveal__proof:
                self.reveal__proof = Id018ProxfordOperationProtocolData.RevealProof(self._io, self, self._root)



    class SmartRollupTimeout(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)
            self.stakers = Id018ProxfordOperationProtocolData.Stakers(self._io, self, self._root)


    class ActivateAccount(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pkh = self._io.read_bytes(20)
            self.secret = self._io.read_bytes(20)


    class Id018ProxfordScriptedContracts(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.code = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.storage = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Step(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.step_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.StepTag, self._io.read_u1())
            if self.step_tag == Id018ProxfordOperationProtocolData.StepTag.dissection:
                self.dissection = Id018ProxfordOperationProtocolData.Dissection0(self._io, self, self._root)

            if self.step_tag == Id018ProxfordOperationProtocolData.StepTag.proof:
                self.proof = Id018ProxfordOperationProtocolData.Proof(self._io, self, self._root)



    class PendingPisEltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.new_state = Id018ProxfordOperationProtocolData.NewState0(self._io, self, self._root)
            self.fee = self._io.read_bytes(32)
            self.exit_validity = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Bool, self._io.read_u1())


    class NewStateEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.new_state_elt = self._io.read_bytes(32)


    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class Id018ProxfordInlinedAttestation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__inlined__attestation = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id018ProxfordOperationProtocolData.Id018ProxfordInlinedAttestationMempoolContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Bool, self._io.read_u1())
            if self.signature_tag == Id018ProxfordOperationProtocolData.Bool.true:
                self.signature = self._io.read_bytes_full()



    class N(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.n = []
            i = 0
            while True:
                _ = Id018ProxfordOperationProtocolData.NChunk(self._io, self, self._root)
                self.n.append(_)
                if not (_.has_more):
                    break
                i += 1


    class Id018ProxfordBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__block_header__alpha__unsigned_contents = Id018ProxfordOperationProtocolData.Id018ProxfordBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes_full()


    class Proposals0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_proposals = self._io.read_u4be()
            if not self.len_proposals <= 640:
                raise kaitaistruct.ValidationGreaterThanError(640, self.len_proposals, self._io, u"/types/proposals_0/seq/0")
            self._raw_proposals = self._io.read_bytes(self.len_proposals)
            _io__raw_proposals = KaitaiStream(BytesIO(self._raw_proposals))
            self.proposals = Id018ProxfordOperationProtocolData.Proposals(_io__raw_proposals, self, self._root)


    class TransferTicket(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.ticket_contents = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.ticket_ty = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.ticket_ticketer = Id018ProxfordOperationProtocolData.Id018ProxfordContractId(self._io, self, self._root)
            self.ticket_amount = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.destination = Id018ProxfordOperationProtocolData.Id018ProxfordContractId(self._io, self, self._root)
            self.entrypoint = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class PendingPis(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pending_pis_entries = []
            i = 0
            while not self._io.is_eof():
                self.pending_pis_entries.append(Id018ProxfordOperationProtocolData.PendingPisEntries(self._io, self, self._root))
                i += 1



    class Payload(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_entries = []
            i = 0
            while not self._io.is_eof():
                self.payload_entries.append(Id018ProxfordOperationProtocolData.PayloadEntries(self._io, self, self._root))
                i += 1



    class Id018ProxfordContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__contract_id_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Id018ProxfordContractIdTag, self._io.read_u1())
            if self.id_018__proxford__contract_id_tag == Id018ProxfordOperationProtocolData.Id018ProxfordContractIdTag.implicit:
                self.implicit = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)

            if self.id_018__proxford__contract_id_tag == Id018ProxfordOperationProtocolData.Id018ProxfordContractIdTag.originated:
                self.originated = Id018ProxfordOperationProtocolData.Originated(self._io, self, self._root)



    class SmartRollupCement(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)


    class CircuitsInfo(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.circuits_info_entries = []
            i = 0
            while not self._io.is_eof():
                self.circuits_info_entries.append(Id018ProxfordOperationProtocolData.CircuitsInfoEntries(self._io, self, self._root))
                i += 1



    class Op12(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_2/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id018ProxfordOperationProtocolData.Op11(_io__raw_op1, self, self._root)


    class Id018ProxfordContractIdOriginated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__contract_id__originated_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Id018ProxfordContractIdOriginatedTag, self._io.read_u1())
            if self.id_018__proxford__contract_id__originated_tag == Id018ProxfordOperationProtocolData.Id018ProxfordContractIdOriginatedTag.originated:
                self.originated = Id018ProxfordOperationProtocolData.Originated(self._io, self, self._root)



    class PendingPisEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pending_pis_elt_field0 = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.pending_pis_elt_field1 = Id018ProxfordOperationProtocolData.PendingPisEltField1(self._io, self, self._root)


    class SmartRollupRecoverBond(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)
            self.staker = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)


    class NewState0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_new_state = self._io.read_u4be()
            if not self.len_new_state <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_new_state, self._io, u"/types/new_state_0/seq/0")
            self._raw_new_state = self._io.read_bytes(self.len_new_state)
            _io__raw_new_state = KaitaiStream(BytesIO(self._raw_new_state))
            self.new_state = Id018ProxfordOperationProtocolData.NewState(_io__raw_new_state, self, self._root)


    class Prim1ArgSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id018ProxfordOperationProtocolData.Id018ProxfordMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Reveal(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.public_key = Id018ProxfordOperationProtocolData.PublicKey(self._io, self, self._root)


    class RevealProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.reveal_proof_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.RevealProofTag, self._io.read_u1())
            if self.reveal_proof_tag == Id018ProxfordOperationProtocolData.RevealProofTag.raw__data__proof:
                self.raw__data__proof = Id018ProxfordOperationProtocolData.RawData0(self._io, self, self._root)

            if self.reveal_proof_tag == Id018ProxfordOperationProtocolData.RevealProofTag.dal__page__proof:
                self.dal__page__proof = Id018ProxfordOperationProtocolData.DalPageProof(self._io, self, self._root)



    class RawData(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.raw_data = self._io.read_bytes_full()


    class SmartRollupAddMessages(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.message = Id018ProxfordOperationProtocolData.Message0(self._io, self, self._root)


    class CircuitsInfo0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_circuits_info = self._io.read_u4be()
            if not self.len_circuits_info <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_circuits_info, self._io, u"/types/circuits_info_0/seq/0")
            self._raw_circuits_info = self._io.read_bytes(self.len_circuits_info)
            _io__raw_circuits_info = KaitaiStream(BytesIO(self._raw_circuits_info))
            self.circuits_info = Id018ProxfordOperationProtocolData.CircuitsInfo(_io__raw_circuits_info, self, self._root)


    class Price(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id = self._io.read_bytes(32)
            self.amount = Id018ProxfordOperationProtocolData.Z(self._io, self, self._root)


    class MessageEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_elt = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class OpEltField0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_code = Id018ProxfordOperationProtocolData.Int31(self._io, self, self._root)
            self.price = Id018ProxfordOperationProtocolData.Price(self._io, self, self._root)
            self.l1_dst = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.rollup_id = self._io.read_bytes(20)
            self.payload = Id018ProxfordOperationProtocolData.Payload0(self._io, self, self._root)


    class Preattestation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class PrivatePisEltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.new_state = Id018ProxfordOperationProtocolData.NewState0(self._io, self, self._root)
            self.fee = self._io.read_bytes(32)


    class Refutation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.refutation_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.RefutationTag, self._io.read_u1())
            if self.refutation_tag == Id018ProxfordOperationProtocolData.RefutationTag.start:
                self.start = Id018ProxfordOperationProtocolData.Start(self._io, self, self._root)

            if self.refutation_tag == Id018ProxfordOperationProtocolData.RefutationTag.move:
                self.move = Id018ProxfordOperationProtocolData.Move(self._io, self, self._root)



    class SeedNonceRevelation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.nonce = self._io.read_bytes(32)


    class PrimNoArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id018ProxfordOperationProtocolData.Id018ProxfordMichelsonV1Primitives(self._io, self, self._root)
            self.annots = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class WhitelistEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.signature__public_key_hash = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)


    class Attestation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot = self._io.read_u2be()
            self.level = self._io.read_s4be()
            self.round = self._io.read_s4be()
            self.block_payload_hash = self._io.read_bytes(32)


    class Id018ProxfordInlinedPreattestationContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__inlined__preattestation__contents_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Id018ProxfordInlinedPreattestationContentsTag, self._io.read_u1())
            if self.id_018__proxford__inlined__preattestation__contents_tag == Id018ProxfordOperationProtocolData.Id018ProxfordInlinedPreattestationContentsTag.preattestation:
                self.preattestation = Id018ProxfordOperationProtocolData.Preattestation(self._io, self, self._root)



    class DoubleBakingEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bh1 = Id018ProxfordOperationProtocolData.Bh10(self._io, self, self._root)
            self.bh2 = Id018ProxfordOperationProtocolData.Bh20(self._io, self, self._root)


    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == Id018ProxfordOperationProtocolData.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == Id018ProxfordOperationProtocolData.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == Id018ProxfordOperationProtocolData.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)

            if self.public_key_tag == Id018ProxfordOperationProtocolData.PublicKeyTag.bls:
                self.bls = self._io.read_bytes(48)



    class Bh2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__block_header__alpha__full_header = Id018ProxfordOperationProtocolData.Id018ProxfordBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Named(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.named = self._io.read_bytes_full()


    class Id018ProxfordEntrypoint(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__entrypoint_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Id018ProxfordEntrypointTag, self._io.read_u1())
            if self.id_018__proxford__entrypoint_tag == Id018ProxfordOperationProtocolData.Id018ProxfordEntrypointTag.named:
                self.named = Id018ProxfordOperationProtocolData.Named0(self._io, self, self._root)



    class PrimGeneric(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id018ProxfordOperationProtocolData.Id018ProxfordMichelsonV1Primitives(self._io, self, self._root)
            self.args = Id018ProxfordOperationProtocolData.Args0(self._io, self, self._root)
            self.annots = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class SlotHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.slot_index = self._io.read_u1()
            self.commitment = self._io.read_bytes(48)
            self.commitment_proof = self._io.read_bytes(96)


    class Bh20(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh2 = self._io.read_u4be()
            if not self.len_bh2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh2, self._io, u"/types/bh2_0/seq/0")
            self._raw_bh2 = self._io.read_bytes(self.len_bh2)
            _io__raw_bh2 = KaitaiStream(BytesIO(self._raw_bh2))
            self.bh2 = Id018ProxfordOperationProtocolData.Bh2(_io__raw_bh2, self, self._root)


    class Commitment(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.compressed_state = self._io.read_bytes(32)
            self.inbox_level = self._io.read_s4be()
            self.predecessor = self._io.read_bytes(32)
            self.number_of_ticks = self._io.read_s8be()


    class Delegation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Bool, self._io.read_u1())
            if self.delegate_tag == Id018ProxfordOperationProtocolData.Bool.true:
                self.delegate = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)



    class RegisterGlobalConstant(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.value = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Solution(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.solution_field0 = self._io.read_bytes(100)
            self.solution_field1 = self._io.read_bytes(100)


    class PayloadEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_elt = self._io.read_bytes(32)


    class Sequence0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_sequence = self._io.read_u4be()
            if not self.len_sequence <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_sequence, self._io, u"/types/sequence_0/seq/0")
            self._raw_sequence = self._io.read_bytes(self.len_sequence)
            _io__raw_sequence = KaitaiStream(BytesIO(self._raw_sequence))
            self.sequence = Id018ProxfordOperationProtocolData.Sequence(_io__raw_sequence, self, self._root)


    class PendingPis0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_pending_pis = self._io.read_u4be()
            if not self.len_pending_pis <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_pending_pis, self._io, u"/types/pending_pis_0/seq/0")
            self._raw_pending_pis = self._io.read_bytes(self.len_pending_pis)
            _io__raw_pending_pis = KaitaiStream(BytesIO(self._raw_pending_pis))
            self.pending_pis = Id018ProxfordOperationProtocolData.PendingPis(_io__raw_pending_pis, self, self._root)


    class DoubleAttestationEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id018ProxfordOperationProtocolData.Op12(self._io, self, self._root)
            self.op2 = Id018ProxfordOperationProtocolData.Op22(self._io, self, self._root)


    class PrivatePis0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_private_pis = self._io.read_u4be()
            if not self.len_private_pis <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_private_pis, self._io, u"/types/private_pis_0/seq/0")
            self._raw_private_pis = self._io.read_bytes(self.len_private_pis)
            _io__raw_private_pis = KaitaiStream(BytesIO(self._raw_private_pis))
            self.private_pis = Id018ProxfordOperationProtocolData.PrivatePis(_io__raw_private_pis, self, self._root)


    class PrivatePisEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.private_pis_elt_field0 = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.private_pis_elt_field1 = Id018ProxfordOperationProtocolData.PrivatePisEltField1(self._io, self, self._root)


    class Sequence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_entries = []
            i = 0
            while not self._io.is_eof():
                self.sequence_entries.append(Id018ProxfordOperationProtocolData.SequenceEntries(self._io, self, self._root))
                i += 1



    class Bh10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bh1 = self._io.read_u4be()
            if not self.len_bh1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bh1, self._io, u"/types/bh1_0/seq/0")
            self._raw_bh1 = self._io.read_bytes(self.len_bh1)
            _io__raw_bh1 = KaitaiStream(BytesIO(self._raw_bh1))
            self.bh1 = Id018ProxfordOperationProtocolData.Bh1(_io__raw_bh1, self, self._root)


    class Op22(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op2 = self._io.read_u4be()
            if not self.len_op2 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op2, self._io, u"/types/op2_2/seq/0")
            self._raw_op2 = self._io.read_bytes(self.len_op2)
            _io__raw_op2 = KaitaiStream(BytesIO(self._raw_op2))
            self.op2 = Id018ProxfordOperationProtocolData.Op21(_io__raw_op2, self, self._root)


    class Id018ProxfordMichelsonV1Primitives(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__michelson__v1__primitives = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Id018ProxfordMichelsonV1Primitives, self._io.read_u1())


    class CircuitsInfoEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.circuits_info_elt_field0 = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.circuits_info_elt_field1 = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.CircuitsInfoEltField1Tag, self._io.read_u1())


    class Message0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_message = self._io.read_u4be()
            if not self.len_message <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_message, self._io, u"/types/message_0/seq/0")
            self._raw_message = self._io.read_bytes(self.len_message)
            _io__raw_message = KaitaiStream(BytesIO(self._raw_message))
            self.message = Id018ProxfordOperationProtocolData.Message(_io__raw_message, self, self._root)


    class InboxProof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.level = self._io.read_s4be()
            self.message_counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.serialized_proof = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class OpEltField1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_elt_field1_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.OpEltField1Tag, self._io.read_u1())
            if self.op_elt_field1_tag == Id018ProxfordOperationProtocolData.OpEltField1Tag.some:
                self.some = Id018ProxfordOperationProtocolData.Some(self._io, self, self._root)



    class BytesDynUint30(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_bytes_dyn_uint30 = self._io.read_u4be()
            if not self.len_bytes_dyn_uint30 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_bytes_dyn_uint30, self._io, u"/types/bytes_dyn_uint30/seq/0")
            self.bytes_dyn_uint30 = self._io.read_bytes(self.len_bytes_dyn_uint30)


    class Op0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op = self._io.read_u4be()
            if not self.len_op <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op, self._io, u"/types/op_0/seq/0")
            self._raw_op = self._io.read_bytes(self.len_op)
            _io__raw_op = KaitaiStream(BytesIO(self._raw_op))
            self.op = Id018ProxfordOperationProtocolData.Op(_io__raw_op, self, self._root)


    class NewState(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.new_state_entries = []
            i = 0
            while not self._io.is_eof():
                self.new_state_entries.append(Id018ProxfordOperationProtocolData.NewStateEntries(self._io, self, self._root))
                i += 1



    class SmartRollupPublish(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)
            self.commitment = Id018ProxfordOperationProtocolData.Commitment(self._io, self, self._root)


    class Bh1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__block_header__alpha__full_header = Id018ProxfordOperationProtocolData.Id018ProxfordBlockHeaderAlphaFullHeader(self._io, self, self._root)


    class Payload0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_payload = self._io.read_u4be()
            if not self.len_payload <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_payload, self._io, u"/types/payload_0/seq/0")
            self._raw_payload = self._io.read_bytes(self.len_payload)
            _io__raw_payload = KaitaiStream(BytesIO(self._raw_payload))
            self.payload = Id018ProxfordOperationProtocolData.Payload(_io__raw_payload, self, self._root)


    class Dissection0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_dissection = self._io.read_u4be()
            if not self.len_dissection <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_dissection, self._io, u"/types/dissection_0/seq/0")
            self._raw_dissection = self._io.read_bytes(self.len_dissection)
            _io__raw_dissection = KaitaiStream(BytesIO(self._raw_dissection))
            self.dissection = Id018ProxfordOperationProtocolData.Dissection(_io__raw_dissection, self, self._root)


    class SetDepositsLimit(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.limit_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Bool, self._io.read_u1())
            if self.limit_tag == Id018ProxfordOperationProtocolData.Bool.true:
                self.limit = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)



    class Op10(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_op1 = self._io.read_u4be()
            if not self.len_op1 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_op1, self._io, u"/types/op1_0/seq/0")
            self._raw_op1 = self._io.read_bytes(self.len_op1)
            _io__raw_op1 = KaitaiStream(BytesIO(self._raw_op1))
            self.op1 = Id018ProxfordOperationProtocolData.Op1(_io__raw_op1, self, self._root)


    class InitState0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_init_state = self._io.read_u4be()
            if not self.len_init_state <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_init_state, self._io, u"/types/init_state_0/seq/0")
            self._raw_init_state = self._io.read_bytes(self.len_init_state)
            _io__raw_init_state = KaitaiStream(BytesIO(self._raw_init_state))
            self.init_state = Id018ProxfordOperationProtocolData.InitState(_io__raw_init_state, self, self._root)


    class Op21(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__inlined__attestation = Id018ProxfordOperationProtocolData.Id018ProxfordInlinedAttestation(self._io, self, self._root)


    class BlsSignaturePrefix(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.bls_signature_prefix_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.BlsSignaturePrefixTag, self._io.read_u1())
            if self.bls_signature_prefix_tag == Id018ProxfordOperationProtocolData.BlsSignaturePrefixTag.bls_prefix:
                self.bls_prefix = self._io.read_bytes(32)



    class Proof(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pvm_step = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.input_proof_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Bool, self._io.read_u1())
            if self.input_proof_tag == Id018ProxfordOperationProtocolData.Bool.true:
                self.input_proof = Id018ProxfordOperationProtocolData.InputProof(self._io, self, self._root)



    class SmartRollupRefute(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.rollup = self._io.read_bytes(20)
            self.opponent = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.refutation = Id018ProxfordOperationProtocolData.Refutation(self._io, self, self._root)


    class Origination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.balance = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.delegate_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Bool, self._io.read_u1())
            if self.delegate_tag == Id018ProxfordOperationProtocolData.Bool.true:
                self.delegate = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)

            self.script = Id018ProxfordOperationProtocolData.Id018ProxfordScriptedContracts(self._io, self, self._root)


    class DrainDelegate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.consensus_key = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.delegate = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.destination = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)


    class Id018ProxfordInlinedPreattestation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__inlined__preattestation = operation__shell_header.OperationShellHeader(self._io)
            self.operations = Id018ProxfordOperationProtocolData.Id018ProxfordInlinedPreattestationContents(self._io, self, self._root)
            self.signature_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Bool, self._io.read_u1())
            if self.signature_tag == Id018ProxfordOperationProtocolData.Bool.true:
                self.signature = self._io.read_bytes_full()



    class ZkRollupUpdate(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.zk_rollup = self._io.read_bytes(20)
            self.update = Id018ProxfordOperationProtocolData.Update(self._io, self, self._root)


    class Id018ProxfordInlinedAttestationMempoolContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__inlined__attestation_mempool__contents_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Id018ProxfordInlinedAttestationMempoolContentsTag, self._io.read_u1())
            if self.id_018__proxford__inlined__attestation_mempool__contents_tag == Id018ProxfordOperationProtocolData.Id018ProxfordInlinedAttestationMempoolContentsTag.attestation:
                self.attestation = Id018ProxfordOperationProtocolData.Attestation(self._io, self, self._root)



    class Id018ProxfordOperationAlphaContentsOrSignaturePrefix(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag, self._io.read_u1())
            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.signature_prefix:
                self.signature_prefix = Id018ProxfordOperationProtocolData.BlsSignaturePrefix(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.preattestation:
                self.preattestation = Id018ProxfordOperationProtocolData.Preattestation(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.attestation:
                self.attestation = Id018ProxfordOperationProtocolData.Attestation(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.double_preattestation_evidence:
                self.double_preattestation_evidence = Id018ProxfordOperationProtocolData.DoublePreattestationEvidence(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.double_attestation_evidence:
                self.double_attestation_evidence = Id018ProxfordOperationProtocolData.DoubleAttestationEvidence(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.dal_attestation:
                self.dal_attestation = Id018ProxfordOperationProtocolData.DalAttestation(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.seed_nonce_revelation:
                self.seed_nonce_revelation = Id018ProxfordOperationProtocolData.SeedNonceRevelation(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.vdf_revelation:
                self.vdf_revelation = Id018ProxfordOperationProtocolData.Solution(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.double_baking_evidence:
                self.double_baking_evidence = Id018ProxfordOperationProtocolData.DoubleBakingEvidence(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.activate_account:
                self.activate_account = Id018ProxfordOperationProtocolData.ActivateAccount(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.proposals:
                self.proposals = Id018ProxfordOperationProtocolData.Proposals1(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.ballot:
                self.ballot = Id018ProxfordOperationProtocolData.Ballot(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.reveal:
                self.reveal = Id018ProxfordOperationProtocolData.Reveal(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.transaction:
                self.transaction = Id018ProxfordOperationProtocolData.Transaction(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.origination:
                self.origination = Id018ProxfordOperationProtocolData.Origination(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.delegation:
                self.delegation = Id018ProxfordOperationProtocolData.Delegation(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.set_deposits_limit:
                self.set_deposits_limit = Id018ProxfordOperationProtocolData.SetDepositsLimit(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.increase_paid_storage:
                self.increase_paid_storage = Id018ProxfordOperationProtocolData.IncreasePaidStorage(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.update_consensus_key:
                self.update_consensus_key = Id018ProxfordOperationProtocolData.UpdateConsensusKey(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.drain_delegate:
                self.drain_delegate = Id018ProxfordOperationProtocolData.DrainDelegate(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.failing_noop:
                self.failing_noop = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.register_global_constant:
                self.register_global_constant = Id018ProxfordOperationProtocolData.RegisterGlobalConstant(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.transfer_ticket:
                self.transfer_ticket = Id018ProxfordOperationProtocolData.TransferTicket(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.dal_publish_slot_header:
                self.dal_publish_slot_header = Id018ProxfordOperationProtocolData.DalPublishSlotHeader(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_originate:
                self.smart_rollup_originate = Id018ProxfordOperationProtocolData.SmartRollupOriginate(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_add_messages:
                self.smart_rollup_add_messages = Id018ProxfordOperationProtocolData.SmartRollupAddMessages(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_cement:
                self.smart_rollup_cement = Id018ProxfordOperationProtocolData.SmartRollupCement(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_publish:
                self.smart_rollup_publish = Id018ProxfordOperationProtocolData.SmartRollupPublish(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_refute:
                self.smart_rollup_refute = Id018ProxfordOperationProtocolData.SmartRollupRefute(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_timeout:
                self.smart_rollup_timeout = Id018ProxfordOperationProtocolData.SmartRollupTimeout(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_execute_outbox_message:
                self.smart_rollup_execute_outbox_message = Id018ProxfordOperationProtocolData.SmartRollupExecuteOutboxMessage(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.smart_rollup_recover_bond:
                self.smart_rollup_recover_bond = Id018ProxfordOperationProtocolData.SmartRollupRecoverBond(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.zk_rollup_origination:
                self.zk_rollup_origination = Id018ProxfordOperationProtocolData.ZkRollupOrigination(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.zk_rollup_publish:
                self.zk_rollup_publish = Id018ProxfordOperationProtocolData.ZkRollupPublish(self._io, self, self._root)

            if self.id_018__proxford__operation__alpha__contents_or_signature_prefix_tag == Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefixTag.zk_rollup_update:
                self.zk_rollup_update = Id018ProxfordOperationProtocolData.ZkRollupUpdate(self._io, self, self._root)



    class Message(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.message_entries = []
            i = 0
            while not self._io.is_eof():
                self.message_entries.append(Id018ProxfordOperationProtocolData.MessageEntries(self._io, self, self._root))
                i += 1



    class Whitelist(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.whitelist_entries = []
            i = 0
            while not self._io.is_eof():
                self.whitelist_entries.append(Id018ProxfordOperationProtocolData.WhitelistEntries(self._io, self, self._root))
                i += 1



    class InitState(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.init_state_entries = []
            i = 0
            while not self._io.is_eof():
                self.init_state_entries.append(Id018ProxfordOperationProtocolData.InitStateEntries(self._io, self, self._root))
                i += 1



    class Args(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_entries = []
            i = 0
            while not self._io.is_eof():
                self.args_entries.append(Id018ProxfordOperationProtocolData.ArgsEntries(self._io, self, self._root))
                i += 1



    class ZkRollupOrigination(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.public_parameters = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)
            self.circuits_info = Id018ProxfordOperationProtocolData.CircuitsInfo0(self._io, self, self._root)
            self.init_state = Id018ProxfordOperationProtocolData.InitState0(self._io, self, self._root)
            self.nb_ops = Id018ProxfordOperationProtocolData.Int31(self._io, self, self._root)


    class DoublePreattestationEvidence(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op1 = Id018ProxfordOperationProtocolData.Op10(self._io, self, self._root)
            self.op2 = Id018ProxfordOperationProtocolData.Op20(self._io, self, self._root)


    class Op2(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__inlined__preattestation = Id018ProxfordOperationProtocolData.Id018ProxfordInlinedPreattestation(self._io, self, self._root)


    class Dissection(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.dissection_entries = []
            i = 0
            while not self._io.is_eof():
                self.dissection_entries.append(Id018ProxfordOperationProtocolData.DissectionEntries(self._io, self, self._root))
                i += 1



    class PrivatePis(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.private_pis_entries = []
            i = 0
            while not self._io.is_eof():
                self.private_pis_entries.append(Id018ProxfordOperationProtocolData.PrivatePisEntries(self._io, self, self._root))
                i += 1



    class UpdateConsensusKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.pk = Id018ProxfordOperationProtocolData.PublicKey(self._io, self, self._root)


    class Prim1ArgNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id018ProxfordOperationProtocolData.Id018ProxfordMichelsonV1Primitives(self._io, self, self._root)
            self.arg = Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1Expression(self._io, self, self._root)


    class ContentsAndSignaturePrefixEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__operation__alpha__contents_or_signature_prefix = Id018ProxfordOperationProtocolData.Id018ProxfordOperationAlphaContentsOrSignaturePrefix(self._io, self, self._root)


    class Update(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.pending_pis = Id018ProxfordOperationProtocolData.PendingPis0(self._io, self, self._root)
            self.private_pis = Id018ProxfordOperationProtocolData.PrivatePis0(self._io, self, self._root)
            self.fee_pi = Id018ProxfordOperationProtocolData.NewState0(self._io, self, self._root)
            self.proof = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class SequenceEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.sequence_elt = Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1Expression(self._io, self, self._root)


    class Int31(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.int31 = self._io.read_s4be()
            if not self.int31 >= -1073741824:
                raise kaitaistruct.ValidationLessThanError(-1073741824, self.int31, self._io, u"/types/int31/seq/0")
            if not self.int31 <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.int31, self._io, u"/types/int31/seq/0")


    class NChunk(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_more = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(7)


    class Prim2ArgsNoAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id018ProxfordOperationProtocolData.Id018ProxfordMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1Expression(self._io, self, self._root)


    class Id018ProxfordMutez(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__mutez = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)


    class Start(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.player_commitment_hash = self._io.read_bytes(32)
            self.opponent_commitment_hash = self._io.read_bytes(32)


    class InitStateEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.init_state_elt = self._io.read_bytes(32)


    class ArgsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.args_elt = Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1Expression(self._io, self, self._root)


    class RawData0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_raw_data = self._io.read_u2be()
            if not self.len_raw_data <= 4096:
                raise kaitaistruct.ValidationGreaterThanError(4096, self.len_raw_data, self._io, u"/types/raw_data_0/seq/0")
            self._raw_raw_data = self._io.read_bytes(self.len_raw_data)
            _io__raw_raw_data = KaitaiStream(BytesIO(self._raw_raw_data))
            self.raw_data = Id018ProxfordOperationProtocolData.RawData(_io__raw_raw_data, self, self._root)


    class Args0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_args = self._io.read_u4be()
            if not self.len_args <= 1073741823:
                raise kaitaistruct.ValidationGreaterThanError(1073741823, self.len_args, self._io, u"/types/args_0/seq/0")
            self._raw_args = self._io.read_bytes(self.len_args)
            _io__raw_args = KaitaiStream(BytesIO(self._raw_args))
            self.args = Id018ProxfordOperationProtocolData.Args(_io__raw_args, self, self._root)


    class Named0(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.len_named = self._io.read_u1()
            if not self.len_named <= 31:
                raise kaitaistruct.ValidationGreaterThanError(31, self.len_named, self._io, u"/types/named_0/seq/0")
            self._raw_named = self._io.read_bytes(self.len_named)
            _io__raw_named = KaitaiStream(BytesIO(self._raw_named))
            self.named = Id018ProxfordOperationProtocolData.Named(_io__raw_named, self, self._root)


    class Transaction(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.amount = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.destination = Id018ProxfordOperationProtocolData.Id018ProxfordContractId(self._io, self, self._root)
            self.parameters_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Bool, self._io.read_u1())
            if self.parameters_tag == Id018ProxfordOperationProtocolData.Bool.true:
                self.parameters = Id018ProxfordOperationProtocolData.Parameters(self._io, self, self._root)



    class Micheline018ProxfordMichelsonV1Expression(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.micheline__018__proxford__michelson_v1__expression_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1ExpressionTag, self._io.read_u1())
            if self.micheline__018__proxford__michelson_v1__expression_tag == Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1ExpressionTag.int:
                self.int = Id018ProxfordOperationProtocolData.Z(self._io, self, self._root)

            if self.micheline__018__proxford__michelson_v1__expression_tag == Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1ExpressionTag.string:
                self.string = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)

            if self.micheline__018__proxford__michelson_v1__expression_tag == Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1ExpressionTag.sequence:
                self.sequence = Id018ProxfordOperationProtocolData.Sequence0(self._io, self, self._root)

            if self.micheline__018__proxford__michelson_v1__expression_tag == Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1ExpressionTag.prim__no_args__no_annots:
                self.prim__no_args__no_annots = Id018ProxfordOperationProtocolData.Id018ProxfordMichelsonV1Primitives(self._io, self, self._root)

            if self.micheline__018__proxford__michelson_v1__expression_tag == Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1ExpressionTag.prim__no_args__some_annots:
                self.prim__no_args__some_annots = Id018ProxfordOperationProtocolData.PrimNoArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__018__proxford__michelson_v1__expression_tag == Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1ExpressionTag.prim__1_arg__no_annots:
                self.prim__1_arg__no_annots = Id018ProxfordOperationProtocolData.Prim1ArgNoAnnots(self._io, self, self._root)

            if self.micheline__018__proxford__michelson_v1__expression_tag == Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1ExpressionTag.prim__1_arg__some_annots:
                self.prim__1_arg__some_annots = Id018ProxfordOperationProtocolData.Prim1ArgSomeAnnots(self._io, self, self._root)

            if self.micheline__018__proxford__michelson_v1__expression_tag == Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1ExpressionTag.prim__2_args__no_annots:
                self.prim__2_args__no_annots = Id018ProxfordOperationProtocolData.Prim2ArgsNoAnnots(self._io, self, self._root)

            if self.micheline__018__proxford__michelson_v1__expression_tag == Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1ExpressionTag.prim__2_args__some_annots:
                self.prim__2_args__some_annots = Id018ProxfordOperationProtocolData.Prim2ArgsSomeAnnots(self._io, self, self._root)

            if self.micheline__018__proxford__michelson_v1__expression_tag == Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1ExpressionTag.prim__generic:
                self.prim__generic = Id018ProxfordOperationProtocolData.PrimGeneric(self._io, self, self._root)

            if self.micheline__018__proxford__michelson_v1__expression_tag == Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1ExpressionTag.bytes:
                self.bytes = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)



    class DalAttestation(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.attestation = Id018ProxfordOperationProtocolData.Z(self._io, self, self._root)
            self.level = self._io.read_s4be()
            self.slot = self._io.read_u2be()


    class Parameters(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.entrypoint = Id018ProxfordOperationProtocolData.Id018ProxfordEntrypoint(self._io, self, self._root)
            self.value = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Some(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents = Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1Expression(self._io, self, self._root)
            self.ty = Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1Expression(self._io, self, self._root)
            self.ticketer = Id018ProxfordOperationProtocolData.Id018ProxfordContractId(self._io, self, self._root)


    class Prim2ArgsSomeAnnots(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.prim = Id018ProxfordOperationProtocolData.Id018ProxfordMichelsonV1Primitives(self._io, self, self._root)
            self.arg1 = Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1Expression(self._io, self, self._root)
            self.arg2 = Id018ProxfordOperationProtocolData.Micheline018ProxfordMichelsonV1Expression(self._io, self, self._root)
            self.annots = Id018ProxfordOperationProtocolData.BytesDynUint30(self._io, self, self._root)


    class Id018ProxfordOperationWithLegacyAttestationNameAlphaContentsAndSignature(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contents_and_signature_prefix = []
            i = 0
            while not self._io.is_eof():
                self.contents_and_signature_prefix.append(Id018ProxfordOperationProtocolData.ContentsAndSignaturePrefixEntries(self._io, self, self._root))
                i += 1

            self.signature_suffix = self._io.read_bytes(64)


    class Id018ProxfordBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_hash = self._io.read_bytes(32)
            self.payload_round = self._io.read_s4be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == Id018ProxfordOperationProtocolData.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.per_block_votes = Id018ProxfordOperationProtocolData.Id018ProxfordPerBlockVotes(self._io, self, self._root)


    class Proposals1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposals = Id018ProxfordOperationProtocolData.Proposals0(self._io, self, self._root)


    class DissectionEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.state_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Bool, self._io.read_u1())
            if self.state_tag == Id018ProxfordOperationProtocolData.Bool.true:
                self.state = self._io.read_bytes(32)

            self.tick = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)


    class Id018ProxfordPerBlockVotes(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__per_block_votes_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.Id018ProxfordPerBlockVotesTag, self._io.read_u1())


    class Ballot(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.period = self._io.read_s4be()
            self.proposal = self._io.read_bytes(32)
            self.ballot = self._io.read_s1()


    class DalPublishSlotHeader(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.slot_header = Id018ProxfordOperationProtocolData.SlotHeader(self._io, self, self._root)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id018ProxfordOperationProtocolData.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id018ProxfordOperationProtocolData.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id018ProxfordOperationProtocolData.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id018ProxfordOperationProtocolData.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id018ProxfordOperationProtocolData.PublicKeyHashTag.bls:
                self.bls = self._io.read_bytes(20)



    class Op11(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__inlined__attestation = Id018ProxfordOperationProtocolData.Id018ProxfordInlinedAttestation(self._io, self, self._root)


    class Proposals(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.proposals_entries = []
            i = 0
            while not self._io.is_eof():
                self.proposals_entries.append(Id018ProxfordOperationProtocolData.ProposalsEntries(self._io, self, self._root))
                i += 1



    class Z(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.has_tail = self._io.read_bits_int_be(1) != 0
            self.sign = self._io.read_bits_int_be(1) != 0
            self.payload = self._io.read_bits_int_be(6)
            self._io.align_to_byte()
            if self.has_tail:
                self.tail = []
                i = 0
                while True:
                    _ = Id018ProxfordOperationProtocolData.NChunk(self._io, self, self._root)
                    self.tail.append(_)
                    if not (_.has_more):
                        break
                    i += 1



    class OpEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_elt_field0 = Id018ProxfordOperationProtocolData.OpEltField0(self._io, self, self._root)
            self.op_elt_field1 = Id018ProxfordOperationProtocolData.OpEltField1(self._io, self, self._root)


    class ProposalsEntries(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.protocol_hash = self._io.read_bytes(32)


    class Op1(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_018__proxford__inlined__preattestation = Id018ProxfordOperationProtocolData.Id018ProxfordInlinedPreattestation(self._io, self, self._root)


    class ZkRollupPublish(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.source = Id018ProxfordOperationProtocolData.PublicKeyHash(self._io, self, self._root)
            self.fee = Id018ProxfordOperationProtocolData.Id018ProxfordMutez(self._io, self, self._root)
            self.counter = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.gas_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.storage_limit = Id018ProxfordOperationProtocolData.N(self._io, self, self._root)
            self.zk_rollup = self._io.read_bytes(20)
            self.op = Id018ProxfordOperationProtocolData.Op0(self._io, self, self._root)


    class Op(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.op_entries = []
            i = 0
            while not self._io.is_eof():
                self.op_entries.append(Id018ProxfordOperationProtocolData.OpEntries(self._io, self, self._root))
                i += 1




