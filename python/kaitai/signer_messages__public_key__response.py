# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class SignerMessagesPublicKeyResponse(KaitaiStruct):
    """Encoding id: signer_messages.public_key.response."""

    class PublicKeyTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
        bls = 3
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.pubkey = SignerMessagesPublicKeyResponse.PublicKey(self._io, self, self._root)

    class PublicKey(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_tag = KaitaiStream.resolve_enum(SignerMessagesPublicKeyResponse.PublicKeyTag, self._io.read_u1())
            if self.public_key_tag == SignerMessagesPublicKeyResponse.PublicKeyTag.ed25519:
                self.ed25519 = self._io.read_bytes(32)

            if self.public_key_tag == SignerMessagesPublicKeyResponse.PublicKeyTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(33)

            if self.public_key_tag == SignerMessagesPublicKeyResponse.PublicKeyTag.p256:
                self.p256 = self._io.read_bytes(33)

            if self.public_key_tag == SignerMessagesPublicKeyResponse.PublicKeyTag.bls:
                self.bls = self._io.read_bytes(48)




