# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class AlphaBlockHeaderProtocolData(KaitaiStruct):
    """Encoding id: alpha.block_header.protocol_data."""

    class AlphaPerBlockVotesTag(Enum):
        case__0 = 0
        case__1 = 1
        case__2 = 2
        case__4 = 4
        case__5 = 5
        case__6 = 6
        case__8 = 8
        case__9 = 9
        case__10 = 10

    class Bool(Enum):
        false = 0
        true = 255
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.alpha__block_header__alpha__signed_contents = AlphaBlockHeaderProtocolData.AlphaBlockHeaderAlphaSignedContents(self._io, self, self._root)

    class AlphaBlockHeaderAlphaSignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__block_header__alpha__unsigned_contents = AlphaBlockHeaderProtocolData.AlphaBlockHeaderAlphaUnsignedContents(self._io, self, self._root)
            self.signature = self._io.read_bytes_full()


    class AlphaBlockHeaderAlphaUnsignedContents(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.payload_hash = self._io.read_bytes(32)
            self.payload_round = self._io.read_s4be()
            self.proof_of_work_nonce = self._io.read_bytes(8)
            self.seed_nonce_hash_tag = KaitaiStream.resolve_enum(AlphaBlockHeaderProtocolData.Bool, self._io.read_u1())
            if self.seed_nonce_hash_tag == AlphaBlockHeaderProtocolData.Bool.true:
                self.seed_nonce_hash = self._io.read_bytes(32)

            self.per_block_votes = AlphaBlockHeaderProtocolData.AlphaPerBlockVotes(self._io, self, self._root)


    class AlphaPerBlockVotes(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.alpha__per_block_votes_tag = KaitaiStream.resolve_enum(AlphaBlockHeaderProtocolData.AlphaPerBlockVotesTag, self._io.read_u1())



