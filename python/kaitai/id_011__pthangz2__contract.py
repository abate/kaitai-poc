# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

class Id011Pthangz2Contract(KaitaiStruct):
    """Encoding id: 011-PtHangz2.contract."""

    class Id011Pthangz2ContractIdTag(Enum):
        implicit = 0
        originated = 1

    class PublicKeyHashTag(Enum):
        ed25519 = 0
        secp256k1 = 1
        p256 = 2
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.id_011__pthangz2__contract_id = Id011Pthangz2Contract.Id011Pthangz2ContractId(self._io, self, self._root)

    class Id011Pthangz2ContractId(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.id_011__pthangz2__contract_id_tag = KaitaiStream.resolve_enum(Id011Pthangz2Contract.Id011Pthangz2ContractIdTag, self._io.read_u1())
            if self.id_011__pthangz2__contract_id_tag == Id011Pthangz2Contract.Id011Pthangz2ContractIdTag.implicit:
                self.implicit = Id011Pthangz2Contract.PublicKeyHash(self._io, self, self._root)

            if self.id_011__pthangz2__contract_id_tag == Id011Pthangz2Contract.Id011Pthangz2ContractIdTag.originated:
                self.originated = Id011Pthangz2Contract.Originated(self._io, self, self._root)



    class Originated(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.contract_hash = self._io.read_bytes(20)
            self.originated_padding = self._io.read_bytes(1)


    class PublicKeyHash(KaitaiStruct):
        def __init__(self, _io, _parent=None, _root=None):
            self._io = _io
            self._parent = _parent
            self._root = _root if _root else self
            self._read()

        def _read(self):
            self.public_key_hash_tag = KaitaiStream.resolve_enum(Id011Pthangz2Contract.PublicKeyHashTag, self._io.read_u1())
            if self.public_key_hash_tag == Id011Pthangz2Contract.PublicKeyHashTag.ed25519:
                self.ed25519 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id011Pthangz2Contract.PublicKeyHashTag.secp256k1:
                self.secp256k1 = self._io.read_bytes(20)

            if self.public_key_hash_tag == Id011Pthangz2Contract.PublicKeyHashTag.p256:
                self.p256 = self._io.read_bytes(20)




