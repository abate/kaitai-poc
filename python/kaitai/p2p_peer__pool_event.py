# This is a generated file! Please edit source .ksy file and use kaitai-struct-compiler to rebuild

import kaitaistruct
from kaitaistruct import KaitaiStruct, KaitaiStream, BytesIO
from enum import Enum


if getattr(kaitaistruct, 'API_VERSION', (0, 9)) < (0, 9):
    raise Exception("Incompatible Kaitai Struct Python API: 0.9 or later is required, but you have %s" % (kaitaistruct.__version__))

from kaitai import timestamp__system
from kaitai import p2p_address
class P2pPeerPoolEvent(KaitaiStruct):
    """Encoding id: p2p_peer.pool_event
    Description: An event that may happen during maintenance of and other operations on the connection to a specific peer."""

    class Bool(Enum):
        false = 0
        true = 255

    class Kind(Enum):
        incoming_request = 0
        rejecting_request = 1
        request_rejected = 2
        connection_established = 3
        disconnection = 4
        external_disconnection = 5
    def __init__(self, _io, _parent=None, _root=None):
        self._io = _io
        self._parent = _parent
        self._root = _root if _root else self
        self._read()

    def _read(self):
        self.kind = KaitaiStream.resolve_enum(P2pPeerPoolEvent.Kind, self._io.read_u1())
        self.timestamp = timestamp__system.TimestampSystem(self._io)
        self.addr = p2p_address.P2pAddress(self._io)
        self.port_tag = KaitaiStream.resolve_enum(P2pPeerPoolEvent.Bool, self._io.read_u1())
        if self.port_tag == P2pPeerPoolEvent.Bool.true:
            self.port = self._io.read_u2be()



