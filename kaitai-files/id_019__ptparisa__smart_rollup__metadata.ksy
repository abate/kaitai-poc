meta:
  id: id_019__ptparisa__smart_rollup__metadata
  endian: be
doc: ! 'Encoding id: 019-PtParisA.smart_rollup.metadata'
seq:
- id: address
  size: 20
- id: origination_level
  type: s4
