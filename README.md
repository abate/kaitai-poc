
# kaitai examples

Two small examples on how to use kaitai to decode RPC binary representations for Octez.

## node

    $ cd node
    $ # nvm use stable // if needed
    $ npm i
    $ node block.jsls

## python

    $ cd python
    $ poetry install
    $ poetry run ./block.js

The kaitai parsers are respectively committed in the kaitai directory in the python
and javascript examples. 

For python we used the command :

    $ kaitai-struct-compiler -t python --outdir python --python-package kaitai *.ksy

while for javascript

    $ kaitai-struct-compiler -t javascript --outdir javascript *.ksy

the ksy files are in tezos/tezos/contrib/kaitai-struct-files